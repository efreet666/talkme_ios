#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "MRMailSDK+Private.h"
#import "MRMailSDK.h"
#import "MRMailSDKConstants.h"
#import "MRSDKAuthorizationResult.h"

FOUNDATION_EXPORT double MRMailSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char MRMailSDKVersionString[];

