//
//  ChatsListViewControllerViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 24.03.2021.
//

import RxSwift
import RxRelay

final class ChatsListViewControllerViewModel {

    enum Flow {
        case toChat(chatId: Int)
        case createNewChat
    }

    // MARK: - Public properties

    let flow = PublishRelay<Flow>()
    let items = PublishRelay<[AnyTableViewCellModelProtocol]>()
    var groupChatModels = [GroupChatListCellViewModel]()
    var chatModels = [ChatListCellViewModel]()
    let userStatus = PublishSubject<UserStatus>()
    var isNeedToUpdateChatList = false

    // MARK: - Private properties

    private var allChatsService: SocketAllChatsService
    private var fullModels = [AnyTableViewCellModelProtocol]()
    private var chatsBag = DisposeBag()
    private var timer: Timer?
    private var searchText: String? = ""
    private var appInBackground = false

    // MARK: - Initializers

    init(service: SocketAllChatsService) {
        allChatsService = service
        followChatsManager(allChatsService: service)
        bindAppState()
    }

    // MARK: - Public methods

    func fetchChats() {
        items.accept(fullModels)
        guard let searchText = searchText, searchText.count > 2 else {
            allChatsService.fetchChats(userId: UserDefaultsHelper.shared.userId)
            return
        }
        filterItems(string: searchText)
    }

    func clearList() {
        items.accept([])
    }

    func filterItems(string: String) {
        searchText = string
        guard string.count > 2 else {
            if fullModels.count != 0 && string != "" {
                items.accept(fullModels)
            }
            return
        }
        allChatsService.searching(userId: UserDefaultsHelper.shared.userId, searchText: string)
    }

    func followListChanges() {
        timer = Timer.scheduledTimer(withTimeInterval: 4, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            guard let searchText = self.searchText, searchText.count <= 2, self.isNeedToUpdateChatList else { return }
            self.allChatsService.fetchChats(userId: UserDefaultsHelper.shared.userId)
        }
    }

    // MARK: - Private methods

    private func followChatsManager(allChatsService: SocketAllChatsService) {

        allChatsService
            .isConnected
            .subscribe(onNext: { [weak self] connection in
                guard let self = self else { return }
                switch connection {
                case .connected:
                    self.allChatsService.fetchChats(userId: UserDefaultsHelper.shared.userId)
                case .cancelled, .disconnected, .error:
                    guard !self.appInBackground else { return }
                    self.reconnectToChat()
                }
            })
            .disposed(by: chatsBag)

        allChatsService
            .chats
            .subscribe(onNext: { [weak self] chatsModel in
                self?.getItems(chatsModel: chatsModel, fromSearch: false)
            })
            .disposed(by: chatsBag)

        allChatsService
            .searchChats
            .subscribe(onNext: { [weak self] searchChatsModel in
                let chatsFromMessage = searchChatsModel.messages.map { message in
                    return Chat(
                        lastMessage: message.content,
                        chatId: message.chatId,
                        lastTimeStamp: Double(message.timestamp),
                        participants: [
                            Participants(
                                userId: message.userId,
                                name: message.author,
                                avatarUrl: message.avatar,
                                lastSeen: Double(message.timestamp),
                                online: false,
                                timeLeft: message.timestamp
                            )],
                        new: 0,
                        group: false)
                }
                let commonChatModel = ChatModel(userId: searchChatsModel.userId, chats: searchChatsModel.chats + chatsFromMessage)
                self?.getItems(chatsModel: commonChatModel, fromSearch: true)
            })
            .disposed(by: chatsBag)
    }

    private func getItems(chatsModel: ChatModel, fromSearch: Bool) {
        var models: [AnyTableViewCellModelProtocol] = [StartNewChatListCellViewModel()]
        chatsModel.chats.forEach { chat in
            guard let chatParticipant = chat.participants.first(where: { $0.userId != UserDefaultsHelper.shared.userId }) else { return }
            let model = ChatListCellViewModel(chatModel: chat, userId: chatParticipant.userId)
            bindChatVM(model)
            if !fromSearch {
                self.fullModels = models
            }
            models.append(model)
            chatModels.append(model)

            // Групповые чаты
/*
            if AppDelegate.isMvp2 {
                let modelGroup = GroupChatListCellViewModel(chatModel: chat, userId: chatParticipant.userId)
                bindGroupChatVM(modelGroup)
                if chat.participants.count > 2 {
                    models.append(modelGroup)
                    groupChatModels.append(modelGroup)
                } else {
                    models.append(model)
                    chatModels.append(model)
                }
            } else {
                models.append(model)
                chatModels.append(model)
            }
*/
        }
        items.accept(models)
    }

    private func deleteItem(userId: Int) {
        fullModels.enumerated().forEach { index, model in
            if AppDelegate.isMvp2 {
                if let chatModel = model as? GroupChatListCellViewModel, chatModel.userId == userId {
                    fullModels.remove(at: index)
                } else if let chatModel = model as? ChatListCellViewModel, chatModel.userId == userId {
                    fullModels.remove(at: index)
                }
            } else {
                guard let chatModel = model as? ChatListCellViewModel, chatModel.userId == userId else { return }
                fullModels.remove(at: index)
            }
        }
        items.accept(fullModels)
    }

    private func bindChatVM(_ model: ChatListCellViewModel) {
        model
            .onDeleteTap
            .bind { [weak self, weak model] userId in
                guard let self = self, let model = model else { return }
                let chatForDelete = SocketChatServiceFactory.make(roomId: String(model.chatModel.chatId))
                chatForDelete.isConnected
                    .subscribe(onNext: { _ in
                        chatForDelete.removeChat(userId: UserDefaultsHelper.shared.userId, chatId: model.chatModel.chatId)
                    })
                    .disposed(by: self.chatsBag)
                guard let userId = userId else { return }
                self.deleteItem(userId: userId)
            }
            .disposed(by: model.bag)

        userStatus
            .subscribe(onNext: { [weak model] userStatus in
                guard let model = model, userStatus.user == model.userId else { return }
                model.onChangeStatus.onNext(userStatus.isOnline)
            })
            .disposed(by: chatsBag)
    }

    private func bindGroupChatVM(_ model: GroupChatListCellViewModel) {
        model
            .onDeleteTap
            .bind { [weak self, weak model] userId in
                guard let self = self, let model = model else { return }
                let chatForDelete = SocketChatServiceFactory.make(roomId: String(model.chatModel.chatId))
                chatForDelete.isConnected
                    .subscribe(onNext: { _ in
                        chatForDelete.removeChat(userId: UserDefaultsHelper.shared.userId, chatId: model.chatModel.chatId)
                    })
                    .disposed(by: self.chatsBag)
                guard let userId = userId else { return }
                self.deleteItem(userId: userId)
            }
            .disposed(by: model.bag)
        userStatus
            .subscribe(onNext: { [weak model] userStatus in
                guard let model = model, userStatus.user == model.userId else { return }
                model.onChangeStatus.onNext(userStatus.isOnline)
            })
            .disposed(by: chatsBag)
    }

    private func disconnectChat() {
        allChatsService.disconnect()
    }

    private func reconnectToChat() {
        let chatService = SocketAllChatsServiceFactory.make()
        self.allChatsService = chatService
        followChatsManager(allChatsService: chatService)
    }

    private func bindAppState() {
        UIApplication.shared.rx
            .applicationWillEnterForeground
            .bind { [weak self] _ in
                self?.appInBackground = false
                self?.reconnectToChat()
            }
            .disposed(by: chatsBag)

        UIApplication.shared.rx
            .applicationDidEnterBackground
            .bind { [weak self] _ in
                self?.appInBackground = true
                self?.disconnectChat()
            }
            .disposed(by: chatsBag)
    }
}
