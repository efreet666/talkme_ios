//
//  ChatContactCellModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 30.03.2021.
//

import Foundation

struct ChatContactCellModel: Decodable {
    let id: Int
    let firstName: String
    let lastName: String
    let avatarUrl: String?
    var lastSeen: Double
}
