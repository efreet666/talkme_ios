//
//  NewChatModel.swift
//  talkme_ios
//
//  Created by 1111 on 12.07.2021.
//

import Foundation

struct NewChatRequest: Codable {
    let createChats: [NewChatUserModel]
}

struct NewChatUserModel: Codable {
    let content: String
    let toUser: Int
}
