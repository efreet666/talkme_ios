//
//  ChatViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 29.04.2021.
//

import RxSwift
import RxRelay

final class ChatViewModel {

    // MARK: - Public properties

    let bag = DisposeBag()
    let newMessage = PublishSubject<ChatMessage>()
    var messagesPresented = false

    // MARK: - Private properties

    private var chatService: SocketChatService?
    private var chatId: Int
    private let userId = UserDefaultsHelper.shared.userId
    private var messagesCount = 0
    private var appInBackground = false

    // MARK: - Initializers

    init(chatId: Int, service: SocketChatService) {
        chatService = service
        self.chatId = chatId
        followChatsManager(chatService: service)
        bindAppState()
    }

    // MARK: - Public methods

    func fetchMessages() {
        guard let chatService = self.chatService else { return }
        chatService.fetchMessages(chatId: chatId, userId: userId)

    }

    func fetchMoreMessages() {
        guard let chatService = self.chatService else { return }
        chatService.moreMessages(chatId: chatId, userId: userId, length: messagesCount + 15)
        messagesCount += 15

    }

    func newMessage(message: String, image: [String?]) {
        guard let chatService = self.chatService else { return }
        chatService.newMessage(chatId: chatId, userId: userId, message: message, image: image)
    }

    func readNewMessages() {
        guard let chatService = self.chatService else { return }
        chatService.readNewMessages(chatId: chatId, userId: userId)
    }

    // MARK: - Private methods

    private func followChatsManager(chatService: SocketChatService) {
        chatService
            .messages
            .subscribe(onNext: { [weak self] messages in
                messages.messages.forEach { message in
                    guard let images = message.image else {
                        guard !message.content.isEmpty else { return }
                        self?.newMessage.onNext(ChatMessage(message: message, isNew: false))
                        return
                    }
                    guard !message.content.isEmpty || !images.isEmpty else { return }
                    self?.newMessage.onNext(ChatMessage(message: message, isNew: false))

                }
                self?.fetchMoreMessages()
            })
            .disposed(by: bag)

        chatService
            .moreMessages
            .subscribe(onNext: { [weak self] moreMessages in
                moreMessages.messages.forEach { message in
                    guard let images = message.image else {
                        guard !message.content.isEmpty else { return }
                        self?.newMessage.onNext(ChatMessage(message: message, isNew: false))
                        return
                    }
                    guard !message.content.isEmpty || !images.isEmpty else { return }
                    self?.newMessage.onNext(ChatMessage(message: message, isNew: false))
                }
            })
            .disposed(by: bag)

        chatService
            .newMessage
            .subscribe(onNext: { [weak self] newMessage in
                guard let images = newMessage.message.image else {
                    guard !newMessage.message.content.isEmpty else { return }
                    self?.newMessage.onNext(ChatMessage(message: newMessage.message, isNew: true))
                    self?.messagesCount += 1
                    return
                }
                guard !newMessage.message.content.isEmpty || !images.isEmpty else { return }
                self?.newMessage.onNext(ChatMessage(message: newMessage.message, isNew: true))
                self?.messagesCount += 1
            })
            .disposed(by: bag)

        chatService
            .isConnected
            .subscribe(onNext: { [weak self] connection in
                guard let self = self else { return }
                switch connection {
                case .connected:
                    self.fetchMessages()
                    self.readNewMessages()
                case .disconnected, .cancelled, .error:
                    guard !self.appInBackground else { return }
                    self.reconnectToChat()
                }
            })
            .disposed(by: bag)
    }

    private func reconnectToChat() {
        let chatService = SocketChatServiceFactory.make(roomId: String(chatId))
        self.chatService = chatService
        followChatsManager(chatService: chatService)
    }

    private func disconnectChat() {
        chatService = nil
    }

    private func bindAppState() {
        UIApplication.shared.rx
            .applicationWillEnterForeground
            .bind { [weak self] _ in
                self?.appInBackground = false
                self?.reconnectToChat()
            }
            .disposed(by: bag)

        UIApplication.shared.rx
            .applicationDidEnterBackground
            .bind { [weak self] _ in
                self?.appInBackground = true
                self?.disconnectChat()
            }
            .disposed(by: bag)
    }
}
