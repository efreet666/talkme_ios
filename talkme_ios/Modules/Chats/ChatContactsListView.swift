//
//  ChatContactsListView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 30.03.2021.
//

import RxSwift
import RxCocoa
import Foundation

final class ChatContactsListView: UIView, DrawerContentView {

    // MARK: - Public properties

    let viewHeight = BehaviorRelay<CGFloat>(value: UIScreen.width > 376 ? UIScreen.height - 315 : UIScreen.height - 220)
    var onDismiss: (() -> Void)?

    // MARK: - Private Properties

    private let createChatLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = TalkmeColors.blackLabels
        label.text = "chats_create_new_chat".localized
        return label
    }()

    private let dismissButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "dismissButton")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentEdgeInsets = UIScreen.isSE
            ? .init(top: 15.5, left: 15.5, bottom: 15.5, right: 15.5)
            : .init(top: 13.5, left: 13.5, bottom: 13.5, right: 13.5)
        button.tintColor = TalkmeColors.blackLabels
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()

    private let searchField: StreamSearchBar = {
        let searchField = StreamSearchBar()
        searchField.searchTextField.placeholder = "chats_search_user_for_create_chat".localized
        searchField.backgroundColor = TalkmeColors.searchbarNewChatUser
        searchField.searchTextField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        searchField.layer.cornerRadius = UIScreen.isSE ? 17 : 21
        return searchField
    }()

    private let contactsTable: UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.rowHeight = UITableView.automaticDimension
        table.backgroundColor = .clear
        table.registerCells(withModels: ChatContactCellViewModel.self)
        return table
    }()

    private let createChatButton = RoundedColoredButton(.createChat)
    private let viewModel: ChatContactsListViewModel
    private var searchUser = ""
    private let bag = DisposeBag()
    var idForChats = [Int]()

    // MARK: - Initializers

    init(viewModel: ChatContactsListViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setUpLayout()
        if AppDelegate.isMvp2 {
            contactsTable.allowsMultipleSelection = true
        } else {
            contactsTable.allowsMultipleSelection = false
        }
        setUpAppearance()
        bindVM()
        bindUI()
        setupBehavior()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func updateLayout(height: CGFloat) {}

    override func didMoveToSuperview() {
        viewModel.getItems(searchText: "")
    }

    // MARK: - Private Methods

    private func setUpLayout() {
        addSubviews(
            createChatLabel,
            dismissButton,
            searchField,
            contactsTable,
            createChatButton
        )

        createChatLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 8 : 10)
            make.trailing.greaterThanOrEqualTo(dismissButton.snp.leading)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 22 : 24)
            make.height.equalTo(UIScreen.isSE ? 19 : 25)
        }

        dismissButton.snp.makeConstraints { make in
            make.size.equalTo(44)
            make.centerX.equalTo(snp.trailing).inset(UIScreen.isSE ? 16.5 : 18.5)
            make.centerY.equalTo(snp.top).inset(UIScreen.isSE ? 31.5 : 36.5)
        }

        searchField.snp.makeConstraints { make in
            make.top.equalTo(createChatLabel.snp.bottom).offset(UIScreen.isSE ? 19 : 22)
            make.height.equalTo(UIScreen.isSE ? 34 : 42)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        contactsTable.snp.makeConstraints { make in
            make.top.equalTo(searchField.snp.bottom).offset(UIScreen.isSE ? 19 : 18)
            make.leading.trailing.equalToSuperview()
        }

        createChatButton.snp.makeConstraints { make in
            make.top.equalTo(contactsTable.snp.bottom).offset(UIScreen.isSE ? 7 : 0)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 42 : 54)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 47 : 60)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
            make.bottom.equalToSuperview().inset((UIScreen.isSE ? 27 : 32))
        }
    }

    private func setUpAppearance() {
        backgroundColor = .white
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        layer.cornerRadius = 20
        clipsToBounds = true
    }

    private func bindVM() {
        viewModel
            .items
            .bind(to: contactsTable.rx.items) { tableView, row, model in
                let index = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: model, for: index)
                model.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        createChatButton
            .rx
            .tap
            .bind(to: viewModel.onCreateChatTap)
            .disposed(by: bag)

        searchField
            .searchTextField
            .rx
            .text
            .compactMap { $0 }
            .debounce(.milliseconds(300), scheduler: ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .bind { [weak viewModel] string in
                guard let viewModel = viewModel else { return }
                self.searchUser = string
                viewModel.getItems(searchText: string)
            }
            .disposed(by: bag)
    }

    private func bindUI() {
        dismissButton
            .rx
            .tap
            .bind { [weak self] in
                self?.onDismiss?()
            }
            .disposed(by: bag)

        createChatButton
            .rx
            .tap
            .bind { [weak self] in
                self?.onDismiss?()
            }
            .disposed(by: bag)

        contactsTable
            .rx
            .modelSelected(ChatContactCellViewModel.self)
            .bind { [weak self] model in
                guard let self = self else { return }
                if AppDelegate.isMvp2 {
                    if !self.viewModel.idForChats.contains(model.cellModel.id) {
                        self.viewModel.idForChats.append(model.cellModel.id)
                    } else {
                        if let index = self.viewModel.idForChats.firstIndex(of: model.cellModel.id) {
                            self.viewModel.idForChats.remove(at: index)
                        }
                    }
                } else {
                    if self.viewModel.idForChats.count < 1 {
                        self.viewModel.idForChats.append(model.cellModel.id)
                    } else if self.viewModel.idForChats.count == 1 {
                        if self.viewModel.idForChats.contains(model.cellModel.id) {
                            self.viewModel.idForChats.removeAll()
                        } else {
                            self.viewModel.idForChats.removeAll()
                            self.viewModel.idForChats.append(model.cellModel.id)
                        }
                    }
                }
                self.viewModel.getItems(searchText: self.searchUser)
            }
            .disposed(by: bag)

        contactsTable
            .rx
            .modelDeselected(ChatContactCellViewModel.self)
            .bind { [weak self] model in
                guard let self = self else { return }
                if AppDelegate.isMvp2 {
                    if let index = self.viewModel.idForChats.firstIndex(of: model.cellModel.id) {
                        self.viewModel.idForChats.remove(at: index)
                    }
                } else {
                    self.viewModel.idForChats.removeAll()
                }
                self.viewModel.getItems(searchText: self.searchUser)
            }
            .disposed(by: bag)
    }

    private func setupBehavior() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        recognizer.cancelsTouchesInView = false

        self.addGestureRecognizer(recognizer)
    }

    @objc func hideKeyboard() {
        searchField.endEditing(true)
    }
}
