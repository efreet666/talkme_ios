//
//  GroupChatListCell.swift
//  talkme_ios
//
//  Created by Vladislav on 22.12.2021.
//

import Foundation
import RxSwift
import RxCocoa
import Kingfisher

final class GroupChatListCell: UITableViewCell {

    enum Constants {
        static let cornerRadius: CGFloat = 12
        static let leadingTrailingInset: CGFloat = 10
        static let bottomInset: CGFloat = 5
        static let containerHeight: CGFloat = UIScreen.isSE ? 79 : 104
        static let deleteWidth: CGFloat = UIScreen.isSE ? 64 : 92
    }

// MARK: - Public properties

    let onDeleteTap = PublishRelay<Void>()
    private(set) var bag = DisposeBag()

   // MARK: - Private Properties

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.layer.cornerRadius = Constants.cornerRadius
        return view
    }()

    private let avatarView: RoundedImageView = {
        let view = RoundedImageView()
        view.contentMode = .scaleAspectFill
        return view
    }()

    private let secondAvatarView: RoundedImageView = {
        let view = RoundedImageView()
        view.contentMode = .scaleAspectFill
        return view
    }()

    private let statusIndicator: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.greenLabels
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = UIScreen.isSE ? 2.5 : 3
        return view
    }()

    private let newMessageIndicator: RoundedLabel = {
        let label = UIScreen.isSE
        ? RoundedLabel(withInsets: 0, 0, 6, 6)
        : RoundedLabel(withInsets: 0, 0, 9, 9)
        label.backgroundColor = TalkmeColors.greenLabels
        label.textColor = .white
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textAlignment = .center
        return label
    }()

    private let fullName: UILabel = {
        let label = UILabel()
        let attributedString = NSMutableAttributedString(string: label.text ?? " ")
        let paragraphStyle = NSMutableParagraphStyle()
        attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        label.attributedText = attributedString
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 17)
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let messageLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 11 : 15)
        label.textColor = TalkmeColors.grayAttributeTitle
        return label
    }()

    private let deleteImage = DeleteCellActionView()
    private var containerLastPositionX: CGFloat = 0
    private var deleteLastPositionX: CGFloat = 0
    private var deleteLastWidth: CGFloat = 0
    private let recognizerBag = DisposeBag()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpLayout()
        cellStyle()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

   // MARK: - Public Methods

    func configure(_ model: Chat) {
        let chatNames = model.participants
        var nameGroupChat = [String]()
        for participants in chatNames {
            nameGroupChat.append(participants.name != nil ? "\(participants.name!)": "")
        }

        let name = nameGroupChat.joined(separator: ", ")
        let chatMember = model.participants.first { participant in
            participant.userId != UserDefaultsHelper.shared.userId
        }
        guard let chatMember = chatMember else { return }
        fullName.text = name
        if let imageURL = chatMember.avatarUrl, !imageURL.isEmpty {
            avatarView.kf.setImage(with: URL(string: imageURL))
        } else {
            avatarView.image = UIImage(named: "noAvatar")
        }

        let chatMemberLast = model.participants.last { participant in
            participant.userId != UserDefaultsHelper.shared.userId
        }
        guard let chatMemberLast = chatMemberLast else { return }
        if let imageUrl = chatMemberLast.avatarUrl, !imageUrl.isEmpty {
            secondAvatarView.kf.setImage(with: URL(string: imageUrl))
        } else {
            secondAvatarView.image = UIImage(named: "noAvatar")
        }
        messageLabel.text = model.lastMessage
        statusIndicator.isHidden = !(chatMember.online ?? false)
        newMessageIndicator.isHidden = model.new == 0

        newMessageIndicator.text = model.new > 999 ? "999+": "+\(model.new)"
        setInitialAppearance()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        avatarView.image = nil
        bag = .init()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        containerView.backgroundColor = selected ? TalkmeColors.selectedChatLightBlue : .white
    }

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let recognizer = gestureRecognizer as? UIPanGestureRecognizer else { return false }
        let isTopRightPan = recognizer.translation(in: self).x > -recognizer.translation(in: self).y
        let isBotRightPan = recognizer.translation(in: self).x > recognizer.translation(in: self).y
        let isTopLeftPan = -recognizer.translation(in: self).x > -recognizer.translation(in: self).y
        let isBotLeftPan = -recognizer.translation(in: self).x > recognizer.translation(in: self).y
        let isRightPan = isTopRightPan && isBotRightPan
        let isLeftPan = isTopLeftPan && isBotLeftPan
        return isLeftPan || isRightPan
    }

    func setStatus(_ isOnline: Bool) {
        statusIndicator.isHidden = !isOnline
    }

   // MARK: - Private Methods

    private func bindUI() {
        rx
            .panGesture { [weak self] recognizer, _ in
                recognizer.delegate = self
            }
            .when(.changed)
            .bind { [weak self] recognizer in
                guard let self = self, let superview = self.containerView.superview else { return }

                let isLeftFromCenter = (self.containerLastPositionX + recognizer.translation(in: self).x) < superview.center.x
                if isLeftFromCenter {
                    self.containerView.center.x = self.containerLastPositionX + recognizer.translation(in: self).x
                    self.deleteImage.frame.size.width = self.deleteLastWidth - recognizer.translation(in: self).x
                    self.deleteImage.center.x = self.deleteLastPositionX + recognizer.translation(in: self).x / 2
                    self.deleteImage.updateFrames(
                        CGSize(
                            width: self.deleteImage.frame.size.width + Constants.cornerRadius,
                            height: self.deleteImage.frame.size.height
                        )
                    )
                    let alphaChangingSpeedModificator: CGFloat = 5
                    let dynamicAlpha = (self.deleteImage.frame.width - Constants.cornerRadius - Constants.leadingTrailingInset)
                    / alphaChangingSpeedModificator
                    self.deleteImage.alpha = min(1, dynamicAlpha)
                } else {
                    self.containerView.center.x = superview.center.x
                    self.deleteImage.frame.origin.x = self.containerView.frame.maxX - Constants.cornerRadius
                    self.deleteImage.frame.size.width = Constants.cornerRadius + Constants.leadingTrailingInset
                    self.deleteImage.updateFrames(
                        CGSize(
                            width: self.deleteImage.frame.size.width + Constants.cornerRadius,
                            height: self.deleteImage.frame.size.height
                        )
                    )
                    self.deleteImage.alpha = 0
                }
            }.disposed(by: recognizerBag)

        rx
            .panGesture()
            .when(.began)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.containerLastPositionX = self.containerView.center.x
                self.deleteLastPositionX = self.deleteImage.center.x
                self.deleteLastWidth = self.deleteImage.frame.width
            }.disposed(by: recognizerBag)

        rx
            .panGesture()
            .when(.ended)
            .bind { [weak self] recognizer in
                guard let self = self, let superview = self.containerView.superview else { return }
                guard self.containerLastPositionX == superview.center.x else {
                    self.hideDeleteView()
                    return
                }
                recognizer.translation(in: self.contentView).x < -(Constants.deleteWidth / 2)
                ? self.revealDeleteView()
                : self.hideDeleteView()
            }.disposed(by: recognizerBag)

        deleteImage
            .rx
            .tapGesture()
            .when(.recognized)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.deleteActionAnimation()
            }
            .disposed(by: recognizerBag)
    }

    private func setUpLayout() {
        contentView.addSubviews(deleteImage, containerView)
        containerView.addSubviews(
            avatarView,
            secondAvatarView,
            statusIndicator,
            newMessageIndicator,
            fullName,
            messageLabel
        )
        setInitialAppearance()
        avatarView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 40 : 52)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 9 : 12)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 20 : 27)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 19 : 25)
        }

        secondAvatarView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 40 : 52)
            make.leading.equalTo(avatarView.snp.leading).offset(UIScreen.isSE ? 13 : 17)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 20 : 27)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 19 : 25)
        }

        statusIndicator.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 12 : 15)
            make.bottom.equalTo(avatarView)
            make.trailing.equalTo(avatarView).inset(UIScreen.isSE ? 6 : 8)
        }

        newMessageIndicator.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 9 : 12)
            make.height.equalTo(UIScreen.isSE ? 16 : 21)
        }

        fullName.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 16 : 21)
            make.leading.equalTo(secondAvatarView.snp.trailing).offset(UIScreen.isSE ? 10 : 13)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 21 : 28)
            make.width.equalTo(UIScreen.isSE ? 170 : 218)
        }

        messageLabel.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 14 : 18)
            make.leading.equalTo(fullName)
            make.top.equalTo(fullName.snp.bottom).offset(UIScreen.isSE ? 7 : 9)
            make.trailing.lessThanOrEqualToSuperview().inset(UIScreen.isSE ? 43 : 56)
        }

    }

    private func cellStyle() {
        selectionStyle = .none
        backgroundColor = .clear
    }

    private func setInitialAppearance() {
        containerView.frame = CGRect(
            x: Constants.leadingTrailingInset,
            y: 0,
            width: contentView.frame.width - Constants.leadingTrailingInset * 2,
            height: Constants.containerHeight
        )

        deleteImage.frame = CGRect(
            x: contentView.frame.width - Constants.cornerRadius - Constants.leadingTrailingInset,
            y: 0,
            width: Constants.cornerRadius + Constants.leadingTrailingInset,
            height: containerView.frame.height
        )

        deleteImage.alpha = 0
    }

    private func revealDeleteView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            guard let self = self, let superview = self.containerView.superview else { return }

            self.containerView.center.x = superview.center.x - Constants.deleteWidth + Constants.leadingTrailingInset
            self.deleteImage.frame.size.width = Constants.deleteWidth + Constants.cornerRadius
            self.deleteImage.frame.origin.x = self.containerView.frame.maxX - Constants.cornerRadius
            self.deleteImage.updateFrames(
                CGSize(
                    width: self.deleteImage.frame.size.width + Constants.cornerRadius,
                    height: self.deleteImage.frame.size.height
                )
            )
            self.deleteImage.alpha = 1
        })
    }

    private func hideDeleteView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            guard let self = self, let superview = self.containerView.superview else { return }
            self.containerView.center.x = superview.center.x
            self.deleteImage.frame.size.width = Constants.cornerRadius + Constants.leadingTrailingInset
            self.deleteImage.frame.origin.x = self.containerView.frame.maxX - Constants.cornerRadius
            self.deleteImage.updateFrames(
                CGSize(
                    width: self.deleteImage.frame.size.width + Constants.cornerRadius,
                    height: self.deleteImage.frame.size.height
                )
            )
            self.deleteImage.alpha = 0
        })
    }

    private func deleteActionAnimation() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            guard let self = self else { return }

            self.containerView.frame.origin.x = -self.containerView.frame.width
            self.deleteImage.frame.size.width = self.contentView.frame.width
            self.deleteImage.frame.origin.x = 0
        }, completion: { [weak self] isSuccess in
            guard isSuccess else { return }
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
                guard let self = self else { return }
                self.containerView.frame.origin.y = 0
                self.containerView.frame.size.height = 0
                self.deleteImage.frame.size.height = 0
                self.deleteImage.frame.origin.y = 0
            })
            self?.onDeleteTap.accept(())
        })
    }
}
