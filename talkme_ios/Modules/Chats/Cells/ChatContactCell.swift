//
//  ChatContactCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 30.03.2021.
//

import RxSwift
import RxCocoa
import Kingfisher

final class ChatContactCell: UITableViewCell {

    // MARK: - Private Properties

    private let avatarView = RoundedImageView()

    private let statusIndicator: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.greenLabels
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = UIScreen.isSE ? 2 : 2.5
        return view
    }()

    private let fullName: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let selectIndicator: RoundedIndicatorView = {
        let indicator = RoundedIndicatorView()
        return indicator
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpLayout()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ model: ChatContactCellModel, id: [Int]) {
        let urlString = model.avatarUrl ?? ""
        if let url = URL(string: urlString) {
            avatarView.kf.setImage(
                with: ImageResource(
                    downloadURL: url,
                    cacheKey: url.absoluteString
                ),
                placeholder: UIImage(named: "noAvatar")
            )
        } else {
            avatarView.image = UIImage(named: "noAvatar")
        }
        fullName.text = "\(model.firstName) \(model.lastName)"
        statusIndicator.isHidden = true
        selectedCell(model, id: id)
    }

    // MARK: - Private Methods

    private func setUpLayout() {
        contentView.addSubviews(
            avatarView,
            statusIndicator,
            fullName,
            selectIndicator
        )

        avatarView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 40 : 52)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 13 : 17)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 8 : 10)
        }

        statusIndicator.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 9 : 12)
            make.bottom.equalTo(avatarView)
            make.trailing.equalTo(avatarView).inset(UIScreen.isSE ? 5 : 6)
        }

        fullName.snp.makeConstraints { make in
            make.centerY.equalTo(avatarView)
            make.leading.equalTo(avatarView.snp.trailing).offset(UIScreen.isSE ? 8 : 10)
            make.trailing.greaterThanOrEqualTo(selectIndicator.snp.leading).offset(20)
        }

        selectIndicator.snp.makeConstraints { make in
            make.centerY.equalTo(avatarView)
            make.size.equalTo(UIScreen.isSE ? 16 : 21)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 13 : 17)
        }
    }

    private func cellStyle() {
        selectionStyle = .none
        backgroundColor = .clear
    }

    private func selectedCell(_ model: ChatContactCellModel, id: [Int]) {
        if id.contains(model.id) {
            selectIndicator.setOn(true)
        } else {
            selectIndicator.setOn(false)
        }
    }
}
