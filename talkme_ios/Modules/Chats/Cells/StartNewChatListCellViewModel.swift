//
//  StartNewChatListCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 01.04.2021.
//

import Foundation
import UIKit

final class StartNewChatListCellViewModel: TableViewCellModelProtocol {
    func configure(_ cell: StartNewChatListCell) {}
}
