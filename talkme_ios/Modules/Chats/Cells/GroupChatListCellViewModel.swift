//
//  GroupChatListCellViewModel.swift
//  talkme_ios
//
//  Created by Vladislav on 22.12.2021.
//

import RxSwift
import RxRelay
import RxDataSources

final class GroupChatListCellViewModel: TableViewCellModelProtocol {

   // MARK: - Public properties

    let onDeleteTap = PublishRelay<Int?>()
    let chatModel: Chat
    let bag = DisposeBag()
    let onChangeStatus = PublishSubject<Bool>()
    let userId: Int

    // MARK: - Initializers

    init(chatModel: Chat, userId: Int) {
        self.chatModel = chatModel
        self.userId = userId
    }

    // MARK: - Public Methods

    func configure(_ cell: GroupChatListCell) {
        cell.configure(chatModel)
        cell
            .onDeleteTap
            .map { [weak self] _ in
                return self?.userId }
            .bind(to: onDeleteTap)
            .disposed(by: cell.bag)

        onChangeStatus
            .subscribe(onNext: { [weak cell] isOnline in
                cell?.setStatus(isOnline)
            })
            .disposed(by: cell.bag)
    }
}
