//
//  MessageImagesCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 17.06.2021.
//

import RxCocoa
import RxSwift

final class MessageImagesCollectionCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let imageString: String?
    let imageTapped = PublishRelay<UIImage?>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(imageString: String?) {
        self.imageString = imageString
    }

    // MARK: - Public Methods

    func configure(_ cell: MessageImagesCollectionCell) {
        cell.configure(image: imageString)

        cell
            .imageTapped
            .bind(to: imageTapped)
            .disposed(by: cell.bag)
    }
}
