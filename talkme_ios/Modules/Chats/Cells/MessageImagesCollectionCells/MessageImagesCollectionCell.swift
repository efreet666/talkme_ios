//
//  MessageImagesCollectionCell.swift
//  talkme_ios
//
//  Created by 1111 on 17.06.2021.
//

import RxSwift
import RxCocoa

final class MessageImagesCollectionCell: UICollectionViewCell {

    // MARK: - Public properties

    var bag = DisposeBag()
    let imageTapped = PublishRelay<UIImage?>()

    // MARK: - Private properties

    private let messageImageView: UIImageView = {
        let iw = UIImageView()
        iw.layer.cornerRadius = 6
        iw.contentMode = .scaleAspectFill
        iw.clipsToBounds = true
        return iw
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(image: String?) {
        guard let image = image else { return }
        guard let dataDecoded: Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters) else {
            let correctImageUrl = image
            messageImageView.kf.setImage(with: URL(string: correctImageUrl)) // image string as url from frontend
            return
        }
        let decodedImage = UIImage(data: dataDecoded)
        messageImageView.image = decodedImage

        messageImageView.rx.tapGesture().when(.recognized)
            .map { [weak self] _ in
                return self?.messageImageView.image
            }
            .bind(to: imageTapped)
            .disposed(by: bag)
    }

    // MARK: - Private Method

    override func prepareForReuse() {
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubview(messageImageView)
        messageImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
