//
//  StartNewChatListCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 23.03.2021.
//

import UIKit

final class StartNewChatListCell: UITableViewCell {

    enum Constants {
        static let contentHeight: CGFloat = UIScreen.isSE ? 45 : 55
        static let bottomInset: CGFloat = UIScreen.isSE ? 13 : 19
    }

    // MARK: - Private Properties

    private let border: DashBorderedView = {
        let view = DashBorderedView()
        view.borderColor = TalkmeColors.dashBorderGray
        view.dashWidth = 1
        view.dashLength = 5
        view.spacing = 5
        view.cornerRadius = 12
        return view
    }()

    private let plusIcon: UIImageView = {
        let imageView = UIImageView(
            image: UIImage(named: "plusButton")?
                .withRenderingMode(.alwaysTemplate)
        )
        imageView.tintColor = TalkmeColors.greenLabels
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let newMessageLabel: UILabel = {
        let label = UILabel()
        label.text = "chats_new_message".localized
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.greenLabels
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpLayout()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setUpLayout() {
        contentView.addSubview(border)
        border.addSubviews(plusIcon, newMessageLabel)

        border.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(1)
            make.centerX.equalToSuperview()
            make.height.equalTo(Constants.contentHeight)
            make.bottom.equalToSuperview().inset(Constants.bottomInset - 1)
        }

        plusIcon.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.size.equalTo(18)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 27 : 40)
        }

        newMessageLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 31 : 44)
            make.leading.equalTo(plusIcon.snp.trailing).offset(14)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
