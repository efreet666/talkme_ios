//
//  ChatContactCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 30.03.2021.
//

import Foundation

final class ChatContactCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let cellModel: ChatContactCellModel
    let viewModel: ChatContactsListViewModel

    // MARK: - Initializers

    init(cellModel: ChatContactCellModel, viewModel: ChatContactsListViewModel) {
        self.viewModel = viewModel
        self.cellModel = cellModel
    }

    // MARK: - Public Methods

    func configure(_ cell: ChatContactCell) {
        cell.configure(cellModel, id: viewModel.idForChats)
    }
}
