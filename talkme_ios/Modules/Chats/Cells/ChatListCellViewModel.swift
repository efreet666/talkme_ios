//
//  ChatListCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 23.03.2021.
//

import RxSwift
import RxRelay
import RxDataSources

final class ChatListCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let onDeleteTap = PublishRelay<Int?>()
    let chatModel: Chat
    let bag = DisposeBag()
    let onChangeStatus = PublishSubject<Bool>()
    let userId: Int

    // MARK: - Initializers

    init(chatModel: Chat, userId: Int) {
        self.chatModel = chatModel
        self.userId = userId
    }

    // MARK: - Public Methods

    func configure(_ cell: ChatListCell) {
        cell.configure(chatModel)
        cell
            .onDeleteTap
            .map { [weak self] _ in
                return self?.userId }
            .bind(to: onDeleteTap)
            .disposed(by: cell.bag)

        onChangeStatus
            .subscribe(onNext: { [weak cell] isOnline in
                cell?.setStatus(isOnline)
            })
            .disposed(by: cell.bag)
    }
}
