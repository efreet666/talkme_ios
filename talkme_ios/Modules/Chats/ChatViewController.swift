//
//  ChatViewController.swift
//  talkme_ios
//
//  Created by 1111 on 29.04.2021.
//

import RxSwift
import RxCocoa
import BSImagePicker
import Photos

private enum Constants {
    static let sendMessageViewHeight: CGFloat = UIScreen.isSE ? 39 : 43
    static let tabBarHeight: CGFloat = 108
    static let navBarHeight: CGFloat = UIScreen.isSE ? 53 : 67
}

final class ChatViewController: UIViewController {

    // MARK: - Private Properties

    private let viewModel: ChatViewModel
    private let bag = DisposeBag()

    private var sendMessageStartFrame: CGRect {
        CGRect(
            x: 0,
            y: view.bounds.height - Constants.tabBarHeight - Constants.sendMessageViewHeight,
            width: UIScreen.width,
            height: Constants.sendMessageViewHeight
        )
    }

    private var chatViewFrame: CGRect {
        CGRect(
            x: 0,
            y: Constants.navBarHeight + UIApplication.topInset,
            width: UIScreen.width,
            height: sendMessageView.frame.minY - Constants.navBarHeight - UIApplication.topInset
        )
    }

    private let imagePicker: ImagePickerController = {
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = 6
        return imagePicker
    }()

    private lazy var chatView = ChatView(frame: chatViewFrame)
    private lazy var sendMessageView = SendMessageView(frame: sendMessageStartFrame)
    private var newSendMessageOrigin: CGFloat?
    private var keyboardIsVisible = false
    private var keyBoardHeight: CGFloat?
    private var heightdifferencre: CGFloat = 0

    // MARK: - Initializers

    init(viewModel: ChatViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cicle

    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
        setupUI()
        bindKeyboard()
        bindUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        hideCustomTabBarLiveButton(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
    }

    // MARK: - Private Methods

    private func setupUI() {
        view.backgroundColor = TalkmeColors.mainAccountBackground
        view.addSubviews([sendMessageView, chatView])
    }

    private func bindKeyboard() {
        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] height in
            guard let self = self else { return }
            let isHidden = height == 0
            self.keyboardIsVisible = !isHidden
            let sendMessageStartFrame = self.sendMessageStartFrame
            let sendMessageOriginY = isHidden
                ? self.sendMessageStartFrame.origin.y - self.heightdifferencre
                : UIScreen.main.bounds.height - height - sendMessageStartFrame.height - self.heightdifferencre - 5
            UIView.animate(withDuration: 0.3) {
                self.sendMessageView.frame.origin.y = sendMessageOriginY
                self.chatView.frame.size.height = sendMessageOriginY - Constants.navBarHeight - UIApplication.topInset
                self.chatView.updateFrame()
            }
            guard !isHidden else { return }
            self.keyBoardHeight = height
        }).disposed(by: bag)
    }

    private func bindUI() {
        sendMessageView
            .updatedSendMessageOriginY
            .bind { [weak self] difference in
                guard let self = self else { return }
                self.heightdifferencre += difference
                self.chatView.frame.size.height -= difference
                self.chatView.updateFrame()
            }
            .disposed(by: bag)

        view
            .rx
            .tapGesture()
            .when(.recognized)
            .filter { [weak self] recognizer in
                guard let self = self else { return false }
                let point = recognizer.location(in: self.sendMessageView)
                return point.y < 0 && self.keyboardIsVisible
            }
            .bind { [weak self] _ in
                self?.view.endEditing(true)
            }
            .disposed(by: bag)

        chatView
            .onFullScreenImage
            .bind { [weak self] image in
                self?.showFullScreenImage(image)
            }
            .disposed(by: bag)
    }

    private func bindVM() {
        sendMessageView
            .sendMessage
            .bind { [weak self] text in
                guard let text =  text?.trimmingCharacters(in: .whitespacesAndNewlines), !text.isEmpty else { return }
                self?.viewModel.newMessage(message: text, image: [])
            }
            .disposed(by: bag)

        viewModel
            .newMessage
            .subscribe(onNext: { [weak self] message in
                self?.chatView.addMessage(model: message)
                self?.viewModel.messagesPresented = true
            }, onDisposed: { [weak self] in
                self?.chatView.resetMessageDataSourse()
            }).disposed(by: bag)

        sendMessageView
            .showImagePicker
            .bind { [weak self] _ in
                self?.showImagePicker()
            }
            .disposed(by: bag)

        chatView
            .onFetchMoreMessages
            .bind { [weak self] _ in
                self?.viewModel.fetchMoreMessages()
            }
            .disposed(by: chatView.bag)
    }

    private func showImagePicker() {
        presentImagePicker(imagePicker, animated: true, select: nil, deselect: nil, cancel: nil, finish: { [weak self] assets in
            guard let self = self else { return }
            self.sendImagesMessage(assets)
        })
    }

    private func sendImagesMessage(_ assets: [PHAsset]) {
        var imageStrings = [String]()
        assets.enumerated().forEach { index, asset in
            let image = getAssetImage(asset: asset)
            if let imageData = image?.jpeg(.medium) {
                let imgString = imageData.base64EncodedString()
                imageStrings.append(imgString)
                imagePicker.deselect(asset: asset)
            }
            guard index == assets.count - 1 else { return }
            self.viewModel.newMessage(message: self.sendMessageView.messageText ?? "", image: imageStrings)
        }
    }

    private func getAssetImage(asset: PHAsset) -> UIImage? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var image: UIImage?
        option.isSynchronous = true
        manager.requestImage(
            for: asset,
            targetSize: CGSize(width: 1000, height: 1000),
            contentMode: .aspectFit,
            options: option,
            resultHandler: { result, _ -> Void in
            image = result
        })
        return image
    }

    private func showFullScreenImage(_ image: UIImage?) {
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        customTabbarVC?.setTabbarHidden(true)
    }

    @objc private func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        customTabbarVC?.setTabbarHidden(false)
        sender.view?.removeFromSuperview()
    }
}
