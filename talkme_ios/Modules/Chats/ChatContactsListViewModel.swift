//
//  ChatContactsListViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 01.04.2021.
//

import RxSwift
import RxCocoa
import UIKit

final class ChatContactsListViewModel {

    enum Flow {
        case createChat(NewChatRequest)
    }

    // MARK: - Public Properties

    let items = BehaviorRelay<[AnyTableViewCellModelProtocol]>(value: [])
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    let onCreateChatTap = PublishRelay<Void>()
    var contactId: Int?
    var idForChats = [Int]()

    // MARK: - Private Properties

    private let service: MainServiceProtocol

    // MARK: - Initialization

    init(service: MainServiceProtocol) {
        self.service = service
        bind()
    }

    func getItems(searchText: String?) {
        service
            .chatsCreate(searchText: searchText)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    let models = response.map { ChatContactCellViewModel(cellModel: $0, viewModel: self) }
                    self.items.accept(models)
                case .error(let error):
                    print("[get users for new chat error \(error)]")
                }
            }
            .disposed(by: bag)
    }

    private func bind() {
        onCreateChatTap
            .bind { [weak self] in
                guard let self = self, !self.idForChats.isEmpty else { return } // Посмотреть что то тут не так
                let newChatModels = self.idForChats.map { id in NewChatUserModel(content: "", toUser: id) }
                self.flow.accept(.createChat(NewChatRequest(createChats: newChatModels)))
                self.idForChats.removeAll()
            }
            .disposed(by: bag)
    }
}
