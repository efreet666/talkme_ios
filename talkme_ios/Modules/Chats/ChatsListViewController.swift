//
//  ChatsListViewController.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 23.03.2021.
//

import RxSwift
import RxCocoa
import RxDataSources

final class ChatsListViewController: UIViewController {

    // MARK: - Private Properties

    private let bag = DisposeBag()

    private let searchField: StreamSearchBar = {
        let searchField = StreamSearchBar()
        searchField.searchTextField.placeholder = "talkme_stream_list_search".localized
        searchField.backgroundColor = TalkmeColors.white
        searchField.searchTextField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        searchField.layer.cornerRadius = UIScreen.isSE ? 17 : 21
        return searchField
    }()

    private let chatsTable: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.keyboardDismissMode = .onDrag
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCells(
            withModels: ChatListCellViewModel.self,
            StartNewChatListCellViewModel.self,
            GroupChatListCellViewModel.self
        )
        return tableView
    }()

    private lazy var recognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        recognizer.cancelsTouchesInView = false
        return recognizer
    }()

    private let viewModel: ChatsListViewControllerViewModel

    // MARK: - Initializers

    init(viewModel: ChatsListViewControllerViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = TalkmeColors.mainAccountBackground
        bindVM()
        setUpLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.followListChanges()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigationBar()
        hideCustomTabBarLiveButton(true)
        viewModel.fetchChats()
        viewModel.isNeedToUpdateChatList = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
        viewModel.clearList()
        viewModel.isNeedToUpdateChatList = false
    }

    // MARK: - Private Methods

    private func setUpLayout() {
        view.addSubviews(searchField, chatsTable)
        view.addGestureRecognizer(recognizer)

        searchField.snp.makeConstraints { make in
            make.height.equalTo(34)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(11)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        chatsTable.snp.makeConstraints { make in
            make.top.equalTo(searchField.snp.bottom).offset(10)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }

    private func setUpNavigationBar() {
        customNavigationController?.style = .plainWhiteWithQR(title: "chats_messages_navigation_bar".localized)
    }

    private func bindVM() {
        DispatchQueue.main.async { [self] in

            viewModel
                .items
                .bind(to: chatsTable.rx.items) { tableView, row, data in
                    let indexPath = IndexPath(row: row, section: 0)
                    let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                    data.configureAny(cell)
                    tableView.deselectRow(at: indexPath, animated: true)
                    return cell
                }
                .disposed(by: bag)

            chatsTable
                .rx
                .modelSelected(AnyTableViewCellModelProtocol.self)
                .bind { [weak self] model in
                    guard let self = self else { return }
                    if model as? StartNewChatListCellViewModel != nil {
                        self.viewModel.flow.accept(.createNewChat)
                    } else if let model = model as? GroupChatListCellViewModel {
                        self.viewModel.flow.accept(.toChat(chatId: model.chatModel.chatId))
                    } else if let model = model as? ChatListCellViewModel {
                        self.viewModel.flow.accept(.toChat(chatId: model.chatModel.chatId))
                    }
                }
                .disposed(by: bag)

            chatsTable
                .rx
                .setDelegate(self)
                .disposed(by: bag)

            searchField
                .searchTextField
                .rx
                .text
                .compactMap { $0 }
                .debounce(.milliseconds(300), scheduler: ConcurrentDispatchQueueScheduler(qos: .userInitiated))
                .bind { [weak viewModel] string in
                    guard let viewModel = viewModel else { return }
                    viewModel.filterItems(string: string)
                }
                .disposed(by: bag)
        }
    }

    @objc private func hideKeyboard() {
        searchField.endEditing(true)
    }
}

// MARK: - UITableViewDelegate

extension ChatsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return UIScreen.isSE ? 58 : 74
        }
        return UIScreen.isSE ? 84 : 109
    }
}
