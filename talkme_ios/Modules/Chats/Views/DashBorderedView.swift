//
//  DashBorderedView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 23.03.2021.
//

import UIKit

final class DashBorderedView: UIView {

    // MARK: - Public properties

    var cornerRadius: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }

    var spacing: NSNumber = 0 {
        didSet {
            setNeedsDisplay()
        }
    }

    var dashLength: NSNumber = 6 {
        didSet {
            setNeedsDisplay()
        }
    }

    var dashWidth: CGFloat = 2 {
        didSet {
            setNeedsDisplay()
        }
    }

    var borderColor: UIColor = .black {
        didSet {
            setNeedsDisplay()
        }
    }

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        contentMode = .redraw
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func draw(_ rect: CGRect) {
        let shapeLayer: CAShapeLayer
        if let existingLayer = layer.sublayers?.first(where: { $0.name == String(describing: self) }) as? CAShapeLayer {
            shapeLayer = existingLayer
        } else {
            shapeLayer = CAShapeLayer()
            layer.addSublayer(shapeLayer)
        }

        shapeLayer.name = String(describing: self)
        shapeLayer.frame = rect
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = borderColor.cgColor
        shapeLayer.lineWidth = dashWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [dashLength, spacing]
        shapeLayer.path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).cgPath
    }
}
