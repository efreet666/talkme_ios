//
//  SendMessageView.swift
//  talkme_ios
//
//  Created by 1111 on 02.06.2021.
//

import RxSwift
import RxCocoa
import UIKit

final class SendMessageView: UIView {

    private enum Constants {
        static let sendMessageRightOffset: CGFloat = 10
        static let betweenButtonAndTextViewOffset: CGFloat = 10
        static let textViewHorisontalOffset: CGFloat = 10
        static let sendMessageViewHeight: CGFloat = UIScreen.isSE ? 39 : 43
        static let sendMessageButtonSize = CGSize(width: UIScreen.isSE ? 54 : 64, height: UIScreen.isSE ? 39 : 43)
        static let bottomSendMessageViewOffset: CGFloat = UIScreen.isSE ? 40 : 42
        static let placeHolderAndTextLeftOffset: CGFloat = UIScreen.isSE ? 19 : 25
        static let placeHolderAndTextRightOffset: CGFloat = UIScreen.isSE ? 32 : 40
        static let smilesButtonSize = CGSize(width: 30, height: UIScreen.isSE ? 39 : 43)
    }

    // MARK: - Public Properties

    private(set) lazy var sendMessage = sendMessageButton.rx.tap
        .map { [weak self] _ in self?.textView.text }
        .do(onNext: { [weak self] _ in self?.endEditing() })

    private(set) lazy var showImagePicker = imagesButton.rx.tap

    let bag = DisposeBag()
    let updatedSendMessageOriginY = PublishRelay<CGFloat>()
    var messageText: String?

    // MARK: - Private Properties

    private var sendMessageButtonOrigin: CGPoint {
        CGPoint(
            x: UIScreen.main.bounds.width
                - Constants.sendMessageRightOffset
                - Constants.sendMessageButtonSize.width,
            y: bounds.height
                - Constants.sendMessageViewHeight + 5
        )
    }

    private lazy var sendMessageButton: UIButton = {
        let button = UIButton(frame: CGRect(origin: sendMessageButtonOrigin, size: Constants.sendMessageButtonSize))
        button.setImage(UIImage(named: "sendMessageImage"), for: .normal)
        button.layer.cornerRadius = UIScreen.isSE ? 19.5 : 21.5
        button.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        button.backgroundColor = TalkmeColors.greenSendMessageButton
        button.isEnabled = true
        return button
    }()

    private lazy var textViewHeight: CGFloat = textView.contentSize.height

    private var textViewWidth: CGFloat {
        return sendMessageButton.frame.minX - textViewOrigin.x - Constants.betweenButtonAndTextViewOffset
    }

    private var textViewOrigin: CGPoint {
        CGPoint(
            x: Constants.textViewHorisontalOffset,
            y: bounds.height
                - Constants.sendMessageViewHeight + 5
        )
    }

    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.frame = CGRect(
            origin: textViewOrigin,
            size: CGSize(
                width: textViewWidth,
                height: Constants.sendMessageViewHeight))
        textView.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        textView.backgroundColor = .clear
        textView.contentInset = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: 6,
            right: UIScreen.isSE
                   ? Constants.placeHolderAndTextRightOffset + 7
                   : Constants.placeHolderAndTextRightOffset)
        textView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        textView.delegate = self
        return textView
    }()

    private lazy var textContainer: UIView = {
        let view = UIView(frame: CGRect(origin: textViewOrigin, size: CGSize(width: textViewWidth, height: Constants.sendMessageViewHeight)))
        view.backgroundColor = TalkmeColors.graySearchBar
        view.layer.cornerRadius = UIScreen.isSE ? 19.5 : 21.5
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        return view
    }()

    private lazy var placeholderSize = CGSize(
        width: textViewWidth - Constants.placeHolderAndTextRightOffset - Constants.placeHolderAndTextLeftOffset,
        height: Constants.sendMessageViewHeight)

    private lazy var placeholderLabel: UILabel = {
        let label = UILabel(frame: CGRect(
            origin: textViewOrigin,
            size: placeholderSize
        ))
        label.frame.origin.x += Constants.placeHolderAndTextLeftOffset
        label.backgroundColor = .clear
        label.text = "send_message_placeholder".localized
        label.isUserInteractionEnabled = false
        label.textColor = TalkmeColors.chatsSendMessagePlaceholder
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    private var imagesButtonOrigin: CGPoint {
        CGPoint(
            x: UIScreen.isSE
               ? textViewOrigin.x + textViewWidth - Constants.placeHolderAndTextRightOffset - 5
               : textViewOrigin.x + textViewWidth - Constants.placeHolderAndTextRightOffset,
            y: sendMessageButtonOrigin.y + 1)
    }
    private lazy var imagesButton: UIButton = {
        let button = UIButton(frame: CGRect(origin: imagesButtonOrigin, size: Constants.smilesButtonSize))
        button.setImage(UIImage(named: "sendImageMassage"), for: .normal)
        button.backgroundColor = .clear
        return button
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func endEditing() {
        textView.text = ""
        self.textViewDidChange(self.textView)
        endEditing(true)
    }

    // MARK: - Private Methods

    private func configureUI() {
        backgroundColor = .clear
        textContainer.addSubview(textView)
        addSubviews(textContainer, placeholderLabel, imagesButton, sendMessageButton)
    }

    func startEditing() {
        textView.becomeFirstResponder()
    }
}

extension SendMessageView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        guard let text = textView.text else { return }
        placeholderLabel.isHidden = !text.isEmpty
        let heightDifference = textView.contentSize.height - self.textViewHeight
        guard heightDifference != 0 else { return }
        let newHeight = min(textView.frame.height + heightDifference, 74)

        guard textView.frame.height <= newHeight || heightDifference < 0 else { return }
        textView.frame.size.height += heightDifference
        frame.size.height += heightDifference
        textContainer.frame.size.height += heightDifference
        textViewHeight =  textView.contentSize.height
        frame.origin.y -= heightDifference
        sendMessageButton.frame.origin.y += heightDifference
        imagesButton.frame.origin.y += heightDifference
        updatedSendMessageOriginY.accept(heightDifference)
    }
}
