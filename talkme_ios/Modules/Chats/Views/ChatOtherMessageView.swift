//
//  ChatOtherMessageView.swift
//  talkme_ios
//
//  Created by 1111 on 08.06.2021.
//

import RxSwift
import RxCocoa

final class ChatOtherMessageView: CommonChatMessage {

    private enum Constants {
        static let avatarOrigin = CGPoint(x: UIScreen.isSE ? 11 : 15, y: UIScreen.isSE ? 15 : 19)
        static let avatarSize = CGSize(width: UIScreen.isSE ? 30 : 40, height: UIScreen.isSE ? 30 : 40)
        static let textLeftOffset: CGFloat = UIScreen.isSE ? 11 : 15
        static let titleTopOffset: CGFloat = UIScreen.isSE ? 15 : 19
        static let titleHeight: CGFloat = UIScreen.isSE ? 11 : 16
        static let messageTextTopOffset: CGFloat = 2
        static let messageTextBottomOffset: CGFloat = 12
        static let reservedWidth: CGFloat = avatarOrigin.x + avatarSize.width + 2 * textLeftOffset
        static let collectionImageSize = CGSize(width: UIScreen.isSE ? 90 : 101, height: UIScreen.isSE ? 96 : 108)
        static let imageCollevtionLineSpasing: CGFloat = UIScreen.isSE ? 12 : 14
        static let imageCollevtionInterItemSpasing: CGFloat = UIScreen.isSE ? 9 : 11
    }

    // MARK: - Public Properties

    let imageSelected = PublishRelay<UIImage?>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let avatarImageView: UIImageView = {
        let view = UIImageView(frame: CGRect(origin: Constants.avatarOrigin, size: Constants.avatarSize))
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFill
        view.backgroundColor = TalkmeColors.avatarBG
        view.layer.cornerRadius = Constants.avatarSize.height / 2
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        let originX = avatarImageView.frame.maxX + Constants.textLeftOffset
        label.frame = CGRect(
            x: originX,
            y: Constants.titleTopOffset,
            width: bounds.width - Constants.reservedWidth - (UIScreen.isSE ? 70 : 112),
            height: Constants.titleHeight
        )
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    private lazy var timeLabel: UILabel = {
        let label = UILabel()
        let originX = containerView.frame.maxX - 45
        label.frame = CGRect(
            x: originX,
            y: Constants.titleTopOffset,
            width: 40,
            height: Constants.titleHeight
        )
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    private lazy var messageTextLabel: UILabel = {
        let label = UILabel()
        let originX = avatarImageView.frame.maxX + Constants.textLeftOffset
        label.frame = CGRect(
            x: originX,
            y: titleLabel.frame.maxY + Constants.messageTextTopOffset,
            width: bounds.width - Constants.reservedWidth - (UIScreen.isSE ? 50 : 92),
            height: 0
        )
        label.lineBreakMode = .byWordWrapping
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.lessonTextDescription
        label.numberOfLines = 0
        return label
    }()

    private lazy var imagesCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = Constants.collectionImageSize
        layout.minimumLineSpacing = Constants.imageCollevtionLineSpasing
        layout.minimumInteritemSpacing = Constants.imageCollevtionInterItemSpasing
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.showsHorizontalScrollIndicator = false
        cvc.showsVerticalScrollIndicator = false
        cvc.isScrollEnabled = false
        cvc.registerCells(withModels: MessageImagesCollectionCellViewModel.self)
        return cvc
    }()

    lazy var containerFrame = CGRect(
        x: self.bounds.minX,
        y: self.bounds.minY,
        width: self.bounds.width - (UIScreen.isSE ? 40 : 82),
        height: self.bounds.height
    )

    lazy var containerView: UIView = {
        let container = UIView(frame: containerFrame)
        container.layer.cornerRadius = 8
        container.layer.masksToBounds = true
        container.backgroundColor = .clear
        return container
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        configureView()
        configureLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func configure(model: Message) {
        let dataItems = model.image?.map { MessageImagesCollectionCellViewModel(imageString: $0)}
        setCollectionImages(dataItems: dataItems)
        let name = model.author
        let nameAttrString = NSAttributedString(string: name, attributes: [.foregroundColor: TalkmeColors.messageAuthor])
        let messageTime = Formatters.convertTime(seconds: model.timestamp)
        let messageTimeString = NSAttributedString(string: messageTime, attributes: [.foregroundColor: TalkmeColors.streamGrayLabel])
        titleLabel.attributedText = nameAttrString
        timeLabel.attributedText = messageTimeString
        if let imageURL = model.avatar {
            let correctImageUrl = imageURL
            avatarImageView.kf.setImage(with: URL(string: correctImageUrl))
        } else {
            avatarImageView.contentMode = .center
            avatarImageView.image = UIImage(named: "emptyAvatar")
        }
        messageTextLabel.text = model.content
        updateLayout(with: model)
    }

    // MARK: - Private Methods

    private func configureView() {
        transform = CGAffineTransform(scaleX: 1, y: -1)
    }

    private func configureLayout() {
        containerView.addSubviews(avatarImageView, titleLabel, timeLabel, messageTextLabel, imagesCollection)
        addSubview(containerView)
    }

    private func updateLayout(with model: Message) {
        titleLabel.frame.size.height = Self.textHeight(from: model, for: bounds.width)
        titleLabel.sizeToFit()
        messageTextLabel.frame = CGRect(x: avatarImageView.frame.maxX + Constants.textLeftOffset,
                                        y: titleLabel.frame.maxY + Constants.messageTextTopOffset,
                                        width: bounds.width - Constants.reservedWidth - (UIScreen.isSE ? 50 : 92),
                                        height: 0)
        messageTextLabel.frame.size.height = Self.textHeight(from: model, for: bounds.width)
        messageTextLabel.sizeToFit()
        let collectionFrame = CGRect(
            origin: CGPoint(
                x: messageTextLabel.frame.origin.x,
                y: messageTextLabel.frame.maxY + 3 * Constants.messageTextTopOffset
            ),
            size: Self.imagesCollectionHeight(imagesCount: model.image?.count ?? 0)
        )
        imagesCollection.frame = collectionFrame

        containerFrame = CGRect(
            x: self.bounds.minX,
            y: self.bounds.minY,
            width: self.bounds.width - (UIScreen.isSE ? 40 : 82),
            height: Self.cellHeight(from: model, for: self.bounds.width)
        )
        containerView.frame = containerFrame
    }

    private func setCollectionImages(dataItems: [MessageImagesCollectionCellViewModel]?) {
        guard let dataItems = dataItems else { return }
        Observable.just(dataItems)
            .bind(to: imagesCollection.rx.items) { [weak self] collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)

                if let self = self {
                    data.imageTapped
                        .bind(to: self.imageSelected)
                        .disposed(by: data.bag)
                }
                return cell
            }
            .disposed(by: bag)
    }
}

// MARK: - SelfSizing

extension ChatOtherMessageView {
    static var heightCache: [String: CGFloat] = [:]
    static var heightTitleCache: [String: CGFloat] = [:]

    static func textHeight(from model: Message, for width: CGFloat) -> CGFloat {
        if let cachedTextHeight = heightCache[model.content] {
            return cachedTextHeight
        } else {
            let textHeight = model.content.height(
                font: .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15),
                width: width )
            heightCache[model.content] = textHeight
            return textHeight
        }
    }

    static func textHeightAuthor(from model: Message, for width: CGFloat) -> CGFloat {
        if let cachedTextHeight = heightTitleCache[model.author] {
            return cachedTextHeight
        } else {
            let textHeight = model.author.height(
                font: .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15),
                width: width - Constants.reservedWidth)
            heightTitleCache[model.author] = textHeight
            return textHeight
        }
    }

    static func imagesCollectionHeight(imagesCount: Int) -> CGSize {
        var collectionHeight: CGFloat = 0
        var collectionWidth = Constants.collectionImageSize.width * 2 + Constants.imageCollevtionInterItemSpasing
        switch imagesCount {
        case 1...2:
            collectionHeight = Constants.collectionImageSize.height
        case 3...4:
            collectionHeight = Constants.collectionImageSize.height * 2 + Constants.imageCollevtionLineSpasing
        case 5...6:
            collectionHeight = Constants.collectionImageSize.height * 3 + Constants.imageCollevtionLineSpasing * 2
        default:
            collectionHeight = .zero
            collectionWidth = .zero
        }
        return CGSize(width: collectionWidth, height: collectionHeight)
    }

    static func cellHeight(from model: Message, for width: CGFloat) -> CGFloat {
        let otherHeights = Constants.titleTopOffset + 2 * Constants.messageTextTopOffset + Constants.messageTextBottomOffset
        let minHeight = 2 * Constants.avatarOrigin.y + Constants.avatarSize.height
        let titleHeight = Self.textHeightAuthor(from: model, for: width)
        let textHeight = Self.textHeight(from: model, for: width)
        let imagesHeight = Self.imagesCollectionHeight(imagesCount: model.image?.count ?? 0).height
        let messageHeight = textHeight + imagesHeight
        return max(otherHeights + messageHeight + titleHeight, minHeight)
    }
}
