//
//  DeleteCellActionView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 29.03.2021.
//

import UIKit

final class DeleteCellActionView: UIView {

    // MARK: - Private Properties

    private let imageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "trashCan")?.withRenderingMode(.alwaysTemplate))
        imageView.frame = CGRect(
            origin: .zero,
            size: UIScreen.isSE
                ? CGSize(width: 19, height: 21)
                : CGSize(width: 25, height: 28)
        )
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        imageView.backgroundColor = .clear
        return imageView
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        updateFrames(frame.size)
        backgroundColor = TalkmeColors.warningColor
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func updateFrames(_ size: CGSize) {
        imageView.center = CGPoint(x: size.width / 2, y: size.height / 2)
    }
}
