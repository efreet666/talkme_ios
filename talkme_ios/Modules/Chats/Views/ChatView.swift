//
//  ChatView.swift
//  talkme_ios
//
//  Created by 1111 on 07.06.2021.
//

import CollectionKit
import RxSwift
import RxCocoa

final class ChatView: UIView {

    // MARK: - Public Properties

    private(set) var onFetchMoreMessages = PublishRelay<Void>()
    private(set) var onFullScreenImage = PublishRelay<UIImage?>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private var dataSource = ArrayDataSource<Message>()
    private var messagesCount = 0

    private lazy var collection: CollectionView = {
        let collection = CollectionView(frame: self.bounds)
        collection.clipsToBounds = true
        collection.showsVerticalScrollIndicator = true
        collection.showsHorizontalScrollIndicator = false
        collection.contentInset = UIEdgeInsets(top: UIScreen.isSE ? 12 : 14, left: 10, bottom: 0, right: 10)
        collection.transform = CGAffineTransform(scaleX: 1, y: -1)
        return collection
    }()

    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        indicator.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        return indicator
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
        fetchOldMessages()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func addMessage(model: ChatMessage) {
        var data = dataSource.data
        activityIndicator.stopAnimating()
        model.isNew ? data.insert(model.message, at: 0) : data.append(model.message)
        dataSource.data.removeAll()
        dataSource.reloadData()
        dataSource.data = data
        messagesCount += 1
    }

    func resetMessageDataSourse() {
        dataSource.data.removeAll()
        StreamChatMessageView.heightCache.removeAll()
        dataSource.reloadData()
    }

    func updateFrame() {
        collection.frame = self.bounds
    }

    // MARK: - Private Methods

    private func configureUI() {
        addSubviews([collection, activityIndicator])
        setupCollectionView()
    }

    private func setupCollectionView() {

        let myMessageSource = ClosureViewSource(viewGenerator: { [weak self] _, _ -> CommonChatMessage in
            return ChatMyMessageView(frame: self?.bounds ?? .zero)
        }, viewUpdater: { [weak self] (view: CommonChatMessage, dataItem: Message, _) in
            view.configure(model: dataItem)
            if let view = view as? ChatMyMessageView {
                view
                    .imageSelected
                    .bind { [weak self] image in
                        self?.onFullScreenImage.accept(image)
                    }
                    .disposed(by: self?.bag ?? DisposeBag())
            }
        })

        let otherMessageSource = ClosureViewSource(viewGenerator: { [weak self] _, _ -> CommonChatMessage in
            return ChatOtherMessageView(frame: self?.bounds ?? .zero)
        }, viewUpdater: { [weak self] (view: CommonChatMessage, dataItem: Message, _) in
            view.configure(model: dataItem)
            if let view = view as? ChatOtherMessageView {
                view
                    .imageSelected
                    .bind { [weak self] image in
                        self?.onFullScreenImage.accept(image)
                    }
                    .disposed(by: self?.bag ?? DisposeBag())
            }
        })

        let provider = BasicProvider(
            dataSource: dataSource,
            viewSource: ComposedViewSource(viewSourceSelector: { data in
                guard data.userId == UserDefaultsHelper.shared.userId else { return otherMessageSource }
                return myMessageSource
            }),
            sizeSource: ClosureSizeSource(sizeSource: { _, data, collectionSize in
                var height: CGFloat {
                    guard data.userId == UserDefaultsHelper.shared.userId else {
                        return ChatOtherMessageView.cellHeight(from: data, for: collectionSize.width)
                    }
                    return ChatMyMessageView.cellHeight(from: data, for: collectionSize.width)
                }
                return CGSize(
                    width: collectionSize.width,
                    height: height
                )
            })
        )
        let layout = FlowLayout(lineSpacing: 10, justifyContent: .start)
        provider.layout = layout
        collection.provider = provider
    }

    private func fetchOldMessages() {
        collection.rx.didEndDragging
            .bind { [weak self] _ in
                guard let self = self else { return }
                guard self.collection.visibleIndexes.contains(self.messagesCount - 1),
                      self.activityIndicator.isAnimating == false
                else { return }
                self.onFetchMoreMessages.accept(())
                self.activityIndicator.startAnimating()
            }
            .disposed(by: bag)
    }
}
