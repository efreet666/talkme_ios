//
//  ChatsCoordinator.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 26.03.2021.
//

import RxSwift
import RxCocoa

final class ChatsCoordinator {

    // MARK: - Public Properties

    let bag = DisposeBag()
    let userStatus = PublishSubject<UserStatus>()

    // MARK: - Private Properties
    private(set) weak var navigationControllerChats: BaseNavigationController?
    private let qrCoordinator = QrCodeCoordinator()
    private let service = MainService()

    // MARK: - Public Methods
    @discardableResult
    func start(_ navigationControllers: UINavigationController? = nil) -> UINavigationController {
        let allChatsService: SocketAllChatsService = SocketAllChatsServiceFactory.make()
        let viewModel = ChatsListViewControllerViewModel(service: allChatsService)
        let controller = ChatsListViewController(viewModel: viewModel)
        let navigationController = BaseNavigationController(rootViewController: controller)
        self.navigationControllerChats = navigationController
        bindChatListVM(viewModel)
        bindQrButton()
        return navigationController
    }

    func startChat(_ navigationController: BaseNavigationController?, userId: Int) {
        self.navigationControllerChats = navigationController
        createNewChat(chats: NewChatRequest(createChats: [NewChatUserModel(content: "", toUser: userId)]))
    }

    // MARK: - Private Methods
    private func bindQrButton() {
        guard let navigationController = navigationControllerChats else { return }
        navigationController
            .qrButtonTapped
            .bind { [weak self] _ in
                guard let navController = self?.navigationControllerChats else { return }
                self?.qrCoordinator.start(from: navController)
            }
            .disposed(by: navigationController.bag)
    }

    private func showContactsPopUp() {
        let model = ChatContactsListViewModel(service: service)
        bindContactsPopUpVM(model)
        let contactsPopUp = ChatContactsListView(viewModel: model)
        let drawer = DrawerViewController(contentView: contactsPopUp)
        navigationControllerChats?.present(drawer, animated: true)
    }

    private func showSelectedChat(chatId: Int, service: SocketChatService) {
        let viewModel = ChatViewModel(chatId: chatId, service: service)
        let controller  = ChatViewController(viewModel: viewModel)
        navigationControllerChats?.pushViewController(controller, animated: true)
    }

    private func bindChatListVM(_ model: ChatsListViewControllerViewModel) {
        model
            .flow
            .bind { [weak self] event in
                switch event {
                case .createNewChat:
                    self?.showContactsPopUp()
                case .toChat(let chatId):
                    let service = SocketChatServiceFactory.make(roomId: String(chatId))
                    self?.showSelectedChat(chatId: chatId, service: service)
                }
            }
            .disposed(by: bag)

        userStatus
            .bind(to: model.userStatus)
            .disposed(by: bag)
    }

    private func bindContactsPopUpVM(_ model: ChatContactsListViewModel) {
        model
            .flow
            .bind { [weak self] event in
                switch event {
                case .createChat(let chats):
                    self?.createNewChat(chats: chats)
                }
            }.disposed(by: model.bag)
    }

    private func createNewChat(chats: NewChatRequest) {
        service
            .createNewChat(chats: chats)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let chatId = response.values.first else { return }
                    let service = SocketChatServiceFactory.make(roomId: String(chatId))
                    self?.showSelectedChat(chatId: chatId, service: service)
                case .error(let error):
                    print("[Create new chat error \(error)]")
                }
            }
            .disposed(by: bag)
    }
}
