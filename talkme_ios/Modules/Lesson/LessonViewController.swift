//
//  LessonViewController.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 15.02.2021.
//

import UIKit
import RxSwift

class LessonViewController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.registerCells(
            withModels: LessonPrimaryInfoViewModel.self,
            LessonGeneralDescriptionViewModel.self,
            TextDescriptionCellViewModel.self,
            LessonOwnerViewModel.self,
            SimilarLessonsCellViewModel.self)
        return tableView
    }()

    private let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "talkme"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let viewModel: LessonViewControllerViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: LessonViewControllerViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        bindVM()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getLesson()
        setupNavigationBar()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel.items.bind(to: tableView.rx.items) { tableView, row, model in
            let index = IndexPath(row: row, section: 0)
            let cell = tableView.dequeueReusableCell(withModel: model, for: index)
            model.configureAny(cell)
            return cell
        }.disposed(by: bag)

        viewModel.classNumber
            .bind { [weak self] classNumber in
                guard !classNumber.isEmpty else {
                    self?.customNavigationController?.style = .gradientBlueWithQR
                    return
                }
                self?.customNavigationController?.style = .gradientBlueWithQRAndClassNumber(classNumber)
            }.disposed(by: bag)

        viewModel
            .onPresentCantUnsubscribeAlert
            .bind { [weak self] text in
                self?.presentCantUnsubscribeAlert(text)
            }
            .disposed(by: bag)
    }

    private func setUpView() {
        view.backgroundColor = TalkmeColors.mainAccountBackground
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigationBar() {
        customNavigationController?.style = UserDefaultsHelper.shared.isSpecialist
            ? .gradientBlueWithQRAndClassNumber(viewModel.classNumber.value)
            : .gradientBlueWithQR
    }

    private func presentCantUnsubscribeAlert(_ text: String) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "general_cancel".localized, style: .cancel)
        alert.addAction(closeAction)
        self.present(alert, animated: true)
    }
}
