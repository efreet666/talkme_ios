//
//  LessonCoordinator.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 18.02.2021.
//

import RxSwift
import RxCocoa

final class LessonCoordinator {

    enum Flow {
        case completeLessonInfo
        case topUpBalance
        case onChat(Int)
        case makeComplaint(Int)
        case exitToRoot
        case exitToPreviousScreen
    }

    // MARK: - Public properties

    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let lessonService = LessonsService()
    private let paymentsService = PaymentsService()
    private let mainService = MainService()
    private let accountService = AccountService()
    private weak var navigationController: BaseNavigationController?
    private let editCoordinator = LessonCreateEditCoordinator()
    private let streamCoordinator = StreamCoordinator()
    private var lastUsedController: UIViewController?
    private let reloadLessonInfo = PublishRelay<Void>()

    // MARK: - Initializers

    init() {
        bindStreamCoordinator()
        bindEditCoordinator()
    }

    // MARK: - Public Methods

    func start(_ navigationController: BaseNavigationController?, lessonId: Int) {
        self.navigationController = navigationController
        showLessonVC(with: lessonId)
    }

    // MARK: - Private Methods

    private func bindLessonControllerViewModel(_ model: LessonViewControllerViewModel) {
        model.flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .watch(let id, let countOfStreamsInOnePage):
                    self.showStream(lessonId: id, addNewStreamsToListByChunksWithSizeOf: countOfStreamsInOnePage)
                case .chatWithOwner(let id):
                    self.flow.accept(.onChat(id))
                case .edit(let id):
                    self.showEditVC(with: id)
                case .showLesson(let id):
                    self.showLessonVC(with: id)
                case .showLessonNotStarted:
                    return
                case .showPayConfirmation(let cost, let id, _):
                    self.showToBuyLessonPopup(cost: cost, id: id)
                case .showReplenishBalance:
                    self.showBalanceRefill()
                case .onSelectedTeacher(let classNumber):
                    guard let classNumber = classNumber else { return }
                    self.showUserAccountInfoVC(classNumber: classNumber)
                case .alertWarning:
                    self.presentAlertWarning()
                case .makeComplaint(let id):
                    self.flow.accept(.makeComplaint(id))
                }
            }.disposed(by: model.bag)

        reloadLessonInfo
            .bind { [weak model] _ in
                model?.getLesson()
            }
            .disposed(by: model.bag)
    }

    private func showBalanceRefill() {
        let balanceVM = MyBalanceRefillViewModel(paymentsService: paymentsService)
        let balanceVC = MyBalanceRefillViewController(viewModel: balanceVM, isWhiteNavBar: false)
        navigationController?.pushViewController(balanceVC, animated: true)
    }

    private func showLessonVC(with lessonID: Int) {
        let model = LessonViewControllerViewModel(service: lessonService, paymentsService: paymentsService, id: lessonID)
        let lessonVC = LessonViewController(viewModel: model)
        bindLessonControllerViewModel(model)
        navigationController?.pushViewController(lessonVC, animated: true)
    }

    private func showEditVC(with lessonID: Int) {
        lastUsedController = navigationController?.viewControllers.last
        editCoordinator.start(navigationController, lessonID: lessonID, type: .edit)
    }

    private func showUserAccountInfoVC(classNumber: String) {
        let viewModel = UserAccountViewModel(service: accountService, classNumber: classNumber)
        let controller = UserAccountViewController(viewModel: viewModel)
        bindUserAccountVM(viewModel: viewModel)
        self.navigationController?.pushViewController(controller, animated: true)
    }

    private func showToBuyLessonPopup(cost: Int, id: Int) {
        let controller = WhitePayToJoinPopUpViewController(cost: cost)
        bindToBuyLessonPopup(controller, id: id)
        controller.modalPresentationStyle = .overFullScreen
        navigationController?.present(controller, animated: false)
    }

    private func presentAlertWarning() {
        let alert = UIAlertController(title: "wait_for_the_teacher_to_start_broadcasting".localized, message: "", preferredStyle: .alert)
         let action = UIAlertAction(title: "OK", style: .cancel)
         alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func subscribeToPrimeryLesson(_ lessonId: Int, controller: WhitePayToJoinPopUpViewController) {
        lessonService.lessonSubscribe(id: lessonId, lessonIsSaved: false)
            .subscribe { [weak controller, weak self] event in
                guard let controller = controller, let self = self else { return }
                switch event {
                case .success(let response):
                    guard response.statusCode == 200 else { return }
                    controller.dismiss(animated: false)
                    self.reloadLessonInfo.accept(())
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func bindToBuyLessonPopup(_ controller: WhitePayToJoinPopUpViewController, id: Int) {
        controller
            .flow
            .bind { [weak self, weak controller] event in
                guard let self = self, let controller = controller else { return }
                switch event {
                case .dismiss:
                    controller.dismiss(animated: false)
                case .buyAndDismiss:
                    self.subscribeToPrimeryLesson(id, controller: controller)
                }
            }
            .disposed(by: controller.bag)
    }

    private func bindUserAccountVM(viewModel: UserAccountViewModel) {
        viewModel
            .flow
            .bind { [weak self] id in
                switch id {
                case .onLessonDetail(let lessonId):
                    self?.start(self?.navigationController, lessonId: lessonId)
                case .makeComplaint(let userId):
                    self?.flow.accept(.makeComplaint(userId))
                case .watchStream(let userId, let lessonId, let streamsList, let numberStreamsinOnePage):
                    self?.showSavedStreams(from: streamsList,
                                           createdBy: userId,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: numberStreamsinOnePage)
                case .onListOfUsersSavedLessons:
                    break
                }
            }
            .disposed(by: viewModel.bag)

        viewModel
            .onCreateChatTapped
            .bind { [weak self] id in
                self?.flow.accept(.onChat(id))
            }
            .disposed(by: viewModel.bag)
    }

    private func bindEditCoordinator() {
        editCoordinator.flow
            .bind {[weak self] event in
                switch event {
                case .completeCreationEdit:
                    guard let last = self?.lastUsedController else { return }
                    self?.navigationController?.popToViewController(last, animated: true)
                }
            }.disposed(by: bag)
    }

    private func showStream(lessonId: Int, addNewStreamsToListByChunksWithSizeOf countOfStreamsInOnePage: Int) {
        streamCoordinator.start(navigationController, lessonId: lessonId, lessonsInfo: [], numberOfStreamsInPage: countOfStreamsInOnePage)
    }

    private func bindStreamCoordinator() {
        streamCoordinator
            .flow
            .bind { [weak self] event in
                switch event {
                case .exitToRoot:
                    self?.navigationController?.popToRootViewController(animated: true)
                case .toChat(let id):
                    self?.flow.accept(.onChat(id))
                case .makeComplaint(let id):
                    self?.flow.accept(.makeComplaint(id))
                case .exitToPreviousScreen:
                    self?.navigationController?.popViewController(animated: true)
                }
            }
            .disposed(by: bag)
    }

    private func showSavedStreams(from streamsList: [LiveStream],
                                  createdBy userId: Int,
                                  startFrom lessonId: Int,
                                  addNewStreamsToListByChunksWithSizeOf countOfStreamsInOnePage: Int
    ) {
        streamCoordinator.start(navigationController,
                                lessonId: lessonId,
                                lessonsInfo: streamsList, numberOfStreamsInPage: countOfStreamsInOnePage,
                                watchSave: true,
                                watchStreamsCreatedBy: userId)
    }
}
