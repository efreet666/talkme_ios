//
//  LessonViewControllerViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 15.02.2021.
//
import RxSwift
import RxCocoa

fileprivate extension Consts {
    static let numberOfLiveStreamsInPage = 50
}

final class LessonViewControllerViewModel {

    enum Flow {
        case watch(_ lessonId: Int, numberOfLiveStreamsInPage: Int)
        case showLessonNotStarted
        case edit(_ id: Int)
        case chatWithOwner(_ id: Int)
        case showLesson(_ id: Int)
        case showPayConfirmation(_ cost: Int, id: Int, numberOfStreamsInOneModel: Int)
        case showReplenishBalance
        case onSelectedTeacher(_ classNumber: String?)
        case alertWarning
        case makeComplaint(_ id: Int)
    }

    // MARK: - Public properties

    let items = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let classNumber = BehaviorRelay<String>(value: UserDefaultsHelper.shared.classNumber)
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    let onPresentCantUnsubscribeAlert = PublishRelay<String>()

    // MARK: - Private Properties

    private let service: LessonsServiceProtocol
    private let paymentsService: PaymentsServiceProtocol
    private var id: Int
    private var responses: (LessonDetailResponse?, MainLessonResponse?)
    private var isSubscribed = true

    // MARK: - Initializers

    init(service: LessonsServiceProtocol, paymentsService: PaymentsServiceProtocol, id: Int) {
        self.service = service
        self.paymentsService = paymentsService
        self.id = id
    }

    // MARK: - Public Methods

    func getLesson() {
        Single.zip(service.lessonDetail(id: id, saved: false), service.mainLesson(id: id))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let responses):
                    self.responses = responses
                    self.setUpItems(with: responses)
                    self.classNumber.accept(UserDefaultsHelper.shared.classNumber)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    // MARK: - Private Methods

    private func setUpItems(with responses: (lessonDetail: LessonDetailResponse, mainLesson: MainLessonResponse)) {
        let mainInfoModel = LessonPrimaryInfoViewModel(responses.lessonDetail)
        bindPrimaryInfoModel(mainInfoModel)
        let generalInfoModel = LessonGeneralDescriptionViewModel(responses)
        var cells: [AnyTableViewCellModelProtocol] = [mainInfoModel, generalInfoModel]
        if !responses.lessonDetail.description.isEmpty {
            let textDescriptionModel = TextDescriptionCellViewModel(responses.lessonDetail)
            cells.append(textDescriptionModel)
        }
        if !responses.lessonDetail.isOwner {
            let ownerModel = LessonOwnerViewModel(responses.lessonDetail.owner)
            bindOwnerViewModel(ownerModel)
            cells.append(ownerModel)
        }
        if !responses.lessonDetail.similarLesson.isEmpty {
            let similarLessonsModel = SimilarLessonsCellViewModel(responses.lessonDetail.similarLesson)
            bindSimilarLessonsviewModel(similarLessonsModel)
            cells.append(similarLessonsModel)
        }
        if isSubscribed {
            if responses.lessonDetail.isSubscribe {
                if responses.lessonDetail.date > Date().timeIntervalSince1970 {
                } else {
                    if !responses.lessonDetail.isStarted {
                        self.flow.accept(.alertWarning)
                    } else {
                        isSubscribed = false
                    }
                }
            }
        }
        items.accept(cells)
    }

    private func lessonStartedCheckWithOwner(_ lessonDetails: LessonDetailResponse) {
        lessonDetails.isStarted
        ? self.flow.accept(.watch(lessonDetails.id, numberOfLiveStreamsInPage: Consts.numberOfLiveStreamsInPage))
            : self.flow.accept(.edit(lessonDetails.id))
    }

    private func lessonStartedCheck(_ lessonDetails: LessonDetailResponse, model: LessonPrimaryInfoViewModel) {
        if lessonDetails.cost == 0 {
            if lessonDetails.date < Date().timeIntervalSince1970 {
                if lessonDetails.isSubscribe {
                    self.flow.accept(.watch(lessonDetails.id, numberOfLiveStreamsInPage: Consts.numberOfLiveStreamsInPage))
                }
            } else {
                self.flow.accept(.alertWarning)
            }
        }
        self.captureSubscribeAction(lessonDetails: lessonDetails, model: model)
    }

    private func bindPrimaryInfoModel(_ model: LessonPrimaryInfoViewModel) {
        model.flow
            .bind { [weak self, weak model] _ in
                guard let lessonDetails = self?.responses.0, let self = self, let model = model else { return }
                if lessonDetails.isOwner {
                    self.lessonStartedCheckWithOwner(lessonDetails)
                    return
                }
                if lessonDetails.isStarted {
                    self.lessonStartedCheck(lessonDetails, model: model)
                    return
                }
                if lessonDetails.isSubscribe {
                    self.captureUnsubscribeAction(lessonDetails: lessonDetails, model: model)
                    return
                }
                self.captureSubscribeAction(lessonDetails: lessonDetails, model: model)
            }.disposed(by: model.bag)

        model.flowComplaint
            .bind { [weak self ] _ in
                if UserDefaultsHelper.shared.userId == self?.responses.0?.owner.id ?? 0 { return }
                self?.flow.accept(.makeComplaint(
                    self?.responses.0?.owner.id ?? 0))
            }.disposed(by: model.bag)
    }

    private func bindOwnerViewModel(_ model: LessonOwnerViewModel) {
        model.flow
            .map { _ in Flow.chatWithOwner((self.responses.0?.owner.id)!) }
            .bind(to: flow)
            .disposed(by: model.bag)

        model
            .onSelectTeacher
            .map { .onSelectedTeacher(self.responses.0?.owner.numberClass) }
            .bind(to: flow)
            .disposed(by: model.bag)
    }

    private func bindSimilarLessonsviewModel(_ model: SimilarLessonsCellViewModel) {
        model.modelSelected
            .bind { [weak self] lessonDetail in
                self?.flow
                    .accept(.showLesson(lessonDetail.id))
            }.disposed(by: model.bag)
    }

    private func subscribeLesson(lessonDetails: LessonDetailResponse, model: LessonPrimaryInfoViewModel) {
        if lessonDetails.cost != 0 {
            subscribeWithCostCheck(lessonDetails: lessonDetails, model: model)
        } else {
            simpleSubscribe(id: lessonDetails.id, model: model)
        }
    }

    private func unsubscribeLesson(lessonDetails: LessonDetailResponse, model: LessonPrimaryInfoViewModel) {
        service.lessonUnsubscribe(id: lessonDetails.id)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    guard let text = response.msg,
                          text == "Отказаться от урока можно не позднее чем за 8 часов до начала."
                    else {
                        self.getLesson()
                        return
                    }
                    self.onPresentCantUnsubscribeAlert.accept(text)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: model.bag)
    }

    private func subscribeWithCostCheck(lessonDetails: LessonDetailResponse, model: LessonPrimaryInfoViewModel) {
        paymentsService.getCoin()
            .subscribe { [weak self] event in
                switch event {
                case .success(let budgetResponse):
                    self?.getLesson()
                    if budgetResponse.coin < lessonDetails.cost {
                        self?.flow.accept(.showReplenishBalance)
                    } else {
                        if lessonDetails.isSubscribe {
                            if lessonDetails.date < Date().timeIntervalSince1970 {
                                lessonDetails.isStarted
                                    ? self?.flow.accept(.watch(lessonDetails.id, numberOfLiveStreamsInPage: Consts.numberOfLiveStreamsInPage))
                                    : self?.flow.accept(.alertWarning)
                            }
                        } else {
                            self?.flow.accept(.showPayConfirmation(lessonDetails.cost,
                                                                   id: lessonDetails.id ,
                                                                   numberOfStreamsInOneModel: Consts.numberOfLiveStreamsInPage))
                        }
                    }
                case .error(let error):
                    print(error)
                }
            }.disposed(by: model.bag)
    }

    private func captureSubscribeAction(lessonDetails: LessonDetailResponse, model: LessonPrimaryInfoViewModel) {
        guard lessonDetails.cost != 0 else {
            simpleSubscribe(id: lessonDetails.id, model: model)
            return
        }
        subscribeWithCostCheck(lessonDetails: lessonDetails, model: model)
    }

    private func simpleSubscribe(id: Int, model: LessonPrimaryInfoViewModel) {
        service.lessonSubscribe(id: id, lessonIsSaved: false)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.statusCode == 200 else { return }
                    self?.getLesson()
                case .error(let error):
                    print(error)
                }
            }.disposed(by: model.bag)
    }

    private func captureUnsubscribeAction(lessonDetails: LessonDetailResponse, model: LessonPrimaryInfoViewModel) {
        self.getLesson()
        guard lessonDetails.cost != 0 else {
            unsubscribeLesson(lessonDetails: lessonDetails, model: model)
            return
        }
        guard lessonDetails.date > Date().timeIntervalSince1970 else {
            lessonDetails.isStarted
                ? self.flow.accept(.watch(lessonDetails.id, numberOfLiveStreamsInPage: Consts.numberOfLiveStreamsInPage))
                : self.flow.accept(.alertWarning)
            return
        }
        unsubscribeLesson(lessonDetails: lessonDetails, model: model)
    }
}
