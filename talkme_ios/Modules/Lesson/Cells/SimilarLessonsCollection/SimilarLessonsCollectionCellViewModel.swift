//
//  SimilarLessonsCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 22.02.2021.
//

import UIKit

final class SimilarLessonsCollectionCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let lessonsModel: LiveStream

    // MARK: - Initializers

    init(lessonsModel: LiveStream) {
        self.lessonsModel = lessonsModel
    }

    // MARK: - Public Methods

    func configure(_ cell: SimilarLessonsCollectionCell) {
        cell.configure(lessonsModel: lessonsModel)
    }
}
