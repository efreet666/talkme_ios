//
//  SimilarLessonsCollectionCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 22.02.2021.
//

import UIKit

final class SimilarLessonsCollectionCell: UICollectionViewCell {

    // MARK: - Private properties

    private let itemView = SimilarLessonView()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(lessonsModel: LiveStream) {
        itemView.configure(lessonModel: lessonsModel)
    }

    // MARK: - Private Method

    private func setupLayout() {
        contentView.addSubview(itemView)
        itemView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
