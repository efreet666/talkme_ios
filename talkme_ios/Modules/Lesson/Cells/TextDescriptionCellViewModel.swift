//
//  TextDescriptionCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 09.02.2021.
//

import UIKit

final class TextDescriptionCellViewModel: TableViewCellModelProtocol {

    // MARK: - Private Properties

    private var lessonDescription: String?

    // MARK: - Initializers

    init(_ response: LessonDetailResponse) {
        lessonDescription = response.description
    }

    // MARK: - Public Methods

    func configure(_ cell: TextDescriptionCell) {
        let title = "lesson_description_title".localized
        cell.configure(title: title, description: lessonDescription)
    }
}
