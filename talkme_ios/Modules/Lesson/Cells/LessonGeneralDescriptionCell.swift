//
//  LessonGeneralDescriptionCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.02.2021.
//

import UIKit

final class LessonGeneralDescriptionCell: UITableViewCell {

    enum Constants {
        static let spacing: CGFloat = UIScreen.isSE ? 5 : 15
        static let leadingTrailingInset: CGFloat = UIScreen.isSE ? 10 : 12
        static let topBottomInset: CGFloat = UIScreen.isSE ? 23 : 24
    }

    // MARK: - Private Properties

    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Constants.spacing
        stackView.distribution = .fillEqually
        return stackView
    }()

    // MARK: - Initializers

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpLayout()
        setUpCell()
    }

    // MARK: - Public Methods

    func configure(model: [LessonGeneralDescriptionElementType]) {
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        model.forEach {
            let view = LessonGeneralDescriptionElementView(
                image: $0.model.icon,
                title: $0.model.title,
                valueDescription: $0.model.value,
                textColor: $0.color)
            stackView.addArrangedSubview(view)
        }
    }

    // MARK: - Private Methods

    private func setUpLayout() {
        contentView.addSubview(stackView)
        let oneViewWidth = (UIScreen.main.bounds.width - (Constants.spacing) * 2 - Constants.leadingTrailingInset * 2) / 3

        stackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(Constants.leadingTrailingInset)
            make.top.bottom.equalToSuperview().inset(Constants.topBottomInset)
            make.height.equalTo(oneViewWidth * (UIScreen.isSE ? 1.236 : 1.1487))
        }
    }

    private func setUpCell() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
