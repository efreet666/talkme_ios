//
//  LessonGeneralDescriptionViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.02.2021.
//

import Foundation
import UIKit

final class LessonGeneralDescriptionViewModel: TableViewCellModelProtocol {

    // MARK: - Private Properties

    private let items: [LessonGeneralDescriptionElementType]

    // MARK: - Initializers

    init(_ response: (lessonDetail: LessonDetailResponse, mainLesson: MainLessonResponse)) {
        let category = LessonCategoryFilledType.init(rawValue: response.mainLesson.category.id)
        let icon = category?.icon
        let duration = Formatters.setUpHourAndMinuteWithoutZeroHoursLocalized(date: response.lessonDetail.lessonTime)
        items = [
            LessonGeneralDescriptionElementType.category(
                LessonGeneralDescriptionElementModel(
                    icon: icon,
                    title: "lesson_attribute_category".localized,
                    value: response.lessonDetail.categoryName
                ),
                response.mainLesson.category.id
            ),
            LessonGeneralDescriptionElementType.numberOfParticipants(
                LessonGeneralDescriptionElementModel(
                    icon: UIImage(named: "numberOfParticipants"),
                    title: "lesson_attribute_number_of_participants".localized,
                    value: "\(response.lessonDetail.numberOfParticipants)"
                )
            ),
            LessonGeneralDescriptionElementType.duration(
                LessonGeneralDescriptionElementModel(
                    icon: UIImage(named: "lessonDuration"),
                    title: "lesson_attribute_duration".localized,
                    value: duration
                )
            )
        ]
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonGeneralDescriptionCell) {
        cell.configure(model: items)
    }
}
