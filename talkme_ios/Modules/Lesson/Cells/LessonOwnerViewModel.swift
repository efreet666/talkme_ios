//
//  LessonOwnerViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 14.02.2021.
//

import RxSwift
import RxCocoa

final class LessonOwnerViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let flow = PublishRelay<Void>()
    let onSelectTeacher = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let owner: LessonOwnerModel?

    // MARK: - Initializers

    init(_ owner: Owner) {
        guard let id = owner.id,
              let firstName = owner.firstName,
              let lastName = owner.lastName,
              let avatarUrl = owner.avatarUrl
        else {
            self.owner = nil
            return
        }
        self.owner = LessonOwnerModel(
            id: id,
            fullName: firstName + " " + lastName,
            avatarURL: avatarUrl)
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonOwnerCell) {
        guard let owner = owner else { return }
        cell.configure(owner.avatarURL, owner.fullName)
        cell.flow
            .bind(to: flow)
            .disposed(by: cell.bag)

        cell
            .onSelectTeacher
            .bind(to: onSelectTeacher)
            .disposed(by: cell.bag)
    }
}
