//
//  SimilarLessonsCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 22.02.2021.
//

import RxSwift
import RxCocoa

final class SimilarLessonsCell: UITableViewCell {

    enum Constants {
        static let inset: CGFloat = UIScreen.isSE ? 10 : 12
        static let collectionCellWidth: CGFloat = (UIScreen.main.bounds.width - inset * 2) / 2.2543
        static let collectionCellHeight: CGFloat = collectionCellWidth * (UIScreen.isSE ? 1.5909 : 1.6821)
    }

    // MARK: - Public Properties

    lazy var modelSelected = lessonsCollectionView.rx
        .modelSelected(SimilarLessonsCollectionCellViewModel.self)
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let nearestLessonsLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let lessonsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()

        layout.itemSize = UIScreen.isSE
            ? CGSize(width: 132, height: 210)
            : CGSize(width: Constants.collectionCellWidth, height: Constants.collectionCellHeight)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = UIScreen.isSE ? 7 : 11
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.contentInset = UIEdgeInsets(top: 0, left: Constants.inset, bottom: 0, right: Constants.inset)
        cvc.showsHorizontalScrollIndicator = false
        cvc.registerCells(withModels: SimilarLessonsCollectionCellViewModel.self)
        return cvc
    }()

    private let nearestButton: UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: "mainPageNext"), for: .normal)
        return bt
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        nearestButton.isHidden = true
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]?, title: String) {
       guard let mainItems = dataItems else { return }
        Observable.just(mainItems)
            .bind(to: lessonsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
        nearestLessonsLabel.text = title
    }

    // MARK: - Private Methods

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([nearestLessonsLabel, lessonsCollectionView, nearestButton])

        nearestLessonsLabel.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 21 : 27)
            make.top.equalToSuperview()
            make.leading.equalToSuperview().inset(Constants.inset)
        }

        nearestButton.snp.makeConstraints { make in
            make.centerY.equalTo(nearestLessonsLabel)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 11 : 13)
            make.size.equalTo(CGSize(width: UIScreen.isSE ? 8 : 11, height: UIScreen.isSE ? 15 : 19))
        }

        lessonsCollectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(nearestLessonsLabel.snp.bottom).offset(UIScreen.isSE ? 11 : 14)
            make.height.equalTo(UIScreen.isSE ? 210 : Constants.collectionCellHeight)
        }
    }
}
