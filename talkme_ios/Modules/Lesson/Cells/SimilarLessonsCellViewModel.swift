//
//  SimilarLessonsCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 22.02.2021.
//

import RxCocoa
import RxSwift

final class SimilarLessonsCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let modelSelected = PublishRelay<LiveStream>()
    let bag = DisposeBag()

    // MARK: - Private properties

    private var collectionTitle = "lesson_similar_lessons_title".localized
    private var dataItems: [AnyCollectionViewCellModelProtocol]?

    // MARK: - Initializers

    init(_ lessons: [LiveStream]) {
        let items: [AnyCollectionViewCellModelProtocol] = lessons.map { SimilarLessonsCollectionCellViewModel(lessonsModel: $0) }
        dataItems = items
    }

    // MARK: Public Methods

    func configure(_ cell: SimilarLessonsCell) {
        cell.setMainItems(dataItems: dataItems, title: collectionTitle)
        cell.modelSelected
            .map { $0.lessonsModel }
            .bind(to: modelSelected)
            .disposed(by: cell.bag)
    }
}
