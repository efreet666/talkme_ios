//
//  LessonPrimaryInfoCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 13.02.2021.
//

import UIKit
import Kingfisher
import RxSwift
import RxCocoa

final class LessonPrimaryInfoCell: UITableViewCell {

    // MARK: - Public properties

    let flowComplaint = PublishRelay<Void>()
    let flow = PublishRelay<Void>()
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let picture: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        let layer = CAGradientLayer()
        layer.colors = [
            TalkmeColors.firstGradientColor.cgColor,
            TalkmeColors.secondGradientColor.cgColor
        ]
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer.transform = CATransform3DMakeAffineTransform(
            CGAffineTransform(
                a: 0,
                b: -0.36,
                c: 0.36,
                d: -0.02,
                tx: 0.29,
                ty: 0.87
            )
        )
        imageView.layer.addSublayer(layer)
        return imageView
    }()

    private let lessonTitle: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: 20)
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()

    private var complaintButton: SideActionButtonView = {
        let button = SideActionButtonView(type: .complaint, size: CGSize(width: UIScreen.isSE ? 26 : 28, height: UIScreen.isSE ? 26 : 28))
        button.contentMode = .scaleAspectFit
        return button
    }()

    private let dateView = LessonDateView()
    private let priceView = LessonPriceView()
    private let actionButton = LessonActionButton()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpConstraints()
        setUpCell()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ model: LessonPrimaryInfoModel) {
        if let imageURL = URL(string: model.image) {
            picture.kf.setImage(with: ImageResource(downloadURL: imageURL, cacheKey: imageURL.absoluteString))
        }
        lessonTitle.text = model.title
        dateView.configure(model.time, date: model.date)
        priceView.configure(model.price)
        actionButton.configure(with: model.buttonState)
        bindUI()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    override func draw(_ rect: CGRect) {
        picture.layer.sublayers?.last?.frame = picture.bounds
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        contentView.addSubviews(picture, lessonTitle, dateView, priceView, actionButton, complaintButton)

        picture.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(picture.snp.width).dividedBy(UIScreen.isSE ? 1.4953 : 1.5109)
        }

        lessonTitle.snp.makeConstraints { make in
            make.leading.equalTo(picture.snp.leading).inset(UIScreen.isSE ? 10 : 16)
            make.trailing.equalTo(picture.snp.trailing).inset(UIScreen.isSE ? 27 : 36)
            make.bottom.equalTo(picture.snp.bottom).inset(UIScreen.isSE ? 30 : 27)
        }

        dateView.snp.makeConstraints { make in
            let inset: CGFloat = UIScreen.isSE ? 10 : 12
            let spacing: CGFloat = UIScreen.isSE ? 9 : 15
            let dateViewWidth = (UIScreen.main.bounds.width - inset * 2 - spacing) / (UIScreen.isSE ? 1.5815 : 1.7045)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.bottom.equalToSuperview()
            make.width.equalTo(dateViewWidth)
            make.top.equalTo(picture.snp.bottom).inset(UIScreen.isSE ? 6 : 9)
            make.height.equalTo(dateView.snp.width).dividedBy(UIScreen.isSE ? 1.5948 : 1.8965)
        }

        priceView.snp.makeConstraints { make in
            make.top.equalTo(dateView)
            make.leading.equalTo(dateView.snp.trailing).offset(UIScreen.isSE ? 9 : 15)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(priceView.snp.width).dividedBy(UIScreen.isSE ? 1.606 : 2.2794)
        }

        actionButton.snp.makeConstraints { make in
            make.bottom.equalTo(dateView)
            make.top.equalTo(priceView.snp.bottom).offset(UIScreen.isSE ? 9 : 7)
            make.leading.trailing.equalTo(priceView)
        }

        complaintButton.snp.makeConstraints { make in
            make.top.equalTo(picture).inset(UIScreen.isSE ? 11 : 13)
            make.trailing.equalTo(actionButton)
            make.size.equalTo(UIScreen.isSE ? 26 : 28)
        }
    }

    private func setUpCell() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func bindUI() {
        actionButton.rx
            .tapGesture()
            .when(.recognized)
            .map { _ in ()}
            .bind(to: flow)
            .disposed(by: bag)

        complaintButton.rx.tapGesture()
            .when(.recognized)
            .map { _ in () }
            .bind(to: flowComplaint)
            .disposed(by: bag)
    }
}
