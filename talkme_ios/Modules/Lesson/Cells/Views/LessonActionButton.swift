//
//  LessonActionButton.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 13.02.2021.
//

import UIKit

final class LessonActionButton: UIView {

    enum State {
        case subscribe
        case unsubscribe
        case pay
        case watch
        case edit
    }

    // MARK: - Private Properties

    private let title: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 14 : 15)
        label.textAlignment = .center
        return label
    }()

    private let icon: UIImageView = {
        let iconView = UIImageView()
        iconView.image = UIImage(named: "streamEdit")?.withRenderingMode(.alwaysTemplate)
        iconView.tintColor = TalkmeColors.blueLabels
        return iconView
    }()

    private var state: State = .pay {
        didSet {
            setUpView(state)
        }
    }

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        setUpConstraints()
        setUpUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(with state: State) {
        self.state = state
    }

    // MARK: - Private Methods

    private func setUpView(_ state: State) {
        switch state {
        case .pay:
            setGreenBackground(with: "lesson_button_title_pay".localized)
            hideIcon(true)
        case .watch:
            setGreenBackground(with: "lesson_button_title_watch".localized)
            hideIcon(true)
        case .subscribe:
            backgroundColor = TalkmeColors.greenLabels
            title.textColor = .white
            title.text = "lesson_button_title_watch".localized
            hideIcon(true)
        case .unsubscribe:
            backgroundColor = TalkmeColors.redLabels
            title.textColor = .white
            title.text = "lesson_button_title_cancel".localized
            hideIcon(true)
        case .edit:
            setClearBackground()
            hideIcon(false)
        }
    }

    private func setGreenBackground(with text: String) {
        backgroundColor = TalkmeColors.greenLabels
        layer.borderWidth = 0
        title.textColor = .white
        title.text = text
    }

    private func hideIcon(_ isHidden: Bool) {
        icon.isHidden = isHidden
        title.isHidden = !isHidden
    }

    private func setClearBackground() {
        backgroundColor = .clear
        layer.borderWidth = 1
        title.textColor = TalkmeColors.blueLabels
    }

    private func setUpConstraints() {
        addSubviews(title, icon)

        title.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }

        icon.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(16)
        }
    }

    private func setUpUI() {
        layer.cornerRadius = 7
        layer.borderColor = TalkmeColors.blueLabels.cgColor
        isUserInteractionEnabled = true
    }
}
