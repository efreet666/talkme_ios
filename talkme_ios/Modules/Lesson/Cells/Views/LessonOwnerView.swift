//
//  LessonOwnerView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 14.02.2021.
//

import UIKit
import Kingfisher
import RxSwift
import RxCocoa

final class LessonOwnerView: UIView {

    // MARK: - Public properties

    let flow = PublishRelay<Void>()
    let onSelectTeacher = PublishRelay<Void>()
    var bag = DisposeBag()

    private(set) lazy var avatarView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    // MARK: - Private Properties

    private let ownerName: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: 15)
        label.textColor = TalkmeColors.blackLabels
        label.numberOfLines = 2
        return label
    }()

    private let messageButton: AccountUnsubscribeButton = {
        let button = AccountUnsubscribeButton(buttonType: .message)
        button.fontSize = 15
        return button
    }()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        setUpConstraints()
        setUpView()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        avatarView.layer.cornerRadius = avatarView.bounds.width / 2
        messageButton.layer.cornerRadius = messageButton.bounds.height / 2
    }

    func configure(_ avatarURL: String, _ ownerName: String) {
        if let imageUrl = URL(string: avatarURL) {
            avatarView.kf.setImage(with: ImageResource(downloadURL: imageUrl, cacheKey: imageUrl.absoluteString))
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.18
        paragraphStyle.alignment = .left
        let fullNameSeparated = ownerName.split(separator: " ")
        if fullNameSeparated.count == 2 {
            let attributedString = NSMutableAttributedString(string: fullNameSeparated[0] + "\n" + fullNameSeparated[1])
            attributedString.addAttribute(
                NSAttributedString.Key.paragraphStyle,
                value: paragraphStyle,
                range: NSRange(location: 0, length: attributedString.length))
            self.ownerName.attributedText = attributedString
        } else {
            self.ownerName.text = ownerName
        }
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        addSubviews(avatarView, ownerName, messageButton)

        avatarView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(snp.trailing).dividedBy(20)
            make.size.equalTo(snp.height).dividedBy(UIScreen.isSE ? 1.4186 : 1.2727)
        }

        ownerName.snp.makeConstraints { make in
            make.centerY.equalTo(snp.bottom).dividedBy(UIScreen.isSE ? 2.9756 : 3.2183)
            make.leading.equalTo(snp.trailing).dividedBy(UIScreen.isSE ? 2.5423 : 2.4615)
        }

        messageButton.snp.makeConstraints { make in
            make.leading.equalTo(ownerName)
            make.trailing.equalToSuperview().dividedBy(UIScreen.isSE ? 1.0563 : 1.0406)
            make.height.equalToSuperview().dividedBy(UIScreen.isSE ? 3.6969 : 3.3333)
            make.bottom.equalToSuperview().dividedBy(UIScreen.isSE ? 1.196 : 1.2068)
        }
    }

    private func setUpView() {
        backgroundColor = TalkmeColors.grayOwnerViewBackgroud
        layer.cornerRadius = 9
    }

    private func bindUI() {
        messageButton.tap
            .map { _ in ()}
            .bind(to: flow)
            .disposed(by: bag)

        avatarView.rx.tapGesture()
            .when(.recognized)
            .map { _ in () }
            .bind(to: onSelectTeacher)
            .disposed(by: bag)
    }
}
