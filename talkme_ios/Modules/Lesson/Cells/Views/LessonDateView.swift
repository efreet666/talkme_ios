//
//  LessonDateView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 11.02.2021.
//

import UIKit

final class LessonDateView: UIView {

    // MARK: - Private Properties

    private let dateTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.grayAttributeTitle
        label.text = "lesson_attribute_date".localized
        return label
    }()

    private let dateView = IconAndTitlePairView()

    private let timeView = IconAndTitlePairView()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        backgroundColor = .white
        layer.cornerRadius = 7
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ time: String, date: String) {
        dateView.configure(.date(date))
        timeView.configure(.time(time))
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        addSubviews(dateTitleLabel, dateView, timeView)

        dateTitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(snp.trailing).dividedBy(UIScreen.isSE ? 8.7619 : 8.1481)
        }

        dateView.snp.makeConstraints { make in
            make.top.equalTo(dateTitleLabel.snp.bottom).offset(UIScreen.isSE ? 10 : 11)
            make.leading.equalTo(dateTitleLabel)
            make.centerY.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 14 : 16)
        }

        timeView.snp.makeConstraints { make in
            make.leading.equalTo(dateTitleLabel)
            make.top.equalTo(dateView.snp.bottom).offset(UIScreen.isSE ? 7 : 11)
            make.height.equalTo(dateView)
        }
    }
}
