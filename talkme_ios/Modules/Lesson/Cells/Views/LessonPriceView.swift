//
//  LessonPriceView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 12.02.2021.
//

import UIKit

final class LessonPriceView: UIView {

    enum Cost {
        case free
        case price(String)
    }

    // MARK: - Public properties

    private let priceTitle: UILabel = {
        let label = UILabel()
        label.font = .montserratFontRegular(ofSize: 13)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "lesson_attribute_price".localized
        return label
    }()

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = .white
        return label
    }()

    private let freeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "lesson_attribute_price_free".localized
        label.textColor = TalkmeColors.greenLabels
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 14)
        return label
    }()

    private let stackViewHorizontal: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = UIScreen.isSE ? 5 : 6
        return stackView
    }()

    private let stackViewVertical: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = UIScreen.isSE ? 4 : 6
        return stackView
    }()

    private let coinIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = UIImage(named: "coin")
        icon.contentMode = .scaleAspectFit
        return icon
    }()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        layer.cornerRadius = 7
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ price: Cost) {
        switch price {
        case .free:
            stackViewVertical.isHidden = true
            backgroundColor = TalkmeColors.greenPriceLabel
            freeLabel.isHidden = false
        case .price(let price):
            freeLabel.isHidden = true
            backgroundColor = TalkmeColors.purpleLessonPrice
            stackViewVertical.isHidden = false
            priceLabel.text = price
        }
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        stackViewHorizontal.addArrangedSubview(priceLabel)
        stackViewHorizontal.addArrangedSubview(coinIcon)
        stackViewVertical.addArrangedSubview(priceTitle)
        stackViewVertical.addArrangedSubview(stackViewHorizontal)
        addSubviews(stackViewVertical, freeLabel)

        stackViewVertical.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }

        coinIcon.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 18 : 19)
        }

        freeLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
