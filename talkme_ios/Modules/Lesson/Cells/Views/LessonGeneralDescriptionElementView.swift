//
//  LessonGeneralDescriptionElementView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.02.2021.
//

import UIKit

final class LessonGeneralDescriptionElementView: UIView {

    // MARK: - Private Properties

    private let icon: UIImageView = {
        let icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        return icon
    }()

    private let title: UILabel = {
        let label = UILabel()
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 10 : 14)
        label.textColor = TalkmeColors.grayAttributeTitle
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()

    private let valueDescription: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textAlignment = .center
        return label
    }()

    // MARK: - Initializers

    init(image: UIImage?, title: String, valueDescription: String, textColor: UIColor) {
        super.init(frame: .zero)
        setUpView()
        icon.image = image
        let attributedString = NSMutableAttributedString(string: title)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.14
        paragraphStyle.alignment = .center
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length))
        self.title.attributedText = attributedString
        self.valueDescription.text = valueDescription
        self.valueDescription.textColor = textColor
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setUpView() {
        backgroundColor = .white
        layer.cornerRadius = 5
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalCentering
        stackView.spacing = UIScreen.isSE ? 3 : 6
        stackView.addArrangedSubview(title)
        stackView.addArrangedSubview(valueDescription)
        addSubviews([icon, stackView])

        icon.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.size.equalTo(snp.height).dividedBy(UIScreen.isSE ? 3.1578 : 2.8367)
            make.centerY.equalTo(snp.bottom).dividedBy(UIScreen.isSE ? 3.9655 : 3.7066)
        }

        stackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(snp.bottom).dividedBy(UIScreen.isSE ? 1.455 : 1.404)
        }
    }
}
