//
//  IconAndTitlePairView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 12.02.2021.
//

import UIKit

final class IconAndTitlePairView: UIView {

    enum UseType {
        case date(String)
        case time(String)
    }

    // MARK: - Private Properties

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = TalkmeColors.lightGrayIcon
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return label
    }()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        setUpView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public properties

    func configure(_ useCase: UseType) {
        switch useCase {
        case .date(let value):
            titleLabel.text = value
            titleLabel.textColor = TalkmeColors.grayAttributeTitle
            iconImageView.image = UIImage(named: "calendar")?.withRenderingMode(.alwaysTemplate)
        case .time(let value):
            titleLabel.text = value
            titleLabel.textColor = TalkmeColors.redLessonLabel
            iconImageView.image = UIImage(named: "streamTime")?.withRenderingMode(.alwaysTemplate)
        }
    }

    private func setUpView() {
        addSubviews(iconImageView, titleLabel)

        iconImageView.snp.makeConstraints { make in
            make.leading.centerY.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? 11 : 14)
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).offset(UIScreen.isSE ? 9 : 11)
            make.trailing.centerY.equalToSuperview()
        }
    }
}
