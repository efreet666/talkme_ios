//
//  SimilarLessonView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 22.02.2021.
//

import UIKit

final class SimilarLessonView: UIView {

    // MARK: - Private Properties

    private let lessonNameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: 15)
        label.numberOfLines = 3
        label.textColor = TalkmeColors.white
        return label
    }()

    private let backgroundImage: UIImageView = {
        let iw = UIImageView()
        iw.layer.cornerRadius = 12
        iw.contentMode = .scaleAspectFill
        iw.clipsToBounds = true
        return iw
    }()

    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private let gradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [
            TalkmeColors.gradientMain.cgColor,
            TalkmeColors.secondGradientColor.cgColor
        ]
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer.transform = UIScreen.isSE
            ? CATransform3DMakeAffineTransform(
                CGAffineTransform(
                    a: 0.14,
                    b: -0.71,
                    c: 0.71,
                    d: 0.06,
                    tx: -0.06,
                    ty: 0.77
                )
            )
            : CATransform3DMakeAffineTransform(
                CGAffineTransform(
                    a: 0.11,
                    b: -0.44,
                    c: 0.44,
                    d: 0.04,
                    tx: 0.14,
                    ty: 0.64
                )
            )
        return layer
    }()

    private let priceLabel = StreamPriceView()

    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        label.textColor = .white
        label.textAlignment = .right
        return label
    }()

    private let durationLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        label.textColor = .white
        label.textAlignment = .right
        return label
    }()

    private let timerIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "clock")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private let startTime: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.warningColor
        return label
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(lessonModel: LiveStream) {
        let lessonCostIsZero = (lessonModel.cost ?? 0) == 0
        priceLabel.configure(viewType: lessonCostIsZero ? .similarLessonFree : .similarLessonPrice(price: String(lessonModel.cost ?? 0)))
        let attributedString = NSMutableAttributedString(string: lessonModel.name)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.09
        paragraphStyle.alignment = .left
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length))
        lessonNameLabel.attributedText = attributedString
        dateLabel.text = Formatters.convertDate3LettersMonth(lessonModel.date)
        let date = Date(timeIntervalSince1970: TimeInterval(lessonModel.date))
        startTime.text = Formatters.timeFormatter.string(from: date)
        durationLabel.text = Formatters.setUpHourAndMinWithoutZeroHoursLocalized(date: lessonModel.lessonTime)
        guard let tileImage = lessonModel.tileImage, let url = URL(string: tileImage) else { return }
        backgroundImage.kf.setImage(with: url)
        guard let avatar = lessonModel.owner.avatarUrl, let avatarUrl = URL(string: avatar) else {
            avatarImageView.image = UIImage(named: "noAvatar")
            return
        }
        avatarImageView.kf.setImage(with: avatarUrl)
    }

    // MARK: - Private Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        let insetModificator: CGFloat = (UIScreen.isSE ? -0.65 : -1.5)
        gradient.bounds = bounds.insetBy(dx: insetModificator * bounds.size.width, dy: insetModificator * bounds.size.height)
        gradient.position = center
    }

    private func initialSetup() {
        backgroundImage.layer.addSublayer(gradient)
        addSubviews(
            backgroundImage,
            lessonNameLabel,
            avatarImageView,
            priceLabel,
            dateLabel,
            durationLabel,
            timerIcon,
            startTime
        )

        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        avatarImageView.snp.makeConstraints { make in
            make.size.equalTo(snp.width).dividedBy(UIScreen.isSE ? 3.6666 : 3.3269)
            make.leading.equalTo(snp.trailing).dividedBy(UIScreen.isSE ? 16.5 : 17.3)
            make.top.equalTo(snp.bottom).dividedBy(UIScreen.isSE ? 19.0909 : 20.7857)
        }

        timerIcon.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 11, height: 12))
            make.bottom.equalTo(lessonNameLabel.snp.top).offset(-7)
            make.leading.equalTo(avatarImageView)
        }

        startTime.snp.makeConstraints { make in
            make.centerY.equalTo(timerIcon)
            make.leading.equalTo(timerIcon.snp.trailing).offset(5)
        }

        lessonNameLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarImageView)
            make.bottom.equalTo(priceLabel.snp.top).offset(UIScreen.isSE ? -9 : -11)
            make.width.equalToSuperview().dividedBy(UIScreen.isSE ? 1.0909 : 1.4297)
        }

        priceLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarImageView)
            make.width.equalTo(UIScreen.isSE ? 71 : 88)
            make.height.equalTo(UIScreen.isSE ? 25 : 32)
            make.bottom.equalToSuperview().dividedBy(UIScreen.isSE ? 1.0881 : 1.0659)
        }

        durationLabel.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 10 : 12)
            make.trailing.equalToSuperview().dividedBy(UIScreen.isSE ? 1.056 : 1.0613)
            make.bottom.equalToSuperview().dividedBy(UIScreen.isSE ? 1.1052 : 1.0818)
            make.top.equalTo(dateLabel.snp.bottom).offset(5)
        }

        dateLabel.snp.makeConstraints { make in
            make.trailing.equalTo(durationLabel)
            make.height.equalTo(durationLabel)
        }
    }
}
