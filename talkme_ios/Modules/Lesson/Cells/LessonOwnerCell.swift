//
//  LessonOwnerCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 14.02.2021.
//

import UIKit
import RxSwift
import RxCocoa

final class LessonOwnerCell: UITableViewCell {

    // MARK: - Private Properties

    private let ownerView = LessonOwnerView()

    // MARK: - Public properties

    private(set) lazy var flow = ownerView.flow
    private(set) lazy var onSelectTeacher = ownerView.onSelectTeacher
    private(set) var bag = DisposeBag()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpConstraints()
        setUpCell()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ avatarURL: String, _ ownerFullName: String) {
        ownerView.configure(avatarURL, ownerFullName)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    func setUpConstraints() {
        contentView.addSubview(ownerView)

        ownerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(ownerView.snp.width).dividedBy(UIScreen.isSE ? 2.459 : 2.7428)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 16 : 21)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 22 : 24)
        }
    }

    private func setUpCell() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
