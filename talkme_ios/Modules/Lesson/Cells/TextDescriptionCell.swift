//
//  TextDescriptionCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 09.02.2021.
//

import UIKit

final class TextDescriptionCell: UITableViewCell {

    // MARK: - Private Properties

    private let lessonTitle: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let lessonDescription: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.lessonTextDescription
        return label
    }()

    // MARK: - Initializers

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpLayout()
        setUpCell()
    }

    // MARK: - Public Methods

    func configure(title: String?, description: String?) {
        lessonTitle.text = title
        lessonDescription.text = description
    }

    // MARK: - Private Methods

    private func setUpLayout() {
        contentView.addSubviews([lessonTitle, lessonDescription])

        lessonTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            make.top.equalToSuperview()
        }

        lessonDescription.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 16)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.top.equalTo(lessonTitle.snp.bottom).offset(UIScreen.isSE ? 12 : 15)
        }
    }

    private func setUpCell() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
