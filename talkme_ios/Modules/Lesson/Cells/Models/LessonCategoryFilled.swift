//
//  LessonCategoryType.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.02.2021.
//

import Foundation
import UIKit

enum LessonCategoryFilledType: Int {
    case cooking = 14
    case fitness = 10
    case education = 71
    case languages = 49
    case other = 26
    case yourShow = 192
    case kids = 20
    case music = 184
    case business = 29
    case games = 211
    case learning = 2
    case entertainment = 7
    case courses = 181
    case chatting = 19
    case girls = 218
    case concerts = 17
    case travel = 27
    case nightlife = 23
    case masterClass = 600
    case sale = 579
    case car = 598
    case health = 595
    case mosStream = 786
    case talkMeStudio = 924
    case standUp = 926

    // MARK: - Public properties

    var color: UIColor {
        switch self {
        case .fitness:
            return TalkmeColors.turquoiseCategoryIcon
        case .learning, .concerts:
            return TalkmeColors.purpleCategoryIcon
        case .entertainment, .other:
            return TalkmeColors.cyanCategoryIcon
        case .chatting:
            return TalkmeColors.yellowCategoryIcon
        case .nightlife, .courses:
            return TalkmeColors.redCategoryIcon
        case .girls, .kids:
            return TalkmeColors.pinkCategoryIcon
        case .education:
            return TalkmeColors.blueCategoryIcon
        case .cooking:
            return TalkmeColors.greenCategoryIcon
        case .travel:
            return TalkmeColors.darkYellowCategoryIcon
        case .business:
            return TalkmeColors.darkBlueCategoryIcon
        case .languages:
            return TalkmeColors.darkCyanCategoryIcon
        case .music:
            return TalkmeColors.darkTurquoiseCategoryIcon
        case .yourShow, .talkMeStudio:
            return TalkmeColors.lightPurpleCategoryIcon
        case .games:
            return TalkmeColors.darkGreenCategoryIcon
        case .masterClass:
            return TalkmeColors.orangeViewBackground
        case .sale:
            return TalkmeColors.redCategoryIcon
        case .car:
            return TalkmeColors.blueCategoryIcon
        case .health:
            return TalkmeColors.greenCategoryIcon
        case .mosStream, .standUp:
            return TalkmeColors.redCategoryIcon
        }
    }

    var icon: UIImage? {
        switch self {
        case .fitness:
            return UIImage(named: "fitness")
        case .learning:
            return UIImage(named: "learning")
        case .concerts:
            return UIImage(named: "concerts")
        case .entertainment:
            return UIImage(named: "entertainment")
        case .other:
            return UIImage(named: "other")
        case .chatting:
            return UIImage(named: "chatting")
        case .nightlife:
            return UIImage(named: "nightlife")
        case .courses:
            return UIImage(named: "courses")
        case .girls:
            return UIImage(named: "girls")
        case .kids:
            return UIImage(named: "kids")
        case .education:
            return UIImage(named: "education")
        case .cooking:
            return UIImage(named: "cooking")
        case .travel:
            return UIImage(named: "travel")
        case .business:
            return UIImage(named: "business")
        case .languages:
            return UIImage(named: "languages")
        case .music:
            return UIImage(named: "music")
        case .yourShow:
            return UIImage(named: "yourShow")
        case .games:
            return UIImage(named: "games")
        case .sale:
            return UIImage(named: "sale")
        case .car:
            return UIImage(named: "car")
        case .mosStream:
            return UIImage(named: "mosStream")
        case .masterClass:
            return UIImage(named: "masterClass")
        case .health:
            return UIImage(named: "health")
        case .talkMeStudio:
            return UIImage(named: "talkMeStudio")
        case .standUp:
            return UIImage(named: "standUp")
        }
    }
}
