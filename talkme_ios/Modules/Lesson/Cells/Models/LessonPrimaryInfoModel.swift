//
//  LessonPrimaryInfoModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 13.02.2021.
//

import UIKit

struct LessonPrimaryInfoModel {
    let image: String
    let title: String
    let date: String
    let time: String
    let price: LessonPriceView.Cost
    let buttonState: LessonActionButton.State
}
