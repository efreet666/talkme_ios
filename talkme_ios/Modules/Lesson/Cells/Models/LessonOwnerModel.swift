//
//  LessonOwnerModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 14.02.2021.
//

struct LessonOwnerModel {
    let id: Int
    let fullName: String
    let avatarURL: String
}
