//
//  LessonGeneralDescriptionElementModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.02.2021.
//

import UIKit

enum LessonGeneralDescriptionElementType {
    case category(LessonGeneralDescriptionElementModel, Int)
    case duration(LessonGeneralDescriptionElementModel)
    case numberOfParticipants(LessonGeneralDescriptionElementModel)

    // MARK: - Public properties

    var model: LessonGeneralDescriptionElementModel {
        switch self {
        case .category(let model, _), .duration(let model), .numberOfParticipants(let model):
            return model
        }
    }

    var color: UIColor {
        switch self {
        case .category(_, let id):
            guard let color = LessonCategoryFilledType(rawValue: id)?.color else {
                return UIColor.black
            }
            return color
        case .duration:
            return TalkmeColors.turquoiseAttributeIcon
        case .numberOfParticipants:
            return TalkmeColors.purpleAttributeIcon
        }
    }
}

struct LessonGeneralDescriptionElementModel {
    let icon: UIImage?
    let title: String
    let value: String

    // todo: Implement init with server response model
}
