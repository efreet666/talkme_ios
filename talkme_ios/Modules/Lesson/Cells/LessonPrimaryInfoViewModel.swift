//
//  LessonPrimaryInfoViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 13.02.2021.
//

import RxSwift
import RxCocoa

final class LessonPrimaryInfoViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let flowComplaint = PublishRelay<Void>()
    let flow = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let infoModel: LessonPrimaryInfoModel

    // MARK: - Initializers

    init(_ response: LessonDetailResponse) {
        func stateSelection(isOwner: Bool, isFree: Bool, isSubscribe: Bool, isStarted: Bool) -> LessonActionButton.State {
            if isOwner {
                return isStarted ? .watch : .edit
            }
            if !isFree {
                if isStarted {
                    return .watch
                }
                return isSubscribe ? .unsubscribe : .pay
            } else {
                if isStarted {
                    return .watch
                }
                return isSubscribe ? .unsubscribe : .subscribe
            }
        }
        let date = Formatters.convertDateDayMonthYear(from: response.date)
        let time = Formatters.convertTime(seconds: response.date)
        let isFree = response.cost == 0
        let cost = Formatters.spaceAfterThousand.string(from: NSNumber(value: response.cost)) ?? ""
        let state = stateSelection(
            isOwner: response.isOwner,
            isFree: isFree,
            isSubscribe: response.isSubscribe,
            isStarted: response.isStarted
        )
        let model = LessonPrimaryInfoModel(
            image: response.tileImage ?? "",
            title: response.name,
            date: date,
            time: time,
            price: isFree ? .free : .price(cost),
            buttonState: state
        )
        infoModel = model
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonPrimaryInfoCell) {
        cell.configure(infoModel)
        cell.flow
            .bind(to: flow)
            .disposed(by: cell.bag)

        cell.flowComplaint
            .bind(to: flowComplaint)
            .disposed(by: cell.bag)
    }
}
