//
//  CreateLessonViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import Kingfisher
import RxCocoa

enum CreateLessonScreenType {
    case create
    case edit
}

final class CreateLessonViewModel {

    enum Flow {
        case onMyLessons
    }

    // MARK: - Public Properties

    private(set) lazy var dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    private(set) var bag = DisposeBag()
    var isCorrectDate: Bool = false
    let onButtonTapped = PublishRelay<Void>()
    let flow = PublishRelay<Flow>()
    let onEndDropDown = PublishRelay<Void>()
    let clearWarningPicture = PublishRelay<Void>()
    let height = PublishRelay<Void>()
    let screenType: CreateLessonScreenType
    let onPickImage = PublishRelay<UIImage>()
    let scrollTo = PublishRelay<IndexPath>()

    // MARK: - Private Properties

    private lazy var timeString = streamData.lessonTime ??
    Formatters.setupLessonDate(streamData.date) ??
    Formatters.setupTodayTime()
    private var categories: [Category] = []
    private var subcategories: [Category] = []
    private var lessonsArray: [Lesson] = []
    private var defaultCategory = 7
    private var itemId = 0
    private var categoryOffsetX: CGFloat = 0
    private let service: MainServiceProtocol
    private let streamData: StreamData
    private let lessonService: LessonsServiceProtocol

    // MARK: - Initializers

    init(id: Int?, service: MainServiceProtocol, screenType: CreateLessonScreenType, lessonService: LessonsServiceProtocol) {
        self.service = service
        self.screenType = screenType
        self.lessonService = lessonService
        streamData = .init(id: id)
    }

    // MARK: - Public Methods

    func getCategories(id: Int) {
        service
            .categories()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.categories = response
                    self?.getSubCategories(for: id)
                case .error(let error):
                    print(error.localizedDescription)
                }
            }
            .disposed(by: bag)
    }

    func getLessonId() {
        guard let id = streamData.id else {
            self.setupViewModels()
            return
        }
        service
            .lessonID(id: id)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    guard let categoryId = response.list?.subCategory?.parent,
                          let subCategory = response.list?.subCategory?.name else { return }
                    self.streamData.category = categoryId
                    self.streamData.subCategory = subCategory
                    self.streamData.lessonName = response.list?.name
                    self.streamData.avatarUrl = response.list?.tileImage
                    self.streamData.lessonDescription = response.list?.description
                    self.streamData.lessonTime = response.list?.lessonTime
                    self.streamData.date = response.list?.date
                    self.streamData.cost = response.list?.cost
                    self.getCategories(id: categoryId)
                case .error(let error):
                    self.setupViewModels()
                    print(error.localizedDescription)
                }
            }
            .disposed(by: bag)
    }

    func chekCurrentTime() {
        isCorrectDate = checkCorrectDate()
        guard !isCorrectDate else { return }
    }

    func fetchStreams() {
        Single.zip(lessonService.live(), lessonService.myBroadcasts(page: 100))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success((_, let lessons)):
                    guard let lesson = lessons.lessons,
                          let lessonsSoon = lessons.lessonsSoon else { return }
                    self.lessonsArray.append(contentsOf: lessonsSoon)
                    self.lessonsArray.append(contentsOf: lesson)
                    self.chekCurrentTime()
                    guard !self.lessonsArray.isEmpty else {
                        if self.isCorrectDate {
                            self.setStartTime()
                        }
                        return
                    }
                    for lesson in self.lessonsArray {
                        let streamStartTime = Double(self.streamData.lessonStartTime ?? "")
                        let streamEndTime = (
                            Double(self.streamData.lessonStartTime ?? "") ?? 0.0)
                        + (self.setupTimeToseconds(time: self.streamData.lessonDuration)
                        )
                        let lessonEndMyBroadcasts = self.setupTimeToseconds(time: lesson.lessonTime) + (lesson.date ?? 0.0)

                        if streamStartTime ?? 0.0 >= lesson.date ?? 0.0 && streamStartTime ?? 0.0 <= lessonEndMyBroadcasts ||
                            streamEndTime >= lesson.date ?? 0.0 && streamEndTime <= lessonEndMyBroadcasts {
                            switch self.screenType {
                            case .create:
                                self.isCorrectDate = false
                            case .edit:
                                if !(self.streamData.id == lesson.id) {
                                    self.isCorrectDate = false
                                }
                            }
                        }
                    }
                    guard self.isCorrectDate else { return }
                    self.setStartTime()
                    self.lessonsArray = []
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func setupTimeToseconds(time: String? = nil) -> Double {
        let hoursString = time?.components(separatedBy: ":")
        let hour = (Double((hoursString?[0] ?? "")) ?? 0) * 60
        let seconds = (Double((hoursString?[1] ?? "")) ?? 0)
        let result = (hour + seconds) * 60
        return result
    }

    // MARK: - Private Methods

    private func setupViewModels() {
        let lessonCategoryVM = LessonCategoryViewModel(
            title: "create_lesson_category".localized,
            model: categories,
            activeCategory: streamData.category ?? defaultCategory,
            offset: categoryOffsetX
        )

        let lessonNameVM = LessonNameViewModel(
            title: "create_lesson_name_lesson".localized,
            count: "0/30",
            text: streamData.lessonName
        )

        let lessonPictureVM = LessonPictureViewModel(
            title: "create_lesson_photo_lesson".localized,
            imageString: streamData.avatarUrl
        )

        let lessonDescriptionVM = LessonDescriptionViewModel(
            title: "create_lesson_description".localized,
            count: "0/1700",
            text: streamData.lessonDescription
        )

        let lessonTimeVM = LessonTimeViewModel(
            title: "create_lesson_stream_time_date".localized,
            placeholderTime: "create_lesson_time".localized,
            placeholderDate: "create_lesson_date".localized,
            textTime: timeString,
            textDate: Formatters.lessonDateString(streamData.date) ?? Formatters.setupTodayDate()
        )

        let lessonRateVM = LessonRateViewModel(
            title: "create_lesson_price".localized,
            count: "",
            text: ""
        )

        let showNextButtonVM = ShowNextButtonCellViewModel()

        let dataItems: [AnyTableViewCellModelProtocol] = [
            lessonCategoryVM,
            lessonNameVM,
            lessonPictureVM,
            lessonDescriptionVM,
            lessonTimeVM,
            lessonRateVM,
            showNextButtonVM
        ]

        self.dataItems.accept(dataItems)

        setupData(
            lessonCategoryVM: lessonCategoryVM,
            lessonNameVM: lessonNameVM,
            lessonPictureVM: lessonPictureVM,
            lessonDescriptionVM: lessonDescriptionVM,
            lessonTimeVM: lessonTimeVM,
            lessonRateVM: lessonRateVM)

        onPickImage(lessonPictureVM: lessonPictureVM)

        lessonTimeVM
            .isOn
            .bind { isOn in
                showNextButtonVM.buttonStatus.accept(!isOn)
            }
            .disposed(by: lessonTimeVM.bag)

        showNextButtonVM
            .tapRelay
            .bind { [weak self, weak lessonNameVM, weak lessonPictureVM] _ in
                guard let self = self,
                      let lessonNameVM = lessonNameVM,
                      let lessonPictureVM = lessonPictureVM else { return }
                if self.streamData.category == nil {
                    self.streamData.category = self.defaultCategory
                    self.streamData.subCategory = "new"
                }
                self.setStartTime()
                self.fetchStreams()
                guard !self.showErrors(
                    for: self.streamData,
                    lessonNameVM: lessonNameVM,
                    lessonPictureVM: lessonPictureVM,
                    showNextButtonVM: showNextButtonVM) else { return }
                self.updateLesson(showNextButtonVM: showNextButtonVM)
            }
            .disposed(by: showNextButtonVM.bag)
    }

    private func getSubCategories(for category: Int) {
        service
            .subCategory(parent: String(category))
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.subcategories = response
                    self?.setupViewModels()
                case .error(let error):
                    print(error.localizedDescription)
                }
            }
            .disposed(by: bag)
    }

    private func setupData(
        lessonCategoryVM: LessonCategoryViewModel,
        lessonNameVM: LessonNameViewModel,
        lessonPictureVM: LessonPictureViewModel,
        lessonDescriptionVM: LessonDescriptionViewModel,
        lessonTimeVM: LessonTimeViewModel,
        lessonRateVM: LessonRateViewModel
    ) {
        lessonCategoryVM
            .onItemSelected
            .bind { [weak self] item, offset in
                guard let self = self else { return }
                self.categoryOffsetX = offset
                if self.itemId != item.id {
                    self.itemId = item.id
                    self.getSubCategories(for: item.id)
                    self.streamData.category = item.id
                    self.streamData.subCategory = "\(item.id)"
                }
            }
            .disposed(by: lessonCategoryVM.bag)

        lessonNameVM
            .textRelay
            .bind { [weak self] text in
                self?.streamData.lessonName = text
            }
            .disposed(by: lessonNameVM.bag)

        lessonPictureVM
            .onPickImage
            .subscribe(onNext: { [weak self] image in
                self?.streamData.lessonImage = image
            })
            .disposed(by: lessonPictureVM.bag)

        lessonDescriptionVM
            .textRelay
            .bind { [weak self] text in
                self?.streamData.lessonDescription = text
            }
            .disposed(by: lessonDescriptionVM.bag)

        lessonTimeVM
            .lessonTime
            .bind { [weak self] date in
                self?.streamData.lessonTime = Formatters.timeFormatter.string(from: date)
            }
            .disposed(by: lessonTimeVM.bag)

        lessonTimeVM
            .lessonDate
            .bind { [weak self] date in
                self?.streamData.lessonDate = Formatters.dateFormatterDefault.string(from: date)
            }
            .disposed(by: lessonTimeVM.bag)

        lessonRateVM
            .textRelay
            .bind { [weak self] text in
                guard let cost = Int(text ?? "") else {
                    self?.streamData.cost = 0
                return }
                self?.streamData.cost = cost
            }
            .disposed(by: lessonRateVM.bag)
    }

    private func showErrors(
        for model: StreamData,
        lessonNameVM: LessonNameViewModel,
        lessonPictureVM: LessonPictureViewModel,
        showNextButtonVM: ShowNextButtonCellViewModel
    ) -> Bool {

        var hasError = false
        var hasImage = streamData.lessonImage != nil || streamData.avatarUrl != nil

        if streamData.lessonName?.isEmpty ?? true ||
           !hasImage ||
           streamData.category == nil {
            hasError = true
            showNextButtonVM.warningRelay.accept(())
            let indexPath = IndexPath(row: 1, section: 0)
            self.scrollTo.accept(indexPath)
        }
        return hasError
    }

    private func onPickImage(lessonPictureVM: LessonPictureViewModel) {
        lessonPictureVM
            .photoButtonTap
            .bind(to: onButtonTapped)
            .disposed(by: lessonPictureVM.bag)

        onPickImage
            .do(onNext: { [weak self] in
                self?.streamData.lessonImage = $0
            })
                .bind(to: lessonPictureVM.onPickImage)
                .disposed(by: bag)

                clearWarningPicture
                .bind(to: lessonPictureVM.onClearWarning)
                .disposed(by: bag)
    }

    private func createLesson(showNextButtonVM: ShowNextButtonCellViewModel) {
        showNextButtonVM
            .buttonIsEnabled
            .accept(false)
        service
            .createLesson(request: CreateLessonRequest(streamData: streamData))
            .subscribe { [weak self] event in
                switch event {
                case .success(let result):
                    self?.flow.accept(.onMyLessons)
                    guard let descriptionError = result.description?.first else { return }
                    AlertControllerHelper.showSimpleOkAlert(title: descriptionError)
                case .error(let error):
                    print(error)
                }
                showNextButtonVM
                    .buttonIsEnabled
                    .accept(true)
            }
            .disposed(by: bag)
    }

    private func updateLesson(showNextButtonVM: ShowNextButtonCellViewModel) {
        guard let id = streamData.id else {
            self.createLesson(showNextButtonVM: showNextButtonVM)
            return
        }
        showNextButtonVM
            .buttonIsEnabled
            .accept(false)
        service
            .updateLesson(request: CreateLessonRequest(streamData: streamData), id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.onMyLessons)
                case .error(let error):
                    print(error)
                }
                showNextButtonVM
                    .buttonIsEnabled
                    .accept(true)
            }
            .disposed(by: bag)
    }

    private func checkCorrectDate() -> Bool {
        var streamStartTime = Double(streamData.lessonStartTime ?? "0") ?? 0
        streamStartTime -= Date.diffrenceBetweenRealTime
        let nowTime = Date().timeIntervalSince1970
        return streamStartTime >= (nowTime - 60)
    }

    private func setStartTime() {
        let date = Date()
        var newDataString: String?
        var newTimeString: String?
        newDataString = streamData.lessonDate == nil
        ? Formatters.lessonDateToString(self.streamData.date)
        : streamData.lessonDate
        newTimeString = streamData.lessonTime == nil
        ? Formatters.setupLessonDate(self.streamData.date)
        : streamData.lessonTime
        let currentDateString = Formatters.dateFormatterDefault.string(from: date)
        let currentTimeString = Formatters.timeFormatter.string(from: date)
        let timeString = (newDataString ?? currentDateString) + " " + (newTimeString ?? currentTimeString) + ":00"
        let lessonTime = Formatters.createLessonTimeFormatter.date(from: timeString)
        let timeStamp = lessonTime?.timeIntervalSince1970 ?? 0
        let lessonTimeStamp = String(format: "%.0f", timeStamp + Date.diffrenceBetweenRealTime)
        self.streamData.lessonStartTime = lessonTimeStamp
    }
}
