//
//  CreateLessonViewController.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import RxCocoa
import RxKeyboard

final class CreateLessonViewController: UIViewController {

    // MARK: - Private properties

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.bounces = false
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.keyboardDismissMode = .onDrag
        tableView.backgroundColor = TalkmeColors.white
        tableView.delaysContentTouches = false
        tableView.registerCells(
            withModels:
            LessonCategoryViewModel.self,
            LessonNameViewModel.self,
            LessonPictureViewModel.self,
            LessonDescriptionViewModel.self,
            LessonTimeViewModel.self,
            LessonRateViewModel.self,
            ShowNextButtonCellViewModel.self
        )
        return tableView
    }()

    private let dismissButton: UIBarButtonItem = {
        let dismissButton = UIButton(type: .custom)
        let navigationButton = UIBarButtonItem(customView: dismissButton)
        dismissButton.setImage(UIImage(named: "dismissController"), for: .normal)
        return navigationButton
    }()

    private let imagePicker = UIImagePickerController()
    private let viewModel: CreateLessonViewModel
    private let bag = DisposeBag()

    // MARK: - Init

    init(viewModel: CreateLessonViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        bindVM()
        bindKeyboard()
        imagePicker.delegate = self
        viewModel.screenType == .create
            ? viewModel.getCategories(id: 7)
            : viewModel.getLessonId()
        bindGrow()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideCustomTabBarLiveButton(true)
        setupNavigation()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        viewModel.onButtonTapped
            .bind { [weak self] _ in
                self?.showImagePickerAlert()
            }
            .disposed(by: bag)

        viewModel.scrollTo
            .bind { [weak self] value in
                self?.tableView.scrollToRow(at: value, at: .bottom, animated: true)
            }
            .disposed(by: viewModel.bag)
    }

    private func bindGrow() {
        viewModel.height
            .bind { [weak self] _ in
                self?.updateTableOnTextEnter()
            }
            .disposed(by: bag)
    }

    private func setupTableView() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func updateTableOnTextEnter() {
        UIView.performWithoutAnimation {
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }

    private func setupNavigation() {
        customNavigationController?.style = .plainGradient(title: "create_lesson_navigation_title".localized)
    }

    private func showImagePickerAlert() {
        viewModel.onEndDropDown.accept(())
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "profile_setting_photo_library".localized, style: .default
        ) { [weak self] _ in
            guard let self = self else { return }
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true)
        })
        alert.addAction(UIAlertAction(title: "profile_setting_camera".localized, style: .default
        ) { [weak self] _ in
            guard let self = self else { return }
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true)
        })
        alert.addAction(UIAlertAction(title: "profile_setting_cancel".localized, style: .cancel))

        self.present(alert, animated: true)
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let isHide = keyboardVisibleHeight == 0
                let insets: CGFloat = UIScreen.isSE ? 20 : 120
                self.tableView.contentInset.bottom = isHide ? keyboardVisibleHeight + insets : keyboardVisibleHeight
                self.tableView.snp.updateConstraints { make in
                    make.bottom.equalToSuperview().inset(keyboardVisibleHeight)
                }
                if !isHide {
                    self.tableView.scrollTo(edge: .all, animated: true)
                }
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
}

extension CreateLessonViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let imagePicked = info[.originalImage] as? UIImage {
            viewModel.onPickImage.accept(imagePicked)
            viewModel.clearWarningPicture.accept(())
        }
        picker.dismiss(animated: true)
    }
}
