//
//  LessonCreateEditCoordinator.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 19.02.2021.
//

import RxSwift
import RxCocoa

final class LessonCreateEditCoordinator {

    enum Flow {
        case completeCreationEdit
    }

    // MARK: - Public properties

    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let mainService = MainService()
    private let lessonService = LessonsService()
    private weak var navigationController: UINavigationController?

    // MARK: - Public Methods

    func start(_ navigationController: UINavigationController?, lessonID: Int?, type: CreateLessonScreenType) {
        self.navigationController = navigationController
        let model = CreateLessonViewModel(id: lessonID, service: mainService, screenType: type, lessonService: lessonService)
        let vc = CreateLessonViewController(viewModel: model)
        bindEditViewControllerModel(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func bindEditViewControllerModel(_ model: CreateLessonViewModel) {
        model.flow
            .bind { [weak self] event in
                switch event {
                case .onMyLessons:
                    self?.flow.accept(.completeCreationEdit)
                }
            }.disposed(by: model.bag)
    }
}
