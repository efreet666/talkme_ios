//
//  CreateLessonMenuModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import UIKit

enum CreateLessonType {
    case category
    case subcategory
    case lessonPicture
    case lessonName
    case lessonDescription
}

struct CreateLessonModel {

    let data: [CreateLessonType] = [
        .category,
        .subcategory,
        .lessonPicture,
        .lessonName,
        .lessonDescription
    ]
}
