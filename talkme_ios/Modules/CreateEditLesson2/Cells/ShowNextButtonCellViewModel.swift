//
//  ShowNextButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 18.01.2021.
//

import RxCocoa
import RxSwift

final class ShowNextButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let tapRelay = PublishRelay<Void>()
    let buttonStatus = BehaviorRelay<Bool>(value: true)
    let buttonIsEnabled = PublishRelay<Bool>()
    let warningRelay = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: ShowNextButtonTableViewCell) {
        cell.buttonTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.tapRelay.accept(())
        }.disposed(by: cell.bag)

        buttonStatus
            .bind { isActive in
                cell.setStatusButton(isActive)
            }
            .disposed(by: bag)

        buttonIsEnabled
            .bind(to: cell.buttonIsEnabled)
            .disposed(by: cell.bag)

        warningRelay
            .bind { [weak cell] _ in
                cell?.setupWarning(type: .requiredFields)
            }
            .disposed(by: cell.bag)
    }
}
