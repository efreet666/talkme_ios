//
//  LessonTimeViewModel.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 01.07.2022.
//

import RxSwift
import RxCocoa

final class LessonTimeViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let lessonDate = PublishRelay<Date>()
    let lessonTime = PublishRelay<Date>()
    let error = PublishRelay<CreateLessonError>()
    let bag = DisposeBag()
    let isOn = BehaviorRelay<Bool>(value: true)

    // MARK: - Private Properties

    private let title: String
    private let placeholderTime: String
    private let placeholderDate: String
    private var textTime: String?
    private var textDate: String?

    // MARK: - Initializers

    init(title: String, placeholderTime: String, placeholderDate: String, textTime: String?, textDate: String?) {
        self.title = title
        self.placeholderTime = placeholderTime
        self.placeholderDate = placeholderDate
        self.textTime = textTime
        self.textDate = textDate
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonTimeViewCell) {
        cell.configure(
            title: title,
            placeholderTime: placeholderTime,
            placeholderDate: placeholderDate,
            textTime: textTime,
            textDate: textDate)
        bindCell(cell)
    }

    func bindCell(_ cell: LessonTimeViewCell) {
        cell
            .lessonTime
            .bind(to: lessonTime)
            .disposed(by: cell.bag)

        cell
            .lessonDate
            .bind(to: lessonDate)
            .disposed(by: cell.bag)

        cell
            .isOn
            .bind { [weak self] isOn in
                self?.isOn.accept(isOn)
            }
            .disposed(by: cell.bag)
    }
}
