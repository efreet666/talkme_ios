//
//  CreateLessonTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import RxCocoa
import GrowingTextView

final class LessonDescriptionTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var sizeChanged = ControlEvent(events: heightChanged)
    private(set) lazy var lessonDescriptionText = lessonDescriptionTextView.rx.text
    private(set) var bag = DisposeBag()
    let beginEditingDescription = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let heightChanged = PublishRelay<Void>()

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 244, green: 246, blue: 255)
        view.layer.cornerRadius = 6
        return view
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return label
    }()

    private let lessonDescriptionTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.backgroundColor = UIColor(red: 244, green: 246, blue: 255)
        textView.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 16)
        textView.textColor = TalkmeColors.black
        textView.attributedPlaceholder = NSAttributedString(
            string: "create_lesson_description_placeholder".localized,
            attributes:
                [NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
                 NSAttributedString.Key.font: UIFont.montserratFontMedium(ofSize: UIScreen.isSE ? 14 : 16)
                ])
        textView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 5, right: 0)
        textView.textContainer.lineFragmentPadding = 0.0
        textView.maxLength = 250
        return textView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        lessonDescriptionTextView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, count: String, text: String?) {
        titleLabel.text = title
        lessonDescriptionTextView.text = text
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
    
    // MARK: - Private Methods

    private func setupLayout() {
        containerView.addSubview(lessonDescriptionTextView)
        contentView.addSubviews([titleLabel, containerView])

        containerView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).inset(-10)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview()
            make.height.greaterThanOrEqualTo(UIScreen.isSE ? 48 : 55)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 25 : 30)
            make.leading.trailing.equalToSuperview().inset(16)
        }

        lessonDescriptionTextView.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.top).offset(10)
            make.leading.trailing.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
            make.bottom.equalToSuperview().inset(17)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension LessonDescriptionTableCell: GrowingTextViewDelegate {

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.numberOfLines() > 10 && !text.isBackspace() {
            return false
        }
        return true
    }

    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        heightChanged.accept(())
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        self.beginEditingDescription.accept(true)
    }
}
