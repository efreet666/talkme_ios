//
//  LessonPictureViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 16.01.2021.
//

import RxCocoa
import RxSwift

final class LessonPictureViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let photoButtonTap = PublishRelay<Void>()
    let warningRelay = PublishRelay<Void>()
    let warningRelayClear = PublishRelay<Void>()
    let onClearWarning = PublishRelay<Void>()
    let onPickImage = PublishRelay<UIImage>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private let imageString: String?

    // MARK: - Initializers

    init(title: String, imageString: String?) {
        self.title = title
        self.imageString = imageString
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonPictureTableCell) {
        bindCell(cell)
        cell.configure(title: title, imageString: imageString)
    }

    func bindCell(_ cell: LessonPictureTableCell) {
        cell
            .onPhotoTap
            .bind(to: photoButtonTap)
            .disposed(by: cell.bag)

        onPickImage
            .bind { [weak cell] _ in
                cell?.removeGradient()
            }
            .disposed(by: bag)

        onPickImage
            .bind(to: cell.onPickImage)
            .disposed(by: bag)
    }
}
