//
//  LessonCategoryTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.01.2021.
//

import RxSwift
import RxCocoa
import collection_view_layouts
import UIKit

final class LessonCategoryTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    let onItemSelected = PublishRelay<(Category, CGFloat)>()
    let onClearTFSubCategoryText = PublishRelay<Void>()
    let activeCategoryName = PublishRelay<Int>()

    // MARK: - Private Properties

    private var cellSizes = [CGSize]()
    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return label
    }()

    private let customLayout: TagsLayout = {
        let layout = TagsLayout()
        layout.scrollDirection = .horizontal
        layout.cellsPadding = ItemsPadding(horizontal: 8, vertical: -5)
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: customLayout)
        collection.backgroundColor = .clear
        collection.registerCells(withModels: CategoryCollectionCellViewModel.self)
        collection.showsHorizontalScrollIndicator = false
        return collection
    }()

    private let errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.error
        label.font = .montserratFontMedium(ofSize: 11)
        label.textAlignment = .center
        return label
    }()

    private let firstIndicator: PageIndicatorView = {
        let indicator = PageIndicatorView()
        return indicator
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        customLayout.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, items: PublishRelay<[AnyCollectionViewCellModelProtocol]>, index: Int?, offset: CGFloat) {
        items
            .do(afterNext: { [weak self] _ in
                let selectedIndexPath = IndexPath(item: index ?? 1, section: 0)
                self?.collectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: [])
            })
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        titleLabel.text = title
        bindCollectionView()

        self.collectionView.contentOffset.x = offset
    }

    func setupWarning(type: CreateLessonError) {
        self.errorLabel.text = type.title
    }

    func setupCategoryNames(names: [String]) {
        cellSizes = names.map { item -> CGSize in
            let width = Double(self.collectionView.bounds.width)
            var size = UIFont.systemFont(ofSize: 13).sizeOfString(string: item, constrainedToWidth: width)
            size.width += UIScreen.isSE ? 50 : 80
            size.height += UIScreen.isSE ? 36 : 40
            return size
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func bindCollectionView() {
        collectionView.rx
            .modelSelected(CategoryCollectionCellViewModel.self)
            .bind { [weak self] item in
                guard let self = self else { return }
                self.onItemSelected.accept((item.categoryModel, self.collectionView.contentOffset.x))
                self.onClearTFSubCategoryText.accept(())
                self.activeCategoryName.accept(item.categoryModel.id)
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, isRequiredImage, collectionView, errorLabel])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(20)
            make.leading.equalToSuperview().inset(16)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(8)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(6)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(13)
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().inset(16)
            make.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 150 : 170)
        }

        errorLabel.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension LessonCategoryTableCell: LayoutDelegate {

    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.row]
    }
}
