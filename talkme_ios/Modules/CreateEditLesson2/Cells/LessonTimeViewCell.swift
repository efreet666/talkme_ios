//
//  LessonTimeViewCell.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 01.07.2022.
//

import RxSwift
import RxCocoa
import UIKit

final class LessonTimeViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var isOn = checkMarkTimeView.isOn
    private(set) var onGetLessonsCount = PublishRelay<Void>()
    private(set) var bag = DisposeBag()
    let lessonTime = PublishRelay<Date>()
    let lessonDate = PublishRelay<Date>()
    let onLoadText = PublishRelay<String>()

    // MARK: - Private Properties

    private var currentTime: String {
        Formatters.timeFormatter.string(from: self.timePicker.date)
    }

    private var currentDate: String {
        Formatters.lessonDateformatterNew.string(from: self.datePicker.date)
    }

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return label
    }()

    private let checkMarkTimeView = CheckMarkView(type: .now)
    private let lessonTimeTF = LessonCommonCopyTextField()
    private let lessonDateTF = LessonCommonCopyTextField()
    private let separatorView = UIView()

    private let timeButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named: "timeIcon"), for: .normal)
        return button
    }()

    private let dateButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage(named: "calendar"), for: .normal)
        button.setTitleColor(TalkmeColors.white, for: .normal)
        return button
    }()

    private let datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let minDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        datePicker.minimumDate = minDate
        return datePicker
    }()

    private let timePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .time
        datePicker.minuteInterval = 1
        datePicker.locale = Locale(identifier: "en_GB")
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let minDate = Calendar.current.date(byAdding: .hour, value: 0, to: Date())
        let maxDate = Calendar.current.date(byAdding: .hour, value: 24, to: Date())
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        return datePicker
    }()

    private lazy var doneButtonDate = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)
    private lazy var doneButtonTime = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)
    private var isActive = false

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, placeholderTime: String, placeholderDate: String, textTime: String?, textDate: String?) {
        titleLabel.text = title
        lessonTimeTF.placeholder = placeholderTime
        updateTimePicker(text: textTime)
        bindUI()
        lessonDateTF.placeholder = placeholderDate
        updateDatePicker(text: textDate)
        lessonTimeTF.isUserInteractionEnabled = false
        lessonDateTF.isUserInteractionEnabled = false
    }

    func setLessonDate(date: Date) {
        if Calendar.current.isDateInToday(date) {
            let minDate = Calendar.current.date(byAdding: .hour, value: 0, to: Date())
            let maxDate = Calendar.current.date(byAdding: .hour, value: 24, to: Date())
            timePicker.minimumDate = minDate
            timePicker.maximumDate = maxDate
        } else {
            timePicker.minimumDate = nil
            timePicker.maximumDate = nil
        }
    }

    func setupTimeToseconds(time: String? = nil) -> Int {
        if let time = time {
            let hoursString = time.components(separatedBy: ":")
            let hour = (Int((hoursString[0])) ?? 0) * 60
            let seconds = (Int((hoursString[1])) ?? 0)
            let result = (hour + seconds) * 60
            return result
        } else {
            let timeString = (Formatters.timeFormatter.string(from: self.timePicker.date))
            let hoursString = timeString.components(separatedBy: ":")
            let countOfSeconds = ((Int(hoursString[0]) ?? 0) * 60 + (Int(hoursString[1]) ?? 0) ) * 60
            return countOfSeconds
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.timeButton.alpha = self.isActive ? 1 : 0.3
        self.dateButton.alpha = self.isActive ? 1 : 0.3
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubviews(
            separatorView,
            titleLabel,
            lessonDateTF,
            dateButton,
            lessonTimeTF,
            timeButton,
            checkMarkTimeView
        )

        titleLabel.snp.makeConstraints { label in
            label.top.equalToSuperview().offset(UIScreen.isSE ? 25 : 30)
            label.leading.equalToSuperview().inset(16)
        }

        checkMarkTimeView.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel)
            make.trailing.equalToSuperview().inset(14)
            make.height.equalTo(26)
            make.width.equalTo(100)
        }

        lessonDateTF.snp.makeConstraints { textView in
            textView.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 14 : 18)
            textView.leading.equalToSuperview().inset(16)
            textView.trailing.equalTo(separatorView.snp.leading).inset(-5.5)
            textView.height.equalTo(UIScreen.isSE ? 47 : 55)
            textView.bottom.equalToSuperview().inset(1)
        }

        lessonTimeTF.snp.makeConstraints { textView in
            textView.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 14 : 18)
            textView.trailing.equalToSuperview().inset(16)
            textView.leading.equalTo(separatorView.snp.trailing).inset(-5.5)
            textView.height.equalTo(UIScreen.isSE ? 47 : 55)
            textView.bottom.equalToSuperview().inset(1)
        }

        separatorView.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
            make.size.equalTo(1)
        }

        timeButton.snp.makeConstraints { button in
            button.centerY.equalTo(lessonTimeTF.snp.centerY)
            button.trailing.equalTo(lessonTimeTF).inset(UIScreen.isSE ? 18 : 20)
            button.size.equalTo(UIScreen.isSE ? 15 : 19)
        }

        dateButton.snp.makeConstraints { button in
            button.centerY.equalTo(lessonDateTF.snp.centerY)
            button.trailing.equalTo(lessonDateTF).inset(UIScreen.isSE ? 18 : 20)
            button.size.equalTo(UIScreen.isSE ? 15 : 19)
        }
    }

    private func updateTimePicker(text: String?) {
        guard let text = text,
              let date = Formatters.timeFormatter.date(from: text) else {
            return
        }
        timePicker.setDate(date, animated: true)
    }

    private func updateDatePicker(text: String?) {
        guard let text = text,
              let date = Formatters.lessonDateformatterNew.date(from: text) else {
            return
        }
        datePicker.setDate(date, animated: true)
    }

    private func bindUI() {
        checkMarkTimeView.rx.tapGesture().when(.recognized)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.changeState()
            }
            .disposed(by: bag)

        timeButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.changeState()
                self.lessonTimeTF.becomeFirstResponder()
            }
            .disposed(by: bag)

        doneButtonTime.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.lessonTimeTF.text = self.currentTime
                self.setupToolbar()
                self.contentView.endEditing(true)
                self.lessonTime.accept(self.timePicker.date)
            }
            .disposed(by: bag)

        dateButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.changeState()
                self.lessonDateTF.becomeFirstResponder()
            }
            .disposed(by: bag)

        doneButtonDate.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.lessonDateTF.text = self.currentDate
                self.lessonTimeTF.text = self.currentTime
                self.contentView.endEditing(true)
                self.lessonDate.accept(self.datePicker.date)
                self.setLessonDate(date: self.datePicker.date)
            }
            .disposed(by: bag)
    }

    private func changeState() {
        isActive.toggle()
        isOn.accept(isActive)
        checkMarkTimeView.setValue(isActive)
        lessonTimeTF.isUserInteractionEnabled = isActive
        lessonDateTF.isUserInteractionEnabled = isActive
        timePicker.date = Date()
        datePicker.date = Date()
        lessonTimeTF.text = isActive ? currentTime : ""
        lessonDateTF.text = isActive ? currentDate : ""
        setNeedsLayout()
    }

    private func setupUI() {
        lessonTimeTF.inputView = timePicker
        lessonDateTF.inputView = datePicker
        setupToolbar()
        setupToolbarTime()
    }

    private func setupToolbarTime() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneButtonTime], animated: true)
        lessonTimeTF.inputAccessoryView = toolBar
    }

    private func setupToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneButtonDate], animated: true)
        lessonDateTF.inputAccessoryView = toolBar
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
