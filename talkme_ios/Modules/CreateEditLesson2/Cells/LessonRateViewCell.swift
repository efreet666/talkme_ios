//
//  File.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 01.07.2022.
//

import RxSwift
import RxCocoa
import GrowingTextView
import UIKit

final class LessonRateViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var lessonRateText = lessonRateTextView.rx.text
    private(set) lazy var isOn = checkMarkRateView.isOn
    private(set) var bag = DisposeBag()
    let beginEditingRate = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 244, green: 246, blue: 255)
        view.layer.cornerRadius = 6
        return view
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return label
    }()

    private let checkMarkRateView = CheckMarkView(type: .free)

    private let lessonRateTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.backgroundColor = UIColor(red: 244, green: 246, blue: 255)
        textView.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 16)
        textView.textColor = TalkmeColors.black
        textView.attributedPlaceholder = NSAttributedString(
            string: "0".localized,
            attributes:
                [NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
                 NSAttributedString.Key.font: UIFont.montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
                ])
        textView.textContainerInset = UIEdgeInsets(
            top: 10,
            left: 8,
            bottom: 10,
            right: 0)
        textView.textContainer.lineFragmentPadding = 0.0
        textView.maxLength = 30
        textView.isUserInteractionEnabled = false
        textView.keyboardType = .numberPad
        return textView
    }()

    private let coinImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "coin")
        return imageView
    }()

    private var isActive = false

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        lessonRateTextView.delegate = self
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, count: String, text: String?) {
        titleLabel.text = title
        lessonRateTextView.text = text
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        coinImage.alpha = isActive ? 1 : 0.3
    }

    // MARK: - Private Methods

    private func bindUI() {
        checkMarkRateView.rx.tapGesture().when(.recognized)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.changeState()
            }
            .disposed(by: bag)

        coinImage.rx.tapGesture().when(.recognized)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.changeState()
            }
            .disposed(by: bag)
    }

    private func changeState() {
        isActive.toggle()
        isOn.accept(isActive)
        checkMarkRateView.setValue(isActive)
        lessonRateTextView.text.removeAll()
        lessonRateTextView.isUserInteractionEnabled = isActive
        if isActive {
            lessonRateTextView.becomeFirstResponder()
        }
        setNeedsLayout()
    }

    private func setupLayout() {
        containerView.addSubview(lessonRateTextView)
        contentView.addSubviews(titleLabel, containerView, checkMarkRateView, coinImage)

        containerView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 14 : 18)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 22 : 28)
            make.height.greaterThanOrEqualTo(UIScreen.isSE ? 48 : 55)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 25 : 30)
            make.leading.equalToSuperview().inset(16)
        }

        checkMarkRateView.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel)
            make.trailing.equalToSuperview().inset(16)
            make.height.equalTo(26)
            make.width.equalTo(130)
        }

        lessonRateTextView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(14)
            make.centerY.equalToSuperview()
        }

        coinImage.snp.makeConstraints { make in
            make.centerY.equalTo(containerView.snp.centerY)
            make.right.equalTo(containerView.snp.right).offset(-10)
            make.size.equalTo(24)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension LessonRateViewCell: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        self.beginEditingRate.accept(true)
    }
}
