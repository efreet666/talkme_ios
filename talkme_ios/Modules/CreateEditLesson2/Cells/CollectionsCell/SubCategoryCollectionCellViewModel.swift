//
//  SubCategoryCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 30.01.2021.
//

import UIKit

final class SubCategoryCollectionCellViewModel: CollectionViewCellModelProtocol {

   // MARK: - Public Properties

    let categoryModel: Category

    // MARK: - Initializers

    init(categoryModel: Category) {
        self.categoryModel = categoryModel
    }

    // MARK: - Public Methods

    func configure(_ cell: SubCategoryCollectionCell) {
        cell.configure(categoryModel: categoryModel)
    }
}
