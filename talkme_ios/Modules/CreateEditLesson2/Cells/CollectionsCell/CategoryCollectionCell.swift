//
//  CategoryCollectionCell.swift
//  talkme_ios
//
//  Created by 1111 on 30.01.2021.
//

import RxSwift
import UIKit

final class CategoryCollectionCell: UICollectionViewCell {

    // MARK: - Public Properties

    override var isSelected: Bool {
        didSet {
            selectedCell(isSelected)
        }
    }

    // MARK: - Private properties

    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIScreen.isSE ? 16 : 20
        view.clipsToBounds = true
        return view
    }()

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
        label.textAlignment = .center
        return label
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(categoryModel: Category) {
        titleLabel.text = categoryModel.name
        iconImageView.image = LessonCategoryType(rawValue: categoryModel.id)?.whiteBackgroundCategoriesImage
        DispatchQueue.main.async {
            self.containerView.backgroundColor = LessonCategoryType(rawValue: categoryModel.id)?.color.withAlphaComponent(0.08)
        }
    }

    // MARK: - Private Method

    private func selectedCell(_ isSelected: Bool) {
        titleLabel.textColor = isSelected ? .white : .black
        if isSelected {
            DispatchQueue.main.async {
                self.containerView.gradientBackground(
                    from: TalkmeColors.gradientBlue,
                    to: TalkmeColors.gradientLightBlue,
                    direction: .rightToLeft
                )
            }
        } else {
            removeGradientLayer(containerView)
        }
    }

    private func removeGradientLayer(_ view: UIView) {
        guard let layers = view.layer.sublayers else { return }
        for aLayer in layers where aLayer is CAGradientLayer {
            aLayer.removeFromSuperlayer()
        }
    }

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.addSubviews(iconImageView, titleLabel)

        containerView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 32 : 40)
            make.leading.trailing.equalToSuperview()
            make.centerY.centerX.equalToSuperview()
        }

        iconImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(4)
            make.centerY.equalToSuperview()
            make.size.equalTo(32)
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).inset(-8)
            make.centerY.equalTo(iconImageView)
        }
    }
}
