//
//  LessonSelectionIndicatorCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 04.02.2021.
//

import RxSwift
import RxCocoa

final class LessonSelectionIndicatorCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let indicator = PageIndicatorView()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Methods

    func configure(tag: Int) {
        self.indicator.setViewActiveWith(tag: tag)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(indicator)

        indicator.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
        separatorInset.right = .greatestFiniteMagnitude
    }
}
