//
//  CategoryCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 30.01.2021.
//

import UIKit

final class CategoryCollectionCellViewModel: CollectionViewCellModelProtocol {

   // MARK: - Public Properties

    let categoryModel: Category

    // MARK: - Initializers

    init(categoryModel: Category) {
        self.categoryModel = categoryModel
    }

    // MARK: - Public Methods

    func configure(_ cell: CategoryCollectionCell) {
        cell.configure(categoryModel: categoryModel)
    }
}
