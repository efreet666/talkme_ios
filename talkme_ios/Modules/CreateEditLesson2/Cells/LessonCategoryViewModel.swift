//
//  LessonCategoryViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.01.2021.
//

import RxCocoa
import RxSwift

final class LessonCategoryViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let bag = DisposeBag()
    let dataItemsRelay = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let onItemSelected = PublishRelay<(Category, CGFloat)>()
    let warningRelay = PublishRelay<Void>()
    let onClearTFSubCategoryText = PublishRelay<Void>()

    // MARK: - Private Properties

    private var dataItems: [AnyCollectionViewCellModelProtocol] = []
    private var cellItems: [Category] = []
    private var activeCategory: Int?
    private var selectedIndex: Int?
    private let title: String
    private let offset: CGFloat

    // MARK: - Initializers

    init(
        title: String,
        model: [Category],
        activeCategory: Int?,
        offset: CGFloat
    ) {
        self.activeCategory = activeCategory
        self.title = title
        self.offset = offset
        let items: [AnyCollectionViewCellModelProtocol] = model.map { CategoryCollectionCellViewModel(categoryModel: $0) }
        cellItems = model
        dataItems = items
        dataItemsRelay.accept(items)
    }

    // MARK: - Public Methods

    func update(models: [Category], activeCategory: Int?) {
        self.activeCategory = activeCategory
        let items: [AnyCollectionViewCellModelProtocol] = models.map { CategoryCollectionCellViewModel(categoryModel: $0) }
        cellItems = models
        dataItems = items
        dataItemsRelay.accept(items)
    }

    func configure(_ cell: LessonCategoryTableCell) {
        selectedIndex = cellItems.firstIndex(where: { (self.activeCategory ?? 7) == $0.id })

        cell.configure(title: title, items: dataItemsRelay, index: selectedIndex, offset: offset)

        cell.setupCategoryNames(names: cellItems.map { $0.name ?? "" })

        warningRelay
            .bind { [weak cell] _ in
                cell?.setupWarning(type: .category)
            }
            .disposed(by: cell.bag)

        cell
            .onItemSelected
            .bind(to: onItemSelected)
            .disposed(by: cell.bag)

        cell
            .onClearTFSubCategoryText
            .bind(to: onClearTFSubCategoryText)
            .disposed(by: cell.bag)

        cell
            .activeCategoryName
            .bind { [weak self] id in
                self?.activeCategory = id
            }
            .disposed(by: cell.bag)

        dataItemsRelay.accept(dataItems)
    }
}
