//
//  LessonPictureTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 16.01.2021.
//

import RxCocoa
import RxSwift
import Kingfisher

final class LessonPictureTableCell: UITableViewCell {

    // MARK: Public Properties

    private(set) lazy var onPhotoTap = photoButton.rx.tap
    private(set) lazy var onPickImage = lessonPicture.rx.image
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))
    private let photoButton = UIButton()

    private let cameraPicture: UIImageView = {
        let iamge = UIImageView()
        iamge.image = UIImage(named: "camera")
        iamge.contentMode = .scaleAspectFit
        return iamge
    }()

    private let lessonPicture: UIImageView = {
        let iamge = UIImageView()
        iamge.contentMode = .scaleAspectFill
        iamge.clipsToBounds = true
        iamge.layer.cornerRadius = 10
        return iamge
    }()

    private let photoTitle: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, imageString: String?) {
        photoTitle.text = title
        if let imageUrl = URL(string: imageString ?? "") {
            lessonPicture.kf.setImage(with: imageUrl)
        } else {
            setGradient()
        }
    }

    func removeGradient() {
        cameraPicture.image = UIImage()
        removeGradientLayer(lessonPicture)
    }

    func setGradient() {
        DispatchQueue.main.async {
            self.lessonPicture.gradientBackground(
                from: TalkmeColors.gradientBlue,
                to: TalkmeColors.gradientLightBlue,
                direction: .rightToLeft
            )
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }
    
    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubviews(photoTitle, isRequiredImage, lessonPicture, photoButton)
        lessonPicture.addSubviews(cameraPicture)

        photoTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 25 : 30)
            make.leading.equalToSuperview().inset(16)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(photoTitle.snp.trailing).offset(8)
            make.centerY.equalTo(photoTitle.snp.centerY).offset(1)
            make.width.height.equalTo(6)
        }

        lessonPicture.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(photoTitle.snp.bottom).offset(UIScreen.isSE ? 9 : 12)
            make.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 197 : 237)
        }

        cameraPicture.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
            make.size.equalTo(40)
        }

        photoButton.snp.makeConstraints { make in
            make.centerY.centerX.equalTo(cameraPicture)
            make.height.equalTo(lessonPicture.snp.height)
            make.width.equalTo(170)
        }
    }
    
    private func removeGradientLayer(_ view: UIView) {
        guard let layers = view.layer.sublayers else { return }
        for aLayer in layers where aLayer is CAGradientLayer {
            aLayer.removeFromSuperlayer()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
