//
//  LessonRateViewModel.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 01.07.2022.
//

import RxSwift
import RxCocoa

final class LessonRateViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let warningRelay = PublishRelay<Void>()
    let bag = DisposeBag()
    let beginEditingRate = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let title: String
    private let count: String
    private var text: String?

    // MARK: - Initializers

    init(title: String, count: String, text: String?) {
        self.title = title
        self.count = count
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonRateViewCell) {
        cell.configure(title: title, count: count, text: text)

        cell
            .lessonRateText
            .do(onNext: { [weak self] in
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .beginEditingRate
            .bind(to: beginEditingRate)
            .disposed(by: cell.bag)
    }
}
