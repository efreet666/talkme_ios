//
//  LessonNameTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import RxCocoa
import GrowingTextView
import UIKit

final class LessonNameTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var lessonNameText = lessonNameTextView.rx.text
    private(set) var bag = DisposeBag()
    let beginEditingName = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 244, green: 246, blue: 255)
        view.layer.cornerRadius = 6
        return view
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return label
    }()

    private let lessonNameTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.backgroundColor = UIColor(red: 244, green: 246, blue: 255)
        textView.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 16)
        textView.textColor = TalkmeColors.black
        textView.attributedPlaceholder = NSAttributedString(
            string: "create_lesson_name_lesson".localized,
            attributes:
                [NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
                 NSAttributedString.Key.font: UIFont.montserratFontMedium(ofSize: UIScreen.isSE ? 14 : 16)
                ])
        textView.textContainerInset = UIEdgeInsets(
            top: 10,
            left: 8,
            bottom: 10,
            right: 0)
        textView.textContainer.lineFragmentPadding = 0.0
        textView.maxLength = 30
        return textView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        lessonNameTextView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, count: String, text: String?) {
        titleLabel.text = title
        lessonNameTextView.text = text
    }

    // MARK: - Private Methods

    private func setupLayout() {
        containerView.addSubview(lessonNameTextView)
        contentView.addSubviews([titleLabel, isRequiredImage, containerView])

        containerView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 9 : 13)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview()
            make.height.greaterThanOrEqualTo(UIScreen.isSE ? 48 : 55)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(8)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(6)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 5 : 8)
            make.leading.equalToSuperview().inset(16)
        }

        lessonNameTextView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(14)
            make.centerY.equalToSuperview().offset(3)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension LessonNameTableCell: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        self.beginEditingName.accept(true)
    }
}
