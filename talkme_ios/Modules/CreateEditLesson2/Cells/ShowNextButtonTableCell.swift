//
//  ShowNextButtonTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 18.01.2021.
//

import RxSwift
import RxCocoa

final class ShowNextButtonTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var buttonTap = saveButton.rx.tapGesture().when(.recognized)
    private(set) lazy var buttonIsEnabled = saveButton.rx.isUserInteractionEnabled
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let saveButton = GradientView(type: .startStream)

    private let errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.errorLabel
        label.font = .montserratFontMedium(ofSize: 13)
        label.textAlignment = .center
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupStyleView()
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setStatusButton(_ isActive: Bool) {
        saveButton.setTitle(isActive)
    }

    func setupWarning(type: CreateLessonError) {
        self.errorLabel.text = type.title
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }
    
    // MARK: - Private Methods

    private func setupStyleView() {
        saveButton.layer.cornerRadius = 7
    }

    private func setupLayout() {
        contentView.addSubviews([saveButton, errorLabel])

        errorLabel.snp.makeConstraints { make in
            make.centerX.equalTo(saveButton.snp.centerX)
            make.bottom.equalTo(saveButton.snp.top).inset(-14)
        }

        saveButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 23 : 28)
            make.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 42 : 50)
            make.leading.trailing.equalToSuperview().inset(16)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
        separatorInset.right = .greatestFiniteMagnitude
    }
}
