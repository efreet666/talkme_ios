//
//  PriceView.swift
//  talkme_ios
//
//  Created by 1111 on 28.01.2021.
//

import UIKit

enum PriceViewType {
    case priceToCreateLesson
    case totalPrice

    var title: String {
        switch self {
        case .priceToCreateLesson:
            return  "create_lesson_price_view".localized
        case .totalPrice:
            return  "create_lesson_price_finally".localized
        }
    }

    var font: UIFont {
        switch self {
        case .priceToCreateLesson:
            return .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 16)
        case .totalPrice:
            return .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        }
    }

    var numberOfLines: Int {
        switch self {
        case .priceToCreateLesson:
            return 3
        case .totalPrice:
            return 1
        }
    }
}

final class PriceView: UIView {

    // MARK: - Private properties

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.grayDescription
        label.font = .montserratBold(ofSize: 17)
        label.textAlignment = .center
        return label
    }()

    private let moneyLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 25 : 30)
        label.textColor = TalkmeColors.greenLabels
        label.textAlignment = .right
        return label
    }()

    private let priceView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = TalkmeColors.grayButtonBorder.cgColor
        view.backgroundColor = .clear
        return view
    }()

    private let coinImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "coin"))
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let priceStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .equalCentering
        sv.spacing = 6
        return sv
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = TalkmeColors.white
        layer.cornerRadius = 9
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        priceView.layer.cornerRadius = priceView.frame.height / 2
    }

    // MARK: - Public methods

    func configure(type: PriceViewType, price: String) {
        priceLabel.text = type.title
        priceLabel.font = type.font
        priceLabel.numberOfLines = type.numberOfLines
        moneyLabel.text = price
    }

    // MARK: - Private methods

    private func setupLayout() {
        priceStack.addArrangedSubview(moneyLabel)
        priceStack.addArrangedSubview(coinImageView)
        priceView.addSubview(priceStack)
        addSubviews([priceLabel, priceView])

        priceLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(50)
            make.top.equalToSuperview().offset(UIScreen.isSE ? 18 : 24)
        }

        priceView.snp.makeConstraints { make in
            make.top.equalTo(priceLabel.snp.bottom).offset(8)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 21 : 27)
            make.centerX.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 46 : 59)
            make.width.equalTo(UIScreen.isSE ? 185 : 240)
        }

        priceStack.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }

        coinImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 18 : 23)
        }
    }
}
