//
//  CheckMarkView.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 04.07.2022.
//

import Foundation
import UIKit
import RxCocoa

final class CheckMarkView: UIView {

    enum TypeView {
        case now
        case free

        var title: String {
            switch self {
            case .now:
                return "create_lesson_now".localized
            case .free:
                return "create_lesson_free_price".localized
            }
        }
    }

    private let checkMarkView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 4, green: 147, blue: 250)
        view.layer.borderColor = UIColor(red: 4, green: 147, blue: 250).cgColor
        view.isUserInteractionEnabled = true
        view.layer.borderWidth = 1.5
        view.layer.cornerRadius = 4
        return view
    }()

    private let checkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "lonlyCheck")
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .montserratFontRegular(ofSize: 16)
        label.isUserInteractionEnabled = true
        label.textAlignment = .right
        return label
    }()

    let isOn = BehaviorRelay<Bool>(value: true)

    init(type: TypeView) {
        titleLabel.text = type.title
        super.init(frame: .zero)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setValue(_ isActive: Bool) {
        checkMarkView.backgroundColor = isActive ? .clear : UIColor(red: 4, green: 147, blue: 250)
        checkImageView.isHidden = isActive
        isOn.accept(isActive)
    }

    private func addConstraints() {
        addSubviews(checkMarkView, titleLabel)
        checkMarkView.addSubviews(checkImageView)

        checkMarkView.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
            make.size.equalTo(26)
        }

        checkImageView.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
            make.size.equalTo(18)
        }

        titleLabel.snp.makeConstraints { make in
            make.centerY.equalTo(checkMarkView)
            make.trailing.equalTo(checkMarkView.snp.leading).inset(-12)
        }
    }
}
