//
//  GreenIndicatiorView.swift
//  talkme_ios
//
//  Created by 1111 on 28.01.2021.
//

import UIKit

final class RoundedIndicatorView: UIView {

    // MARK: - Private properties

    private let isActiveView = UIView()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 1
        layer.borderColor = TalkmeColors.grayTimeSlider.cgColor
        backgroundColor = .clear
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        isActiveView.layer.cornerRadius = isActiveView.frame.height / 2
    }

    // MARK: - Public methods

    func setOn(_ isOn: Bool) {
        isActiveView.backgroundColor = isOn ? TalkmeColors.greenLabels : .clear
        layer.borderColor = isOn ? TalkmeColors.greenLabels.cgColor : TalkmeColors.grayTimeSlider.cgColor
    }

    func setColor(_ color: UIColor) {
        layer.borderColor = color.cgColor
        isActiveView.backgroundColor = color
    }

    // MARK: - Private methods

    private func setupLayout() {
        addSubview(isActiveView)

        isActiveView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 10 : 13)
            make.center.equalToSuperview()
        }
    }
}
