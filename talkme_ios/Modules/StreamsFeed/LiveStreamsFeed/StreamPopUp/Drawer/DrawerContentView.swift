//
//  DrawerContentView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 23.09.2020.
//  Copyright © 2020 organization. All rights reserved.
//

import UIKit
import RxCocoa

protocol DrawerContentView: UIView {
    var viewHeight: BehaviorRelay<CGFloat> { get }
    var onDismiss: (() -> Void)? { get set }
    func updateLayout(height: CGFloat)
}
