//
//  FadeAnimator.swift
//  talkme_ios
//
//  Created by Elina Efremova on 23.09.2020.
//  Copyright © 2020 organization. All rights reserved.
//

import UIKit

final class FadeAnimator: NSObject {

    enum TransitionType {
        case present
        case dismiss
    }

    // MARK: - Properties

    private let type: TransitionType
    private let duration: TimeInterval
    private let contentView: UIView

    // MARK: - Init

    init(type: TransitionType, duration: TimeInterval = 0.3, contentView: UIView) {
        self.type = type
        self.duration = duration
        self.contentView = contentView
        super.init()
    }
}

// MARK: - UIViewControllerAnimatedTransitioning

extension FadeAnimator: UIViewControllerAnimatedTransitioning {

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to) else { return }
        var transVC = fromVC as? DrawerControllerProtocol
        if type == .present {
            transVC = toVC as? DrawerControllerProtocol
            transitionContext.containerView.insertSubview(toVC.view, aboveSubview: fromVC.view)
            transVC?.contentView.transform = CGAffineTransform(
                translationX: 0,
                y: transVC?.contentHeight ?? 0)
        }

        let duration: TimeInterval = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            UIView.setAnimationCurve(.linear)
            if self.type == .present {
                toVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                transVC?.contentView.transform = .identity
            } else {
                fromVC.view.backgroundColor = .clear
                transVC?.contentView.transform = CGAffineTransform(translationX: 0, y: transVC?.contentHeight ?? 0)
            }
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        duration
    }
}
