//
//  DrawerViewController.swift
//  talkme_ios
//
//  Created by Elina Efremova on 23.09.2020.
//  Copyright © 2020 organization. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol DrawerControllerProtocol: UIViewController, UIViewControllerTransitioningDelegate {
    var contentView: UIView { get }
    var contentHeight: CGFloat { get }
}

class DrawerViewController<T: DrawerContentView>: UIViewController,
    UIViewControllerTransitioningDelegate,
    UIGestureRecognizerDelegate,
    UIScrollViewDelegate {

    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
    let disposeBag = DisposeBag()
    var clousure: (() -> Void)?

    // MARK: - Private properties

    private let interactor = DrawerAnimationInteractor()
    private let content: T

    // MARK: - Init

    init(contentView: T) {
        content = contentView
        super.init(nibName: nil, bundle: nil)
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        addDismissAction()
        addPanGesture()
        configureContent()
        bindKeyboard()
    }

    // MARK: - Private methods

    private func configureContent() {
        view.addSubview(content)
        content.clipsToBounds = true
        content.onDismiss = { [weak self] in self?.dismissVC() }
        updateSize(height: contentHeight)

        content.viewHeight
            .asDriver(onErrorJustReturn: 0)
            .drive(onNext: { [weak self] in self?.updateSize(height: $0) })
            .disposed(by: disposeBag)
    }

    private func addDismissAction() {
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        tapGR.delegate = self
        let dismissTouchReceivingView = UIView(frame: view.bounds)
        dismissTouchReceivingView.addGestureRecognizer(tapGR)
        view.addSubview(dismissTouchReceivingView)
    }

    private func addPanGesture() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        pan.delegate = self
        content.addGestureRecognizer(pan)
    }

    private func updateSize(height: CGFloat) {
        let width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        let frameWidth = view.frame.width
        self.content.frame = CGRect(
            x: frameWidth > width ? (frameWidth - width) / 2 : 0,
            y: self.view.frame.height - height,
            width: width,
            height: height)
        self.content.setNeedsLayout()
    }

    // MARK: - Actions

    @objc private func dismissVC() {
        clousure?()
        dismiss(animated: true, completion: nil)
    }

    @objc private func handlePan(_ sender: UIPanGestureRecognizer) {
        let percentThreshold: CGFloat = 0.4
        let translation = sender.translation(in: contentView)
        let verticalMovement = translation.y / contentHeight
        let downwardMovement = max(verticalMovement, 0)
        var progress = min(downwardMovement, 1)
        progress *= 0.7
        switch sender.state {
        case .began:
            interactor.hasStarted = true
            dismiss(animated: true, completion: nil)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish ? interactor.finish() : interactor.cancel()
        default:
            break
        }
    }

    private func bindKeyboard() {
        let initialHeight = contentHeight
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let height = keyboardVisibleHeight / 4 * 3 // raise the keyboard just above the button
                self.content.viewHeight.accept(
                    keyboardVisibleHeight == 0
                        ? initialHeight
                        : self.content.viewHeight.value + height
                )
                self.content.updateLayout(height: height)
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: disposeBag)
    }

    // MARK: - UIGestureRecognizerDelegate

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer is UITapGestureRecognizer { return true }
        guard let pan = gestureRecognizer as? UIPanGestureRecognizer,
            let panView = pan.view else { return false }
        let velocity = pan.velocity(in: panView)
        let isVertical = abs(velocity.y) > abs(velocity.x)
        return isVertical
    }

    // MARK: - UIViewControllerTransitioningDelegate

    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        FadeAnimator(type: .present, contentView: contentView)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        FadeAnimator(type: .dismiss, contentView: contentView)
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            interactor.hasStarted ? interactor : nil
    }
}

// MARK: - DrawerControllerProtocol

extension DrawerViewController: DrawerControllerProtocol {
    var contentView: UIView { content }
    var contentHeight: CGFloat { content.viewHeight.value }
}
