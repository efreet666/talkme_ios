//
//  DrawerAnimationInteractor.swift
//  talkme_ios
//
//  Created by Elina Efremova on 23.09.2020.
//  Copyright © 2020 organization. All rights reserved.
//

import UIKit

final class DrawerAnimationInteractor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}
