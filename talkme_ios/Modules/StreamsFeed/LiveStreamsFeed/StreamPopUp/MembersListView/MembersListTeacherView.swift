//
//  MembersListTeacherView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.04.2021.
//

import RxSwift
import RxCocoa
import OpenTok
import CollectionKit

final class MembersListTeacherView: UIView {

    private(set) lazy var reloadButtonTapped = reloadButton.rx.tapGesture().when(.recognized)
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let membersLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 16 : 18)
        label.text = "stream_feed_members".localized
        label.textColor = TalkmeColors.grayMembersLabel
        return label
    }()

    let reloadButton = ReloadButtomPopUp(buttonType: .reloadTeacherView)

    private let countLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    private let collectionView: CollectionView = {
        let collectionView = CollectionView()
        collectionView.bounces = false
        collectionView.showsVerticalScrollIndicator = true
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()

    private let microButton: UIButton = {
        let button = UIButton()
        button.imageView?.contentMode = .scaleAspectFit
        button.imageView?.clipsToBounds = true
        button.setImage(UIImage(named: "microfonOn"), for: .normal)
        return button
    }()

    private var session: OTSession?
    private var dataSource = ArrayDataSource<MemberWhoBoughtCameraModel>()
    private let onUpdateAudio = PublishRelay<AlteredStream>()
    private let onUpdateVideo = PublishRelay<AlteredStream>()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupUI(camerasList: SubscriberForPopup) {
        self.session = camerasList.session
        let currentCount = String(camerasList.subscribersArray.count)
        let currentAttrString = NSAttributedString(string: currentCount, attributes: [.foregroundColor: TalkmeColors.white,
                                                                        .font: UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 16 : 18 )])
        let maxCount = "/50"
        let maxAttrString = NSAttributedString(string: maxCount, attributes: [.foregroundColor: TalkmeColors.streamGrayLabel,
                                                                .font: UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 12)])
        countLabel.attributedText = currentAttrString + maxAttrString

        dataSource.data.removeAll()
        camerasList.subscribersArray.forEach { model in
            let dataItem = MemberWhoBoughtCameraModel(memberModel: model, session: camerasList.session, isOwner: camerasList.isOwner)
            dataSource.data.append(dataItem)
        }

        let memberInfoSource = ClosureViewSource(viewGenerator: { _, _ -> MemberInfoCollectionCellView in
            return MemberInfoCollectionCellView(frame: .zero)
        }, viewUpdater: { [weak self] (view: MemberInfoCollectionCellView, dataItem: MemberWhoBoughtCameraModel, _) in
            view.configure(memberModel: dataItem.memberModel, session: dataItem.session, isOwner: dataItem.isOwner)

            self?.onUpdateAudio
                .bind { [weak view] model in
                    guard dataItem.memberModel.subscriber.stream == model.subscriber.stream else { return }
                    guard let isMicroOn = model.isMicroOn else {
                        view?.updateAudioState(hasAudio: model.subscriber.stream?.hasAudio ?? false)
                        return }
                    view?.updateAudioState(hasAudio: isMicroOn)
                }
                .disposed(by: view.bag)

            self?.onUpdateVideo
                .bind { [weak view] model in
                    guard dataItem.memberModel.subscriber.stream == model.subscriber.stream else { return }
                    let isMicroButtonTapped = model.isMicroOn != nil
                    let hasVideo = isMicroButtonTapped
                        ? model.subscriber.stream?.hasVideo ?? false
                        : model.subscriberHasVideo ?? false
                    let videoState = LearnerVideoState(hasVideo: hasVideo, subscriberView: model.subscriber.view)
                    view?.updateVideoState(videoState: videoState)
                }
                .disposed(by: view.bag)
        })

        let provider = BasicProvider(
            dataSource: dataSource,
            viewSource: memberInfoSource,
            sizeSource: ClosureSizeSource(sizeSource: { _, _, _ in
                return CGSize(
                    width: UIScreen.width / 3.7,
                    height: UIScreen.width / 2.17
                )
            })
        )
        provider.layout = FlowLayout(lineSpacing: 22, interitemSpacing: UIScreen.isSE ? 16 : 18)
        collectionView.provider = provider
    }

    func addNewLearner(learner: CurrentSubscriber) {
        dataSource.data.append(MemberWhoBoughtCameraModel(memberModel: learner, session: session, isOwner: true))
    }

    func removeLearner(learner: CurrentSubscriber) {
        dataSource.data.enumerated().forEach { index, member in
            guard member.memberModel == learner else { return }
            dataSource.data.remove(at: index)
        }
    }

    func updateMemberVideoState(model: AlteredStream) {
        onUpdateVideo.accept(model)
    }

    func updateMemberAudioState(model: AlteredStream) {
        onUpdateAudio.accept(model)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([membersLabel, reloadButton, countLabel, collectionView, microButton])

        membersLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(6)
            make.leading.equalToSuperview().inset(10)
        }

        countLabel.snp.makeConstraints { make in
            make.width.equalTo(45)
            make.leading.equalTo(10)
            make.top.equalTo(membersLabel.snp.bottom).offset(0)
        }

        reloadButton.snp.makeConstraints { make in
            make.trailing.equalTo(microButton.snp.leading).offset(-13)
            make.leading.equalTo(membersLabel.snp.trailing).offset(UIScreen.isSE ? 30 : 70)
            make.top.equalTo(10)
            make.height.equalTo(UIScreen.isSE ? 34 : 36)
        }

        microButton.snp.makeConstraints { make in
            make.top.equalTo(10)
            make.trailing.equalToSuperview().inset(9)
            make.height.equalTo(reloadButton)
        }

        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(12)
            make.top.equalTo(reloadButton.snp.bottom).offset(20)
            make.bottom.equalToSuperview()
        }
    }
}
