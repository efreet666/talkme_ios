//
//  MemberWhoBoughtCameraModel.swift
//  talkme_ios
//
//  Created by 1111 on 10.03.2021.
//

import OpenTok

struct MemberWhoBoughtCameraModel {
    let memberModel: CurrentSubscriber
    let session: OTSession?
    let isOwner: Bool
}
