//
//  AudioAndVideoImagesView.swift
//  talkme_ios
//
//  Created by 1111 on 11.03.2021.
//

import UIKit

final class AudioAndVideoImagesView: UIView {

    // MARK: - Public properties

    lazy var buttonTapped = self.rx.tapGesture().when(.recognized)

    // MARK: - Private properties

    private let cameraImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "streamVideo")?.withRenderingMode(.alwaysTemplate))
        iw.contentMode = .scaleAspectFill
        iw.tintColor = TalkmeColors.streamGrayLabel
        return iw
    }()

    private let microImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "streamMicOn")?.withRenderingMode(.alwaysTemplate))
        iw.contentMode = .scaleAspectFill
        iw.tintColor = TalkmeColors.streamGrayLabel
        return iw
    }()

    private let indicatorView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIScreen.isSE ? 1 : 1.5
        view.backgroundColor = TalkmeColors.shadow
        view.isHidden = true
        return view
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func setOn(_ isActive: Bool) {
        cameraImageView.tintColor = isActive ? TalkmeColors.shadow : TalkmeColors.streamGrayLabel
        microImageView.tintColor = isActive ? TalkmeColors.shadow : TalkmeColors.streamGrayLabel
        indicatorView.isHidden = !isActive
    }

    // MARK: - Private methods

    private func setupLayout() {
        addSubviews([cameraImageView, microImageView, indicatorView])

        cameraImageView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 12 : 15)
            make.width.equalTo(UIScreen.isSE ? 16 : 20)
            make.centerY.equalTo(microImageView)
            make.trailing.equalTo(microImageView.snp.leading).offset(UIScreen.isSE ? -10 : -13)
        }

        microImageView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 15 : 19)
            make.width.equalTo(UIScreen.isSE ? 11 : 14)
            make.bottom.equalTo(indicatorView.snp.top).offset(UIScreen.isSE ? -2 : -3)
            make.top.equalToSuperview().offset(UIScreen.isSE ? 2 : 3)
            make.leading.equalTo(indicatorView.snp.trailing).offset(UIScreen.isSE ? 3 : 2)
        }

        indicatorView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 2 : 3)
            make.width.equalTo(UIScreen.isSE ? 9 : 13)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview()
        }

    }
}
