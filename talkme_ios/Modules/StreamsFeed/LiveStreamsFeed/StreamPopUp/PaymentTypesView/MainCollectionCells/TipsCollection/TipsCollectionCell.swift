//
//  TipsCollectionCell.swift
//  talkme_ios
//
//  Created by 1111 on 16.03.2021.
//

import RxSwift
import RxCocoa

final class TipsCollectionCell: UICollectionViewCell {

    // MARK: - Public Properties

    var bag = DisposeBag()
    let onSendTips = PublishRelay<Int>()

    // MARK: - Private Properties

    private let tipsView: StreamDonationView = {
        let view = StreamDonationView(frame: .zero, withRating: false)
        view.layer.cornerRadius = 13
        view.clipsToBounds = true
        return view
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func configure() {
        bindUI()
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tipsView.endEditing(true)
    }

    private func setupLayout() {
        addSubviews(tipsView)

        tipsView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 5 : 0)
            make.bottom.equalToSuperview()
        }
    }

    private func bindUI() {
        tipsView.sendButtonTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                guard let text = self.tipsView.textRelay.value else { return }
                if let tips = Int(text), (tips != 0) && !text.isEmpty {
                    self.onSendTips.accept(tips)
                }
            }
            .disposed(by: tipsView.bag)
    }
}
