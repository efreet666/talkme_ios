//
//  TipsCollectionCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 16.03.2021.
//

import RxSwift
import RxCocoa

final class TipsCollectionCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public properties

    let onSendTips = PublishRelay<Int>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: TipsCollectionCell) {
        cell.configure()

        cell
            .onSendTips
            .bind(to: onSendTips)
            .disposed(by: cell.bag)
    }
}
