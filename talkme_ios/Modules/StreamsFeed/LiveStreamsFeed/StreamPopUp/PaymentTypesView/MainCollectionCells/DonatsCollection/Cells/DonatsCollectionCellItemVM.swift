//
//  DonatsCollectionCellItemVM.swift
//  talkme_ios
//
//  Created by 1111 on 12.03.2021.
//

import UIKit

final class DonatsCollectionCellItemVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let donat: GetPlansResponse

    // MARK: - Initializers

    init(donat: GetPlansResponse) {
        self.donat = donat
    }

    // MARK: - Public Methods

    func configure(_ cell: DonatsCollectionCellItem) {
        cell.configure(donat: donat)
    }
}
