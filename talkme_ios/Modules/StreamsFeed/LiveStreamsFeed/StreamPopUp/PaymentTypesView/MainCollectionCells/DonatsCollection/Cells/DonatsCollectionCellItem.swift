//
//  DonatsCollectionCellItem.swift
//  talkme_ios
//
//  Created by 1111 on 12.03.2021.
//

import UIKit

final class DonatsCollectionCellItem: UICollectionViewCell {

    // MARK: - Private Properties

    private let backView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIScreen.main.bounds.width / 9.8
        view.backgroundColor = TalkmeColors.balanceViewGray
        return view
    }()

    private let donatImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()

    private let priceView: LabelAndCoinView = {
        let view = LabelAndCoinView()
        view.configure(viewType: .streamDonatPrice(price: ""))
        return view
    }()

    override var isSelected: Bool {
        didSet {
                self.backView.backgroundColor = self.isSelected ? TalkmeColors.pinkCollectionItem : TalkmeColors.balanceViewGray
        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(donat: GetPlansResponse) {
        guard let url = URL(string: donat.image) else { return }
        donatImageView.kf.setImage(with: url)
        priceView.configure(viewType: .streamDonatPrice(price: String(donat.cost)))
    }

    // MARK: - Private Methods

    private func initialSetup() {
        backView.addSubview(donatImageView)
        addSubviews([backView, priceView])

        let width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)

        backView.snp.makeConstraints { make in
            make.size.equalTo(width / 4.9)
            make.centerX.equalToSuperview()
        }

        donatImageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.size.equalTo(width / 4)
        }

        priceView.snp.makeConstraints { make in
            make.top.equalTo(backView.snp.bottom).offset(UIScreen.isSE ? 5 : 6)
            make.centerX.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 16 : 19)
            make.bottom.equalToSuperview()
        }
    }
}
