//
//  TopUpBalanceCollectionCellItemVM.swift
//  talkme_ios
//
//  Created by 1111 on 16.03.2021.
//

import UIKit

final class TopUpBalanceCollectionCellItemVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let type: GetPlansResponse

    // MARK: - Initializers

    init(type: GetPlansResponse) {
        self.type = type
    }

    // MARK: - Public Methods

    func configure(_ cell: TopUpBalanceCollectionCellItem) {
        cell.configure(topUpType: type)
    }
}
