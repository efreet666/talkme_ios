//
//  TopUpBalanceCollectionCell.swift
//  talkme_ios
//
//  Created by 1111 on 16.03.2021.
//

import RxSwift
import RxCocoa

final class TopUpBalanceCollectionCell: UICollectionViewCell {

    // MARK: - Public Properties

    private(set) lazy var selectedPlan = collectionView.rx.modelSelected(TopUpBalanceCollectionCellItemVM.self).map { $0.type.iosInAppId }
    private(set) lazy var toBuyButtonTapped = bottomButton.rx.tap

    // MARK: - Private Properties

    var bag = DisposeBag()
    let items = PublishRelay<[TopUpBalanceCollectionCellItemVM]>()

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let width = UIScreen.main.bounds.width / 2.3
        let height = width / 1.4
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumLineSpacing = 0
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.contentInset = UIEdgeInsets(top: 0, left: UIScreen.isSE ? 7 : 7, bottom: 0, right: UIScreen.isSE ? 10 : 13)
        cvc.indicatorStyle = .white
        cvc.registerCells(withModels: TopUpBalanceCollectionCellItemVM.self)
        return cvc
    }()

     var bottomButton = RoundedColoredButton(.buy)

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
        activateButton(false)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func buttonTouched(_ sender: UIButton) {
        UIButton.animate(
            withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
                sender.alpha = 0.5
            },
            completion: { _ in
                UIButton.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform.identity
                    sender.alpha = 1
                })
            })
    }

    func configure(items: [TopUpBalanceCollectionCellItemVM]) {
        bindUI()
        self.items.accept(items)
    }

    func activateButton(_ isEnabled: Bool) {
        bottomButton.isEnabled = isEnabled
    }

    // MARK: - Private Methods

    private func bindUI() {
        items
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func initialSetup() {
        addSubviews([collectionView, bottomButton])

        collectionView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview()
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -2 : -8)
            make.bottom.equalTo(bottomButton.snp.top).offset(UIScreen.isSE ? -15 : -22)
        }

        bottomButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE  ? 37 : 48)
            make.trailing.leading.equalToSuperview().inset(UIScreen.isSE ? 45 : 57)
        }
    }
}
