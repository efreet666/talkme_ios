//
//  TopUpBalanceCollectionCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 16.03.2021.
//

import RxSwift
import RxCocoa

final class TopUpBalanceCollectionCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let selectedPlan = PublishRelay<Int>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    var items: [TopUpBalanceCollectionCellItemVM]
    var selectedPlanId: Int?

    // MARK: - Init

    init(plansModel: [GetPlansResponse]) {
        self.items = plansModel.map { TopUpBalanceCollectionCellItemVM(type: $0)}
    }

    // MARK: - Public Methods

    func configure(_ cell: TopUpBalanceCollectionCell) {
        cell.configure(items: items)

        cell
            .selectedPlan
            .bind { [weak self, weak cell] selectedPlan in
                self?.selectedPlanId = selectedPlan
                cell?.activateButton(true)
            }
            .disposed(by: cell.bag)

        cell
            .toBuyButtonTapped
            .bind { [weak self] _ in
                guard let self = self, let selectedPlanId = self.selectedPlanId else { return }
                cell.buttonTouched(cell.bottomButton)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
                    self.selectedPlan.accept(selectedPlanId)
                }
            }
            .disposed(by: cell.bag)
    }
}
