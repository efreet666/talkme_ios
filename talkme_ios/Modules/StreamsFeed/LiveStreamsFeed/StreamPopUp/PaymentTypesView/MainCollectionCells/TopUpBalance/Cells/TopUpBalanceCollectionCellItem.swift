//
//  TopUpBalanceCollectionCellItem.swift
//  talkme_ios
//
//  Created by 1111 on 16.03.2021.
//

import RxSwift

final class TopUpBalanceCollectionCellItem: UICollectionViewCell {

    // MARK: - Private Properties

    private let coinImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()

    private let countView: LabelAndCoinView = {
        let view = LabelAndCoinView()
        view.configure(viewType: .streamFeedTimeItem(price: ""))
        return view
    }()

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .montserratSemiBold(ofSize: 15)
        label.textColor = TalkmeColors.white
        label.backgroundColor = TalkmeColors.blueLabels
        label.layer.cornerRadius = UIScreen.isSE ? 11 : 14
        label.layer.masksToBounds = true
        return label
    }()

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 17)
        label.textColor = TalkmeColors.white
        return label
    }()

    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIScreen.isSE ? 5 : 8
        view.layer.borderWidth = 2
        view.layer.borderColor = TalkmeColors.unselectedTimeItem.cgColor
        view.backgroundColor = TalkmeColors.unselectedTimeItem
        view.clipsToBounds = true
        return view
    }()

    override var isSelected: Bool {
        didSet {
            containerView.backgroundColor = isSelected ? TalkmeColors.selectedTimeItem : TalkmeColors.unselectedTimeItem
            containerView.layer.borderColor = isSelected ? TalkmeColors.selectedTimeItemBorder.cgColor : TalkmeColors.unselectedTimeItem.cgColor
        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(topUpType: GetPlansResponse) {
        guard let url = URL(string: topUpType.image) else { return }
        coinImageView.kf.setImage(with: url)
        nameLabel.text = topUpType.title
        guard let purchaseId = topUpType.iosInAppId else { return }
        priceLabel.text = IAPManager.shared.price(for: String(purchaseId))
        guard let price = topUpType.iosCoinAmount else {
            countView.configure(viewType: .streamFeedTimeItem(price: ""))
            return }
        countView.configure(viewType: .streamFeedTimeItem(price: String(price)))
    }

    // MARK: - Private Methods

    private func initialSetup() {
        containerView.addSubviews([coinImageView, nameLabel, countView, priceLabel])
        addSubview(containerView)

        containerView.snp.makeConstraints { make in
            make.trailing.leading.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -12 : -14)
        }

        coinImageView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().inset(UIScreen.isSE ? 7 : 9)
            make.bottom.equalTo(priceLabel.snp.top).offset(UIScreen.isSE ? -2 : -4)
            make.width.equalTo(coinImageView.snp.height)
            make.trailing.equalTo(countView.snp.leading).offset(UIScreen.isSE ? -13 : -17)
        }

        nameLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.top.equalToSuperview().inset(UIScreen.isSE ? 6 : 9)
            make.leading.equalTo(coinImageView.snp.trailing).offset(UIScreen.isSE ? 13 : 17)
        }

        countView.snp.makeConstraints { make in
            make.bottom.equalTo(priceLabel.snp.top).offset(UIScreen.isSE ? -7 : -13)
            make.top.equalTo(nameLabel.snp.bottom)
            make.trailing.lessThanOrEqualToSuperview()
            make.height.equalTo(UIScreen.isSE ? 14 : 19)
        }

        priceLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 38 : 48)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 9 : 12)
            make.height.equalTo(UIScreen.isSE ? 22 : 28)
        }
    }
}
