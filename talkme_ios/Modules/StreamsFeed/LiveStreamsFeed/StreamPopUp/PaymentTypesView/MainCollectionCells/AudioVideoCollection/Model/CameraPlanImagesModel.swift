//
//  CameraPlanImagesModel.swift
//  talkme_ios
//
//  Created by 1111 on 24.03.2021.
//

import UIKit

enum CameraPlanImagesModel: Int, Decodable {
    case redClock = 1
    case yellowClock
    case blueClock
    case greenClock
    case others

    var icon: UIImage? {
        switch self {
        case .redClock:
            return UIImage(named: "redClock")
        case .yellowClock:
            return UIImage(named: "yellowClock")
        case .blueClock:
            return UIImage(named: "blueClock")
        case .greenClock:
            return UIImage(named: "greenClock")
        case .others:
            return nil
        }
    }
}
