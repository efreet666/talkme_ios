//
//  BuyTimeCollectionCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 15.03.2021.
//

import RxSwift
import RxCocoa

final class BuyTimeCollectionCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let sendSelectedPlan = PublishRelay<GetCameraPlanResponse>()
    let bag = DisposeBag()
    let onButtonTapped = PublishRelay<Void>()

    // MARK: - Private Properties

    var items: [AudioVideoCollectionCellItemVM]
    var cameraPlan: GetCameraPlanResponse?

    // MARK: - Init

    init(buyTimeModel: [GetCameraPlanResponse]) {
        self.items = buyTimeModel.map { AudioVideoCollectionCellItemVM(time: $0) }
    }

    // MARK: - Public Methods

    func configure(_ cell: BuyTimeCollectionCell) {
        cell.configure(items: items)

        cell
            .selectedPlan
            .bind { [weak self, weak cell] cameraPlan in
                self?.cameraPlan = cameraPlan
                cell?.activateButton(true)
            }
            .disposed(by: cell.bag)

        cell
            .connectButtonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                guard let cameraPlan = self.cameraPlan else { return }
                self.sendSelectedPlan.accept(cameraPlan)
            }
            .disposed(by: cell.bag)
    }
}
