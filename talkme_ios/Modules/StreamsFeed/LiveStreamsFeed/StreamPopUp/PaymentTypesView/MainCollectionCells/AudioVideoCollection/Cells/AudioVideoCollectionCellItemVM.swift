//
//  AudioVideoCollectionCellItemVM.swift
//  talkme_ios
//
//  Created by 1111 on 15.03.2021.
//

import UIKit

final class AudioVideoCollectionCellItemVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let time: GetCameraPlanResponse

    // MARK: - Initializers

    init(time: GetCameraPlanResponse) {
        self.time = time
    }

    // MARK: - Public Methods

    func configure(_ cell: AudioVideoCollectionCellItem) {
        cell.configure(cameraPlan: time)
    }
}
