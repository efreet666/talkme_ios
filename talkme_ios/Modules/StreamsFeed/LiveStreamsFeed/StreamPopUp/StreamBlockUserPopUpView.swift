//
//  StreamBlockUserPopUpView.swift
//  talkme_ios
//
//  Created by nikita on 23.05.2022.
//

import RxSwift
import RxCocoa
import UIKit

final class StreamBlockUserPopUpView: UIView, DrawerContentView {

    private enum Constants {
        static let topToSwipeLine: CGFloat = UIScreen.isSE ? 8 : 11
        static let swipeLineSize = UIScreen.isSE
            ? CGSize(width: 97, height: 3)
            : CGSize(width: 126, height: 4)
        static let leftOffest: CGFloat = 25
    }

    // MARK: - Public properties

    var onDismiss: (() -> Void)?
    let bag = DisposeBag()
    // MARK: - Private Properties

    enum Flow {
        case dissmissPopupWithSuccess
        case dissmissPopupWithError
        case cancelPopup
    }

    let flow = PublishRelay<Flow>()

    let viewHeight = BehaviorRelay<CGFloat>(
        value: (UIScreen.isSE ? 190 : 210)
    )

    private let swipeLineView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = .white
        return view
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 20 : 25)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "complaint_block_title".localized
        return label
    }()

    private let blockButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = UIScreen.isSE ? 21 : 27
        button.backgroundColor = TalkmeColors.blueLabels
        button.titleLabel?.font = UIFont.montserratExtraBold(ofSize: UIScreen.isSE ? 15 : 17)
        button.setTitle( "complaint_block_button_title".localized, for: .normal)
        button.setTitleColor(TalkmeColors.grayLabel, for: .highlighted)
        return button
    }()

    private let cancelButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.montserratExtraBold(ofSize: UIScreen.isSE ? 15 : 17)
        button.setTitle( "complaint_cancel_button_title".localized, for: .normal)
        button.setTitleColor( TalkmeColors.blueLabels, for: .normal)
        button.setTitleColor(TalkmeColors.white, for: .highlighted)
        button.layer.borderWidth = UIScreen.isSE ? 2.5 : 3
        button.layer.borderColor = TalkmeColors.blueLabels.cgColor
        button.layer.cornerRadius = UIScreen.isSE ? 21 : 27

        return button
    }()

    private let accountService: AccountService
    private let userAboutId: Int

    init(accountService: AccountService, userAbout: Int) {
        self.userAboutId = userAbout
        self.accountService = accountService
        super.init(frame: .zero)
        setUpAppearance()
        setUpConstraints()
        bindUI()
        AppDelegate.deviceOrientation = .portrait
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func didMoveToSuperview() {
        guard superview != nil else { return }
    }

    func updateLayout(height: CGFloat) {
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

    private let radioButtonsView: RadioButtonsView = {
        let radioButtonView = RadioButtonsView()
        return radioButtonView
    }()

    private func setUpConstraints() {
        addSubviews(
            swipeLineView,
            titleLabel,
            blockButton,
            cancelButton
        )

        swipeLineView.snp.makeConstraints { make in
            make.size.equalTo(Constants.swipeLineSize)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(Constants.topToSwipeLine)
        }

        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalTo(UIScreen.width)
            make.top.equalTo(swipeLineView.snp.bottom).offset(UIScreen.isSE ? 20 : 25)
        }

        blockButton.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.width * 0.5)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
            make.bottom.equalTo(self.snp.bottom).inset( UIApplication.bottomInset == 0 ? 15 : UIApplication.bottomInset)
            make.left.equalTo(self).inset(UIScreen.width * 0.06)
        }

        cancelButton.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.width * 0.35)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
            make.centerY.equalTo(blockButton)
            make.left.equalTo(blockButton.snp.right).offset(UIScreen.width * 0.03)
        }
    }

    private var complaint: ComplaintResponse?

    private func sendComplaint() {
        guard let complaint = complaint else { return }
        accountService.complaintCreate(toUserId: userAboutId, compliantType: complaint.key, compliantString: complaint.value)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.statusCode == 201
                    else {
                        self?.flow.accept(.dissmissPopupWithError)
                        return
                    }
                    self?.flow.accept(.dissmissPopupWithSuccess)
                case .error(let error):
                    print(error)
                    self?.flow.accept(.dissmissPopupWithError)
                }
            }
            .disposed(by: bag)
    }

    private func bindUI() {
        accountService.getComplaints()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.complaint = response.complaints?.first
                case .error(let error):
                    print(error)
                }
            }

        cancelButton.rx
            .tap
            .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.flow.accept(.cancelPopup)
            })
            .disposed(by: bag)

        blockButton.rx
            .tap
            .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.sendComplaint()
            })
            .disposed(by: bag)
    }

    @objc private func hideKeyboard() {
        endEditing(true)
    }

    private func setUpAppearance() {
        backgroundColor = TalkmeColors.streamPopUpGray
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        layer.cornerRadius = 13
        clipsToBounds = true
    }
}
