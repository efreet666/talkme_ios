//
//  StreamMembersCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 16.03.2021.
//

import RxSwift
import RxCocoa
import OpenTok

final class StreamMembersCell: UICollectionViewCell {

    // MARK: - Public properties

    private(set) lazy var reloadButtonTapped = membersList.reloadButtonTapped
    private(set) lazy var onConnectButtonTapped = membersList.onConnectButtonTapped
    private(set) var bag = DisposeBag()
    let camerasListRelay = PublishRelay<SubscriberForPopup>()

    // MARK: - Private Properties

    private let membersList = MembersListView()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func setupMembersList(list: SubscriberForPopup, membersCount: String) {
        membersList.setupUI(camerasList: list, membersCount: membersCount)
    }

    func updateLearnerVideoState(learner: AlteredStream) {
        membersList.updateMemberVideoState(model: learner)
    }

    func updateLearnerAudioState(learner: AlteredStream) {
        membersList.updateMemberAudioState(model: learner)
    }

    func addNewLearner(learner: CurrentSubscriber) {
        membersList.addNewLearner(learner: learner)
    }

    func removeLearner(learner: CurrentSubscriber) {
        membersList.removeLearner(learner: learner)
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        contentView.addSubview(membersList)

        membersList.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 5 : 16)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
}
