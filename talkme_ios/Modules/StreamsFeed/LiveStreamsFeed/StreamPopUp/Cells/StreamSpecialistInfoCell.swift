//
//  StreamSpecialistInfoCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 09.03.2021.
//

import RxSwift
import RxCocoa
import Kingfisher

final class StreamSpecialistInfoCell: UITableViewCell {

    // MARK: - Public properties

    private(set) lazy var messageButtonTapped = messageButton.rx.tap
    private(set) lazy var plusButtonTapped = plusButton.rx.tap
    private(set) lazy var avatarTapped = avatarImage.rx.tapGesture().when(.recognized)
    private(set) lazy var fullNameTapped = fullNameLabel.rx.tapGesture().when(.recognized)
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let ratingLabel = ImageAndTitleView()

    private let avatarImage: RoundedImageView = {
        let iw = RoundedImageView()
        iw.contentMode = .scaleAspectFill
        return iw
    }()

    private let ratingContainer: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.ratingViewYellow
        return view
    }()

    private let classNumberLabel: ClassNumberLabel = {
        let label = ClassNumberLabel()
        label.configure(type: .streamSpecialist)
        return label
    }()

    private let plusButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "streamAddSpecialist"), for: .normal)
        return button
    }()

    private let messageButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "streamSendMessage"), for: .normal)
        return button
    }()

    private lazy var plusMessageStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [plusButton, messageButton])
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        return stackView
    }()

    private let fullNameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratExtraBold(ofSize: UIScreen.isSE ? 14 : 17)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()

    private let ageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    private let sexImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private lazy var ageSexStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [ageLabel, sexImage])
        stackView.spacing = UIScreen.isSE ? 6 : 7
        return stackView
    }()

    private let locationLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textAlignment = .center
        return label
    }()

    private let idLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = .white
        return label
    }()

    private let idContainer: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.grayLabels
        return view
    }()

    private let awardsImage: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "cup"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let awardsContainer: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.grayLabels
        return view
    }()

    private let awardsNumberLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.greenLabels
        return label
    }()

    private lazy var awardsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [awardsImage, awardsNumberLabel])
        stackView.spacing = UIScreen.isSE ? 5 : 6
        stackView.distribution = .equalSpacing
        return stackView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpConstraints()
        cellStyle()
        messageButton.isHidden = false
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func isContact(isContact: Bool) {
        plusButton.setImage(UIImage(named: isContact ? "streamRemoveSpecialist" : "streamAddSpecialist"), for: .normal)
    }

    func configure(_ model: SpecialistMainInfoModel) {
        if let url = URL(string: model.avatarUrl) {
            avatarImage
                .kf
                .setImage(
                    with: ImageResource(downloadURL: url, cacheKey: url.absoluteString),
                    placeholder: UIImage(named: "teacherPlaceholder")
                )
        }
        classNumberLabel.text = "№ \(model.classNumber)"
        ratingLabel.configure(.streamRating(model.rating))
        awardsNumberLabel.text = model.awardsNumber
        fullNameLabel.text = model.fullName

        if model.sex == .male {
            sexImage.image = UIImage(named: "male")
        } else {
            sexImage.image = UIImage(named: "female")
        }

        ageLabel.text = model.age
        locationLabel.text = model.location
        idLabel.text = model.userName
        isContact(isContact: model.isContact)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        contentView.addSubviews(
            avatarImage,
            ratingContainer,
            classNumberLabel,
            plusMessageStack,
            fullNameLabel,
            ageSexStack,
            locationLabel,
            idContainer,
            awardsContainer
        )
        idContainer.addSubview(idLabel)
        ratingContainer.addSubview(ratingLabel)
        awardsContainer.addSubviews(awardsStack)
        classNumberLabel.isHidden = true

        avatarImage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(UIScreen.isSE ? 10 : 15)
            make.size.equalTo(UIScreen.isSE ? 82 : 106)
        }

        ratingContainer.snp.makeConstraints { make in
            make.centerX.equalTo(avatarImage)
            make.bottom.equalTo(avatarImage).offset(UIScreen.isSE ? 6 : 8)
            make.size.equalTo(UIScreen.isSE
                                ? CGSize(width: 53, height: 21)
                                : CGSize(width: 69, height: 27)
            )
        }

        ratingLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }

        classNumberLabel.snp.makeConstraints { make in
            make.centerY.equalTo(avatarImage)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
            make.height.equalTo(UIScreen.isSE ? 33 : 43)
            make.width.greaterThanOrEqualTo(UIScreen.isSE ? 85 : 110).priority(900)
            make.trailing.lessThanOrEqualTo(avatarImage.snp.leading).offset(-5).priority(1000)
        }

        plusMessageStack.snp.makeConstraints { make in
            make.centerY.equalTo(avatarImage)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
            make.leading.equalTo(avatarImage.snp.trailing).offset(UIScreen.isSE ? 9 : 11)
            make.height.equalTo(UIScreen.isSE ? 43 : 56).priority(700)
        }

        plusButton.snp.makeConstraints { make in
            make.width.equalTo(plusButton.snp.height)
        }

        messageButton.snp.makeConstraints { make in
            make.size.equalTo(plusButton)
        }

        fullNameLabel.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 20 : 27)
            make.top.equalTo(ratingContainer.snp.bottom).offset(UIScreen.isSE ? 6 : 8)
            make.centerX.equalToSuperview()
        }

        sexImage.snp.makeConstraints { make in
            make.size.equalTo(
                UIScreen.isSE
                    ? CGSize(width: 9, height: 13)
                    : CGSize(width: 12, height: 18)
            )
        }

        ageSexStack.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 13 : 18)
            make.top.equalTo(fullNameLabel.snp.bottom).offset(UIScreen.isSE ? 4 : 8)
        }

        locationLabel.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 17 : 22)
            make.centerX.equalToSuperview()
            make.top.equalTo(ageSexStack.snp.bottom).offset(UIScreen.isSE ? 7 : 6)
        }

        idContainer.snp.makeConstraints { make in
            make.top.equalTo(locationLabel.snp.bottom).offset(UIScreen.isSE ? 12 : 13)
            make.centerX.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 29 : 37)
            make.width.lessThanOrEqualToSuperview().inset(32)
        }

        idLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.lessThanOrEqualToSuperview().inset(UIScreen.isSE ? 22 : 39)
        }

        awardsContainer.snp.makeConstraints { make in
            make.centerY.equalTo(avatarImage)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
            make.height.equalTo(UIScreen.isSE ? 33 : 43)
            make.width.greaterThanOrEqualTo(UIScreen.isSE ? 85 : 110).priority(900)
            make.trailing.lessThanOrEqualTo(avatarImage.snp.leading).offset(-15)
        }

        awardsImage.snp.makeConstraints { make in
            make.size.equalTo(
                UIScreen.isSE
                    ? CGSize(width: 10, height: 12)
                    : CGSize(width: 13, height: 16)
            )
        }

//        awardsStack.snp.makeConstraints { make in
//            make.centerX.equalTo(classNumberLabel)
//            make.top.equalTo(classNumberLabel.snp.bottom).offset(UIScreen.isSE ? 15 : 19)
//            make.height.equalTo(UIScreen.isSE ? 12 : 16)
//        }

//        awardsContainer.snp.makeConstraints { make in
//            make.centerY.equalTo(avatarImage)
//            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
//            make.height.equalTo(UIScreen.isSE ? 33 : 43)
//            make.width.greaterThanOrEqualTo(UIScreen.isSE ? 85 : 110).priority(900)
//            make.trailing.lessThanOrEqualTo(avatarImage.snp.leading).offset(-15)
//        }

        awardsStack.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 12 : 16)
        }

//        awardsImage.snp.makeConstraints { make in
//            make.centerY.equalTo(awardsContainer)
//            make.centerX.equalTo(awardsContainer).multipliedBy(0.5)
////            make.leading.equalTo(awardsContainer).offset(10)
//        }
//
//        awardsNumberLabel.snp.makeConstraints { make in
//            make.centerY.equalTo(awardsContainer)
//            make.centerX.equalTo(awardsContainer).multipliedBy(1.5)
////            make.trailing.equalTo(awardsImage).offset(10)
//        }
    }

    private func cellStyle() {
        selectionStyle = .none
        backgroundColor = .clear
    }
}
