//
//  StreamSpecialistInfoCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.03.2021.
//
import Foundation
import RxSwift
import RxCocoa

final class StreamSpecialistInfoCellViewModel: TableViewCellModelProtocol {

    enum Flow {
        case showSpecialistScreen
        case addSpecialist(Int)
        case removeSpecialist(Int)
        case showChatWithSpecialist(Int)
    }

    // MARK: - Public properties

    let specialistInfo: SpecialistMainInfoModel
    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()
    var isContact = true

    // MARK: - Initializers

    init(_ responseModel: PublicProfileResponse) {
        let model = responseModel.specialistInfo
        let dateComponents = DateComponents(year: model.age)

        specialistInfo = SpecialistMainInfoModel(
            fullName: "\(model.firstName ?? "") \(model.lastName ?? "")",
            age: Formatters.dateComponentsFormatter.string(from: dateComponents) ?? "",
            avatarUrl: model.avatarUrl ?? "",
            classNumber: model.numberClass ?? "",
            sex: model.gender == "male" ? .male : .female,
            location: "\(model.city?.country ?? ""). \(model.city?.city ?? "")",
            userName: "ID \(model.username ?? "")",
            id: model.id ?? 0,
            awardsNumber: "\(model.lessonPassed ?? 0)",
            rating: model.rating ?? 0,
            isContact: responseModel.isContact
        )
        self.isContact = specialistInfo.isContact
    }

    // MARK: - Public Methods

    func configure(_ cell: StreamSpecialistInfoCell) {
        bindUI(cell)
        cell.configure(specialistInfo)
    }

    // MARK: - Private Methods

    private func bindUI(_ cell: StreamSpecialistInfoCell) {
        cell.avatarTapped
            .map { _ in .showSpecialistScreen }
            .bind(to: flow)
            .disposed(by: cell.bag)

        cell.fullNameTapped
            .map { _ in .showSpecialistScreen }
            .bind(to: flow)
            .disposed(by: cell.bag)

        cell.plusButtonTapped
            .compactMap { [weak self, weak cell] _ in
                guard let id = self?.specialistInfo.id, let isContact = self?.isContact else { return nil }
                if isContact {
                    self?.isContact = false
                    cell?.isContact(isContact: false)
                    return .removeSpecialist(id)
                } else {
                    self?.isContact = true
                    cell?.isContact(isContact: true)
                    return .addSpecialist(id)
                }
            }
            .bind(to: flow)
            .disposed(by: cell.bag)

        cell.messageButtonTapped
            .bind { [weak self] _ in
                guard let id = self?.specialistInfo.id else { return }
                self?.flow.accept(.showChatWithSpecialist(id))
            }
            .disposed(by: cell.bag)
    }
}
