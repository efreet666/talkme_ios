//
//  TeacherMemberCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.04.2021.
//

import RxSwift
import RxCocoa

final class TeacherMemberCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public properties

    let refreshMembersList = PublishRelay<Void>()
    let camerasListRelay = PublishRelay<SubscriberForPopup>()
    let bag = DisposeBag()
    let onUpdateLearnerVideoState = PublishRelay<AlteredStream>()
    let onUpdateLearnerAudioState = PublishRelay<AlteredStream>()
    let onAddLearnerOnPopup = PublishRelay<CurrentSubscriber>()
    let onRemoveLearnerFromPopup = PublishRelay<CurrentSubscriber>()

    // MARK: - Private properties

    private let socketCamerasService: SocketCamerasService?

    // MARK: - Init

    init( socketCamerasService: SocketCamerasService?) {
        self.socketCamerasService = socketCamerasService
    }

    // MARK: - Public Methods

    func configure(_ cell: TeacherMemberCell) {
        cell
            .reloadButtonTapped
            .bind { [weak self] _ in
                self?.refreshMembersList.accept(())
            }
            .disposed(by: cell.bag)

        camerasListRelay
            .bind { [weak cell] list in
                cell?.setupMembersList(list: list)
            }
            .disposed(by: cell.bag)

        socketCamerasService?.fetchCamerasList()

        onUpdateLearnerVideoState
            .bind { [weak cell] learner in
                cell?.updateLearnerVideoState(learner: learner)
            }
            .disposed(by: cell.bag)

        onUpdateLearnerAudioState
            .bind { [weak cell] learner in
                cell?.updateLearnerAudioState(learner: learner)
            }
            .disposed(by: cell.bag)

        onAddLearnerOnPopup
            .bind { [weak cell] learner in
                cell?.addNewLearner(learner: learner)
            }
            .disposed(by: cell.bag)

        onRemoveLearnerFromPopup
            .bind { [weak cell] learner in
                cell?.removeLearner(learner: learner)
            }
            .disposed(by: cell.bag)
    }
}
