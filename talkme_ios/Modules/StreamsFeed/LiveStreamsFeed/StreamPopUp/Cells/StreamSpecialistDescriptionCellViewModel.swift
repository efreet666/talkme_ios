//
//  StreamSpecialistDescriptionCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.03.2021.
//

import RxSwift
import RxCocoa

final class StreamSpecialistDescriptionCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let showMoreTapped = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let description: String

    // MARK: - Initializers

    init(_ description: String) {
        self.description = description
    }

    // MARK: - Public Methods

    func configure(_ cell: StreamSpecialistDescriptionCell) {
        cell.configure(description)
        cell.showMoreTapped
            .bind(to: showMoreTapped)
            .disposed(by: cell.bag)
    }
}
