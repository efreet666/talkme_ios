//
//  StreamSocialCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 11.03.2021.
//

import RxSwift
import RxCocoa

final class StreamSocialCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let socialTypeSelected = PublishRelay<SocialMedia>()
    let bag = DisposeBag()
    let socials: [SocialMedia]
    let info: PublicSpecialistInfo?

    // MARK: - Initializers

    init(_ info: PublicSpecialistInfo) {
        self.info = info
        var socials = [SocialMedia]()
        if info.vk != nil { socials.append(.vk) }
        if info.telegram != nil { socials.append(.telegram) }
        self.socials = socials
    }

    // MARK: - Public Methods

    func configure(_ cell: StreamSocialCell) {
        let socialModels = socials.map { StreamSocialCollectionCellViewModel(categoryModel: $0, resp: info) }
        cell.configure(socialModels)

        cell.modelSelected
            .map { $0.socialsModel }
            .bind(to: socialTypeSelected)
            .disposed(by: cell.bag)
    }
}
