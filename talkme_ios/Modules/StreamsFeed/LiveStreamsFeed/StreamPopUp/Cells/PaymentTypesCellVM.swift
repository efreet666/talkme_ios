//
//  StreamPaidServicesCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 16.03.2021.
//

import RxSwift
import RxCocoa

final class PaymentTypesCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let selectedDonat = PublishRelay<GetPlansResponse>()
    let selectedAudioVideoPlan = PublishRelay<GetCameraPlanResponse>()
    let selectedPaymentPlan = PublishRelay<Int>()
    let toGetCoins = PublishRelay<Void>()
    let toGetCoinsIsNotSubscribe = PublishRelay<Void>()
    let audioVideoButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()
    let updatedBalance = PublishRelay<Int>()
    let collectionItems = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let onSendTips = PublishRelay<Int>()
    let onScrollToPayments = PublishRelay<Void>()

    // MARK: - Private Properties

    var cells = [AnyCollectionViewCellModelProtocol]()
    private var coins = 0
    private var isOnlyPurchasesPopup: Bool

    // MARK: - Init

    init(onlyPurchases: Bool) {
        self.isOnlyPurchasesPopup = onlyPurchases
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func updateBalance(coins: Int) {
        self.coins = coins
        updatedBalance.accept(coins)
    }

    func setCollectionItems(
        gifts: [GetPlansResponse],
        plans: [GetPlansResponse],
        cameraPlans: [GetCameraPlanResponse],
        isNeedScrollToPayments: Bool
    ) {
        let donatsCollectionVM = CollectionDonatsCellVM(giftsModel: gifts)
//        let buyTimeCollectionVM = BuyTimeCollectionCellVM(buyTimeModel: cameraPlans)
        let topUpBalanceVM = TopUpBalanceCollectionCellVM(plansModel: plans)
        let tipsCollectionCellVM = TipsCollectionCellVM()
        let items: [AnyCollectionViewCellModelProtocol] = isOnlyPurchasesPopup ? [topUpBalanceVM] : [donatsCollectionVM, /*buyTimeCollectionVM,*/ tipsCollectionCellVM, topUpBalanceVM]

        cells = items
        collectionItems.accept(items)

        donatsCollectionVM.sendSelectedDonat
            .bind(to: selectedDonat)
            .disposed(by: donatsCollectionVM.bag)

//        buyTimeCollectionVM.sendSelectedPlan
//            .bind(to: selectedAudioVideoPlan)
//            .disposed(by: buyTimeCollectionVM.bag)

        topUpBalanceVM.selectedPlan
            .bind(to: selectedPaymentPlan)
            .disposed(by: topUpBalanceVM.bag)

        tipsCollectionCellVM.onSendTips
            .bind(to: onSendTips)
            .disposed(by: tipsCollectionCellVM.bag)

        guard isNeedScrollToPayments else { return}
        onScrollToPayments.accept(())
    }

    func configure(_ cell: PaymentTypesCell) {
        cell.configure(isOnlyPurchases: isOnlyPurchasesPopup)
        cell.updateBalance(coins: coins)
        cell.setupCollection(cells: cells)

        updatedBalance
            .bind { [weak cell] coins in
                cell?.updateBalance(coins: coins)
            }
            .disposed(by: cell.bag)

        collectionItems
            .bind { [weak cell] items in
                cell?.setupCollection(cells: items)
            }
            .disposed(by: cell.bag)

        toGetCoins
            .bind { [weak cell, weak self] _ in
                guard let self = self, !self.isOnlyPurchasesPopup else { return }
                cell?.scrollToGetCoins()
            }
            .disposed(by: cell.bag)

        toGetCoinsIsNotSubscribe
            .bind { [weak cell] _ in
                cell?.showTopUpBalanceIsNotSubscribe()
            }
            .disposed(by: cell.bag)

        cell
            .audioVideoTap
            .bind { [weak cell] _ in
                cell?.onAudioTap.accept(())
            }
            .disposed(by: cell.bag)

        cell
            .onAudioTap
            .bind(to: audioVideoButtonTap)
            .disposed(by: cell.bag)

        audioVideoButtonTap
            .bind { [weak cell] in
                cell?.showAudioVideo()
            }
            .disposed(by: bag)

        onScrollToPayments
            .bind { [weak cell, weak self] _ in
                guard let self = self, !self.isOnlyPurchasesPopup else { return }
                cell?.scrollToGetCoins()
            }
            .disposed(by: cell.bag)
    }
}
