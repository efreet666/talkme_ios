//
//  StreamMembersCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 16.03.2021.
//

import RxSwift
import RxCocoa
import OpenTok

final class StreamMembersCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public properties

    let refreshMembersList = PublishRelay<Void>()
    let onConnectButtonTappedRelay = PublishRelay<Void>()
    let camerasListRelay = PublishRelay<PopupData>()
    let bag = DisposeBag()
    let onUpdateLearnerVideoState = PublishRelay<AlteredStream>()
    let onUpdateLearnerAudioState = PublishRelay<AlteredStream>()
    let onAddLearnerOnPopup = PublishRelay<CurrentSubscriber>()
    let onRemoveLearnerFromPopup = PublishRelay<CurrentSubscriber>()

    // MARK: - Public Methods

    func configure(_ cell: StreamMembersCell) {
        cell
            .onConnectButtonTapped
            .bind(to: onConnectButtonTappedRelay)
            .disposed(by: cell.bag)

        cell
            .reloadButtonTapped
            .bind { [weak self] _ in
                self?.refreshMembersList.accept(())
            }
            .disposed(by: cell.bag)

        camerasListRelay
            .bind { [weak cell] data in
                cell?.setupMembersList(list: data.subscriberForPopup, membersCount: data.membersCount)
            }
            .disposed(by: cell.bag)

        onUpdateLearnerVideoState
            .bind { [weak cell] learner in
                cell?.updateLearnerVideoState(learner: learner)
            }
            .disposed(by: cell.bag)

        onUpdateLearnerAudioState
            .bind { [weak cell] learner in
                cell?.updateLearnerAudioState(learner: learner)
            }
            .disposed(by: cell.bag)

        onAddLearnerOnPopup
            .bind { [weak cell] learner in
                cell?.addNewLearner(learner: learner)
            }
            .disposed(by: cell.bag)

        onRemoveLearnerFromPopup
            .bind { [weak cell] learner in
                cell?.removeLearner(learner: learner)
            }
            .disposed(by: cell.bag)
    }

}
