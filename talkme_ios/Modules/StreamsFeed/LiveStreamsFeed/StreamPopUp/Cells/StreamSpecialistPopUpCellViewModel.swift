//
//  StreamSpecialistPopUpCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 10.03.2021.
//

import RxSwift
import RxCocoa

final class StreamSpecialistPopUpCellViewModel: CollectionViewCellModelProtocol {

    enum Flow {
        case showSpecialistScreen
        case addSpecialist(Int)
        case removeSpecialist(Int)
        case showChatWithSpecialist(Int)
        case showSocial(SocialMedia)
    }

    // MARK: - Public properties

    let expandCompactDescription = PublishRelay<Void>()
    let items = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    let response: BehaviorRelay<PublicProfileResponse>

    // MARK: - Initializers

    init(_ response: PublicProfileResponse) {
        self.response = BehaviorRelay<PublicProfileResponse>(value: response)
        self.response.bind { [weak self] _ in
            self?.setUpItems()
        }.disposed(by: bag)
    }

    // MARK: - Public Methods

    func configure(_ cell: StreamSpecialistPopUpCell) {
        cell.configure(self)
        setUpItems()
    }

    // MARK: - Private Methods

    private func setUpItems() {
        var items = [AnyTableViewCellModelProtocol]()
        let mainModel = StreamSpecialistInfoCellViewModel(response.value)
        bindMainModel(mainModel)
        items.append(mainModel)
        let socialsModel = StreamSocialCellViewModel(response.value.specialistInfo)
        if !socialsModel.socials.isEmpty {
            bindSocialsModel(socialsModel)
            items.append(socialsModel)
        }
        if let descriptionText = response.value.specialistInfo.bio {
            let descriptionModel = StreamSpecialistDescriptionCellViewModel(descriptionText)
            bindDescriptionModel(descriptionModel)
            items.append(descriptionModel)
        }
        self.items.accept(items)
    }

    private func bindMainModel(_ model: StreamSpecialistInfoCellViewModel) {
        model.flow
            .map { event in
                switch event {
                case .addSpecialist(let id):
                    return Flow.addSpecialist(id)
                case .removeSpecialist(let id):
                    return Flow.removeSpecialist(id)
                case .showChatWithSpecialist(let id):
                    return Flow.showChatWithSpecialist(id)
                case .showSpecialistScreen:
                    return Flow.showSpecialistScreen
                }
            }
            .bind(to: flow)
            .disposed(by: model.bag)
    }

    private func bindDescriptionModel(_ model: StreamSpecialistDescriptionCellViewModel) {
        model.showMoreTapped
            .bind(to: expandCompactDescription)
            .disposed(by: model.bag)
    }

    private func bindSocialsModel(_ model: StreamSocialCellViewModel) {
        model.socialTypeSelected
            .compactMap { type in
                return Flow.showSocial(type)
            }.bind(to: flow)
            .disposed(by: model.bag)
    }
}
