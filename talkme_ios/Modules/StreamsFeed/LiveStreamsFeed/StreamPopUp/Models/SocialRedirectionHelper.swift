//
//  SocialRedirectionHelper.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 15.03.2021.
//

import Foundation
import UIKit

enum SocialRedirectionHelper: String {
    case instagram
    case vk
    case telegram
    case facebook

    func redirect(to urlString: String) {
        guard let url = getValidUrl(from: urlString) else { return }
            UIApplication.shared.open(url, options: [:])
            return
        }

    func getValidUrl(from url: String) -> URL? {
        switch self {
        case .vk:
            if let url = URL(string: url) {
                if !url.absoluteString.contains("https://") {
                    return URL(string: "https://\(url)")
                } else {
                    return url
                }
            }
        case .instagram:
            if let url = URL(string: url) {
                if !url.absoluteString.contains("https://www.") {
                    return URL(string: "https://www.\(url)")
                } else if !url.absoluteString.contains("https://www.instagram.com/") {
                    return URL(string: "https://www.instagram.com/\(url)")
                } else {
                    return url
                }
            }
        case .facebook:
            if let url = URL(string: url) {
                if !url.absoluteString.contains("https://www.facebook.com/") {
                    return URL(string: "https://www.facebook.com/\(url)")
                } else if !url.absoluteString.contains("https://www.") {
                    return URL(string: "https://www.\(url)")
                } else {
                    return url
                }
            }
        case .telegram:
            if let urlTelegram = URL(string: url) {
                if !urlTelegram.absoluteString.contains("https://") {
                    return URL(string: "https://\(url)")
                } else {
                    return urlTelegram
                }
            }
        }
        return URL(string: url)
    }
}
