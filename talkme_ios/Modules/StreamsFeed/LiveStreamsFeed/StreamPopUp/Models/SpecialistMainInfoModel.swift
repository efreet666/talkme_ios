//
//  SpecialistMainInfoModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 09.03.2021.
//

import Foundation

struct SpecialistMainInfoModel {

    enum Sex {
        case male
        case female
    }

    let fullName: String
    let age: String
    let avatarUrl: String
    let classNumber: String
    let sex: Sex
    let location: String
    let userName: String
    let id: Int
    let awardsNumber: String
    let rating: Double
    let isContact: Bool
}
