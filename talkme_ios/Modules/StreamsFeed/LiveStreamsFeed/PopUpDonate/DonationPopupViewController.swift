//
//  PopupDonateController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 04.03.2021.
//

import RxSwift

final class DonationPopupViewController: UIViewController {
    
    // MARK: - Private Properties
    
    private let blurEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        let blurEffect = UIBlurEffect(style: .light)
        view.effect = blurEffect
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .montserratExtraBold(ofSize: 24)
        label.textAlignment = .center
        label.text = "broadcast_completed".localized
        return label
    }()
    
    private let titleContainerLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.otherDarkViewColor
        label.font = .montserratFontRegular(ofSize: 15)
        label.textAlignment = .center
        label.text = "rate_the_broadcast_and_the_presenter".localized
        return label
    }()
    
    private let closeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "closeStream")
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.notGradient.withAlphaComponent(0.7)
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        return view
    }()
    
    private let ratingView = StreamRatingView()
    
    //    private let donationView = StreamDonationViewNew(frame: .zero, withRating: true)
    private let donationView: StreamDonationViewNew = {
        let view = StreamDonationViewNew(frame: .zero, withRating: false)
        view.isStreamEnded = true
        return view
    }()
    
    private let donationBT = GradientView(type: .thankCloseStream)
    private let bag = DisposeBag()
    private lazy var tapClose = closeImageView.rx.tapGesture().when(.recognized)
    private let viewModel: DonationPopupViewModel
    
    // MARK: - Initializers
    
    init(viewModel: DonationPopupViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        donationView.layer.cornerRadius = 14
        setupLayout()
        bindKeyboard()
        bindUI()
    }
    
    // MARK: - Private Methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    private func bindUI() {
        donationBT.tapView
            .bind { [weak self] _ in
                guard let self = self else { return }
                guard let text = self.donationView.textRelay.value else { return }
                if let tips = Int(text), (tips != 0) && !text.isEmpty {
                    self.viewModel.sendTips(tips: tips)
                }
                if let rating = self.ratingView.ratingRelay.value {
                    self.viewModel.sendRating(rating: rating)
                }
                self.viewModel.dismiss()
            }
            .disposed(by: bag)
        
        tapClose
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.dismiss()
            }
            .disposed(by: bag)
    }
    
    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let isHide = keyboardVisibleHeight == 0
                let offset: CGFloat = UIScreen.isSE ? 270 : 350
                self.donationView.snp.remakeConstraints { make in
                    make.bottom.equalTo(self.donationBT.snp.top).inset(!isHide ?  keyboardVisibleHeight - offset : -16)
                    make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 20 : 14)
                }
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }
    
    private func setupLayout() {
        view.addSubviews(blurEffectView)
        
        blurEffectView.contentView.addSubviews(titleLabel, containerView, donationView, donationBT, closeImageView)
        containerView.addSubviews(titleContainerLabel, ratingView)
        
        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(containerView.snp.top).inset(-20)
        }
        
        containerView.snp.makeConstraints { make in
            make.bottom.equalTo(donationView.snp.top).inset(-16)
            make.trailing.leading.equalToSuperview().inset(16)
            make.height.equalTo(125)
        }
        
        titleContainerLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(20)
            make.centerX.equalToSuperview()
        }
        
        ratingView.snp.makeConstraints { make in
            make.top.equalTo(titleContainerLabel.snp.bottom).offset(11)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 47 : 73)
            make.height.equalTo(44)
        }
        
        donationView.snp.makeConstraints { make in
            make.bottom.equalTo(donationBT.snp.top).inset(-16)
            let width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height) - (UIScreen.isSE ? 20 : 14) * 2
            make.width.equalTo(width)
        }
        
        donationBT.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(UIScreen.isSE
                                                 ? UIScreen.height / 14
                                                 : UIScreen.height / 6
            )
            make.trailing.leading.equalToSuperview().inset(16)
            make.height.equalTo(58)
        }
        
        closeImageView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(44)
            make.size.equalTo(18)
        }
    }
}
