//
//  DonationPopupViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 12.03.2021.
//

import RxCocoa
import RxSwift

final class DonationPopupViewModel {

    enum Flow {
        case dismiss
    }

    // MARK: - Public Properties

    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()
    let lessonId: Int     // TODO: lessonId (flow)
    let lessonIsSaved: Bool

    // MARK: - Private Properties

    private let service: LessonsServiceProtocol

    // MARK: - Initializers

    init(service: LessonsServiceProtocol, lessonId: Int, lessonIsSaved: Bool) {
        self.service = service
        self.lessonId = lessonId
        self.lessonIsSaved = lessonIsSaved
    }

    // MARK: - Public Methods

    func dismiss() {
        flow.accept(.dismiss)
    }

    func sendTips(tips: Int) {
        service
            .tips(tips: tips, lessonId: self.lessonId, lessonIsSaved: self.lessonIsSaved)
            .subscribe()
            .disposed(by: bag)
    }

    func sendRating(rating: Int) {
        let request = LeaveRatingRequest(lessonId: self.lessonId, rating: rating)
        service
            .leaveRating(request: request)
            .subscribe()
            .disposed(by: bag)
    }
}
