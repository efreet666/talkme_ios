//
//  StreamChatMessageView.swift
//  talkme_ios
//
//  Created by Den on 13.03.2021.
//

import UIKit

final class StreamChatMessageView: CommonMessageCell {

    private enum Constants {
        static let avatarOrigin = CGPoint(x: 8, y: UIScreen.isSE ? 7 : 13)
        static let avatarSize = CGSize(width: UIScreen.isSE ? 33 : 36, height: UIScreen.isSE ? 33 : 36)
        static let textLeftOffset: CGFloat = 4
        static let titleTopOffset: CGFloat = UIScreen.isSE ? 6 : 9
        static let titleHeight: CGFloat = 19
        static let messageTextTopOffset: CGFloat = 2
        static let messageTextBottomOffset: CGFloat = UIScreen.isSE ? 6 : 7
        static let reservedWidth: CGFloat = avatarOrigin.x + avatarSize.width + 2 * textLeftOffset
    }

    // MARK: - Private Properties

    private let messageImageView: UIImageView = {
        let view = UIImageView(frame: CGRect(origin: Constants.avatarOrigin, size: Constants.avatarSize))
        view.layer.masksToBounds = true
        view.backgroundColor = TalkmeColors.avatarBG
        view.layer.cornerRadius = Constants.avatarSize.height / 2
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        let originX = messageImageView.frame.maxX + Constants.textLeftOffset
        label.frame = CGRect(
            x: originX, y: Constants.titleTopOffset,
            width: bounds.width - Constants.reservedWidth, height: 0)
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 14)
        label.textColor = TalkmeColors.messageAuthor
        return label
    }()

    private lazy var messageTextLabel: UILabel = {
        let label = UILabel()
        let originX = messageImageView.frame.maxX + Constants.textLeftOffset
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 14)
        label.textColor = TalkmeColors.messageText
        label.numberOfLines = 0
        return label
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
        configureLayout()
        backgroundColor = TalkmeColors.messageBG
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func configure(model: LessonMessage) {
        if let imageURL = model.avatar {
            messageImageView.contentMode = .scaleAspectFill
            messageImageView.kf.setImage(with: URL(string: imageURL))
        } else {
            messageImageView.contentMode = .center
            messageImageView.image = UIImage(named: "emptyAvatar")
        }
        titleLabel.text = model.author
        messageTextLabel.text = model.content
        updateLayout(with: model)
    }

    // MARK: - Private Methods

    private func configureView() {
        layer.masksToBounds = true
        layer.cornerRadius = 8
        transform = CGAffineTransform(scaleX: 1, y: -1)
    }

    private func configureLayout() {
        addSubviews(messageImageView, titleLabel, messageTextLabel)
    }

    private func updateLayout(with model: LessonMessage) {
        titleLabel.numberOfLines = 0
        titleLabel.frame.size.height = Self.height(from: model, for: bounds.width)
        titleLabel.sizeToFit()
        messageTextLabel.frame = CGRect(x: messageImageView.frame.maxX + Constants.textLeftOffset, y: titleLabel.frame.maxY + Constants.messageTextTopOffset, width: bounds.width - Constants.reservedWidth, height: 0)
        messageTextLabel.frame.size.height = Self.height(from: model, for: bounds.width)
    }
}

// MARK: - SelfSizing

extension StreamChatMessageView {
    static var heightCache: [String: CGFloat] = [:]
    static var heightLableCache: [String: CGFloat] = [:]

    static func height(from model: LessonMessage, for width: CGFloat) -> CGFloat {
        if let cachedTextHeight = heightCache[model.content] {
            return cachedTextHeight
        } else {
            let textHeight = model.content.height(
                font: .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 14),
                width: width - Constants.reservedWidth)
            heightCache[model.content] = textHeight
            return textHeight
        }
    }

    static func heightTitle(from model: LessonMessage, for width: CGFloat) -> CGFloat {
        if let cachedTextHeight = heightLableCache[model.author] {
            return cachedTextHeight
        } else {
            let textHeight = model.author.height(
                font: .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 14),
                width: width - Constants.reservedWidth)
            heightLableCache[model.author] = textHeight
            return textHeight
        }
    }

    static func cellHeight(from model: LessonMessage, for width: CGFloat) -> CGFloat {
        let otherHeights = Constants.titleTopOffset + Constants.messageTextTopOffset + Constants.messageTextBottomOffset
        let minHeight = 2 * Constants.avatarOrigin.y + Constants.avatarSize.height
        let messageHeight = Self.height(from: model, for: width)
        let titleHeight = Self.heightTitle(from: model, for: width)
        return max(otherHeights + messageHeight + titleHeight, minHeight)
    }
}
