//
//  StreamFeedActionsView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 06.01.2021.
//

import RxSwift
import RxCocoa
import CollectionKit
import UIKit

final class StreamFeedActionsView: UIView {

    private enum Constants {
        static let circleWidth: CGFloat = UIScreen.isSE ? 52 : 70
        static let circleSize: CGSize = .init(width: circleWidth, height: circleWidth)

        static let paginationHeight: CGFloat = UIScreen.isSE ? 40 : 46
        static let footerHeight: CGFloat = UIScreen.isSE ? 36 : 52
        static let actionViewBottom: CGFloat = UIScreen.isSE ? 13 : 15
        static let backButtonInset: CGFloat = UIScreen.isSE ? 13 : 11
        static let rightOffset: CGFloat = UIScreen.isSE ? 9 : 15
        static let chatLeftInset: CGFloat = UIScreen.isSE ? 12 : 13
        static var chatSize: CGSize { CGSize(width: 216, height: 300) }
        static let sendMessageHeight: CGFloat = UIScreen.isSE ? 45 : 58

        static let viewersHintOffsetV: CGFloat = UIScreen.isSE ? 6 : 5
        static let viewersHintOffsetH: CGFloat = UIScreen.isSE ? 4 : 10

        static let membersMicroOffsetH: CGFloat = 6
        static let membersLearnerOffsetV: CGFloat = UIScreen.isSE ? 5 : 5
        static let membersSpecialistOffsetV: CGFloat = 4

        static let topViewOffset: CGFloat = 10

        static let complaintButtonSize = CGSize(width: UIScreen.isSE ? 26 : 28,
                                                height: UIScreen.isSE ? 26 : 28)

        static let pageIndicatorSize = UIScreen.isSE
            ? CGSize(width: 27, height: 2)
            : CGSize(width: 35, height: 3)
    }

    // MARK: - Public Properties

    private(set) lazy var isCameraOn = actionsView.isCameraOn
    private(set) lazy var isMicroOn = actionsView.isMicroOn

    private(set) lazy var onCameraTap = actionsView.onCameraTap
    private(set) lazy var onMicrophoneTap = actionsView.onMicrophoneTap
    private(set) lazy var onFlipCameraTap = actionsView.onFlipCameraTap
    private(set) lazy var onBackButtonTap = backButton.rx.tap
    private(set) lazy var onGiftButtonTap = actionsView.onGiftTap
    private(set) lazy var onFullScreenTap = actionsView.onFullScreenTap
    private(set) lazy var onMembersTap = actionsView.onViewersTap
    private(set) lazy var onFetchMoreMessages = chatView.onFetchMoreMessages
    let onShowPopUp = PublishRelay<Void>()
    let onShowMembersPopUp = PublishRelay<Void>()
    lazy var hintView = StreamHintView()
    private(set) lazy var sendMessage = sendMessageView.sendMessage
    private(set) lazy var sendComplaint = complaintButton.rx.tapGesture().when(.recognized)

    let streamViewers = PublishRelay<Int>()
    private(set) var bag = DisposeBag()
    let orientationIsPortrait = PublishRelay<Bool>()
    let orientationIsPortraitViewer = PublishRelay<Bool>()
    private(set) lazy var lessonFinished = footerView.lessonFinished

    // MARK: - Private Properties

    private let backButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "closeStream"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        btn.contentHorizontalAlignment = .fill
        btn.contentVerticalAlignment = .fill
        btn.imageEdgeInsets = .init(
            top: Constants.backButtonInset,
            left: Constants.backButtonInset,
            bottom: Constants.backButtonInset,
            right: Constants.backButtonInset)
        return btn
    }()

    private let actionsView: StreamSideActionsView
    private lazy var footerView = StreamFeedFooterView(frame: footerFrame(for: UIScreen.size), userType: userType)
    private let swipeZoneView = UIView()

    private var sendMessageStartFrame: CGRect {
        CGRect(x: 0, y: UIScreen.height,
               width: UIScreen.width, height: Constants.sendMessageHeight)
    }

    private lazy var sendMessageView: StreamSendMessageView = {
        let view = StreamSendMessageView(frame: sendMessageStartFrame)
        view.alpha = 0
        view.isHidden = true
        return view
    }()

    private lazy var chatView = StreamChatView()

    private var isHeightExceeded: Bool {
        self.sendMessageView.frame.height > 98
    }

    private let pagingView = UIView()

    var buttonsCount: Int {
        switch userType {
        case .participant:
            return 4
        case .viewer:
            // TODO: будет 3, если обычный зритель не будет видеть участников, и в попапе убрать эту ячейку
            return 4
        case .specialist:
            return 2
        }
    }

    private lazy var dataSourceButtonView: [GenericViewType] = Array(
        repeating: GenericViewType.pageControl, count: buttonsCount)
    private lazy var pageIndicator = RepeatingViewsMakerView(
        viewsData: dataSourceButtonView,
        viewType: StreamPageControlView.self,
        size: Constants.pageIndicatorSize,
        spacing: 7)

    private let userType: UserType

    private var keyboardIsVisible = false

    private let classLabel: RoundedLabel = {
        let label = RoundedLabel(
            withInsets: 0,
            0,
            UIScreen.isSE ? 15 : 23,
            UIScreen.isSE ? 15 : 23
        )
        label.textAlignment = .center
        label.textColor = .white
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        label.isHidden = true
        return label
    }()

    private var complaintButton: SideActionButtonView = {
        let button = SideActionButtonView(type: .complaint, size: Constants.complaintButtonSize)
        button.contentMode = .scaleAspectFit
        return button
    }()

    private var isFirstStreamPresenting = false
    private var isPortrait = true
    private var swipesIsEnabled = true

    // MARK: - Initializers

    init(type: UserType, classNumber: String?) {
        userType = type
        actionsView = .init(type: type)
        super.init(frame: .zero)
        if let numberClass = classNumber {
            setupClassLabel(numberClass)
        }
        complaintButton.isHidden = (userType == .specialist)
        setupLayout()
        updateOrientationUI()
        if !UserDefaultsHelper.shared.wasHintShown {
            addSubview(hintView)
            setupHintView()
            UserDefaultsHelper.shared.wasHintShown = true
            isFirstStreamPresenting = true
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overriden Methods

    override func layoutSubviews() {
        super.layoutSubviews()

        let pagingViewOrigin = CGPoint(x: 0, y: UIScreen.height - Constants.paginationHeight)
        let pagingViewSize = CGSize(width: UIScreen.width, height: Constants.paginationHeight)
        pagingView.frame = .init(origin: pagingViewOrigin, size: pagingViewSize)

        pageIndicator.frame.origin = CGPoint(
            x: (pagingViewSize.width - pageIndicator.frame.width) / 2,
            y: (pagingViewSize.height - pageIndicator.frame.height) / 2
        )
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard
            traitCollection.verticalSizeClass != previousTraitCollection?.verticalSizeClass
            || traitCollection.horizontalSizeClass != previousTraitCollection?.horizontalSizeClass
        else {
            return
        }

        updateOrientationUI()

        setNeedsUpdateConstraints()
    }

    override func updateConstraints() {
        super.updateConstraints()

        let isPortrait = UIApplication.shared.statusBarOrientation.isPortrait

        footerView.snp.remakeConstraints { make in
            make.leading.equalTo(safeAreaLayoutGuide)
            make.bottom.equalTo(self).offset(-Constants.paginationHeight)
            make.height.equalTo(Constants.footerHeight)

            if isPortrait {
                make.trailing.equalTo(self)
            } else {
                make.trailing.equalTo(actionsView.snp.leading)
            }
        }

        actionsView.snp.remakeConstraints { make in
            make.trailing.equalTo(self).offset(-Constants.rightOffset)

            if isPortrait {
                make.bottom.equalTo(footerView.snp.top).offset(-Constants.actionViewBottom)
            } else {
                make.bottom.equalTo(footerView.snp.bottom)
            }
        }

        chatView.snp.remakeConstraints { make in
            make.leading.equalTo(safeAreaLayoutGuide).offset(Constants.chatLeftInset)
            make.bottom.equalTo(footerView.snp.top)
            if isPortrait {
                make.size.equalTo(Constants.chatSize)
            } else {
                make.width.equalTo(Constants.chatSize.width)
                make.top.equalTo(backButton.snp.bottom).offset(10)
            }
        }
    }

    // MARK: - Public Methods

    func configure(streamModel: StreamModel) {
        if let numberClass = streamModel.specialistInfo?.numberClass {
            setupClassLabel(numberClass)
        }
        bindUI()
        setButtonsHidden(false)
        pageIndicator.viewsArray[0].setSelected(true)
        addNotificationObservers()
        chatView.isHidden = false
    }

    func  startLiveStreamTimer(withEndDate endDate: Double) {
        footerView.startLiveStreamTimer(withEndDate: endDate)
    }

    func  startSavedStreamTimer(withDuration duration: Double) {
        footerView.startSavedStreamTimer(withDuration: duration)
    }

    func invalidateLiveStreamTimer() {
        footerView.invalidateLiveStreamTimer()
    }

    func hideComplaintButtton(isHidden: Bool) {
        complaintButton.isHidden = isHidden
    }

    func hideTimer(isHidden: Bool) {
        footerView.hideTimer(isHidden: isHidden)
    }

    func hideChatMessages() {
        chatView.isHidden = true
    }

    func setupChats(dataSource: ArrayDataSource<LessonMessage>) {
        chatView.setupCollectionView(dataSource: dataSource)
    }

    func updateSubviewsFrames(for size: CGSize, isLandscape: Bool) {
        sendMessageView.updateFrames()
        footerView.updateFrames(isLandscape: isLandscape)
    }

    func setVipLessonState(_ isActive: Bool) {
        swipesIsEnabled = !isActive
        setButtonsHidden(isActive)
        backButton.isHidden = isActive
    }

    func setButtonsToActiveStates() {
        actionsView.isMicroOn.accept(true)
        actionsView.isCameraOn.accept(true)
    }

    func hideFullScreenButton(isHidden: Bool) {
        actionsView.hideFullScreenButton(isHidden: isHidden)
    }

    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Private Methods

    private func updateOrientationUI() {
        let isPortrait = UIApplication.shared.statusBarOrientation.isPortrait
        swipeZoneView.isUserInteractionEnabled = isPortrait
        pagingView.isHidden = !isPortrait

        sendMessageView.frame = sendMessageStartFrame
    }

    private func setupConstaints() {
        addSubview(backButton)
    }

    private func setupClassLabel(_ number: String) {
        classLabel.text = "№ \(number)"
        classLabel.sizeToFit()
        classLabel.isHidden = true // Пока скрываем везде
//        guard let timeEnd = UserDefaultsHelper.shared.cameraTimeEnd, Date().timeIntervalSince1970 < timeEnd else {
//            if !isFirstStreamPresenting {
//                classLabel.isHidden = false
//            }
//            return }
    }

    private func bindUI() {
        bag = .init()

        swipeZoneView
            .rx
            .swipeGesture(.right)
            .when(.recognized)
            .filter { [weak self] _ in self?.keyboardIsVisible == false }
            .bind { [weak self] _ in
                guard let self = self, self.swipesIsEnabled else { return }
                guard self.actionsView.alpha != 0 else { return }
                self.setButtonsHidden(true)
            }
            .disposed(by: bag)

        switch userType {
        case .participant, .viewer:
            swipeZoneView
                .rx
                .swipeGesture(.left)
                .when(.recognized)
                .filter { [weak self] _ in self?.keyboardIsVisible == false }
                .bind { [weak self] _ in
                    guard let self = self, self.swipesIsEnabled else { return }
                    guard self.actionsView.alpha != 0 else {
                        self.setButtonsHidden(false)
                        return
                    }
                    guard self.isPortrait else { return }
                    self.onShowPopUp.accept(())
                }
                .disposed(by: bag)
        case .specialist:
            swipeZoneView
                .rx
                .swipeGesture(.left)
                .when(.recognized)
                .filter { [weak self] _ in self?.keyboardIsVisible == false }
                .bind { [weak self] _ in
                    guard self?.actionsView.alpha != 0 else {
                        self?.setButtonsHidden(false)
                        return
                    }
                    guard let isPoratait = self?.isPortrait, isPoratait else { return }
                    self?.onShowMembersPopUp.accept(())
                }
                .disposed(by: bag)
        }

            rx
            .tapGesture()
            .when(.recognized)
            .filter { [weak self] recognizer in
                guard let self = self else { return false }
                let point = recognizer.location(in: self.sendMessageView)
                return point.y < 0 && self.keyboardIsVisible
            }
            .bind { [weak self] _ in
                self?.sendMessageView.endEditing()
            }
            .disposed(by: bag)

        footerView
            .onCommentTap
            .bind { [weak self] _ in
                self?.sendMessageView.startEditing()
            }
            .disposed(by: bag)

        footerView
            .onMessageButtonTap
            .bind { [weak self] _ in
                self?.chatView.isHidden.toggle()
                self?.footerView.toggleCommentView()
            }
            .disposed(by: bag)

        // RxKeyboard biinds to multipal cells so its buggy now - add tags to remove bugs and then uncommit code below

//        RxKeyboard.instance.visibleHeight.drive(onNext: { [weak self] height in
//            let isVisible = height.isZero
//            guard let self = self else { return }
//            self.keyboardIsVisible = !isVisible
//            let sendMessageStartFrame = self.sendMessageStartFrame
//            let sendMessageOriginY = isVisible ? sendMessageStartFrame.origin.y
//                : UIScreen.main.bounds.height - height - sendMessageStartFrame.height
//            UIView.visible((isVisible ? .hide : .show), views: [self.sendMessageView], withDuration: 0.3) {
//                self.sendMessageView.frame.origin.y = self.isHeightExceeded ? sendMessageOriginY - 40 : sendMessageOriginY
//            }
//        }).disposed(by: bag)

        streamViewers
            .bind { [weak self] count in
                self?.footerView.setViewersCount(count: count)
            }
            .disposed(by: bag)

        orientationIsPortrait
            .bind { [weak self] isActive in
                self?.isPortrait = isActive
                self?.sendMessageView.deviceOrientationChanged(isPortrait: isActive)
            }
            .disposed(by: bag)

    }

    private func setButtonsHidden(_ isHidden: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.actionsView.alpha = isHidden ? 0 : 1
            self.footerView.alpha = isHidden ? 0 : 1
            self.chatView.alpha = isHidden ? 0 : 1
        }
    }

    private func footerFrame(for screenSize: CGSize) -> CGRect {
        let originX: CGFloat = UIApplication.leftInset
        let bottomInsetLandscape = UIApplication.bottomInset == 0 ? 20 : UIApplication.bottomInset
        let originY = UIDevice.current.orientation.isLandscape
            ? screenSize.height - bottomInsetLandscape - Constants.footerHeight
            : screenSize.height - Constants.paginationHeight - Constants.footerHeight
        let footerOrigin = CGPoint(x: originX, y: originY)
        let footerSize = CGSize(width: screenSize.width, height: Constants.footerHeight)
        return .init(origin: footerOrigin, size: footerSize)
    }

    private func setupLayout() {
        backgroundColor = .clear
        addSubviews(swipeZoneView,
                    backButton,
                    actionsView,
                    chatView,
                    footerView,
                    pagingView,
                    classLabel,
                    sendMessageView,
                    complaintButton)
        pagingView.addSubviews(pageIndicator)

        swipeZoneView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self)
            make.leading.equalTo(self).offset(20)
            make.trailing.equalTo(self)
            make.bottom.equalTo(self)
        }

        backButton.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).offset(UIScreen.isSE ? 7 : 10)
            make.leading.equalTo(safeAreaLayoutGuide).offset(UIScreen.isSE ? 0 : 5)
            make.height.width.equalTo(40)
        }

        complaintButton.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).offset(UIScreen.isSE ? 17 : 19)
            make.trailing.equalTo(self).offset(-Constants.rightOffset)
            make.size.equalTo(Constants.complaintButtonSize)
        }

        classLabel.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).offset(UIScreen.isSE ? 11 : 13)
            make.trailing.equalTo(complaintButton.snp.leading).offset(UIScreen.isSE ? -9 : -11)
            make.height.equalTo(UIScreen.isSE ? 33 : 42)
        }
    }

    func setupHintView() {
        bindHintView()
        hintView.frame = .init(origin: .zero, size: UIScreen.size)
        switch userType {
        case .participant, .viewer:
            setupHintForLearner()
        case .specialist:
            setupHintForSpecialist()
        }

        hintView.setupViewersHint(originWithoutSize: CGPoint(
                        x: UIScreen.width - Constants.viewersHintOffsetH,
                        y: footerView.frame.minY - Constants.viewersHintOffsetV))

        let backBtnHintX: CGFloat = 12
        let backBtnHintOrigin = CGPoint(
            x: UIScreen.isSE
               ? backButton.frame.origin.x - backBtnHintX + 6
               : backButton.frame.origin.x - backBtnHintX,
            y: UIScreen.isSE
               ? backButton.frame.origin.y - (Constants.circleWidth - backButton.frame.height) / 2
               : backButton.frame.origin.y - (Constants.circleWidth - backButton.frame.height) / 2.7)
        hintView.setupExitButtonHint(origin: backBtnHintOrigin)
    }

    private func bindHintView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideHintView))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.isEnabled = true
        tapGesture.cancelsTouchesInView = false
        hintView.addGestureRecognizer(tapGesture)
    }

    @objc func hideHintView() {
        self.hintView.isHidden = true
        self.hintView.removeFromSuperview()
        isFirstStreamPresenting = false
    }

    private func setupHintForSpecialist() {
        let actionsContainerframe = actionsView.selfFrame
        let flipCameraFrame = actionsView.topViewButtonFrame
        let offsetH = (Constants.circleWidth - flipCameraFrame.width) / 2
        let flipCameraViewOrigin = UIScreen.isSE ? CGPoint(
            x: actionsContainerframe.minX + flipCameraFrame.minX - offsetH,
            y: actionsContainerframe.minY + flipCameraFrame.minY - Constants.membersSpecialistOffsetV)
            :  CGPoint(x: actionsContainerframe.minX + flipCameraFrame.minX - offsetH + 3,
            y: actionsContainerframe.minY + flipCameraFrame.minY - Constants.membersSpecialistOffsetV)
        hintView.setupFlipCameraHint(origin: flipCameraViewOrigin)

        let membersFrame = actionsView.membersWithMicroButtonFrame
        let membersOrigin = UIScreen.isSE ? CGPoint(
            x: actionsContainerframe.minX + flipCameraFrame.minX - offsetH,
            y: actionsContainerframe.minY + membersFrame.minY - Constants.membersSpecialistOffsetV)
            :  CGPoint(x: actionsContainerframe.minX + flipCameraFrame.minX - offsetH + 3,
            y: actionsContainerframe.minY + membersFrame.minY - Constants.membersSpecialistOffsetV)
        hintView.setupMembersFor(type: .specialist, origin: membersOrigin)
    }

    private func setupHintForLearner() {
        hintView.setupSwipeRightHint()
        hintView.setupSwipeUpHint()

        let actionsContainerframe = actionsView.selfFrame
        let membersFrame = actionsView.membersWithMicroButtonFrame
        let membersOrigin = CGPoint(
            x: actionsContainerframe.minX + membersFrame.minX - Constants.membersMicroOffsetH,
            y: actionsContainerframe.minY + membersFrame.minY - Constants.membersSpecialistOffsetV)
        hintView.setupMembersFor(type: .participant, origin: membersOrigin)

        let giftFrame = actionsView.topViewButtonFrame
        let giftViewOrigin = CGPoint(
            x: actionsContainerframe.minX + giftFrame.minX - Constants.topViewOffset,
            y: actionsContainerframe.minY + giftFrame.minY - Constants.topViewOffset)
        hintView.setupGiftHint(origin: giftViewOrigin)
    }

    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

     @objc private func keyboardWillShow(_ notification: Notification) {
        changeCustomTextFieldOrigin(becauseOf: notification, andHideIt: false)
    }

    @objc private func keyboardWillHide(_ notification: Notification) {
        changeCustomTextFieldOrigin(becauseOf: notification, andHideIt: true)
    }

    private func changeCustomTextFieldOrigin(becauseOf notification: Notification, andHideIt textFieldshouldBeHidden: Bool) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height

            self.keyboardIsVisible = !textFieldshouldBeHidden
            let sendMessageStartFrame = self.sendMessageStartFrame
            let sendMessageOriginY = isHidden ? sendMessageStartFrame.origin.y
            : UIScreen.main.bounds.height - keyboardHeight - sendMessageStartFrame.height
            self.sendMessageView.isHidden = textFieldshouldBeHidden
            UIView.animate(withDuration: 0.3) {
                self.sendMessageView.alpha = textFieldshouldBeHidden ? 0 : 1
                let endYPointForShowing = self.isHeightExceeded ? sendMessageOriginY - 40 : sendMessageOriginY
                self.sendMessageView.frame.origin.y = textFieldshouldBeHidden ? UIScreen.height : endYPointForShowing
            }
        }
    }
}
