//
//  StreamFeedViewModel.swift
//  talkme_ios
//
//  Created by Elina Efremova on 13.01.2021.
//

import RxCocoa
import RxSwift
import AVFoundation

struct StreamModel {
    let id: Int
    var specialistInfo: Owner?
    let lessonDateEnd: Double?
    let categoryId: Int
}

final class StreamFeedViewModel {

    var streamModel: StreamModel
    let bag = DisposeBag()

    init(streamModel: StreamModel) {
        self.streamModel = streamModel
    }
}
