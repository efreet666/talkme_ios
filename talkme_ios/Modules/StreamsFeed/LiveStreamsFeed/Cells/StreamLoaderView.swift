//
//  StreamLoaderView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 16.06.2021.
//

import UIKit

final class StreamLoaderView: UIView {

    // MARK: - Private Properties

    let circle1 = UIView(frame: CGRect(x: 19, y: 0, width: 17, height: 17))
    let circle2 = UIView(frame: CGRect(x: -19, y: 0, width: 17, height: 17))

    let positions: [CGRect] = [
        CGRect(x: 19, y: 0, width: 17, height: 17),
        CGRect(x: 19, y: 0, width: 17, height: 17),
        CGRect(x: -19, y: 0, width: 17, height: 17),
        CGRect(x: -19, y: 0, width: 17, height: 17)
    ]

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure() {
        addSubviews(circle1, circle2)
        translatesAutoresizingMaskIntoConstraints = false

        circle1.backgroundColor = TalkmeColors.white
        circle1.layer.cornerRadius = circle1.frame.width / 2
        circle1.layer.zPosition = 2

        circle2.backgroundColor = TalkmeColors.blueCollectionCell
        circle2.layer.cornerRadius = circle2.frame.width / 2
        circle2.layer.zPosition = 1

    }

    func animate(circle: UIView, counter: Int) {
        var counter = counter

        UIView.animate(withDuration: 0.45, delay: 0, options: .curveLinear, animations: {
            circle.frame = self.positions[counter]
            circle.layer.cornerRadius = circle.frame.width / 2

            switch counter {
            case 1:
                if circle == self.circle1 {
                    self.circle1.layer.zPosition = 2
                }
            case 3:
                if circle == self.circle1 {
                    self.circle1.layer.zPosition = 0
                }
            default:
                break
            }
        }) { _ in
            switch counter {
            case 0...2:
                counter += 1
            case 3:
                counter = 0
            default:
                break
            }
            self.animate(circle: circle, counter: counter)
        }
    }

    func stopAnimating() {
        circle1.layer.removeAllAnimations()
        circle2.layer.removeAllAnimations()
    }
}
