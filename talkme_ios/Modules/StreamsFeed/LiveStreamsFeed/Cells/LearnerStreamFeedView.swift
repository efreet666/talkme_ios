//
//  LearnerStreamFeedView.swift
//  talkme_ios
//
//  Created by 1111 on 01.07.2021.
//

import RxCocoa
import RxSwift
import OpenTok

final class LearnerStreamFeedView: UIView {

    private enum Constants {
        static let smallPublisherViewWidth = UIScreen.isSE ? 113 : 132
        static let smallPublisherViewHeight = UIScreen.isSE ? 163 : 191
        static let footerHeight: CGFloat = UIScreen.isSE ? 36 : 52
    }

    // MARK: - Public Properties

    private(set) lazy var isCameraOn = actionsView.isCameraOn
    private(set) lazy var isMicroOn = actionsView.isMicroOn

    private(set) lazy var isLearnerCameraOn = PublishRelay<Bool>()
    private(set) lazy var isLearnerMicroOn = PublishRelay<Bool>()
    private(set) lazy var isCameraButtonActive = smallPublisherView?.isCameraButtonActive
    private(set) lazy var isMicroButtonActive = smallPublisherView?.isMicroButtonActive

    private(set) lazy var onCameraTap = actionsView.onCameraTap
    private(set) lazy var onMicrophoneTap = actionsView.onMicrophoneTap
    private(set) lazy var onBackButtonTap = actionsView.onBackButtonTap
    private(set) lazy var onGiftButtonTap = actionsView.onGiftButtonTap
    private(set) lazy var onMembersTap = actionsView.onMembersTap
    private(set) lazy var sendMessage = actionsView.sendMessage
    private(set) lazy var onShowPopUp = actionsView.onShowPopUp
    private(set) lazy var onShowMembersPopUp = actionsView.onShowMembersPopUp
    private(set) lazy var orientationIsPortrait = actionsView.orientationIsPortrait
    private(set) lazy var onFetchMoreMessages = actionsView.onFetchMoreMessages
    private var showAvatarFromTeacher = false
    private var showAvatarFromLearner = false
    private var loaderView: StreamLoaderView?

    let hasCameraPermissions = PublishRelay<Bool>()
    let hasMicrophonePermissions = PublishRelay<Bool>()
    let onBoughtCameraTimeIsUp = PublishRelay<Void>()
    let onCameraMessage = PublishRelay<Bool>()
    let onMicrophoneMessage = PublishRelay<Bool>()

    let newMessage = PublishSubject<LessonChatMessage>()
    let onDeviceOrientationChanged = PublishRelay<CGSize>()
//    let streamViewers = PublishRelay<StreamTimeViewWersModel>()
    private(set) var bag = DisposeBag()
    private(set) lazy var lessonFinished = actionsView.lessonFinished

    // MARK: - Private Properties

    private var streamView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFill
        return iv
    }()

    private let actionsView: StreamFeedActionsView
    private var mainSubscriberPublisherView: UIView?
    private var smallPublisherView: StreamMemberCameraView?
    private var avatarImage: UIImage?
    private var publisher: OTPublisher?
    private var learnerCameraBag = DisposeBag()

    // MARK: - Initializers

    init(classNumber: String?) {
        actionsView = StreamFeedActionsView(type: .participant, classNumber: classNumber)
        super.init(frame: .zero)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func smallPublisherCameraIsHidden(_ isHidden: Bool) {
        smallPublisherView?.isHidden = isHidden
    }

    func showLoader() {
        let loaderView = StreamLoaderView()
        addSubview(loaderView)
        loaderView.center.x = streamView.center.x
        loaderView.center.y = streamView.center.y
        loaderView.animate(circle: loaderView.circle1, counter: 1)
        loaderView.animate(circle: loaderView.circle2, counter: 3)
        self.loaderView = loaderView
    }

    func hideLoader() {
        guard let loaderView = self.loaderView else { return }
        loaderView.stopAnimating()
        loaderView.removeFromSuperview()
    }

    func configure(specialistInfo: Owner?, streamUrlString: String, token: String?) {
        bag = .init()
        setAvatar(specialistInfo?.avatarUrl)
//        actionsView.configure(numberClass: specialistInfo?.numberClass)
        bindUI()
        configureConference(subscriberView: nil)
    }

    func configureConference(subscriberView: UIView?) {
        guard let subscriberView = subscriberView else { return }
        streamView.subviews.forEach { $0.removeFromSuperview() }
        streamView.addSubview(subscriberView)
        subscriberView.frame = .init(origin: .zero, size: UIScreen.size)
        mainSubscriberPublisherView = subscriberView
        mainSubscriberPublisherView?.backgroundColor = .clear
    }

    func showFullscreenAvatarPlaceholder(_ show: Bool) {
        showAvatarFromLearner = show
        if show {
            mainSubscriberPublisherView?.isHidden = show
        } else {
            guard showAvatarFromTeacher == show else { return }
            mainSubscriberPublisherView?.isHidden = show
        }
    }

    func hideVideoByTeacher(_ hide: Bool) {
        showAvatarFromTeacher = hide
        if hide {
            mainSubscriberPublisherView?.isHidden = hide
        } else {
            guard showAvatarFromLearner == hide else { return }
            mainSubscriberPublisherView?.isHidden = hide
        }
    }

    func showPublisherAfterBuyCamera(publisherModel: LearnerPublisher, showPublisher: Bool, avatarUrl: String) {
        self.smallPublisherView?.isHidden = !showPublisher
        self.publisher = publisherModel.publisher
        self.publisher?.publishVideo = false
        self.publisher?.publishAudio = false
        guard let publisherView = publisherModel.publisher?.view else { return }
        let origin = CGPoint(
            x: UIScreen.width - CGFloat(Constants.smallPublisherViewWidth) - (UIScreen.isSE ? 10 : 16) ,
            y: Constants.footerHeight
        )
        let size = CGSize(width: Constants.smallPublisherViewWidth, height: Constants.smallPublisherViewHeight)
        let smallPublisherView = StreamMemberCameraView(
            frame: .init(origin: origin, size: size),
            publisherView: publisherView,
            avatarUrl: avatarUrl,
            timeEnd: publisherModel.timeEnd)
        actionsView.addSubview(smallPublisherView)
        self.smallPublisherView = smallPublisherView

        smallPublisherView
            .onCameraTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.onCameraMessage.accept(smallPublisherView.isCameraOn.value)
            }
            .disposed(by: learnerCameraBag)

        smallPublisherView
            .onMicrophoneTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.onMicrophoneMessage.accept(smallPublisherView.isMicrophoneOn.value)
            }
            .disposed(by: learnerCameraBag)

        smallPublisherView
            .onTimeIsUp
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.onBoughtCameraTimeIsUp.accept(())
                self.removeSmallPublisherView()
            }
            .disposed(by: learnerCameraBag)

        isLearnerCameraOn
            .bind(to: smallPublisherView.isCameraOn)
            .disposed(by: learnerCameraBag)

        isLearnerMicroOn
            .bind(to: smallPublisherView.isMicrophoneOn)
            .disposed(by: learnerCameraBag)
    }

    func removeSmallPublisherView() {
        smallPublisherView?.removeFromSuperview()
        smallPublisherView = nil
        learnerCameraBag = .init()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews(streamView, actionsView)
        bringSubviewToFront(actionsView)
        updateFrames(for: UIScreen.size)
    }

    private func setAvatar(_ coverImage: String?) {
        streamView.image = nil
        avatarImage = nil
        guard let coverImage = coverImage, let imageUrl = URL(string: coverImage) else { return }
        streamView.kf.setImage(with: imageUrl) { [weak self] result in
            switch result {
            case .success(let image):
                self?.avatarImage = image.image
            case .failure:
                break
            }
        }
    }

    private func bindUI() {
        onDeviceOrientationChanged
            .do(onNext: { [weak self] _ in
                self?.streamView.image = nil
            }, afterNext: { [weak self] _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self?.streamView.image = self?.avatarImage
                }
            })
            .bind { [weak self] size in
                self?.updateFrames(for: size)
            }
            .disposed(by: bag)
    }

    private func updateFrames(for size: CGSize) {
        mainSubscriberPublisherView?.frame = .init(origin: .zero, size: size)
        streamView.frame = .init(origin: .zero, size: size)
        actionsView.frame = .init(origin: .zero, size: size)
        let smallPublisherViewOrigin = CGPoint(
            x: UIScreen.width - CGFloat(Constants.smallPublisherViewWidth) - (UIScreen.isSE ? 10 : 38) ,
            y: Constants.footerHeight
        )
        let smallPublisherViewSize = CGSize(
            width: Constants.smallPublisherViewWidth,
            height: Constants.smallPublisherViewHeight
        )
        smallPublisherView?.frame = .init(origin: smallPublisherViewOrigin, size: smallPublisherViewSize)
        actionsView.updateSubviewsFrames(for: size, isLandscape: UIDevice.current.orientation.isLandscape)
        guard smallPublisherView != nil else { return }
    }
}
