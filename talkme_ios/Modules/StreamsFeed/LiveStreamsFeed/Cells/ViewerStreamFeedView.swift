//
//  ViewerStreamFeedView.swift
//  talkme_ios
//
//  Created by 1111 on 01.07.2021.
//

import RxCocoa
import RxSwift
import AVFoundation
import CollectionKit
import UIKit
import AVKit

final class ViewerStreamFeedView: UIView {

    private enum Constants {
        static let footerHeight: CGFloat = UIScreen.isSE ? 36 : 52
    }

    // MARK: - Output

    let pipWasClosed = PublishRelay<Void>()

    // MARK: - Public Properties

    private(set) lazy var isCameraOn = actionsView.isCameraOn
    private(set) lazy var isMicroOn = actionsView.isMicroOn
    private(set) lazy var onCameraTap = actionsView.onCameraTap
    private(set) lazy var onMicrophoneTap = actionsView.onMicrophoneTap
    private(set) lazy var onBackButtonTap = actionsView.onBackButtonTap
    private(set) lazy var onGiftButtonTap = actionsView.onGiftButtonTap
    private(set) lazy var onFullScreenTap = actionsView.onFullScreenTap
    private(set) lazy var onMembersTap = actionsView.onMembersTap
    private(set) lazy var sendMessage = actionsView.sendMessage
    private(set) lazy var onShowPopUp = actionsView.onShowPopUp
    private(set) lazy var onShowMembersPopUp = actionsView.onShowMembersPopUp
    private(set) lazy var orientationIsPortrait = actionsView.orientationIsPortrait
    private(set) lazy var orientationIsPortraitViewer = actionsView.orientationIsPortraitViewer
    private(set) lazy var onFetchMoreMessages = actionsView.onFetchMoreMessages
    private(set) var bag = DisposeBag()
    private(set) lazy var lessonFinished = actionsView.lessonFinished
    private(set) lazy var streamViewers = actionsView.streamViewers
    private(set) lazy var sendComplaint = actionsView.sendComplaint

    private(set) lazy var avatarView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()

    let onDeviceOrientationChanged = PublishRelay<CGSize>()
    weak var viewModel: StreamsFeedViewModel?

    // MARK: - Private Properties

    private var streamView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFill
        return iv
    }()

    private var loaderView: StreamLoaderView?
    private let actionsView: StreamFeedActionsView
    private var playerView = UIView(frame: .init(origin: .zero, size: UIScreen.size))
    private var playerContentSize: CGSize?
    private var avatarImage: UIImage?
    private var showAvatarFromTeacher = false
    private var showAvatarFromLearner = false
    private var popupView: VipLessonPopupView?
    private var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    private var loaderIsActive = false
    private var lessonId: Int = 0
    private var oldTeacherCameraIsActiveState = false
    private var pictureInPictureController: AVPictureInPictureController?

    // MARK: - Initializers

    init(streamModel: StreamModel) {
        actionsView = StreamFeedActionsView(type: .viewer, classNumber: streamModel.specialistInfo?.numberClass)
        super.init(frame: .zero)
        setAvatar(streamModel.specialistInfo?.avatarUrlOriginal)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        print("[DEINIT VIEWER VIEW]")
        removePlayerLayer()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        updateFrames(for: UIScreen.size)

        avatarView.frame = CGRect(x: 0,
            y: (UIScreen.height - ((UIScreen.height/3) * 2))/2,
            width: UIScreen.width,
            height: (UIScreen.height/3) * 2)

        updateVisibleFullScreenButton()
    }

    // MARK: - Public Methods

    func configure(streamModel: StreamModel, cameraIsActive: Bool, isWatchingSavedStream: Bool) {
        bag = .init()
        actionsView.configure(streamModel: streamModel)
        if let liveLessonEndDate = streamModel.lessonDateEnd {
            actionsView.invalidateLiveStreamTimer()
            actionsView.startLiveStreamTimer(withEndDate: liveLessonEndDate)
        }
    }

    func setPlayer(player: AVPlayer?) {
        var resetPlayerIsNeeded = self.player == nil

        if let recievedAsset = player?.currentItem?.asset as? AVURLAsset,
           let currentAsset = self.player?.currentItem?.asset as? AVURLAsset,
            recievedAsset.url.absoluteString != currentAsset.url.absoluteString {
            resetPlayerIsNeeded = true
        }

        if resetPlayerIsNeeded {
            pictureInPictureController?.stopPictureInPicture()
            pictureInPictureController = nil
            self.playerLayer?.removeFromSuperlayer()
            guard let player = player else { return }
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.videoGravity = .resizeAspectFill
            playerLayer.contentsGravity = .resizeAspectFill
            self.playerLayer = playerLayer
            playerView.layer.addSublayer(playerLayer)
            self.player = player

            setNeedsLayout()
            layoutIfNeeded()

            pictureInPictureController = AVPictureInPictureController(playerLayer: playerLayer)
            pictureInPictureController?.delegate = self
        }
    }

    func setPlayerSize(size: CGSize) {
        playerContentSize = size
        setNeedsLayout()
        layoutIfNeeded()
    }

    func presentStream(_ teacherVideoIsActive: Bool) {
        guard teacherVideoIsActive, !oldTeacherCameraIsActiveState else { return }
        oldTeacherCameraIsActiveState = teacherVideoIsActive
        streamView.subviews.forEach { $0.removeFromSuperview() }
        streamView.addSubview(playerView)
    }

    func setupChats(dataSource: ArrayDataSource<LessonMessage>) {
        actionsView.setupChats(dataSource: dataSource)
    }

    func prepareForReuse(streamModel: StreamModel, andShowingSavedStream showingSavedStream: Bool) {
        avatarView.isHidden = false
        playerView.isHidden = true
        player?.pause()
        self.popupView?.removeFromSuperview()
        actionsView.hideComplaintButtton(isHidden: streamModel.specialistInfo?.id == UserDefaultsHelper.shared.userId)
        actionsView.hideTimer(isHidden: true)
        actionsView.setButtonsToActiveStates()
        setAvatar(streamModel.specialistInfo?.avatarUrl)
        hideLoader()
        actionsView.invalidateLiveStreamTimer()
        actionsView.removeObservers()
        actionsView.hideChatMessages()
    }

    func setAvatar(_ coverImage: String?) {
            guard let coverImage = coverImage, let imageUrl = URL(string: coverImage) else { return }
            avatarView.kf.setImage(with: imageUrl) { [weak self] result in
                switch result {
                case .success(let image):
                    self?.avatarImage = image.image
                case .failure:
                    break
                }
            }
    }

    func showFullscreenAvatarPlaceholder(_ show: Bool) {
        showAvatarFromLearner = show
        if show {
            playerView.isHidden = show
            avatarView.isHidden = !show
        } else {
            guard showAvatarFromTeacher == show else { return }
            playerView.isHidden = show
            avatarView.isHidden = !show
        }
    }

    func hideVideoByTeacher(_ hide: Bool) {
        showAvatarFromTeacher = hide
        if hide {
            playerView.isHidden = hide
        } else {
            guard showAvatarFromLearner == hide else { return }
            playerView.isHidden = hide
        }
    }

    func removeStreamView() {
        playerView.removeFromSuperview()
        avatarView.removeFromSuperview()
        actionsView.removeFromSuperview()
    }

    func presentVipLessonPopup(popupView: VipLessonPopupView) {
        self.popupView = popupView
        actionsView.addSubview(popupView)
        actionsView.setVipLessonState(true)
        popupView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    func removeVipLessonPopup() {
        popupView?.removeFromSuperview()
        popupView = nil
        actionsView.setVipLessonState(false)
        playerLayer?.player?.isMuted = false
    }

    func showLoader() {
        guard !loaderIsActive else { return }
        let loaderView = StreamLoaderView()
        addSubview(loaderView)
        loaderView.center = streamView.center
        loaderView.animate(circle: loaderView.circle1, counter: 1)
        loaderView.animate(circle: loaderView.circle2, counter: 3)
        self.loaderView = loaderView
        loaderIsActive = true
    }

    func hideLoader() {
        loaderView?.stopAnimating()
        loaderView?.removeFromSuperview()
        loaderIsActive = false
    }

    func videoIsHidden() -> Bool {
        playerView.isHidden ?? true
    }

    // MARK: - Private Methods

    private func removePlayerLayer() {
        playerLayer?.player?.pause()
        playerLayer?.removeFromSuperlayer()
        playerLayer = nil
    }

    private func setupLayout() {
        addSubviews(avatarView, streamView, actionsView)
        bringSubviewToFront(actionsView)
        updateFrames(for: UIScreen.size)
    }

    private func updateFrames(for size: CGSize) {
        playerView.frame = .init(origin: .zero, size: size)
        playerLayer?.frame = .init(origin: .zero, size: size)
        streamView.frame = .init(origin: .zero, size: size)
        loaderView?.center = streamView.center
        actionsView.frame = .init(origin: .zero, size: size)
        actionsView.updateSubviewsFrames(for: size, isLandscape: false)
        updatePlayerSize(size)
    }

    private func updatePlayerSize(_ screenSize: CGSize) {
        guard let playerLayer = playerLayer else { return }

        guard let playerContentSize = playerContentSize,
              playerContentSize.height != 0 else {
            playerLayer.frame = CGRect(origin: .zero, size: screenSize)
            return
        }

        let isPortrait = UIDevice.current.orientation.isPortrait
        let isLandscape = playerContentSize.width > playerContentSize.height

        switch (isPortrait, isLandscape) {
        case (true, true):
            let ratio = screenSize.width / playerContentSize.width
            let width = screenSize.width
            let height = playerContentSize.height * ratio

            playerLayer.frame = CGRect(origin: CGPoint(x: 0, y: 70), size: CGSize(width: width, height: height))
        case (false, false):
            let ratio = screenSize.height / playerContentSize.height
            let width = playerContentSize.width * ratio
            let height = screenSize.height
            let xOrigin = screenSize.width / 2 - width / 2

            playerLayer.frame = CGRect(origin: CGPoint(x: xOrigin, y: 0), size: CGSize(width: width, height: height))
        case (true, false), (false, true):
            playerLayer.frame = CGRect(origin: .zero, size: screenSize)
        }
    }

    private func updateVisibleFullScreenButton() {
        var isHiddenFullScreenButton = UIApplication.shared.statusBarOrientation.isPortrait

        if isHiddenFullScreenButton == true, let playerContentSize = playerContentSize {
            isHiddenFullScreenButton = playerContentSize.height > playerContentSize.width
        }

        actionsView.hideFullScreenButton(isHidden: isHiddenFullScreenButton)
    }
}

extension ViewerStreamFeedView: AVPictureInPictureControllerDelegate {

    func pictureInPictureControllerWillStartPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        guard let viewModel = self.viewModel else { return }
        viewModel.isActivePictureInPicture = true
    }

    func pictureInPictureControllerWillStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        guard let viewModel = self.viewModel else { return }
        if viewModel.isActivePictureInPicture == true {
            viewModel.flow.accept(.onBack(animated: false))
        }
        viewModel.isActivePictureInPicture = false
    }

    func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        guard let viewModel = self.viewModel else { return }
        viewModel.isActivePictureInPicture = false
        completionHandler(false)
    }
}
