//
//  StreamSocialCollectionCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 19.03.2021.
//

import RxCocoa
import RxSwift

final class StreamSocialCollectionCell: UICollectionViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private lazy var socialsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ type: SocialMedia, isEnabled: Bool) {
        socialsImageView.image = type.icon
        socialsImageView.alpha = isEnabled ? 1 : 0.5
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Private Methods

    private func initialSetup() {
        contentView.addSubviews(socialsImageView)
        socialsImageView.snp.makeConstraints { make in
            make.height.width.equalTo(UIScreen.isSE ? 35 : 46)
            make.leading.trailing.top.equalToSuperview()
        }
    }
}
