//
//  TeacherStreamFeedView.swift
//  talkme_ios
//
//  Created by 1111 on 01.07.2021.
//

import RxCocoa
import RxSwift
import Reachability
import HaishinKit
import CollectionKit

final class TeacherStreamFeedView: UIView {

    private enum Constants {
        static let footerHeight: CGFloat = UIScreen.isSE ? 36 : 52
    }

    // MARK: - Public Properties

    private(set) lazy var isCameraOn = actionsView.isCameraOn
    private(set) lazy var isMicroOn = actionsView.isMicroOn
    private(set) lazy var onCameraTap = actionsView.onCameraTap
    private(set) lazy var onMicrophoneTap = actionsView.onMicrophoneTap
    private(set) lazy var onFlipCameraTap = actionsView.onFlipCameraTap
    private(set) lazy var onBackButtonTap = actionsView.onBackButtonTap
    private(set) lazy var onMembersTap = actionsView.onMembersTap
    private(set) lazy var sendMessage = actionsView.sendMessage
    private(set) lazy var onShowMembersPopUp = actionsView.onShowMembersPopUp
    private(set) lazy var orientationIsPortrait = actionsView.orientationIsPortrait
    private(set) lazy var onFetchMoreMessages = actionsView.onFetchMoreMessages
    private(set) lazy var streamViewers = actionsView.streamViewers
    private(set) lazy var lessonFinished = actionsView.lessonFinished

    private(set) lazy var avatarView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        return iv
    }()

    let hasCameraPermissions = PublishRelay<Bool>()
    let hasMicrophonePermissions = PublishRelay<Bool>()
    let onDeviceOrientationChanged = PublishRelay<CGSize>()
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private var streamView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFill
        return iv
    }()

    private let actionsView: StreamFeedActionsView
    private var mainStreamView: UIView?
    private var avatarImage: UIImage?
    private var loaderIsActive = false
    private var loaderView: StreamLoaderView?

    // MARK: - Initializers

    init(classNumber: String?) {
        actionsView = StreamFeedActionsView(type: .specialist, classNumber: classNumber)
        super.init(frame: .zero)
        setupLayout()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        print("[DEINIT TEACHER VIEW]")
        actionsView.removeObservers()
    }

    // MARK: - Public Methods

    func configure(hkView: GLHKView?, streamModel: StreamModel) {
        setAvatar(streamModel.specialistInfo?.avatarUrlOriginal)
        actionsView.configure(streamModel: streamModel)
        actionsView.startLiveStreamTimer(withEndDate: streamModel.lessonDateEnd ?? 0)
        streamView.subviews.forEach { $0.removeFromSuperview() }
        guard let hkView = hkView else { return }
        streamView.addSubview(hkView)
        mainStreamView = hkView
        hideLoader()
    }

    func setupChats(dataSource: ArrayDataSource<LessonMessage>) {
        actionsView.setupChats(dataSource: dataSource)
    }

    func hideStreamView(_ isHidden: Bool) {
        mainStreamView?.isHidden = isHidden
    }

    func showLoader() {
        guard !loaderIsActive else { return }
        let loaderView = StreamLoaderView()
        self.addSubview(loaderView)
        loaderView.center.x = self.streamView.center.x
        loaderView.center.y = self.streamView.center.y
        loaderView.animate(circle: loaderView.circle1, counter: 1)
        loaderView.animate(circle: loaderView.circle2, counter: 3)
        self.loaderView = loaderView
        loaderIsActive = true
    }

    func hideLoader() {
        loaderView?.stopAnimating()
        loaderView?.removeFromSuperview()
        loaderIsActive = false
    }

    // MARK: - Private Methods

    func setAvatar(_ coverImage: String?) {
        avatarView.image = nil
        avatarImage = nil
        avatarView.frame = CGRect(x: 0,
                                  y: (UIScreen.height - ((UIScreen.height/3) * 2))/2,
                                  width: UIScreen.width,
                                  height: (UIScreen.height/3) * 2)
        guard let coverImage = coverImage, let imageUrl = URL(string: coverImage) else { return }
        avatarView.kf.setImage(with: imageUrl) { [weak self] result in
            switch result {
            case .success(let image):
                self?.avatarImage = image.image
            case .failure:
                break
            }
        }
    }

    func removeStreamView() {
        avatarView.removeFromSuperview()
        actionsView.removeFromSuperview()
    }

    private func setupLayout() {
        addSubviews(avatarView, streamView, actionsView)
        bringSubviewToFront(actionsView)
        updateFrames(for: UIScreen.size)
    }

    private func bindUI() {
        onDeviceOrientationChanged
            .do(onNext: { [weak self] _ in
                self?.avatarView.image = nil
            }, afterNext: { [weak self] _ in
                guard let self = self else { return }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.avatarView.image = self.avatarImage
                }
            })
            .bind { [weak self] size in
                if UIDevice.current.orientation.isPortrait {
                    self?.mainStreamView?.transform = CGAffineTransform(rotationAngle: 0)
                    self?.updateFrames(for: size)
                } else {
                    if UIDevice.current.orientation == .landscapeRight {
                        self?.mainStreamView?.transform = CGAffineTransform(rotationAngle: CGFloat(90 * Double.pi / 180.0))
                        self?.updateFrames(for: size)
                    } else if UIDevice.current.orientation == .portrait {
                        self?.mainStreamView?.transform = CGAffineTransform(rotationAngle: 0)
                        self?.updateFrames(for: size)
                    } else {
                        self?.mainStreamView?.transform = CGAffineTransform(rotationAngle: CGFloat(-90 * Double.pi / 180.0))
                        self?.updateFrames(for: size)
                    }
                }
            }
            .disposed(by: bag)
    }

    private func updateFrames(for size: CGSize) {
        udpateAvatarFrame()
        mainStreamView?.frame = .init(origin: .zero, size: size)
        streamView.frame = .init(origin: .zero, size: size)
        actionsView.frame = .init(origin: .zero, size: size)
        actionsView.updateSubviewsFrames(for: size, isLandscape: UIDevice.current.orientation.isLandscape)
    }

    private func udpateAvatarFrame() {
        avatarView.center.y = UIScreen.height/2
        avatarView.center.x = UIScreen.width/2
    }
}
