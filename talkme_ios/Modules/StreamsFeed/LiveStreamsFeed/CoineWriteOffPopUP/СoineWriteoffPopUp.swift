//
//  СoineWriteoffPopUp.swift
//  talkme_ios
//
//  Created by Vladislav on 26.07.2021.
//

import UIKit

final class СoineWriteoffPopUp: UIView {

    // MARK: Public Properties

    lazy var toConfirmButtonTap = confirmButton.rx.tap

    // MARK: - Private Properties

    private let coinImage = UIImageView(image: UIImage(named: "coin"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "stream_pop_up_write_off".localized
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratBold(ofSize: 20)
        return lbl
    }()

    private let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "mainBackground")
        image.contentMode = UIView.ContentMode.scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    private let countLabel: UILabel = {
        let lbl = UILabel()
        lbl.layer.cornerRadius = 28
        lbl.textAlignment = .center
        lbl.textColor = TalkmeColors.shadow
        lbl.text = "35"
        lbl.font = .montserratBold(ofSize: 25)
        return lbl
    }()

    private let roundedView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = UIScreen.isSE ? 18 : 23.5
        return view
    }()

    private let confirmButton: UIButton = {
        let btn = UIButton()
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        btn.titleLabel?.font = .montserratBold(ofSize: 18)
        btn.setTitle("talkme_popup_continue".localized, for: .normal)
        btn.backgroundColor = TalkmeColors.greenLabels
        btn.layer.cornerRadius = 28
        return btn
    }()

    // MARK: - Initializers

    init(frame: CGRect, cost: Int) {
        super.init(frame: frame)
        countLabel.text = String(cost)
        layer.cornerRadius = 19
        clipsToBounds = true
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([backgroundImage, titleLabel, countLabel, coinImage, roundedView, confirmButton])
        roundedView.addSubviews([coinImage, countLabel])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(28)
            make.leading.trailing.equalToSuperview().inset(26)
        }

        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        roundedView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(12)
            make.leading.trailing.equalToSuperview().inset(42)
            make.height.equalTo(48)
        }

        countLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()

        }

        coinImage.snp.makeConstraints { image in
            image.trailing.equalToSuperview().offset(-12)
            image.top.equalToSuperview().offset(10)
            image.height.width.equalTo(28)
        }

        confirmButton.snp.makeConstraints { make in
            make.top.equalTo(roundedView.snp.bottom).offset(24)
            make.leading.equalToSuperview().offset(42)
            make.trailing.equalToSuperview().offset(-42)
            make.bottom.equalToSuperview().offset(-28)
            make.height.equalTo(54)
        }

    }
}
