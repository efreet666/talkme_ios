//
//  CoineWriteoffPopUp.swift
//  talkme_ios
//
//  Created by Vladislav on 01.10.2021.
//

import RxCocoa
import RxSwift

final class CoineWriteoffPopUpViewController: UIViewController {

    enum Flow {
        case dismiss
        case buyAndDismiss
    }

    // MARK: - Public Properties

    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    let dissmissVipLessonPopup = PublishRelay<Void>()

    // MARK: - Private Properties

    private lazy var coineWriteOffPopUp = СoineWriteoffPopUp(frame: .zero, cost: cost)

    private let popUpBackground: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.popupBackground
        view.alpha = 5
        return view
    }()

    private let dismissButtonImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "dismissButton"))
        iw.contentMode = .scaleAspectFill
        return iw
    }()

    private let cost: Int
    private let id: Int

    // MARK: - Initializers

    init(cost: Int, id: Int) {
        self.cost = cost
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        bind()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        view.addSubviews(popUpBackground, coineWriteOffPopUp, dismissButtonImageView)
        coineWriteOffPopUp.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview().offset(UIScreen.isSE ? 20 : 24)
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -20 : -24)
            make.height.equalTo(213)
        }

        dismissButtonImageView.snp.makeConstraints { make in
            make.height.width.equalTo(18)
            make.trailing.equalTo(coineWriteOffPopUp.snp.trailing)
            make.bottom.equalTo(coineWriteOffPopUp.snp.top).offset(-12)
        }

        popUpBackground.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func bind() {
        dismissButtonImageView
            .rx
            .tapGesture()
            .when(.recognized)
            .bind { [weak self] recognizer in
                guard let self = self else { return }
                let point = recognizer.location(in: self.coineWriteOffPopUp)
                guard self.view.frame.contains(point) else {
                    self.flow.accept(.dismiss)
                    return
                }
            }
            .disposed(by: bag)

        coineWriteOffPopUp
            .toConfirmButtonTap
            .bind { [weak self] _ in
                self?.flow.accept(.buyAndDismiss)
            }
            .disposed(by: bag)
    }
}
