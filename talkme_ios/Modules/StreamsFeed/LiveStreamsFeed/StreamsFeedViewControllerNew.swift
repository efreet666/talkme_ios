//
//  StreamsFeedViewControllerNew.swift
//  talkme_ios
//
//  Created by admin on 29.09.2022.
//

import CollectionKit
import RxCocoa
import RxSwift
import UIKit
import NotificationBannerSwift
import AVFoundation

fileprivate extension Consts {
    // dont push numbers more then 100 nd less then 0 to precentOfPageSizeWichIsALoadBorder
    static var precentOfPageSizeWichIsALoadBorder: Int = 70
}

final class StreamsFeedViewControllerNew: UIViewController {

    private enum SwipeState {
        case completeWithLessonChange
        case loading
        case completeWithoutLessonChange
        case canUpate
    }

    // MARK: - Private Properties

    private lazy var collectionView: CollectionView? = {
        let collectionView = CollectionView()
        collectionView.bounces = false
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()

    private lazy var teacherView: TeacherStreamFeedView = {
        let view = TeacherStreamFeedView(classNumber: self.viewModel.currentClassNumber)
        return view
    }()
    
    private lazy var viewHelp = HelpView(viewModel: self.viewModel)

    private let viewModel: StreamsFeedViewModelNew
    private let onDeviceOrientationChanged = PublishRelay<CGSize>()
    private var bag = DisposeBag()
    private var contentOffset: CGFloat = 0
    private let hasCameraPermissions = BehaviorRelay<Bool>(value: false)
    let removeViewFromSuperviewTeacher = PublishRelay<Void>()
    private let hasMicrophonePermissions = BehaviorRelay<Bool>(value: false)
    private var streamViewHeight: CGFloat { UIScreen.height }
    private var orientationIsPortrait = BehaviorRelay<Bool>(value: true)
    private var teacherIsNotActiveAlert: UIAlertController?
    private var cameraIsActive: Bool { return viewModel.cameraIsActiveState }
    private var userType: UserType?
    private var showPopupBlock: (() -> Void)?
    private var currentSwipeState: SwipeState = .completeWithLessonChange
    private var collectionWilldecelerate = false
    private let onSwipe = PublishRelay<Void>()
    private var collectionKitDataSource = ArrayDataSource<StreamFeedViewModel>()

    // MARK: - Initializers

    init(viewModel: StreamsFeedViewModelNew) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = TalkmeColors.streamBG
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // repeatedLoginRequest()
        #if DEBUG
//        showAlertStreamInfo()
        #endif
        showNotificationChatInfo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTopBottomBarsHidden(true)
        keepScreenAwake(true)
        viewModel.setPlayer(mute: false)
        if let showPopupBlock = showPopupBlock {
            showDonatePopupIfNeeded(showBlock: showPopupBlock)
        }
        bindVM()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setTopBottomBarsHidden(false)
        UIDevice.current.changeOrientation(.portrait)
        keepScreenAwake(false)
        viewModel.setPlayer(mute: true)
        viewModel.finishTraking()
        bag = .init()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        DispatchQueue.main.async {
            self.view.endEditing(true)

            guard self.userType == .specialist else { return }
            self.onDeviceOrientationChanged.accept(size)
            if UIDevice.current.orientation.isLandscape {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.orientationIsPortrait.accept(false)
                }
            } else if UIDevice.current.orientation.isPortrait {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.orientationIsPortrait.accept(true)
                }
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            if let collectionView = self.collectionView {
                self.viewModel.canUpdateCollection = false
                self.scrollToIndex(index: self.viewModel.currentIndex)
                self.setSwipestate(index: self.viewModel.currentIndex)
            }
        }
    }

    // MARK: - Public Methods

    func showDonatePopupIfNeeded(showBlock: @escaping () -> Void) {
        guard self.navigationController?.topViewController === self else {
            self.showPopupBlock = showBlock
            return
        }
        showBlock()
        showPopupBlock = nil
    }

    // MARK: - Private Methods

    private func notificationChatInfo(title: String, subTitle: String) {
        let banner = NotificationBanner(title: title, subtitle: subTitle, style: .warning)
        banner.autoDismiss = false
        banner.dismissOnTap = true
        banner.show()
    }

    private func repeatedLoginRequest() {
        let emailOrMobile = KeychainManager.load(service: "MobileService", account: "Mobile") ?? ""
        let password = KeychainManager.load(service: "PasswordService", account: "Password")  ?? ""
        viewModel.repeatedLoginRequest(emailOrMobile: emailOrMobile, password: password)
    }

    private func showNotificationChatInfo() {
        viewModel.showNotification = { [weak self] title, subTitle in
            self?.notificationChatInfo(title: title, subTitle: subTitle)
        }
    }

    private func showAlertStreamInfo() {
        viewModel.dataResponse = { [weak self] model, code in
            let data = "gcoreDashUrl: \(model?.gcoreDashUrl ?? "") \n\ngcoreHlsUrl: \(model?.gcoreHlsUrl ?? "") \n\npushUrl: \(model?.pushUrl ?? "") \n\nuserId: \(model?.userId ?? 0) \n\nstausCode: \(code ?? "")"
            guard self?.currentSwipeState == .completeWithLessonChange else {
                return nil
            }
            return self?.errorAlert(title: "isOwner: \(model?.owner ?? true)", data: data)
        }
    }

    private func errorAlert(title: String, data: String) {
        let alert = UIAlertController(title: title, message: data, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        navigationController?.present(alert, animated: true, completion: nil)
    }

    private func bindVM() {
        viewModel
            .onGetLessons
            .filter { $0 }
            .take(1)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.userType = self.viewModel.userType
                if self.userType == .specialist {
//                    AppDelegate.deviceOrientation = .all
                }
                self.setupLayout()
                self.createStreamView()
            }.disposed(by: bag)

        viewModel.onPresentLessonsIsEmptyAlert
            .filter { $0 }
            .bind { [weak self] _ in
                self?.presentNoLessonsAlert()
            }.disposed(by: bag)
    }

    private func createStreamView() {
        if let userType = userType {
            switch userType {
            case .specialist:
                self.bindStreamViewForSpecialist(teacherView)
            case .participant:
                return
            case .viewer:
                setupCollectionView()
                viewModel
                    .onDataSourceUpdate
                    .filter { (cellsModelsArray,_) in cellsModelsArray.count != 0 }
                    .filter { [weak self] _-> Bool in
                        guard let self = self else { return false }
                        return self.viewModel.canUpdateCollection && self.currentSwipeState != .loading
                    }
                    .bind { [weak self] (cellsModelsArray, causeOfUpdate) in
                        guard let self = self else { return }
                        switch causeOfUpdate {
                        case .newPageLoaded:
                            break
                        case .periodiсUpdate:
                            let indexOfTopVsibleCell = self.collectionView?.visibleIndexes.first ?? 0
                            let changeInCountOfStreams = self.viewModel.getIndexOfCellAfterUpdate(forCellWithIndexBeforeUpdate: indexOfTopVsibleCell)
                            self.collectionView?.contentOffset.y = self.streamViewHeight * CGFloat(changeInCountOfStreams)
                        case .newStreamSelected:
                            self.collectionView?.contentOffset.y = self.streamViewHeight * CGFloat(self.viewModel.currentIndex)
                        }
                        self.collectionKitDataSource.data = cellsModelsArray
                        self.collectionView?.reloadData()
                    }
                    .disposed(by: bag)
            }
        }
        checkCameraAndMicrophonePermissions()
    }

   private func presentNoLessonsAlert() {
            let alert = UIAlertController(title: nil, message: "live_lessons_is_empty".localized, preferredStyle: .alert)
            let alertCancel = UIAlertAction(title: "profile_setting_cancel".localized, style: .default) { [weak self] _ in
                self?.viewModel.handleOnBackTap()
            }
            alert.addAction(alertCancel)
       self.present(alert, animated: true)
        }

    private func setTopBottomBarsHidden(_ isHidden: Bool) {
        navigationController?.isNavigationBarHidden = isHidden
        setCustomTabbarHidden(isHidden)
    }

    private func setupLayout() {
        guard let userType = viewModel.userType else { return }
        var userView = UIView()
        switch userType {
        case .specialist:
            userView = teacherView
        case .participant:
            return
        case .viewer:
            guard let collectionView = collectionView else { return }
            userView = collectionView
        }
        view.addSubview(userView)
        userView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        userView.layoutIfNeeded()
    }

    private func setupCollectionView() {
        let sourceView = ClosureViewSource(viewGenerator: { data, _ -> ViewerStreamFeedViewNew in
            return ViewerStreamFeedViewNew(streamModel: data.streamModel)
        },
                                           viewUpdater: { [weak self] (view: ViewerStreamFeedViewNew, data: StreamFeedViewModel, index: Int) in
            guard let self = self else { return }
            if self.viewModel.onDataSourceUpdate.value.1 == .periodiсUpdate,
               view.videoIsHidden() == false,
               self.collectionView?.visibleIndexes.contains(index) ?? false {
                // do nothing because this case represents cell that are on screen and are playing video
                // video blinks on update so we dont update cell if it is playing video
            } else {
                switch self.currentSwipeState {
                case .canUpate, .completeWithLessonChange, .completeWithoutLessonChange:
                    print("[swipe] CONFIG")
                    let dataSource = self.viewModel.refreshChatsDataSource(lessonId: data.streamModel.id)
                    view.setupChats(dataSource: dataSource)
                    if let lessonDetail = self.viewModel.lessonDetails[data.streamModel.id] {
                        view.configure(streamModel: data.streamModel, lessonDetail: lessonDetail, cameraIsActive: self.viewModel.cameraIsActiveState, isConnect: self.viewModel.infoStream?.isContact ?? false)
                    }
                    self.bindStreamViewForViewer(view, data: data)
                    self.viewModel.onReadyToPresentVipLessonPopup.accept(data.streamModel.id)
                     // по-умолчанию камера у юзера вкл
                     self.viewModel.onChangeTeacherCameraState.onNext(true)
                case .loading:
                    print("[swipe] failed state")
                    view.prepareForReuse(streamModel: data.streamModel, andShowingSavedStream: false)
                }
            }
        })
        sourceView.reuseManager.lifeSpan = 0

        let sizes = { (_: Int, _: StreamFeedViewModel, collectionSize: CGSize) -> CGSize in
            return CGSize(width: collectionSize.width, height: collectionSize.height)
        }

        let provider = BasicProvider(
            dataSource: collectionKitDataSource,
            viewSource: sourceView,
            sizeSource: sizes
        )

        provider.layout = InsetLayout(FlowLayout(spacing: 0), insets: UIEdgeInsets.zero)

        guard let collectionView = collectionView else { return }
        collectionView.provider = provider

        collectionView.rx.willBeginDragging
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.collectionWilldecelerate = false
                self.currentSwipeState = .loading
                print("[swipe] STARTED")
            }
            .disposed(by: bag)

        collectionView.rx.didEndDragging
            .bind { [weak self] cellWilldecelerate in
                guard let self = self else { return }
                print("[swipe] DID END DRAGGING")
                self.collectionWilldecelerate = cellWilldecelerate
                print("[swipe] [lesson will changed \(cellWilldecelerate)]")
                guard !cellWilldecelerate else { return }
                self.currentSwipeState = .canUpate
            }
            .disposed(by: bag)

        let bottomPagingBoundObserver = viewModel
            .onDataSourceUpdate
            .map { [weak self] (cellModelsArray, _) -> CGFloat in
                guard let self = self else { return CGFloat(0) }
                let nextPagingBorderOffset = self.culculateBorderOffsetValueForLoadingNextPage(from: cellModelsArray.count,
                                                                                               and: self.viewModel.numberOfStreamsInLastPage)
                return nextPagingBorderOffset
            }

        Observable
            .combineLatest(collectionView.rx.contentOffset, bottomPagingBoundObserver)
            .filter { contentOffset, bottomPagingBound -> Bool in
                guard bottomPagingBound != 0
                else { return false}
                return contentOffset.y > bottomPagingBound
            }
            .map { $0.1 }
            .distinctUntilChanged()
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.loadNextPage()
            }
            .disposed(by: bag)

        collectionView.rx
            .contentOffset
            .debounce(.milliseconds(250), scheduler: MainScheduler.instance)
            .bind { [weak self] _ in
                guard let self = self,
                      self.collectionView?.visibleIndexes.count == 1,
                      let indexOfVisibleCell = self.collectionView?.visibleIndexes.first,
                      let visibleCell = self.collectionView?.cell(at: indexOfVisibleCell) as? ViewerStreamFeedViewNew,
                      visibleCell.videoIsHidden()
                else { return }
                self.handleScroll(index: indexOfVisibleCell)
            }
            .disposed(by: bag)
    }

    private func handleScroll(index: Int) {
        viewModel.canUpdateCollection = false
        self.setSwipestate(index: index)
        print("[swipe] HANDLE SCROLL  INDEX \(index)")
    }

    private func scrollToIndex(index: Int) {
        do {
            try collectionView?.scroll(to: index, animated: false)
        } catch {
            print("[swipe] \(error.localizedDescription)")
        }
    }
    private func visibleCell(cell: UIView) -> Bool {
        guard let collection = collectionView else { return false }
        guard cell.frame.minY != collection.contentOffset.y else { return true }
        let collectionBottom = collection.contentOffset.y + cell.frame.height
        if cell.frame.minY > collection.contentOffset.y {
            if (collectionBottom - cell.frame.minY) >= (cell.frame.height / 2) {
                return true
            }
        } else if cell.frame.minY < collection.contentOffset.y {
            if (collection.contentOffset.y - cell.frame.minY) <= (cell.frame.height / 2) {
                return true
            }
        }
        return false
    }

    private func setSwipestate(index: Int) {
        currentSwipeState = .completeWithLessonChange
        connectWithIndividualIndex(index)
        // for testing
        if Consts.liveStreamsStubbed {
            self.viewModel.chatService?.dataChannel.onNext(DataChannelResponce(data: DataChannelModel(anotherData: ["TeacherCameraIsActive": true])))
        }
    }

    private func connectWithIndividualIndex(_ index: Int) {
        viewModel.canUpdateCollection = true
        viewModel.connectWithIndex(index)
    }

    private func checkCameraAndMicrophonePermissions() {
        Single.zip(
            PermissionsHelper.checkCameraPermissions(),
            PermissionsHelper.checkMicrophonePermissions())
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let result):
                    let (cameraAccept, microAccept) = result
                    self.hasCameraPermissions.accept(cameraAccept)
                    self.hasMicrophonePermissions.accept(microAccept)

                    let allAccept = cameraAccept && microAccept
                    if allAccept { return }

                    let allDenied = !cameraAccept && !microAccept
                    if allDenied {
                        AlertControllerHelper.showRedirectToSettingsAlert(title: PermissionError.cameraMicrophoneDenied.localized)
                        return
                    }

                    let noCamera = !cameraAccept && microAccept
                    if noCamera {
                        AlertControllerHelper.showRedirectToSettingsAlert(title: PermissionError.cameraDenied.localized)
                        return
                    }

                    let noMicro = cameraAccept && !microAccept
                    if noMicro {
                        AlertControllerHelper.showRedirectToSettingsAlert(title: PermissionError.microphoneDenied.localized)
                    }
                case .error:
                    break
                }
            }
            .disposed(by: bag)
    }

    private func keepScreenAwake(_ isAwake: Bool) {
        UIApplication.shared.isIdleTimerDisabled = isAwake
    }

    private func culculateBorderOffsetValueForLoadingNextPage(from itemsCount: Int, and numberOfStreamsInPage: Int) -> CGFloat {
        let lengthBeforeLastPage = Float(self.streamViewHeight) * Float(itemsCount - numberOfStreamsInPage)
        let lastPageLength = Float(self.streamViewHeight) * Float(numberOfStreamsInPage)
        let pagingBorderInlastPage = lastPageLength * Float(Float(Consts.precentOfPageSizeWichIsALoadBorder) / 100.0 )
        return CGFloat(lengthBeforeLastPage + pagingBorderInlastPage)
    }

    // MARK: - bind teacher

    private func bindStreamViewForSpecialist(_ view: TeacherStreamFeedView) {
        viewModel
            .onTeacherStreamViewReady
            .subscribe(onNext: { [weak view, weak self] streamView in
                guard let view = view,
                      let self = self,
                      let details = self.viewModel.currentLessonDetails
                else { return }
                let lessonDateEnd = Formatters.getIntervalWithDuration(duration: details.lessonTime, dateStart: details.date - Date.diffrenceBetweenRealTime)
                let model = StreamModel(id: self.viewModel.currentLessonId ?? 0,
                                        specialistInfo: details.owner,
                                        lessonDateEnd: lessonDateEnd,
                                        categoryId: details.subCategory.parent)
                view.configure(hkView: streamView, streamModel: model)
                let dataSource = self.viewModel.refreshChatsDataSource(lessonId: self.viewModel.currentLessonId ?? 0)
                view.setupChats(dataSource: dataSource)
            })
            .disposed(by: viewModel.bag)

        view
            .onBackButtonTap
            .bind { [weak self] _ in
                UIDevice.current.changeOrientation(.portrait)
                self?.viewModel.handleOnBackTap()
            }
            .disposed(by: view.bag)

        self.removeViewFromSuperviewTeacher
            .subscribe(onNext: { [weak view] _ in
                view?.removeStreamView()
            })
            .disposed(by: view.bag)

        view
            .sendMessage
            .bind(to: viewModel.sendMessage)
            .disposed(by: view.bag)

        onDeviceOrientationChanged
            .bind(to: view.onDeviceOrientationChanged)
            .disposed(by: bag)

        view
            .onMembersTap
            .bind { [weak self] _ in
                self?.viewModel.handleMembersTap()
            }
            .disposed(by: view.bag)

        view
            .onShowMembersPopUp
            .bind { [weak self] _ in
                self?.viewModel.handleMembersTap()
            }
            .disposed(by: view.bag)

        viewModel
            .streamViewers
            .bind(to: view.streamViewers)
            .disposed(by: viewModel.bag)

        orientationIsPortrait
            .bind(to: view.orientationIsPortrait)
            .disposed(by: bag)

        hasCameraPermissions
            .bind(to: view.isCameraOn)
            .disposed(by: bag)

        hasMicrophonePermissions
            .bind(to: view.isMicroOn)
            .disposed(by: bag)

        view
            .onFlipCameraTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                let hasCameraPermissions = self.hasCameraPermissions.value
                if hasCameraPermissions {
                    self.viewModel.handleFlipCameraTap()
                } else {
                    AlertControllerHelper.showRedirectToSettingsAlert(title: PermissionError.cameraDenied.localized)
                }
            }
            .disposed(by: view.bag)

        view
            .onCameraTap
            .bind { [weak self, weak view] _ in
                guard let self = self, let view = view else { return }
                let isActive = !self.viewModel.cameraIsActiveState
                let hasCameraPermissions = self.hasCameraPermissions.value
                if hasCameraPermissions {
                    view.isCameraOn.accept(isActive)
                    self.viewModel.handleOnCameraTeacherTap(cameraIsActive: isActive)
                    self.viewModel.cameraIsActiveState = isActive
                } else {
                    view.isCameraOn.accept(hasCameraPermissions)
                    AlertControllerHelper.showRedirectToSettingsAlert(title: PermissionError.cameraDenied.localized)
                }
            }
            .disposed(by: view.bag)

        view
            .onMicrophoneTap
            .bind { [weak self, weak view] _ in
                guard let self = self, let view = view else { return }
                let isActive = !self.viewModel.microOrPlayerIsActiveState
                let hasMicrophonePermissions = self.hasMicrophonePermissions.value
                self.viewModel.microOrPlayerIsActiveState.toggle()
                if hasMicrophonePermissions {
                    self.viewModel.handleMicrophoneTap()
                    view.isMicroOn.accept(isActive)
                } else {
                    view.isMicroOn.accept(hasMicrophonePermissions)
                    AlertControllerHelper.showRedirectToSettingsAlert(title: PermissionError.microphoneDenied.localized)
                }
            }
            .disposed(by: view.bag)

        view.onFetchMoreMessages
            .subscribe { [weak self] _ in
                self?.viewModel.fetchMoreMessages()
            }
            .disposed(by: bag)

        viewModel
            .onChangeTeacherCameraState
            .subscribe(onNext: { [weak view] flag in
                view?.hideStreamView(!flag)
            })
            .disposed(by: viewModel.bag)

        view.lessonFinished
            .subscribe { [weak self] _ in
                self?.viewModel.handleLessonFinished()
            }
            .disposed(by: bag)

        Observable
            .combineLatest(viewModel.onShowLoader, viewModel.onLessonFinished)
            .observeOn(MainScheduler.instance)
            .debounce(.seconds(5), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak view] show, lessonIsFinished in
                if show, !lessonIsFinished {
                    view?.showLoader()
                } else {
                    view?.hideLoader()
                }
            })
            .disposed(by: view.bag)
    }

    // MARK: - bind viewer

    private func bindStreamViewForViewer(_ view: ViewerStreamFeedViewNew, data: StreamFeedViewModel) {
        
        view.viewModel = viewModel

//        Observable
//            .combineLatest(viewModel.onPlayerReadyForDisplay, viewModel.onChangeTeacherCameraState)
//            .filter { $0.0 != nil }
//            .observeOn(MainScheduler.instance)
//            .subscribe(onNext: { [weak view] player, cameraIsActive in
//                view?.setPlayer(player: player)
//                view?.hideVideoByTeacher(!cameraIsActive)
//                view?.presentStream(cameraIsActive)
//            })
//            .disposed(by: view.bag)
        
        viewHelp.flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                switch flow {
                case .back:
                    self.viewModel.flow.accept(.finishStream)
                }
            }
            .disposed(by: view.bag)
        
        viewModel
            .onPlayerReadyForDisplay
            .subscribe(onNext: { [weak view] player in
                view?.setPlayer(player: player)
            })
            .disposed(by: view.bag)

        viewModel
            .dismissVipLessonPopup
//            .observeOn(MainScheduler.instance)
            .bind { [weak view] _ in
                view?.removeVipLessonPopup()
            }
            .disposed(by: view.bag)

        view
            .onGiftButtonTap
//            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
//                self?.viewModel.handleOnGiftTap(false)
                self?.viewModel.handleGiftButtonTap()
            }
            .disposed(by: view.bag)
        
        view
            .onCoinTap
            .bind { [weak self] _ in
                self?.viewModel.handleThankButtonTap()
        }
        .disposed(by: view.bag)
        
        view
            .onBackButtonTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                UIDevice.current.changeOrientation(.portrait)
                self?.viewModel.handleOnBackTap()
            }
            .disposed(by: view.bag)
        
        view
            .sendMessage
            .observeOn(MainScheduler.instance)
            .bind(to: viewModel.sendMessage)
            .disposed(by: view.bag)
        
        view
            .onShowPopUp
//            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.handleOnGiftTap(false)
            }
            .disposed(by: view.bag)
        
        view
            .onHelpButtonTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.handleOnComplaintButtonTap()
            }
            .disposed(by: view.bag)
        
        viewModel
            .streamViewers
//            .observeOn(MainScheduler.instance)
            .bind(to: view.streamViewers)
            .disposed(by: view.bag)
        
        viewModel
            .streamLikes
            .bind(to: view.streamLikes)
            .disposed(by: view.bag)
        
        view
            .onFetchMoreMessages
//            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] _ in
                self?.viewModel.fetchMoreMessages()
            }
            .disposed(by: view.bag)
        
        viewModel
            .onChangeTeacherCameraState
            .subscribe(onNext: { [weak view] cameraIsActive in
                view?.hideVideoByTeacher(!cameraIsActive)
                view?.presentStream(cameraIsActive)
            })
            .disposed(by: view.bag)
        
        view
            .lessonFinished
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] _ in
                self?.viewModel.handleLessonFinished()
            }
            .disposed(by: view.bag)
        
        viewModel
            .onRemoveLessonView
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak view] _ in
                view?.removeStreamView()
            })
            .disposed(by: view.bag)
        
        viewModel
            .onPresentVipLessonPopup
//            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak view] popup in
                view?.presentVipLessonPopup(popupView: popup)
            })
            .disposed(by: view.bag)
        
        view
            .onCameraTap
//            .observeOn(MainScheduler.instance)
            .bind { [weak self, weak view] _ in
                guard let self = self, let view = view else { return }
                self.viewModel.cameraIsActiveState.toggle()
                view.isCameraOn.accept(self.viewModel.cameraIsActiveState)
                view.showFullscreenAvatarPlaceholder(!self.viewModel.cameraIsActiveState)
            }
            .disposed(by: view.bag)
        
        view
            .onMicrophoneTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self, weak view] _ in
                guard let self = self, let view = view else { return }
                self.viewModel.microOrPlayerIsActiveState.toggle()
                self.viewModel.handleMicrophoneTap()
                view.isMicroOn.accept(self.viewModel.microOrPlayerIsActiveState)
            }
            .disposed(by: view.bag)
        
        viewModel
            .onShowLoader
            .subscribe(onNext: { [weak view] show in
                if show {
                    view?.showLoader()
                } else {
                    view?.hideLoader()
                }
            })
            .disposed(by: view.bag)
        
        view
            .onAvatarButtonTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.handleProfleInfoButtonTap()
            }
            .disposed(by: view.bag)
        
        view
            .onAddTeacherButtonTap
            .bind(onNext: { [weak self, weak view] _ in
                guard let self = self, let view = view, let model = self.viewModel.infoStream else { return }
                self.viewModel.fetchData(classNumber: model.specialistInfo.numberClass ?? "")
                model.isContact
                    ? self.viewModel.removeContact(id: model.specialistInfo.id ?? 0)
                    : self.viewModel.addContact(id: model.specialistInfo.id ?? 0)
                view.isContact = model.isContact
                view.isContact.toggle()
                view.setValue(isContact: view.isContact)
                if view.isContact {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        NotificationSubscription.shared.showNotification(.subscription)
                    }
                }
            })
            .disposed(by: view.bag)
        
        viewModel
            .isContactRelay
            .bind { [weak view] isContact in
                view?.setValue(isContact: isContact)
            }
            .disposed(by: bag)

        view.onShareTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.shareApp()
            }
            .disposed(by: view.bag)

        view.onLikeTap
            .bind { [weak self, weak view] _ in
                guard
                    let self = self,
                    let view = view,
                    let lessonDetail = AllLiveLessonsModel.liveLessonsArray.first(where: { $0.id == view.streamID.value })
                else {
                    return
                }
                let completion: (LessonDetailResponse) -> Void = { [weak view] lessonDetail in
                    view?.updateLikes(lessonDetail: lessonDetail)
                }
                if !(lessonDetail.liked ?? false) {
                    self.viewModel.setLike(id: view.streamID.value, completion: completion)
                } else {
                    self.viewModel.deleteLike(id: view.streamID.value, completion: completion)
                }
            }
            .disposed(by: view.bag)

//        viewModel
//            .onUpdatePlayerSize
//            .observeOn(MainScheduler.instance)
//            .subscribe { [weak view]  size in
//                view?.setPlayerSize(size: size)
//            }
//            .disposed(by: view.bag)

//        viewModel
//            .onStartPlaying
//            .observeOn(MainScheduler.instance)
//            .subscribe(onNext: { [weak view] in
//                view?.avatarView.isHidden = true
//            })
//            .disposed(by: viewModel.bag)

//        viewModel
//            .onChangeTeacherCameraState
//            .subscribe(onNext: { [weak view] cameraState in
//                if cameraState {
//                    // do nothing because here we wait not for cameraState but for video, for moment when it strarts playing
//                    // this moment is when viewModel.onShowLoader returns false
//                } else {
//                    view?.avatarView.isHidden = false
//                }
//            })
//            .disposed(by: view.bag)

        Observable
            .combineLatest(viewModel.onShowLoader, viewModel.onLessonFinished)
            .observeOn(MainScheduler.instance)
            .do { [weak view] show, lessonIsFinished in
                if show, !lessonIsFinished {
                    view?.avatarView.isHidden = false
                } else {
                    view?.avatarView.isHidden = true
                }
            }
            .distinctUntilChanged { $0 == $1 }
            .debounce(.seconds(5), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak view] show, lessonIsFinished in
                if show, !lessonIsFinished {
                    view?.showLoader()
                } else {
                    view?.hideLoader()
                }
            })
            .disposed(by: view.bag)
    }
    
    private func shareApp() {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "talkme.page.link"
        components.path = "/stream"

        let lessonIdQueryItem = URLQueryItem(name: "lessonId", value: "\(viewModel.currentLessonId ?? 0)")
        let userIdQueryItem = URLQueryItem(name: "userId", value: "\(viewModel.currentTeacherId ?? UserDefaultsHelper.shared.userId)")
        components.queryItems = [lessonIdQueryItem, userIdQueryItem]

        guard let linkParameter = components.url else { return }

        let vc = UIActivityViewController(activityItems: [linkParameter], applicationActivities: nil)
        present(vc, animated: true)
    }
}

// MARK: - extensions

private extension CollectionView {
    func scroll(to index: Int, animated: Bool) throws {
        guard self.provider as? ComposedProvider == nil else {
            throw(CollectionKitError.unableToScroll)
        }
        guard let provider = self.provider else {
            throw(CollectionKitError.unableToScroll)
        }
        let itemFrame = provider.frame(at: index)
        var targetPoint = itemFrame.origin
        let itemXDiff = self.contentSize.width - itemFrame.origin.x
        let itemYDiff = self.contentSize.height - itemFrame.origin.y
        if itemXDiff < self.visibleFrame.width || itemYDiff < self.visibleFrame.height {
            targetPoint = CGPoint(
                x: self.contentSize.width - self.visibleFrame.width,
                y: self.contentSize.height - self.visibleFrame.height
            )}
        if animated {
            UIView.animate(withDuration: 0.3) {
                self.contentOffset = targetPoint
            }
        } else {
            self.contentOffset = targetPoint
        }
    }
}

//enum CollectionKitError: Error {
//    case unableToScroll
//}

