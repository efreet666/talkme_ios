//
//  StreamPageControlView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 10.03.2021.
//

import RxSwift

final class StreamPageControlView: UIView, RepeatableViewProtocol {

    // MARK: - Public Properties

    let size: CGSize
    let viewTag: Int
    let bag = DisposeBag()

    // MARK: - Initializers

    init(size: CGSize, isSelected: Bool, viewTag: Int, viewsData: GenericViewType) {
        self.viewTag = viewTag
        self.size = size
        super.init(frame: CGRect(origin: .zero, size: size))
        layer.cornerRadius = size.height / 2
        backgroundColor = TalkmeColors.grayPageIndicator
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setSelected(_ selected: Bool) {
        backgroundColor = selected ? .white : TalkmeColors.grayPageIndicator
    }
}
