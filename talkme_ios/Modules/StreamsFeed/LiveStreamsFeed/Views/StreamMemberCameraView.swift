//
//  StreamMemberCameraView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 25.03.2021.
//

import Foundation
import RxSwift
import RxCocoa
import Kingfisher

final class StreamMemberCameraView: UIView {

    // MARK: - Public properties

    let isCameraOn = BehaviorRelay<Bool>(value: false)
    let isMicrophoneOn = BehaviorRelay<Bool>(value: false)
    let isCameraButtonActive = PublishRelay<Bool>()
    let isMicroButtonActive = PublishRelay<Bool>()
    let bag = DisposeBag()
    let onTimeIsUp = PublishRelay<Void>()
    private(set) lazy var onCameraTap = cameraButton.rx.tap
    private(set) lazy var onMicrophoneTap = microphoneButton.rx.tap

    // MARK: - Private Properties

    private var cameraView: UIView?

    private let userAvatar: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 11
        iv.clipsToBounds = true
        return iv
    }()

    private let microphoneButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFill
        return button
    }()

    private let cameraButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFill
        button.contentHorizontalAlignment = .fill
        button.contentVerticalAlignment = .fill
        return button
    }()

    private let timerLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = .white
        return label
    }()

    private var timer: Timer?

    // MARK: - Initializers

    init(frame: CGRect, publisherView: UIView?, avatarUrl: String, timeEnd: Double) {
        super.init(frame: frame)
        initialSetUp(size: frame.size)
        configure(avatarUrl: avatarUrl, publisherView: publisherView, time: timeEnd)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func updateFrames(_ size: CGSize) {
        userAvatar.frame = CGRect(origin: .zero, size: CGSize(width: size.width, height: size.height / 1.09))
        let buttonWidth = min(size.width, size.height) / 3.32
        let buttonSize = CGSize(width: buttonWidth, height: buttonWidth)
        microphoneButton.frame = CGRect(origin: .zero, size: buttonSize)
        cameraButton.frame = CGRect(origin: .zero, size: buttonSize)
        let cameraCenterX = size.width / 3.77
        let cameraCenterY = userAvatar.frame.maxY - buttonWidth / 17
        cameraButton.center = CGPoint(x: cameraCenterX, y: cameraCenterY)
        microphoneButton.center = CGPoint(x: size.width - cameraCenterX, y: cameraCenterY)
        timerLabel.frame = CGRect(
            origin: .zero,
            size: CGSize(
                width: UIScreen.isSE ? 38 : 44,
                height: UIScreen.isSE ? 19 : 22
            )
        )
        let timerCenterY = userAvatar.frame.height / (UIScreen.isSE ?  9.59 : 7.6 )
        let timerCenterX = size.width / (UIScreen.isSE ? 1.31 : 1.32)
        timerLabel.center = CGPoint(x: timerCenterX, y: timerCenterY)
    }

    func configure(avatarUrl: String?, publisherView: UIView?, time: TimeInterval?) {
        if let urlString = avatarUrl, let url = URL(string: urlString) {
            userAvatar.kf.setImage(with: ImageResource(downloadURL: url, cacheKey: url.absoluteString), placeholder: UIImage(named: "noAvatar"))
        } else {
            userAvatar.image = UIImage(named: "noAvatar")
        }

        if let time = time {
            setTimer(time: time)
        }

        guard let publisher = publisherView else { return }
        userAvatar.subviews.forEach { $0.removeFromSuperview() }
        userAvatar.addSubview(publisher)

        publisher.frame = userAvatar.bounds
        cameraView = publisher
        cameraView?.backgroundColor = .clear

        self.bindUI()
    }

    // MARK: - Private Methods

    private func initialSetUp(size: CGSize) {
        addSubviews(userAvatar, cameraButton, microphoneButton, timerLabel)
        updateFrames(size)
    }

    private func setTimer(time: TimeInterval) {
        let adjustedTime = time
        if time <= 0 {
            onTimeIsUp.accept(())
        }
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.setFormatedTime(time: adjustedTime + 1)
        }
    }

    private func setFormatedTime(time: TimeInterval) {
        let time = TimerManager.diffFromFutureTime(timeInterval: time)
        guard time >= 0 else {
            timer?.invalidate()
            onTimeIsUp.accept(())
            return
        }
        timerLabel.text = Formatters.minutesSeconds(time: time)
        timerLabel.sizeToFit()
    }

    private func bindUI() {
        isCameraOn
            .bind { [weak self] isOn in
                self?.cameraButton.setImage(UIImage(named: isOn ? "cameraOn" : "cameraOff"), for: .normal)
                self?.cameraView?.isHidden = !isOn
            }
            .disposed(by: bag)

        isMicrophoneOn
            .bind { [weak self] isOn in
                self?.microphoneButton.setImage(UIImage(named: isOn ? "microfonOn" : "microfonOff"), for: .normal)
            }
            .disposed(by: bag)

        isMicroButtonActive
            .bind { [weak self] isOn in
                self?.microphoneButton.isEnabled = isOn
            }
            .disposed(by: bag)

        isCameraButtonActive
            .bind { [weak self] isOn in
                self?.cameraButton.isEnabled = isOn
            }
            .disposed(by: bag)
    }
}
