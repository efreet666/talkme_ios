//
//  StreamSendMessageView.swift
//  talkme_ios
//
//  Created by Den on 15.03.2021.
//

import RxSwift
import UIKit

final class StreamSendMessageView: UIView {

    private enum Constants {
        static let avatarSize = CGSize(width: UIScreen.isSE ? 31 : 40, height: UIScreen.isSE ? 31 : 40)
        static let textViewHorisontalOffset: CGFloat = UIScreen.isSE ? 14 : 18
        static let textViewHeight: CGFloat = UIScreen.isSE ? 30 : 35
        static let sendMessageRightOffset: CGFloat = UIScreen.isSE ? 13 : 17
        static let sendMessageSize = CGSize(width: UIScreen.isSE ? 20 : 24, height: UIScreen.isSE ? 17 : 21)
        static let constantHeightDifference: CGFloat = 85
        static let constantHeightTF: CGFloat  = 104
        static let constantFrameOrigin: CGFloat  = 40
    }

    // MARK: - Public Properties

    private(set) lazy var sendMessage = sendMessageImageView.rx.tapGesture().when(.recognized)
        .compactMap { [weak self] _ in self?.textView.text }
        .do(onNext: { [weak self] _ in self?.endEditing() })

    // MARK: - Private Properties

    private var textViewHeight: CGFloat = Constants.textViewHeight
    private var orientationIsPortrait = true

    private var avatarOrigin: CGPoint {
        CGPoint(x: (UIScreen.isSE ? 10 : 13) + UIApplication.leftInset, y: UIScreen.isSE ? 7 : 9)
    }
    private lazy var avatarImageView: UIImageView = {
        let view = UIImageView(frame: CGRect(origin: avatarOrigin, size: Constants.avatarSize))
        view.contentMode = .scaleAspectFill
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Constants.avatarSize.height / 2
        return view
    }()

    private var sendMessageImageViewOrigin: CGPoint {
        CGPoint(
            x: bounds.width
                - Constants.sendMessageRightOffset
                - Constants.sendMessageSize.width
                - UIApplication.rightInset,
            y: bounds.height / 2 - Constants.sendMessageSize.height / 2)
    }

    private lazy var sendMessageImageView: UIImageView = {
        let view = UIImageView(frame: CGRect(origin: sendMessageImageViewOrigin, size: Constants.sendMessageSize))
        view.image = UIImage(named: "sendMessageImage")
        view.contentMode = .scaleToFill
        return view
    }()

    private var textViewWidth: CGFloat {
        sendMessageImageView.frame.minX - textViewOrigin.x - Constants.textViewHorisontalOffset
    }

    private var textViewOrigin: CGPoint {
        CGPoint(
            x: avatarOrigin.x + Constants.avatarSize.width + Constants.textViewHorisontalOffset,
            y: UIScreen.isSE ? 7 : 11)
    }

    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.frame = CGRect(
            origin: textViewOrigin,
            size: CGSize(
                width: textViewWidth,
                height: Constants.textViewHeight))
        textView.textColor = .white
        textView.contentInset = .zero
        textView.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        textView.backgroundColor = .clear
        textView.delegate = self
        textView.tintColor = .white
        return textView
    }()

    private lazy var placeholderLabel: UILabel = {
        let label = UILabel(frame: textView.frame)
        label.frame.origin.x += 4
        label.backgroundColor = .clear
        label.text = "stream_send_message_placeholder".localized
        label.textColor = TalkmeColors.sendMessagePlaceholder
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func startEditing() {
        guard orientationIsPortrait else {
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            textView.becomeFirstResponder()
            return
        }
        textView.becomeFirstResponder()
    }

    func endEditing() {
        textView.text = ""
        textViewDidChange(textView)
        endEditing(true)
    }

    func updateFrames() {
        sendMessageImageView.frame.origin = sendMessageImageViewOrigin
        avatarImageView.frame.origin = avatarOrigin
        textView.frame.origin = textViewOrigin
        textView.frame.size.width = textViewWidth
        placeholderLabel.frame.origin.x = textView.frame.origin.x + 4
    }

    func deviceOrientationChanged(isPortrait: Bool) {
        orientationIsPortrait = isPortrait
    }

    // MARK: - Private Methods

    private func configureUI() {
        backgroundColor = TalkmeColors.sendMessageBG
        addSubviews(avatarImageView, sendMessageImageView, textView, placeholderLabel)
        avatarImageView.kf.setImage(with: URL(string: UserDefaultsHelper.shared.avatarImage))
    }
}

extension StreamSendMessageView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        guard let text = textView.text else { return }

        placeholderLabel.isHidden = !text.isEmpty
        let heightDifference = textView.contentSize.height - self.textViewHeight + 5
        guard heightDifference != 0 else { return }

        let newHeight = min(textView.frame.height + heightDifference, 71)
        guard textView.frame.height <= newHeight || heightDifference < 0 else { return }

        if textView.contentSize.height <= 76 {
            textView.frame.size.height += heightDifference
            frame.size.height += heightDifference
            textViewHeight = textView.frame.height
            frame.origin.y -= heightDifference
        } else {
            textView.frame.size.height = Constants.constantHeightDifference
            frame.size.height = Constants.constantHeightTF
            textViewHeight = textView.frame.height
            frame.origin.y -= Constants.constantFrameOrigin
        }
    }
}
