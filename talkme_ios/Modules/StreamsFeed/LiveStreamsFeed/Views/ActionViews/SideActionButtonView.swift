//
//  SideActionButtonView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.03.2021.
//

import UIKit

final class SideActionButtonView: UIView {

    let size: CGSize

    private enum Constants {
        static let iconWidth = UIScreen.isSE ? 21 : 32
        static let iconSize = CGSize(width: iconWidth, height: iconWidth)

        static let muteIconWidth = UIScreen.isSE ? 11 : 22
        static let muteIconSize = CGSize(width: muteIconWidth, height: muteIconWidth)
    }

    // MARK: - Private Properties

    private let iconView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()

    private let hexagonView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = UIImage(named: "hexagon")
        return iv
    }()

    private let roundBGView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        view.clipsToBounds = true
        return view
    }()

    private let blackCyrcleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "soundBack")
        imageView.alpha = 0.25
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: - Initializers

    init(type: StreamSideActionType, size: CGSize, isActive: Bool = true) {
        self.size = size

        super.init(frame: CGRect(origin: .zero, size: size))
        iconView.image = type.icon(isActive: isActive)
        iconView.tintColor = type.tintColor

        setupLayout(for: type)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupIcon(for type: StreamSideActionType, isActive: Bool = true) {
        iconView.image = type.icon(isActive: isActive)
        iconView.tintColor = type.tintColor
    }

    // MARK: - Private Methods

    private func setupLayout(for type: StreamSideActionType) {
        switch type {
        case .gift, .fullScreen:
            setupForRoundBackground()
        case .complaint, .complaintBlack:
            setupForComplaint()
        case .mute:
            setupLayoutForImageBackground()
        default:
            setupForHexagonBackground()
        }
    }

    private func setupForComplaint() {
        addSubview(iconView)
        let iconSize = Constants.iconSize
        iconView.frame = .init(origin: .zero, size: bounds.size)
    }

    private func setupForRoundBackground() {
        addSubview(roundBGView)
        roundBGView.addSubview(iconView)

        let roundBGViewOrigin = CGPoint.zero
        let roundBGViewWidth = bounds.width
        let roundBGViewOriginSize = CGSize(width: roundBGViewWidth, height: roundBGViewWidth)
        roundBGView.frame = CGRect(origin: roundBGViewOrigin, size: roundBGViewOriginSize)
        roundBGView.layer.cornerRadius = roundBGViewWidth / 2

        let iconSize = Constants.iconSize
        let iconOrigin = CGPoint(
            x: (roundBGView.frame.width - iconSize.width) / 2,
            y: (roundBGView.frame.height - iconSize.height) / 2)
        iconView.frame = .init(origin: iconOrigin, size: iconSize)
    }

    private func setupForHexagonBackground() {
        addSubview(hexagonView)
        hexagonView.addSubview(iconView)

        hexagonView.frame = bounds
        let iconSize = Constants.iconSize
        let iconOrigin = CGPoint(
            x: (bounds.width - iconSize.width) / 2,
            y: (bounds.height - iconSize.height) / 2)
        iconView.frame = .init(origin: iconOrigin, size: iconSize)
    }

    private func setupLayoutForImageBackground() {
        addSubviews(blackCyrcleImageView,
                    iconView
        )

        blackCyrcleImageView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(self)
        }

        iconView.snp.makeConstraints { make in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
        }
    }
}
