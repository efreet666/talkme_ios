//
//  StreamAttributeView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.03.2021.
//

import UIKit

final class StreamAttributeView: UIView {

    private enum Constants {
        static let iconWidth: CGFloat = UIScreen.isSE ? 14 : 18
        static let iconSize: CGSize = .init(width: iconWidth, height: iconWidth)
        static let timeWidth: CGFloat = UIScreen.isSE ? 65 : 74
        static let font: UIFont = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 14)
    }

    enum ViewType {
        case time
        case viewers

        var icon: UIImage? {
            switch self {
            case .time:
                return UIImage(named: "streamTimer")
            case .viewers:
                return UIImage(named: "streamViewer")
            }
        }
    }

    // MARK: - Public Properties

    var size: CGSize {
        let width = spacing + Constants.iconWidth + labelWidth
        return .init(width: width, height: Constants.iconWidth)
    }

    // MARK: - Private Properties

    private let icon: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()

    let label: UILabel = {
        let label = UILabel()
        label.font = Constants.font
        label.textColor = .white
        return label
    }()

    private var labelWidth: CGFloat {
        switch type {
        case .time:
            return Constants.timeWidth
        case .viewers:
            return label.frame.width
        }
    }

    private(set) lazy var spacing: CGFloat = {
        switch type {
        case .time:
            return UIScreen.isSE ? 0 : 0
        case .viewers:
            return 1
        }
    }()

    private let type: ViewType

    // MARK: - Initializers

    init(type: ViewType) {
        self.type = type
        super.init(frame: .zero)
        icon.image = type.icon
        addSubviews(icon, label)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(text: String) {
        label.text = text
        label.sizeToFit()
        setupLayout()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        icon.frame = .init(origin: .zero, size: Constants.iconSize)
        let labelOrigin = CGPoint(x: icon.frame.maxX + spacing, y: 0)
        label.frame = CGRect(
            origin: labelOrigin,
            size: CGSize(width: labelWidth, height: Constants.iconSize.height))
        frame = CGRect(origin: frame.origin, size: size)
    }

}
