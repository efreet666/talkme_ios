//
//  StreamFeedFooterView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.03.2021.
//

import UIKit
import RxSwift

final class StreamFeedFooterView: UIView {

    private enum Constants {
        static let messageSize: CGSize = UIScreen.isSE ? .init(width: 34, height: 36) : .init(width: 47, height: 52)
        static let messageLeft: CGFloat = UIScreen.isSE ? 10 : 13
        static let commentLabelOrigin: CGPoint = UIScreen.isSE ? .init(x: 10, y: 5) : .init(x: 13, y: 8)
        static let commentContainerSize: CGSize = UIScreen.isSE ? .init(width: 120, height: 27) : .init(width: 125, height: 35)
        static let commentLabelSize: CGSize = .init(
            width: commentContainerSize.width - commentLabelOrigin.x * 2,
            height: commentContainerSize.height - commentLabelOrigin.y * 2 )
        static let commentContainerLeft: CGFloat = UIScreen.isSE ? 9 : 14
        static let commentContainerRight: CGFloat = UIScreen.isSE ? 24 : 46
        static let commentFontSize: CGFloat = UIScreen.isSE ? 11 : 13
    }

    // MARK: - Public Properties

    private(set) lazy var onCommentTap = commentContainer.rx.tapGesture().when(.recognized)
    private(set) lazy var onMessageButtonTap = messageButton.rx.tapGesture().when(.recognized)
    private(set) lazy var lessonFinished = attributesView.lessonFinished

    // MARK: - Private Properties

    private let attributesView = StreamTimeViewersView()
    private let messageButton = SideActionButtonView(type: .message, size: Constants.messageSize)

    private let commentContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Constants.commentContainerSize.height / 2
        view.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        return view
    }()

    private let commentLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: Constants.commentFontSize)
        label.textColor = TalkmeColors.streamCommentPlaceholder
        label.text = "stream_feed_hello".localized
        return label
    }()

    private let userType: UserType

    // MARK: - Initializers

    init(frame: CGRect, userType: UserType) {
        self.userType = userType
        super.init(frame: frame)

        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        attributesView.frame = CGRect(
            origin: CGPoint(x: 0, y: (bounds.height - attributesView.size.height) / 2),
            size: CGSize(width: frame.width, height: attributesView.size.height)
        )
        messageButton.frame.origin = CGPoint(x: Constants.messageLeft, y: 0)
        commentLabel.frame = CGRect(origin: Constants.commentLabelOrigin, size: Constants.commentLabelSize)
        commentContainer.frame = .init(
            origin: CGPoint(
                x: messageButton.frame.maxX + Constants.commentContainerLeft,
                y: (bounds.height - Constants.commentContainerSize.height) / 2
            ),
            size: Constants.commentContainerSize
        )
    }

    // MARK: - Public Methods

    func hideChatInput( isHidden: Bool) {
        commentContainer.isHidden = isHidden
        messageButton.isHidden = isHidden
        attributesView.hideViewwers(isHidden: isHidden)
    }

    func hideTimer(isHidden: Bool) {
        attributesView.hideTimer(isHidden: isHidden)
    }

    func startLiveStreamTimer(withEndDate: Double) {
        attributesView.startTimerForLiveStream(dateEnd: withEndDate)
    }

    func startSavedStreamTimer(withDuration: Double) {
        attributesView.startTimerForSavedStream(withDuration: withDuration)
    }

    func invalidateLiveStreamTimer() {
        attributesView.invalidateLiveStreamTimer()
    }

    func setViewersCount(count: Int) {
        attributesView.setViewers(count: count)
    }

    func toggleCommentView() {
        commentContainer.isHidden.toggle()
    }

    func updateFrames(isLandscape: Bool) {
        attributesView.isHidden = isLandscape
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews(attributesView, messageButton, commentContainer)
        commentContainer.addSubview(commentLabel)
    }

}
