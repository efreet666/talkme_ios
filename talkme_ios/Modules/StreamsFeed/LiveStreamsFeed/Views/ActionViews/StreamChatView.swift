//
//  StreamChatView.swift
//  talkme_ios
//
//  Created by Den on 13.03.2021.
//

import CollectionKit
import RxSwift

final class StreamChatView: UIView {

    // MARK: - Public Properties

    private(set) var onFetchMoreMessages = PublishSubject<Void>()

    // MARK: - Private Properties

    private let bag = DisposeBag()
    private var isLoading = false
    private var messagesCount = 0

    private lazy var collection: CollectionView = {
        let collection = CollectionView(frame: self.bounds)
        collection.bounces = false
        collection.contentInsetAdjustmentBehavior = .never
        collection.showsVerticalScrollIndicator = false
        collection.showsHorizontalScrollIndicator = false
        collection.contentInset = UIEdgeInsets(top: UIScreen.isSE ? 12 : 14, left: 0, bottom: 74, right: 0)
        collection.transform = CGAffineTransform(scaleX: 1, y: -1)
        return collection
    }()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        configureUI()
        fetchOldMessages()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        collection.frame = bounds
        setupGradient()
    }

    // MARK: - Public Methods

    func setupCollectionView(dataSource: ArrayDataSource<LessonMessage>) {
        let giftSource = ClosureViewSource(viewGenerator: { [weak self] _, _ -> CommonMessageCell in
            return StreamChatGiftView(frame: self?.bounds ?? .zero)
        }, viewUpdater: { (view: CommonMessageCell, dataItem: LessonMessage, _) in
            self.messagesCount = dataSource.data.count
            view.configure(model: dataItem)
        })

        let textSource = ClosureViewSource(viewGenerator: { [weak self] _, _ -> CommonMessageCell in
            return StreamChatMessageView(frame: self?.bounds ?? .zero)
        }, viewUpdater: { (view: CommonMessageCell, dataItem: LessonMessage, _) in
            self.isLoading = false
            self.messagesCount = dataSource.data.count
            view.configure(model: dataItem)
        })

        let provider = BasicProvider(
            dataSource: dataSource,
            viewSource: ComposedViewSource(viewSourceSelector: { data in
                if data.gift != nil {
                    return giftSource
                } else {
                    return textSource
                }
            }),
            sizeSource: ClosureSizeSource(sizeSource: { _, data, collectionSize in
                var height: CGFloat = 0
                if data.gift != nil {
                    height = StreamChatGiftView.cellHeight(from: data, for: collectionSize.width)
                } else {
                    height = StreamChatMessageView.cellHeight(from: data, for: collectionSize.width)
                }
                return CGSize(width: collectionSize.width, height: height)
            })
        )
        provider.layout = FlowLayout(lineSpacing: UIScreen.isSE ? 7 : 8)
        collection.provider = provider
    }

    // MARK: - Private Methods

    private func configureUI() {
        addSubviews(collection)
    }

    private func setupGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [
            UIColor.clear.cgColor,
            UIColor.black.cgColor,
            UIColor.black.cgColor,
            UIColor.clear.cgColor
        ]
        let topPoint = NSNumber(value: Float(74 / (UIScreen.height / 2)))
        let bottomPoint = NSNumber(value: Float(1 - 14 / (UIScreen.height / 2)))
        gradient.locations = [0.0, topPoint, bottomPoint, 1.0]
        layer.mask = gradient
    }

    private func fetchOldMessages() {
        collection.rx.didEndDragging
            .bind { [weak self] _ in
                guard let self = self else { return }
                guard self.collection.visibleIndexes.contains(self.messagesCount - 1), self.isLoading == false else { return }
                self.onFetchMoreMessages.onNext(())
                self.isLoading = true
            }
            .disposed(by: bag)
    }
}
