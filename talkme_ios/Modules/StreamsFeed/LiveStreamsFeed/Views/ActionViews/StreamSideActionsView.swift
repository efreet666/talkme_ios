//
//  StreamSideActionsView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.03.2021.
//

import RxCocoa
import RxSwift

final class StreamSideActionsView: UIView {

    private enum Constants {
        static let spacing: CGFloat = UIScreen.isSE ? 14 : 20
        static let hexagonIconSize = UIScreen.isSE ? CGSize(width: 38, height: 41) : CGSize(width: 51, height: 56)
        static let circleIconWidth = UIScreen.isSE ? 32 : 44
        static let circleIconSize = CGSize(width: circleIconWidth, height: circleIconWidth)
    }

    // MARK: - Public Properties

    let isCameraOn = PublishRelay<Bool>()
    let isMicroOn = PublishRelay<Bool>()
    private(set) lazy var onFlipCameraTap = flipCameraButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onGiftTap = giftButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onFullScreenTap = fullScreenButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onViewersTap = membersWithMicroButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onCameraTap = cameraButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onMicrophoneTap = microphoneButton.rx.tapGesture().when(.recognized)

    private(set) lazy var selfFrame = frame
    private(set) lazy var membersWithMicroButtonFrame = membersWithMicroButton.frame

    var topViewButtonFrame: CGRect {
        switch type {
        case .participant, .viewer:
            return giftButton.frame
        case .specialist:
            return flipCameraButton.frame
        }
    }

    // MARK: - Private Properties

    private let giftButton = SideActionButtonView(type: .gift, size: Constants.circleIconSize)
    private let fullScreenButton = SideActionButtonView(type: .fullScreen, size: Constants.circleIconSize)
    private let membersWithMicroButton = SideActionButtonView(type: .viewers, size: Constants.hexagonIconSize)
    private let cameraButton = SideActionButtonView(type: .camera, size: Constants.hexagonIconSize)
    private let microphoneButton = SideActionButtonView(type: .microphone, size: Constants.hexagonIconSize)
    private let flipCameraButton = SideActionButtonView(type: .flipCamera, size: Constants.hexagonIconSize)

    private var currentSideViews = [SideActionButtonView]()

    private var activeCurrentSideViews: [SideActionButtonView] {
        currentSideViews.filter { $0.isHidden == false }
    }

    private lazy var stackButtons: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.spacing = Constants.spacing
        return stack
    }()

    private let type: UserType
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(type: UserType) {
        self.type = type
        super.init(frame: .zero)

        setupLayout()
        updateOrientationUI()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard
            traitCollection.verticalSizeClass != previousTraitCollection?.verticalSizeClass
            || traitCollection.horizontalSizeClass != previousTraitCollection?.horizontalSizeClass
        else {
            return
        }

        updateOrientationUI()
    }

    // MARK: - Public Methods

    func hideCameraAndMicrophoneButtons(isHidden: Bool) {
        cameraButton.isHidden = isHidden
        microphoneButton.isHidden = isHidden
    }

    func hideFullScreenButton(isHidden: Bool) {
        // uncommit this to unHide button that rotates video to landscape mode and back
//        fullScreenButton.isHidden = isHidden
        fullScreenButton.isHidden = true
    }

    // MARK: - Private Methods

    private func setupLayout() {
        membersWithMicroButton.isHidden = true  // todo: убрать когда будет покупка камеры

        switch type {
        case .specialist:
            currentSideViews = [flipCameraButton, membersWithMicroButton, cameraButton, microphoneButton]
        case .participant, .viewer:
            currentSideViews = [fullScreenButton, giftButton, membersWithMicroButton, microphoneButton]
        }

        addSubview(stackButtons)
        stackButtons.addArrangedSubviews(currentSideViews)

        currentSideViews.forEach { button in
            button.snp.makeConstraints { make in
                make.size.equalTo(button.size)
            }
        }

        stackButtons.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }

    private func updateOrientationUI() {
        giftButton.isHidden = UIApplication.shared.statusBarOrientation.isLandscape
    }

    private func bindUI() {
        isMicroOn
            .observeOn(MainScheduler.instance)
            .bind { [weak self] isOn in
                self?.microphoneButton.setupIcon(for: .microphone, isActive: isOn)
            }
            .disposed(by: bag)

        isCameraOn
            .observeOn(MainScheduler.instance)
            .bind { [weak self] isOn in
                self?.cameraButton.setupIcon(for: .camera, isActive: isOn)
            }
            .disposed(by: bag)
    }
}
