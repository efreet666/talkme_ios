//
//  StreamDonationViewNew.swift
//  talkme_ios
//
//  Created by admin on 06.10.2022.
//

import RxSwift
import UIKit

enum PriceNew: String {
    case fifty = "50"
    case hundred = "100"
    case twoHundred = "200"
    case fiveHundred = "500"

    var isActive: Bool {
        switch self {
        case .fifty, .hundred, .twoHundred, .fiveHundred:
            return false
        }
    }
}

final class StreamDonationViewNew: UIView {

    // MARK: - Public Properties

    private(set) lazy var ratingTap = ratingView.ratingRelay
    private(set) lazy var textRelay = enterDonationView.textRelay
    let bag = DisposeBag()

    // MARK: - Private Properties

    private var withRating: Bool!
    private let priceArray: [PriceNew] = [.fifty, .hundred, .twoHundred, .fiveHundred]
    private let dataSourceButtonView: [GenericViewType] = [
        .pay(PriceNew.fifty),
        .pay(PriceNew.hundred),
        .pay(PriceNew.twoHundred),
        .pay(PriceNew.fiveHundred)]
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "specify_the_amount_stream".localized
        label.textColor = TalkmeColors.otherDarkViewColor
        label.textAlignment = .left
        label.font = UIScreen.isSE ? .montserratFontRegular(ofSize: 13) : .montserratFontRegular(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()

    private let enterDonationView = EnterDonationView()
    private let ratingView = StreamRatingView()
    private lazy var priceButtonView = RepeatingViewsMakerView(
        viewsData: dataSourceButtonView,
        viewType: StreamPriceButtonView.self,
        size: CGSize(width: UIScreen.width / 6, height: UIScreen.isSE ? 32 : 40),
        spacing: 10)
    var isStreamEnded = false

    // MARK: - Initializers

    init(frame: CGRect, withRating: Bool) {
        super.init(frame: frame)
        backgroundColor = TalkmeColors.notGradient.withAlphaComponent(0.7)
        self.withRating = withRating
        setupLayout()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        ratingView.isHidden = !withRating
        addSubviews(titleLabel, ratingView, enterDonationView, priceButtonView)

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(19)
            make.leading.equalToSuperview().inset(28)
        }

        ratingView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 15 : 17)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 54 : 80)
            make.bottom.equalTo(enterDonationView.snp.top).offset(UIScreen.isSE ? -13 : -22)
        }

        enterDonationView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 35 : 49)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 25 : 44)
            make.top.equalTo(titleLabel.snp.bottom).offset(
                withRating
                ? UIScreen.isSE ? 57 : 80
                : UIScreen.isSE ? 31 : 33
            )
        }

        priceButtonView.snp.makeConstraints { make in
            make.top.equalTo(enterDonationView.snp.bottom).offset(UIScreen.isSE ? 18 : 20)
            make.size.equalTo(priceButtonView.bounds.size)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(32)
        }
    }

   private func buttonTouched(_ sender: UIButton) {
        UIButton.animate(
            withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
                sender.alpha = 0.5
            },
            completion: { _ in
                UIButton.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform.identity
                    sender.alpha = 1
                })
            })
    }

    private func selectedPriceView() {
        priceButtonView.viewsArray.forEach {
            $0.setSelected(isStreamEnded)
        }
    }

    private func clearSelectedButton() {
        enterDonationView.clearSelectedButton
            .bind { [weak self] _ in
                self?.selectedPriceView()
            }
            .disposed(by: enterDonationView.bag)
    }

    private func bindUI() {
        clearSelectedButton()

        priceButtonView
            .onViewTap
            .bind { [weak self] view in
                guard let self = self else { return }
                self.selectedPriceView()
                view.setSelected(true)
                let text = self.priceArray[view.viewTag].rawValue
                self.enterDonationView.setupText(text)
                self.textRelay.accept(text)
            }
            .disposed(by: bag)
    }
}

