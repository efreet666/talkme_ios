//
//  DonateFieldView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 04.03.2021.
//

import RxCocoa
import RxSwift

final class EnterDonationView: UIView {

    // MARK: - Public Properties

    let textRelay = BehaviorRelay<String?>(value: nil)
    let clearSelectedButton = PublishRelay<String?>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        return view
    }()

    private let coinImage: UIImageView = {
        let coinImage = UIImageView()
        coinImage.image = UIImage(named: "coin")
        coinImage.contentMode = .scaleAspectFill
        return coinImage
    }()

    private let textField: UITextField = {
        let textField = UITextField()
        textField.keyboardType = .numberPad
        textField.tintColor = TalkmeColors.white
        textField.font = UIScreen.isSE ? .montserratBold(ofSize: 50) : .montserratBold(ofSize: 55)
        textField.textColor = TalkmeColors.white
        textField.textAlignment = .left
        return textField
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        textField.delegate = self
        setupLayout()
        bindUI()
//        textField.becomeFirstResponder() todo: пока не нужно чтобы клавиатура появлялась по умолчанию
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupText(_ text: String) {
        textField.text = text
    }

    // MARK: - Private Methods

    private func bindUI() {
        textField.rx.text
            .bind(to: textRelay)
            .disposed(by: bag)

        textField.rx.text
            .bind(to: clearSelectedButton)
            .disposed(by: bag)
    }

    private func setupLayout() {
        addSubviews([textField, coinImage, separatorView])

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(textField.snp.bottom).offset(7)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }

        coinImage.snp.makeConstraints { make in
            make.trailing.equalTo(separatorView.snp.trailing)
            make.bottom.equalTo(separatorView.snp.top).offset(-23.4)
            make.size.equalTo(UIScreen.isSE ? 30 : 35)
        }

        textField.snp.makeConstraints { make in
            make.centerY.equalTo(coinImage)
            make.trailing.equalTo(coinImage.snp.leading).offset(-10)
            make.leading.equalToSuperview()
        }
    }
}

extension EnterDonationView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        guard textField.text?.count != 0 || string != "0" else { return false }
        return newString.length <= maxLength
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        let point = CGPoint(x: bounds.maxX, y: bounds.height / 2)
        if let textPosition = textField.closestPosition(to: point) {
            textField.selectedTextRange = textField.textRange(from: textPosition, to: textPosition)
        }
    }
}
