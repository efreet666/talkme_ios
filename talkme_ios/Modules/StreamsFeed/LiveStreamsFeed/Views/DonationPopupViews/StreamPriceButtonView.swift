//
//  StreamPriceButtonView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 04.03.2021.
//

import RxSwift

final class StreamPriceButtonView: UIButton, RepeatableViewProtocol {

    // MARK: - Public Properties

    let size: CGSize
    let viewTag: Int
    let bag = DisposeBag()
    var isSelectedd: Bool
    var isStreamEnded = false

    // MARK: - Initializers

    init(size: CGSize, isSelected: Bool, viewTag: Int, viewsData: GenericViewType) {
        self.isSelectedd = isSelected
        self.viewTag = viewTag
        self.size = size
        super.init(frame: CGRect(origin: .zero, size: size))
        switch viewsData {
        case .pay(let title):
            setTitle(title.rawValue, for: .normal)
            setSelected(title.isActive)
        default: break
        }
        setupUI()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    // MARK: - Public Methods

    func setSelected(_ selected: Bool) {
        removeGradientLayer()
                if !isStreamEnded {
                    if !selected {
                        backgroundColor = TalkmeColors.darkViewColor
                        setTitleColor(TalkmeColors.streamSelectedBackground, for: .normal)
                    } else {
                        removeGradientLayer()
                        setTitleColor(TalkmeColors.white, for: .normal)
                        gradientBackground(
                            from: TalkmeColors.gradientLightBlue,
                            to: TalkmeColors.gradientBlue,
                            direction: .bottomToTop
                        )
                    }
                } else {
                    if !selected {
                        backgroundColor = TalkmeColors.darkViewColor
                        setTitleColor(TalkmeColors.streamSelectedBackground, for: .normal)
                    } else {
                        removeGradientLayer()
                        setTitleColor(TalkmeColors.white, for: .normal)
                        gradientBackground(
                            from: TalkmeColors.gradientLightPurple,
                            to: TalkmeColors.gradientPurple,
                            direction: .bottomToTop
                        )
                    }
                }
                clipsToBounds = true
    }

    // MARK: - Private Methods

    private func removeGradientLayer() {
        guard let layers = layer.sublayers else { return }
        for aLayer in layers where aLayer is CAGradientLayer {
            aLayer.removeFromSuperlayer()
        }
    }

    private func setupUI() {
        titleLabel?.font = .montserratFontMedium(ofSize: 15)
        backgroundColor = TalkmeColors.darkViewColor
    }
}
