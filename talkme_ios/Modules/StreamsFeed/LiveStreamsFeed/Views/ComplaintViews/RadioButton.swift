//
//  RadioButton.swift
//  talkme_ios
//
//  Created by nikita on 26.04.2022.
//

import UIKit

final class RadioButton: UIButton {

    override var isSelected: Bool {
        didSet {
            toggleButon()
        }
    }

    var radioImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "radio"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    var filledRadioImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "radioFiller"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        isSelected = false
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        radioImageView.frame =          CGRect(x: 0, y: 0, width: frame.height / 2, height: frame.height / 2)
        filledRadioImageView.frame =    CGRect(x: 0, y: 0, width: frame.height / 3, height: frame.height / 3)
        radioImageView.center = CGPoint(x: frame.height / 4, y: frame.height * 0.5)
        filledRadioImageView.center = radioImageView.center
        titleLabel?.frame = CGRect(x: radioImageView.frame.maxX + 7, y: 0, width: frame.width - radioImageView.frame.maxX, height: frame.height)
        titleLabel?.font = UIFont.montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
    }

    private func setup() {
        addSubviews(
            radioImageView,
            filledRadioImageView)
    }

    private func toggleButon() {
        filledRadioImageView.isHidden = !isSelected
    }

}
