//
//  StreamHintView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 08.03.2021.
//

import RxSwift

final class StreamHintView: UIView {

    private enum Constants {
        static let circleWidth: CGFloat = UIScreen.isSE ? 51 : 63
        static let circleSize: CGSize = .init(width: circleWidth, height: circleWidth)

        static let arrowWidth: CGFloat = UIScreen.isSE ? 28 : 45
        static let arrowSize = CGSize(width: arrowWidth, height: arrowWidth)
        static let bottomArrowOffset: CGFloat = UIScreen.isSE ? 2 : 4

        static let bottomLabelSize = UIScreen.isSE ? CGSize(width: 142, height: 48) : CGSize(width: 224, height: 64)
        static let fontSize: CGFloat = UIScreen.isSE ? 12 : 15

        static let membersWithMicroLeft: CGFloat = UIScreen.isSE ? 22 : 56

        static let giftArrowRight: CGFloat = UIScreen.isSE ? 7 : 2
        static let giftLabelRight: CGFloat = UIScreen.isSE ? 10 : 15
        static let giftLabelTop: CGFloat = UIScreen.isSE ? 5 : 8

        static let flipCameraLabelRight: CGFloat = UIScreen.isSE ? 6 : 9

        static let swipeArrowWidth: CGFloat = UIScreen.isSE ? 41 : 65
        static let swipeArrowSize = CGSize(width: swipeArrowWidth, height: swipeArrowWidth)

        static let swipeRightArrowY: CGFloat = UIScreen.isSE ? 160 : 245
        static let swipeRightArrowX: CGFloat = UIScreen.width - (UIScreen.isSE ? 67 : 78) - swipeArrowWidth
        static let swipeRightLabelRight: CGFloat = UIScreen.isSE ? 12 : 20
        static let swipeRightLabelLeft: CGFloat = UIScreen.isSE ? 56 : 28

        static let swipeRightLabelTop: CGFloat = UIScreen.height * 0.07
    }

    // MARK: - Public Properties

    let bag = DisposeBag()

    // MARK: - Private Properties

    // MARK: - Viewers

    private lazy var viewersCircleView = makeRoundView()
    private lazy var viewersArrow = makeArrow()

    private lazy var viewersLabel: UILabel = {
        let label = makeRightAlignedLabel()
        label.text = "stream_feed_viewers_hint".localized
        return label
    }()

    // MARK: - Members With Micro

    private lazy var membersWithMicroCircleView = makeRoundView()
    private lazy var membersWithMicroArrow = makeArrow()
    private lazy var membersWithMicroLabel = makeRightAlignedLabel()

    // MARK: - Top circle

    private lazy var topCircleView = makeRoundView()
    private lazy var topCircleArrow = makeArrow()
    private lazy var topCircleLabel = makeRightAlignedLabel()

    // MARK: - Exit

    private lazy var exitCircleView = makeRoundView()
    private lazy var exitCircleArrow = makeArrow()
    private lazy var exitCircleLabel = makeCenterAlignedLabel()

    // MARK: - Swipe right

    private lazy var swipeRightArrow = makeStraightArrow()
    private lazy var swipeRightLabel = makeRightAlignedLabel()

    // MARK: - Swipe up

    private lazy var swipeUpArrow = makeStraightArrow()
    private lazy var swipeUpLabel = makeCenterAlignedLabel()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupSwipeUpHint() {
        addSubviews(swipeUpArrow, swipeUpLabel)

        swipeUpLabel.text = "stream_feed_swipe_up_hint".localized
        swipeUpLabel.frame.size.width = Constants.bottomLabelSize.width
        swipeUpLabel.sizeToFit()
        swipeUpLabel.frame.origin.y = swipeRightLabel.frame.minY - Constants.swipeRightLabelTop - swipeUpLabel.frame.size.height
        swipeUpLabel.frame.origin.x = UIScreen.width - swipeUpLabel.frame.size.width - 24

        swipeUpArrow.frame.origin.y = swipeUpLabel.frame.minY - 14 - Constants.swipeArrowWidth
        swipeUpArrow.frame.size = Constants.swipeArrowSize
        swipeUpArrow.center.x = swipeUpLabel.center.x
    }

    func setupSwipeRightHint() {
        addSubviews(swipeRightArrow, swipeRightLabel)

        swipeRightArrow.frame.size = Constants.swipeArrowSize
        swipeRightArrow.frame.origin.x = Constants.swipeRightArrowX

        swipeRightLabel.text = "stream_feed_swipe_right_hint".localized
        swipeRightLabel.frame.origin.y =  UIScreen.height * 0.32
        swipeRightLabel.frame.origin.x = Constants.swipeRightLabelLeft
        swipeRightLabel.frame.size.width = swipeRightArrow.frame.origin.x - Constants.swipeRightLabelLeft - Constants.swipeRightLabelRight
        swipeRightLabel.sizeToFit()

        swipeRightArrow.center.y = swipeRightLabel.center.y
        swipeRightArrow.transform = .init(rotationAngle: .pi / 180 * 90)
    }

    func setupExitButtonHint(origin: CGPoint) {
        addSubviews(exitCircleView, exitCircleArrow, exitCircleLabel)
        exitCircleView.frame = .init(origin: origin, size: Constants.circleSize)

        exitCircleArrow.image = UIImage(named: "streamExitArrow")
        exitCircleArrow.frame.origin.x = exitCircleView.frame.maxX
        exitCircleArrow.center.y = exitCircleView.frame.maxY
        exitCircleArrow.frame.size = Constants.arrowSize

        exitCircleLabel.text = "stream_feed_exit_hint".localized
        exitCircleLabel.sizeToFit()
        exitCircleLabel.center.x = exitCircleArrow.center.x + 10
        exitCircleLabel.frame.origin.y = exitCircleArrow.frame.maxY + 8
    }

    func setupFlipCameraHint(origin: CGPoint) {
        addSubviews(topCircleView, topCircleArrow, topCircleLabel)
        topCircleView.frame = .init(origin: origin, size: Constants.circleSize)

        topCircleArrow.frame.origin.y = topCircleView.frame.minY - Constants.arrowWidth
        topCircleArrow.center.x = topCircleView.frame.minX - 30
        let width = UIScreen.isSE ? 30 : 45
        let height = UIScreen.isSE ? 35 : 45
        topCircleArrow.frame.size = CGSize(width: width, height: height)
        let transform = UIScreen.isSE ? 40 : 50
        topCircleArrow.transform = .init(rotationAngle: .pi / 180 * CGFloat(transform))

        topCircleLabel.text = "stream_feed_flip_camera_hint".localized
        topCircleLabel.sizeToFit()
        topCircleLabel.center.y = topCircleArrow.center.y - 10
        topCircleLabel.frame.origin.x = topCircleArrow.frame.minX - Constants.flipCameraLabelRight - topCircleLabel.frame.width
    }

    func setupGiftHint(origin: CGPoint) {
        addSubviews(topCircleView, topCircleArrow, topCircleLabel)

        topCircleView.frame = .init(origin: origin, size: Constants.circleSize)

        topCircleArrow.center.y = topCircleView.frame.minY - 30
        topCircleArrow.frame.origin.x = topCircleView.frame.minX - Constants.giftArrowRight - Constants.arrowSize.width
        topCircleArrow.frame.size = Constants.arrowSize
        topCircleArrow.transform = .init(rotationAngle: .pi / 180 * 25)

        topCircleLabel.frame.size = Constants.bottomLabelSize
        topCircleLabel.frame.origin.y = topCircleArrow.frame.minY + Constants.giftLabelTop
        topCircleLabel.text = "stream_feed_gift_hint".localized
        topCircleLabel.sizeToFit()
        topCircleLabel.frame.origin.x = topCircleArrow.frame.minX - 6 - topCircleLabel.frame.width
    }

    func setupMembersFor(type: UserType, origin: CGPoint) {
        addSubviews(membersWithMicroCircleView, membersWithMicroArrow, membersWithMicroLabel)
        switch type {
        case .specialist:
            break
            //setupMembersHintForSpecialist(origin: origin) // TODO: Return after adding button of broadcast participants
        case .participant, .viewer:
            break
            //setupMembersHintForLearner(origin: origin) // TODO: Return after adding button of broadcast participants
        }
    }

    func setupViewersHint(originWithoutSize: CGPoint) {
        addSubviews(viewersCircleView, arrowForViewers, viewersLabel)

        let viewersOrigin = CGPoint(
            x: originWithoutSize.x - Constants.circleSize.width,
            y: originWithoutSize.y)
        viewersCircleView.frame = .init(origin: viewersOrigin, size: Constants.circleSize)

        arrowForViewers.frame.origin.y = viewersCircleView.frame.minY - Constants.arrowSize.height / 2 - 14
        arrowForViewers.frame.origin.x = viewersCircleView.frame.minX - Constants.giftArrowRight - Constants.arrowSize.width
        arrowForViewers.frame.size = Constants.arrowSize
        arrowForViewers.transform = .init(rotationAngle: .pi / 180 * 44)

        let bottomLabelSize = Constants.bottomLabelSize
        let bottomLabelOrigin = CGPoint(
            x: arrowForViewers.frame.minX - 9 - bottomLabelSize.width,
            y: arrowForViewers.frame.minY - bottomLabelSize.height / 2 + 20)
        viewersLabel.frame = .init(origin: bottomLabelOrigin, size: bottomLabelSize)
    }

    // MARK: - Private Methods

    private func makeArrow() -> UIImageView {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = UIImage(named: "hintArcArrow")
        return iv
    }

    private let arrowForViewers: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = UIImage(named: "hintArcArrow")
        return img
    }()

    private func makeStraightArrow() -> UIImageView {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = UIImage(named: "straightArrow")
        return iv
    }

    private func makeRightAlignedLabel() -> UILabel {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: Constants.fontSize)
        label.textColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .right
        return label
    }

    private func makeCenterAlignedLabel() -> UILabel {
        let label = makeRightAlignedLabel()
        label.textAlignment = .center
        return label
    }

    private func makeRoundView() -> RoundedView {
        let view = RoundedView()
        view.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        return view
    }

    private func setupMembersHintForSpecialist(origin: CGPoint) {
        membersWithMicroCircleView.frame = .init(origin: origin, size: Constants.circleSize)

        membersWithMicroArrow.frame.origin.y = membersWithMicroCircleView.center.y - Constants.circleWidth / 3
        membersWithMicroArrow.frame.origin.x = membersWithMicroCircleView.frame.minX - Constants.arrowSize.width - 14
        membersWithMicroArrow.frame.size = Constants.arrowSize

        let viewersLabelWidth = membersWithMicroArrow.frame.minX - 9 - Constants.membersWithMicroLeft
        membersWithMicroLabel.frame.size.width = viewersLabelWidth
        membersWithMicroLabel.text = "stream_feed_members_with_micro_hint".localized
        membersWithMicroLabel.sizeToFit()

        membersWithMicroLabel.frame.origin.x = Constants.membersWithMicroLeft
        membersWithMicroLabel.center.y = membersWithMicroArrow.center.y

        membersWithMicroArrow.transform = CGAffineTransform(rotationAngle: .pi / 180)
    }

    private func setupMembersHintForLearner(origin: CGPoint) {
        membersWithMicroCircleView.frame = .init(origin: origin, size: Constants.circleSize)

        membersWithMicroArrow.frame.origin.y = membersWithMicroCircleView.center.y - Constants.circleWidth / 3
        membersWithMicroArrow.frame.origin.x = membersWithMicroCircleView.frame.minX - Constants.arrowSize.width - 14
        membersWithMicroArrow.frame.size = Constants.arrowSize

        let viewersLabelWidth = membersWithMicroArrow.frame.minX - 9 - Constants.membersWithMicroLeft
        membersWithMicroLabel.frame.size.width = viewersLabelWidth
        membersWithMicroLabel.text = "stream_feed_members_with_video_hint".localized
        membersWithMicroLabel.sizeToFit()

        membersWithMicroLabel.frame.origin.x = Constants.membersWithMicroLeft
        membersWithMicroLabel.center.y = membersWithMicroArrow.center.y + 16

        membersWithMicroArrow.transform = CGAffineTransform(rotationAngle: .pi / 180)
    }

}
