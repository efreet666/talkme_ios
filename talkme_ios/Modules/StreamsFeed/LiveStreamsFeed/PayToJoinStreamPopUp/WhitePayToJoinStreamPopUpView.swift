//
//  WhitePayToJoinStreamPopUpView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 13.06.2021.
//

import RxSwift
import RxCocoa

final class WhitePayToJoinStreamPopUpView: UIView {

    // MARK: Public Properties

    lazy var toBuyButtonTapped = confirmButton.rx.tap
    lazy var closePopUpTap = crossButton.rx.tap

    // MARK: - Private Properties

    private let payForJoinStream: UILabel = {
        let lbl = UILabel()
        lbl.text = "white_popup_pay_from_balance".localized
        lbl.textColor = TalkmeColors.grayLabel
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 18 : 20)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()

    private let stackViewBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = UIScreen.isSE ? 19 : 24
        view.layer.masksToBounds = true
        view.layer.borderWidth = 1
        view.layer.borderColor = TalkmeColors.graySearchBar.cgColor
        return view
    }()

    private let sumOfStream: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blueCollectionCell
        lbl.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 23 : 25)
        lbl.numberOfLines = 0
        return lbl
    }()

    private let smallCoinImageView: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "minicoin")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let popUpInfoView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.popUoPayToJoin
        view.layer.cornerRadius = UIScreen.isSE ? 12 : 19
        return view
    }()

    private let crossImageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "whitePopUpPayCross")
        return view
    }()

    private let confirmButton: UIButton = {
        let bt = UIButton()
        bt.backgroundColor = TalkmeColors.blueCollectionCell
        bt.setTitle("talkme_сonfirm_сode_confirm".localized, for: .normal)
        bt.setTitleColor(TalkmeColors.white, for: .normal)
        bt.titleLabel?.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 17)
        bt.titleLabel?.textColor = TalkmeColors.shadow
        bt.layer.cornerRadius = UIScreen.isSE ? 21 : 28
        return bt
    }()

    private let emptyCoinView = UIView()

    private let crossButton: UIButton = {
        let bt = UIButton()
        bt.isUserInteractionEnabled = true
        return bt
    }()

    // MARK: - Initializers

    init(frame: CGRect, cost: Int) {
        sumOfStream.text = String(cost)
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews(crossButton, crossImageView, popUpInfoView)
        popUpInfoView.addSubviews([payForJoinStream, stackViewBackgroundView, confirmButton])
        stackViewBackgroundView.addSubview(emptyCoinView)
        emptyCoinView.addSubviews([sumOfStream, smallCoinImageView])

        crossImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 14 : 17)
            make.bottom.equalTo(popUpInfoView.snp.top).inset(-10)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 5 : 25)
            make.top.equalToSuperview().inset(15)
        }

        crossButton.snp.makeConstraints { make in
            make.size.equalTo(44)
            make.centerX.centerY.equalTo(crossImageView)
        }

        popUpInfoView.snp.makeConstraints { make in
            make.top.equalTo(crossImageView.snp.bottom).inset(10)
            make.centerX.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 28)
        }

        payForJoinStream.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(32)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(stackViewBackgroundView.snp.top).inset(-15)
        }

        stackViewBackgroundView.snp.makeConstraints { make in
            make.top.equalTo(payForJoinStream.snp.bottom).inset(UIScreen.isSE ? 15 : 20)
            make.height.equalTo(UIScreen.isSE ? 38 : 47)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 68 : 84)
            make.bottom.equalTo(confirmButton.snp.top).inset(-17)
        }

        emptyCoinView.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }

        sumOfStream.snp.makeConstraints { make in
            make.top.bottom.leading.equalToSuperview().inset(4)
            make.trailing.equalTo(smallCoinImageView.snp.leading).inset(-6)
        }

        smallCoinImageView.snp.makeConstraints { make in
            make.top.bottom.trailing.equalToSuperview().inset(6)
            make.leading.equalTo(sumOfStream.snp.trailing).inset(6)
            make.width.height.equalTo(UIScreen.isSE ? 19 : 28)
        }

        confirmButton.snp.makeConstraints { make in
            make.top.equalTo(stackViewBackgroundView.snp.bottom).inset(UIScreen.isSE ? 17 : 23)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 55 : 68)
            make.bottom.equalToSuperview().inset(30)
            make.height.equalTo(UIScreen.isSE ? 42 : 53)
        }
    }
}
