//
//  PayToJoinStreamPopUpView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.04.2021.
//

import RxSwift
import RxCocoa

final class PayToJoinStreamPopUpView: UIView {

    // MARK: Public Properties

    lazy var toBuyButtonTapped = greenView.rx.tapGesture().when(.recognized)

    // MARK: - Private Properties

    private let backgroundImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "mainBackground")
        img.clipsToBounds = true
        img.layer.cornerRadius = 20
        img.isUserInteractionEnabled = true
        return img
    }()

    private let payForJoinStream: UILabel = {
        let lbl = UILabel()
        lbl.text = "popup_stream_pay".localized
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratBold(ofSize: 20)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()

    private let bigGoldCoinImageView: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "goldCoin")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let greenView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.greenLabels
        view.clipsToBounds = true
        view.layer.cornerRadius = 12
        return view
    }()

    private let onEnter: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratBold(ofSize: 20)
        lbl.numberOfLines = 0
        lbl.text = "talkme_auth_login".localized
        lbl.isUserInteractionEnabled = false
        return lbl
    }()

    private let sumOfStream: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratBold(ofSize: 20)
        lbl.numberOfLines = 0
        lbl.isUserInteractionEnabled = false
        return lbl
    }()

    private let smallCoinImageView: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "goldCoin")
        img.contentMode = .scaleAspectFill
        img.isUserInteractionEnabled = false
        return img
    }()

    // MARK: - Initializers

    init(frame: CGRect, cost: Int) {
        sumOfStream.text = String(cost)
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        greenView.addSubviews([onEnter, sumOfStream, smallCoinImageView])
        backgroundImage.addSubviews([bigGoldCoinImageView, payForJoinStream, greenView])
        addSubview(backgroundImage)

        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        bigGoldCoinImageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(31)
            make.width.height.equalTo(46)
            make.bottom.equalTo(payForJoinStream.snp.top).inset(-15)
        }

        payForJoinStream.snp.makeConstraints { make in
            make.top.equalTo(bigGoldCoinImageView.snp.bottom).inset(15)
            make.leading.trailing.equalToSuperview().inset(23)
            make.bottom.equalTo(greenView.snp.top).inset(-16)
        }

        greenView.snp.makeConstraints { make in
            make.top.equalTo(payForJoinStream.snp.bottom).inset(16)
            make.leading.trailing.equalToSuperview().inset(30)
            make.bottom.equalToSuperview().inset(30)
        }

        onEnter.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(29)
        }

        sumOfStream.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(smallCoinImageView.snp.leading).inset(-6)
        }

        smallCoinImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.size.equalTo(21)
            make.trailing.equalToSuperview().inset(16)
            make.leading.equalTo(sumOfStream.snp.trailing).inset(6)
        }
    }
}
