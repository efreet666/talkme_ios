//
//  PayToJoinStreamPopUpViewController.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.04.2021.
//

import RxCocoa
import RxSwift

final class PayToJoinStreamPopUpViewController: UIViewController {

    enum Flow {
        case dismiss
        case buyAndDismiss
    }

    // MARK: - Public Properties

    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private lazy var payToJoinStream = PayToJoinStreamPopUpView(frame: .zero, cost: cost)
    private let cost: Int

    // MARK: - Initializers

    init(cost: Int) {
        self.cost = cost
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        bind()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        view.addSubview(payToJoinStream)
        payToJoinStream.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(319)
            make.height.equalTo(213)
        }
    }

    private func bind() {
        view
            .rx
            .tapGesture()
            .when(.recognized)
            .bind { [weak self] recognizer in
                guard let self = self else { return }
                let point = recognizer.location(in: self.payToJoinStream)
                guard self.view.frame.contains(point) else {
                    self.flow.accept(.dismiss)
                    return
                }
            }
            .disposed(by: bag)

        payToJoinStream
            .toBuyButtonTapped
            .bind { [weak self] _ in
                self?.flow.accept(.buyAndDismiss)
            }
            .disposed(by: bag)
    }
}
