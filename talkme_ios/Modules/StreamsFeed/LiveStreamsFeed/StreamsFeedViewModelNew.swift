//
//  StreamsFeedViewModelNew.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//


import RxAppState
import RxCocoa
import RxSwift
import CollectionKit
import UIKit
import Reachability
import CallKit
import HaishinKit
import AVFoundation
import Logboard
import VideoToolbox
import Moya

fileprivate extension Consts {
    static let countOfStreamsForOnePage = 50
}

final class StreamsFeedViewModelNew: NSObject, InputDataForStreamPopUpViewModelProtocol, StreamsFeedViewModelFlowProtocol {

    enum RTMPConnectionState {
        case active
        case closed
        case loading
    }

    enum StreamsUpdateСause {
        case newPageLoaded
        case periodiсUpdate
        case newStreamSelected
    }

    typealias LessonId = Int

    enum Flow {
        case onBack(animated: Bool = true)
        case onPopUp(
            page: Int,
            classNumber: String,
            lessonID: Int,
            isOwner: Bool,
            socketCamerasService: SocketCamerasService?
        )
        case finishStream
        case rateAndDonate(lessonId: LessonId)
        case reloadStreamsOnFinish(lessonId: Int)
        case onPopupWithOnlyPurchases(
            page: Int,
            lessonID: Int,
            socketCamerasService: SocketCamerasService?
        )
        case onPopupWithSendComplaint(userAboutID: Int)
        case onPopupThank(userAboutID: Int)
        case onPopupGift(userAboutID: Int)
        case onPopupProfileInfo(userAboutID: Int)
        case showCoineWriteOffPopup(lessonId: Int, cost: Int)
        case dismissPopUp
        case networkErrorPopUp(errorMessage: String?, action: () -> Void)
    }

    enum CustomMessages: String {
        case teacherOnLesson = "TeacherOnLesson"
        case teacherCameraIsActive = "TeacherCameraIsActive"
        case lessonFinished = "LessonFinished"
        case undefined
    }

    // MARK: - Public Properties

    // common
    let onGetLessons = BehaviorRelay<Bool>(value: false)
    let onPresentLessonsIsEmptyAlert = BehaviorRelay<Bool>(value: false)
    var lessonDetails: [LessonId: LessonDetailResponse] = [:]
    var userType: UserType?
    let sendMessage = PublishSubject<String>()
    let sendGift = PublishSubject<Int>()
    let flow = PublishRelay<Flow>()
    let finishStream = PublishRelay<Void>()
    let streamViewers = PublishRelay<Int>()
    let streamLikes = BehaviorRelay<Int>(value: 0)
    let bag = DisposeBag()
    var isOwner: Bool = false
    let onChangeTeacherCameraState = PublishSubject<Bool>()
    let onRemoveLessonView = PublishSubject<Void>()
    var lessonIds: [LessonId] = []
    private var lessonIdsBeforeUpdate: [LessonId] = []
    var currentLessonId: Int?
    var currentTeacherId: Int?

    var currentIndex: Int {
        get {
            lessonIds.firstIndex(of: currentLessonId ?? -1) ?? -1
        }
        set(selectedLessonsIndex) {
            guard selectedLessonsIndex < lessonIds.count, selectedLessonsIndex >= 0 else {
                assertionFailure("currentLessonId not found in lessons array")
                return
            }
            currentLessonId = lessonIds[selectedLessonsIndex]
        }
    }

    var currentClassNumber: String? {
        return lessonDetails[currentLessonId ?? 0]?.owner.numberClass
    }

    var currentLessonDetails: LessonDetailResponse? {
        return lessonDetails[currentLessonId ?? 0]
    }

    var cameraIsActiveState = true
    var microOrPlayerIsActiveState = true
    var cameraIsFrontState = true
    let onShowLoader = PublishSubject<Bool>()
    let onStartPlaying = PublishSubject<Void>()

    var currentChatDataSource: ArrayDataSource<LessonMessage>? { return chatsDataSourceDict[currentLessonId ?? 0] }
    var chatsDataSourceDict = [Int: ArrayDataSource<LessonMessage>]()
    private var arraysOfMessages: [Int:[LessonMessage]] = [:]
    var appOnBackground = false

    // teacher
    let onTeacherStreamViewReady = PublishSubject<GLHKView>()

    // viewer
    var cellModels: [StreamFeedViewModel] = []
    let onReadyToPresentVipLessonPopup = BehaviorRelay<Int>(value: 0)
    let onDataSourceUpdate = BehaviorRelay<([StreamFeedViewModel],StreamsUpdateСause)>(value: ([], .newPageLoaded))
    var isSubscribe: Bool = false
    var cost = 0
    let onPresentVipLessonPopup = PublishRelay<VipLessonPopupView>()
    let onPlayerReadyForDisplay = BehaviorRelay<AVPlayer?>(value: nil)
    let onSetCameraStare = PublishSubject<Bool>()
    let onSetMicroStare = PublishSubject<Bool>()
    let onLessonFinished = BehaviorRelay<Bool>(value: false)
    let dismissVipLessonPopup = PublishSubject<Int>()
    let onUpdatePlayerSize = PublishSubject<CGSize>()
    var canUpdateCollection = true
    var isActivePictureInPicture = false
    private(set) var numberOfStreamsInLastPage: Int
    var totalStreamsCount: Int {
        recievedStreams.count
    }
    private(set) var numberOfLastPage: Int = 1
    private(set) var countOfStreamsForLoad: Int = Consts.countOfStreamsForOnePage
//    var numberOfLastPage: Int {
//        Int(ceil(Float(recievedStreams.count) / Float(numberOfStreamsInPage)))
//    }

    // MARK: - Private Properties

    // common
    private var lessonsIdsForUpdate: [LessonId] = []
    private var cellModelsForUpdate: [StreamFeedViewModel] = []
    private var sendCameraSignalBag = DisposeBag()
    private var timer: Timer?
    private var lessonService: LessonsServiceProtocol
    private var authService = AuthService()
    private var gcoreService: GcoreServiceProtocol
    private var camerasSocketManager: SocketClientManager?
    private var socketCamerasService: SocketCamerasService?
    private(set) var chatService: SocketLessonChatSrevice?
    private var accountService = AccountService()
    private var chatBag = DisposeBag()
    private var messagesCount = 0
    private var vipLessonPopupView: VipLessonPopupView?
    private var wasDisconnected = false
    private let reachabilityManager = ReachabilityManager()
    private var isNetworkActive = true
    private var loaderIsActive = false
    private(set) var infoStream: PublicProfileResponse?
    private(set) var isContactRelay = PublishRelay<Bool>()
    // teacher
    private var currentConnactionState: RTMPConnectionState = .closed
    private lazy var callObserver = CXCallObserver()
    private var callIsActive = false
    private var audioSession: AVAudioSession?
    private var rtmpConnection: RTMPConnection?
    private var rtmpStream: RTMPStream?
    private lazy var cameraIsFront = cameraIsFrontState
    var currentCamera: AVCaptureDevice? { cameraIsFront ? frontCamera  : backCamera }

    private lazy var frontCamera: AVCaptureDevice? = {
        let frontCameraDevice = DeviceUtil.device(withPosition: .front)
        return frontCameraDevice
    }()

    private lazy var backCamera: AVCaptureDevice? = {
        let defaultVideoDevice = DeviceUtil.device(withPosition: .back)
        return defaultVideoDevice
    }()

    // viewer
    private var isNeedPresentVipLessonPopUp = false
    private var streamPlayer: AVPlayer?
    private var streamPlayerItem: AVPlayerItem?
    private var streamUrlString: String?
    private var playerBufferingObservationTimeRanges: NSKeyValueObservation?
    private var playerBufferingObservationisFull: NSKeyValueObservation?
    private var playerBufferingObservationIsEmpty: NSKeyValueObservation?
    private var playerItemKeepUpObservation: NSKeyValueObservation?
    private var playerItemStatusObservation: NSKeyValueObservation?
    private var playerTimeControlStatusObservation: NSKeyValueObservation?
    private var playerSizeObservation: NSKeyValueObservation?
    private var isNeedToPresentLoader = false
    private var isShowDonateAlert = false
    private(set) var isWatchingSave = false
    private var streamsCreatorId: Int?
    private var selectedCategoriesIds: [Int] = [0]
    private var streamsList: [Int: LiveStream] = [:]
    private var streamsListForUpdate: [Int: LiveStream] = [:]
    private var myBroadcasts: MyBroadcastsResponse?
    
    var isStreamActive = true

    private(set) var status = ""
    var dataResponse: ((GcoreResponse?, String?) -> Void?)?
    var responceCopy: GcoreResponse?
    var showNotification: ((String, String) -> Void?)?
    private var pleyerIsRefreshing = false
    private var previousTimeTeachersCameraWasActive: Bool = true
    private let recahbilityManager = ReachabilityManager()
    private var recievedStreams: [LiveStream] = []
    private var recievedStreamsForUpdate: [LiveStream] = []
    private var countOfLoadedPages: Int
    private var didntRecieveStreamsInLastRequest = false
    private var maxCountOfNetworkRetries = 3
    private var currentCountOfNetworkRetries = 0

    // MARK: - Initializers

    init(lessonService: LessonsServiceProtocol,
         gcoreService: GcoreServiceProtocol,
         lessonId: Int?,
         streamsList: [LiveStream],
         countOfLoadedPages: Int = 0,
         numberOfStreamsInPage: Int,
         broadcastsList: MyBroadcastsResponse?,
         isWatchingSave: Bool = false,
         streamsCreatorId: Int? = nil,
         streamsCategoriesIds: [Int] = [0]
    ) {
        self.lessonService = lessonService
        self.gcoreService = gcoreService
        self.currentLessonId = lessonId
        self.isWatchingSave = isWatchingSave
        self.streamsCreatorId = streamsCreatorId
        self.selectedCategoriesIds = streamsCategoriesIds
        self.myBroadcasts = broadcastsList
        self.numberOfStreamsInLastPage = streamsList.count
        self.recievedStreams = streamsList
        self.countOfLoadedPages = countOfLoadedPages
        super.init()
        connectOnStart(with: streamsList)
        bindAppState()
        bindInputs()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
        disconnectСhatSocket()
    }

    // MARK: - Public Methods
    
    func setLike(id: Int, completion: ((LessonDetailResponse) -> Void)? = nil) {
        lessonService
            .putLike(id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.streamLikes.accept(response.likes)
                    guard let lessonDetailIndex = AllLiveLessonsModel.liveLessonsArray.firstIndex(where: { $0.id == id }) else { return }
                    var lessonDetail = AllLiveLessonsModel.liveLessonsArray[lessonDetailIndex]
                    lessonDetail.liked = true
                    let set = Set(lessonDetail.usersLiked)
                    let userID = UserDefaultsHelper.shared.userId
                    if !set.contains(userID) {
                        lessonDetail.usersLiked.append(userID)
                    }
                    AllLiveLessonsModel.liveLessonsArray[lessonDetailIndex] = lessonDetail
                    completion?(lessonDetail)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
    
    func deleteLike(id: Int, completion: ((LessonDetailResponse) -> Void)? = nil) {
        lessonService
            .deleteLike(id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.streamLikes.accept(response.likes)
                    guard let lessonDetailIndex = AllLiveLessonsModel.liveLessonsArray.firstIndex(where: { $0.id == id }) else { return }
                    var lessonDetail = AllLiveLessonsModel.liveLessonsArray[lessonDetailIndex]
                    lessonDetail.liked = false
                    let usersLiked = lessonDetail.usersLiked.filter { $0 != UserDefaultsHelper.shared.userId }
                    lessonDetail.usersLiked = usersLiked
                    AllLiveLessonsModel.liveLessonsArray[lessonDetailIndex] = lessonDetail
                    completion?(lessonDetail)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
    
    func addContact(id: Int) {
        accountService
            .addContact(id: id)
            .subscribe { event in
                switch event {
                case .success(let response):
                    print(response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func removeContact(id: Int) {
        accountService
            .removeContact(id: id)
            .subscribe { event in
                switch event {
                case .success(let response):
                    print(response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }


    func loadNextPage() {
        if numberOfStreamsInLastPage != 0 {
            lessonService.live(pageNumber: numberOfLastPage + 1, lessonsInPage: countOfStreamsForLoad)
                .subscribe { [weak self] event in
                    guard let self = self
                    else { return }

                    switch event {
                    case .success(let response):
                        let streamsFromNewPage = response.data ?? []

                        self.numberOfLastPage += 1
                        self.numberOfStreamsInLastPage = streamsFromNewPage.count

                        self.recievedStreams += streamsFromNewPage

                        self.setStreamsList(from: self.recievedStreams)
                        self.setLessonIds(from: self.recievedStreams, and: self.myBroadcasts)

                        self.cellModels = self.lessonIds.map { self.streamFeedModel(lessonId: $0) }

                        let filteredCellModels = self.cellModels.filter {
                            $0.streamModel.specialistInfo != nil &&
                            $0.streamModel.specialistInfo?.id != UserDefaultsHelper.shared.userId
                        }

                        self.onDataSourceUpdate.accept((filteredCellModels, .newPageLoaded))

                        self.didntRecieveStreamsInLastRequest = streamsFromNewPage.isEmpty
                    case .error:
                        break
                    }
                }
                .disposed(by: bag)
        }
    }

    func repeatedLoginRequest(emailOrMobile: String, password: String) {
        let request = LoginRequest(emailOrMobile: emailOrMobile, password: password, rememberMe: true)
        authService
            .login(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.authToken != nil else { return }
                    self?.getStream(lessonId: self?.currentLessonId ?? 0)
                case .error(let error):
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                    appDelegate.makeLogout()
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func getIndexOfCellAfterUpdate(forCellWithIndexBeforeUpdate index: Int) -> Int {
        let lesssonsId = lessonIdsBeforeUpdate[index]
        let newCount = lessonsIdsForUpdate.firstIndex(of: lesssonsId) ?? 0
        let result = newCount
        return result
    }

    private func connectOnStart(with recievedStreams: [LiveStream]) {
        if recievedStreams.count == 0 {
            Single.zip(lessonService.live(pageNumber: 1, lessonsInPage: (countOfLoadedPages + 1) * Consts.countOfStreamsForOnePage),
                       lessonService.myBroadcasts(page: 1))
                .subscribe { [weak self] event in
                    switch event {
                    case .success((let liveResponse, let myMroadcasts)):
                        let liveStreams = liveResponse.data ?? []
                        self?.myBroadcasts = myMroadcasts
                        self?.prepareForWatchStreams(listedIn: liveStreams)
                    case .error:
                        self?.prepareForWatchStreams(listedIn: [])
                    }
                }
                .disposed(by: bag)
        } else {
            prepareForWatchStreams(listedIn: recievedStreams)
        }
    }

    private func prepareForWatchStreams(listedIn streamsList: [LiveStream]) {
        self.recievedStreams = streamsList

        setStreamsList(from: recievedStreams)
        setLessonIds(from: recievedStreams, and: myBroadcasts)

        if lessonIds.isEmpty {
            if currentLessonId == nil {
                self.onPresentLessonsIsEmptyAlert.accept(true)
                return
            }
        } else {
            if currentLessonId == nil {
                currentLessonId = lessonIds.first
            }
        }

        if userType == .viewer {
            if let teacherId = recievedStreams[currentIndex].owner.id {
                currentTeacherId = teacherId
            }
        }
        self.lessonService
            .lessonDetail(id: currentLessonId ?? 0, saved: false)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let detail):
                    self.isOwner = detail.isOwner
                    self.setUserType()
                    self.lessonDetails[self.currentLessonId ?? 0] = detail
                    self.onGetLessons.accept(true)
                    switch self.userType {
                    case .specialist:
                        self.observeNetwork()
                        self.callObserver.setDelegate(self, queue: nil)
                        self.bindAlertPositiveAction()
                        self.sendCameraStateMessages()
                        self.connectWithIndex(self.currentIndex)
                    case .participant:
                        return
                    case .viewer:
                        self.lessonDetails[detail.id] = detail
                        self.cellModels = self.lessonIds.map { self.streamFeedModel(lessonId: $0) }
                        self.connectWithIndex(self.currentIndex)
                        self.updateCollectinEveryMinute()
                        self.bindViewerStreamComplete()
                    case .none:
                        return
                    }
                case .error(let error):
                    self.restartNetwork(function: { [weak self] in self?.prepareForWatchStreams(listedIn: streamsList) },
                                        ifFailedShowAlertWithError: error, andOurErrorCode: "81")
                    print(error)
                }
            }.disposed(by: self.bag)
    }

    func finishTraking() {
        AmplitudeProvider.shared.currentChatModel = currentChatDataSource ?? ArrayDataSource<LessonMessage>()
        AmplitudeProvider.shared.finishStreamTraking()
    }
    
    func fetchData(classNumber: String) {
        accountService.streamPublicProfile(classNumber: classNumber)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let info):
                    self.infoStream = info
                    self.isContactRelay.accept(info.isContact)
                case .error(let error):
                    print(error.localizedDescription)
                }
            }
            .disposed(by: bag)
    }

    func connectWithIndex(_ index: Int) {
        self.onLessonFinished.accept(false)
        finishTraking()
        isNeedPresentVipLessonPopUp = false
        currentIndex = index
        connectWithLessonId(currentLessonId ?? 0)
        AmplitudeProvider.shared.startStreamTracking(lesson: currentLessonDetails)
        switch userType {
        case .specialist:
            return
        case .participant:
            return
        case .viewer:
            fetchData(classNumber: currentClassNumber ?? "")
            cameraIsActiveState = true
            microOrPlayerIsActiveState = true
            if let teacherId = currentLessonDetails?.owner.id {
                currentTeacherId = teacherId
            }
        case .none:
            return
        }
    }

    func handleOnComplaintButtonTap() {
        flow.accept(.onPopupWithSendComplaint(userAboutID: currentTeacherId ?? 0))
    }
    
    func handleProfleInfoButtonTap() {
        flow.accept(.onPopupProfileInfo(userAboutID: currentLessonId ?? 0))
    }
    
    func handleThankButtonTap() {
        flow.accept(.onPopupThank(userAboutID: currentLessonId ?? 0))
    }
    
    func handleGiftButtonTap() {
        flow.accept(.onPopupGift(userAboutID: currentLessonId ?? 0))
    }

    func handleOnGiftTap(_ onlyPurchases: Bool) {
        if onlyPurchases {
            flow.accept(.onPopupWithOnlyPurchases(page: 1, lessonID: currentLessonId ?? 0, socketCamerasService: socketCamerasService))
        } else {
            guard let classNumber = self.currentClassNumber else { return }
            flow.accept(.onPopUp(
                page: 1,
                classNumber: classNumber,
                lessonID: currentLessonId ?? 0,
                isOwner: isOwner,
                socketCamerasService: socketCamerasService
            ))
        }
    }

    func handleLessonFinished() {
        onRemoveLessonView.onNext(())
        if isOwner {
            gcoreService
                .finishLesson(lessonId: currentLessonId ?? 0)
                .subscribe { [weak self] event in
                    switch event {
                    case .success:
                        self?.newCustomMessage(messagetype: .lessonFinished, messageValue: true)
                    case .error(let error):
                        print("finish lesson error \(error.localizedDescription)")
                    }
                    self?.closeConnection()
                    self?.flow.accept(.reloadStreamsOnFinish(lessonId: self?.currentLessonId ?? 0))
                }
                .disposed(by: bag)
        } else {
            streamPlayer?.isMuted = true
            flow.accept(.rateAndDonate(lessonId: currentLessonId ?? 0))
        }
    }
    
//    func handleLessonFinished() {
//        onRemoveLessonView.onNext(())
//        isStreamActive = false
//        guard isOwner else {
//            flow.accept(.rateAndDonate(lessonId: currentLessonId ?? 0))
//            return
//        }
//        streamPlayer?.isMuted = true
////        flow.accept(.onBack)
//        flow.accept(.reloadStreamsOnFinish(lessonId: currentLessonId ?? 0))
//    }

    func handleMembersTap() {
        guard let classNumber = self.currentClassNumber else { return }
        flow.accept(.onPopUp(
            page: 3,
            classNumber: classNumber,
            lessonID: currentLessonId ?? 0,
            isOwner: isOwner,
            socketCamerasService: socketCamerasService
        ))
    }

    func handleOnBackTap() {
        if isOwner {
            flow.accept(.finishStream)
        } else {
            flow.accept(.onBack())
        }
    }

    func handleMicrophoneTap() {
        switch userType {
        case .specialist:
            rtmpStream?.audioSettings = [.muted: !self.microOrPlayerIsActiveState]
        case .participant:
            return
        case .viewer:
            streamPlayer?.isMuted = !self.microOrPlayerIsActiveState
        case .none:
            return
        }
    }
    
    func sendNewGift(unauthUser: String, gift: Int) {
        newGift(unauthUser: unauthUser, gift: gift)
    }

    // MARK: - Private Methods

    private func bindInputs() {
        dismissVipLessonPopup
            .bind { [weak self] lessonId in
                self?.isSubscribe = true
                self?.isNeedPresentVipLessonPopUp = false
                self?.getStream(lessonId: self?.currentLessonId ?? 0)
            }
            .disposed(by: bag)

        onChangeTeacherCameraState
            .distinctUntilChanged()
            .bind { [weak self] teacheCameraIsActive in
                guard let self = self
                else { return }
                if teacheCameraIsActive, !self.previousTimeTeachersCameraWasActive {
                    self.reconnectToChat(roomId: self.currentLessonId ?? 0)
                }
            }
            .disposed(by: bag)

        onShowLoader
            .debounce(.seconds(1), scheduler: MainScheduler.instance)
            .do { [weak self] loaderIsActive in
                guard let self = self
                else { return }
                self.loaderIsActive ? self.showLoader() : self.hideLoader()
            }
            .debounce(.seconds(3), scheduler: MainScheduler.instance)
            .bind { [weak self] loaderIsActive in
                guard let self = self
                else { return }
                if loaderIsActive && !self.pleyerIsRefreshing {
                    self.refreshPlaying(lessonId: self.currentLessonId ?? 0)
                }
            }
            .disposed(by: bag)
    }

    private func setNotificationObservers(on player: AVPlayer) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerItemDidReachEnd(notification:)),
            name: .AVPlayerItemDidPlayToEndTime,
            object: player
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerItemStalled),
            name: .AVPlayerItemPlaybackStalled,
            object: player
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerItemDidReachEnd(notification:)),
            name: .AVPlayerItemFailedToPlayToEndTime,
            object: player
        )
    }

    private func setUserType() {
        if isOwner {
            self.userType = .specialist
        } else {
            guard let timeEnd = UserDefaultsHelper.shared.cameraTimeEnd,
                  Date().timeIntervalSince1970 < timeEnd
            else {
                self.userType = .viewer
                return
            }
            self.userType = .participant
        }
    }

    private func setupSession(session: AVAudioSession) {
        do {
            if userType == .viewer {
                try session.setCategory(.playback, mode: .moviePlayback)
            } else {
                try session.setCategory(.playAndRecord, mode: .default, options: [.defaultToSpeaker, .allowBluetooth])
            }
            try session.setActive(true)
        } catch {
            print(error)
        }
    }

    private func observeNetwork() {
        reachabilityManager.hasNetworkConnection
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self] connected in
                guard let self = self else { return }
                if connected {
                    self.isNetworkActive = true
                    guard self.wasDisconnected else { return }
                    self.wasDisconnected = false
                    self.handleConnectionState()
                } else {
                    self.isNetworkActive = false
                    self.wasDisconnected = true
                    self.closeConnection()
                }
            }).disposed(by: bag)
    }

    private func getStream(lessonId: Int) {
        gcoreService
            .getLessonStream(lessonId: lessonId)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.currentCountOfNetworkRetries = 0
                    switch self.userType {
                    case .specialist:
                        self.responceCopy = response
                        self.streamUrlString = response.pushUrl
                        self.startRTMP(streamUrlString: response.pushUrl ?? "", token: response.token ?? "")
                    case .participant:
                        return
                    case .viewer:
                        print("[chat] getStream CONNECT")
                        self.responceCopy = response
                        self.dataResponse?(response, nil)
                        self.streamUrlString = response.gcoreHlsUrl ?? ""
                        self.configureViewersPlayer(streamUrlString: response.gcoreHlsUrl ?? "", lessonId: lessonId)
                        self.updateCellModels(lessonId: lessonId)
                    case .none:
                        return
                    }
                    self.connectToChat(lessonId: lessonId)
                case .error(let error):
                    self.restartNetwork(function: { [weak self] in self?.getStream(lessonId: lessonId) },
                                        ifFailedShowAlertWithError: error, andOurErrorCode: "82")
                }
            }
            .disposed(by: bag)
    }

    func getLiveLessons() {
        guard !self.wasDisconnected else { return }
        lessonService.live(pageNumber: 1, lessonsInPage: currentIndex + 50)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let live):
                    self.numberOfLastPage = 1

                    let recievedLiveStreams = live.data ?? []

                    self.recievedStreams = recievedLiveStreams
                    self.setStreamsList(from: recievedLiveStreams)
                    self.setLessonIds(from: recievedLiveStreams)

                    self.numberOfStreamsInLastPage = recievedLiveStreams.count

                    guard let lessons = live.data, !lessons.isEmpty else {
                        guard let message = live.msg, !message.isEmpty else { return }
                        self.exitTheLesson(fromBackground: true)
                        return
                    }
                    if lessons.contains(where: { stream in stream.id == self.currentLessonId }) {
                        self.getStream(lessonId: self.currentLessonId ?? 0)
                    } else {
                        self.exitTheLesson(fromBackground: true)
                    }
                case .error:
                    self.exitTheLesson(fromBackground: true)
                }
            }
            .disposed(by: bag)
    }

    private func exitTheLesson(fromBackground: Bool) {
        switch userType {
        case .participant:
            break
        case .specialist:
            flow.accept(.reloadStreamsOnFinish(lessonId: currentLessonId ?? 0))
        case .viewer:
            if fromBackground {
                onRemoveLessonView.onNext(())
                streamPlayer?.isMuted = true
                flow.accept(.rateAndDonate(lessonId: currentLessonId ?? 0))
            } else {
                let finishedLessonId = currentLessonId ?? 0
                DispatchQueue.main.asyncAfter(deadline: .now() + 8) { [weak self] in
                    guard let self = self, self.currentLessonId == finishedLessonId else {
                        self?.updateLiveLessons()
                        return
                    }
                    self.streamPlayer?.isMuted = true
                    self.onRemoveLessonView.onNext(())
                    self.isShowDonateAlert = true
                    self.flow.accept(.rateAndDonate(lessonId: finishedLessonId))
                }
            }
        case .none:
            break
        }
    }

    private func bindAppState() {
        UIApplication.shared.rx
            .applicationWillEnterForeground
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.appOnBackground = false
                if !self.isActivePictureInPicture {
                    self.handleConnectionState()
                }
            }
            .disposed(by: bag)

        UIApplication.shared.rx
            .applicationDidEnterBackground
            .bind { [weak self] _ in
                guard let self = self else { return }
                if self.userType == .specialist {
                }
                self.appOnBackground = true
                self.closeConnection()
            }
            .disposed(by: bag)
    }

    private func showLoader() {
        guard self.loaderIsActive == false else { return }
        self.loaderIsActive = true
    }

    private func hideLoader() {
        loaderIsActive = false
        isNeedToPresentLoader = false
    }

    private func setLessonIds(from liveStreams: [LiveStream], and myBroadcasts: MyBroadcastsResponse? = nil) {
        if  !liveStreams.isEmpty {
            let streams = liveStreams.filter { $0.owner.id != UserDefaultsHelper.shared.userId}
            lessonIds = streams.map { stream in stream.id }
            // uncommit this to enable adding new streams to the end of streams list
//            if lessonIds.isEmpty {
//                lessonIds = streams.map({ stream -> Int in stream.id })
//            } else {
//                let newStreams = streams.filter { $0.owner.id != UserDefaultsHelper.shared.userId}
//                var arrayOfExistedStreams: [Int] = []
//                var arrayOfNewStreams: [Int] = []
//                newStreams.forEach { stream in
//                    if lessonIds.contains(stream.id) {
//                        arrayOfExistedStreams.append(stream.id)
//                    } else {
//                        arrayOfNewStreams.append(stream.id)
//                    }
//                }
//                lessonIds = arrayOfExistedStreams + arrayOfNewStreams
//            }
        }
        if let lessonSoon = myBroadcasts?.lessonsSoon {
            lessonIds += lessonSoon.map({ stream -> Int in stream.id })
        }
        if lessonIds.isEmpty, let lessonId = currentLessonId {
            lessonIds.append(lessonId)
        }
    }

    private func setLessonIdsForUpdate(from liveStreams: [LiveStream]) {
        lessonsIdsForUpdate = []
        if  !liveStreams.isEmpty {

            // uncommit this to enable adding new live streams to the end of streams list

//            let newStreams = liveStreams.filter { $0.owner.id != UserDefaultsHelper.shared.userId}
//            var arrayOfExistedStreams: [Int] = []
//            var arrayOfNewStreams: [Int] = []
//            newStreams.forEach { stream in
//                if lessonIds.contains(stream.id) {
//                    arrayOfExistedStreams.append(stream.id)
//                } else {
//                    arrayOfNewStreams.append(stream.id)
//                }
//            }
//            lessonsIdsForUpdate = arrayOfExistedStreams + arrayOfNewStreams
            let newStreams = liveStreams.filter { $0.owner.id != UserDefaultsHelper.shared.userId}
            newStreams.forEach { stream in
                lessonsIdsForUpdate.append(stream.id)
            }
        }
        if lessonsIdsForUpdate.isEmpty, let lessonId = currentLessonId {
            lessonsIdsForUpdate.append(lessonId)
        }
    }

    private func setStreamsList(from liveStreams: [LiveStream]) {
        for stream in liveStreams {
            self.streamsList[stream.id] = stream
        }
    }

    private func setStreamsListForUpdate(from liveStreams: [LiveStream]) {
        for stream in liveStreams {
            self.streamsListForUpdate[stream.id] = stream
        }
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)",
                                                    action: { self.flow.accept(.onBack()) }))
            }
        }
    }

    // MARK: - CHATS

    func refreshChatsDataSource(lessonId: Int) -> ArrayDataSource<LessonMessage> {
        if let dataSource = chatsDataSourceDict[lessonId] {
            print("[chat] [REFRESH CHAT DATASOURCE \(lessonId) OLD current \(currentLessonId)]")
            #if DEBUG
            self.showNotification?("Chat info:", "[chat] [REFRESH CHAT DATASOURCE \(lessonId) OLD current \(currentLessonId)]")
            #endif
            AmplitudeProvider.shared.currentChatModel = currentChatDataSource ?? ArrayDataSource<LessonMessage>()
            return dataSource
        }
        print("[chat] [REFRESH CHAT DATASOURCE \(lessonId) NEW current \(currentLessonId)]")
        #if DEBUG
        self.showNotification?("Chat info:", "[chat] [REFRESH CHAT DATASOURCE \(lessonId) NEW current \(currentLessonId)]")
        #endif
        let chatsDataSource = ArrayDataSource<LessonMessage>()
        chatsDataSourceDict.removeAll()
        arraysOfMessages.removeAll()
        chatsDataSourceDict[lessonId] = chatsDataSource
        arraysOfMessages[lessonId] = []
        return chatsDataSource
    }

    private func addMessage(model: LessonChatMessage, lessonId: Int) {
        guard let dataSource = chatsDataSourceDict[lessonId] else { return }
        if model.isNew {
            arraysOfMessages[lessonId]?.insert(model.message, at: 0)
        } else {
            arraysOfMessages[lessonId]?.append(model.message)
        }

        dataSource.data.removeAll() // не удалять, ломаются подарки
        dataSource.reloadData() // +
        dataSource.data = arraysOfMessages[lessonId] ?? []
        dataSource.reloadData() // +
    }

    private func reconnectToChat(roomId: Int) {
        guard currentLessonId == roomId, !self.appOnBackground else { return }
        switch self.userType {
        case .specialist:
            guard !self.callIsActive else { return }
            self.connectToChat(lessonId: currentLessonId ?? 0)
            #if DEBUG
            self.showNotification?("Chat info:", "[chat] TRY TO RECONNECT")
            #endif
        case .participant, .viewer:
            print("[chat] TRY TO RECONNECT")
            #if DEBUG
            self.showNotification?("Chat info:", "[chat] TRY TO RECONNECT")
            #endif
            self.connectToChat(lessonId: currentLessonId ?? 0)
        case .none:
            return
        }
    }

    private func connectToChat(lessonId: Int) {
        print("[chat] [CONNECT TO CHAT \(lessonId)]")
        #if DEBUG
        self.showNotification?("Chat info:", "[chat] [CONNECT TO CHAT \(lessonId)]")
        #endif
        chatBag = .init()
        followChatsManager(chatService: SocketLessonChatServiceFactory.make(roomId: String(lessonId)), lessonId: lessonId)

        sendMessage.subscribe(onNext: { [weak self] text in
            guard !text.isEmpty else { return }
            self?.newMessage(message: text, unauthUser: "")
        }).disposed(by: chatBag)

        sendGift.subscribe(onNext: { [weak self] gift in
            self?.newGift(unauthUser: "", gift: gift)
        }).disposed(by: chatBag)
    }

    func fetchMoreMessages() {
        guard let chatService = self.chatService else { return }
        messagesCount += 15
        chatService.moreMessages(length: messagesCount)
    }

    func disconnectСhatSocket() {
        chatService?.disconnect()
        chatService = nil
    }

    private func fetchMessages() {
        guard let chatService = self.chatService else { return }
        chatService.fetchMessages()
    }

    private func newMessage(message: String, unauthUser: String) {
        guard let chatService = self.chatService else { return }
        chatService.newMessage(unauthUser: unauthUser, message: message, gift: nil)
    }

    private func newGift(unauthUser: String, gift: Int) {
        guard let chatService = self.chatService else { return }
        chatService.newMessage(unauthUser: unauthUser, message: "", gift: gift)
    }

    private func newCustomMessage(messagetype: CustomMessages, messageValue: Bool) {
        guard let chatService = self.chatService, self.isOwner else { return }
        let request = [messagetype.rawValue: messageValue]
        chatService.newCustomMessage(message: request)
    }

    private func followChatsManager(chatService: SocketLessonChatSrevice, lessonId: Int) {
        disconnectСhatSocket()
        self.chatService = chatService

        Observable
            .combineLatest(recahbilityManager.hasNetworkConnection, chatService.isConnected)
            .filter { $0 == true && $1 == .cancelled }
            .buffer(timeSpan: .seconds(30), count: 2, scheduler: MainScheduler.instance)
            .bind { [weak self] socketCanceledEvents in
                if socketCanceledEvents.count >= 2 {
                    let title = "lost_connection_to_server".localized
                    let subTitle = "log_into_the_app_again".localized
                    self?.showNotification?(title, subTitle)
                }
            }
            .disposed(by: chatBag)

        chatService
            .messages
            .subscribe(onNext: { [weak self] messages in
                messages.messages.forEach { message in
                    self?.addMessage(model: LessonChatMessage(message: message, isNew: false), lessonId: lessonId)
                }
            })
            .disposed(by: chatBag)

        chatService
            .newMessage
            .subscribe(onNext: { [weak self] newMessage in
                self?.addMessage(model: LessonChatMessage(message: newMessage.message, isNew: true), lessonId: lessonId)
                self?.messagesCount += 1
                AmplitudeProvider.shared.streamMessageTraking(userId: newMessage.message.authorId, message: newMessage.message.content)
            })
            .disposed(by: chatBag)

        chatService
            .viewersCount
            .subscribe(onNext: { [weak self] viewers in
                guard let self = self else { return }
                self.streamViewers.accept(viewers.count)
                self.newCustomMessage(messagetype: .teacherCameraIsActive, messageValue: self.cameraIsActiveState)
            })
            .disposed(by: chatBag)

        chatService
            .dataChannel
            .subscribe(onNext: { [weak self] customMessageModel in
                guard let message = customMessageModel.data.anotherData.keys.first,
                      let self = self
                else { return }

                let signalType = CustomMessages(rawValue: message) ?? .undefined
                guard let flag = customMessageModel.data.anotherData[message].value else { return }
                switch signalType {
                case .lessonFinished:
                    self.exitTheLesson(fromBackground: false)
                    self.onLessonFinished.accept(true)
                case .teacherCameraIsActive:
                    self.onChangeTeacherCameraState.onNext(flag)
                    self.previousTimeTeachersCameraWasActive = flag
                    switch self.userType {
                    case .specialist:
                        self.hideStreamView(!flag)
                    case .participant:
                        return
                    case .viewer:
                        guard self.microOrPlayerIsActiveState else { return }
                        self.streamPlayer?.isMuted = false
                    case .none:
                        return
                    }
                case .teacherOnLesson:
                    return
                case .undefined:
                    return
                }
            })
            .disposed(by: chatBag)

        chatService
            .isConnected
            .subscribe(onNext: { [weak self] connection in
                guard let self = self else { return }
                switch connection {
                case .connected:
                    print("[chat] connected")
                    #if DEBUG
                    self.showNotification?("Chat info:", "[chat] connected")
                    #endif
                    self.arraysOfMessages[lessonId]? = []
                    self.messagesCount = 0
                    self.fetchMessages()
                    if self.isOwner {
                        self.newCustomMessage(messagetype: .teacherOnLesson, messageValue: true)
                    }
                case .disconnected:
                    print("[chat] disconnected")
                    #if DEBUG
                    self.showNotification?("Chat info:", "[chat] disconnected")
                    #endif
                    return
                case .cancelled:
                    print("[chat] cancelled")
                    #if DEBUG
                    self.showNotification?("Chat info:", "[chat] cancelled")
                    #endif
                case .error:
                    print("[chat] error")
                    #if DEBUG
                    self.showNotification?("Chat info:", "[chat] error")
                    #endif
                    guard let roomId = Int(chatService.roomId), roomId == lessonId else { return }
                    self.reconnectToChat(roomId: roomId)
                }
            })
            .disposed(by: chatBag)
    }

    private func sendCameraStateMessages() {
        guard isOwner else { return }
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.newCustomMessage(messagetype: .teacherCameraIsActive, messageValue: self.cameraIsActiveState)
        }
    }

    // MARK: - TEACHER METHODS

    private func startRTMP(streamUrlString: String, token: String) {
        let session = AVAudioSession.sharedInstance()
        setupSession(session: session)
        let rtmpConnection = RTMPConnection()
        let rtmpStream = RTMPStream(connection: rtmpConnection)
        setupRTMP(rtmpStream: rtmpStream)
        rtmpConnection.addEventListener(.rtmpStatus, selector: #selector(rtmpStatusHandler), observer: self)
        rtmpConnection.connect(streamUrlString)
        rtmpStream.publish(token)
        let hkView = GLHKView(frame: .init(origin: .zero, size: UIScreen.size))
        hkView.attachStream(rtmpStream)
        hkView.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.rtmpStream = rtmpStream
        self.rtmpConnection = rtmpConnection
        onTeacherStreamViewReady.onNext(hkView)
    }

    @objc private func rtmpStatusHandler(_ notification: Notification) {
        let event = Event.from(notification)
        guard let data: ASObject = event.data as? ASObject,
              let code: String = data["code"] as? String,
              let responce = self.responceCopy else {
                  return
              }
        DispatchQueue.main.async {
            switch code {
            case RTMPConnection.Code.connectSuccess.rawValue:
                print("[CONNECT connectSuccess]")
                self.currentConnactionState = .active
                self.onShowLoader.onNext(false)
            case RTMPConnection.Code.connectFailed.rawValue:
                print("[CONNECT connectFailed]")
                self.currentConnactionState = .closed
            case RTMPConnection.Code.callFailed.rawValue:
                print("[CONNECT callFailed]")
                self.currentConnactionState = .closed
            case RTMPConnection.Code.connectClosed.rawValue:
                print("[CONNECT connectClosed]")
                self.currentConnactionState = .closed
                self.reconnectRTMPIfNeeded()
                guard !self.isNetworkActive else { return }
            case RTMPConnection.Code.connectInvalidApp.rawValue:
                print("[CONNECT connectInvalidApp]")
            default:
                break
            }
            self.dataResponse?(responce, code)
        }
    }

    func setupRTMP(rtmpStream: RTMPStream) {
        rtmpStream.videoSettings = [
            .width: 720,
            .height: 1280,
            .bitrate: 2000 * 1000,
            .muted: !self.cameraIsActiveState
        ]

        rtmpStream.captureSettings = [
            .fps: 30,
            .sessionPreset: AVCaptureSession.Preset.high,
            .continuousExposure: true,
            .continuousAutofocus: true,
            .isVideoMirrored: cameraIsFront ? true : false
        ]

        rtmpStream.audioSettings = [
            .muted: !microOrPlayerIsActiveState,
            .bitrate: 128 * 1000
        ]

        rtmpStream.attachAudio(AVCaptureDevice.default(for: AVMediaType.audio)) { error in
            print(error)
        }

        rtmpStream.attachCamera(currentCamera) { error in
            print(error)
        }
    }

    func handleFlipCameraTap() {
        rtmpStream?.attachCamera(cameraIsFront ? backCamera : frontCamera) { error in
            print(error)
        }
        cameraIsFront.toggle()
        self.cameraIsFront = cameraIsFront

        rtmpStream?.captureSettings = [
            .isVideoMirrored: cameraIsFront ? true : false
        ]
    }

    func hideStreamView(_ isHidden: Bool) {
        rtmpStream?.videoSettings = [
            .muted: isHidden
        ]
    }

    func closeConnection() {
        rtmpConnection?.close()
        rtmpStream?.close()
        currentConnactionState = .closed
    }

    func handleOnCameraTeacherTap(cameraIsActive: Bool) {
        newCustomMessage(messagetype: .teacherCameraIsActive, messageValue: cameraIsActive)
    }

    private func bindAlertPositiveAction() {
        finishStream
            .bind { [weak self] _ in
                self?.finishCurrentStream()
            }
            .disposed(by: bag)
    }

    private func finishCurrentStream() {
        gcoreService
            .finishLesson(lessonId: currentLessonId ?? 0)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.newCustomMessage(messagetype: .lessonFinished, messageValue: true)
                case .error(let error):
                    print("finish lesson error \(error.localizedDescription)")
                }
                self?.closeConnection()
            }
            .disposed(by: bag)
    }

    private func reconnectRTMPIfNeeded() {
        guard currentConnactionState == .closed else { return }
        getLiveLessons()
        currentConnactionState = .loading
    }

    func handleConnectionState() {
        switch self.userType {
        case .specialist:
            self.reconnectRTMPIfNeeded()
        case .viewer:
            self.getLiveLessons()
        case .participant:
            return
        case .none:
            return
        }
    }

    // MARK: - VIEWER METHODS

    func printInfo(_ items: Any) {
        print("[Viewer Stream] \(String(describing: items))")
    }

    func connectWithLessonId(_ lessonId: Int) {
        lessonService
            .lessonDetail(id: lessonId, saved: false)
            .subscribe { [weak self] event in
                guard let self = self
                else { return }
                switch event {
                case .success(let details):
                    self.lessonDetails[lessonId] = details
                    self.isSubscribe = details.isSubscribe
                    self.cost = details.cost
                    self.isNeedPresentVipLessonPopUp = false
                    if details.isSubscribe == false, details.cost != 0, !details.isOwner {
                        self.isNeedPresentVipLessonPopUp = true
                        self.vipLessonPopupView = VipLessonPopupView(
                            lesson: details,
                            categoryImage: IconCategory(rawValue: details.categoryName.localized)?.icon
                        )
                        if let popupView = self.vipLessonPopupView {
                            self.bindVipLessonPopup(popupView: popupView, lesson: details)
                        }
                        self.updateCellModels(lessonId: lessonId)
                    } else {
                        self.getStream(lessonId: lessonId)
                    }
                case .error(let error):
                    self.restartNetwork(function: { [weak self] in self?.connectWithLessonId(lessonId) },
                                        ifFailedShowAlertWithError: error, andOurErrorCode: "3")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func streamFeedModel(lessonId: Int) -> StreamFeedViewModel {
        let model = StreamFeedViewModel(streamModel: StreamModel(
            id: lessonId,
            specialistInfo: streamsList[lessonId]?.owner,
            lessonDateEnd: nil,
            categoryId: streamsList[lessonId]?.category?.parent ?? 0
        ))
        return model
    }

    private func streamFeedModelForUpdate(lessonId: Int) -> StreamFeedViewModel {
        let model = StreamFeedViewModel(streamModel: StreamModel(
            id: lessonId,
            specialistInfo: streamsListForUpdate[lessonId]?.owner,
            lessonDateEnd: streamsListForUpdate[lessonId]?.date,
            categoryId: streamsListForUpdate[lessonId]?.category?.parent ?? 0
        ))
        return model
    }

    private func updateCellModels(lessonId: Int) {
        cellModels = lessonIds.map { streamFeedModel(lessonId: $0) }
        cellModels = cellModels.map {
            guard $0.streamModel.id == lessonId,
                  let details = self.lessonDetails[lessonId]
            else { return $0 }

            let lessonDateEnd = Formatters.getIntervalWithDuration(duration: details.lessonTime,
                                                                   dateStart: details.date - Date.diffrenceBetweenRealTime)
            $0.streamModel = StreamModel(id: lessonId,
                                         specialistInfo: details.owner,
                                         lessonDateEnd: lessonDateEnd,
                                         categoryId: details.subCategory.parent)
            return $0
        }
        let filteredCellModels = cellModels.filter {
            $0.streamModel.specialistInfo != nil &&
            $0.streamModel.specialistInfo?.id != UserDefaultsHelper.shared.userId
        }
        if filteredCellModels.isEmpty {
            self.flow.accept(.networkErrorPopUp(errorMessage: "4",
                                                action: { self.flow.accept(.onBack()) }))
        }
        onDataSourceUpdate.accept((filteredCellModels, .newStreamSelected))
    }

    private func prepareCellModelsForUpdate(lessonId: Int) {
        cellModelsForUpdate = cellModelsForUpdate.map {
            guard $0.streamModel.id == lessonId,
                  let details = self.lessonDetails[lessonId]
            else { return $0 }

            let lessonDateEnd = Formatters.getIntervalWithDuration(duration: details.lessonTime,
                                                                   dateStart: details.date - Date.diffrenceBetweenRealTime)
            $0.streamModel = StreamModel(id: lessonId,
                                         specialistInfo: details.owner,
                                         lessonDateEnd: lessonDateEnd,
                                         categoryId: details.subCategory.parent)
            return $0
        }
        let filteredCellModels = cellModelsForUpdate.filter {
            $0.streamModel.specialistInfo != nil &&
            $0.streamModel.specialistInfo?.id != UserDefaultsHelper.shared.userId
        }
        if filteredCellModels.isEmpty {
            self.flow.accept(.networkErrorPopUp(errorMessage: "5",
                                                action: { self.flow.accept(.onBack()) }))
        }
        guard canUpdateCollection else { return }
        lessonIdsBeforeUpdate = lessonIds
        lessonIds = lessonsIdsForUpdate
        streamsList = streamsListForUpdate
        recievedStreams = recievedStreamsForUpdate
        cellModels = cellModelsForUpdate
        onDataSourceUpdate.accept((filteredCellModels, .periodiсUpdate))
    }

    private func bindVipLessonPopup(popupView: VipLessonPopupView, lesson: LessonDetailResponse) {
        popupView
            .flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .buyAndDismiss:
                    self.flow.accept(.showCoineWriteOffPopup(lessonId: lesson.id, cost: lesson.cost))
                case .dismiss:
                    self.handleOnBackTap()
                }
            }
            .disposed(by: bag)
    }

    private func bindViewerStreamComplete() {
        self.onReadyToPresentVipLessonPopup
            .bind { [weak self] id in
                guard let self = self,
                      self.isNeedPresentVipLessonPopUp,
                      let vipLessonPopupView = self.vipLessonPopupView,
                      id == self.currentLessonId,
                      vipLessonPopupView.lesson.id == id
                else { return }
                self.onPresentVipLessonPopup.accept(vipLessonPopupView)
            }.disposed(by: bag)
    }

    private func configureViewersPlayer(streamUrlString newUrlAbsoluteString: String, lessonId: Int, itemNeedsRefreshing: Bool = false) {
        streamPlayer?.pause()
        guard lessonId == self.currentLessonId else { return }
        let session = AVAudioSession.sharedInstance()
        self.setupSession(session: session)
        guard let streamUrl = URL(string: newUrlAbsoluteString) else { return }
        let asset = AVAsset(url: streamUrl)
        let requiredAssetKeys = ["playable", "hasProtectedContent"]
        let playerItem = AVPlayerItem(asset: asset, automaticallyLoadedAssetKeys: requiredAssetKeys)
        playerItem.preferredForwardBufferDuration = 1

        if let currentUrl = streamPlayer?.currentItem?.asset as? AVURLAsset,
           currentUrl.url.absoluteString != newUrlAbsoluteString || itemNeedsRefreshing {
            streamPlayer?.replaceCurrentItem(with: nil) // this added to remove last frame of paused video
            streamPlayer?.replaceCurrentItem(with: playerItem)
            streamPlayer?.isMuted = true
            self.observe(playerItem: playerItem)
            self.onPlayerReadyForDisplay.accept(streamPlayer)
        } else if streamPlayer == nil {
            let streamPlayer = AVPlayer(playerItem: playerItem)
            self.streamPlayerItem = playerItem
            self.streamPlayer = streamPlayer
            self.setNotificationObservers(on: streamPlayer)
            self.observe(player: streamPlayer)
            streamPlayer.isMuted = true
            self.observe(playerItem: playerItem)
            self.onPlayerReadyForDisplay.accept(streamPlayer)
        }
        streamPlayer?.play()
    }

    func setPlayer(mute: Bool) {
        guard let player = streamPlayer, microOrPlayerIsActiveState else { return }
        player.isMuted = mute
    }

    var playerItemDidReachEnd: Bool = false

    @objc func playerItemDidReachEnd(notification: Notification) {
        guard (notification.object as? AVPlayerItem) == streamPlayer?.currentItem else { return }
        print("[playerItemDidReachEnd]")
    }

    @objc func playerItemStalled() {
        print("[playerItemStalled]")
    }

    private func observe(player: AVPlayer?) {
        guard let player = player else {
            playerTimeControlStatusObservation = nil
            return
        }

        if let observersToken = playerTimeControlStatusObservation {
            player.removeObserverForAllProperties(observer: observersToken)
        }

        playerTimeControlStatusObservation = player.observe(\.timeControlStatus) { [weak self] player, _ in
            guard let self = self else { return }
            switch player.timeControlStatus {
            case .paused:
                print("[PAUSED]")
                print("PLAYER ERROR Paused \(String(describing: player.error?.localizedDescription))")
            case .waitingToPlayAtSpecifiedRate:
                print("[WAITING]")
            case .playing:
                print("[PLAYING]")
                self.onStartPlaying.onNext(())
                self.onShowLoader.onNext(false)
            @unknown default:
                print("[unknown default]")
            }
        }
    }

    private func observe(playerItem: AVPlayerItem?) {
        guard let playerItem = playerItem else {
            playerBufferingObservationTimeRanges = nil
            playerBufferingObservationisFull = nil
            playerBufferingObservationIsEmpty = nil
            playerItemStatusObservation = nil
            playerItemKeepUpObservation = nil
            return
        }

        playerBufferingObservationIsEmpty = playerItem.observe(\.isPlaybackBufferEmpty) { [weak self] item, _ in
            guard let self = self
            else { return }
            if item.isPlaybackBufferEmpty {
                self.printInfo("[isPlaybackBufferEmpty]")
                self.onShowLoader.onNext(true)
            } else {
                self.printInfo("buffer is not empty")
            }
        }

        playerItemStatusObservation = playerItem.observe(\.status) { [weak self] item, _ in
            guard let self = self, let player = self.streamPlayer else { return }
            if item.status == .readyToPlay {
                if !player.isPlaying {
                    player.play()
                }
                self.printInfo("[READY TO PLAY]")
            } else if item.status == .failed {
                self.printInfo("[FAILED]")
                if let error = item.error {
                    self.printInfo(error.localizedDescription)
                }
                self.onShowLoader.onNext(true)
            }
        }

        playerItemKeepUpObservation = playerItem.observe(\.isPlaybackLikelyToKeepUp) { [weak self] item, _ in
            guard let self = self, let player = self.streamPlayer else { return }
            if item.isPlaybackLikelyToKeepUp {
                print("[isPlaybackLikelyToKeepUp = true]")
                if !player.isPlaying, player.currentItem?.status == .readyToPlay {
                    player.play()
                }
            } else {
                print("[isPlaybackLikelyToKeepUp = false]")
            }
        }

        playerSizeObservation = playerItem.observe(\.presentationSize, options: NSKeyValueObservingOptions()) {[weak self] _, _ in
            guard
                let self = self,
                let presentationSize = self.streamPlayer?.currentItem?.presentationSize,
                presentationSize != .zero
            else { return }

            self.onUpdatePlayerSize.onNext(presentationSize)
        }
    }

    private func refreshPlaying(lessonId: Int) {
        pleyerIsRefreshing = true
        guard let streamUrl = self.streamUrlString,
              !streamUrl.isEmpty
        else {
            print("[PLAYER NO REFRESHED]")
            return
        }
        configureViewersPlayer(streamUrlString: streamUrl, lessonId: lessonId, itemNeedsRefreshing: true)
        pleyerIsRefreshing = false
        print("[PLAYER REFRESHED]")
    }

    private func updateCollectinEveryMinute() {
        timer?.invalidate()
        timer = Timer(timeInterval: 30, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.updateLiveLessons()
        }
        if let timerForUpdate = timer {
            RunLoop.main.add(timerForUpdate, forMode: .default)
        }
    }

    private func updateLiveLessons() {
        let countOfLessonsToDownload = currentIndex + Consts.countOfStreamsForOnePage
        lessonService.live(pageNumber: 1, lessonsInPage: countOfLessonsToDownload)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let liveResponse):
                    guard self.isShowDonateAlert else {
                        let recievedLiveStreams = liveResponse.data ?? []
                        self.recievedStreamsForUpdate = recievedLiveStreams

                        self.setStreamsListForUpdate(from: recievedLiveStreams)
                        self.setLessonIdsForUpdate(from: recievedLiveStreams)

                        self.numberOfStreamsInLastPage = self.lessonsIdsForUpdate.count
                        self.numberOfLastPage = 1

                        self.updateCollectionDataSource(lessonIds: self.lessonsIdsForUpdate)
                        return
                    }
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    func updateCollectionDataSource(lessonIds: [Int]) {
        guard lessonIds.contains(where: { $0 == currentLessonId }) else { return }
        cellModelsForUpdate = lessonIds.map { streamFeedModelForUpdate(lessonId: $0) }
        guard !cellModelsForUpdate.isEmpty else { return }
        print("[swipe] UPDAte Collection every 30 sec")
        prepareCellModelsForUpdate(lessonId: currentLessonId ?? 0)
    }
}

// MARK: - EXTENSIONS

extension StreamsFeedViewModelNew: CXCallObserverDelegate {
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        if call.hasEnded == true {
            print("[CALL] Disconnected") // сброс
            callIsActive = false
            guard isOwner else { return }
            self.reconnectRTMPIfNeeded()
        }
        if call.isOutgoing == true && call.hasConnected == false {
            print("[CALL] Dialing") // исходяший
            callIsActive = true
            guard isOwner else { return }
            self.closeConnection()
        }
        if call.isOutgoing == false && call.hasConnected == false && call.hasEnded == false {
            print("[CALL] Incoming") // входящий
            callIsActive = true
            guard isOwner else { return }
            self.closeConnection()
        }
        if call.hasConnected == true && call.hasEnded == false {
            print("[CALL] Connected") // взяли трубку
            callIsActive = true
        }
    }
}
