//
//  PopupPayView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxCocoa
import RxSwift

final class PopupPayView: UIView, DrawerContentView {

    // MARK: - Public Properties

    var onDismiss: (() -> Void)?

    let viewHeight = BehaviorRelay<CGFloat>(value: 0)

    // MARK: - Private Properties

    private let dismissButton: UIButton = {
        let btn = UIButton()
        btn.contentMode = .scaleAspectFill
        btn.setImage(UIImage(named: "dismissButtonGrey"), for: .normal)
        return btn
    }()

    private let titleLabel: UILabel = {
        let title = UILabel()
        title.text = "stream_feed_pay".localized
        title.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        title.textColor = TalkmeColors.grayLabels
        title.textAlignment = .center
        return title
    }()

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        tbv.registerCells(withModels:
            ChangePayCellViewModel.self,
            ApplePayCellViewModel.self,
            CardDataCellViewModel.self,
            PayButtonCellViewModel.self,
            EmptyTableCellViewModel.self)
        return tbv
    }()

    private let viewModel: PopupPayViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: PopupPayViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setUpAppearance()
        setupLayout()
        bindVM()
        viewModel.setupApplePayItems()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func updateLayout(height: CGFloat) {
        //todo: update
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tableView.endEditing(true)
    }

    private func setUpAppearance() {
        backgroundColor = TalkmeColors.mainAccountBackground
        layer.cornerRadius = 15
        clipsToBounds = true
    }

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: viewModel.bag)

        dismissButton.rx.tap
            .map { .dismiss }
            .bind(to: viewModel.flow)
            .disposed(by: viewModel.bag)

        viewModel.updateHeight
            .bind(to: viewHeight)
            .disposed(by: viewModel.bag)
    }

    private func setupLayout() {
        addSubviews([tableView, titleLabel, dismissButton])

        dismissButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 16 : 20)
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -10 : -12)
            make.size.equalTo(UIScreen.isSE ? 13 : 17)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(dismissButton.snp.bottom).offset(4)
            make.leading.trailing.equalToSuperview()
        }

        tableView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 17 : 22)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
}
