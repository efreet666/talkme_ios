//
//  PopupPayData.swift
//  talkme_ios
//
//  Created by Yura Fomin on 30.03.2021.
//

final class PopupPayData {
    var card: String?
    var cardDate: String?
    var cardCode: String?

    init(card: String?, cardDate: String?, cardCode: String?) {
        self.card = card
        self.cardDate = cardDate
        self.cardCode = cardCode
    }
}
