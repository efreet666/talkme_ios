//
//  ApplePayTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxSwift

final class ApplePayTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var applePayButtonTap = applePayButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let applePayButton = ApplePayButton()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(applePayButton)

        applePayButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 15 : 20)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 65 : 84)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
        }
    }
}
