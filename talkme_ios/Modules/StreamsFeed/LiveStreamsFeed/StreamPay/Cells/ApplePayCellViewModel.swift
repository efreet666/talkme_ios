//
//  ApplePayCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxCocoa
import RxSwift

final class ApplePayCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let applePayButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: ApplePayTableCell) {

        cell
            .applePayButtonTap
            .bind(to: applePayButtonTap)
            .disposed(by: cell.bag)
    }
}
