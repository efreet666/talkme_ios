//
//  ChangePayTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxSwift

final class ChangePayTableCell: UITableViewCell {

    // MARK: = Public Properties

    private(set) lazy var segmentControlTap = changePaySegmentedControl.rx.value.changed
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let items = ["stream_feed_apple_pay".localized, "stream_feed_card".localized]
    private let segmentFont: UIFont = .montserratSemiBold(ofSize: UIScreen.isSE ? 15 : 17)

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.codeCountry
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 17)
        return lbl
    }()

    private lazy var changePaySegmentedControl: UISegmentedControl = {
        let control = UISegmentedControl(items: items)
        control.clipsToBounds = true
        control.layer.masksToBounds = true
        control.selectedSegmentIndex = 0
        control.tintColor = TalkmeColors.white
        control.backgroundColor = TalkmeColors.grayTimeSlider
        control.setTitleTextAttributes([NSAttributedString.Key.font: segmentFont], for: .normal)
        if #available(iOS 13.0, *) {
            control.selectedSegmentTintColor = TalkmeColors.white
        }
        return control
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    func configure(title: String?) {
        titleLabel.text = title
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([changePaySegmentedControl, titleLabel])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().inset(10)
            make.bottom.equalTo(changePaySegmentedControl.snp.top).inset(UIScreen.isSE ? -16 : -18)
        }
        changePaySegmentedControl.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(UIScreen.isSE ? 40 : 50)
            make.bottom.equalTo(UIScreen.isSE ? -4 : -6)
        }
    }
}
