//
//  PayButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxCocoa
import RxSwift

final class PayButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: Public properties

    let payButtonTap = PublishRelay<Void>()
    let payButtonIsActive = BehaviorRelay<Bool>(value: false)
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: PayButtonTableCell) {

        cell
            .payButtonTap
            .bind(to: payButtonTap)
            .disposed(by: cell.bag)

        payButtonIsActive
            .bind(to: cell.payButtonIsEnabled)
            .disposed(by: cell.bag)

        payButtonIsActive
            .bind { [weak cell] in
                cell?.configure($0)
            }
            .disposed(by: cell.bag)
    }
}
