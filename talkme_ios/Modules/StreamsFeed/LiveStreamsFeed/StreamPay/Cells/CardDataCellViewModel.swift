//
//  CardDataCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxCocoa
import RxSwift

final class CardDataCellViewModel: TableViewCellModelProtocol {

    // MARK: Public properties

    let cardNumberTextRelay = PublishRelay<String?>()
    let cardDateRelay = PublishRelay<String?>()
    let cardCodeRelay = PublishRelay<String?>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: CardDataTableCell) {

        cell
            .cardNumberTextRelay
            .bind(to: cardNumberTextRelay)
            .disposed(by: cell.bag)

        cell
            .cardDateTextRelay
            .bind(to: cardDateRelay)
            .disposed(by: cell.bag)

        cell
            .cardCodeTextRelay
            .bind(to: cardCodeRelay)
            .disposed(by: cell.bag)
    }
}
