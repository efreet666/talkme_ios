//
//  ChangePayCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxCocoa
import RxSwift

enum SegmentPaymentType: Int {
    case pay
    case card
}

final class ChangePayCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let segmentControlTap = PublishRelay<SegmentPaymentType>()
    let bag = DisposeBag()
    let title: String?

    init(title: String?) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: ChangePayTableCell) {
        cell
            .segmentControlTap
            .compactMap { SegmentPaymentType(rawValue: $0) }
            .bind(to: segmentControlTap)
            .disposed(by: cell.bag)

        cell.configure(title: title)
    }
}
