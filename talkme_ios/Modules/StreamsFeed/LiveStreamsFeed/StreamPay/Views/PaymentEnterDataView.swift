//
//  PaymentEnterDataView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import RxSwift
import SwiftMaskTextfield

final class PaymentEnterDataView: UIView {

    // MARK: - Public Properties

    private(set) lazy var textRelay = textField.rx.text

    // MARK: - Private Properties

    private let textField: SwiftMaskTextfield = {
        let tf = SwiftMaskTextfield()
        tf.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        return tf
    }()

    // MARK: - Initializers

    init(placeholder: String, formatPattern: String) {
        super.init(frame: .zero)
        backgroundColor = TalkmeColors.white
        textField.placeholder = placeholder
        textField.formatPattern = formatPattern
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(textField)

        textField.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 14 : 20)
        }
    }
}
