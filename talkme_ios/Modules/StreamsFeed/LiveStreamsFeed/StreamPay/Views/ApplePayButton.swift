//
//  ApplePayButton.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.03.2021.
//

import UIKit

final class ApplePayButton: UIButton {

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    // MARK: - Private Methods

    private func initialSetup() {
        setTitle("stream_feed_pay_title".localized, for: .normal)
        contentMode = .scaleAspectFill
        setImage(UIScreen.isSE
                    ? UIImage(named: "apple")
                    : UIImage(named: "appleBig"), for: .normal)
        titleLabel?.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        setTitleColor(TalkmeColors.white, for: .normal)
        backgroundColor = TalkmeColors.black
        imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 0)
        titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -10)
    }
}
