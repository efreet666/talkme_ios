//
//  ButtonInHelpView.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import Foundation
import RxGesture
import UIKit

final class ButtonInHelpView: UIView {

    enum TypeView {
        case complain
        case block

        var title: String {
            switch self {
            case .complain:
                return "complaint_title_view".localized
            case .block:
                return "blocked_title_view".localized
            }
        }

        var icon: UIImage {
            switch self {
            case .complain:
                return UIImage(named: "complaintTitle") ?? UIImage()
            case .block:
                return UIImage(named: "blockedTitle") ?? UIImage()
            }
        }
    }

    private let titleLabel: UILabel = {
        let lb = UILabel()
        lb.textColor = .black
        lb.textAlignment = .left
        lb.font = .montserratBold(ofSize: 15)
        return lb
    }()

    private let iconImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        return img
    }()

    private(set) lazy var tapView = self.rx.tapGesture().when(.recognized)

    init(type: TypeView) {
        titleLabel.text = type.title
        iconImage.image = type.icon
        super.init(frame: .zero)
        setupStyleView()
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupStyleView() {
        backgroundColor = .clear
    }

    private func addConstraints() {
        addSubviews(iconImage, titleLabel)

        iconImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(14)
            make.size.equalTo(24)
        }

        titleLabel.snp.makeConstraints { make in
            make.centerY.equalTo(iconImage)
            make.leading.equalTo(iconImage.snp.trailing).inset(-11)
        }
    }
}

