//
//  CoinView.swift
//  talkme_ios
//
//  Created by admin on 06.10.2022.
//

import UIKit

final class CoinView: UIView {

    private let coinImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "coin")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let coinLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.textColor = TalkmeColors.white
        priceLabel.font = .montserratBold(ofSize: 18)
        priceLabel.textAlignment = .right
        return priceLabel
    }()

    init() {
        super.init(frame: .zero)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setCoin(coin: Int?) {
        coinLabel.text = "\(coin ?? 0)"
    }

    private func addConstraints() {
        addSubviews(coinImage, coinLabel)

        coinImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview()
            make.size.equalTo(17.7)
        }

        coinLabel.snp.makeConstraints { make in
            make.centerY.equalTo(coinImage)
            make.trailing.equalTo(coinImage.snp.leading).inset(-3)
        }
    }
}

