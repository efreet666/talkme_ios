//
//  NewSocialView.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import UIKit

class NewSocialView: UIView {

    enum TypeNewSocial {
        case vk
        case telegram
        case odnoklasniki

        var icon: UIImage {
            switch self {
            case .vk:
                return UIImage(named: "vkSupport") ?? UIImage()
            case .telegram:
                return UIImage(named: "telegramSupport") ?? UIImage()
            case .odnoklasniki:
                return UIImage(named: "odnoklasnikiSupport") ?? UIImage()
            }
        }

        var color: UIColor {
            switch self {
            case .vk:
                return TalkmeColors.vkIconColor
            case .telegram:
                return TalkmeColors.telegramIconColor
            case .odnoklasniki:
                return TalkmeColors.odnoklasnikiIconColor
            }
        }
    }

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    init(type: TypeNewSocial) {
        iconImageView.image = type.icon
        super.init(frame: .zero)
        backgroundColor = type.color
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
    }

    private func addConstraints() {
        addSubview(iconImageView)

        iconImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? 19 : 23)
        }
    }
}

