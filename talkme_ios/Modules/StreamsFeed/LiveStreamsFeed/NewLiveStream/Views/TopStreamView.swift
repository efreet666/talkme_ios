//
//  TopStreamView.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import UIKit
import RxSwift
import RxGesture
import RxCocoa
import Kingfisher

final class TopStreamView: UIView {

    private let closeImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "closeStream")
        image.contentMode = .scaleAspectFit
        image.isUserInteractionEnabled = true
        return image
    }()

    private let avatarImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.isUserInteractionEnabled = true
        image.layer.cornerRadius = 6
        image.clipsToBounds = true
        return image
    }()

    private let helpImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named: "help")
        image.isUserInteractionEnabled = true
        return image
    }()

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = TalkmeColors.white
        label.font = .montserratBold(ofSize: 15)
        return label
    }()

    private let streamNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = TalkmeColors.white
        label.font = .montserratFontRegular(ofSize: 15)
        return label
    }()

    private let vipView = VipView()
    private let liveView = LiveView()
    private let timeView = TimeAndViewersView(type: .time)
    private var timer: Timer?
    private let bag = DisposeBag()
    private let addView = AddView()

    private(set) var countViewersView = TimeAndViewersView(type: .viewers)
    private(set) lazy var tapHelp = helpImageView.rx.tapGesture().when(.recognized)
    private(set) lazy var tapAddTeacher = addView.rx.tapGesture().when(.recognized)
    private(set) lazy var tapClose = closeImageView.rx.tapGesture().when(.recognized)
    private(set) lazy var tapAvatar = avatarImageView.rx.tapGesture().when(.recognized)
    private(set) lazy var isConnect = PublishRelay<Bool>()
    private var secondTime = 0
    
    private lazy var savedStreamViews = [avatarImageView, nameLabel, streamNameLabel, closeImageView, helpImageView, addView]

    let lessonFinished = PublishSubject<Void>()
    var isSaved = false

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    deinit {
        timer?.invalidate()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        addConstraints(isSaved)
    }

    func setValue(isContact: Bool) {
        addView.setValue(isContact)
    }

    func configure(_ model: StreamModel, lessonDetail: LessonDetailResponse, isContact: Bool) {
        hideIfTeacher(lessonDetail.isOwner)
        if let imageURL = model.specialistInfo?.avatarUrlOriginal, !imageURL.isEmpty {
            let correctImageUrl = imageURL
            avatarImageView.kf.setImage(with: URL(string: correctImageUrl))
        } else {
            avatarImageView.image = UIImage(named: "addImage")
        }
        nameLabel.text = model.specialistInfo?.firstName
        streamNameLabel.text = lessonDetail.name
        setupTimer(initial: lessonDetail.date, dateEnd: model.lessonDateEnd ?? 0)
        addView.setValue(isContact)
        vipView.checkStatusVip(lessonDetail.vip)
    }
    
    func configureSavedStream(_ model: SavedStreamModel, lessonDetail: LessonDetailResponse, isContact: Bool) {
        hideIfTeacher(lessonDetail.isOwner)
        if let imageURL = model.specialistInfo?.avatarUrlOriginal, !imageURL.isEmpty {
            let correctImageUrl = imageURL
            avatarImageView.kf.setImage(with: URL(string: correctImageUrl))
        } else {
            avatarImageView.image = UIImage(named: "addImage")
        }
        nameLabel.text = model.specialistInfo?.firstName
        streamNameLabel.text = lessonDetail.name
        addView.setValue(isContact)
    }

    func hideAnimation(_ isHide: Bool) {
        [avatarImageView, nameLabel, streamNameLabel, vipView, liveView, timeView, countViewersView, helpImageView]
            .forEach { $0.isHidden = isHide }
    }

    func alphaAnimation(_ isHidden: Bool) {
        [avatarImageView, nameLabel, streamNameLabel, addView, vipView, liveView, timeView, countViewersView, helpImageView]
            .forEach { $0.alpha = isHidden ? 0 : 1 }
    }

    private func setupTimer(initial interval: TimeInterval, dateEnd: Double) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.setupLabel(timeInterval: interval, dateEnd: dateEnd)
        }
    }

    private func setupLabel(timeInterval: TimeInterval, dateEnd: Double) {
        let time = TimerManager.diffFromNowTime(timeInterval: timeInterval)
        let date = Date().timeIntervalSince1970
        let newDate = dateEnd - date
        if newDate / 60 <= 1 {
            timeView.setRedView(TalkmeColors.warningColor)
        }
        if newDate / 60 <= 0 {
            lessonFinished.onNext(())
            timer?.invalidate()
        }
        timeView.setValue(getLessonTime(timeEnd: Double(time)))
    }

    private func getLessonTime(timeEnd: Double) -> String {
        return Formatters.streamTimeFormatter.string(from: Date(timeIntervalSince1970: timeEnd))
    }

    private func hideIfTeacher(_ isOwner: Bool) {
        addView.isHidden = isOwner
        helpImageView.isUserInteractionEnabled = !isOwner
    }

    private func addConstraints(_ isSaved: Bool) {
        if isSaved {
            addSubviews(savedStreamViews)
            closeImageView.snp.makeConstraints { make in
                make.trailing.equalToSuperview().inset(16)
                make.top.equalTo(avatarImageView)
                make.size.equalTo(18)
            }
            
            helpImageView.snp.makeConstraints { make in
                make.centerX.equalTo(closeImageView)
                make.top.equalTo(closeImageView.snp.bottom).inset(UIScreen.isSE ? -18 : -23)
                make.size.equalTo(28)
            }
            
            avatarImageView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(17)
                make.top.equalToSuperview().inset(UIScreen.isSE ? 5 : 25)
                make.size.equalTo(44)
                
                nameLabel.snp.makeConstraints { make in
                    make.leading.equalTo(avatarImageView.snp.trailing).inset(-11)
                    make.top.equalTo(avatarImageView)
                }
                
                addView.snp.makeConstraints { make in
                    make.size.equalTo(25)
                    make.leading.equalTo(nameLabel.snp.trailing).inset(-11)
                    make.centerY.equalTo(nameLabel)
                }
                
                streamNameLabel.snp.makeConstraints { make in
                    make.bottom.equalTo(avatarImageView)
                    make.leading.equalTo(avatarImageView.snp.trailing).inset(-15)
                    make.trailing.equalTo(helpImageView.snp.leading).inset(-10)
                }
            }
        } else {
            addSubviews(
                closeImageView,
                avatarImageView,
                vipView,
                nameLabel,
                streamNameLabel,
                addView,
                liveView,
                countViewersView,
                timeView,
                helpImageView
            )
            
            closeImageView.snp.makeConstraints { make in
                make.trailing.equalToSuperview().inset(16)
                make.top.equalTo(avatarImageView)
                make.size.equalTo(18)
            }
            
            helpImageView.snp.makeConstraints { make in
                make.centerX.equalTo(closeImageView)
                make.top.equalTo(closeImageView.snp.bottom).inset(UIScreen.isSE ? -18 : -23)
                make.size.equalTo(28)
            }
            
            avatarImageView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(17)
                make.top.equalToSuperview().inset(UIScreen.isSE ? 5 : 25)
                make.size.equalTo(44)
            }
            
            liveView.snp.makeConstraints { make in
                make.top.equalTo(avatarImageView.snp.bottom).inset(UIScreen.isSE ? -15 : -25)
                make.centerX.equalTo(avatarImageView)
                make.width.equalTo(44)
                make.height.equalTo(25)
            }
            
            countViewersView.snp.makeConstraints { make in
                make.leading.equalTo(liveView.snp.trailing).inset(-9)
                make.centerY.equalTo(liveView)
                make.height.equalTo(25)
            }
            
            timeView.snp.makeConstraints { make in
                make.leading.equalTo(countViewersView.snp.trailing).inset(-9)
                make.centerY.equalTo(countViewersView)
                make.height.equalTo(25)
                make.width.equalTo(98)
            }
            
            vipView.snp.makeConstraints { make in
                make.top.equalTo(avatarImageView)
                make.leading.equalTo(avatarImageView.snp.trailing).inset(-15)
                make.height.equalTo(19)
                make.width.equalTo(57)
            }
            
            nameLabel.snp.makeConstraints { make in
                make.leading.equalTo(vipView.snp.trailing).inset(-11)
                make.top.equalTo(avatarImageView)
            }
            
            addView.snp.makeConstraints { make in
                make.size.equalTo(25)
                make.leading.equalTo(nameLabel.snp.trailing).inset(-11)
                make.centerY.equalTo(nameLabel)
            }
            
            streamNameLabel.snp.makeConstraints { make in
                make.bottom.equalTo(avatarImageView)
                make.leading.equalTo(avatarImageView.snp.trailing).inset(-15)
                make.trailing.equalTo(helpImageView.snp.leading).inset(-10)
            }
        }
    }
}
