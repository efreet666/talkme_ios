//
//  CupView.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import UIKit

class CupView: UIView {

    private let awardsImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "whiteCup")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    let awardsNumberLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratBold(ofSize: 12)
        return label
    }()

    init() {
        super.init(frame: .zero)
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupAppearance()
    }

    private func setupAppearance() {
        gradientBackground(from: TalkmeColors.gradientLightGreen, to: TalkmeColors.gradientGreen, direction: .leftToRight)
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
    }

    private func setupConstraints() {
        addSubviews(awardsImage, awardsNumberLabel)

        awardsImage.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(7)
            make.centerY.equalToSuperview()
            make.height.equalTo(11)
            make.width.equalTo(12)
        }

        awardsNumberLabel.snp.makeConstraints { make in
            make.left.equalTo(awardsImage.snp.right).inset(-3)
            make.right.equalToSuperview().inset(7)
            make.centerY.equalToSuperview()
            make.height.equalTo(13)
        }

    }
}

