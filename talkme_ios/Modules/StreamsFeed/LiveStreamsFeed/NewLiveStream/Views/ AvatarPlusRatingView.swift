//
//   AvatarPlusRatingView.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import Foundation
import UIKit

class AvatarPlusRating: UIView {

    // MARK: - Private Properties

    private(set) var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 11
        imageView.clipsToBounds = true
        imageView.image = UIImage(named: "addImage")
        return imageView
    }()

    private let ratingView: ImageAndTitleView = {
        let view = ImageAndTitleView()
        view.configure(ImageAndTitleViewType.rating(0))
        return view
    }()

    private let backgroundRating: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.ratingViewYellow
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        return view
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setImage(image: UIImage?) {
        avatarImageView.image = image
    }

    func hideRating(_ hide: Bool) {
        ratingView.isHidden = hide
        backgroundRating.backgroundColor = hide ? .clear: TalkmeColors.ratingViewYellow
    }

    func configure(rating: Double?) {
        ratingView.configure(ImageAndTitleViewType.rating(rating ?? 0))
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews(avatarImageView, backgroundRating)
        backgroundRating.addSubview(ratingView)

        avatarImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        backgroundRating.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(-8)
            make.width.equalTo(67)
        }

        ratingView.snp.makeConstraints { make in
            make.center.equalTo(backgroundRating)
        }
    }
}

