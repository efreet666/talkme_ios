//
//  TopUpBalancePopUpView.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import RxSwift
import RxCocoa
import UIKit

final class TopUpBalancePopUpView: UIView, DrawerContentView {

    enum Flow {
        case dissmissPopupWithSuccess
        case dissmissPopupWithError
    }

    private enum Constants {
        static let topToSwipeLine: CGFloat = UIScreen.isSE ? 8 : 11
        static let swipeLineSize = UIScreen.isSE
            ? CGSize(width: 30, height: 3)
            : CGSize(width: 37, height: 3)
        static let leftOffest: CGFloat = 25
        static let cellHeight: CGFloat = 161
    }

    // MARK: - Public properties

    var onDismiss: (() -> Void)?
    let flow = PublishRelay<Flow>()
    let viewHeight = BehaviorRelay<CGFloat>(
        value: (UIScreen.height / 1.3)
    )

    // MARK: - Private Properties

    private let blurEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
            view.effect = blurEffect
        }
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 13
        view.clipsToBounds = true
        return view
    }()

    private let swipeLineView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.swipeLineColor
        return view
    }()

    private let thankLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 16 : 20)
        label.textColor = TalkmeColors.white
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = "top_up_balance".localized
        return label
    }()

    private let balanceContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.darkColorView
        view.layer.cornerRadius = 7
        return view
    }()

    private let balanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        label.text = "new_stream_balance".localized
        return label
    }()

    private let coinImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "coin")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.textColor = TalkmeColors.white
        priceLabel.font = .montserratBold(ofSize: 18)
        priceLabel.textAlignment = .right
        priceLabel.adjustsFontSizeToFitWidth = true
        return priceLabel
    }()

    private let sendComplaintButton = GradientView(type: .pay)

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .clear
        collection.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        collection.registerCells(withModels: TopUpBalanceCellViewModel.self)
        return collection
    }()

    private let viewModel: TopUpBalanceViewModel
    private let bag = DisposeBag()

    init(viewModel: TopUpBalanceViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        viewModel.getPurchases()
        setUpAppearance()
        setUpConstraints()
        bindUI()
        AppDelegate.deviceOrientation = .portrait
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func updateLayout(height: CGFloat) {}

    // MARK: - Private Methods

    private func setUpConstraints() {
        addSubviews(blurEffectView)
        blurEffectView.contentView.addSubviews(
            swipeLineView,
            thankLabel,
            collectionView,
            balanceContainerView,
            sendComplaintButton
        )
        balanceContainerView.addSubviews(balanceLabel, coinImage, priceLabel)

        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        swipeLineView.snp.makeConstraints { make in
            make.size.equalTo(Constants.swipeLineSize)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(Constants.topToSwipeLine)
        }

        thankLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(30)
        }

        balanceContainerView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(16)
            make.centerY.equalTo(thankLabel)
            make.height.equalTo(40)
            make.width.equalTo(UIScreen.isSE ? 131 : 151)
        }

        balanceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(9)
            make.centerY.equalToSuperview()
        }

        coinImage.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(9)
            make.centerY.equalTo(balanceLabel)
            make.height.equalTo(17.6)
        }

        priceLabel.snp.makeConstraints { make in
            make.trailing.equalTo(coinImage.snp.leading).inset(3)
            make.leading.equalTo(balanceLabel.snp.trailing).inset(-3)
            make.centerY.equalTo(balanceLabel)
        }

        sendComplaintButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 20)
            make.leading.trailing.equalToSuperview().inset(16)
            make.height.equalTo(UIScreen.isSE ? 46 : 56)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(thankLabel.snp.bottom).inset(UIScreen.isSE ? -16 : -26)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(20)
        }
    }

    private func bindUI() {
        viewModel
            .dataItems
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        collectionView.rx
            .setDelegate(self)
            .disposed(by: bag)

        collectionView.rx
            .modelSelected(TopUpBalanceCellViewModel.self)
            .bind { [weak self] modelPlan in
                guard let self = self else { return }
                self.viewModel.selectedID = "\(modelPlan.donat.iosInAppId ?? 0)"
            }
            .disposed(by: bag)

        viewModel
            .myBalance
            .bind { [weak self] coin in
                guard let self = self else { return }
                self.priceLabel.text = "\(coin ?? 0)"
            }
            .disposed(by: viewModel.bag)

        sendComplaintButton
            .tapView
            .bind { [weak self] _ in
                guard let self = self, let selectedID = self.viewModel.selectedID else { return }
                IAPManager.shared.purchase(productWith: selectedID)
                self.viewModel.flow.accept(.dismiss)
            }
            .disposed(by: bag)
    }

    @objc private func hideKeyboard() {
        endEditing(true)
    }

    private func setUpAppearance() {
        clipsToBounds = true
    }
}

extension TopUpBalancePopUpView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 104)
    }
}


