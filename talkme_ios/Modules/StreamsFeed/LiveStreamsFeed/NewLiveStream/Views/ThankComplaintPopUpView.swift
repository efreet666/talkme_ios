//
//  ThankComplaintPopUpView.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import RxSwift
import RxCocoa
import UIKit
import RxKeyboard

final class ThankComplaintPopUpView: UIView, DrawerContentView {

    enum Flow {
        case dissmissPopupWithSuccess
        case dissmissPopupWithError
    }

    private enum Constants {
        static let topToSwipeLine: CGFloat = UIScreen.isSE ? 8 : 11
        static let swipeLineSize = UIScreen.isSE
            ? CGSize(width: 30, height: 3)
            : CGSize(width: 37, height: 3)
        static let leftOffest: CGFloat = 25
    }

    // MARK: - Public properties

    var onDismiss: (() -> Void)?
    let flow = PublishRelay<Flow>()
    let viewHeight = BehaviorRelay<CGFloat>(
        value: (UIScreen.isSE ? 404 : 438)
    )

    // MARK: - Private Properties

    private let blurEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
            view.effect = blurEffect
        }
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 13
        view.clipsToBounds = true
        return view
    }()

    private let swipeLineView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.swipeLineColor
        return view
    }()

    private let thankLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.width / 20)
        label.textColor = TalkmeColors.white
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = "express_gratitude_stream".localized
        return label
    }()

    private let tipsView: StreamDonationViewNew = {
        let view = StreamDonationViewNew(frame: .zero, withRating: false)
        view.layer.cornerRadius = 10
        view.backgroundColor = TalkmeColors.darkColorView
        view.clipsToBounds = true
        return view
    }()

    private lazy var recognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        recognizer.cancelsTouchesInView = false
        return recognizer
    }()

    private let balanceContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.darkColorView
        view.layer.cornerRadius = 7
        return view
    }()

    private let balanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        label.text = "new_stream_balance".localized
        return label
    }()

    private let coinImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "coin")
        image.contentMode = .scaleAspectFit
        return image
    }()

    private let priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.textColor = TalkmeColors.white
        priceLabel.font = .montserratBold(ofSize: 18)
        priceLabel.textAlignment = .right
        priceLabel.adjustsFontSizeToFitWidth = true
        return priceLabel
    }()

    private let sendComplaintButton = GradientView(type: .thanks)
    private let topUpBalanceButton = GradientView(type: .add)

    private let bag = DisposeBag()
    private let userAboutId: Int
    private let viewModel: ThankComplaintViewModel

    init(viewModel: ThankComplaintViewModel, userAbout: Int) {
        self.viewModel = viewModel
        self.userAboutId = userAbout
        super.init(frame: .zero)
        setUpAppearance()
        setUpConstraints()
        bindUI()
        AppDelegate.deviceOrientation = .portrait
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        bindKeyboard()
    }

    // MARK: - Public Methods

    func updateLayout(height: CGFloat) {}

    // MARK: - Private Methods

    private func setUpConstraints() {
        addGestureRecognizer(recognizer)
        addSubviews(blurEffectView)
        blurEffectView.contentView.addSubviews(
            swipeLineView,
            thankLabel,
            tipsView,
            sendComplaintButton,
            topUpBalanceButton,
            balanceContainerView
        )
        balanceContainerView.addSubviews(balanceLabel, coinImage, priceLabel)

        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        swipeLineView.snp.makeConstraints { make in
            make.size.equalTo(Constants.swipeLineSize)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(Constants.topToSwipeLine)
        }

        thankLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 16)
            make.top.equalToSuperview().inset(30)
        }

        topUpBalanceButton.snp.makeConstraints { make in
            make.centerY.equalTo(thankLabel)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 16)
            make.height.equalTo(40)
            make.width.equalTo(30)
        }

        balanceContainerView.snp.makeConstraints { make in
            make.trailing.equalTo(topUpBalanceButton.snp.leading).inset(-4)
            make.centerY.equalTo(topUpBalanceButton)
            make.height.equalTo(40)
            make.width.equalTo(UIScreen.isSE ? 131 : 151)
        }

        balanceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(9)
            make.centerY.equalToSuperview()
        }

        coinImage.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalTo(balanceLabel)
            make.height.equalTo(17.6)
        }

        priceLabel.snp.makeConstraints { make in
            make.trailing.equalTo(coinImage.snp.leading).inset(6)
            make.leading.equalTo(balanceLabel.snp.trailing).inset(-3)
            make.centerY.equalTo(balanceLabel)
        }

        tipsView.snp.makeConstraints { make in
            make.top.equalTo(thankLabel.snp.bottom).inset(-20)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 16)
            make.bottom.equalTo(sendComplaintButton.snp.top).inset(-16)
        }

        sendComplaintButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 20)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 16)
            make.height.equalTo(UIScreen.isSE ? 46 : 56)
        }
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let isHide = keyboardVisibleHeight == 0
                let offset: CGFloat = UIScreen.isSE ? 70 : 100
                self.blurEffectView.snp.remakeConstraints { make in
                    make.width.equalTo(UIScreen.width)
                    make.bottom.equalTo(self).inset(!isHide ? keyboardVisibleHeight - offset : keyboardVisibleHeight)
                }
                UIView.animate(withDuration: 0) {
                    self.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    private func bindUI() {
        viewModel
            .myBalance
            .bind { [weak self] coin in
                guard let self = self else { return }
                self.priceLabel.text = "\(coin ?? 0)"
            }
            .disposed(by: viewModel.bag)

        sendComplaintButton
            .tapView
            .bind { [weak self] _ in
                guard let self = self, let text = self.tipsView.textRelay.value else { return }
                if let tips = Int(text), (tips != 0) && !text.isEmpty {
                    if self.viewModel.myBalance.value ?? 0 > 0 {
                        self.viewModel.sendTips(tips: tips)
                    } else {
                        self.viewModel.flow.accept(.showTopUpBalancePopUp)
                    }
                }
                self.viewModel.flow.accept(.dismiss)
            }
            .disposed(by: bag)

        topUpBalanceButton
            .tapView
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.flow.accept(.showTopUpBalancePopUp)
            }
            .disposed(by: bag)
    }

    @objc private func hideKeyboard() {
        endEditing(true)
    }

    private func setUpAppearance() {
        clipsToBounds = true
    }
}

