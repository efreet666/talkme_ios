//
//  AddView.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import UIKit

final class AddView: UIView {

    private let blurEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        view.backgroundColor = .black.withAlphaComponent(0.25)
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        return view
    }()

    private let iconImage: UIImageView = {
        let imageVIew = UIImageView()
        imageVIew.contentMode = .scaleAspectFit
        imageVIew.image = UIImage(named: "notSubscribe")
        return imageVIew
    }()

    private var gradientLayer: CAGradientLayer?

    init() {
        super.init(frame: .zero)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setValue(_ isContact: Bool) {
        
        iconImage.image = isContact ? UIImage(named: "subscribe") : UIImage(named: "notSubscribe")

        let startColor = isContact ? TalkmeColors.gradientLightGreen : .black.withAlphaComponent(0.25)
        let endColor = isContact ? TalkmeColors.gradientGreen : .black.withAlphaComponent(0.25)
        gradientLayer?.removeFromSuperlayer()
        gradientLayer = blurEffectView.gradientBackground(from: startColor, to: endColor, direction: .leftToRight)
    }

    private func addConstraints() {
        addSubviews(blurEffectView)
        blurEffectView.contentView.addSubviews(iconImage)

        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        iconImage.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
            make.size.equalTo(7)
        }
    }
}
