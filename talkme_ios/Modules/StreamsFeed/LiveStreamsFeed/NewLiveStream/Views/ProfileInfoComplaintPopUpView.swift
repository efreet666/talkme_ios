//
//  ProfileInfoComplaintPopUpView.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import RxSwift
import RxCocoa
import UIKit

final class ProfileInfoComplaintPopUpView: UIView, DrawerContentView {

    private enum Constants {
        static let topToSwipeLine: CGFloat = UIScreen.isSE ? 8 : 11
        static let swipeLineSize = UIScreen.isSE
            ? CGSize(width: 30, height: 3)
            : CGSize(width: 37, height: 3)
        static let leftOffest: CGFloat = 25
    }

    // MARK: - Public properties

    var onDismiss: (() -> Void)?
    var viewHeight = BehaviorRelay<CGFloat>(
        value: (UIScreen.isSE ? 404 : 523)
    )

    // MARK: - Private Properties

    private let blurEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
            view.effect = blurEffect
        }
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 13
        view.clipsToBounds = true
        return view
    }()

    private let swipeLineView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.white
        return view
    }()

    private let avatarView = AvatarPlusRating()
    private let vipView = VipView()
    private let cupView = CupView()

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.textAlignment = .left
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 16 : 20)
        label.numberOfLines = 0
        return label
    }()

    private let ageLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.textAlignment = .left
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        label.numberOfLines = 0
        return label
    }()

    private let cityLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.textAlignment = .left
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        label.numberOfLines = 0
        return label
    }()

    private let vkontakteView = NewSocialView(type: .vk)
    private let telegramView = NewSocialView(type: .telegram)
    private let odnoclassnikiView = NewSocialView(type: .odnoklasniki)

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.textAlignment = .left
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 12 : 15)
        label.numberOfLines = 3
        return label
    }()

    private let readLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 91, green: 100, blue: 253)
        label.textAlignment = .left
        label.font = .montserratBold(ofSize: 15)
        label.text = "Читать полностью"
        label.isUserInteractionEnabled = true
        return label
    }()

    private let idView = IdView()

    private let separatorView = UIView()
    private let complaintView = GradientView(type: .blue)
    private let messageView = GradientView(type: .green)

    private lazy var tapReadLabel = readLabel.rx.tapGesture().when(.recognized)
    private lazy var tapAvatarView = avatarView.rx.tapGesture().when(.recognized)
    private lazy var tapComplaintView = complaintView.rx.tapGesture().when(.recognized)
    private lazy var tapMessageView = messageView.rx.tapGesture().when(.recognized)
    private lazy var tapVkontakteView = vkontakteView.rx.tapGesture().when(.recognized)
    private lazy var tapTelegramView = telegramView.rx.tapGesture().when(.recognized)
    private lazy var tapOdnoclassnikiView = odnoclassnikiView.rx.tapGesture().when(.recognized)

    private let bag = DisposeBag()
    private var isRead = false
    private let infoModel: PublicProfileResponse
    private let viewModel: ProfileInfoComplaintViewModel

    init(infoModel: PublicProfileResponse, viewModel: ProfileInfoComplaintViewModel) {
        self.viewModel = viewModel
        self.infoModel = infoModel
        super.init(frame: .zero)
        viewModel.fetchData(classNumber: infoModel.specialistInfo.numberClass ?? "")
        setupAppearance(infoModel: infoModel)
        setUpAppearance()
        setUpConstraints()
        bindUI()
        AppDelegate.deviceOrientation = .portrait
        configure(infoModel: infoModel)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    private func configure(infoModel: PublicProfileResponse) {
        if let imageURL = infoModel.specialistInfo.avatarUrl, !imageURL.isEmpty {
            let correctImageUrl = imageURL
            avatarView.avatarImageView.kf.setImage(with: URL(string: correctImageUrl))
        } else {
            avatarView.setImage(image: UIImage(named: "addImage"))
        }
        nameLabel.text = "\(infoModel.specialistInfo.firstName ?? "") \n\(infoModel.specialistInfo.lastName ?? "")"
        ageLabel.text = "\(infoModel.specialistInfo.age ?? 0)" + " " + "of_the_year_stream".localized
        cityLabel.text = infoModel.specialistInfo.city?.city
        descriptionLabel.text = infoModel.specialistInfo.bio
        avatarView.configure(rating: infoModel.specialistInfo.rating)
        idView.setValue(id: "\(infoModel.specialistInfo.id ?? 0)")
        vipView.checkStatusVip(infoModel.specialistInfo.vip)
        cupView.awardsNumberLabel.text = "\(infoModel.specialistInfo.lessonPassed ?? 0)"
        hideSocial()
    }

    private func setupAppearance(infoModel: PublicProfileResponse) {
        complaintView.isContact = infoModel.isContact
    }

    func updateLayout(height: CGFloat) {}

    // MARK: - Private Methods

    private func setUpConstraints() {
        addSubviews(blurEffectView)
        blurEffectView.contentView.addSubviews(
            swipeLineView,
            avatarView,
            vipView,
            cupView,
            nameLabel,
            ageLabel,
            cityLabel,
            vkontakteView,
            telegramView,
            odnoclassnikiView,
            descriptionLabel,
            idView,
            readLabel,
            complaintView,
            separatorView,
            messageView
        )

        swipeLineView.snp.makeConstraints { make in
            make.size.equalTo(Constants.swipeLineSize)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(Constants.topToSwipeLine)
        }

        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        avatarView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 14)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 33 : 43)
            make.height.equalTo(UIScreen.height / 5.5)
            make.width.equalTo(UIScreen.width / 2.6)
        }

        vipView.snp.makeConstraints { make in
            make.leading.equalTo(avatarView.snp.trailing).inset(UIScreen.isSE ? -17 : -25)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 45 : 55)
            make.height.equalTo(19)
            make.width.equalTo(57)
        }

        cupView.snp.makeConstraints { make in
            make.left.equalTo(vipView.snp.right).inset(-5)
            make.centerY.equalTo(vipView)
            make.height.equalTo(19)
            make.trailing.lessThanOrEqualTo(75)
        }

        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(vipView.snp.bottom).inset(-9)
            make.leading.equalTo(avatarView.snp.trailing).inset(UIScreen.isSE ? -17 : -25)
            make.trailing.equalToSuperview().inset(47)
        }

        ageLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).inset(-7)
            make.leading.equalTo(avatarView.snp.trailing).inset(UIScreen.isSE ? -17 : -25)
            make.trailing.equalToSuperview().inset(47)
        }

        cityLabel.snp.makeConstraints { make in
            make.top.equalTo(ageLabel.snp.bottom).inset(-8)
            make.leading.equalTo(avatarView.snp.trailing).inset(UIScreen.isSE ? -17 : -25)
            make.trailing.equalToSuperview().inset(47)
        }

        vkontakteView.snp.makeConstraints { make in
            make.top.equalTo(avatarView.snp.bottom).inset(-26)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            make.size.equalTo(UIScreen.isSE ? 34 :44)
        }

        telegramView.snp.makeConstraints { make in
            make.leading.equalTo(vkontakteView.snp.trailing).inset(UIScreen.isSE ? -7.1 : -9.1)
            make.centerY.equalTo(vkontakteView)
            make.size.equalTo(UIScreen.isSE ? 34 :44)
        }

        odnoclassnikiView.snp.makeConstraints { make in
            make.leading.equalTo(telegramView.snp.trailing).inset(UIScreen.isSE ? -7.1 : -9.1)
            make.centerY.equalTo(telegramView)
            make.size.equalTo(UIScreen.isSE ? 34 :44)
        }

        readLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            make.top.equalTo(descriptionLabel.snp.bottom).inset(UIScreen.isSE ? -10 : -19)
        }

        idView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 12 : 14)
            make.top.equalTo(readLabel.snp.bottom).inset(UIScreen.isSE ? -10 : -19)
            make.height.equalTo(UIScreen.isSE ? 34 : 44)
            make.width.equalTo(UIScreen.isSE ? 158 : 188)
        }

        complaintView.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 24)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            make.trailing.equalTo(separatorView.snp.leading).inset(-4.5)
            make.height.equalTo(UIScreen.isSE ? 46 : 56)
        }
        separatorView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(10)
            make.height.equalTo(10)
            make.width.equalTo(1)
        }

        messageView.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 24)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            make.leading.equalTo(separatorView.snp.trailing).inset(-4.5)
            make.height.equalTo(UIScreen.isSE ? 46 : 56)
        }
    }

    private func hideSocial() {
        odnoclassnikiView.isHidden = true

        if infoModel.specialistInfo.vk == nil || infoModel.specialistInfo.telegram == "" {
            vkontakteView.isHidden = true

            telegramView.snp.makeConstraints { make in
                make.top.equalTo(avatarView.snp.bottom).inset(-26)
                make.leading.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
                make.size.equalTo(UIScreen.isSE ? 34 :44)
            }
        }

        if infoModel.specialistInfo.telegram == nil || infoModel.specialistInfo.telegram == "" {
            telegramView.isHidden = true
        }

        if (infoModel.specialistInfo.telegram == nil || infoModel.specialistInfo.telegram == "") && (infoModel.specialistInfo.vk == nil || infoModel.specialistInfo.telegram == "") {
            descriptionLabel.snp.makeConstraints { make in
                make.top.equalTo(avatarView.snp.bottom).inset(-26)
                make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            }
        } else {
            descriptionLabel.snp.makeConstraints { make in
                make.top.equalTo(vkontakteView.snp.bottom).inset(UIScreen.isSE ? -10 : -18)
                make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
            }
        }
    }

    private func bindUI() {
        tapReadLabel
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.isRead.toggle()
                self.readLabel.text = self.isRead ? "hide_text".localized : "read_completely".localized
                let labelHeight = self.descriptionLabel.text?.height(
                    font: .montserratFontRegular(ofSize: UIScreen.isSE ? 12 : 15),
                    width: UIScreen.width,
                    numberOfLines: 10) ?? 0
                self.descriptionLabel.numberOfLines = self.isRead ? 10 : 3
                if self.descriptionLabel.maxNumberOfLines > 3 {
                    self.viewHeight.accept(
                        self.isRead
                        ? UIScreen.isSE ? 404 + labelHeight : 523 + labelHeight
                        : UIScreen.isSE ? 404 : 523
                    )
                }
            }.disposed(by: bag)

        tapAvatarView
            .bind { [weak self] _ in
            guard let self = self else { return }
            self.viewModel.flow.accept(.showProfileVC(self.infoModel.specialistInfo.numberClass ?? ""))
        }.disposed(by: bag)

        tapMessageView
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.flow.accept(.showChatVC(self.infoModel.specialistInfo.id ?? 0))
            }.disposed(by: bag)

        tapVkontakteView
            .bind { [weak self] _ in
                guard let self = self else { return }
                SocialRedirectionHelper.vk.redirect(to: self.infoModel.specialistInfo.vk ?? "")
            }.disposed(by: bag)

        tapTelegramView
            .bind { [weak self] _ in
                guard let self = self else { return }
                SocialRedirectionHelper.telegram.redirect(to: self.infoModel.specialistInfo.telegram ?? "")
            }.disposed(by: bag)

        tapComplaintView
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.fetchData(classNumber: self.infoModel.specialistInfo.numberClass ?? "")
                guard let isContact = self.viewModel.infoStream?.isContact else { return }
                isContact
                    ? self.viewModel.removeContact(id: self.infoModel.specialistInfo.id ?? 0)
                    : self.viewModel.addContact(id: self.infoModel.specialistInfo.id ?? 0)
                self.complaintView.isContact = isContact
                self.complaintView.isContact.toggle()
                self.complaintView.setValue(self.complaintView.isContact)
                if !self.viewModel.isContactRelay.value {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        NotificationSubscription.shared.showNotification(.subscription)
                    }
                }
            }.disposed(by: bag)

        viewModel
            .isContactRelay
            .bind { [weak self] isContact in
                guard let self = self else { return }
                self.complaintView.setValue(isContact)
            }.disposed(by: bag)
    }

    @objc private func hideKeyboard() {
        endEditing(true)
    }

    private func setUpAppearance() {
        clipsToBounds = true
    }
}


extension UILabel {

    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font as Any], context: nil).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
}

