//
//  TimeAndViewersView.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import UIKit

final class TimeAndViewersView: UIView {

    enum TimeAndViewersType {
        case viewers
        case time

        var icon: UIImage {
            switch self {
            case .viewers:
                return UIImage(named: "streamerNameWhite") ?? UIImage()
            case .time:
                return UIImage(named: "whiteTimer") ?? UIImage()
            }
        }
    }

    private let blurEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        view.backgroundColor = .black.withAlphaComponent(0.25)
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        return view
    }()

    private let maskBlurView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        return view
    }()

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private(set) var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = TalkmeColors.white
        label.font = .montserratSemiBold(ofSize: 14)
        return label
    }()

    let type: TimeAndViewersType

    init(type: TimeAndViewersType, color: UIColor? = nil) {
        self.type = type
        maskBlurView.backgroundColor = color
        iconImageView.image = type.icon
        super.init(frame: .zero)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        addConstraints()
    }

    func setValue(_ value: String?) {
        titleLabel.text = value ?? "0"
    }

    func setRedView(_ color: UIColor) {
        let blurEffect = UIBlurEffect(style: .light)
        blurEffectView.effect = blurEffect
    }

    private func addConstraints() {
        addSubviews(blurEffectView)
        blurEffectView.contentView.addSubviews(maskBlurView)
        maskBlurView.addSubviews(titleLabel, iconImageView)

        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        maskBlurView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        iconImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(10)
            make.size.equalTo(14)
        }

        if type == .viewers {
            titleLabel.snp.makeConstraints { make in
                make.leading.equalTo(iconImageView.snp.trailing).inset(-6)
                make.trailing.equalToSuperview().inset(10)
                make.centerY.equalTo(iconImageView)
            }
        } else {
            titleLabel.snp.makeConstraints { make in
                make.leading.equalTo(iconImageView.snp.trailing).inset(-6)
                make.centerY.equalTo(iconImageView)
            }
        }
    }
}
