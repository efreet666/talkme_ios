//
//  GradientView.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import Foundation
import UIKit
import RxSwift

final class GradientView: UIView {

    enum ViewType {
        case blue
        case green
        case thanks
        case add
        case send
        case thankCloseStream
        case pay
        case logInStream
        case startStream

        var colorOne: UIColor {
            switch self {
            case .green, .send, .logInStream:
                return TalkmeColors.gradientLightGreen
            case .blue, .thanks, .add, .pay, .startStream:
                return TalkmeColors.gradientLightBlue
            case .thankCloseStream:
                return TalkmeColors.gradientLightPurple
            }
        }

        var colorTwo: UIColor {
            switch self {
            case .green, .send, .logInStream:
                return TalkmeColors.gradientGreen
            case .blue, .thanks, .add, .pay, .startStream:
                return TalkmeColors.gradientBlue
            case .thankCloseStream:
                return TalkmeColors.gradientPurple
            }
        }

        var notColor: UIColor {
            switch self {
            case .green, .send, .logInStream:
                return TalkmeColors.gradientGreen
            case .blue, .thanks, .add, .thankCloseStream, .pay, .startStream:
                return TalkmeColors.notGradient
            }
        }

        var textAligment: NSTextAlignment {
            switch self {
            case .blue, .green, .thanks, .add, .send, .thankCloseStream, .pay, .startStream:
                return .center
            case .logInStream:
                return .left
            }
        }

        var title: String {
            switch self {
            case .green:
                return "message_stream".localized
            case .blue:
                return "subscribe_stream".localized
            case .thanks:
                return "give_thanks_stream".localized
            case .add:
                return "+"
            case .send:
                return "send_stream".localized
            case .thankCloseStream:
                return "give_thanks_stream".localized
            case .pay:
                return "stream_feed_buy".localized
            case .logInStream:
                return "log_in_stream".localized
            case .startStream:
                return "start_stream".localized
            }
        }

        var isContactTitle: String {
            switch self {
            case .green:
                return "message_stream".localized
            case .blue:
                return "unsubscribe_stream".localized
            case .thanks:
                return "give_thanks_stream".localized
            case .add:
                return "+"
            case .send:
                return "send_stream".localized
            case .thankCloseStream:
                return "give_thanks_stream".localized
            case .pay:
                return "stream_feed_buy".localized
            case .logInStream:
                return "log_in_stream".localized
            case .startStream:
                return "plan_stream".localized
            }
        }

        var font: UIFont {
            switch self {
            case .logInStream:
                return .montserratBold(ofSize: UIScreen.isSE ? 13 : 17)
            case .startStream:
                return .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 16)
            default:
                return .montserratFontMedium(ofSize: UIScreen.isSE ? 14 : 18)
            }
        }
    }

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        return label
    }()

    private let coinImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "coin")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.textColor = TalkmeColors.white
        priceLabel.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 17)
        priceLabel.textAlignment = .right
        return priceLabel
    }()

    var type: ViewType
    var isContact = false
    private(set) lazy var tapView = self.rx.tapGesture().when(.recognized)

    init(type: ViewType) {
        titleLabel.font = type.font
        titleLabel.text = type.title
        titleLabel.textAlignment = type.textAligment
        self.type = type
        super.init(frame: .zero)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.setValue(self.isContact)
        }
    }

    func setCoinValue(_ coin: String?) {
        priceLabel.text = coin ?? "0"
    }

    func setTitle(_ isContact: Bool) {
        if isContact {
            titleLabel.text = type.title
        } else {
            titleLabel.text = type.isContactTitle
        }
    }

    func setValue(_ isContact: Bool) {
        if isContact {
            removeGradientLayer()
            backgroundColor = UIColor(red: 19, green: 27, blue: 29)
            alpha = 0.7
            titleLabel.text = type.isContactTitle
        } else {
            removeGradientLayer()
            let colorOne = type.colorOne
            let colorTwo = type.colorTwo
            gradientBackground(from: colorOne, to: colorTwo, direction: .leftToRight)
            alpha = 1
            titleLabel.text = type.title
        }
        layer.cornerRadius = 7
        clipsToBounds = true
    }

    // MARK: - Private Methods

    private func removeGradientLayer() {
        guard let layers = layer.sublayers else { return }
        for aLayer in layers where aLayer is CAGradientLayer {
            aLayer.removeFromSuperlayer()
        }
    }

    private func addConstraints() {
        switch type {
        case .logInStream:
            addSubviews(titleLabel, coinImage, priceLabel)
            titleLabel.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.leading.trailing.equalToSuperview().inset(21)
            }

            coinImage.snp.makeConstraints { make in
                make.trailing.equalToSuperview().inset(21)
                make.size.equalTo(18)
                make.centerY.equalToSuperview()
            }

            priceLabel.snp.makeConstraints { make in
                make.trailing.equalTo(coinImage.snp.leading).inset(-3)
                make.centerY.equalTo(coinImage)
            }
        default:
            addSubview(titleLabel)
            titleLabel.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }
    }
}
