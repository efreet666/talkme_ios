//
//  HelpView.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import Foundation

import RxGesture
import RxRelay
import UIKit

final class HelpView: UIView {

    enum Flow {
        case back
    }

    private let containerView: UIView = {
        let vw = UIView()
        vw.backgroundColor = TalkmeColors.white
        vw.layer.cornerRadius = 11
        return vw
    }()

    private let complainView = ButtonInHelpView(type: .complain)
    private let blockView = ButtonInHelpView(type: .block)


    private let accountService = AccountService()
    private(set) var flow = PublishRelay<Flow>()
    private lazy var tapView = self.rx.tapGesture().when(.recognized)

    private let viewModel: StreamsFeedViewModelNew

    init(viewModel: StreamsFeedViewModelNew) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        bindUI()
        setupStyleView()
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func sendBlocked() {
//           accountService
//               .blocked(toUserId: viewModel.currentTeacherId ?? 0)
//               .subscribe { [weak self] event in
//                   guard let self = self else { return }
//                   switch event {
//                   case .success:
//                       self.flow.accept(.back)
//                   case .error(let error):
//                       print(error)
//                       self.flow.accept(.back)
//                   }
//               }
               //.disposed(by: viewModel.bag)
       }

    private func bindUI() {
        tapView
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.removeFromSuperview()
            }.disposed(by: viewModel.bag)

        complainView
            .tapView
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.handleOnComplaintButtonTap()
            }.disposed(by: viewModel.bag)

        blockView
            .tapView
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.sendBlocked()
                self.removeFromSuperview()
            }.disposed(by: viewModel.bag)
    }

    private func setupStyleView() {
        backgroundColor = .black.withAlphaComponent(0.5)
    }

    private func addConstraints() {
        addSubview(containerView)
        containerView.addSubviews(complainView, blockView)

        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(91)
            make.trailing.equalToSuperview().inset(9)
            make.width.equalTo(194)
            make.height.equalTo(95)
        }

        complainView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(47.5)
        }

        blockView.snp.makeConstraints { make in
            make.top.equalTo(complainView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(47.5)
        }
    }
}

