//
//  IdView.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import UIKit

final class IdView: UIView {

    private let idLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.textAlignment = .left
        label.font = .montserratSemiBold(ofSize: 15)
        return label
    }()

    init() {
        super.init(frame: .zero)
        setupAppearance()
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setValue(id: String) {
        idLabel.text = "ID: \(id)"
    }

    private func setupAppearance() {
        backgroundColor = TalkmeColors.darkColorView
        layer.cornerRadius = 10
    }

    private func addConstraints() {
        addSubview(idLabel)

        idLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(16)
        }
    }
}

