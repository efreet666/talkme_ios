//
//  LiveView.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import UIKit

final class LiveView: UIView {

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = TalkmeColors.white
        label.font = .montserratSemiBold(ofSize: 14)
        label.text = "live_stream_label".localized
        return label
    }()

    init() {
        super.init(frame: .zero)
        addConstraints()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupAppearance()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupAppearance() {
        layer.cornerRadius = 4
        let colorFrom = UIColor(red: 13, green: 186, blue: 93)
        let colorTo = UIColor(red: 13, green: 186, blue: 155)
        gradientBackground(from: colorFrom, to: colorTo, direction: .rightToLeft)
        clipsToBounds = true
    }

    private func addConstraints() {
        addSubviews(titleLabel)

        titleLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

