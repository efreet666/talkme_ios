//
//  AmountCountView.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import Foundation
import UIKit

final class AmountCountView: UIView {

    private let amountLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratFontRegular(ofSize: 13)
        label.textAlignment = .left
        label.text = "Кол-во"
        return label
    }()

    private let priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.textColor = TalkmeColors.white
        priceLabel.font = .montserratBold(ofSize: UIScreen.width / 16)
        priceLabel.textAlignment = .left
        return priceLabel
    }()

    private let coinImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "coin")
        image.contentMode = .scaleAspectFit
        return image
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setValue(coin: Int) {
        priceLabel.text = "\(coin)"
    }

    private func addConstraints() {
        addSubviews(amountLabel, priceLabel, coinImage)

        amountLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview().inset(8)
        }

        priceLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(8)
            make.leading.equalToSuperview()
        }

        coinImage.snp.makeConstraints { make in
            make.leading.equalTo(priceLabel.snp.trailing).inset(-5)
            make.centerY.equalTo(priceLabel)
            make.size.equalTo(28)
        }
    }
}

