//
//  StreamFeedActionsViewNew.swift
//  talkme_ios
//
//  Created by admin on 29.09.2022.
//

import RxSwift
import RxCocoa
import CollectionKit
import UIKit

final class StreamFeedActionsViewNew: UIView {

    private enum Constants {
        static let circleWidth: CGFloat = UIScreen.isSE ? 52 : 70
        static let circleSize: CGSize = .init(width: circleWidth, height: circleWidth)

        static let paginationHeight: CGFloat = UIScreen.isSE ? 40 : 46
        static let footerHeight: CGFloat = UIScreen.isSE ? 36 : 52
        static let actionViewBottom: CGFloat = UIScreen.isSE ? 13 : 15
        static let backButtonInset: CGFloat = UIScreen.isSE ? 13 : 11
        static let rightOffset: CGFloat = UIScreen.isSE ? 9 : 15
        static let chatLeftInset: CGFloat = UIScreen.isSE ? 12 : 13
        static let chatSize: CGSize = .init(
            width: 216,
            height: UIScreen.isSE
                ? UIScreen.height / 2.2
                : UIScreen.height / 3.5
        )
        static let sendMessageHeight: CGFloat = UIScreen.isSE ? 45 : 58

        static let viewersHintOffsetV: CGFloat = UIScreen.isSE ? 6 : 5
        static let viewersHintOffsetH: CGFloat = UIScreen.isSE ? 4 : 10

        static let membersMicroOffsetH: CGFloat = 6
        static let membersLearnerOffsetV: CGFloat = UIScreen.isSE ? 5 : 5
        static let membersSpecialistOffsetV: CGFloat = 4

        static let topViewOffset: CGFloat = 10

        static let pageIndicatorSize = UIScreen.isSE
            ? CGSize(width: 27, height: 2)
            : CGSize(width: 35, height: 3)
    }

    // MARK: - Public Properties

    private(set) lazy var onCoinTap = actionsView.onCoinTap
    private(set) lazy var onBackButtonTap = topView.tapClose
    private(set) lazy var onHelpButtonTap = topView.tapHelp
    private(set) lazy var onAddTeacherButtonTap = topView.tapAddTeacher
    private(set) lazy var onAvatarButtonTap = topView.tapAvatar
    private(set) lazy var isConnectRelay = topView.isConnect
    private(set) lazy var isCameraOn = actionsView.isCameraOn
    private(set) lazy var isMicroOn = actionsView.isMicroOn
    private(set) lazy var onCameraTap = actionsView.onCameraTap
    private(set) lazy var onMicrophoneTap = actionsView.onMicrophoneTap
    private(set) lazy var onMicrophoneOwnerTap = actionsView.onMicrophoneOwnerTap
    private(set) lazy var onFlipCameraTap = actionsView.onFlipCameraTap
    private(set) lazy var onGiftButtonTap = actionsView.onGiftTap
    private(set) lazy var onShareTap = actionsView.onShareTap
    private(set) lazy var onLikeTap = actionsView.onLikeTap
    private(set) lazy var onMembersTap = actionsView.onViewersTap
    private(set) lazy var onFetchMoreMessages = chatView.onFetchMoreMessages
    private(set) lazy var lessonFinished = topView.lessonFinished
    private(set) lazy var sendMessage = sendMessageView.sendMessage
    private(set) var bag = DisposeBag()
    lazy var hintView = StreamHintView()

    let onShowPopUp = PublishRelay<Void>()
    let onShowMembersPopUp = PublishRelay<Void>()
    let streamViewers = PublishRelay<Int>()
    let streamLikes = PublishRelay<Int>()
    let orientationIsPortrait = PublishRelay<Bool>()
    let orientationIsPortraitViewer = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let actionsView: StreamSideActionsViewNew
    private let topView = TopStreamView(frame: .zero)
    private let topPozitionZView = UIView()
    private let centerView = UIView()

    private lazy var footerView: StreamFeedFooterViewNew = {
        let view = StreamFeedFooterViewNew(frame: footerFrame(for: UIScreen.size), userType: userType)
        view.layer.borderWidth = 1.5
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.cornerRadius = 19.5
        return view
    }()

    private let swipeZoneView = UIView()

    private var sendMessageStartFrame: CGRect {
        CGRect(x: 0, y: UIScreen.height,
               width: UIScreen.width, height: Constants.sendMessageHeight)
    }

    private lazy var sendMessageView: StreamSendMessageView = {
        let view = StreamSendMessageView(frame: sendMessageStartFrame)
        view.alpha = 0
        view.isHidden = true
        return view
    }()

    private var chatViewOrigin: CGPoint {
        CGPoint(
            x: Constants.chatLeftInset + UIApplication.leftInset,
            y: footerView.frame.minY - Constants.chatSize.height + 25)
    }

    private lazy var chatView: StreamChatViewNew = {
        let view = StreamChatViewNew(frame: .init(origin: chatViewOrigin, size: Constants.chatSize))
        return view
    }()

    private var isHeightExceeded: Bool {
        self.sendMessageView.frame.height > 98
    }

    private let pagingView = UIView()

    var buttonsCount: Int {
        switch userType {
        case .participant:
            return 4
        case .viewer:
            return 4 // TODO: будет 3, если обычный зритель не будет видеть участников, и в попапе убрать эту ячейку
        case .specialist:
            return 2
        }
    }

    private lazy var dataSourceButtonView: [GenericViewType] = Array(
        repeating: GenericViewType.pageControl, count: buttonsCount)
    private lazy var pageIndicator = RepeatingViewsMakerView(
        viewsData: dataSourceButtonView,
        viewType: StreamPageControlView.self,
        size: Constants.pageIndicatorSize,
        spacing: 7)

    private let userType: UserType
    private var keyboardIsVisible = false
    private var isFirstStreamPresenting = false
    private var isPortrait = true
    private var swipesIsEnabled = true
    private var isHide = false

    // MARK: - Initializers

    init(type: UserType, classNumber: String?) {
        userType = type
        actionsView = .init(type: type)
        super.init(frame: .zero)
        setupLayout(false)
        updateOrientationUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setValue(isContact: Bool) {
        topView.setValue(isContact: isContact)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        func setValue(isContact: Bool) {
            topView.setValue(isContact: isContact)
        }

        let pagingViewOrigin = CGPoint(x: 0, y: UIScreen.height - Constants.paginationHeight)
        let pagingViewSize = CGSize(width: UIScreen.width, height: Constants.paginationHeight)
        pagingView.frame = .init(origin: pagingViewOrigin, size: pagingViewSize)

        pageIndicator.frame.origin = CGPoint(
            x: (pagingViewSize.width - pageIndicator.frame.width) / 2,
            y: (pagingViewSize.height - pageIndicator.frame.height) / 2
        )
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard
            traitCollection.verticalSizeClass != previousTraitCollection?.verticalSizeClass
            || traitCollection.horizontalSizeClass != previousTraitCollection?.horizontalSizeClass
        else {
            return
        }

        updateOrientationUI()

        setNeedsUpdateConstraints()
    }

    // MARK: - Public Methods

    func configure(streamModel: StreamModel, lessonDetail: LessonDetailResponse, isContact: Bool?) {
        bindUI()
        setButtonsHidden(false)
        pageIndicator.viewsArray[0].setSelected(true)
        topView.configure(streamModel, lessonDetail: lessonDetail, isContact: isContact ?? false)
                topView.isConnect.accept(isContact ?? false)
        actionsView.configure(lessonDetails: lessonDetail)
        addNotificationObservers()
    }

    func  startLiveStreamTimer(withEndDate endDate: Double) {
        footerView.startLiveStreamTimer(withEndDate: endDate)
    }

    func  startSavedStreamTimer(withDuration duration: Double) {
        footerView.startSavedStreamTimer(withDuration: duration)
    }

    func prepareViewForShowingSavedStream() {
        footerView.hideChatInput(isHidden: true)
//        actionsView.hideCameraAndMicrophoneButtons(isHidden: true)
        chatView.isHidden = true
    }

    func hideComplaintButtton(isHidden: Bool) {
//        complaintButton.isHidden = isHidden
    }

    func hideTimer(isHidden: Bool) {
        footerView.hideTimer(isHidden: isHidden)
    }

    func setupChats(dataSource: ArrayDataSource<LessonMessage>) {
        chatView.setupCollectionView(dataSource: dataSource)
    }

    func updateSubviewsFrames(for size: CGSize, isLandscape: Bool) {
        hideIsHorizontal(isFirstStreamPresenting ? true : isLandscape)
        setupLayout(isFirstStreamPresenting ? true : isLandscape)
    }

    func setVipLessonState(_ isActive: Bool) {
        swipesIsEnabled = !isActive
        setButtonsHidden(isActive)
//        backButton.isHidden = isActive
    }

    func updateLikes(lessonDetail: LessonDetailResponse) {
        actionsView.configure(lessonDetails: lessonDetail)
    }

    // MARK: - Private Methods

    private func setupComplaintButton() {
//        complaintButton.frame.origin = .init(
//            x: userType == .viewer ? UIScreen.width - (UIScreen.isSE ? 9 : 15) - complaintButton.frame.width : UIScreen.width,
//            y: UIApplication.topInset + (UIScreen.isSE ? 11 : 13))
    }

    private func updateOrientationUI() {
        let isPortrait = UIApplication.shared.statusBarOrientation.isPortrait
        swipeZoneView.isUserInteractionEnabled = isPortrait
        pagingView.isHidden = !isPortrait

        sendMessageView.frame = sendMessageStartFrame
    }

    private func setupConstaints() {
//        addSubview(backButton)
    }

    private func hideIsHorizontal(_ isHorizontal: Bool) {
        actionsView.isHidden = isHorizontal
        topView.hideAnimation(isHorizontal)
    }

    private func bindUI() {
        bag = .init()

        swipeZoneView
            .rx
            .swipeGesture(.right)
            .when(.recognized)
            .filter { [weak self] _ in self?.keyboardIsVisible == false }
            .bind { [weak self] _ in
                guard let self = self, self.swipesIsEnabled else { return }
                guard self.actionsView.alpha != 0 else { return }
                self.setButtonsHidden(true)
            }
            .disposed(by: bag)

        centerView.rx
            .tapGesture()
            .when(.recognized)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.isHide.toggle()
                self.setButtonsHidden(self.isHide)
            }
            .disposed(by: bag)

        rx
            .swipeGesture(.left)
            .when(.recognized)
            .filter { [weak self] _ in self?.keyboardIsVisible == false }
            .bind { [weak self] _ in
                guard let self = self, self.swipesIsEnabled else { return }
                self.setButtonsHidden(false)
            }
            .disposed(by: bag)

        rx
            .tapGesture()
            .when(.recognized)
            .filter { [weak self] recognizer in
                guard let self = self else { return false }
                let point = recognizer.location(in: self.sendMessageView)
                return point.y < 0 && self.keyboardIsVisible
            }
            .bind { [weak self] _ in
                self?.sendMessageView.endEditing()
            }
            .disposed(by: bag)

        footerView
            .onCommentTap
            .bind { [weak self] _ in
                self?.sendMessageView.startEditing()
            }
            .disposed(by: bag)
        
        footerView
            .onMessageButtonTap
            .bind { [weak self] _ in
                self?.chatView.isHidden.toggle()
                self?.footerView.toggleCommentView()
            }
            .disposed(by: bag)

//        footerView
//            .onCommentTap
//            .bind { [weak self] _ in
//                self?.sendMessageView.startEditing()
//            }
//            .disposed(by: bag)

        streamViewers
            .bind { [weak self] count in
                self?.topView.countViewersView.setValue("\(count)")
            }
            .disposed(by: bag)

        streamLikes
            .skip(1)
            .bind { [weak self] count in
                print("!!! count", count)
                self?.actionsView.likeButton.setValue("\(count)")
            }
            .disposed(by: bag)

        orientationIsPortrait
            .bind { [weak self] isActive in
                self?.isPortrait = isActive
                self?.sendMessageView.deviceOrientationChanged(isPortrait: isActive)
            }
            .disposed(by: bag)

    }

    private func removeGradientLayer(_ view: UIView) {
        guard let layers = view.layer.sublayers else { return }
        for aLayer in layers where aLayer is CAGradientLayer {
            aLayer.removeFromSuperlayer()
        }
    }

    private func setButtonsHidden(_ isHidden: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.actionsView.alpha = isHidden ? 0 : 1
            self.footerView.alpha = isHidden ? 0 : 1
            self.chatView.alpha = isHidden ? 0 : 1
            self.topView.alphaAnimation(isHidden)
            self.setGradientZView(isHidden)
        }
    }

    private func setGradientZView(_ isHidden: Bool) {
        if !isHidden {
            DispatchQueue.main.async {
                self.removeGradientLayer(self.topPozitionZView)
                self.topPozitionZView.gradientBackground(
                    from: .clear,
                    to: .black.withAlphaComponent(0.85),
                    direction: .bottomToTop
                )
            }
        } else {
            removeGradientLayer(topPozitionZView)
        }
    }

    private func footerFrame(for screenSize: CGSize) -> CGRect {
        let originX: CGFloat = UIApplication.leftInset
        let bottomInsetLandscape = UIApplication.bottomInset == 0 ? 20 : UIApplication.bottomInset
        let originY = UIDevice.current.orientation.isLandscape
            ? screenSize.height - bottomInsetLandscape - Constants.footerHeight
            : screenSize.height - Constants.paginationHeight - Constants.footerHeight
        let footerOrigin = CGPoint(x: originX, y: originY)
        let footerSize = CGSize(width: screenSize.width, height: Constants.footerHeight)
        return .init(origin: footerOrigin, size: footerSize)
    }

    private func setupLayout(_ isHorizontal: Bool) {
        addSubviews(topPozitionZView, centerView, topView, actionsView, chatView, footerView, sendMessageView)

        topPozitionZView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalTo(topView.snp.bottom)
        }

        centerView.snp.makeConstraints { make in
            make.top.equalTo(topPozitionZView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }

        topView.snp.remakeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).inset(5)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview().inset(isHorizontal ? 20 : 0)
            make.height.equalTo(106)
        }

        actionsView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 15)
            make.bottom.equalTo(footerView)
            make.top.equalTo(topView.snp.bottom)
            make.width.equalTo(40)
        }

        footerView.snp.remakeConstraints { make in
            make.height.equalTo(39)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 24)
            make.leading.equalToSuperview().inset(13)
            if isHorizontal {
                make.width.equalTo(UIScreen.width / 3.5)
            } else {
                make.trailing.equalTo(actionsView.snp.leading).inset(-30)
            }
        }
    }

    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    @objc private func keyboardWillShow(_ notification: Notification) {
        changeCustomTextFieldOrigin(becauseOf: notification, andHideIt: false)
    }

    @objc private func keyboardWillHide(_ notification: Notification) {
        changeCustomTextFieldOrigin(becauseOf: notification, andHideIt: true)
    }

    private func changeCustomTextFieldOrigin(becauseOf notification: Notification, andHideIt textFieldshouldBeHidden: Bool) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height

            self.keyboardIsVisible = !textFieldshouldBeHidden
            let sendMessageStartFrame = self.sendMessageStartFrame
            let sendMessageOriginY = isHidden ? sendMessageStartFrame.origin.y
            : UIScreen.main.bounds.height - keyboardHeight - sendMessageStartFrame.height
            self.sendMessageView.isHidden = textFieldshouldBeHidden
            UIView.animate(withDuration: 0.3) {
                self.sendMessageView.alpha = textFieldshouldBeHidden ? 0 : 1
                let endYPointForShowing = self.isHeightExceeded ? sendMessageOriginY - 40 : sendMessageOriginY
                self.sendMessageView.frame.origin.y = textFieldshouldBeHidden ? UIScreen.height : endYPointForShowing
            }
        }
    }
}
