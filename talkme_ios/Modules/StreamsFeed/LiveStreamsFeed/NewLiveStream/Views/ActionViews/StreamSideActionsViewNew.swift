//
//  StreamSideActionsViewNew.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import RxCocoa
import RxSwift

final class StreamSideActionsViewNew: UIView {

    // MARK: - Public Properties

    let isCameraOn = PublishRelay<Bool>()
    let isMicroOn = PublishRelay<Bool>()
    let isLikeOn = PublishRelay<Bool>()
    let likeButton = SideActionButtonViewNew(type: .like)
    private(set) lazy var onFlipCameraTap = flipCameraButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onGiftTap = giftButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onViewersTap = membersWithMicroButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onCameraTap = cameraButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onMicrophoneTap = microphoneButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onMicrophoneOwnerTap = microphoneOwnerButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onLikeTap = likeButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onCoinTap = coinButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onShareTap = shareButton.rx.tapGesture().when(.recognized)

    // MARK: - Private Properties

    private let shareButton = SideActionButtonViewNew(type: .share)
    private let coinButton = SideActionButtonViewNew(type: .coin)
    private let giftButton = SideActionButtonViewNew(type: .gift)
    private let membersWithMicroButton = SideActionButtonViewNew(type: .viewers)
    private let cameraButton = SideActionButtonViewNew(type: .camera)
    private let microphoneButton = SideActionButtonViewNew(type: .microphone)
    private let microphoneOwnerButton = SideActionButtonViewNew(type: .microphoneOwner)
    private let flipCameraButton = SideActionButtonViewNew(type: .flipCamera)

    private lazy var learnerActionViews = [likeButton, coinButton, shareButton, giftButton, microphoneButton]
    private lazy var specialistActionViews = [shareButton, likeButton, flipCameraButton, cameraButton, microphoneOwnerButton]

    private let type: UserType
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(type: UserType) {
        self.type = type
        super.init(frame: .zero)

        switch type {
        case .participant, .viewer:
            addSubviews(learnerActionViews)
        case .specialist:
            addSubviews(specialistActionViews)
        }

        setupLayout()
        bindUI()
        print("init")
    }
    
    deinit {
        print("deinit")
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func configure(lessonDetails: LessonDetailResponse) {
        likeButton.setupRedBackground(for: .like, isActive: lessonDetails.liked ?? false)
        likeButton.setValue("\(lessonDetails.usersLiked.count)")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        switch type {
        case .specialist:
            hideIfTeacher(true)
        case .viewer, .participant:
            hideIfTeacher(false)
        }
    }
    private func hideIfTeacher(_ isOwner: Bool) {
        if isOwner {
            microphoneOwnerButton.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview()
                make.size.equalTo(39)
            }

            cameraButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneOwnerButton)
                make.bottom.equalTo(microphoneOwnerButton.snp.top).inset(-22)
                make.size.equalTo(39)
            }

            flipCameraButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneOwnerButton)
                make.bottom.equalTo(cameraButton.snp.top).inset(-22)
                make.size.equalTo(39)
            }

            shareButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneOwnerButton)
                make.bottom.equalTo(flipCameraButton.snp.top).inset(-22)
                make.size.equalTo(39)
            }

            likeButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneOwnerButton)
                make.bottom.equalTo(shareButton.snp.top).inset(-43)
                make.size.equalTo(39)
            }
        } else {
            microphoneButton.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview()
                make.size.equalTo(39)
            }

            giftButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneButton)
                make.bottom.equalTo(microphoneButton.snp.top).inset(-22)
                make.size.equalTo(39)
            }

            giftButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneButton)
                make.bottom.equalTo(microphoneButton.snp.top).inset(-22)
                make.size.equalTo(39)
            }

            shareButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneButton)
                make.bottom.equalTo(giftButton.snp.top).inset(-22)
                make.size.equalTo(39)
            }

            coinButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneButton)
                make.bottom.equalTo(shareButton.snp.top).inset(-22)
                make.size.equalTo(39)
            }

            likeButton.snp.makeConstraints { make in
                make.centerX.equalTo(microphoneButton)
                make.bottom.equalTo(coinButton.snp.top).inset(-43)
                make.size.equalTo(39)
            }
        }
    }

    private func bindUI() {
        isMicroOn
            .observeOn(MainScheduler.instance)
            .bind { [weak self] isOn in
                self?.microphoneButton.setupIcon(for: .microphone, isActive: isOn)
                self?.microphoneOwnerButton.setupIcon(for: .microphoneOwner, isActive: isOn)
            }
            .disposed(by: bag)

        isCameraOn
            .observeOn(MainScheduler.instance)
            .bind { [weak self] isOn in
                self?.cameraButton.setupIcon(for: .camera, isActive: isOn)
            }
            .disposed(by: bag)
    }
}

