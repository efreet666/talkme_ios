//
//  StreamFeedFooterViewNew.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import UIKit
import RxSwift

final class StreamFeedFooterViewNew: UIView {

    private enum Constants {
        static let commentFontSize: CGFloat = UIScreen.isSE ? 11 : 13
    }

    // MARK: - Public Properties

    private(set) lazy var onCommentTap = commentContainer.rx.tapGesture().when(.recognized)
    private(set) lazy var onMessageButtonTap = messageButton.rx.tapGesture().when(.recognized)
    private(set) lazy var lessonFinished = attributesView.lessonFinished

    // MARK: - Private Properties

    private let attributesView = StreamTimeViewersView()
    private let messageButton = SideActionButtonViewNew(type: .share)

    private let commentContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    private let commentLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontRegular(ofSize: Constants.commentFontSize)
        label.textColor = .white
        label.textAlignment = .left
        label.text = "stream_feed_hello".localized
        return label
    }()

    private let userType: UserType

    // MARK: - Initializers

    init(frame: CGRect, userType: UserType) {
        self.userType = userType
        super.init(frame: frame)

        setupMessagesViewsFrames()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func hideChatInput( isHidden: Bool) {
        commentContainer.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    func hideTimer(isHidden: Bool) {
        attributesView.hideTimer(isHidden: isHidden)
    }

    func startLiveStreamTimer(withEndDate: Double) {
        attributesView.startTimerForLiveStream(dateEnd: withEndDate)
    }

    func startSavedStreamTimer(withDuration: Double) {
        attributesView.startTimerForSavedStream(withDuration: withDuration)
    }

    func setViewersCount(count: Int) {
        attributesView.setViewers(count: count)
    }

    func toggleCommentView() {
        commentContainer.isHidden.toggle()
    }

    func updateFrames(isLandscape: Bool) {
        attributesView.isHidden = isLandscape
    }

    // MARK: - Private Methods

    private func setupMessagesViewsFrames() {
        addSubviews(commentContainer)
        commentContainer.addSubview(commentLabel)

        commentContainer.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        commentLabel.snp.makeConstraints { make in
            make.trailing.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().inset(22)
        }
    }
}

