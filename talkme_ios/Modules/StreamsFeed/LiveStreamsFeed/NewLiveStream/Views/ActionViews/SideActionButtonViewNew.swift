//
//  SideActionButtonViewNew.swift
//  talkme_ios
//
//  Created by admin on 29.09.2022.
//

import UIKit
import SnapKit
import RxSwift
import RxGesture

final class SideActionButtonViewNew: UIView {

    // MARK: - Private Properties

    private let iconView: UIImageView = {
        let iconView = UIImageView()
        iconView.contentMode = .scaleAspectFill
        return iconView
    }()

    private let blurView: UIVisualEffectView = {
        let blurVW = UIVisualEffectView()
        blurVW.backgroundColor = .black.withAlphaComponent(0.25)
        blurVW.clipsToBounds = true
        return blurVW
    }()

    let likeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .montserratFontRegular(ofSize: 13)
        return label
    }()

    private let blackCyrcleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "soundBack")
        imageView.alpha = 0.25
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: - Initializers

    init(type: StreamSideActionTypeNew) {
        super.init(frame: .zero)
        iconView.image = type.icon(isActive: true)
        iconView.tintColor = type.tintColor
        setupForRoundBackground(width: type.size.width, height: type.size.height)
        guard type != .like else { return }
        likeLabel.isHidden = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        blurView.layer.cornerRadius = blurView.frame.height / 2
    }

    // MARK: - Public Methods

    func setupRedBackground(for type: StreamSideActionTypeNew, isActive: Bool = false) {
        blurView.backgroundColor = type.background(isActive: isActive)
    }

    func setValue(_ value: String?) {
        likeLabel.text = value ?? "0"
    }

    func setupIcon(for type: StreamSideActionTypeNew, isActive: Bool = true) {
        iconView.image = type.icon(isActive: isActive)
        iconView.tintColor = type.tintColor
    }

    // MARK: - Private Methods

    private func setupForRoundBackground(width: CGFloat, height: CGFloat) {
        addSubviews(blurView, likeLabel)
        blurView.contentView.addSubview(iconView)

        blurView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        likeLabel.snp.makeConstraints { make in
            make.centerX.equalTo(blurView)
            make.top.equalTo(blurView.snp.bottom).inset(-5)
        }

        iconView.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
            make.height.equalTo(height)
            make.width.equalTo(width)
        }
    }

    private func setupLayoutForImageBackground() {
        addSubviews(blackCyrcleImageView,
                    iconView
        )

        blackCyrcleImageView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(self)
        }

        iconView.snp.makeConstraints { make in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
        }
    }
}

