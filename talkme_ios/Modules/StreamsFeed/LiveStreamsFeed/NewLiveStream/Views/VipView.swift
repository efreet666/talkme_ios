//
//  VipView.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import Foundation
import UIKit

final class VipView: UIView {

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = TalkmeColors.white
        label.font = .montserratBold(ofSize: 12)
        label.text = "vip_status".localized
        return label
    }()

    private let checkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "checkMark")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    init() {
        super.init(frame: .zero)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
    }

    func checkStatusVip(_ isVip: Bool) {
        DispatchQueue.main.async {
            let colorFromOne = UIColor(red: 23, green: 56, blue: 227, alpha: 0.9)
            let colorFromTwo = UIColor(red: 113, green: 23, blue: 227, alpha: 0.9)
            let colorToOne = UIColor.black.withAlphaComponent(0.5)
            let colorToTwo = UIColor.black.withAlphaComponent(0.5)
            self.gradientBackground(
                from: isVip ? colorFromOne : colorToOne,
                to: isVip ? colorFromTwo : colorToTwo,
                direction: .leftToRight
            )
            self.titleLabel.textColor = isVip ? TalkmeColors.white : TalkmeColors.white.withAlphaComponent(0.5)
            self.checkImageView.alpha = isVip ? 1 : 0.5
        }
    }

    private func addConstraints() {
        addSubviews(titleLabel, checkImageView)

        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(9)
            make.centerY.equalToSuperview()
        }

        checkImageView.snp.makeConstraints { make in
            make.height.equalTo(7.5)
            make.width.equalTo(13.2)
            make.centerY.equalToSuperview()
            make.leading.equalTo(titleLabel.snp.trailing).inset(-2)
        }
    }
}
