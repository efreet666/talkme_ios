//
//  GiftComplaintCollectionViewCell.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import UIKit
import Kingfisher

final class GiftComplaintCollectionViewCell: UICollectionViewCell {

    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 11
        return view
    }()

    private let glowImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = UIImage(named: "glowImage")
        img.isHidden = true
        return img
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratFontMedium(ofSize: 13)
        label.textAlignment = .center
        return label
    }()

    private let priceView = LabelAndCoinView()

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var isSelected: Bool {
        didSet {
            selectedCell(isSelected)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.containerView.gradientBackground(
                from: TalkmeColors.gradientLightPurple,
                to: TalkmeColors.gradientPurple,
                direction: .bottomToTop
            )
            self.containerView.clipsToBounds = true
        }
    }

    func configure(donat: GetPlansResponse) {
        iconImageView.kf.setImage(with: URL(string: donat.image))
        titleLabel.text = donat.title ?? ""
        priceView.configure(viewType: .streamDonatPrice(price: "\(donat.cost)"))
    }

    private func selectedCell(_ isSelected: Bool) {
            containerView.layer.borderWidth = 2
            containerView.layer.borderColor = isSelected ? TalkmeColors.pinkBorder.cgColor : UIColor.clear.cgColor
            glowImageView.isHidden = !isSelected
            removeGradientLayer(containerView)
            let colorOne = UIColor(red: 255, green: 84, blue: 84, alpha: 0.8)
            let colorTwo = UIColor(red: 68, green: 38, blue: 207, alpha: 0.9)
            DispatchQueue.main.async {
                self.containerView.gradientBackground(
                    from: self.isSelected ? colorTwo : TalkmeColors.gradientLightPurple,
                    to: self.isSelected ? colorOne : TalkmeColors.gradientPurple,
                    direction: .topToBottom
                )
            }
        }

        private func removeGradientLayer(_ view: UIView) {
            guard let layers = view.layer.sublayers else { return }
            for aLayer in layers where aLayer is CAGradientLayer {
                aLayer.removeFromSuperlayer()
            }
        }

    private func addConstraints() {
        contentView.addSubviews(containerView, iconImageView)
        containerView.addSubviews(glowImageView, priceView, titleLabel)

        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        iconImageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(-12)
            make.bottom.equalTo(titleLabel.snp.top).offset(4)
        }

        glowImageView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.top.equalToSuperview().offset(-12)
            make.bottom.equalTo(titleLabel).inset(-4)
            make.height.equalTo(104)
        }

        titleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(priceView.snp.top).inset(-8)
            make.centerX.equalToSuperview()
        }

        priceView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(16)
            make.height.equalTo(23)
        }
    }
}

