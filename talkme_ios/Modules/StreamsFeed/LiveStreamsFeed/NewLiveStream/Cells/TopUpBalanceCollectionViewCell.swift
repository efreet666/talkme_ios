//
//  TopUpBalanceCollectionViewCell.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import UIKit
import SnapKit
import Kingfisher

final class TopUpBalanceCollectionViewCell: UICollectionViewCell {

    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 11
        view.backgroundColor = UIColor(red: 3, green: 14, blue: 22, alpha: 0.4)
        return view
    }()

    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratSemiBold(ofSize: 16)
        label.textAlignment = .center
        label.backgroundColor = .black
        label.layer.cornerRadius = 3
        label.clipsToBounds = true
        return label
    }()

    private let priceView = AmountCountView(frame: .zero)

    override init(frame: CGRect) {
        super.init(frame: frame)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var isSelected: Bool {
        didSet {
            containerView.layer.borderWidth = 2
            containerView.layer.borderColor = isSelected ? TalkmeColors.blueBorderColor.cgColor : UIColor.clear.cgColor
        }
    }

    func configure(donat: GetPlansResponse) {
        iconImageView.kf.setImage(with: URL(string: donat.image))
        guard let purchaseId = donat.iosInAppId else { return }
        priceLabel.text = IAPManager.shared.price(for: String(purchaseId))
        priceView.setValue(coin: donat.iosCoinAmount ?? 0)
    }

    private func addConstraints() {
        contentView.addSubviews(containerView)
        containerView.addSubviews(iconImageView, priceLabel, priceView)

        containerView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(16)
        }

        iconImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(14)
            make.centerY.equalToSuperview()
            make.width.equalTo(70.5)
            make.height.equalTo(74.3)
        }

        priceLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(19)
            make.height.equalTo(31)
            make.width.equalTo(78)
        }

        priceView.snp.makeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).inset(-14.2)
            make.centerY.equalTo(iconImageView)
            make.top.bottom.equalToSuperview().inset(19)
        }
    }
}

