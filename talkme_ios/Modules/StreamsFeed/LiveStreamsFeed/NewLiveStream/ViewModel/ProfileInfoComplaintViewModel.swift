//
//  ProfileInfoComplaintViewModel.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import Foundation
import RxSwift
import RxCocoa

final class ProfileInfoComplaintViewModel {

    enum Flow {
        case dissmissPopupWithSuccess
        case dissmissPopupWithError
        case showChatVC(_ id: Int)
        case showProfileVC(_ classNumber: String)
    }

    let flow = PublishRelay<Flow>()
    private(set) var infoStream: PublicProfileResponse?
    private(set) var isContactRelay = BehaviorRelay<Bool>(value: false)
    private let bag = DisposeBag()
    private let accountService = AccountService()

    func addContact(id: Int) {
        accountService
            .addContact(id: id)
            .subscribe { event in
                switch event {
                case .success(let responce):
                    print(responce)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func removeContact(id: Int) {
        accountService
            .removeContact(id: id)
            .subscribe { event in
                switch event {
                case .success(let responce):
                    print(responce)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func fetchData(classNumber: String) {
        accountService.streamPublicProfile(classNumber: classNumber)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let info):
                    self.isContactRelay.accept(info.isContact)
                    self.infoStream = info
                case .error(let error):
                    print(error.localizedDescription)
                }
            }
            .disposed(by: bag)
    }

}

