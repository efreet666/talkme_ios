//
//  GiftComplaintViewModel.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay
import CollectionKit

final class GiftComplaintViewModel {

    enum Flow {
        case dismiss
        case showTopUpBalancePopUp
    }

    // MARK: - Public Properties

    private(set) var dataItems = PublishRelay<[GiftComplaintCellViewModel]>()

    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()
    let myBalance = PublishRelay<Int?>()
    var copyResponse: GetPlansResponse?
    let lessonId: Int
    var cost = 0

    // MARK: - Private Properties

    private let service: LessonsServiceProtocol
    private let paymentService = PaymentsService()
    private var timer: Timer?

    // MARK: - Initializers

    init(service: LessonsServiceProtocol, lessonId: Int) {
        self.service = service
        self.lessonId = lessonId
        getCurrentBalance()
        setupTimer()
        getPaymentsTypesData()
    }

    // MARK: - Public Methods

    func sendTips(tips: Int) {
        service
            .tips(tips: tips, lessonId: lessonId, lessonIsSaved: false)
            .subscribe()
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    func getPaymentsTypesData() {
        paymentService.getGifts()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    let items: [GiftComplaintCellViewModel] = response.sorted { $0.cost < $1.cost }.map { GiftComplaintCellViewModel(donat: $0) }
                    self.dataItems.accept(items)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func makeGift(donatID: Int) {
        let request = MakeGiftRequest(gift: donatID, lesson: lessonId, saved: false)
        paymentService
            .makeGift(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.getCurrentBalance()
                    self?.flow.accept(.dismiss)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func setupTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.getCurrentBalance()
        }
    }

    private func getCurrentBalance() {
        paymentService
            .getCoin()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.myBalance.accept(response.coin)
                    self.cost = response.coin
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
}

