//
//  ThankComplaintViewModel.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import Foundation
import RxSwift
import RxCocoa

final class ThankComplaintViewModel {

    enum Flow {
        case dismiss
        case showTopUpBalancePopUp
    }

    // MARK: - Public Properties

    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()
    let myBalance = BehaviorRelay<Int?>(value: 0)
    let lessonId: Int

    // MARK: - Private Properties

    private let service: LessonsServiceProtocol
    private let paymentService = PaymentsService()
    private var timer: Timer?

    // MARK: - Initializers

    init(service: LessonsServiceProtocol, lessonId: Int) {
        self.service = service
        self.lessonId = lessonId
        getCurrentBalance()
        setupTimer()
    }

    // MARK: - Public Methods

    func sendTips(tips: Int) {
        service
            .tips(tips: tips, lessonId: lessonId, lessonIsSaved: false)
            .subscribe()
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func setupTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.getCurrentBalance()
        }
    }

    private func getCurrentBalance() {
        paymentService
            .getCoin()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.myBalance.accept(response.coin)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
}

