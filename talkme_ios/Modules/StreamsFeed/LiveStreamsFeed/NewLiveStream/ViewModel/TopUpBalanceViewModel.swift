//
//  TopUpBalanceViewModel.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import Foundation
import RxSwift
import RxCocoa

final class TopUpBalanceViewModel {

    enum Flow {
        case dismiss
    }

    // MARK: - Public Properties

    var dataItems = PublishRelay<[TopUpBalanceCellViewModel]>()
    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()
    let myBalance = PublishRelay<Int?>()
    let lessonId: Int
    var selectedID: String?

    // MARK: - Private Properties

    private let service: LessonsServiceProtocol
    private let paymentService = PaymentsService()
    private var timer: Timer?

    // MARK: - Initializers

    init(service: LessonsServiceProtocol, lessonId: Int) {
        self.service = service
        self.lessonId = lessonId
        getPurchases()
        getCurrentBalance()
        setupTimer()
    }

    // MARK: - Public Methods

    func sendTips(tips: Int) {
        service
            .tips(tips: tips, lessonId: lessonId, lessonIsSaved: false)
            .subscribe()
            .disposed(by: bag)
    }

    func getPurchases() {
        paymentService.getPlan()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    IAPManager.shared.purchases = response
                    var inAppProducts = [String]()
                    response.forEach { plan in
                        guard let planId = plan.iosInAppId else { return }
                        inAppProducts.append(String(planId))
                    }
                    IAPManager.shared.setupPurchases { success in
                        guard success else { return }
                        IAPManager.shared.getProducts(identifiers: inAppProducts)
                        let items: [TopUpBalanceCellViewModel] = response.map { TopUpBalanceCellViewModel(donat: $0) }
                        self.dataItems.accept(items)
                    }
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func setupTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.getCurrentBalance()
        }
    }

    private func getCurrentBalance() {
        paymentService
            .getCoin()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.myBalance.accept(response.coin)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
}

