//
//  NotificationSubscription.swift
//  talkme_ios
//
//  Created by admin on 03.10.2022.
//

import Foundation
import UIKit
import SnapKit

final class NotificationSubscription {

    enum NotificationType {
        case subscription

        var title: String {
            switch self {
            case .subscription:
                return "Вы подписались".localized
            }
        }

        var color: UIColor {
            switch self {
            case .subscription:
                return .green.withAlphaComponent(0.8)
            }
        }
    }

    static let shared = NotificationSubscription()

    func showNotification(_ type: NotificationType) {
        let titleLB = UILabel()
        titleLB.text = type.title
        titleLB.textAlignment = .left
        titleLB.textColor = .white
        titleLB.font = .montserratSemiBold(ofSize: 15)

        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named: "chek")

        let containerVW = UIView()
        containerVW.alpha = 0
        containerVW.layer.cornerRadius = 7
        containerVW.clipsToBounds = true

        DispatchQueue.main.async {
            containerVW.layoutIfNeeded()
            containerVW.gradientBackground(
                from: TalkmeColors.gradientLightGreen.withAlphaComponent(0.8),
                to: TalkmeColors.gradientGreen.withAlphaComponent(0.8),
                direction: .bottomToTop
            )
        }

        guard let viewController = UIApplication.shared.windows.last?.rootViewController else { return }
        viewController.view.addSubview(containerVW)
        containerVW.addSubviews(titleLB, image)

        containerVW.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(UIScreen.height / 25)
            make.width.equalTo(219)
            make.height.equalTo(49)
        }

        image.snp.makeConstraints { make in
            make.size.equalTo(24)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(24)
        }

        titleLB.snp.makeConstraints { make in
            make.leading.equalTo(image.snp.trailing).inset(-10)
            make.trailing.equalToSuperview()
            make.centerY.equalTo(image)
        }

        UIView.animate(withDuration: 1, delay: 0.1) {
            containerVW.alpha = 1
        } completion: { _ in
            UIView.animate(withDuration: 1, delay: 0.1) {
                containerVW.alpha = 0
            } completion: { _ in
                self.removeGradientLayer(containerVW)
                self.removeGradientLayer(titleLB)
            }
        }
    }

    private func removeGradientLayer(_ view: UIView) {
        guard let layers = view.layer.sublayers else { return }
        for aLayer in layers where aLayer is CAGradientLayer {
            aLayer.removeFromSuperlayer()
        }
    }
}

