//
//  TopUpBalanceCellViewModel.swift
//  talkme_ios
//
//  Created by admin on 05.10.2022.
//

import Foundation
import RxRelay
import RxCocoa
import RxSwift

final class TopUpBalanceCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let donat: GetPlansResponse

    // MARK: - Initializers

    init(donat: GetPlansResponse) {
        self.donat = donat
    }

    // MARK: - Public Methods

    func configure(_ cell: TopUpBalanceCollectionViewCell) {
        cell.configure(donat: donat)
    }
}

