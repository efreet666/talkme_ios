//
//  StreamSideActionTypeNew.swift
//  talkme_ios
//
//  Created by admin on 29.09.2022.
//

import UIKit

enum StreamSideActionTypeNew {
    case like
    case share
    case coin
    case gift
    case viewers
    case camera
    case microphone
    case microphoneOwner
    case flipCamera
    case complaintBlack
    case mute
    case fullScreen

    var tintColor: UIColor {
        return .clear
    }

    var size: CGSize {
        switch self {
        case .like:
            return CGSize(width: 19, height: 18)
        case .share:
            return CGSize(width: 18, height: 15)
        case .coin:
            return CGSize(width: 18, height: 18)
        case .gift:
            return CGSize(width: 16, height: 20)
        case .microphoneOwner, .microphone:
            return CGSize(width: 22, height: 22)
        case .camera, .viewers, .flipCamera, .complaintBlack, .mute, .fullScreen:
            return CGSize(width: 25, height: 25)
        }
    }

    func icon(isActive: Bool) -> UIImage? {
        switch self {
        case .like:
            return UIImage(named: "like")
        case .share:
            return UIImage(named: "shareStream")
        case .coin:
            return UIImage(named: "coin")
        case .gift:
            return UIImage(named: "streamGift")
        case .viewers:
            return UIImage(named: "streamViewers")
        case .camera:
            return isActive ? UIImage(named: "streamVideo") : UIImage(named: "streamVideoOff")
        case .microphone:
            return isActive ? UIImage(named: "speakerOn") : UIImage(named: "speakerOff")
        case .microphoneOwner:
            return isActive ? UIImage(named: "microOnWhite") : UIImage(named: "streamMicOff")
        case .flipCamera:
            return UIImage(named: "flipCamera")
        case .complaintBlack:
            return UIImage(named: "reportBlack")
        case .mute:
            return isActive ? UIImage(named: "SoundOff") : UIImage(named: "SoundOn")
        case .fullScreen:
            return UIImage(named: "filled")
        }
    }

    func background(isActive: Bool) -> UIColor {
        switch self {
        case .like:
            return isActive ? TalkmeColors.likeButton : .black.withAlphaComponent(0.25)
        case
            .share,
            .coin,
            .gift,
            .viewers,
            .camera,
            .microphone,
            .microphoneOwner,
            .flipCamera,
            .complaintBlack,
            .mute,
            .fullScreen:
            return .black
        }
    }
}

