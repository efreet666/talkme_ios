//
//  LimitsForStreamMemberVC.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.04.2021.
//

import UIKit
import RxSwift

final class LimitsForStreamMemberVC: UIViewController {

    // MARK: - Private Properties

    private let limitsForStreamMemberView = LimitsForStreamMemberView()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        view.addSubview(limitsForStreamMemberView)
        limitsForStreamMemberView.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
        }
    }
}
