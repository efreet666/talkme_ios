//
//  VipLessonPopupView.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 23.08.2021.
//

import RxCocoa
import RxSwift

final class VipLessonPopupView: UIView {

    enum Flow {
        case buyAndDismiss
        case dismiss
    }

    // MARK: - Public Properties

    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    let lesson: LessonDetailResponse

    // MARK: - Private Properties

    private lazy var dummyStream = DummyScreenOnPaidAir(
        frame: .zero,
        lesson: lesson,
        categoryImage: categoryImage
    )
    private let categoryImage: UIImage?

    // MARK: - Initializers

    init(lesson: LessonDetailResponse, categoryImage: UIImage?) {
        self.lesson = lesson
        self.categoryImage = categoryImage
        super.init(frame: .zero)
        setupView()
        setupLayout()
        bind()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.cornerRadius = UIScreen.isSE ? 20 : 30
        return view
    }()

    private let dismissButtonImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "dismissButton"))
        iw.contentMode = .scaleAspectFill
        return iw
    }()

    private func setupLayout() {
        containerView.addSubviews(dummyStream)
        addSubviews(containerView, dismissButtonImageView)
        dummyStream.layer.cornerRadius = containerView.layer.cornerRadius
        dummyStream.snp.makeConstraints { make in
            make.edges.equalTo(containerView)
        }

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().offset(UIScreen.isSE ? 20 : 24)
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -20 : -24)
            make.top.equalToSuperview().offset(UIScreen.isSE ? 33 : 63)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 33 : 33)
        }

        dismissButtonImageView.snp.makeConstraints { make in
            make.height.width.equalTo(20)
            make.bottom.equalTo(containerView.snp.top)
            make.left.equalToSuperview().offset(12)
        }
    }

    private func setupView() {
        backgroundColor = TalkmeColors.popupBackground
        alpha = 5
    }

    private func bind() {
        dummyStream
            .toBuyButtonTapped
            .bind { [weak self] _ in
                self?.flow.accept(.buyAndDismiss)
            }
            .disposed(by: bag)

        dismissButtonImageView.rx.tapGesture().when(.recognized)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.dismissButtonImageView.isHidden = true
                self.flow.accept(.dismiss)
            }
            .disposed(by: bag)

    }
}
