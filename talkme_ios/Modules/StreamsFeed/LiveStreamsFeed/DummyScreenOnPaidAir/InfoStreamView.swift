//
//  InfoStreamView.swift
//  talkme_ios
//
//  Created by Vladislav on 27.07.2021.
//

import UIKit

enum InfoStreamViewType {

    case category
    case numberOfParticipants
    case streamDuration

    var icon: UIImage? {
        switch self {
        case .category:
            return UIImage(named: "fitness")
        case .numberOfParticipants:
            return UIImage(named: "numberOfParticipants")
        case .streamDuration:
            return UIImage(named: "lessonDuration")
        }
    }

    var title: String {
        switch self {
        case .category:
            return "lesson_attribute_category".localized
        case .numberOfParticipants:
            return "create_lesson_members_number".localized
        case .streamDuration:
            return "lesson_attribute_duration".localized
        }
    }

    var content: String {
        switch self {
        case .category:
            return "Развлечение"
        case .numberOfParticipants:
            return "1000"
        case .streamDuration:
            return "1ч 30мин"
        }

    }
}

final class InfoStreamView: UIView {

    // MARK: - Private Properties

    private let container = UIView()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        lbl.font = .montserratFontMedium(ofSize: 14)
        lbl.textColor = TalkmeColors.white
        return lbl
    }()

    private let contentLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratBold(ofSize: 15)
        return lbl
    }()

    private var categoryImage: UIImageView = {
        let img = UIImageView()
        img.clipsToBounds = true
        img.layer.cornerRadius = UIScreen.isSE ? 21 : 25
        return img
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        container.layer.cornerRadius = container.bounds.height / 2
    }

    // MARK: - Initializers

    init(frame: CGRect, viewType: InfoStreamViewType) {
        super.init(frame: frame)
        setContainerConstraints()
        titleLabel.text = viewType.title
        contentLabel.text = viewType.content
        categoryImage.image = viewType.icon
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setContainerConstraints() {
        addSubviews(container)
        container.addSubviewsWithoutAutoresizing(categoryImage, titleLabel, contentLabel)

        container.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 50 : 56)

            titleLabel.snp.makeConstraints { make in
                make.top.equalTo(categoryImage.snp.top).offset(5)
                make.leading.equalTo(categoryImage.snp.trailing).offset(10)
            }

            categoryImage.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(10)
                make.leading.equalToSuperview()
                make.height.width.equalTo(UIScreen.isSE ? 44 : 50)
            }

            contentLabel.snp.makeConstraints { make in
                make.top.equalTo(titleLabel.snp.bottom).offset(2)
                make.leading.equalTo(categoryImage.snp.trailing).offset(10)

            }
        }
    }

    func configure(text: String, image: UIImage) {
        self.contentLabel.text = text.localized
        self.categoryImage.image = image
    }
}
