//
//  DummyScreenOnPaidAir.swift
//  talkme_ios
//
//  Created by Vladislav on 26.07.2021.
//

import UIKit

final class DummyScreenOnPaidAir: UIView {

    // MARK: - Public Properties

    private (set) lazy var toBuyButtonTapped = stackViewButton.rx.tapGesture().when(.recognized)

    // MARK: - Private Properties

    private let infoCategoryView = InfoStreamView(frame: .zero, viewType: .category)
    private let numberOfParticipants = InfoStreamView(frame: .zero, viewType: .numberOfParticipants)
    private let streamDuration = InfoStreamView(frame: .zero, viewType: .streamDuration)

    private let avatarImage: UIImageView = {
        let img = UIImageView()
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.layer.cornerRadius = UIScreen.isSE ? 21 : 25
        return img
    }()

    private let infoTeacherView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.streamPlugScreen
        return view
    }()

    private let nameLabel: UILabel = {
        let lbl = UILabel()
        let attributedString = NSMutableAttributedString(string: lbl.text ?? " ")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
                                      value: paragraphStyle,
                                      range: NSMakeRange(0, attributedString.length))
        lbl.attributedText = attributedString
        lbl.textAlignment = .left
        lbl.textColor = TalkmeColors.white
        lbl.lineBreakMode = .byTruncatingTail
        lbl.numberOfLines = 1
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.75
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        return lbl
    }()

    private let secondNameLabel: UILabel = {
        let lbl = UILabel()
        let attributedString = NSMutableAttributedString(string: lbl.text ?? " ")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        lbl.attributedText = attributedString
        lbl.textAlignment = .left
        lbl.textColor = TalkmeColors.white
        lbl.lineBreakMode = .byTruncatingTail
        lbl.numberOfLines = 1
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.75
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        return lbl
    }()

    private let classLabel: ClassNumberLabel = {
        let label = ClassNumberLabel()
        label.configure(type: .navBar)
        return label
    }()

    private let nameStreamLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = TalkmeColors.white
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 16 : 18)
        return lbl
    }()

    private let streamView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.blueLabels
        return view
    }()

    private let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "mainBackground")
        image.contentMode = UIView.ContentMode.scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    private let aboutStreamTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = TalkmeColors.white
        lbl.text = "lesson_description_title".localized
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 16 : 18)
        return lbl
    }()

    private let aboutStreamLabel: UILabel = {
        let lbl = UILabel()
        let attributedString = NSMutableAttributedString(
            string: lbl.text ?? " "
        )
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
                                      value: paragraphStyle,
                                      range: NSMakeRange(0, attributedString.length))
        lbl.attributedText = attributedString
        lbl.textAlignment = .left
        lbl.textColor = TalkmeColors.white
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 10
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        return lbl
    }()

    private let titleLbButton: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        label.text = "lesson_button_title_pay".localized
        label.textColor = TalkmeColors.white
        return label
    }()

    private let countLbButton: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.textColor = TalkmeColors.white
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 18 : 20)
        return label
    }()

    private let imageViewButton: UIImageView = {
        let iw = UIImageView()
        iw.image = UIImage(named: "coin")
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let stackViewButton: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.clipsToBounds = true
        sv.backgroundColor = TalkmeColors.greenLabels
        sv.layer.cornerRadius = 12
        sv.distribution = .fillProportionally
        return sv
    }()

    // MARK: - Initializers
    init(frame: CGRect, lesson: LessonDetailResponse, categoryImage: UIImage?) {
        countLbButton.text = String(lesson.cost)
        if  let avatarIMG = lesson.owner.avatarUrl, !avatarIMG.isEmpty {
            avatarImage.kf.setImage(with: URL(string: avatarIMG))
        } else {
            avatarImage.image = UIImage(named: "noAvatar")
        }
        aboutStreamLabel.text = lesson.description
        nameLabel.text = lesson.owner.firstName ?? ""
        secondNameLabel.text = lesson.owner.lastName ?? ""
        nameStreamLabel.text = lesson.name
        classLabel.text = "№" + " " + (lesson.owner.numberClass ?? "0")
        infoCategoryView.configure(text: lesson.categoryName, image: categoryImage ?? #imageLiteral(resourceName: "allCatWhite"))
        numberOfParticipants.configure(text: String(lesson.numberOfParticipants), image: #imageLiteral(resourceName: "numberOfParticipants"))
        streamDuration.configure(text: lesson.lessonTime, image: #imageLiteral(resourceName: "lessonDuration"))
        super.init(frame: frame)
        clipsToBounds = true
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([backgroundImage,
                     infoTeacherView,
                     avatarImage,
                     nameLabel,
                     secondNameLabel,
                     nameStreamLabel,
                     infoCategoryView,
                     numberOfParticipants,
                     streamDuration,
                     aboutStreamTitleLabel,
                     aboutStreamLabel,
                     classLabel,
                     stackViewButton
                    ])
        infoTeacherView.addSubviews([avatarImage, nameLabel, secondNameLabel, classLabel])
        stackViewButton.addSubviewsWithoutAutoresizing(titleLbButton, imageViewButton, countLbButton)

        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        infoTeacherView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 84 : 100)
        }

        classLabel.snp.makeConstraints { make in
            make.top.equalTo(infoTeacherView.snp.top).offset(UIScreen.isSE ? 32 : 36)
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -13 : -9)
            make.width.equalTo(UIScreen.isSE ? 82 : 90)
            make.height.equalTo(UIScreen.isSE ? 22 : 26)
        }

        avatarImage.snp.makeConstraints { make in
            make.top.equalTo(infoTeacherView.snp.top).offset(UIScreen.isSE ? 20 : 24)
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 20 : 14)
            make.height.width.equalTo(UIScreen.isSE ? 42 : 50)
        }

        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(infoTeacherView.snp.top).offset(UIScreen.isSE ? 24 : 28)
            make.leading.equalTo(avatarImage.snp.trailing).offset(UIScreen.isSE ? 8 : 10)
            make.trailing.equalTo(classLabel.snp.leading).offset(-4)
            make.width.equalTo(UIScreen.isSE ? 88 : 120)
        }

        secondNameLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(UIScreen.isSE ? 4 : 6)
            make.leading.equalTo(avatarImage.snp.trailing).offset(UIScreen.isSE ? 8 : 10)
            make.trailing.equalTo(classLabel.snp.leading).offset(-4)
            make.width.equalTo(UIScreen.isSE ? 88 : 120)
        }

        nameStreamLabel.snp.makeConstraints { make in
            make.top.equalTo(infoTeacherView.snp.bottom).offset(22)
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 14 : 24)
            make.trailing.equalToSuperview().offset(-22)
        }

        infoCategoryView.snp.makeConstraints { make in
            make.top.equalTo(nameStreamLabel.snp.bottom).offset(UIScreen.isSE ? 20 : 22)
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 13 : 24)
            make.height.equalTo(UIScreen.isSE ? 48 : 54)
        }

        numberOfParticipants.snp.makeConstraints { make in
            make.top.equalTo(infoCategoryView.snp.bottom).offset(UIScreen.isSE ? 8 : 10)
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 13 : 24)
            make.height.equalTo(UIScreen.isSE ? 48 : 54)
        }

        streamDuration.snp.makeConstraints { make in
            make.top.equalTo(numberOfParticipants.snp.bottom).offset(UIScreen.isSE ? 8 : 10)
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 13 : 24)
            make.height.equalTo(UIScreen.isSE ? 48 : 54)
        }

        aboutStreamTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(streamDuration.snp.bottom).offset(UIScreen.isSE ? 20 : 22)
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 14 : 24)
        }

        aboutStreamLabel.snp.makeConstraints { make in
            make.top.equalTo(aboutStreamTitleLabel.snp.bottom).offset(UIScreen.isSE ? 12 : 8)
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 14 : 24)
            make.trailing.equalToSuperview().offset(-15)
        }

        stackViewButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 22 : 24)
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -22 : -24)
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -24 : -27)
            make.height.equalTo(54)
        }

        imageViewButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -16 : -18)
            make.centerY.equalTo(stackViewButton.snp.centerY)
            make.width.height.equalTo(UIScreen.isSE ? 18 : 20)
        }

        titleLbButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 18 : 22)
            make.centerY.equalTo(countLbButton.snp.centerY)
            make.height.equalTo(UIScreen.isSE ? 22 : 54)
            make.trailing.equalTo(countLbButton.snp.leading)
        }

        countLbButton.snp.makeConstraints { make in
            make.centerY.equalTo(stackViewButton.snp.centerY)
            make.height.equalTo(22)
            make.trailing.equalTo(imageViewButton.snp.leading).offset(-6)
        }
    }
}
