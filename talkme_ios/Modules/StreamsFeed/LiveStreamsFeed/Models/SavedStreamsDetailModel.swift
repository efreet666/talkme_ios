//
//  SavedStreamsDetailModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 10/7/2022.
//

struct SavedStreamsDetailModel {
    let allStreamsShort: [SavedStreamShort]
    let currentStream: SavedStreamShort
    let index: Int
}
