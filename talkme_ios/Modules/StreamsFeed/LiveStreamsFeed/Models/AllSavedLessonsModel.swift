//
//  AllSavedStreemsModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 28/6/2022.
//

import RxSwift

struct AllSavedStreamLessonsModel {

    static var savedLessonsDetails = [LessonDetailResponse]().uniques
    static var allSavedLessonsData = [LiveStream]()
    static var userSavedLessonsData = [LiveStream]()
    static let lessonsUpdate = PublishSubject<Void>()

    static func updateSavedLessonsSet(lessonService: LessonsServiceProtocol, bag: DisposeBag, lessonId: Int) {
        Self.savedLessonsDetails.enumerated().forEach { index, lesson in
            if lesson.id == lessonId {
                Self.savedLessonsDetails.remove(at: index)
            }
        }

        lessonService.lessonDetail(id: lessonId, saved: true)
            .subscribe { event in
                switch event {
                case .success(let responce):
                    Self.savedLessonsDetails.append(responce)
                    Self.lessonsUpdate.onNext(())
                case .error(let error):
                    print("AllSavedLessonsModel updateSavedLessonsSet error: \(error)")
                }
            }
            .disposed(by: bag)
    }
}
