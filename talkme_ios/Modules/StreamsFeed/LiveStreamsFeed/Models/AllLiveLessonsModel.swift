//
//  AllLiveLessonsModel.swift
//  talkme_ios
//
//  Created by 1111 on 09.07.2021.
//

import RxSwift

struct AllLiveLessonsModel {

    static var liveLessonsArray = [LessonDetailResponse]().uniques
    static let lessonsUpdate = PublishSubject<Void>()

    static func updateLiveLessonsSet(lessonService: LessonsServiceProtocol, bag: DisposeBag, lessonId: Int) {
        Self.liveLessonsArray.enumerated().forEach { index, lesson in
            if lesson.id == lessonId {
                Self.liveLessonsArray.remove(at: index)
            }
        }

        lessonService.lessonDetail(id: lessonId, saved: false)
            .subscribe { event in
                switch event {
                case .success(let responce):
                    Self.liveLessonsArray.append(responce)
                    Self.lessonsUpdate.onNext(())
                case .error(let error):
                    print("AllLiveLessonsModel updateLiveLessonsSet error: \(error)")
                }
            }
            .disposed(by: bag)
    }
}

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
