//
//  LearnerPublisherModel.swift
//  talkme_ios
//
//  Created by 1111 on 07.04.2021.
//

import OpenTok

struct LearnerPublisher {
    let publisher: OTPublisher?
    let timeEnd: Double
}
