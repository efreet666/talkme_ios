//
//  SubscriberForPopup.swift
//  talkme_ios
//
//  Created by 1111 on 17.04.2021.
//

import OpenTok

struct PopupData {
    let subscriberForPopup: SubscriberForPopup
    let membersCount: String
}

struct SubscriberForPopup: Equatable {
    var subscribersArray: [CurrentSubscriber]
    let session: OTSession?
    let isOwner: Bool
}

struct CurrentSubscriber: Equatable {
    let subscriber: OTSubscriber
    let timeEnd: Double
    let avatarUrl: String
    let blockedAudioByPub: Bool
    let blockedVideoByPub: Bool
    let isMicroOn: Bool?
    let subscriberHasVideo: Bool?
}

struct AlteredStream {
    let subscriber: OTSubscriber
    var blockedAudioByPub = false
    var blockedVideoByPub = false
    let isMicroOn: Bool?
    let subscriberHasVideo: Bool?
}
