//
//  StreamSideActionType.swift
//  talkme_ios
//
//  Created by Elina Efremova on 08.03.2021.
//

import UIKit

enum StreamSideActionType {
    case gift
    case viewers
    case camera
    case microphone
    case message
    case flipCamera
    case complaint
    case complaintBlack
    case mute
    case fullScreen

    func icon(isActive: Bool) -> UIImage? {
        switch self {
        case .gift:
            return UIImage(named: "streamGift")
        case .viewers:
            return UIImage(named: "streamViewers")
        case .camera:
            return isActive ? UIImage(named: "streamVideo") : UIImage(named: "streamVideoOff")
        case .microphone:
            return isActive ? UIImage(named: "streamMicOn") : UIImage(named: "streamMicOff")
        case .message:
            return UIImage(named: "streamMessage")
        case .flipCamera:
            return UIImage(named: "flipCamera")
        case .complaint:
            return UIImage(named: "reportWhite")
        case .complaintBlack:
            return UIImage(named: "reportBlack")
        case .mute:
            return isActive ? UIImage(named: "SoundOff") : UIImage(named: "SoundOn")
        case .fullScreen:
            return UIImage(named: "filled")
        }
    }

    var tintColor: UIColor {
        switch self {
        case .microphone:
            return TalkmeColors.microRed
        default:
            return  .white
        }
    }
}
