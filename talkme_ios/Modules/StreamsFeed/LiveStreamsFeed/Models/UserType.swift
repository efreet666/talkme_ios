//
//  UserType.swift
//  talkme_ios
//
//  Created by Elina Efremova on 15.03.2021.
//

enum UserType {
    case specialist
    case participant
    case viewer
}
