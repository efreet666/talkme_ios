//
//  LikeModel.swift
//  talkme_ios
//
//  Created by admin on 06.10.2022.
//

import Foundation

struct LikeModel: Codable, Hashable {
    let likes: Int
}
