//
//  StreamdDetailModel.swift
//  talkme_ios
//
//  Created by Elina Efremova on 23.03.2021.
//

struct StreamdDetailModel {
    let allStreamsShort: [LiveStreamShort]
    let currentStream: LiveStreamShort
    let index: Int
}
