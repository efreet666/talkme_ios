//
//  StreamCoordinator.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 15.03.2021.
//

import RxSwift
import RxCocoa
import OpenTok

final class StreamCoordinator {

    enum Flow {
        case exitToRoot
        case exitToPreviousScreen(animated: Bool = true)
        case toChat(Int)
        case makeComplaint(Int)
    }

    // MARK: - Public properties

    let flow = PublishRelay<Flow>()
    let onReloadStreamsList = PublishRelay<Void>()
    let dissmissVipLessonPopup = PublishRelay<Void>()
    let removeViewFromSuperviewTeacher = PublishRelay<Void>()
    let createChatProfileTeacher = PublishRelay<Int>()
    let bag = DisposeBag()
    let sendGift = PublishRelay<Int>()
    let onUpdateStreamInfo = PublishRelay<Int>()
    let onPresentPurchasesPopup = PublishRelay<Void>()
    weak var navigationController: BaseNavigationController?
    let updateAfterClose = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let accountService = AccountService()
    private let lessonService = LessonsService()
    private let mainService = MainService()
    private let paymentsService = PaymentsService()
    private let gcoreService = GcoreService()
    private var vipLessonPopupView: VipLessonPopupView?
    private var vipLessonPopupForSavedStreamsView: VipLessonPopupViewForSavedStreams?
    private var donateVC: DonationPopupViewController?
    private var savedStreamsDonateVC: SavedStreamsDonationPopupViewController?

    // MARK: - Public Methods

    @discardableResult
    func start(_ navigationController: BaseNavigationController?,
               lessonId: Int?,
               lessonsInfo: [LiveStream] = [],
               countOfLoadedPages: Int = 1,
               numberOfStreamsInPage: Int,
               broadcastsList: MyBroadcastsResponse? = nil,
               watchSave: Bool = false,
               watchStreamsCreatedBy userId: Int? = nil,
               withCategories categoriesIds: [Int] = [0]) -> UINavigationController {
        self.navigationController = navigationController
        if watchSave {
            let streamModel = SavedStreamsFeedViewModel(lessonService: lessonService,
                                                        gcoreService: gcoreService,
                                                        lessonId: lessonId,
                                                        streamsList: lessonsInfo,
                                                        numberOfStreamsInPage: numberOfStreamsInPage,
                                                        isWatchingSave: watchSave,
                                                        streamsCreatorId: userId,
                                                        streamsCategoriesIds: categoriesIds)

            let streamController = SavedStreamsFeedViewController(viewModel: streamModel, watchSave: watchSave)
//            streamController.loadViewIfNeeded()
            bindSavedtreamModel(streamModel)

            self.removeViewFromSuperviewTeacher
                .bind { [weak streamController] _ in
                    streamController?.removeViewFromSuperviewTeacher.accept(())
                }
                .disposed(by: bag)

            if let navigationController = navigationController {
                self.navigationController = navigationController
                navigationController.pushViewController(streamController, animated: true)
                return navigationController
            } else {
                let navController = BaseNavigationController(rootViewController: streamController)
                self.navigationController = navController
                return navController
            }
        } else {
            let streamModel = StreamsFeedViewModelNew(lessonService: lessonService,
                                                   gcoreService: gcoreService,
                                                   lessonId: lessonId,
                                                   streamsList: lessonsInfo,
                                                   numberOfStreamsInPage: numberOfStreamsInPage,
                                                   broadcastsList: broadcastsList,
                                                   isWatchingSave: watchSave,
                                                   streamsCreatorId: userId,
                                                   streamsCategoriesIds: categoriesIds)
            let streamController = StreamsFeedViewControllerNew(viewModel: streamModel)
            bindStreamModel(streamModel)

            self.removeViewFromSuperviewTeacher
                .bind { [weak streamController] _ in
                    streamController?.removeViewFromSuperviewTeacher.accept(())
                }
                .disposed(by: bag)

            if let navigationController = navigationController {
                self.navigationController = navigationController
                navigationController.pushViewController(streamController, animated: true)
                return navigationController
            } else {
                let navController = BaseNavigationController(rootViewController: streamController)
                self.navigationController = navController
                return navController
            }
        }
    }

    // MARK: - Private Methods

    private func bindStreamModel(_ model: StreamsFeedViewModelNew) {

        model
            .flow
            .bind { [weak self, weak model] event in
                guard let self = self, let model = model else { return }
                switch event {
                case .onBack(let animated):
                    self.flow.accept(.exitToPreviousScreen(animated: animated))
                    self.removeViewFromSuperviewTeacher.accept(())
                case .onPopUp(let page, let classNumber, let lessonId, let isOwner, let service):
                    self.showPopUp(
                        on: page,
                        classNumber: classNumber,
                        lessonId: lessonId,
                        isOwner: isOwner,
                        socketCamerasService: service,
                        streamModel: model,
                        onlyPurchases: false
                    )
                case .finishStream:
                    AlertControllerHelper.showFinishStreamAlert {
                        model.finishStream.accept(())
                    }
                case .rateAndDonate(let lessonId):
                    self.showDonatePopUp(lessonId: lessonId, streamIsSaved: false)
                case .reloadStreamsOnFinish(let lessonId):
                    self.presentAlertForSaveStream(lessonId: lessonId) {
                        self.flow.accept(.exitToPreviousScreen())
                        self.removeViewFromSuperviewTeacher.accept(())
                    }
                case .onPopupWithOnlyPurchases(let page, let lessonId, let service):
                    self.showPopUp(
                        on: page,
                        classNumber: nil,
                        lessonId: lessonId,
                        isOwner: false,
                        socketCamerasService: service,
                        streamModel: model,
                        onlyPurchases: true
                    )
                case .showCoineWriteOffPopup(lessonId: let lessonId, cost: let cost):
                    self.showToBuyLessonPopup(cost: cost, id: lessonId, streamIsSaved: false, viewModel: model)
                case .dismissPopUp:
                    self.dismissPopUp()
                case .onPopupWithSendComplaint(let userId):
                    self.flow.accept(.makeComplaint(userId))
                case .networkErrorPopUp(let errorMessage, let retryNetwRequestAction):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: retryNetwRequestAction)
                case .onPopupThank(let userId):
                    self.showThankPopUp(userId: userId)
                case .onPopupGift(let userId):
                    self.showGiftPopUp(userId: userId, streamModel: model)
                case .onPopupProfileInfo(let userId):
                    guard let infoStream = model.infoStream else { return }
                    self.showProfileInfoPopUp(userId: userId, infoModel: infoStream, model: model)
                }
            }
            .disposed(by: model.bag)

        self.sendGift
            .bind(to: model.sendGift)
            .disposed(by: model.bag)

        self.onUpdateStreamInfo
            .bind { [weak model] id in
                model?.connectWithLessonId(id)
            }
            .disposed(by: model.bag)

        self.onPresentPurchasesPopup
            .bind { [weak model] _ in
                model?.handleOnGiftTap(true)
            }
            .disposed(by: model.bag)
    }

    private func bindSavedtreamModel(_ model: SavedStreamsFeedViewModel) {

        model
            .flow
            .bind { [weak self, weak model] event in
                guard let self = self, let model = model else { return }
                switch event {
                case .onBack(let animated):
                    self.flow.accept(.exitToPreviousScreen(animated: animated))
                    self.removeViewFromSuperviewTeacher.accept(())
                case .onPopUp(let page, let classNumber, let lessonId, let isOwner, let service):
                    self.showSavedStreamsPopUp(
                        on: page,
                        classNumber: classNumber,
                        lessonId: lessonId,
                        isOwner: isOwner,
                        socketCamerasService: service,
                        streamModel: model,
                        onlyPurchases: false
                    )
                case .finishStream:
                    AlertControllerHelper.showFinishStreamAlert {
                        model.finishStream.accept(())
                    }
                case .rateAndDonate(let lessonId):
                    self.showDonatePopUpForSavedStreams(lessonId: lessonId, streamIsSaved: model.isWatchingSave)
                case .reloadStreamsOnFinish(let lessonId):
                    self.presentAlertForSaveStream(lessonId: lessonId) {
                        self.flow.accept(.exitToPreviousScreen())
                        self.removeViewFromSuperviewTeacher.accept(())
                    }
                case .onPopupWithOnlyPurchases(let page, let lessonId, let service):
                    self.showSavedStreamsPopUp(
                        on: page,
                        classNumber: nil,
                        lessonId: lessonId,
                        isOwner: false,
                        socketCamerasService: service,
                        streamModel: model,
                        onlyPurchases: true
                    )
                case .showCoineWriteOffPopup(lessonId: let lessonId, cost: let cost):
                    self.showToBuySavedLessonPopup(cost: cost, id: lessonId, streamIsSaved: true, viewModel: model)
                case .dismissPopUp:
                    self.dismissPopUp()
                case .onPopupWithSendComplaint(let userId):
                    self.flow.accept(.makeComplaint(userId))
                case .networkErrorPopUp(let errorMessage, let retryNetwRequestAction):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: retryNetwRequestAction)
                case .onPopupGift(userAboutID: let userId):
                    self.showGiftPopUp(userId: userId, streamModel: model)
                case .onPopupThank(userAboutID: let userId):
                    self.showThankPopUp(userId: userId)
                case .onPopupProfileInfo(userAboutID: let userId):
                    guard let infoStream = model.infoStream else { return }
                    self.showProfileInfoPopUp(userId: userId, infoModel: infoStream, model: model)
                }
            }
            .disposed(by: model.bag)

        self.sendGift
            .bind(to: model.sendGift)
            .disposed(by: model.bag)

        self.onUpdateStreamInfo
            .bind { [weak model] id in
                model?.connectWithLessonId(id)
            }
            .disposed(by: model.bag)

        self.onPresentPurchasesPopup
            .bind { [weak model] _ in
                model?.handleOnGiftTap(true)
            }
            .disposed(by: model.bag)
    }

    private func dismissPopUp() {
        navigationController?.dismiss(animated: false)
    }

    private func showPopUp(
        on page: Int,
        classNumber: String?,
        lessonId: Int,
        isOwner: Bool,
        socketCamerasService: SocketCamerasService?,
        streamModel: InputDataForStreamPopUpViewModelProtocol,
        onlyPurchases: Bool
    ) {
        let model = StreamPopUpViewModel(
            classNumber,
            accountService: accountService,
            lessonId: lessonId,
            mainService: mainService,
            lessonService: lessonService,
            paymentsService: paymentsService,
            socketCamerasService: socketCamerasService,
            isOwner: isOwner,
            streamModel: streamModel,
            onlyPurchases: onlyPurchases
        )
        let view = StreamPopUpView(model, pageIndex: page)
        bindPopUpViewModel(model, lessonId: lessonId)
        let drawer = DrawerViewController(contentView: view)
        model
            .sendGift
            .bind(to: sendGift)
            .disposed(by: model.bag)
        navigationController?.present(drawer, animated: true)
    }

    private func showSavedStreamsPopUp(
        on page: Int,
        classNumber: String?,
        lessonId: Int,
        isOwner: Bool,
        socketCamerasService: SocketCamerasService?,
        streamModel: SavedStreamsFeedViewModel,
        onlyPurchases: Bool
    ) {
        let model = SavedStreamsPopUpViewModel(
            classNumber,
            accountService: accountService,
            lessonId: lessonId,
            mainService: mainService,
            lessonService: lessonService,
            paymentsService: paymentsService,
            socketCamerasService: socketCamerasService,
            isOwner: isOwner,
            streamModel: streamModel,
            onlyPurchases: onlyPurchases
        )
        let view = SavedStreamsPopUpView(model, pageIndex: page)
        bindSavedStreamsPopUpViewModel(model, lessonId: lessonId)
        let drawer = SavedStreamsDrawerViewController(contentView: view)
        model
            .sendGift
            .bind(to: sendGift)
            .disposed(by: model.bag)
        navigationController?.present(drawer, animated: true)
    }
    
    private func showProfileInfoPopUp(userId: Int, infoModel: PublicProfileResponse, model: StreamsFeedViewModelFlowProtocol) {
        let viewModel = ProfileInfoComplaintViewModel()
        let view = ProfileInfoComplaintPopUpView(infoModel: infoModel, viewModel: viewModel)
        let drawer = DrawerViewController(contentView: view)

        drawer.clousure = { [weak self] in
            guard let viewModel = viewModel.infoStream else { return }
            model.isContactRelay.accept(viewModel.isContact)
        }

        viewModel.flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                self.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self.presentAlertAndCloseStream()
                case .dissmissPopupWithError:
                    self.presentAlertWithError()
                case .showChatVC(let id):
                    self.flow.accept(.toChat(id))
                case .showProfileVC(let classNumber):
                    self.showSpecialistVC(classNumber)
                }
            }
            .disposed(by: bag)

        navigationController?.present(drawer, animated: true)
    }
    
    private func showThankPopUp(userId: Int) {
        let viewModel = ThankComplaintViewModel(service: lessonService, lessonId: userId)
        let view = ThankComplaintPopUpView(viewModel: viewModel, userAbout: userId)
        let drawer = DrawerViewController(contentView: view)

        viewModel.flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                switch flow {
                case .dismiss:
                    self.dismissDrawer()
                case .showTopUpBalancePopUp:
                    self.showTopUpBalancePopUp(userId: userId)
                }
            }.disposed(by: bag)

        view.flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                self.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self.presentAlertAndCloseStream()
                case .dissmissPopupWithError:
                    self.presentAlertWithError()
                }
            }
            .disposed(by: bag)

        navigationController?.present(drawer, animated: true)
    }
    
    private func showTopUpBalancePopUp(userId: Int) {
         let viewModel = TopUpBalanceViewModel(service: lessonService, lessonId: userId)
         let view = TopUpBalancePopUpView(viewModel: viewModel)
         let drawer = DrawerViewController(contentView: view)

         viewModel.flow
             .bind { [weak self] flow in
                 guard let self = self else { return }
                 switch flow {
                 case .dismiss:
                     self.dismissDrawer()
                 }
             }.disposed(by: bag)

         view.flow
             .bind { [weak self] flow in
                 self?.dismissDrawer()
                 switch flow {
                 case .dissmissPopupWithSuccess:
                     self?.presentAlertAndCloseStream()
                 case .dissmissPopupWithError:
                     self?.presentAlertWithError()
                 }
             }.disposed(by: bag)

         navigationController?.dismiss(animated: false, completion: {
             self.navigationController?.present(drawer, animated: true)
         })
     }
    
    private func showGiftPopUp(userId: Int, streamModel: StreamsFeedViewModelFlowProtocol) {
        let viewModel = GiftComplaintViewModel(service: lessonService, lessonId: userId)
        let view = GiftComplaintPopUpView(viewModel: viewModel, streamViewModel: streamModel)
        let drawer = DrawerViewController(contentView: view)

        viewModel.flow.bind { [weak self] flow in
            guard let self = self else { return }
            switch flow {
            case .dismiss:
                self.dismissDrawer()
            case .showTopUpBalancePopUp:
                self.showTopUpBalancePopUp(userId: userId)
            }
        }.disposed(by: bag)

        view.flow
            .bind { [weak self] flow in
                self?.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self?.presentAlertAndCloseStream()
                case .dissmissPopupWithError:
                    self?.presentAlertWithError()
                }
            }.disposed(by: bag)

        navigationController?.present(drawer, animated: true)
    }

    private func showDonatePopUp(lessonId: Int, streamIsSaved: Bool) {
        let model = DonationPopupViewModel(service: lessonService, lessonId: lessonId, lessonIsSaved: streamIsSaved)
        bindDonateVM(model)
        let donateVC = DonationPopupViewController(viewModel: model)
        donateVC.modalPresentationStyle = .overFullScreen
        self.donateVC = donateVC
        guard let viewControllers = navigationController?.viewControllers else { return }
        for vc in viewControllers {
            if let streamsVC = vc as? StreamsFeedViewControllerNew {
                if streamsVC.presentedViewController != nil {
                    streamsVC.dismiss(animated: true) {
                        streamsVC.showDonatePopupIfNeeded { [weak streamsVC] in
                            streamsVC?.present(donateVC, animated: true)
                        }
                    }
                } else {
                    streamsVC.showDonatePopupIfNeeded { [weak streamsVC] in
                        streamsVC?.present(donateVC, animated: true)
                    }
                }
                break
            }
        }
    }

    private func showDonatePopUpForSavedStreams(lessonId: Int, streamIsSaved: Bool) {
        let model = SavedStreamsDonationPopupViewModel(service: lessonService, lessonId: lessonId, lessonIsSaved: streamIsSaved)
        bindSavedStreamsDonateVM(model)
        let donateVC = SavedStreamsDonationPopupViewController(viewModel: model)
        donateVC.modalPresentationStyle = .overFullScreen
        self.savedStreamsDonateVC = donateVC
        guard let viewControllers = navigationController?.viewControllers else { return }
        for vc in viewControllers {
            if let streamsVC = vc as? SavedStreamsFeedViewController {
                if streamsVC.presentedViewController != nil {
                    streamsVC.dismiss(animated: true) {
                        streamsVC.showDonatePopupIfNeeded { [weak streamsVC] in
                            streamsVC?.present(donateVC, animated: true)
                        }
                    }
                } else {
                    streamsVC.showDonatePopupIfNeeded { [weak streamsVC] in
                        streamsVC?.present(donateVC, animated: true)
                    }
                }
                break
            }
        }
    }

    private func subscribeToPrimeryLessonWithChekBalance(_ lessonId: Int,
                                                         cost: Int,
                                                         streamIsSaved: Bool,
                                                         viewModel: StreamsFeedViewModelFlowProtocol) {
        paymentsService.getCoin()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let budgetResponse):
                    if budgetResponse.coin < cost {
                        self.onPresentPurchasesPopup.accept(())
                    } else {
                        self.subscribeToLesson(lessonId, lessonIsSaved: streamIsSaved, viewModel: viewModel)
//                        viewModel.dismissVipLessonPopup.onNext(())
                    }
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func subscribeToLesson(_ lessonId: Int, lessonIsSaved: Bool, viewModel: StreamsFeedViewModelFlowProtocol) {
        lessonService.lessonSubscribe(id: lessonId, lessonIsSaved: lessonIsSaved)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    guard response.statusCode == 200 else {
                        return
                    }
                    AllLiveLessonsModel.updateLiveLessonsSet(lessonService: self.lessonService, bag: self.bag, lessonId: lessonId)
                    viewModel.dismissVipLessonPopup.onNext(lessonId)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
        AllLiveLessonsModel.lessonsUpdate.subscribe(onNext: { [weak self] _ in
            self?.onUpdateStreamInfo.accept(lessonId)
        })
        .disposed(by: bag)
    }

    private func bindDonateVM(_ model: DonationPopupViewModel) {
        model
            .flow
            .bind { [weak self] event in
                switch event {
                case .dismiss:
                    self?.navigationController?.dismiss(animated: true, completion: {
                        self?.flow.accept(.exitToPreviousScreen())
                    })
                }
            }
            .disposed(by: model.bag)
    }

    private func bindSavedStreamsDonateVM(_ model: SavedStreamsDonationPopupViewModel) {
        model
            .flow
            .bind { [weak self] event in
                switch event {
                case .dismiss:
                    self?.navigationController?.dismiss(animated: true, completion: {
                        self?.flow.accept(.exitToPreviousScreen())
                    })
                }
            }
            .disposed(by: model.bag)
    }

    private func bindPopUpViewModel(_ model: StreamPopUpViewModel, lessonId: Int) {
        model
            .flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .showSocial:
                    return
                case .chatWithSpecialist(let id):
                    self.dismissDrawer()
                    self.flow.accept(.toChat(id))
                case .showSpecialistScreen(let classNumber):
                    self.showSpecialistVC(classNumber)
                case .dissmissPopup:
                    self.dismissDrawer()
                case .showThanks:
                    self.showThanks()
                }
            }.disposed(by: model.bag)

//        streamModel.subscribersPopupRelay
//            .bind(to: model.camerasList)
//            .disposed(by: model.bag)
//
//        streamModel.onUpdateLearnerVideoState
//            .bind(to: model.onUpdateLearnerVideoState)
//            .disposed(by: model.bag)
//
//        streamModel.onUpdateLearnerAudioState
//            .bind(to: model.onUpdateLearnerAudioState)
//            .disposed(by: model.bag)
//
//        streamModel.onRemoveLearnerFromPopup
//            .bind(to: model.onRemoveLearnerFromPopup)
//            .disposed(by: model.bag)
//
//        streamModel.onAddLearnerOnPopup
//            .bind(to: model.onAddLearnerOnPopup)
//            .disposed(by: model.bag)
    }
    
    private func presentAlertWithError() {
        let alert = UIAlertController(title: "complaint_alert_error".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func presentAlertAndCloseStream() {
        let alert = UIAlertController(title: "complaint_alert_success".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { _ in
            self.flow.accept(.exitToRoot)
            self.updateAfterClose.accept(true)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func bindSavedStreamsPopUpViewModel(_ model: SavedStreamsPopUpViewModel, lessonId: Int) {
        model
            .flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .showSocial:
                    return
                case .chatWithSpecialist(let id):
                    self.dismissDrawer()
                    self.flow.accept(.toChat(id))
                case .showSpecialistScreen(let classNumber):
                    self.showSpecialistVC(classNumber)
                case .dissmissPopup:
                    self.dismissDrawer()
                case .showThanks:
                    self.showThanks()
                }
            }.disposed(by: model.bag)
    }

    private func showLessonVC(with lessonID: Int) {
        let model = LessonViewControllerViewModel(service: lessonService,
                                                  paymentsService: paymentsService,
                                                  id: lessonID)
        let lessonVC = LessonViewController(viewModel: model)
        bindLessonControllerViewModel(model)
        navigationController?.pushViewController(lessonVC, animated: true)
    }

    private func bindLessonControllerViewModel(_ model: LessonViewControllerViewModel) {
        model.flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .watch(let id, let numberOfStreamsInOnePahe):
                    self.start(self.navigationController, lessonId: id, numberOfStreamsInPage: numberOfStreamsInOnePahe)
                case .chatWithOwner(let id):
                    self.createChatProfileTeacher.accept(id)
                case .edit(let id):
                    self.showEditVC(with: id)
                case .showLesson(let id):
                    self.showLessonVC(with: id)
                case .showLessonNotStarted:
                    return
                case .showPayConfirmation(let cost, let id, let numberOfStreamsInOnePage):
                    let viewModel = StreamsFeedViewModel(lessonService: self.lessonService,
                                                         gcoreService: self.gcoreService,
                                                         lessonId: id,
                                                         streamsList: [],
                                                         numberOfStreamsInPage: numberOfStreamsInOnePage,
                                                         broadcastsList: nil)
                    self.showToBuyLessonPopup(cost: cost, id: id, streamIsSaved: false, viewModel: viewModel)
                case .showReplenishBalance:
                    self.showBalanceRefill()
                case .onSelectedTeacher(let classNumber):
                    guard let classNumber = classNumber else { return }
                    self.showSpecialistVC(classNumber)
                case .alertWarning:
                    self.presentAlertWarning()
                case .makeComplaint:
                    self.flow.accept(.makeComplaint(0))
                }
            }.disposed(by: model.bag)
    }

    private func showEditVC(with lessonID: Int) {
        let editCoordinator = LessonCreateEditCoordinator()
        editCoordinator.start(self.navigationController, lessonID: lessonID, type: .edit)
    }

    private func presentAlertForSaveStream(lessonId: Int, action: @escaping () -> Void) {
        let alert = UIAlertController(title: "popup_save_alert_title".localized, message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "popup_save_alert_yes_button".localized, style: .default) { _ in
            self.saveStream(lessonId: lessonId, action: action)
        }
        let skipAction = UIAlertAction(title: "popup_save_alert_cancel_button".localized, style: .cancel) { _ in
            self.onReloadStreamsList.accept(())
            action()
        }
        alert.addAction(saveAction)
        alert.addAction(skipAction)
        navigationController?.present(alert, animated: true)
    }

    private func saveStream(lessonId: Int, action: @escaping () -> Void) {
        lessonService.saveStream(lessonId: lessonId)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let remainingSlotsCount = Int(response.remainingVideoSlots ?? "nil")
                    else {
                        self?.onReloadStreamsList.accept(())
                        action()
                        return
                    }
                    self?.presentAlertWithRemainingSlots(count: remainingSlotsCount, action: action)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func presentAlertWithRemainingSlots(count slotsCount: Int, action: @escaping () -> Void) {
        let alert = UIAlertController(title: "remaining_save_slots".localized, message: "\(slotsCount)", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { _ in
            self.onReloadStreamsList.accept(())
            action()
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func presentAlertWarning() {
        let alert = UIAlertController(title: "wait_for_the_teacher_to_start_broadcasting".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func showStreamEndedPopUp() {
        let alert = UIAlertController(title: "stream_feed_has_end".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { _ in
            self.flow.accept(.exitToPreviousScreen())
            self.updateAfterClose.accept(true)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func showBalanceRefill() {
        let balanceVM = MyBalanceRefillViewModel(paymentsService: paymentsService)
        let balanceVC = MyBalanceRefillViewController(viewModel: balanceVM, isWhiteNavBar: false)
        navigationController?.pushViewController(balanceVC, animated: true)
    }

    private func bindCreateChat(viewModel: UserAccountViewModel) {
        viewModel
            .flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                switch flow {
                case .onLessonDetail(let id):
                    self.showLessonVC(with: id)
                case .makeComplaint(let id):
                    self.flow.accept(.makeComplaint(id))
                case .watchStream(_, let lessonId, let streamsList, let countOfStreamsInOnePage):
                    self.start(nil, lessonId: lessonId, lessonsInfo: streamsList, numberOfStreamsInPage: countOfStreamsInOnePage)
                case .onListOfUsersSavedLessons:
                    break
                }
            }.disposed(by: viewModel.bag)

        viewModel
            .onCreateChatTapped
            .bind { [weak self] id in
                self?.createChatProfileTeacher.accept(id)
            }.disposed(by: viewModel.bag)
    }

    private func bindToBuyLessonPopup(_ controller: CoineWriteoffPopUpViewController,
                                      id: Int,
                                      cost: Int,
                                      streamIsSaved: Bool,
                                      viewModel: StreamsFeedViewModelFlowProtocol) {
            controller
                .flow
                .bind { [weak self, weak controller] event in
                    guard let self = self, let controller = controller else { return }
                    switch event {
                    case .dismiss:
                        controller.dismiss(animated: false)
                    case .buyAndDismiss:
                        self.subscribeToPrimeryLessonWithChekBalance(id, cost: cost, streamIsSaved: streamIsSaved, viewModel: viewModel)
                        controller.dismiss(animated: true)
                    }
                }
                .disposed(by: controller.bag)
        }

    private func bindToBuySavedLessonPopup(_ controller: CoineWriteoffPopUpForSavedStreamsViewController,
                                           id: Int,
                                           cost: Int,
                                           streamIsSaved: Bool,
                                           viewModel: SavedStreamsFeedViewModel) {
        controller
            .flow
            .bind { [weak self, weak controller] event in
                    guard let self = self, let controller = controller else { return }
                    switch event {
                    case .dismiss:
                        controller.dismiss(animated: false)
                    case .buyAndDismiss:
                        self.subscribeToPrimeryLessonWithChekBalance(id, cost: cost, streamIsSaved: streamIsSaved, viewModel: viewModel)
                        controller.dismiss(animated: true)
                    }
                }
                .disposed(by: controller.bag)
        }

    private func showSpecialistVC(_ classNumber: String) {
        let vm = UserAccountViewModel(service: self.accountService, classNumber: classNumber)
        let specialistVC = UserAccountViewController(viewModel: vm)
        bindCreateChat(viewModel: vm)
        self.dismissDrawerShow(specialistVC)
    }

    private func showToBuyLessonPopup(cost: Int, id: Int, streamIsSaved: Bool, viewModel: StreamsFeedViewModelFlowProtocol) {
        let controller = CoineWriteoffPopUpViewController(cost: cost, id: id)
        bindToBuyLessonPopup(controller, id: id, cost: cost, streamIsSaved: streamIsSaved, viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        navigationController?.present(controller, animated: false)
    }

    private func showToBuySavedLessonPopup(cost: Int, id: Int, streamIsSaved: Bool, viewModel: SavedStreamsFeedViewModel) {
        let controller = CoineWriteoffPopUpForSavedStreamsViewController(cost: cost, id: id)
        bindToBuySavedLessonPopup(controller, id: id, cost: cost, streamIsSaved: streamIsSaved, viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        navigationController?.present(controller, animated: false)
    }

    private func dismissDrawerShow(_ vc: UIViewController) {
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.dismiss(animated: true) {
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    private func showPopupPay() {
        let viewModel = PopupPayViewModel()
        let view = PopupPayView(viewModel: viewModel)
        let drawer = DrawerViewController(contentView: view)
        bindShowPopupPayViewModel(viewModel)
        navigationController?.visibleViewController?.present(drawer, animated: true)
    }

    private func bindShowPopupPayViewModel(_ viewModel: PopupPayViewModel) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .dismiss:
                    self?.navigationController?.visibleViewController?.dismiss(animated: true)
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func dismissDrawer() {
        navigationController?.dismiss(animated: true)
    }

    private func showThanks() {
        PopUp(title: "popup_thaks_for_gift_title".localized).present()
    }
}
