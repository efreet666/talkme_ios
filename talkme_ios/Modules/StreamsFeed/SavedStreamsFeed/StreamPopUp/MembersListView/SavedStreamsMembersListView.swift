//
//  SavedStreamsMembersListView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa
import OpenTok
import CollectionKit

final class SavedStreamsMembersListView: UIView {

    private(set) lazy var reloadButtonTapped = reloadButton.rx.tapGesture().when(.recognized)
    private(set) lazy var onConnectButtonTapped = connectButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let membersLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 16 : 20)
        label.text = "stream_feed_members".localized
        label.textColor = TalkmeColors.streamGrayLabel
        return label
    }()

    let reloadButton = ReloadButtomPopUp(buttonType: .reloadView)
    private let countLabel: UILabel = {
        let label = UILabel()
        // TODO: удалить когда будет срабатывать setupUI()
        let currentCount = "0"
        let currentAttrString = NSAttributedString(string: currentCount, attributes: [.foregroundColor: TalkmeColors.white,
                                                                        .font: UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 16 : 18 )])
        let maxCount = "/50"
        let maxAttrString = NSAttributedString(string: maxCount, attributes: [.foregroundColor: TalkmeColors.streamGrayLabel,
                                                                .font: UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 12)])
        label.attributedText = currentAttrString + maxAttrString
        return label
    }()

    private let connectWithCameraOfMicroLabel: UILabel = {
        let lbl = UILabel()
        let attributedString = NSMutableAttributedString(string: "stream_popup_connect_micro_camera".localized)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.07
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSMakeRange(0, attributedString.length))
        lbl.attributedText = attributedString
        lbl.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 16 : 20)
        lbl.textColor = TalkmeColors.streamGrayLabel
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        return lbl
    }()

    private let connectButton = SavedStreamsRoundedColoredButton(.connect)

    private let collectionView: CollectionView = {
        let collectionView = CollectionView()
        collectionView.bounces = false
        collectionView.showsVerticalScrollIndicator = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.contentInset.right = 13
        collectionView.contentInset.bottom = 16
        return collectionView
    }()

    private var session: OTSession?
    private var dataSource = ArrayDataSource<SavedStreamsMemberWhoBoughtCameraModel>()
    private let onUpdateAudio = PublishRelay<SavedStreamsAlteredStream>()
    private let onUpdateVideo = PublishRelay<SavedStreamsAlteredStream>()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupUI(camerasList: SavedStreamsSubscriberForPopup, membersCount: String) {
        self.session = camerasList.session
        let currentCount = membersCount
        let currentAttrString = NSAttributedString(string: currentCount, attributes: [.foregroundColor: TalkmeColors.white,
                                                                        .font: UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 10 : 18 )])
        let maxCount = "/50"
        let maxAttrString = NSAttributedString(string: maxCount, attributes: [.foregroundColor: TalkmeColors.streamGrayLabel,
                                                                .font: UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 10 : 12)])
        countLabel.attributedText = currentAttrString + maxAttrString
        hideCollectionView(isHidden: membersCount == "0")

        dataSource.data.removeAll()
        camerasList.subscribersArray.forEach { model in
            let dataItem = SavedStreamsMemberWhoBoughtCameraModel(memberModel: model, session: camerasList.session, isOwner: camerasList.isOwner)
            dataSource.data.append(dataItem)
        }

        let memberInfoSource = ClosureViewSource(viewGenerator: { _, _ -> SavedStreamsMemberInfoCollectionCellView in
            return SavedStreamsMemberInfoCollectionCellView(frame: .zero)
        }, viewUpdater: { [weak self] (view: SavedStreamsMemberInfoCollectionCellView, dataItem: SavedStreamsMemberWhoBoughtCameraModel, _) in
            view.configure(memberModel: dataItem.memberModel, session: dataItem.session, isOwner: dataItem.isOwner)

            self?.onUpdateAudio
                .bind { [weak view] model in
                    guard dataItem.memberModel.subscriber.stream == model.subscriber.stream else { return }
                    guard let isMicroOn = model.isMicroOn else {
                        view?.updateAudioState(hasAudio: model.subscriber.stream?.hasAudio ?? false)
                        return }
                    view?.updateAudioState(hasAudio: isMicroOn)
                }
                .disposed(by: view.bag)

            self?.onUpdateVideo
                .bind { [weak view] model in
                    guard dataItem.memberModel.subscriber.stream == model.subscriber.stream else { return }
                    let isMicroButtonTapped = model.isMicroOn != nil
                    let hasVideo = isMicroButtonTapped
                        ? model.subscriber.stream?.hasVideo ?? false
                        : model.subscriberHasVideo ?? false
                    let videoState = SavedStreamsLearnerVideoState(hasVideo: hasVideo, subscriberView: model.subscriber.view)
                    view?.updateVideoState(videoState: videoState)
                }
                .disposed(by: view.bag)
        })

        let provider = BasicProvider(
            dataSource: dataSource,
            viewSource: memberInfoSource,
            sizeSource: ClosureSizeSource(sizeSource: { _, _, _ in
                return CGSize(
                    width: UIScreen.width / 3.7,
                    height: UIScreen.width / 2.17
                )
            })
        )
        provider.layout = FlowLayout(lineSpacing: 22, interitemSpacing: UIScreen.isSE ? 16 : 18)
        collectionView.provider = provider
    }

    func addNewLearner(learner: SavedStreamsCurrentSubscriber) {
        dataSource.data.append(SavedStreamsMemberWhoBoughtCameraModel(memberModel: learner, session: session, isOwner: false))
    }

    func removeLearner(learner: SavedStreamsCurrentSubscriber) {
        dataSource.data.enumerated().forEach { index, member in
            guard member.memberModel == learner else { return }
            dataSource.data.remove(at: index)
        }
    }

    func updateMemberVideoState(model: SavedStreamsAlteredStream) {
        onUpdateVideo.accept(model)
    }

    func updateMemberAudioState(model: SavedStreamsAlteredStream) {
        onUpdateAudio.accept(model)
    }

    // MARK: - Private Methods

    private func hideCollectionView(isHidden: Bool) {
        collectionView.isHidden = isHidden
        connectWithCameraOfMicroLabel.isHidden = !isHidden
        connectButton.isHidden = !isHidden
    }

    private func setupLayout() {
        addSubviews([membersLabel, reloadButton, countLabel, collectionView, connectWithCameraOfMicroLabel, connectButton])

        membersLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().inset(10)
            make.bottom.equalTo(reloadButton).offset(-5)
        }

        reloadButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(10)
            make.leading.equalTo(countLabel.snp.trailing).offset(UIScreen.isSE ? 40 : 80)
            make.centerY.equalTo(membersLabel)
        }

        countLabel.snp.makeConstraints { make in
            make.width.equalTo(45)
            make.height.equalTo(membersLabel)
            make.leading.equalTo(membersLabel.snp.trailing).offset(UIScreen.isSE ? 7 : 10)
            make.centerY.equalTo(membersLabel)
        }

        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(12)
            make.top.equalTo(membersLabel.snp.bottom).offset(10)
            make.bottom.equalToSuperview()
        }

        connectWithCameraOfMicroLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 110 : 140)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(connectButton.snp.top).inset(UIScreen.isSE ? -18 : -22)
        }

        connectButton.snp.makeConstraints { make in
            make.top.equalTo(connectWithCameraOfMicroLabel.snp.bottom).inset(UIScreen.isSE ? 40 : 80)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 45 : 59)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 80 : 100)
        }
    }
}

