//
//  SavedStreamsMemberInfoCollectionCellView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa
import OpenTok

struct SavedStreamsLearnerVideoState {
    let hasVideo: Bool?
    let subscriberView: UIView?
}

final class SavedStreamsMemberInfoCollectionCellView: UIView {

    // MARK: Public Properties

    var bag = DisposeBag()

    // MARK: - Private Properties

    private lazy var isMicrophoneOn = BehaviorRelay<Bool>(value: false)
    private lazy var microButtonTapped = microImageView.rx.tapGesture().when(.recognized)

    private let timeLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textAlignment = .center
        label.textColor = TalkmeColors.streamGrayLabel
        return label
    }()

    private let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 8
        imageView.clipsToBounds = true
        return imageView
    }()

    private let microImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()

    private var timer: Timer?

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
        isUserInteractionEnabled = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(memberModel: SavedStreamsCurrentSubscriber, session: OTSession?, isOwner: Bool) {
        var unserror: OTError?
        session?.unsubscribe(memberModel.subscriber, error: &unserror)
        avatarImageView.kf.setImage(with: URL(string: memberModel.avatarUrl))
        setTimer(time: memberModel.timeEnd)
        bindUI(memberModel: memberModel, session: session, isOwner: isOwner)
        var error: OTError?
        session?.subscribe(memberModel.subscriber, error: &error)
        createSubscriberView(videoState: SavedStreamsLearnerVideoState(
                                hasVideo: memberModel.subscriber.stream?.hasVideo,
                                subscriberView: memberModel.subscriber.view
        ))
        isMicrophoneOn.accept(memberModel.subscriber.stream?.hasAudio ?? false)
    }

    func updateAudioState(hasAudio: Bool) {
        isMicrophoneOn.accept(hasAudio)
    }

    func updateVideoState(videoState: SavedStreamsLearnerVideoState) {
        createSubscriberView(videoState: videoState)
    }

    // MARK: - Private Methods

    private func createSubscriberView(videoState: SavedStreamsLearnerVideoState) {
        avatarImageView.subviews.forEach { $0.removeFromSuperview() }
        guard let hasVideo = videoState.hasVideo,
              hasVideo, let currentSubscriberView = videoState.subscriberView
        else { return }
        currentSubscriberView.layer.cornerRadius = 8
        currentSubscriberView.clipsToBounds = true
        avatarImageView.addSubview(currentSubscriberView)
        currentSubscriberView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func bindUI(memberModel: SavedStreamsCurrentSubscriber, session: OTSession?, isOwner: Bool) {
        isMicrophoneOn
            .bind { [weak self] isOn in
                self?.microImageView.image = UIImage(named: isOn ? "microfonOn" : "microfonOff")
            }
            .disposed(by: bag)

        microButtonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                var isOn = self.isMicrophoneOn.value
                let subscriber = memberModel.subscriber
                if isOwner {
//                    do {
//                        guard let stream = subscriber.stream,
//                              !UserDefaultsHelper.shared.isBlockedAudioByPub
//                              else { return }
//                        let model = OTSessionDemonstrateAudioFromTeacher(
//                            id: stream.connection.connectionId,
//                            flag: isOn
//                        )
//                        let signal = try JSONEncoder().encode(model)
//                        session?.signal(
//                            withType: "toggleSpecificAudio",
//                            string: isOn == true ? "true" : nil,
//                            connection: stream.connection,
//                            retryAfterReconnect: true,
//                            error: nil)
//                        session?.signal(
//                            withType: "demonstrateAudio",
//                            string: String(data: signal, encoding: .utf8),
//                            connection: nil,
//                            retryAfterReconnect: true,
//                            error: nil)
//                    } catch {
//                        assertionFailure("Can't to encode message model: \(error.localizedDescription)")
//                    }
                } else {
                    guard !memberModel.blockedAudioByPub else { return }
                    isOn = !isOn
                    subscriber.subscribeToAudio = isOn
                    self.isMicrophoneOn.accept(isOn)
                }
            }
            .disposed(by: bag)
    }

    private func setTimer(time: TimeInterval) {
        let adjustedTime = time
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.setFormatedTime(time: adjustedTime + 1)
        }
    }

    private func setFormatedTime(time: TimeInterval) {
        let time = TimerManager.diffFromFutureTime(timeInterval: time)
        guard time >= 0 else {
            timer?.invalidate()
            return
        }
        timeLabel.text = Formatters.minutesSeconds(time: time)
        timeLabel.sizeToFit()
    }

    private func initialSetup() {
        addSubviews([avatarImageView, timeLabel, microImageView])

        avatarImageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(microImageView).offset(UIScreen.isSE ? -15 : -20)
            make.top.equalTo(timeLabel.snp.bottom).offset(5)
        }

        timeLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(UIScreen.isSE ? 12 : 17)
            make.height.equalTo(UIScreen.isSE ? 19 : 25)
        }

        microImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 40 : 50)
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
        }
    }
}
