//
//  SavedStreamsMemberWhoBoughtCameraModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import OpenTok

struct SavedStreamsMemberWhoBoughtCameraModel {
    let memberModel: SavedStreamsCurrentSubscriber
    let session: OTSession?
    let isOwner: Bool
}
