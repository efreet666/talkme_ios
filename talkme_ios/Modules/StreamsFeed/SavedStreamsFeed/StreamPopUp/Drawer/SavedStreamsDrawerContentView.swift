//
//  SavedStreamsDrawerContentView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit
import RxCocoa

protocol SavedStreamsDrawerContentView: UIView {
    var viewHeight: BehaviorRelay<CGFloat> { get }
    var onDismiss: (() -> Void)? { get set }
    func updateLayout(height: CGFloat)
}
