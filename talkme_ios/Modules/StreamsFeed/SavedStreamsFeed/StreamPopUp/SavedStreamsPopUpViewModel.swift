//
//  SavedStreamsPopUpViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa
import OpenTok

protocol SavedStreamsInputDataForStreamPopUpViewModelProtocol {
    var cost: Int { get }
    var isWatchingSave: Bool { get }
    var isSubscribe: Bool { get }
}

final class SavedStreamsPopUpViewModel {
    enum Flow {
        case showSpecialistScreen(String)
        case chatWithSpecialist(Int)
        case showSocial(String)
        case dissmissPopup
        case showThanks
    }

    // MARK: - Public properties

    let items = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    let connectButton = PublishRelay<Void>()
    let connectButtonTapped = PublishRelay<Void>()
    let audioVideoTap = PublishRelay<Void>()
    let reloadCollection = PublishRelay<Void>()
    let toGetCoins = PublishRelay<Void>()
    let toGetCoinsIsNotSubscribe = PublishRelay<Void>()
    let isOwner: Bool
    let sendGift = PublishRelay<Int>()
    let camerasList = PublishRelay<SavedStreamsPopupData>()
    let iapManager = IAPManager.shared
    let onUpdateLearnerVideoState = PublishRelay<SavedStreamsAlteredStream>()
    let onUpdateLearnerAudioState = PublishRelay<SavedStreamsAlteredStream>()
    let onAddLearnerOnPopup = PublishRelay<SavedStreamsCurrentSubscriber>()
    let onRemoveLearnerFromPopup = PublishRelay<SavedStreamsCurrentSubscriber>()

    // MARK: - Private Properties

    private let mainService: MainServiceProtocol
    private let lessonService: LessonsServiceProtocol
    private let accountService: AccountServiceProtocol
    private let paymentsService: PaymentsServiceProtocol
    private let classNumber: String?
    private var balance = 0
    private let lessonId: Int
    private let socketCamerasService: SocketCamerasService?
    private var paymentTypesVM: SavedStreamsPaymentTypesCellVM
    private var timer: Timer?
    private var isNeedScrollToPayments: Bool
    private var isOnlyPurchasesPopup: Bool
    private let lessonCost: Int
    private let userIsSubscribedToTheLesson: Bool
    private let lessonIsSaved: Bool

    // MARK: - Initializers

    init(_ classNumber: String?,
         accountService: AccountServiceProtocol,
         lessonId: Int,
         mainService: MainServiceProtocol,
         lessonService: LessonsServiceProtocol,
         paymentsService: PaymentsServiceProtocol,
         socketCamerasService: SocketCamerasService?,
         isOwner: Bool,
         streamModel: SavedStreamsFeedViewModel,
         onlyPurchases: Bool
    ) {
        self.paymentTypesVM = SavedStreamsPaymentTypesCellVM(onlyPurchases: onlyPurchases)
        self.classNumber = classNumber
        self.accountService = accountService
        self.lessonId = lessonId
        self.mainService = mainService
        self.lessonService = lessonService
        self.paymentsService = paymentsService
        self.socketCamerasService = socketCamerasService
        self.isOwner = isOwner
        self.isOnlyPurchasesPopup = onlyPurchases
        self.lessonCost = streamModel.cost
        self.lessonIsSaved = streamModel.isWatchingSave
        self.userIsSubscribedToTheLesson = streamModel.isSubscribe
        isNeedScrollToPayments = classNumber == nil
    }

    // MARK: - Public Methods

    func getItems(_ completion: (() -> Void)? = nil) {
        paymentTypesVM = SavedStreamsPaymentTypesCellVM(onlyPurchases: self.isOnlyPurchasesPopup)
        self.setupTimer(model: paymentTypesVM)
        self.getCurrentBalance(paymentTypesVM)
        self.getPaymentsTypesData(paymentTypesVM)
        self.bindPaymentsVM(self.paymentTypesVM)
        var items: [AnyCollectionViewCellModelProtocol] = []

        guard let classNumber = self.classNumber else {
            items = [self.paymentTypesVM]
            self.items.accept(items)
            return
        }
        accountService
            .streamPublicProfile(classNumber: classNumber)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    if !self.isOwner {
                        let model = SavedStreamsSpecialistPopUpCellViewModel(response)
                        self.bindSpecialistCellmodel(model)
                        items = [self.paymentTypesVM, model]
                    }
//                    if self.isOwner {
//                        let teacherMemberList = TeacherMemberCellViewModel(socketCamerasService: self.socketCamerasService)
//                        self.bindTeachersMembersListVM(teacherMemberList)
//                        items.append(teacherMemberList)
//                    } else {
//                        let memberListModel = StreamMembersCellViewModel()
//                        self.bindMembersListVM(memberListModel)
//                        items.append(memberListModel)
//                    }
                    DispatchQueue.main.async {
                        self.items.accept(items)
                        completion?()
                    }
                case .error(let error) :
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func setupTimer(model: SavedStreamsPaymentTypesCellVM) {
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.getCurrentBalance(model)
        }
    }

    // MARK: - Private Methods

    private func bindSpecialistCellmodel(_ model: SavedStreamsSpecialistPopUpCellViewModel) {
        model
            .flow
            .bind { [weak self, weak model] event in
                guard let self = self, let model = model else { return }
                switch event {
                case .addSpecialist(let id):
                    self.addSpecialist(model, id: id)
                case .removeSpecialist(let id):
                    self.removeSpecialist(model, id: id)
                case .showSocial(let socialMedia):
                    let url = model.response.value.specialistInfo
                    switch socialMedia {
                    case .vk:
                        SocialRedirectionHelper.vk.redirect(to: url.vk ?? "")
                    case .telegram:
                        SocialRedirectionHelper.telegram.redirect(to: url.telegram ?? "")
                    case .instagram:
                        SocialRedirectionHelper.instagram.redirect(to: url.instagram ?? "")
                    case .facebook:
                        SocialRedirectionHelper.facebook.redirect(to: url.facebook ?? "")
                    }
                case .showChatWithSpecialist(let id):
                    self.flow.accept(.chatWithSpecialist(id))
                case .showSpecialistScreen:
                    guard let classNumber = self.classNumber else { return }
                    self.flow.accept(.showSpecialistScreen(classNumber))
                }
            }.disposed(by: model.bag)
    }

    private func bindMembersListVM(_ model: SavedStreamsMembersCellViewModel) {
        self.socketCamerasService?.fetchCamerasList()

        model
            .refreshMembersList
            .bind { [weak self] in
                self?.socketCamerasService?.fetchCamerasList()
            }.disposed(by: model.bag)

        camerasList
            .bind(to: model.camerasListRelay)
            .disposed(by: model.bag)

        model
            .onConnectButtonTappedRelay
            .map { [weak self] tap in
                self?.connectButtonTapped.accept(tap)
            }
            .bind(to: connectButton)
            .disposed(by: model.bag)

        onUpdateLearnerVideoState
            .bind(to: model.onUpdateLearnerVideoState)
            .disposed(by: model.bag)

        onUpdateLearnerAudioState
            .bind(to: model.onUpdateLearnerAudioState)
            .disposed(by: model.bag)

        onAddLearnerOnPopup
            .bind(to: model.onAddLearnerOnPopup)
            .disposed(by: model.bag)

        onRemoveLearnerFromPopup
            .bind(to: model.onRemoveLearnerFromPopup)
            .disposed(by: model.bag)
    }

    private func bindTeachersMembersListVM(_ model: SavedStreamsTeacherMemberCellViewModel) {
        model
            .refreshMembersList
            .bind { [weak self] in
                self?.socketCamerasService?.fetchCamerasList()
            }.disposed(by: model.bag)

        camerasList
            .bind { [weak model] data in
                model?.camerasListRelay.accept(data.subscriberForPopup)
            }
            .disposed(by: model.bag)

        onUpdateLearnerVideoState
            .bind(to: model.onUpdateLearnerVideoState)
            .disposed(by: model.bag)

        onUpdateLearnerAudioState
            .bind(to: model.onUpdateLearnerAudioState)
            .disposed(by: model.bag)

        onAddLearnerOnPopup
            .bind(to: model.onAddLearnerOnPopup)
            .disposed(by: model.bag)

        onRemoveLearnerFromPopup
            .bind(to: model.onRemoveLearnerFromPopup)
            .disposed(by: model.bag)
    }

    private func bindPaymentsVM(_ model: SavedStreamsPaymentTypesCellVM) {
        model
            .selectedDonat
            .bind { [weak self] donat in
                guard let self = self else { return }
//                let freeOrSubsribed = self.streamCost == 0 || self.streamIsSubscribe != false
//                if donat.cost <= self.balance && freeOrSubsribed {
//                    self.makeGift(donatID: donat.id, model: model)
//                } else if self.streamIsSubscribe == false && self.streamCost != 0 {
//                    self.toGetCoinsIsNotSubscribe.accept(())
//                } else {
//                    self.toGetCoins.accept(())
//                }
                let freeOrSubsribed = self.lessonCost == 0 || self.userIsSubscribedToTheLesson == true
                if donat.cost <= self.balance && freeOrSubsribed {
                    self.makeGift(donatID: donat.id, model: model)
                } else if self.userIsSubscribedToTheLesson == false && self.lessonCost != 0 {
                    self.toGetCoinsIsNotSubscribe.accept(())
                } else {
                    self.toGetCoins.accept(())
                }
            }
            .disposed(by: model.bag)

        model
            .selectedAudioVideoPlan
            .bind { [weak self] plan in
                guard let self = self else { return }
                if plan.cost <= self.balance {
                    self.buyCameraAndMicro(planID: plan.id.rawValue, model: model)
                } else {
                    self.toGetCoins.accept(())
                }
            }
            .disposed(by: model.bag)

        toGetCoins
            .bind(to: model.toGetCoins)
            .disposed(by: model.bag)

        toGetCoinsIsNotSubscribe
            .bind(to: model.toGetCoinsIsNotSubscribe)
            .disposed(by: model.bag)

        model
            .onSendTips
            .bind { [weak self, weak model] tips in
                guard let model = model else { return }
                let freeOrSubsribed = self?.lessonCost == 0 || self?.userIsSubscribedToTheLesson != false
                if freeOrSubsribed {
                    self?.sendTips(tips: tips, model: model)
                } else {
                    self?.toGetCoinsIsNotSubscribe.accept(())
                }
            }
            .disposed(by: model.bag)

        audioVideoTap
            .bind(to: model.audioVideoButtonTap)
            .disposed(by: model.bag)

        model
            .selectedPaymentPlan
            .bind { [weak self] id in
                self?.toBuyCoins(id: id)
            }
            .disposed(by: model.bag)
    }

    private func addSpecialist(_ model: SavedStreamsSpecialistPopUpCellViewModel, id: Int) {
        accountService
            .addContact(id: id)
            .subscribe { [weak model, weak self] event in
                guard let model = model else { return }
                switch event {
                case .success(let response):
                    guard response.resp != nil else { return }
                    self?.updateSpecialistinfo(model)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: model.bag)
    }

    private func removeSpecialist(_ model: SavedStreamsSpecialistPopUpCellViewModel, id: Int) {
        accountService
            .removeContact(id: id)
            .subscribe { [weak model, weak self] event in
                guard let model = model else { return }
                switch event {
                case .success(let response):
                    guard response.resp != nil else { return }
                    self?.updateSpecialistinfo(model)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: model.bag)
    }

    private func updateSpecialistinfo(_ model: SavedStreamsSpecialistPopUpCellViewModel) {
        guard let classNumber = self.classNumber else { return }
        accountService
            .streamPublicProfile(classNumber: classNumber)
            .subscribe { [weak model] event in
                switch event {
                case .success(let response):
                    model?.response.accept(response)
                case .error(let error) :
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func getCurrentBalance(_ model: SavedStreamsPaymentTypesCellVM) {
        paymentsService
            .getCoin()
            .subscribe { [weak self, weak model] event in
                switch event {
                case .success(let response):
                    self?.balance = response.coin
                    model?.updateBalance(coins: response.coin)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func getPaymentsTypesData(_ model: SavedStreamsPaymentTypesCellVM) {
        Single.zip(paymentsService.getGifts(), paymentsService.getCameraPlan())
            .subscribe { [weak model, weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    let (gifts, cameraPlans) = response
                    model?.setCollectionItems(
                        gifts: gifts,
                        plans: self.iapManager.purchases,
                        cameraPlans: cameraPlans,
                        isNeedScrollToPayments: self.isNeedScrollToPayments
                    )
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func makeGift(donatID: Int, model: SavedStreamsPaymentTypesCellVM) {
        let request = MakeGiftRequest(gift: donatID, lesson: lessonId, saved: lessonIsSaved)
        paymentsService
            .makeGift(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.sendGift.accept(response.id)
                    self?.getCurrentBalance(model)
                    AppsFlyerProvider.shared.trackUserDonatingToStreamer(andGetResponse: response)
                    self?.flow.accept(.dissmissPopup)
                    if request.saved {
                        self?.flow.accept(.showThanks)
                    }
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func buyCameraAndMicro(planID: Int, model: SavedStreamsPaymentTypesCellVM) {
        guard let timeEnd = UserDefaultsHelper.shared.cameraTimeEnd, Date().timeIntervalSince1970 < timeEnd else {
            socketCamerasService?.buyMinutes(user: UserDefaultsHelper.shared.tokBoxUniqueId, minutePlan: planID)
            getCurrentBalance(model)
            self.flow.accept(.dissmissPopup)
            return
        }
        self.flow.accept(.dissmissPopup)
    }

    private func sendTips(tips: Int, model: SavedStreamsPaymentTypesCellVM) {
        lessonService
            .tips(tips: tips, lessonId: self.lessonId, lessonIsSaved: self.lessonIsSaved)
            .subscribe { [weak self, weak model] event in
                guard let model = model, let self = self else { return}
                switch event {
                case .success:
                    if tips > self.balance {
                        self.toGetCoins.accept(())
                    } else {
                        self.getCurrentBalance(model)
                        self.flow.accept(.dissmissPopup)
                        if self.lessonIsSaved {
                            self.flow.accept(.showThanks)
                        }
                    }
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func toBuyCoins(id: Int) {
        let productId = String(id)
        iapManager.purchase(productWith: productId)
        flow.accept(.dissmissPopup)
    }
}
