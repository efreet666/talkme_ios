//
//  SavedStreamsPopUpView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsPopUpView: UIView, SavedStreamsDrawerContentView {

    private enum Constants {
        static let topToSwipeLine: CGFloat = UIScreen.isSE ? 8 : 11
        static let swipeLineSize = UIScreen.isSE
            ? CGSize(width: 97, height: 3)
            : CGSize(width: 126, height: 4)
        static let swipeToCollection: CGFloat = 5
        static let collectionToIndicator: CGFloat = UIScreen.isSE ? 15 : 18
        static let indicatorSize = UIScreen.isSE
            ? CGSize(width: 27, height: 2)
            : CGSize(width: 35, height: 3)
        static let indicatorToBottom: CGFloat = UIScreen.isSE ? 20 : 24
        static var height: CGFloat {
            min((UIScreen.isSE ? 404 : 523), UIScreen.main.bounds.height)
        }
        static var width: CGFloat {
            min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        }
    }

    // MARK: - Public properties

    var onDismiss: (() -> Void)?

    let viewHeight = BehaviorRelay<CGFloat>(
        value: Constants.height
    )

    // MARK: - Private Properties

    private let bag = DisposeBag()

    private let swipeLineView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = .white
        return view
    }()

    private lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let height = viewHeight.value
            - Constants.topToSwipeLine
            - Constants.swipeLineSize.height
            - Constants.swipeToCollection
            - Constants.collectionToIndicator
            - Constants.indicatorSize.height
            - Constants.indicatorToBottom
        layout.itemSize = CGSize(width: Constants.width, height: height)
        layout.minimumLineSpacing = 0
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.registerCells(
            withModels: SavedStreamsSpecialistPopUpCellViewModel.self,
//            viewModel.isOwner ? TeacherMemberCellViewModel.self : StreamMembersCellViewModel.self,
            SavedStreamsPaymentTypesCellVM.self
        )
        return collectionView
    }()

    private lazy var pageIndicator = SavedStreamsPageIndicatorView(
        itemSize: Constants.indicatorSize,
        spacing: UIScreen.isSE ? 6 : 8,
        itemsCount: viewModel.isOwner ? 2 : 4
    )

    private lazy var recognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        recognizer.cancelsTouchesInView = false
        return recognizer
    }()

    private let viewModel: SavedStreamsPopUpViewModel

    // MARK: - Initializers

    init(_ model: SavedStreamsPopUpViewModel, pageIndex: Int) {
        viewModel = model
        super.init(frame: .zero)
        collectionView.delegate = self
        setUpAppearance()
        pageIndicator.selectItem(at: pageIndex)
        setUpConstraints()
        bindVM()
        bindUI()
        bindTap()
        AppDelegate.deviceOrientation = .portrait
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        if viewModel.isOwner {
            AppDelegate.deviceOrientation = .all
        } else {
            AppDelegate.deviceOrientation = .portrait
        }
    }

    // MARK: - Public Methods

    override func didMoveToSuperview() {
        guard superview != nil else { return }
        viewModel.getItems {
            self.collectionView.contentOffset.x = Constants.width * CGFloat(self.pageIndicator.selectedIndex - 1)
        }
    }

    func updateLayout(height: CGFloat) {
        collectionView.snp.updateConstraints { make in
            make.top.equalTo(swipeLineView.snp.bottom).offset(Constants.swipeToCollection - height)
        }
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        collectionView.endEditing(true)
    }

    private func setUpConstraints() {
        addSubviews(
            swipeLineView,
            collectionView,
            pageIndicator
        )
        addGestureRecognizer(recognizer)

        swipeLineView.snp.makeConstraints { make in
            make.size.equalTo(Constants.swipeLineSize)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(Constants.topToSwipeLine)
        }

        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(swipeLineView.snp.bottom).offset(Constants.swipeToCollection)
        }

        pageIndicator.snp.makeConstraints { make in
            make.top.equalTo(collectionView.snp.bottom).offset(Constants.collectionToIndicator)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(Constants.indicatorToBottom)
        }
    }

    private func bindVM() {
        viewModel
            .items
            .asDriver(onErrorJustReturn: [])
            .drive(collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func bindUI() {
        collectionView.rx.didScroll
            .bind { [weak self] in
                self?.hideKeyboard()
            }
            .disposed(by: bag)
    }

    private func bindTap() {
        viewModel
            .connectButtonTapped
            .bind { [weak self] in
                let indexpath = IndexPath(item: 0, section: 0)
                self?.collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self?.viewModel.audioVideoTap.accept(())
                }
            }
            .disposed(by: bag)
    }

    @objc private func hideKeyboard() {
        endEditing(true)
    }

    private func setUpAppearance() {
        backgroundColor = TalkmeColors.streamPopUpGray
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        layer.cornerRadius = 13
        clipsToBounds = true
    }
}
// MARK: - UICollectionViewDelegate

extension SavedStreamsPopUpView: UICollectionViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let collectionBounds = collectionView.bounds
        guard let indexPath = collectionView.indexPathForItem(
            at: CGPoint(
                x: collectionBounds.midX,
                y: collectionBounds.midY
            )
        ) else {
            return
        }
        pageIndicator.selectItem(at: indexPath.item + 1)
    }
}
