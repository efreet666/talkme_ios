//
//  savedStreamsStreamPageIndicatorView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsPageIndicatorView: UIView {

    private(set) var selectedIndex: Int

    private let indicatorView: SavedStreamsRepeatingViewsMakerView<SavedStreamPageControlView>

    init(itemSize: CGSize, spacing: CGFloat, itemsCount: Int, selectedIndex: Int = 0) {
        let indicators = [SavedStreamsGenericViewType](repeating: .pageControl, count: itemsCount)
        let indicatorView = SavedStreamsRepeatingViewsMakerView(viewsData: indicators,
                                                                viewType: SavedStreamPageControlView.self,
                                                                size: itemSize,
                                                                spacing: spacing)
        self.indicatorView = indicatorView
        if selectedIndex < itemsCount && selectedIndex >= 0 {
            indicatorView.viewsArray[selectedIndex].setSelected(true)
            self.selectedIndex = selectedIndex
        } else {
            self.selectedIndex = 0
        }
        super.init(frame: indicatorView.frame)
        backgroundColor = .clear
        setUpConstaints(indicatorView.frame.size)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func selectItem(at index: Int) {
        guard index <= indicatorView.count && index >= 0 else { return }
        indicatorView
            .viewsArray
            .forEach { $0.setSelected($0.viewTag == index) }
        selectedIndex = index
    }

    private func setUpConstaints(_ size: CGSize) {
        addSubview(indicatorView)

        snp.makeConstraints { make in
            make.size.equalTo(size)
        }

        indicatorView.snp.makeConstraints { make in
            make.center.size.equalToSuperview()
        }
    }
}
