//
//  SavedStreamsLabelAndCoinView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

final class SavedStreamsLabelAndCoinView: UIView {

    // MARK: - Public properties

    enum LabelAndCoinType {
        case streamFeedTimeItem(price: String)
        case streamDonatPrice(price: String)
    }

    // MARK: - Private Properties

    private let priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.textColor = TalkmeColors.white
        priceLabel.textAlignment = .center
        priceLabel.adjustsFontSizeToFitWidth = true
        return priceLabel
    }()

    private let coinImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "coin")
        img.contentMode = .scaleAspectFit
        img.clipsToBounds = true
        return img
    }()

    private let priceStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .equalSpacing
        sv.spacing = 0
        return sv
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
    }

    func configure(viewType: LabelAndCoinType) {
        switch viewType {
        case .streamFeedTimeItem(let price):
            priceLabel.textColor = TalkmeColors.white
            priceStack.spacing = 0
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 17 : 19)
            priceLabel.text = price
            priceLabel.textAlignment = .left
        case .streamDonatPrice(let price):
            priceLabel.textColor = TalkmeColors.white
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
            priceLabel.text = price
        }
    }

    // MARK: - Private Methods

    private func setupLayout() {
        priceStack.addArrangedSubview(priceLabel)
        priceStack.addArrangedSubview(coinImage)
        addSubview(priceStack)

        priceStack.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
