//
//  SavedStreamsChangePaymentTypeButton.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

enum SavedStreamsPaymentType {
    case donats
    case tips

    var text: String {
        switch self {
        case .donats:
            return "stream_feed_donats".localized
        case .tips:
            return "stream_feed_tips".localized
        }
    }
}

final class SavedStreamsChangePaymentTypeButton: UIView {

    // MARK: - Public properties

    lazy var buttonTapped = self.rx.tapGesture().when(.recognized)

    // MARK: - Private properties

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = TalkmeColors.streamGrayLabel
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
        return label
    }()

    private let indicatorView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIScreen.isSE ? 1 : 1.5
        view.backgroundColor = TalkmeColors.shadow
        view.isHidden = true
        return view
    }()

    // MARK: - Init

    init(type: SavedStreamsPaymentType) {
        nameLabel.text = type.text
        super.init(frame: .zero)
        setupLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func setOn(_ isActive: Bool) {
        nameLabel.textColor = isActive ? TalkmeColors.shadow : TalkmeColors.streamGrayLabel
        indicatorView.isHidden = !isActive
    }

    // MARK: - Private methods

    private func setupLayout() {
        addSubviews([nameLabel, indicatorView])

        nameLabel.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalTo(indicatorView.snp.top).offset(-2)
        }

        indicatorView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 2 : 3)
            make.width.equalTo(UIScreen.isSE ? 9 : 13)
            make.centerX.equalTo(nameLabel)
            make.bottom.equalToSuperview()
        }
    }
}
