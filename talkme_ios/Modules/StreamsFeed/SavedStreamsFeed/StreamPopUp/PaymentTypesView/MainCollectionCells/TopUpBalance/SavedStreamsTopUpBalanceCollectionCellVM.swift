//
//  SavedStreamsTopUpBalanceCollectionCellVM.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsTopUpBalanceCollectionCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let selectedPlan = PublishRelay<Int>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    var items: [SavedStreamsTopUpBalanceCollectionCellItemVM]
    var selectedPlanId: Int?

    // MARK: - Init

    init(plansModel: [GetPlansResponse]) {
        self.items = plansModel.map { SavedStreamsTopUpBalanceCollectionCellItemVM(type: $0)}
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsTopUpBalanceCollectionCell) {
        cell.configure(items: items)

        cell
            .selectedPlan
            .bind { [weak self, weak cell] selectedPlan in
                self?.selectedPlanId = selectedPlan
                cell?.activateButton(true)
            }
            .disposed(by: cell.bag)

        cell
            .toBuyButtonTapped
            .bind { [weak self] _ in
                guard let self = self, let selectedPlanId = self.selectedPlanId else { return }
                cell.buttonTouched(cell.bottomButton)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
                    self.selectedPlan.accept(selectedPlanId)
                }
            }
            .disposed(by: cell.bag)
    }
}

