//
//  savedStreamsTopUpBalanceCollectionCellItemVM.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

final class SavedStreamsTopUpBalanceCollectionCellItemVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let type: GetPlansResponse

    // MARK: - Initializers

    init(type: GetPlansResponse) {
        self.type = type
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedSteramsTopUpBalanceCollectionCellItem) {
        cell.configure(topUpType: type)
    }
}
