//
//  SavedStreamsAudioVideoCollectionCellItem.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

final class SavedStreamsAudioVideoCollectionCellItem: UICollectionViewCell {

    // MARK: - Private Properties

    private let clockImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()

    private let priceView: SavedStreamsLabelAndCoinView = {
        let view = SavedStreamsLabelAndCoinView()
        view.configure(viewType: .streamFeedTimeItem(price: ""))
        return view
    }()

    private let timeLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 17)
        label.textColor = TalkmeColors.white
        return label
    }()

    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIScreen.isSE ? 5 : 9
        view.layer.borderWidth = 2
        view.layer.borderColor = TalkmeColors.unselectedTimeItem.cgColor
        view.backgroundColor = TalkmeColors.unselectedTimeItem
        view.clipsToBounds = true
        return view
    }()

    override var isSelected: Bool {
        didSet {
            containerView.backgroundColor = isSelected ? TalkmeColors.selectedTimeItem : TalkmeColors.unselectedTimeItem
            containerView.layer.borderColor = isSelected ? TalkmeColors.selectedTimeItemBorder.cgColor : TalkmeColors.unselectedTimeItem.cgColor
        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(cameraPlan: GetCameraPlanResponse) {
        clockImageView.image = cameraPlan.id.icon
        timeLabel.text = "\(String(cameraPlan.minutes)) \("stream_feed_minutes".localized)"
        priceView.configure(viewType: .streamFeedTimeItem(price: String(cameraPlan.cost)))
    }

    // MARK: - Private Methods

    private func initialSetup() {
        containerView.addSubviews([clockImageView, priceView, timeLabel])
        addSubview(containerView)

        containerView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(UIScreen.main.bounds.width / 4.57)
        }

        clockImageView.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 14 : 18)
            make.width.equalTo(clockImageView.snp.height).dividedBy(1.25)
        }

        timeLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.top.equalToSuperview().inset(UIScreen.isSE ? 17 : 22)
            make.height.equalTo(UIScreen.isSE ? 18 : 24)
            make.bottom.equalTo(priceView.snp.top)
            make.leading.equalTo(clockImageView.snp.trailing).offset(UIScreen.isSE ? 20 : 25)
        }

        priceView.snp.makeConstraints { make in
            make.trailing.lessThanOrEqualToSuperview()
            make.leading.equalTo(timeLabel)
            make.height.equalTo(UIScreen.isSE ? 17 : 23)
        }
    }
}
