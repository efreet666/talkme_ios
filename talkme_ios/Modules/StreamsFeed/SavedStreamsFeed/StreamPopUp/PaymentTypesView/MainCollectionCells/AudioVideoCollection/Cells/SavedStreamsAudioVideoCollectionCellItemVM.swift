//
//  SavedStreamsAudioVideoCollectionCellItemVM.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

final class SavedStreamsAudioVideoCollectionCellItemVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let time: GetCameraPlanResponse

    // MARK: - Initializers

    init(time: GetCameraPlanResponse) {
        self.time = time
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsAudioVideoCollectionCellItem) {
        cell.configure(cameraPlan: time)
    }
}
