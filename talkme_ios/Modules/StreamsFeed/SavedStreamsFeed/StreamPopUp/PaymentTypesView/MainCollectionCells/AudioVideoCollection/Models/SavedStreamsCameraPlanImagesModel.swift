//
//  SavedStreamsCameraPlanImagesModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

enum SavedStreamsCameraPlanImagesModel: Int, Decodable {
    case redClock = 1
    case yellowClock
    case blueClock
    case greenClock
    case others

    var icon: UIImage? {
        switch self {
        case .redClock:
            return UIImage(named: "redClock")
        case .yellowClock:
            return UIImage(named: "yellowClock")
        case .blueClock:
            return UIImage(named: "blueClock")
        case .greenClock:
            return UIImage(named: "greenClock")
        case .others:
            return nil
        }
    }
}
