//
//  SavedStreamsBuyTimeCollectionCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsBuyTimeCollectionCell: UICollectionViewCell {

    // MARK: - Public Properties

    private(set) lazy var connectButtonTapped = bottomButton.rx.tap
    private(set) lazy var selectedPlan = collectionView.rx.modelSelected(SavedStreamsAudioVideoCollectionCellItemVM.self).map { $0.time }

    // MARK: - Private Properties

    var bag = DisposeBag()
    let items = PublishRelay<[SavedStreamsAudioVideoCollectionCellItemVM]>()

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let width = UIScreen.main.bounds.width / 2.34
        let height = width / 1.5
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumLineSpacing = 0
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.isScrollEnabled = false
        cvc.contentInset.left = UIScreen.isSE ? 10 : 13
        cvc.contentInset.right = UIScreen.isSE ? 10 : 13
        cvc.contentInset.top = UIScreen.isSE ? 7 : 3
        cvc.registerCells(withModels: SavedStreamsAudioVideoCollectionCellItemVM.self)
        return cvc
    }()

    private var bottomButton = SavedStreamsRoundedColoredButton(.connect)

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func configure(items: [SavedStreamsAudioVideoCollectionCellItemVM]) {
        initialSetup()
        bindUI()
        self.items.accept(items)
    }

    func activateButton(_ isEnabled: Bool) {
        bottomButton.isEnabled = isEnabled
    }

    // MARK: - Private Methods

    private func bindUI() {
        items
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func initialSetup() {
        addSubviews([collectionView, bottomButton])

        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(UIScreen.isSE ? 10 : 10)
            make.bottom.equalTo(bottomButton.snp.top)
        }

        bottomButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE  ? 37 : 48)
            make.trailing.leading.equalToSuperview().inset(UIScreen.isSE ? 45 : 57)
        }
    }
}
