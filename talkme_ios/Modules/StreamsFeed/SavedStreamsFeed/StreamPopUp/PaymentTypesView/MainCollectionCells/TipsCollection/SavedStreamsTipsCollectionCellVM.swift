//
//  SavedStreamsTipsCollectionCellVM.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsTipsCollectionCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public properties

    let onSendTips = PublishRelay<Int>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsTipsCollectionCell) {
        cell.configure()

        cell
            .onSendTips
            .bind(to: onSendTips)
            .disposed(by: cell.bag)
    }
}
