//
//  SavedStreamsCollectionDonatsCellVM.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsCollectionDonatsCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let sendSelectedDonat = PublishRelay<GetPlansResponse>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    var items = [SavedStreamsDonatsCollectionCellItemVM]()
    var donat: GetPlansResponse?

    // MARK: - Lifecycle

//    static var lastTime = CFAbsoluteTimeGetCurrent()

    init(giftsModel: [GetPlansResponse]) {
        items = giftsModel
            .sorted { $0.cost < $1.cost }
            .map { SavedStreamsDonatsCollectionCellItemVM(donat: $0) }
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsCollectionDonatsCell) {
        cell.configure(items: items)

        cell
            .selectedDonat
            .bind { [weak self, weak cell] donat in
//                Self.lastTime = CFAbsoluteTimeGetCurrent()
                self?.donat = donat
                cell?.activateButton(true)
            }
            .disposed(by: cell.bag)

        cell
            .sendButtonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                guard let donat = self.donat else { return }
                cell.buttonTouched(cell.bottomButton)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
                    self.sendSelectedDonat.accept(donat)
                }
            }
            .disposed(by: cell.bag)
    }
}
