//
//  SavedStreamsCollectionDonatsCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsCollectionDonatsCell: UICollectionViewCell {

    // MARK: - Public Properties

    private(set) lazy var sendButtonTapped = bottomButton.rx.tap
    private(set) lazy var selectedDonat = collectionView.rx.modelSelected(SavedStreamsDonatsCollectionCellItemVM.self).map { $0.donat}

    // MARK: - Private Properties

    var bag = DisposeBag()
    let items = PublishRelay<[SavedStreamsDonatsCollectionCellItemVM]>()

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height) / 3.8
        let height = width * 1.1
        layout.itemSize = CGSize(width: width, height: height)
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        cvc.indicatorStyle = .white
        cvc.registerCells(withModels: SavedStreamsDonatsCollectionCellItemVM.self)
        return cvc
    }()

     var bottomButton: SavedStreamsRoundedColoredButton = {
        let button = SavedStreamsRoundedColoredButton(.send)
        button.isEnabled = false
        return button
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func buttonTouched(_ sender: UIButton) {
        UIButton.animate(
            withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
                sender.alpha = 0.5
            },
            completion: { _ in
                UIButton.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform.identity
                    sender.alpha = 1
                })
            })
    }

    func configure(items: [SavedStreamsDonatsCollectionCellItemVM]) {
        bindUI()
        self.items.accept(items)
    }

    func activateButton(_ isEnabled: Bool) {
        bottomButton.isEnabled = isEnabled
    }

    // MARK: - Private Methods

    private func bindUI() {
        items
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func initialSetup() {
        addSubviews([collectionView, bottomButton])

        collectionView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview()
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -2 : -8)
            make.bottom.equalTo(bottomButton.snp.top).offset(UIScreen.isSE ? -10 : -13)
        }

        bottomButton.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE  ? 37 : 48)
            make.trailing.leading.equalToSuperview().inset(UIScreen.isSE ? 45 : 57)
            make.bottom.equalToSuperview()
        }
    }
}
