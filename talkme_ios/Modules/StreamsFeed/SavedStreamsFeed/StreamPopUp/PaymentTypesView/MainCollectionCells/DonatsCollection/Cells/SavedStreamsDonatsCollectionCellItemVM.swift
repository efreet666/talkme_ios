//
//  SavedStreamsDonatsCollectionCellItemVM.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

final class SavedStreamsDonatsCollectionCellItemVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let donat: GetPlansResponse

    // MARK: - Initializers

    init(donat: GetPlansResponse) {
        self.donat = donat
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsDonatsCollectionCellItem) {
        cell.configure(donat: donat)
    }
}
