//
//  SavedStreamsSpecialistDescriptionCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsSpecialistDescriptionCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let showMoreTapped = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let description: String

    // MARK: - Initializers

    init(_ description: String) {
        self.description = description
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsSpecialistDescriptionCell) {
        cell.configure(description)
        cell.showMoreTapped
            .bind(to: showMoreTapped)
            .disposed(by: cell.bag)
    }
}
