//
//  SavedStreamsSocialCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsSocialCell: UITableViewCell {

    // MARK: - Public properties

    private(set) lazy var modelSelected = socialCollection.rx.modelSelected(SavedStreamsSocialCollectionCellViewModel.self)
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = UIScreen.isSE
            ? CGSize(width: 35, height: 35)
            : CGSize(width: 46, height: 46)
        layout.minimumLineSpacing = UIScreen.isSE ? 13 : 16
        layout.scrollDirection = .horizontal
        return layout
    }()

    private lazy var socialCollection: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.registerCells(withModels: SavedStreamsSocialCollectionCellViewModel.self)
        collectionView .backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ models: [AnyCollectionViewCellModelProtocol]) {
        Observable.just(models)
            .do(afterNext: { [weak self] _ in
                self?.updateCollectionWidth()
            })
            .bind(to: socialCollection.rx.items) { collectionView, row, model in
                let index = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: model, for: index)
                model.configureAny(cell)
                return cell
            }.disposed(by: bag)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        contentView.addSubview(socialCollection)

        socialCollection.snp.makeConstraints { make in
            make.bottom.centerX.equalToSuperview()
            make.width.equalTo(0)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 16 : 21)
            make.height.equalTo(UIScreen.isSE ? 35 : 46)
        }
    }

    private func updateCollectionWidth() {
        let width = CGFloat(socialCollection.numberOfItems(inSection: 0))
            * layout.itemSize.width + (CGFloat(socialCollection.numberOfItems(inSection: 0)) - 1)
            * layout.minimumLineSpacing

        socialCollection.snp.updateConstraints { make in
            make.width.equalTo(width)
        }
    }
}
