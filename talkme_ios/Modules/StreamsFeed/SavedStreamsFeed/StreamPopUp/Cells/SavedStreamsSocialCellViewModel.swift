//
//  SavedStreamsSocialCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsSocialCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let socialTypeSelected = PublishRelay<SocialMedia>()
    let bag = DisposeBag()
    let socials: [SocialMedia]
    let info: PublicSpecialistInfo?

    // MARK: - Initializers

    init(_ info: PublicSpecialistInfo) {
        self.info = info
        var socials = [SocialMedia]()
        if info.vk != nil { socials.append(.vk) }
        if info.telegram != nil { socials.append(.telegram) }
        self.socials = socials
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsSocialCell) {
        let socialModels = socials.map { SavedStreamsSocialCollectionCellViewModel(categoryModel: $0, resp: info) }
        cell.configure(socialModels)

        cell.modelSelected
            .map { $0.socialsModel }
            .bind(to: socialTypeSelected)
            .disposed(by: cell.bag)
    }
}
