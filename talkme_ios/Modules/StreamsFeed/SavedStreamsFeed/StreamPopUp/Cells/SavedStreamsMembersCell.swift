//
//  SavedStreamsMembersCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa
import OpenTok

final class SavedStreamsMembersCell: UICollectionViewCell {

    // MARK: - Public properties

    private(set) lazy var reloadButtonTapped = membersList.reloadButtonTapped
    private(set) lazy var onConnectButtonTapped = membersList.onConnectButtonTapped
    private(set) var bag = DisposeBag()
    let camerasListRelay = PublishRelay<SavedStreamsSubscriberForPopup>()

    // MARK: - Private Properties

    private let membersList = SavedStreamsMembersListView()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func setupMembersList(list: SavedStreamsSubscriberForPopup, membersCount: String) {
        membersList.setupUI(camerasList: list, membersCount: membersCount)
    }

    func updateLearnerVideoState(learner: SavedStreamsAlteredStream) {
        membersList.updateMemberVideoState(model: learner)
    }

    func updateLearnerAudioState(learner: SavedStreamsAlteredStream) {
        membersList.updateMemberAudioState(model: learner)
    }

    func addNewLearner(learner: SavedStreamsCurrentSubscriber) {
        membersList.addNewLearner(learner: learner)
    }

    func removeLearner(learner: SavedStreamsCurrentSubscriber) {
        membersList.removeLearner(learner: learner)
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        contentView.addSubview(membersList)

        membersList.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 5 : 16)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
}
