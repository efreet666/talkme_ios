//
//  SavedStreamsMembersCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa
import OpenTok

final class SavedStreamsMembersCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public properties

    let refreshMembersList = PublishRelay<Void>()
    let onConnectButtonTappedRelay = PublishRelay<Void>()
    let camerasListRelay = PublishRelay<SavedStreamsPopupData>()
    let bag = DisposeBag()
    let onUpdateLearnerVideoState = PublishRelay<SavedStreamsAlteredStream>()
    let onUpdateLearnerAudioState = PublishRelay<SavedStreamsAlteredStream>()
    let onAddLearnerOnPopup = PublishRelay<SavedStreamsCurrentSubscriber>()
    let onRemoveLearnerFromPopup = PublishRelay<SavedStreamsCurrentSubscriber>()

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsMembersCell) {
        cell
            .onConnectButtonTapped
            .bind(to: onConnectButtonTappedRelay)
            .disposed(by: cell.bag)

        cell
            .reloadButtonTapped
            .bind { [weak self] _ in
                self?.refreshMembersList.accept(())
            }
            .disposed(by: cell.bag)

        camerasListRelay
            .bind { [weak cell] data in
                cell?.setupMembersList(list: data.subscriberForPopup, membersCount: data.membersCount)
            }
            .disposed(by: cell.bag)

        onUpdateLearnerVideoState
            .bind { [weak cell] learner in
                cell?.updateLearnerVideoState(learner: learner)
            }
            .disposed(by: cell.bag)

        onUpdateLearnerAudioState
            .bind { [weak cell] learner in
                cell?.updateLearnerAudioState(learner: learner)
            }
            .disposed(by: cell.bag)

        onAddLearnerOnPopup
            .bind { [weak cell] learner in
                cell?.addNewLearner(learner: learner)
            }
            .disposed(by: cell.bag)

        onRemoveLearnerFromPopup
            .bind { [weak cell] learner in
                cell?.removeLearner(learner: learner)
            }
            .disposed(by: cell.bag)
    }

}
