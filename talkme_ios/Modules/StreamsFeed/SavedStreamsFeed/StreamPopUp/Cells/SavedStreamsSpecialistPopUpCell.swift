//
//  SavedStreamsSpecialistPopUpCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsSpecialistPopUpCell: UICollectionViewCell {

    // MARK: - Private Properties

    private var bag = DisposeBag()

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.registerCells(
            withModels: SavedStreamsSpecialistInfoCellViewModel.self,
            SavedStreamsSocialCellViewModel.self,
            SavedStreamsSpecialistDescriptionCellViewModel.self
        )
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.bounces = false
        return tableView
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ viewModel: SavedStreamsSpecialistPopUpCellViewModel) {
        viewModel
            .items
            .bind(to: tableView.rx.items) { tableView, row, model in
                let index = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: model, for: index)
                model.configureAny(cell)
                return cell
            }.disposed(by: bag)

        viewModel
            .expandCompactDescription
            .bind { [weak self] in
                self?.tableView.beginUpdates()
                self?.tableView.endUpdates()
            }.disposed(by: bag)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        contentView.addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalToSuperview()
        }
    }
}
