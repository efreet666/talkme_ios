//
//  SavedStreamsPaymentTypesCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsPaymentTypesCell: UICollectionViewCell, UICollectionViewDelegateFlowLayout {

    // MARK: - Public Properties

    var isTapButton = true
    var bag = DisposeBag()
    let items = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    private(set) lazy var audioVideoTap = audioAndVideoButton.buttonTapped
    private(set) lazy var onAudioTap = PublishRelay<Void>()

    // MARK: - Private Properties

    private let donatsButton = SavedStreamsChangePaymentTypeButton(type: .donats)
    private let audioAndVideoButton = SavedStreamsAudioAndVideoImagesView()
    private let tipsButton = SavedStreamsChangePaymentTypeButton(type: .tips)

    private let buttonsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.codeCountry
        return view
    }()

    private let balanceContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.balanceViewGray
        view.layer.cornerRadius = UIScreen.isSE ? 20 : 25
        return view
    }()

    private let balanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        label.text = "stream_feed_balance".localized
        return label
    }()

    private let balanceView: StreamPriceView = {
        let view = StreamPriceView()
        view.configure(viewType: .streamFeedBalance(price: ""))
        return view
    }()

    private let topUpBalanceButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "topUpBalance"), for: .normal)
        button.contentEdgeInsets = UIScreen.isSE
            ? UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            : UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
        return button
    }()

    private let layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        var height: CGFloat {
            UIApplication.shared.statusBarOrientation.isPortrait
                ? (UIScreen.isSE ? 246 : 323)
                : 170
        }
        layout.itemSize = CGSize(width: width, height: height)
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.isScrollEnabled = false
        cvc.registerCells(withModels:
                            SavedStreamsCollectionDonatsCellVM.self,
                          SavedStreamsBuyTimeCollectionCellVM.self,
                          SavedStreamsTipsCollectionCellVM.self,
                          SavedStreamsTopUpBalanceCollectionCellVM.self
        )
        return cvc
    }()

    private let topButtonsStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        return stack
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(isOnlyPurchases: Bool) {
        donatsButton.isHidden = isOnlyPurchases
        audioAndVideoButton.isHidden = isOnlyPurchases
        tipsButton.isHidden = isOnlyPurchases
        topUpBalanceButton.isEnabled = !isOnlyPurchases
        bindUI()
    }

    func updateBalance(coins: Int) {
        balanceView.configure(viewType: .streamFeedBalance(price: String(coins)))
    }

    func scrollToGetCoins() {
        showTopUpBalance()
    }

    func showTopUpBalanceIsNotSubscribe() {
        donatsButton.setOn(false)
        audioAndVideoButton.setOn(false)
        tipsButton.setOn(false)
        let indexpath = IndexPath(item: 3, section: 0)
        collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: false)
        self.isTapButton = false
    }

    func setupCollection(cells: [AnyCollectionViewCellModelProtocol]) {
        self.items.accept(cells)
        donatsButtonSetOn()
    }

    func showAudioVideo() {
        donatsButton.setOn(false)
        audioAndVideoButton.setOn(true)
        tipsButton.setOn(false)
        let indexpath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: false)
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        collectionView.endEditing(true)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindUI() {
        donatsButton.buttonTapped
            .bind { [weak self] _ in
                self?.showDonats()
            }
            .disposed(by: bag)

        audioAndVideoButton.buttonTapped
            .bind { [weak self] _ in
                self?.showAudioVideo()
                self?.onAudioTap.accept(())
            }
            .disposed(by: bag)

        tipsButton.buttonTapped
            .bind { [weak self] _ in
                self?.showTips()
            }
            .disposed(by: bag)

        topUpBalanceButton.rx.tap
            .bind { [weak self] _ in
                self?.hideKeyboard()
                self?.showTopUpBalance()
            }
            .disposed(by: bag)

        items
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func hideKeyboard() {
        endEditing(true)
    }

    private func donatsButtonSetOn() {
        donatsButton.setOn(true)
        audioAndVideoButton.setOn(false)
        tipsButton.setOn(false)
    }

    private func showDonats() {
        donatsButtonSetOn()
        let indexpath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: false)
    }

    private func showTips() {
        donatsButton.setOn(false)
        audioAndVideoButton.setOn(false)
        tipsButton.setOn(true)
        let indexpath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: false)
    }

    private func showTopUpBalance() { // TODO: - rewrite indexes for scroll
        if isTapButton == true {
            donatsButton.setOn(false)
            audioAndVideoButton.setOn(false)
            tipsButton.setOn(false)
            let indexpath = IndexPath(item: 2, section: 0)
            collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: false)
            self.isTapButton = false
        } else {
            self.showDonats()
            self.isTapButton = true
        }
    }

    private func setUpConstraints() {
        topButtonsStack.addArrangedSubviews([donatsButton, tipsButton])
//        topButtonsStack.addArrangedSubviews([donatsButton, audioAndVideoButton, tipsButton])
        buttonsContainerView.addSubview(topButtonsStack)
        balanceContainerView.addSubviews([balanceLabel, balanceView, topUpBalanceButton])
        addSubviews([buttonsContainerView, balanceContainerView, collectionView])

        buttonsContainerView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 46 : 60)
        }

        topButtonsStack.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().inset(UIScreen.isSE ? 14 : 18)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 9 : 12)
        }

        balanceContainerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
            make.height.equalTo(UIScreen.isSE ? 40 : 50)
            make.top.equalTo(buttonsContainerView.snp.bottom).offset(UIScreen.isSE ? 6 : 7)
        }

        balanceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 14 : 18)
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 12 : 15)
            make.trailing.equalTo(balanceView.snp.leading)
        }

        balanceView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
        }

        topUpBalanceButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 0 : 5)
            make.size.equalTo(40)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 3 : 10)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(balanceContainerView.snp.bottom).offset(UIScreen.isSE ? 13 : 16)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
}

