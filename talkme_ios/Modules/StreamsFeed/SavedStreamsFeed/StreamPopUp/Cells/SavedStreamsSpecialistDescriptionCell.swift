//
//  SavedStreamsSpecialistDescriptionCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa

final class SavedStreamsSpecialistDescriptionCell: UITableViewCell {

    private enum Constants {
        static let labelInsets: CGFloat = UIScreen.isSE ? 10 : 13
    }

    private enum ButtonStyle {
        case showMore
        case showLess

        var title: NSAttributedString {
            switch self {
            case .showMore:
                return NSAttributedString(
                    string: "talkme_popup_show_more".localized,
                    attributes: [
                        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
                    ]
                )
            case .showLess:
                return NSAttributedString(
                    string: "talkme_popup_show_less".localized.localized,
                    attributes: [
                        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
                    ]
                )
            }
        }

        var image: UIImage? {
            switch self {
            case .showMore:
                return UIImage(named: "dropDownIcon")?.withRenderingMode(.alwaysTemplate)
            case .showLess:
                return UIImage(named: "rollUpIcon")?.withRenderingMode(.alwaysTemplate)
            }
        }
    }

    // MARK: - Public properties

    private(set) lazy var showMoreTapped = showMoreButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private var isExpanded: Bool = false

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = .white
        label.numberOfLines = 2
        label.isUserInteractionEnabled = false
        return label
    }()

    private let imageButton = UIImageView()

    private let showMoreButton: UIButton = {
        let button = UIButton(type: .custom)
        button.semanticContentAttribute = .forceRightToLeft
        button.titleLabel?.textColor = TalkmeColors.blueLabels
        button.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        button.tintColor = TalkmeColors.blueLabels
        return button
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpConstraints()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ description: String) {
        descriptionLabel.text = description
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.23
        paragraphStyle.lineBreakMode = .byTruncatingTail
        descriptionLabel.attributedText = NSMutableAttributedString(
            string: description,
            attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle]
        )
        if descriptionLabel.isTruncatedOrNot(UIScreen.main.bounds.width - Constants.labelInsets * 2) {
            showMoreButton.isHidden = false
            imageButton.isHidden = false
        } else {
            showMoreButton.isHidden = true
            imageButton.isHidden = true
        }
        isExpanded
            ? setButtonAppearance(.showLess)
            : setButtonAppearance(.showMore)
        bindUI()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func setUpConstraints() {
        contentView.addSubviews(descriptionLabel, showMoreButton, imageButton)

        showMoreButton.snp.makeConstraints { make in
            make.leading.equalTo(descriptionLabel)
            make.height.equalTo(UIScreen.isSE ? 18 : 23)
            make.top.equalTo(descriptionLabel.snp.bottom).offset(UIScreen.isSE ? 9 : 17)
            make.bottom.equalToSuperview()
        }

        imageButton.snp.makeConstraints { make in
            make.leading.equalTo(showMoreButton.snp.trailing).offset(UIScreen.isSE ? 5 : 8)
            make.centerY.equalTo(showMoreButton.snp.centerY).offset(2)
        }

        descriptionLabel.snp.remakeConstraints { make in
            make.top.equalToSuperview().inset(12)
            make.leading.trailing.equalToSuperview().inset(Constants.labelInsets)
        }
    }

    private func bindUI() {
        showMoreButton.rx
            .tap
            .bind { [weak self] _ in
                if self?.isExpanded == false {
                    self?.setButtonAppearance(.showLess)
                    self?.makeExpandedConstraints()
                    self?.isExpanded = true
                } else {
                    self?.setButtonAppearance(.showMore)
                    self?.makeCompactedConstraints()
                    self?.isExpanded = false
                }
            }.disposed(by: bag)
    }

    private func cellStyle() {
        selectionStyle = .none
        backgroundColor = .clear
    }

    private func setButtonAppearance(_ style: ButtonStyle) {
        showMoreButton.setAttributedTitle(style.title, for: .normal)
        imageButton.image = style.image
    }

    private func makeExpandedConstraints() {
        UIView.transition(
            with: descriptionLabel,
            duration: 0.3,
            options: .beginFromCurrentState
        ) {
            self.descriptionLabel.numberOfLines = 0
        }
    }

    private func makeCompactedConstraints() {
        UIView.transition(
            with: descriptionLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.descriptionLabel.numberOfLines = 2
        }
    }
}
