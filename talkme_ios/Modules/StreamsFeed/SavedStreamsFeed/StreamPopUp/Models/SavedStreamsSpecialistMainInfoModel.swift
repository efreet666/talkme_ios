//
//  SavedStreamsSpecialistMainInfoModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import Foundation

struct SavedStreamsSpecialistMainInfoModel {

    enum Sex {
        case male
        case female
    }

    let fullName: String
    let age: String
    let avatarUrl: String
    let classNumber: String
    let sex: Sex
    let location: String
    let userName: String
    let id: Int
    let awardsNumber: String
    let rating: Double
    let isContact: Bool
}
