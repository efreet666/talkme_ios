//
//  SavedStreamsComplaintPopUpView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift
import RxCocoa
import UIKit

final class SavedStreamsComplaintPopUpView: UIView, SavedStreamsDrawerContentView {

    private enum Constants {
        static let topToSwipeLine: CGFloat = UIScreen.isSE ? 8 : 11
        static let swipeLineSize = UIScreen.isSE
            ? CGSize(width: 97, height: 3)
            : CGSize(width: 126, height: 4)
        static let leftOffest: CGFloat = 25
    }

    // MARK: - Public properties

    var onDismiss: (() -> Void)?
    let bag = DisposeBag()

    // MARK: - Private Properties

    let viewHeight = BehaviorRelay<CGFloat>(
        value: (UIScreen.isSE ? 404 : 523)
    )

    enum Flow {
        case dissmissPopupWithSuccess
        case dissmissPopupWithError
    }

    let flow = PublishRelay<Flow>()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 20 : 25)
        label.numberOfLines = 2
        label.textAlignment = .left
        label.text = "complaint_title".localized
        return label
    }()

    private let reasonComplaintTitle: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        label.text = "complaint_subtitle".localized
        return label
    }()

    private let swipeLineView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = .white
        return view
    }()

    private let sendComplaintButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = TalkmeColors.blueLabels
        button.titleLabel?.font = UIFont.montserratExtraBold(ofSize:  UIScreen.isSE ? 15 : 17)
        button.setTitle("complaint_button".localized, for: .normal)
        button.setTitleColor( TalkmeColors.grayLabel, for: .highlighted)
        return button
    }()

    private let accountService: AccountService
    private let userAboutId: Int

    init(accountService: AccountService, userAbout: Int) {
        self.userAboutId = userAbout
        self.accountService = accountService
        super.init(frame: .zero)
        setUpAppearance()
        setUpConstraints()
        bindUI()
        AppDelegate.deviceOrientation = .portrait
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
    }

    // MARK: - Public Methods

    override func didMoveToSuperview() {
        guard superview != nil else { return }
    }

    func updateLayout(height: CGFloat) {
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

    private let radioButtonsView: SavedStreamsRadioButtonsView = {
        let radioButtonView = SavedStreamsRadioButtonsView()
        return radioButtonView
    }()

    private func setUpConstraints() {
        addSubviews(
            swipeLineView,
            titleLabel,
            reasonComplaintTitle,
            sendComplaintButton,
            radioButtonsView
        )

        swipeLineView.snp.makeConstraints { make in
            make.size.equalTo(Constants.swipeLineSize)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(Constants.topToSwipeLine)
        }

        titleLabel.snp.makeConstraints { make in
            make.left.equalTo(self.snp.left).inset(Constants.leftOffest)
            make.width.equalTo(UIScreen.width - Constants.leftOffest * 2)
            make.top.equalTo(swipeLineView.snp.bottom).offset(UIScreen.isSE ? 20 : 25)
        }

        reasonComplaintTitle.snp.makeConstraints { make in
            make.left.equalTo(self.snp.left).inset(Constants.leftOffest)
            make.width.equalTo(UIScreen.width - Constants.leftOffest * 2)
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 16 : 19)
        }

        radioButtonsView.snp.makeConstraints { make in
            make.left.equalTo(self.snp.left).inset(Constants.leftOffest)
            make.height.equalTo(UIScreen.isSE ? 240 : 240)
            make.width.equalTo(250)
            make.top.equalTo(reasonComplaintTitle.snp.bottom).offset(UIScreen.isSE ? 16 : 19)
        }

        sendComplaintButton.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.width * 0.9)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
            self.sendComplaintButton.layer.cornerRadius = UIScreen.isSE ? 21 : 27
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.snp.bottom).inset(15)
        }
    }

    private func sendComplaints() {
        guard let key = radioButtonsView.currentComplaint?.key,
              let value = radioButtonsView.currentComplaint?.value
        else { return }
        accountService.complaintCreate(toUserId: userAboutId, compliantType: key, compliantString: value)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.statusCode == 201
                    else {
                        self?.flow.accept(.dissmissPopupWithError)
                        return
                    }
                    self?.flow.accept(.dissmissPopupWithSuccess)
                case .error(let error):
                    print(error)
                    self?.flow.accept(.dissmissPopupWithError)
                }
            }
            .disposed(by: bag)
    }

    private func setupRadioButtons(complaints: Complaints) {
        radioButtonsView.complaints = complaints
    }

    private func bindUI() {
        accountService.getComplaints()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.setupRadioButtons(complaints: response)
                case .error(let error):
                    print(error)
                }
            }
        sendComplaintButton.rx
            .tap
            .throttle(0.5, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.sendComplaints()
            })
            .disposed(by: bag)
    }

    @objc private func hideKeyboard() {
        endEditing(true)
    }

    private func setUpAppearance() {
        backgroundColor = TalkmeColors.streamPopUpGray
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        layer.cornerRadius = 13
        clipsToBounds = true
    }
}
