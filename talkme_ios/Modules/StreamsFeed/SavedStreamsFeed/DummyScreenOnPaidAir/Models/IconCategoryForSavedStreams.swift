//
//  IconCategoryForSavedStreams.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import Foundation
import UIKit

enum IconCategoryForSavedStreams: String {
    case cooking = "Кулинария"
    case fitness = "Фитнес"
    case education = "Образование"
    case languages = "Языки"
    case other = "Разное"
    case yourShow = "Твоё шоу"
    case kids = "Kids"
    case music = "Музыка"
    case business = "Бизнес"
    case games = "Игры"
    case learning = "Обучение"
    case entertainment = "Развлечение"
    case courses = "Курсы"
    case chatting = "Общение"
    case girls = "Женское"
    case concerts = "Концерты"
    case travel = "Travel"
    case nightlife = "Night life"

    // MARK: - Public properties

    var icon: UIImage? {
        switch self {
        case .fitness:
            return UIImage(named: "fitness")
        case .learning:
            return UIImage(named: "learning")
        case .concerts:
            return UIImage(named: "concerts")
        case .entertainment:
            return UIImage(named: "entertainment")
        case .other:
            return UIImage(named: "other")
        case .chatting:
            return UIImage(named: "chatting")
        case .nightlife:
            return UIImage(named: "nightlife")
        case .courses:
            return UIImage(named: "courses")
        case .girls:
            return UIImage(named: "girls")
        case .kids:
            return UIImage(named: "kids")
        case .education:
            return UIImage(named: "education")
        case .cooking:
            return UIImage(named: "cooking")
        case .travel:
            return UIImage(named: "travel")
        case .business:
            return UIImage(named: "business")
        case .languages:
            return UIImage(named: "languages")
        case .music:
            return UIImage(named: "music")
        case .yourShow:
            return UIImage(named: "yourShow")
        case .games:
            return UIImage(named: "games")
        }
    }
}

