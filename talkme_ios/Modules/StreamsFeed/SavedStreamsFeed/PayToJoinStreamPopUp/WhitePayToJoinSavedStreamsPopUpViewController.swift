//
//  WhitePayToJoinPopUpForSavedStreamsViewController.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxCocoa
import RxSwift

final class WhitePayToJoinSavedStreamsPopUpViewController: UIViewController {

    enum Flow {
        case dismiss
        case buyAndDismiss
    }

    // MARK: - Public Properties

    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private lazy var payToJoinStream = WhitePayToJoinSavedStreamPopUpView(frame: .zero, cost: cost)
    private let cost: Int

    // MARK: - Initializers

    init(cost: Int) {
        self.cost = cost
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = TalkmeColors.lightBlackBalanceWithAlpha
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        bind()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        view.addSubview(payToJoinStream)
        payToJoinStream.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(UIScreen.isSE ? 299 : 405)
            make.height.equalTo(UIScreen.isSE ? 196 : 272)
        }
    }

    private func bind() {
        payToJoinStream
            .closePopUpTap
            .bind { [weak self] _ in
                self?.flow.accept(.dismiss)
            }
            .disposed(by: bag)

        payToJoinStream
            .toBuyButtonTapped
            .bind { [weak self] _ in
                self?.flow.accept(.buyAndDismiss)
            }
            .disposed(by: bag)
    }
}
