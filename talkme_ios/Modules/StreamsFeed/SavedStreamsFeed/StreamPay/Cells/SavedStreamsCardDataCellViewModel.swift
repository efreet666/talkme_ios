//
//  SavedStreamsCardDataCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import Foundation

import RxCocoa
import RxSwift

final class SavedStreamsCardDataCellViewModel: TableViewCellModelProtocol {

    // MARK: Public properties

    let cardNumberTextRelay = PublishRelay<String?>()
    let cardDateRelay = PublishRelay<String?>()
    let cardCodeRelay = PublishRelay<String?>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsCardDataTableCell) {

        cell
            .cardNumberTextRelay
            .bind(to: cardNumberTextRelay)
            .disposed(by: cell.bag)

        cell
            .cardDateTextRelay
            .bind(to: cardDateRelay)
            .disposed(by: cell.bag)

        cell
            .cardCodeTextRelay
            .bind(to: cardCodeRelay)
            .disposed(by: cell.bag)
    }
}
