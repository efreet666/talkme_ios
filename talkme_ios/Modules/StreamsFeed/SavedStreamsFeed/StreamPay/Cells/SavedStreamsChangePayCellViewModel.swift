//
//  SavedStreamsChangePayCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxCocoa
import RxSwift

enum SavedStreamsSegmentPaymentType: Int {
    case pay
    case card
}

final class SavedStreamsChangePayCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let segmentControlTap = PublishRelay<SavedStreamsSegmentPaymentType>()
    let bag = DisposeBag()
    let title: String?

    init(title: String?) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsChangePayTableCell) {
        cell
            .segmentControlTap
            .compactMap { SavedStreamsSegmentPaymentType(rawValue: $0) }
            .bind(to: segmentControlTap)
            .disposed(by: cell.bag)

        cell.configure(title: title)
    }
}
