//
//  SavedStreamsPayButtonTableCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

final class SavedStreamsPayButtonTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var payButtonTap = payButton.rx.tap
    private(set) lazy var payButtonIsEnabled = payButton.rx.isEnabled
    private(set) lazy var payButtonBackgroundColor = payButton.rx.backgroundColor
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let payButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = TalkmeColors.blueLabels
        button.setTitleColor(TalkmeColors.white, for: .normal)
        button.titleLabel?.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 17)
        // TODO: перенести title с аргументом, когда будет оплата
        button.setTitle("stream_feed_send_pay".localized, for: .normal)
        button.layer.cornerRadius = UIScreen.isSE ? 21 : 27
        return button
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ active: Bool) {
       payButton.backgroundColor = active ? TalkmeColors.blueCollectionCell : TalkmeColors.grayTimeSlider
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(payButton)

        payButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(5)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 46 : 56)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
        }
    }
}

