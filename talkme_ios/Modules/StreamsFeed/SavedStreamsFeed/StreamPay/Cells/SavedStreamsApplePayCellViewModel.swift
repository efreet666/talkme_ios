//
//  SavedStreamsApplePayCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxCocoa
import RxSwift

final class SavedStreamsApplePayCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let applePayButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: SavedStreamsApplePayTableCell) {

        cell
            .applePayButtonTap
            .bind(to: applePayButtonTap)
            .disposed(by: cell.bag)
    }
}
