//
//  savedStreamsCardDataTableCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

final class SavedStreamsCardDataTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var cardNumberTextRelay = cardNumberTextField.textRelay
    private(set) lazy var cardDateTextRelay = cardDateTextField.textRelay
    private(set) lazy var cardCodeTextRelay = cardCodeTextField.textRelay
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let cardNumberTextField = SavedStreamsPaymentEnterDataView(
        placeholder: "stream_feed_number_card".localized, formatPattern: "#### #### #### ####")
    private let cardDateTextField = SavedStreamsPaymentEnterDataView(
        placeholder: "stream_feed_month_year_card".localized, formatPattern: "##/##")
    private let cardCodeTextField = SavedStreamsPaymentEnterDataView(
        placeholder: "stream_feed_code_card".localized, formatPattern: "###")
    private let saveThisCardIndicator = RoundedIndicatorView()

    private let saveThisCardTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.placeholderColor
        lbl.text = "stream_feed_remembered_card".localized
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        return lbl
    }()

    private let rememberedView = UIView()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        bindUI()
        saveThisCardIndicator.setOn(false)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func bindUI() {
        rememberedView.rx.tapGesture()
            .when(.recognized)
            .bind { [weak self] _ in
                self?.saveThisCardIndicator.setOn(true)
            }
            .disposed(by: bag)
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        rememberedView.backgroundColor = .clear
        rememberedView.addSubviews([saveThisCardIndicator, saveThisCardTitleLabel])
        contentView.addSubviews([cardNumberTextField, cardDateTextField, cardCodeTextField, rememberedView])

        cardNumberTextField.snp.makeConstraints { make in
            make.top.equalTo(UIScreen.isSE ? 14 : 18)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(UIScreen.isSE ? 36 : 46)
        }

        cardDateTextField.snp.makeConstraints { make in
            make.top.equalTo(cardNumberTextField.snp.bottom).offset(UIScreen.isSE ? 10 : 12)
            make.leading.equalTo(cardNumberTextField.snp.leading)
            make.trailing.equalTo(cardCodeTextField.snp.leading).offset(UIScreen.isSE ? -6 : -8)
            make.height.equalTo(UIScreen.isSE ? 36 : 46)
        }

        cardCodeTextField.snp.makeConstraints { make in
            make.top.equalTo(cardNumberTextField.snp.bottom).offset(UIScreen.isSE ? 10 : 12)
            make.trailing.equalToSuperview().offset(UIScreen.isSE ? -10 : -12)
            make.width.equalTo(cardDateTextField.snp.width)
            make.height.equalTo(UIScreen.isSE ? 36 : 46)
        }

        rememberedView.snp.makeConstraints { make in
            make.top.equalTo(cardCodeTextField.snp.bottom).offset(UIScreen.isSE ? 21 : 25)
            make.leading.equalTo(cardNumberTextField.snp.leading)
            make.height.equalTo(30)
            make.trailing.equalToSuperview().inset(50)
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -4 : -6)
        }

        saveThisCardIndicator.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 16 : 21)
            make.centerY.equalToSuperview()
            make.leading.equalTo(rememberedView.snp.leading)
        }

        saveThisCardTitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(saveThisCardIndicator.snp.trailing).offset(UIScreen.isSE ? 8 : 10)
            make.centerY.equalTo(saveThisCardIndicator.snp.centerY)
        }
    }
}

