//
//  PopupPayDataForSavedStreams.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

final class SavedStreamsPopupPayData {
    var card: String?
    var cardDate: String?
    var cardCode: String?

    init(card: String?, cardDate: String?, cardCode: String?) {
        self.card = card
        self.cardDate = cardDate
        self.cardCode = cardCode
    }
}
