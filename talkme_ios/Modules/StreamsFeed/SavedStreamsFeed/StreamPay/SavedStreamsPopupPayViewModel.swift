//
//  SavedStreamsPopupPayViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxCocoa
import RxSwift

final class SavedStreamsPopupPayViewModel {

    enum Flow {
        case dismiss
    }

    // MARK: - Public Properties

    var updateHeight = BehaviorRelay<CGFloat>(
        value: (UIScreen.isSE ? 197 : 254) + UIApplication.bottomInset)
    let flow = PublishRelay<Flow>()
    let dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let bag = DisposeBag()

    // MARK: - Private properties

    private var popupPayData = SavedStreamsPopupPayData(card: nil, cardDate: nil, cardCode: nil)
    private let changePayCellViewModel = SavedStreamsChangePayCellViewModel(title: nil)
    private let applePayCellViewModel = SavedStreamsApplePayCellViewModel()
    private let cardDataCellViewModel = SavedStreamsCardDataCellViewModel()
    private let payButtonCellViewModel = SavedStreamsPayButtonCellViewModel()
    private let emptyCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? 90 : 120)
    private var payItems: [AnyTableViewCellModelProtocol] = []

    // MARK: - Initializers

    init() {
        bindViewModel()
    }

    // MARK: - Public Methods

    func setupApplePayItems() {
        self.payItems = [changePayCellViewModel, applePayCellViewModel]
        self.dataItems.accept(payItems)
    }

    // MARK: - Pirvate Methods

    private func setupCardPayItems() {
        self.payItems = [changePayCellViewModel, cardDataCellViewModel, emptyCellViewModel, payButtonCellViewModel]
        self.dataItems.accept(payItems)
    }

    private func bindViewModel() {
        changePayCellViewModel
            .segmentControlTap
            .bind { [weak self] type in
                switch type {
                case .pay:
                    self?.setupApplePayItems()
                    self?.updateHeight.accept(
                        (UIScreen.isSE ? 197 : 254) + UIApplication.bottomInset)
                case .card:
                    self?.setupCardPayItems()
                    self?.updateHeight.accept(
                        (UIScreen.isSE ? 420 : 531) + UIApplication.bottomInset)
                }
            }
            .disposed(by: changePayCellViewModel.bag)

        cardDataCellViewModel
            .cardNumberTextRelay
            .bind { [weak self] text in
                self?.popupPayData.card = text
            }
            .disposed(by: cardDataCellViewModel.bag)

        cardDataCellViewModel
            .cardDateRelay
            .bind { [weak self] text in
                self?.popupPayData.cardDate = text
            }
            .disposed(by: cardDataCellViewModel.bag)

        cardDataCellViewModel
            .cardCodeRelay
            .bind { [weak self] text in
                self?.popupPayData.cardCode = text
            }
            .disposed(by: cardDataCellViewModel.bag)

        Observable.combineLatest(
            cardDataCellViewModel.cardNumberTextRelay.compactMap { $0 },
            cardDataCellViewModel.cardDateRelay.compactMap { $0 },
            cardDataCellViewModel.cardCodeRelay.compactMap { $0 })
            .map { $0.0.count > 15 && $0.1.count > 3 && $0.2.count > 2 }
            .bind(to: payButtonCellViewModel.payButtonIsActive)
            .disposed(by: cardDataCellViewModel.bag)

        payButtonCellViewModel
            .payButtonTap
            .bind { [weak self] _ in
                guard let card = self?.popupPayData.card,
                      let cardYear = self?.popupPayData.cardDate,
                      let code = self?.popupPayData.cardCode else { return }
                let cardDate = cardYear.replacingOccurrences(of: "/", with: "")
                    // TODO: enter card data
            }
            .disposed(by: payButtonCellViewModel.bag)
    }
}
