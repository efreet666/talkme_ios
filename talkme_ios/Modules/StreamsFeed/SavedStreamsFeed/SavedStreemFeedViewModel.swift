//
//  SavedStreemFeedViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 8/7/2022.
//

import RxAppState
import RxCocoa
import RxSwift
import CollectionKit
import UIKit
import Reachability
import CallKit
import HaishinKit
import AVFoundation
import Logboard
import VideoToolbox
import Moya

protocol StreamsFeedViewModelFlowProtocol {
    var dismissVipLessonPopup: PublishSubject<Int> { get }
    var isSubscribe: Bool { get set }
    var isContactRelay: PublishRelay<Bool> { get }
    func sendNewGift(unauthUser: String, gift: Int)
}

final class SavedStreamsFeedViewModel: NSObject, InputDataForStreamPopUpViewModelProtocol, StreamsFeedViewModelFlowProtocol {

    enum RTMPConnectionState {
        case active
        case closed
        case loading
    }

    typealias LessonId = Int

    enum Flow {
        case onBack(animated: Bool = false)
        case onPopUp(
            page: Int,
            classNumber: String,
            lessonID: Int,
            isOwner: Bool,
            socketCamerasService: SocketCamerasService?
        )
        case finishStream
        case rateAndDonate(lessonId: LessonId)
        case reloadStreamsOnFinish(lessonId: Int)
        case onPopupWithOnlyPurchases(
            page: Int,
            lessonID: Int,
            socketCamerasService: SocketCamerasService?
        )
        case onPopupWithSendComplaint(userAboutID: Int)
        case onPopupThank(userAboutID: Int)
        case onPopupGift(userAboutID: Int)
        case onPopupProfileInfo(userAboutID: Int)
        case showCoineWriteOffPopup(lessonId: Int, cost: Int)
        case dismissPopUp
        case networkErrorPopUp(errorMessage: String?, action: () -> Void)
    }

    enum CustomMessages: String {
        case lessonFinished = "LessonFinished"
        case undefined
    }

    // MARK: - Public Properties

    // common
    var lessonDetails: [LessonId: LessonDetailResponse] = [:]
    let onGetLessons = PublishRelay<Void>()
    let onPresentLessonsIsEmptyAlert = PublishRelay<Void>()
    let streamIsUnrichable = BehaviorRelay<Bool>(value: false)
    var listOfStreams: [LessonId: LiveStream] = [:]

    private var userTypeStored: UserType?
    var userType: UserType = .viewer

    let sendMessage = PublishSubject<String>()
    let sendGift = PublishSubject<Int>()
    let flow = PublishRelay<Flow>()
    let finishStream = PublishRelay<Void>()
    let streamViewers = PublishRelay<Int>()
    let bag = DisposeBag()

    private var ownerStored: Bool = false
    var isOwner = false

    let playerIsPaused = PublishSubject<Bool>()
    let onRemoveLessonView = PublishSubject<Void>()
    var idsOfStreamsToShow: [LessonId] = []
    var currentLessonId: Int?
    var currentTeacherId: Int?

    var currentIndex: Int {
        get {
            idsOfStreamsToShow.firstIndex(of: currentLessonId ?? -1) ?? -1
        }
        set(selectedLessonsIndex) {
            guard selectedLessonsIndex < idsOfStreamsToShow.count, selectedLessonsIndex >= 0 else {
                assertionFailure("currentLessonId not found in lessons array")
                return
            }
            currentLessonId = idsOfStreamsToShow[selectedLessonsIndex]
        }
    }

    var currentClassNumber: String? {
        return listOfStreams[currentLessonId ?? 0]?.owner.numberClass
    }
    
    var currentLessonDetails: LessonDetailResponse? {
        return lessonDetails[currentLessonId ?? 0]
    }

    var currentLessonsDetails: LessonDetailResponse?
    var currentLessonDescription: LiveStream? {
        return listOfStreams[currentLessonId ?? 0]
    }

    var cameraIsActiveState = true
    var microOrPlayerIsActiveState = true

    var streamSoundIsMutedByUser = false
    var cameraIsFrontState = true
    let onShowLoader = PublishSubject<Bool>()

    var currentChatDataSource: ArrayDataSource<LessonMessage>? { return chatsDataSourceDict[currentLessonId ?? 0] }
    var chatsDataSourceDict = [Int: ArrayDataSource<LessonMessage>]()
    var appOnBackground = false

    // viewer
    let dataSource = ArrayDataSource<SavedStreamsFeedCellViewModel>()
    var cellModels: [SavedStreamsFeedCellViewModel] = []
    let onReadyToPresentVipLessonPopup = PublishRelay<Int>()
    let onDataSourceUpdate = BehaviorRelay<Int>(value: 0)
    var isSubscribe: Bool = false
    var cost = 0
    let onPresentVipLessonPopup = PublishRelay<VipLessonPopupViewForSavedStreams>()
    let onPlayerReadyForDisplay = PublishSubject<(AVPlayer?, Double)>()
    let onSetCameraStare = PublishSubject<Bool>()
    let onSetMicroStare = PublishSubject<Bool>()
    let onLessonFinished = BehaviorRelay<Bool>(value: false)
    let dismissVipLessonPopup = PublishSubject<Int>()
    let onUpdatePlayerSize = PublishSubject<CGSize>()
    var canUpdateCollection = true
    var isActivePictureInPicture = false

    // MARK: - Private Properties

    // common
    private var sendCameraSignalBag = DisposeBag()
    private var timer: Timer?
    private var lessonService: LessonsServiceProtocol
    private var authService = AuthService()
    private var gcoreService: GcoreServiceProtocol
    private var camerasSocketManager: SocketClientManager?
    private var socketCamerasService: SocketCamerasService?
    private var chatService: SocketLessonChatSrevice?
    private var accountService = AccountService()
    private var chatBag = DisposeBag()
    private var messagesCount = 0
    private var vipLessonPopupView: VipLessonPopupViewForSavedStreams?
    private var wasDisconnected = false
    private let reachabilityManager = ReachabilityManager()
    private var isNetworkActive = true
    private var loaderIsActive = false
    private(set) var infoStream: PublicProfileResponse?
    private(set) var isContactRelay = PublishRelay<Bool>()

    // viewer
    private var isNeedPresentVipLessonPopUp = false
    private var streamPlayer: AVPlayer?
    private var streamPlayerItem: AVPlayerItem?
    private var streamUrlString: String?
    private var playerBufferingObservationTimeRanges: NSKeyValueObservation?
    private var playerBufferingObservationisFull: NSKeyValueObservation?
    private var playerBufferingObservationIsEmpty: NSKeyValueObservation?
    private var playerItemKeepUpObservation: NSKeyValueObservation?
    private var playerItemStatusObservation: NSKeyValueObservation?
    private var playerTimeControlStatusObservation: NSKeyValueObservation?
    private var playerSizeObservation: NSKeyValueObservation?
    private var isNeedToPresentLoader = false
    private var isShowDonateAlert = false
    private(set) var isWatchingSave = false
    private var playerIsPausedByPlayButton = false
    private var playerIsPausedByTrackingSlider = false
    private var playerStartsPlayingFirstTime = true
    private var lastTimeRecievedFromSlider: Float = 0.0
    private var streamsCreatorId: Int?
    private var selectedCategoriesIds: [Int] = [0]
    private var recievedStreams: [LiveStream]
    let numberOfStreamsInPage: Int
    var numberOfLastPage: Int {
        Int(ceil(Float(recievedStreams.count) / Float(numberOfStreamsInPage)))
    }

    private var currentLessonsDurationInSeconds: Double {
        guard let lessonId = currentLessonId,
              let lessonsTime = listOfStreams[lessonId]?.lessonTime
        else { return 0 }
        let lessonTimeInSeconds = Formatters.getInterval(from: lessonsTime)
        return lessonTimeInSeconds
    }

    // Удалить когда станет ненужным (175, 176, 177)
    private(set) var status = ""
    var dataResponse: ((GcoreResponse?, String?) -> Void?)?
    var responceCopy: GcoreResponse?
    var showNotification: ((String, String) -> Void?)?
    private var maxCountOfNetworkRetries = 3
    private var currentCountOfNetworkRetries = 0

    // MARK: - Initializers

    init(
        lessonService: LessonsServiceProtocol,
        gcoreService: GcoreServiceProtocol,
        lessonId: Int?,
        streamsList: [LiveStream],
        numberOfStreamsInPage: Int,
        isWatchingSave: Bool = false,
        streamsCreatorId: Int? = nil,
        streamsCategoriesIds: [Int] = [0]
    ) {
        self.lessonService = lessonService
        self.gcoreService = gcoreService
        self.currentLessonId = lessonId
        self.isWatchingSave = isWatchingSave
        self.streamsCreatorId = streamsCreatorId
        self.selectedCategoriesIds = streamsCategoriesIds
        self.recievedStreams = streamsList
        self.numberOfStreamsInPage = numberOfStreamsInPage
        super.init()
        connectOnStart()
        bindAppState()
        bindVMInputsAndOutputs()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
        streamPlayer?.isMuted = true
    }

    // MARK: - Public Methods
    func addContact(id: Int) {
        accountService
            .addContact(id: id)
            .subscribe { event in
                switch event {
                case .success(let response):
                    print(response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func removeContact(id: Int) {
        accountService
            .removeContact(id: id)
            .subscribe { event in
                switch event {
                case .success(let response):
                    print(response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func loadNextPage() {        
        lessonService.savedStreams(withOwnersId: streamsCreatorId, page: numberOfLastPage + 1, count: numberOfStreamsInPage)
            .subscribe { [weak self] event in
                guard let self = self
                else { return }

                switch event {
                case .success(let response):
                    let streamsFromNewPage = response.data ?? []
                    self.recievedStreams += streamsFromNewPage
                    self.connectOnStart()
                case .error:
                    break
                }
            }
            .disposed(by: bag)
    }

    var videoEndObserver: Any?

    func removeVideoEndObserver() {
        guard let observer = videoEndObserver else { return }

        streamPlayer?.removeTimeObserver(observer)
        videoEndObserver = nil
    }

    func setPlayersCurrentTime(to time: Float, andPauseTheVideo pauseNeeded: Bool) {
        guard let player = streamPlayer
        else { return }

        playerIsPausedByTrackingSlider = pauseNeeded
        lastTimeRecievedFromSlider = time

        let cmTime = CMTimeMakeWithSeconds(Float64(time), preferredTimescale: 1000)

        if pauseNeeded {
            player.seek(to: cmTime, toleranceBefore: .zero, toleranceAfter: .zero)
            player.pause()
        } else {
            player.pause()
            if playerWillRichEnd(ifSeekTo: time) {
                playerIsPausedByPlayButton = true
            }
            player.seek(to: cmTime, toleranceBefore: .zero, toleranceAfter: .zero) { [weak self] _ in
                guard let self = self
                else { return }
                self.playerIsPausedByPlayButton ?  player.pause() :  player.play()
            }
        }
    }

    func changePlayerStateToOpposite() {
        guard let player = streamPlayer
        else { return }
        // we rich the end of video and now start video from the begining
        if playerWillRichEnd(ifSeekTo: lastTimeRecievedFromSlider) {
            let cmTime = CMTime(seconds: 0, preferredTimescale: 1000000)
            player.seek(to: cmTime)
            player.play()
            player.isMuted = streamSoundIsMutedByUser || isNeedPresentVipLessonPopUp
            playerIsPausedByPlayButton = false
        } else {
            // we dont rich the end of video and change players playing state to opposite
            playerIsPausedByPlayButton.toggle()
            if playerIsPausedByPlayButton {
                player.pause()
            } else {
                player.play()
                player.isMuted = streamSoundIsMutedByUser || isNeedPresentVipLessonPopUp
            }
        }
    }

    func repeatedLoginRequest(emailOrMobile: String, password: String) {
        let request = LoginRequest(emailOrMobile: emailOrMobile, password: password, rememberMe: true)
        authService
            .login(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.authToken != nil else { return }
                    self?.getStream(lessonId: self?.currentLessonId ?? 0)
                case .error(let error):
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                    appDelegate.makeLogout()
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func connectOnStart() {
        if streamsCreatorId == nil {
            getAllSavedLessonsIds()
        } else if let creatorId = streamsCreatorId {
            getSavedLessonsIds(createdBy: creatorId)
        }
    }

    func getLessonsOfSelectedCategoriesOnly(from savedStreamsArray: [LiveStream]) -> [LiveStream] {
        var result = savedStreamsArray
        if selectedCategoriesIds != [0] {
            result = savedStreamsArray.filter {
                guard let lessonCategory = $0.category,
                      let lessonCategoryId = lessonCategory.parent
                else { return false }
                return selectedCategoriesIds.contains(lessonCategoryId) }
        }
        return result
    }

    private func playerWillRichEnd(ifSeekTo: Float) -> Bool {
        let cmTime = CMTimeMakeWithSeconds(Float64(ifSeekTo), preferredTimescale: 1000)
        if  let videoDuration = streamPlayer?.currentItem?.asset.duration {
            let endTime =  videoDuration - CMTimeMakeWithSeconds(0.1, preferredTimescale: videoDuration.timescale)
            if cmTime.seconds.rounded(toPlaces: 3) >= endTime.seconds.rounded(toPlaces: 3) {
                return true
            }
        }
        return false
    }

    private func getAllSavedLessonsIds() {
        let filteredLessons = getLessonsOfSelectedCategoriesOnly(from: recievedStreams)
        idsOfStreamsToShow = filteredLessons.map { stream -> Int in stream.id }
        for stream in recievedStreams {
            listOfStreams[stream.id] = stream
        }
        if idsOfStreamsToShow.isEmpty {
            onPresentLessonsIsEmptyAlert.accept(())
            return
        }
        if self.currentLessonId == nil {
            currentLessonId = idsOfStreamsToShow.first
        }
        currentIndex = idsOfStreamsToShow.firstIndex(where: { $0 == self.currentLessonId }) ?? 0
        cellModels = idsOfStreamsToShow.map { streamFeedViewModel(lessonId: $0) }
        updateCellModels(lessonId: currentLessonId ?? 0)
        self.startSelectedStream()
    }

    private func getSavedLessonsIds(createdBy userId: Int) {
        let filteredLessons = getLessonsOfSelectedCategoriesOnly(from: recievedStreams)
        idsOfStreamsToShow = filteredLessons.map { stream -> Int in stream.id }
        for stream in recievedStreams {
            listOfStreams[stream.id] = stream
        }
        if idsOfStreamsToShow.isEmpty {
            onPresentLessonsIsEmptyAlert.accept(())
            return
        }
        if self.currentLessonId == nil {
            currentLessonId = idsOfStreamsToShow.first
        }
        currentIndex = idsOfStreamsToShow.firstIndex(where: { $0 == self.currentLessonId }) ?? 0
        cellModels = idsOfStreamsToShow.map { streamFeedViewModel(lessonId: $0) }
        updateCellModels(lessonId: currentLessonId ?? 0)
        self.startSelectedStream()
    }

    private func startSelectedStream() {
        self.lessonService
            .lessonDetail(id: self.currentLessonId ?? 0, saved: true)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let details):
                    self.lessonDetails[self.currentLessonId ?? 0] = details
                    self.isOwner = details.isOwner
                    self.isSubscribe = details.isSubscribe
                    self.onGetLessons.accept(())
                    self.getAllLessonsDetails(ids: self.idsOfStreamsToShow, fromStart: true)
                    self.bindViewerStreamComplete()
                case .error(let error):
                    self.restartNetwork(function: { [weak self] in self?.startSelectedStream() },
                                        ifFailedShowAlertWithError: error, andOurErrorCode: "91")
                    print(error)
                }
            }.disposed(by: self.bag)
    }

    func finishTraking() {
        AmplitudeProvider.shared.currentChatModel = currentChatDataSource ?? ArrayDataSource<LessonMessage>()
        AmplitudeProvider.shared.finishStreamTraking()
    }

//    func connectWithIndex(_ index: Int) {
//        finishTraking()
//        isNeedPresentVipLessonPopUp = false
//        currentIndex = index
//        removePlayer()
//        connectWithLessonId(currentLessonId ?? 0)
//        AmplitudeProvider.shared.startStreamTracking(lesson: currentLessonsDetails)
//        cameraIsActiveState = true
//        streamSoundIsMutedByUser = false
//        if let teacherId = currentLessonDescription?.owner.id {
//            currentTeacherId = teacherId
//        }
//    }
    func fetchData(classNumber: String) {
        accountService.streamPublicProfile(classNumber: classNumber)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let info):
                    self.infoStream = info
                    self.isContactRelay.accept(info.isContact)
                case .error(let error):
                    print(error.localizedDescription)
                }
            }
            .disposed(by: bag)
    }

    func connectWithIndex(_ index: Int) {
        self.onLessonFinished.accept(false)
        finishTraking()
        isNeedPresentVipLessonPopUp = false
        currentIndex = index
        connectWithLessonId(currentLessonId ?? 0)
        AmplitudeProvider.shared.startStreamTracking(lesson: currentLessonDetails)
        fetchData(classNumber: currentClassNumber ?? "")
        cameraIsActiveState = true
        microOrPlayerIsActiveState = true
        if let teacherId = currentLessonDetails?.owner.id {
            currentTeacherId = teacherId
        }
    }

    func handleOnComplaintButtonTap() {
        flow.accept(.onPopupWithSendComplaint(userAboutID: currentTeacherId ?? 0))
    }

    func handleOnGiftTap(_ onlyPurchases: Bool) {
        if onlyPurchases {
            flow.accept(.onPopupWithOnlyPurchases(page: 1, lessonID: currentLessonId ?? 0, socketCamerasService: socketCamerasService))
        } else {
            guard let classNumber = self.currentClassNumber else { return }
            flow.accept(.onPopUp(page: 1,
                                 classNumber: classNumber,
                                 lessonID: currentLessonId ?? 0,
                                 isOwner: isOwner,
                                 socketCamerasService: socketCamerasService))
        }
    }

    func handleLessonFinished() {
        onRemoveLessonView.onNext(())
        flow.accept(.onBack())
    }

    func handleMembersTap() {
        guard let classNumber = self.currentClassNumber else { return }
        flow.accept(.onPopUp(
            page: 3,
            classNumber: classNumber,
            lessonID: currentLessonId ?? 0,
            isOwner: isOwner,
            socketCamerasService: socketCamerasService
        ))
    }

    func handleOnBackTap() {
        flow.accept(.onBack())
    }
    func handleProfleInfoButtonTap() {
        flow.accept(.onPopupProfileInfo(userAboutID: currentLessonId ?? 0))
    }
    func handleThankButtonTap() {
        flow.accept(.onPopupThank(userAboutID: currentLessonId ?? 0))
    }
    
    func handleGiftButtonTap() {
        flow.accept(.onPopupGift(userAboutID: currentLessonId ?? 0))
    }

    func handleMicrophoneTap() {
        streamPlayer?.isMuted = !self.microOrPlayerIsActiveState
    }
    
    func sendNewGift(unauthUser: String, gift: Int) {
//        newGift(unauthUser: unauthUser, gift: gift)
    }

    // MARK: - Private Methods

    private func setupSession(session: AVAudioSession) {
        do {
            try session.setCategory(.playback, mode: .moviePlayback)
            try session.setActive(true)
        } catch {
            print(error)
        }
    }

    private func observeNetwork() {
        reachabilityManager.hasNetworkConnection
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self] connected in
                guard let self = self else { return }
                if connected {
                    self.isNetworkActive = true
                    guard self.wasDisconnected else { return }
                    self.wasDisconnected = false
                    self.getStream(lessonId: self.currentLessonId ?? 0)
                } else {
                    self.isNetworkActive = false
                    self.wasDisconnected = true
                }
            }).disposed(by: bag)
    }

    private func getStream(lessonId: Int) {
        guard let currentLessonResponse = listOfStreams[lessonId]
        else { return }
        self.updateCellModels(lessonId: lessonId)
        self.streamUrlString = currentLessonResponse.gcoreHlsUrl ?? ""
        self.configureViewersPlayer(streamUrlString: currentLessonResponse.gcoreHlsUrl ?? "", lessonId: lessonId)
    }

    private func exitTheLesson(fromBackground: Bool) {
        if fromBackground {
            onRemoveLessonView.onNext(())
            self.streamPlayer?.isMuted = true
            flow.accept(.rateAndDonate(lessonId: currentLessonId ?? 0))
        } else {
            self.streamPlayer?.isMuted = true
            self.onRemoveLessonView.onNext(())
            self.isShowDonateAlert = true
            self.flow.accept(.rateAndDonate(lessonId: currentLessonId ?? 0))
        }
    }

    private func bindAppState() {
        UIApplication.shared.rx
            .applicationWillEnterForeground
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.appOnBackground = false
                if !self.isActivePictureInPicture, !self.playerIsPausedByPlayButton {
                    self.streamPlayer?.isMuted = self.streamSoundIsMutedByUser || self.isNeedPresentVipLessonPopUp
                    self.streamPlayer?.play()
                }
            }
            .disposed(by: bag)

        UIApplication.shared.rx
            .applicationDidEnterBackground
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.appOnBackground = true
                if !self.isActivePictureInPicture {
                    self.streamPlayer?.isMuted = true
                    self.self.streamPlayer?.pause()
                }
            }
            .disposed(by: bag)
    }

    private func bindVMInputsAndOutputs() {
        dismissVipLessonPopup
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.isNeedPresentVipLessonPopUp = false
            }
            .disposed(by: bag)
    }

    private func showLoader() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
            guard let self = self, self.loaderIsActive == false, self.isNeedToPresentLoader else { return }
            self.onShowLoader.onNext(true)
            self.loaderIsActive = true
        }
    }

    private func hideLoader() {
        loaderIsActive = false
        isNeedToPresentLoader = false
        self.onShowLoader.onNext(false)
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)",
                                                    action: { self.flow.accept(.onBack()) }))
            }
        }
    }

    // MARK: - VIEWER METHODS

    func printInfo(_ items: Any) {
        print("[Viewer Stream] \(String(describing: items))")
    }

    func connectWithLessonId(_ lessonId: Int) {
        self.lessonService
            .lessonDetail(id: lessonId, saved: true)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let lessonDetails):
                    self.currentLessonsDetails = lessonDetails
                    self.isSubscribe = lessonDetails.isSubscribe
                    self.cost = lessonDetails.cost
                    self.isNeedPresentVipLessonPopUp = false
                    if lessonDetails.isSubscribe == false, lessonDetails.cost != 0, !self.isOwner {
                        self.isNeedPresentVipLessonPopUp = true
                        self.vipLessonPopupView = VipLessonPopupViewForSavedStreams(
                            lesson: lessonDetails,
                            categoryImage: IconCategoryForSavedStreams(rawValue: lessonDetails.categoryName.localized)?.icon
                        )
                        if let popupView = self.vipLessonPopupView {
                            self.bindVipLessonPopup(popupView: popupView, lesson: lessonDetails)
                        }
                    }
                case .error(let error):
                    self.restartNetwork(function: { [weak self] in self?.connectWithLessonId(lessonId) },
                                        ifFailedShowAlertWithError: error, andOurErrorCode: "1")
                }
                self.getStream(lessonId: lessonId)
            }
            .disposed(by: bag)
    }

    private func streamFeedViewModel(lessonId: Int) -> SavedStreamsFeedCellViewModel {
        let model = SavedStreamsFeedCellViewModel(streamModel: SavedStreamModel(id: lessonId,
                                                                                specialistInfo: listOfStreams[lessonId]?.owner,
                                                                                lessonDateEnd: nil,
                                                                                categoryId: listOfStreams[lessonId]?.category?.parent ?? 0))
        return model
    }

    private func updateCellModels(lessonId: Int) {
        cellModels = cellModels.map {
            guard $0.streamModel.id == lessonId, let savedStreamResponse = self.listOfStreams[lessonId] else { return $0 }
            let lessonDateEnd = Formatters.getIntervalWithDuration(duration: savedStreamResponse.lessonTime,
                                                                   dateStart: savedStreamResponse.date - Date.diffrenceBetweenRealTime)

            $0.streamModel = SavedStreamModel(id: lessonId,
                                              specialistInfo: savedStreamResponse.owner,
                                              lessonDateEnd: lessonDateEnd,
                                              categoryId: savedStreamResponse.category?.parent ?? 0)
            return $0
        }
        guard canUpdateCollection else { return }
        dataSource.data = cellModels
        onDataSourceUpdate.accept(recievedStreams.count)
    }

    private func bindVipLessonPopup(popupView: VipLessonPopupViewForSavedStreams, lesson: LessonDetailResponse) {
        popupView
            .flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .buyAndDismiss:
                    self.flow.accept(.showCoineWriteOffPopup(lessonId: lesson.id, cost: lesson.cost))
                case .dismiss:
                    self.handleOnBackTap()
                }
            }
            .disposed(by: bag)
    }

    private func bindViewerStreamComplete() {
        self.onReadyToPresentVipLessonPopup
            .bind { [weak self] id in
                guard let self = self,
                      self.isNeedPresentVipLessonPopUp,
                      let vipLessonPopupView = self.vipLessonPopupView,
                      id == self.currentLessonId,
                      vipLessonPopupView.lesson.id == id
                else { return }
                self.onPresentVipLessonPopup.accept(vipLessonPopupView)
                self.streamPlayer?.isMuted = true
            }.disposed(by: bag)
    }

    private func configureViewersPlayer(streamUrlString newUrlAbsoluteString: String, lessonId: Int) {
        guard lessonId == self.currentLessonId else { return }
        self.playerIsPausedByPlayButton = false
        self.playerIsPausedByTrackingSlider = false
        self.removePlayer()
        let session = AVAudioSession.sharedInstance()
        self.setupSession(session: session)
        guard let streamUrl = URL(string: newUrlAbsoluteString) else { return }

        let asset = AVAsset(url: streamUrl)
        let keys: [String] = ["playable", "hasProtectedContent"]

        asset.loadValuesAsynchronously(forKeys: keys) {
            DispatchQueue.main.async {
                let playerItem = AVPlayerItem(asset: asset)

                let streamPlayer = AVPlayer(playerItem: playerItem)
                self.streamPlayerItem = playerItem
                self.streamPlayer = streamPlayer
                self.observe(player: streamPlayer, lessonId: lessonId)
                self.observe(playerItem: playerItem, lessonId: lessonId)
                streamPlayer.isMuted = true

                self.onPlayerReadyForDisplay.onNext((streamPlayer, self.currentLessonsDurationInSeconds))

                NotificationCenter.default.removeObserver(self)

                NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(self.playerItemDidReachEnd(notification:)),
                    name: .AVPlayerItemDidPlayToEndTime,
                    object: streamPlayer
                )

                NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(self.playerItemStalled),
                    name: .AVPlayerItemPlaybackStalled,
                    object: streamPlayer
                )

                NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(self.playerItemDidReachEnd(notification:)),
                    name: .AVPlayerItemFailedToPlayToEndTime,
                    object: streamPlayer
                )
            }
        }
    }

    private func removePlayer() {
        print("[REMOVE PLAYER]")
        streamPlayerItem = nil
        streamPlayer = nil
        playerBufferingObservationTimeRanges = nil
        playerBufferingObservationisFull = nil
        playerBufferingObservationIsEmpty = nil
        playerItemStatusObservation = nil
        playerItemKeepUpObservation = nil
        playerTimeControlStatusObservation = nil
        playerSizeObservation = nil
    }

    func setPlayer(mute: Bool) {
        guard let player = streamPlayer, streamSoundIsMutedByUser else { return }
        player.isMuted = mute
    }

    @objc func playerItemDidReachEnd(notification: Notification) {
        guard (notification.object as? AVPlayerItem) == streamPlayer?.currentItem else { return }
        print("[playerItemDidReachEnd]")
        playerIsPausedByPlayButton = true
        self.refreshPlaying(lessonId: currentLessonId ?? 0)
    }

    @objc func playerItemStalled() {
        print("[playerItemStalled]")
        self.showLoader()
    }

    private func observe(player: AVPlayer?, lessonId: Int) {
        guard let player = player else {
            playerTimeControlStatusObservation = nil
            return
        }

        playerTimeControlStatusObservation = player.observe(\.timeControlStatus) { [weak self] player, _ in
            guard let self = self else { return }
            switch player.timeControlStatus {
            case .paused:
                print("[PAUSED]")
                print("PLAYER ERROR Paused \(String(describing: player.error?.localizedDescription))")
                self.playerIsPaused.onNext(true)
            case .waitingToPlayAtSpecifiedRate:
                print("[WAITING]")
                self.isNeedToPresentLoader = true
            case .playing:
                self.streamIsUnrichable.accept(false)
                if let videoDuration = self.streamPlayer?.currentItem?.duration,
                   videoDuration == .zero {
                    self.playerIsPaused.onNext(true)
                } else { self.playerIsPaused.onNext(false)}
            @unknown default:
                if !self.appOnBackground {
                    self.refreshPlaying(lessonId: lessonId)
                }
                print("[unknown default]")
            }
        }

        playerSizeObservation = player.currentItem?.observe(\.presentationSize, options: NSKeyValueObservingOptions()) {[weak self] _, _ in
            guard
                let self = self,
                let presentationSize = self.streamPlayer?.currentItem?.presentationSize,
                presentationSize != .zero
            else { return }

            self.onUpdatePlayerSize.onNext(presentationSize)
        }
    }

    private func observe(playerItem: AVPlayerItem?, lessonId: Int) {
        guard let playerItem = playerItem else {
            playerBufferingObservationTimeRanges = nil
            playerBufferingObservationisFull = nil
            playerBufferingObservationIsEmpty = nil
            playerItemStatusObservation = nil
            playerItemKeepUpObservation = nil
            return
        }

        playerBufferingObservationIsEmpty = playerItem.observe(\.isPlaybackBufferEmpty) { [weak self] item, _ in
            if item.isPlaybackBufferEmpty {
                self?.printInfo("[isPlaybackBufferEmpty]")
            } else {
                self?.printInfo("buffer is not empty")
            }
        }

        playerItemStatusObservation = playerItem.observe(\.status) { [weak self] item, _ in
            guard let self = self, let player = self.streamPlayer else { return }
            if item.status == .readyToPlay {

                if !player.isPlaying, !self.playerIsPausedByPlayButton, !self.playerIsPausedByTrackingSlider {
                    player.play()
                    player.isMuted = self.streamSoundIsMutedByUser || self.isNeedPresentVipLessonPopUp
                }
                self.printInfo("[READY TO PLAY]")
            } else if item.status == .failed {
                self.printInfo("[FAILED]")
                self.streamIsUnrichable.accept(true)
                self.refreshPlaying(lessonId: lessonId)
                if let error = item.error {
                    self.printInfo(error.localizedDescription)
                }
            }
        }

        playerItemKeepUpObservation = playerItem.observe(\.isPlaybackLikelyToKeepUp) { [weak self] item, _ in
            guard let self = self, let player = self.streamPlayer else { return }
            if item.isPlaybackLikelyToKeepUp {
                print("[isPlaybackLikelyToKeepUp = true]")
                if !player.isPlaying,
                   player.currentItem?.status == .readyToPlay,
                   !self.playerIsPausedByPlayButton,
                   !self.playerIsPausedByTrackingSlider {
                    player.play()
                    player.isMuted = self.streamSoundIsMutedByUser || self.isNeedPresentVipLessonPopUp
                }
            } else {
                print("[isPlaybackLikelyToKeepUp = false]")
            }
        }
    }

    private func refreshPlaying(lessonId: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { [weak self] in
            guard let streamUrl = self?.streamUrlString, !streamUrl.isEmpty else {
                print("[PLAYER NO REFRESHED]")
                return }
            self?.configureViewersPlayer(streamUrlString: streamUrl, lessonId: lessonId)
            print("[PLAYER REFRESHED]")
        }
    }

    private func getAllLessonsDetails(ids: [Int], fromStart: Bool) {
//        for stream in recievedStreams {
//            self.listOfStreams[stream.id] = stream
//        }
        if !cellModels.isEmpty {
            currentIndex = idsOfStreamsToShow.firstIndex(where: { $0 == currentLessonId }) ?? 0
        }
        guard fromStart else { return }
        self.connectWithIndex(self.currentIndex)
    }

    func updateCollectionDataSource(lessonIds: [Int]) {
        guard lessonIds.contains(where: { $0 == currentLessonId }) else { return }
        self.idsOfStreamsToShow = lessonIds
        cellModels = lessonIds.map { streamFeedViewModel(lessonId: $0) }
        guard !cellModels.isEmpty else { return }
        currentIndex = lessonIds.firstIndex(where: { $0 == currentLessonId }) ?? 0
        print("[swipe] UPDAte Collection every 30 sec")
        updateCellModels(lessonId: currentLessonId ?? 0)
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
