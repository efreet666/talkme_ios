//
//  SavedStreamsDonationPopupViewController.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

final class SavedStreamsDonationPopupViewController: UIViewController {

    // MARK: - Private Properties

    private let donationView = SavedStreamsDonationView(frame: .zero, withRating: true)
    private let bag = DisposeBag()
    private let viewModel: SavedStreamsDonationPopupViewModel

    // MARK: - Initializers

    init(viewModel: SavedStreamsDonationPopupViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        donationView.layer.cornerRadius = 14
        setupLayout()
        bindKeyboard()
        bindUI()
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    private func bindUI() {
        donationView.sendButtonTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                guard let text = self.donationView.textRelay.value else { return }
                if let tips = Int(text), (tips != 0) && !text.isEmpty {
                    self.viewModel.sendTips(tips: tips)
                }
                if let rating = self.donationView.ratingTap.value {
                    self.viewModel.sendRating(rating: rating)
                }
                self.viewModel.dismiss()
            }
            .disposed(by: donationView.bag)
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let isHide = keyboardVisibleHeight == 0
                let offset: CGFloat = UIScreen.isSE ? 270 : 350
                self.donationView.snp.updateConstraints { make in
                    make.centerY.equalToSuperview().offset(!isHide ?  keyboardVisibleHeight - offset : keyboardVisibleHeight)
                }
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    private func setupLayout() {
        view.addSubviews(donationView)

        donationView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            let width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height) - (UIScreen.isSE ? 20 : 14) * 2
            make.width.equalTo(width)
        }
    }
}

