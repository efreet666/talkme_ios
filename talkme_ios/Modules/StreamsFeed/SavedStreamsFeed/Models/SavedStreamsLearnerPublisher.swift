//
//  SavedStreamsLearnerPublisher.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import OpenTok

struct SavedStreamsLearnerPublisher {
    let publisher: OTPublisher?
    let timeEnd: Double
}
