//
//  SavedStreamsPopupData.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import OpenTok

struct SavedStreamsPopupData {
    let subscriberForPopup: SavedStreamsSubscriberForPopup
    let membersCount: String
}

struct SavedStreamsSubscriberForPopup: Equatable {
    var subscribersArray: [SavedStreamsCurrentSubscriber]
    let session: OTSession?
    let isOwner: Bool
}

struct SavedStreamsCurrentSubscriber: Equatable {
    let subscriber: OTSubscriber
    let timeEnd: Double
    let avatarUrl: String
    let blockedAudioByPub: Bool
    let blockedVideoByPub: Bool
    let isMicroOn: Bool?
    let subscriberHasVideo: Bool?
}

struct SavedStreamsAlteredStream {
    let subscriber: OTSubscriber
    var blockedAudioByPub = false
    var blockedVideoByPub = false
    let isMicroOn: Bool?
    let subscriberHasVideo: Bool?
}
