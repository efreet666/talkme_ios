//
//  AllSavedLessonsModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

struct AllSavedLessonsModel {

    static var liveLessonsArray = [LessonDetailResponse]().uniques
    static let lessonsUpdate = PublishSubject<Void>()

    static func updateLiveLessonsSet(lessonService: LessonsServiceProtocol, bag: DisposeBag, lessonId: Int) {
        Self.liveLessonsArray.enumerated().forEach { index, lesson in
            if lesson.id == lessonId {
                Self.liveLessonsArray.remove(at: index)
            }
        }

        lessonService.lessonDetail(id: lessonId, saved: false)
            .subscribe { event in
                switch event {
                case .success(let responce):
                    Self.liveLessonsArray.append(responce)
                    Self.lessonsUpdate.onNext(())
                case .error(let error):
                    print("AllLiveLessonsModel updateLiveLessonsSet error: \(error)")
                }
            }
            .disposed(by: bag)
    }
}
