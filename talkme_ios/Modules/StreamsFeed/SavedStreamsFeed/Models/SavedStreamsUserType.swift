//
//  SavedStreamsUserType.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

enum SavedStreamsUserType {
    case specialist
    case participant
    case viewer
}
