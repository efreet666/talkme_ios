//
//  SavedStreamsdDetailModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

struct SavedStreamsdDetailModel {
    let allStreamsShort: [LiveStreamShort]
    let currentStream: LiveStreamShort
    let index: Int
}
