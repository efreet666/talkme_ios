//
//  LimitsForSavedStreamMemberView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

final class LimitsForSavedStreamMemberView: UIView {

    // MARK: - Private Properties

    private let backgroundImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "mainBackground")
        img.clipsToBounds = true
        img.layer.cornerRadius = 20
        return img
    }()

    private let limitedMembersLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "popup_limited_member_of_stream".localized
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratBold(ofSize: 20)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()

    private let tryLaterLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "popup_try_later".localized
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratFontRegular(ofSize: 16)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()

    private let viewersImageView: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "streamViewers")
        img.contentMode = .scaleAspectFill
        return img
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(backgroundImage)
        backgroundImage.addSubviews(limitedMembersLabel, tryLaterLabel, viewersImageView)

        backgroundImage.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
            make.width.equalTo(319)
            make.height.equalTo(213)
        }

        viewersImageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(30)
            make.width.equalTo(66)
            make.height.equalTo(40)
            make.bottom.equalTo(limitedMembersLabel.snp.top).inset(-16)
        }

        limitedMembersLabel.snp.makeConstraints { make in
            make.top.equalTo(viewersImageView.snp.bottom).inset(16)
            make.leading.trailing.equalToSuperview().inset(23)
            make.bottom.equalTo(tryLaterLabel.snp.top).inset(-13)
        }

        tryLaterLabel.snp.makeConstraints { make in
            make.top.equalTo(limitedMembersLabel.snp.bottom).inset(13)
            make.leading.trailing.equalToSuperview().inset(35)
            make.bottom.equalToSuperview().inset(33)
        }
    }
}
