//
//  LimitsForSavedStreamMemberVC.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit
import RxSwift

final class LimitsForSavedStreamMemberVC: UIViewController {

    // MARK: - Private Properties

    private let limitsForStreamMemberView = LimitsForSavedStreamMemberView()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        view.addSubview(limitsForStreamMemberView)
        limitsForStreamMemberView.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
        }
    }
}
