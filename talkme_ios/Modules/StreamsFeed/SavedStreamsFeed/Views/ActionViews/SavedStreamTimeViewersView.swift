//
//  SavedStreamTimeViewersView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

final class SavedStreamTimeViewersView: UIView {

    private enum Constants {
        static let spacing: CGFloat = UIScreen.isSE ? 6 : 9
    }

    // MARK: - Public Properties

    private(set) lazy var size: CGSize = {
        let width = timeView.size.width + viewersView.size.width + Constants.spacing
        return .init(width: width, height: timeView.size.height)
    }()

    let lessonFinished = PublishSubject<Void>()

    // MARK: - Private Properties

    private let timeView = SavedStreamsAttributeView(type: .time)
    private let viewersView = SavedStreamsAttributeView(type: .viewers)
    private var timer: Timer?
    private var streamIsSaved = false

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        addSubviews(timeView, viewersView)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupLayout()
    }

    // MARK: - Public Methods

    func startTimerForLiveStream(dateEnd: Double) {
        timeView.alpha = 1
        streamIsSaved = false
        let date = Date()
        let currentTime = date.timeIntervalSince1970
        let lessonEnd = TimeInterval(dateEnd)
        let lessonTime = lessonEnd - currentTime
        self.timeView.configure(text: self.getLessonTime(timeEnd: lessonTime))
        startTimer(withEndDate: lessonEnd)
    }

    func startTimerForSavedStream(withDuration duration: Double) {
        streamIsSaved = true
        let endDate = Date().timeIntervalSince1970 + duration + 1
        timeView.alpha = 1
        self.timeView.configure(text: self.getLessonTime(timeEnd: duration))
        startTimer(withEndDate: endDate)
    }

    func setViewers(count: Int) {
        viewersView.configure(text: String(count))
    }

    func hideViewwers(isHidden: Bool) {
        viewersView.isHidden = isHidden
    }

    func hideTimer(isHidden: Bool) {
        timeView.alpha = isHidden ? 0 : 1
    }

    func invalidateLiveStreamTimer() {
        timer?.invalidate()
    }

    // MARK: - Private Methods

    private func startTimer(withEndDate endDate: Double) {
        timer?.invalidate()
        self.timer = Timer(timeInterval: 1, repeats: true) { [weak self] timer in
            guard let self = self else { return }
            let currentTime = Date().timeIntervalSince1970
            let timerEndTime = TimeInterval(endDate)
            let lessonTime = timerEndTime - currentTime

            if lessonTime <= 0 {
                self.lessonFinished.onNext(())
                timer.invalidate()
            }
            self.timeView.configure(text: self.getLessonTime(timeEnd: lessonTime))
        }
        self.timer?.tolerance = 0.2
        if let timer = timer {
            RunLoop.current.add(timer, forMode: .common)
        }
        setupLayout()
    }

    private func setupLayout() {
        let centerPointX: CGFloat = UIScreen.isSE ? 31 : 45
        let centerX = frame.width - centerPointX
        viewersView.center = CGPoint(x: centerX, y: viewersView.center.y)
        let timeViewCenterX = viewersView.frame.origin.x - Constants.spacing - timeView.frame.width / 2
        timeView.center = CGPoint(x: streamIsSaved ?  frame.width / 2 : timeViewCenterX,
                                  y: timeView.center.y)
    }

    private func getLessonTime(timeEnd: Double) -> String {
        let timeString = Formatters.streamTimeFormatter.string(from: Date(timeIntervalSince1970: timeEnd))
        return timeString
    }
}
