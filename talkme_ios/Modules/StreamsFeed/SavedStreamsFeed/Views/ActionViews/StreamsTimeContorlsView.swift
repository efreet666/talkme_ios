//
//  StreamContorlsView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 19/7/2022.
//

import Foundation
import UIKit
import AVKit
import SnapKit
import RxSwift
import RxCocoa

fileprivate extension Consts {
    static var playButtonTopInset: CGFloat = 0
    static var playButtonBottomInset: CGFloat = 0
    static var playButtonLeadingInset: CGFloat = 0

    static var currentTimeLabelLeadingInset: CGFloat = 5
    static var currentTimeLabelWidth: CGFloat = 45

    static var sliderLeadingInset: CGFloat = 5

    static var videoDurationLabelLeadingInset: CGFloat = 11
    static var videoDurationLabelTrailingInset: CGFloat = 0
    static var videoDurationLabelWidth: CGFloat = 45
}

class SavedStreamTimeControlsView: UIView {

    // MARK: - Outputs

    private(set) lazy var playButtonTapped = videoPlayerButton.rx.tapGesture().when(.recognized)
    private(set) lazy var sliderTouchEvent = Observable.merge(sliderPanEvent, sliderTapEvent)
    private(set) lazy var sliderPanEvent = videoPlayerSlider.rx.value.map { [weak self] in  ($0, self?.videoPlayerSlider.isTracking ?? false) }
    private(set) lazy var sliderTapEvent = videoPlayerSlider.rx
        .tapGesture()
        .when(.recognized)
        .map { [weak self] in (self?.getSlidersValue(from: $0) ?? 0, false) }

    // MARK: - Private properties

    private var videosCurrentTimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14)
        label.adjustsFontSizeToFitWidth = true
        label.contentMode = .center
        return label
    }()

    private var videoTotalTimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14)
        label.adjustsFontSizeToFitWidth = true
        label.contentMode = .center
        return label
    }()

    private lazy var videoPlayerSlider: TimeSlider = {
        let slider = TimeSlider()
        slider.minimumValue = 0
        slider.isContinuous = false
        slider.minimumTrackTintColor = .white
        slider.maximumTrackTintColor = .darkGray
        slider.setThumbImage(UIImage(named: "circle"), for: .normal)
        slider.thumbRadius = 5
        slider.trackHeight = 4
        slider.thumbTouchZoneInsetX = -17
        slider.thumbTouchZoneInsetY = -10

        return slider
    }()

    private var videoPlayerButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "pause"), for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)
        return button
    }()

    private var videoPlayer: AVPlayer? {
        didSet {
            timeObserverToken = nil
        }
    }
    private var videoDuration: Float = 0
    private var formattedVideoDuration: String = "88:88"
    private var timeObserverToken: Any?
    private var playButtonIsInPause = false
    private var bag = DisposeBag()

    // MARK: - public funcs

    func setControlsToEmptyState() {
        pauseTimer()
        videoPlayerSlider.minimumValue = 0
        videoPlayerSlider.maximumValue = 1
        videoPlayerSlider.value = 0
        videoTotalTimeLabel.text = "00:00"
        videosCurrentTimeLabel.text = "00:00"
    }

    func setStreamsPlayer(to player: AVPlayer, andDuration duration: Double) {
        DispatchQueue.global(qos: .background).async {
            self.videoPlayer = player
            self.timeObserverToken = nil
            if let videoDuration = player.currentItem?.asset.duration {
                self.videoDuration = Float(videoDuration.seconds)
                DispatchQueue.main.async { [weak self] in
                    self?.videoPlayerSlider.maximumValue = self?.videoDuration ?? 0
                }
            }
        }
    }

    func startTimer() {
        guard timeObserverToken == nil,
              videoDuration != 0
        else { return }

        formattedVideoDuration = Formatters.minutesMoreTheSixtyAndSeconds(seconds: Double(videoDuration))
        videoTotalTimeLabel.text = formattedVideoDuration

        let interval = CMTimeMake(value: 1, timescale: 1)
        timeObserverToken = videoPlayer?.addPeriodicTimeObserver(forInterval: interval,
                                                                 queue: DispatchQueue.main
        ) { [weak self] _ in
            self?.updateTimerAndSlider()
        }
    }

    func pauseTimer() {
        if let token = timeObserverToken {
            videoPlayer?.removeTimeObserver(token)
            timeObserverToken = nil
        }
    }

    // MARK: - Live cycle

    init() {
        super.init(frame: .zero)
        setupLayout()
        bindUI()
        videoPlayerSlider.isContinuous = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private funcs

    private func setupLayout() {
        addSubviews(videoPlayerButton,
                    videosCurrentTimeLabel,
                    videoPlayerSlider,
                    videoTotalTimeLabel
        )

        videoPlayerButton.snp.makeConstraints { make in
            make.top.equalTo(self).offset(Consts.playButtonTopInset)
            make.leading.equalTo(self).offset(Consts.playButtonLeadingInset)
            make.bottom.equalTo(self).offset(Consts.playButtonBottomInset)
        }

        videosCurrentTimeLabel.snp.makeConstraints { make in
            make.centerY.equalTo(videoPlayerButton)
            make.leading.equalTo(videoPlayerButton.snp.trailing).offset(Consts.currentTimeLabelLeadingInset)
            make.width.equalTo(Consts.currentTimeLabelWidth)
        }

        videoPlayerSlider.snp.makeConstraints { make in
            make.centerY.equalTo(videoPlayerButton)
            make.leading.equalTo(videosCurrentTimeLabel.snp.trailing).offset(Consts.sliderLeadingInset)
        }

        videoTotalTimeLabel.snp.makeConstraints { make in
            make.centerY.equalTo(videoPlayerButton)
            make.leading.equalTo(videoPlayerSlider.snp.trailing).offset(Consts.videoDurationLabelLeadingInset)
            make.trailing.equalTo(self).offset(Consts.videoDurationLabelTrailingInset)
            make.width.equalTo(Consts.videoDurationLabelWidth)
        }
    }

    private func updateTimerAndSlider() {
        guard let videoPlayer = videoPlayer,
              videoDuration != 0,
              !videoPlayerSlider.isTracking,
              videoPlayer.timeControlStatus == .playing
        else { return }

        if let currentItem = videoPlayer.currentItem {
            let currentTimeInSeconds = Float(CMTimeGetSeconds( currentItem.currentTime() ))
            let mins = currentTimeInSeconds / 60
            let secs = currentTimeInSeconds.truncatingRemainder(dividingBy: 60)
            let timeformatter = NumberFormatter()
            timeformatter.minimumIntegerDigits = 2
            timeformatter.minimumFractionDigits = 0
            timeformatter.roundingMode = .down
            guard var minsStr = timeformatter.string(from: NSNumber(value: mins)),
                  var secsStr = timeformatter.string(from: NSNumber(value: secs))
            else { return }

            minsStr = minsStr == "-00" ? "00" : minsStr
            secsStr = secsStr == "-00" ? "00" : secsStr

            videosCurrentTimeLabel.text = "\(minsStr):\(secsStr)"
            videoPlayerSlider.value = currentTimeInSeconds
        }
        if videoPlayerSlider.value == videoPlayerSlider.maximumValue {
            playButtonIsInPause = true
            videoPlayerButton.setImage(UIImage(named: "play"), for: .normal)
            pauseTimer()
        }
    }

    private func getSlidersValue(from tapGestureReconizer: UIGestureRecognizer) -> Float {
            let pointTapped: CGPoint = tapGestureReconizer.location(in: self)

            let positionOfSlider: CGPoint = videoPlayerSlider.frame.origin
            let widthOfSlider: CGFloat = videoPlayerSlider.frame.size.width
            let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.videoPlayerSlider.maximumValue) / widthOfSlider)

            var result = Float(newValue) > videoPlayerSlider.maximumValue ? videoPlayerSlider.maximumValue : Float(newValue)
            result = Float(newValue) < videoPlayerSlider.minimumValue ? videoPlayerSlider.minimumValue : result

            return result
    }

    private func bindUI() {
        playButtonTapped
            .bind { [weak self] _  in
                guard let self = self
                else { return }
                self.playButtonIsInPause.toggle()
                if self.playButtonIsInPause {
                    self.videoPlayerButton.setImage(UIImage(named: "play"), for: .normal)
                    self.pauseTimer()
                } else {
                    self.videoPlayerButton.setImage(UIImage(named: "pause"), for: .normal)
                    self.startTimer()
                }
            }
            .disposed(by: bag)

        sliderTouchEvent
            .subscribe(onNext: { [weak self] value, _ in
                guard let self = self
                else { return }

                let mins = value / 60
                let secs = value.truncatingRemainder(dividingBy: 60)
                let timeformatter = NumberFormatter()
                timeformatter.minimumIntegerDigits = 2
                timeformatter.minimumFractionDigits = 0
                timeformatter.roundingMode = .down
                guard let minsStr = timeformatter.string(from: NSNumber(value: mins)),
                      let secsStr = timeformatter.string(from: NSNumber(value: secs))
                else { return }
                self.videosCurrentTimeLabel.text = "\(minsStr):\(secsStr)"

                if value >= self.videoPlayerSlider.maximumValue {
                    self.playButtonIsInPause = true
                    self.videoPlayerButton.setImage(UIImage(named: "play"), for: .normal)
                }
            })
            .disposed(by: bag)

        sliderTapEvent
            .bind { [weak self] value, _ in
                self?.videoPlayerSlider.setValue(value, animated: true)
            }
            .disposed(by: bag)
    }
}
