//
//  SavedStreamsControlsView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 19/7/2022.
//

import Foundation
import UIKit
import AVFoundation
import RxSwift
import RxCocoa

fileprivate extension Consts {
    static let spacing: CGFloat = UIScreen.isSE ? 14 : 20
    static let closeButtonInset: CGFloat = UIScreen.isSE ? 13 : 11
}

class SavedStreamControlsView: UIView {

    // MARK: - Outputs
    private(set) lazy var onAvatarButtonTap = topView.tapAvatar
    private(set) lazy var onAddTeacherButtonTap = topView.tapAddTeacher
    private(set) lazy var onBackButtonTap = topView.tapClose
    private(set) lazy var onHelpButtonTap = topView.tapHelp
    private(set) lazy var onShareTap = actionsView.onShareTap
    private(set) lazy var onCoinTap = actionsView.onCoinTap
    private(set) lazy var onMicrophoneTap = actionsView.onMicrophoneTap
    private(set) lazy var onGiftButtonTap = actionsView.onGiftTap
    private(set) lazy var onLikeTap = actionsView.onLikeTap
    
    private(set) lazy var isMicroOn = actionsView.isMicroOn

    private(set) lazy var onShowPopUp = PublishRelay<Void>()
    private(set) lazy var onShowMembersPopUp = PublishRelay<Void>()

    private(set) lazy var playButtonTapped = timeControls.playButtonTapped
    private(set) lazy var sliderPanEvent = timeControls.sliderTouchEvent

    // MARK: - Inputs
    private(set) lazy var loaderIsActive = BehaviorRelay<Bool>(value: true)

    // MARK: - Private properties
    private let actionsView: StreamSideActionsViewNew
    private let topView = TopStreamView(frame: .zero)
    private let topPozitionZView = UIView()
    private let centerView = UIView()

    private let userCreatedThisStream: Bool
    private var userMutedSound = false
    private(set) var bag = DisposeBag()

    private let timeControls: SavedStreamTimeControlsView

    private lazy var gradientView: UIView = {
        let view = UIView()
        entierViewBackgroundGradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
        entierViewBackgroundGradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        entierViewBackgroundGradientLayer.colors = [UIColor.black.withAlphaComponent(0).cgColor,
                                                    UIColor.black.withAlphaComponent(1).cgColor]

        view.layer.addSublayer(entierViewBackgroundGradientLayer)
        return view
    }()

    private var timerForHidingControls: Timer?
    private let entierViewBackgroundGradientLayer = CAGradientLayer()
    
    private var videoIsPlaying = false
    
    private let userType: UserType

    private lazy var controlElements: [UIView] = [
        timeControls,
        gradientView
    ]

    // MARK: - live cycle

    init(forOwner owner: Bool, type: UserType, classNumber: String?) {
        userType = type
        timeControls = SavedStreamTimeControlsView()
        userCreatedThisStream = owner
        actionsView = .init(type: .viewer)
        topView.isSaved = true
        super.init(frame: .zero)
        setupLayout()
        hideControlls()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if entierViewBackgroundGradientLayer.frame != gradientView.bounds {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            entierViewBackgroundGradientLayer.frame = gradientView.bounds
            CATransaction.commit()
        }
    }
    
    // MARK: - Public Methods
    func setValue(isContact: Bool) {
        topView.setValue(isContact: isContact)
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if !loaderIsActive.value && videoIsPlaying {
            showTimeControls(onlyForDurationOf: 5)
        }
        return super.hitTest(point, with: event)
    }

    func setPlayer(to player: AVPlayer, withDuration duration: Double) {
        timeControls.setStreamsPlayer(to: player, andDuration: duration)
    }

    func stratDisplayingProgress() {
        timeControls.startTimer()
        videoIsPlaying = true
    }

    func showTimeControls() {
        showTimeControls(onlyForDurationOf: 5, andStartWithAnimation: false)
    }

    func stopDisplayingProgress() {
        timeControls.setControlsToEmptyState()
        hideControlls()
        videoIsPlaying = false
    }

    func configure(streamModel: SavedStreamModel, lessonDetail: LessonDetailResponse, isContact: Bool?) {
        bag = .init()
        bindUI()
        videoIsPlaying = false
        topView.configureSavedStream(streamModel, lessonDetail: lessonDetail, isContact: isContact ?? false)
        topView.isConnect.accept(isContact ?? false)
    }

    func hideControlls(animated: Bool = false) {
        if animated {
            UIView.animate(withDuration: 1, animations: {
                self.timeControls.alpha = 0
                self.gradientView.alpha = 0
            }, completion: { _ in
                self.timeControls.isHidden = true
                self.gradientView.isHidden = true
            })
        } else {
            self.timeControls.alpha = 0
            self.gradientView.alpha = 0

            self.timeControls.isHidden = true
            self.gradientView.isHidden = true
        }
    }

    func showControls(animated: Bool = false) {
        self.timeControls.isHidden = false
        self.gradientView.isHidden = false

        if animated {
            UIView.animate(withDuration: 0.25, animations: {
                self.timeControls.alpha = 1
                self.gradientView.alpha = 1
            }, completion: { _ in
                self.timeControls.isHidden = false
                self.gradientView.isHidden = false
            })
        } else {
            self.timeControls.alpha = 1
            self.gradientView.alpha = 1

            self.timeControls.isHidden = false
            self.gradientView.isHidden = false
        }
    }

    func setVipLessonState(_ isActive: Bool) {
        isActive ? hideControlls() : showControls(animated: true)
    }

    // MARK: - Private funcs
    
    private func removeGradientLayer(_ view: UIView) {
        guard let layers = view.layer.sublayers else { return }
        for aLayer in layers where aLayer is CAGradientLayer {
            aLayer.removeFromSuperlayer()
        }
    }
    
    private func setGradientZView(_ isHidden: Bool) {
        if !isHidden {
            DispatchQueue.main.async {
                self.removeGradientLayer(self.topPozitionZView)
                self.topPozitionZView.gradientBackground(
                    from: .clear,
                    to: .black.withAlphaComponent(0.85),
                    direction: .bottomToTop
                )
            }
        } else {
            removeGradientLayer(topPozitionZView)
        }
    }

    private func setupLayout() {
        addSubviews(
            topPozitionZView,
            centerView,
            topView,
            actionsView,
                    gradientView,
                    timeControls
        )
        
        topPozitionZView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalTo(topView.snp.bottom)
        }
        
        centerView.snp.makeConstraints { make in
            make.top.equalTo(topPozitionZView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        topView.snp.remakeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 20 : 25)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(106)
        }
        
        actionsView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 15)
            make.bottom.equalTo(timeControls.snp.top).offset(-10)
            make.top.equalTo(topView.snp.bottom)
            make.width.equalTo(40)
        }

        gradientView.snp.makeConstraints { make in
            make.top.equalTo(timeControls).offset(10)
            make.leading.trailing.bottom.equalTo(self)
        }

        timeControls.snp.makeConstraints { make in
            make.leading.equalTo(safeAreaLayoutGuide)
            make.trailing.equalTo(self).offset(-12)
            make.bottom.equalToSuperview().inset(25)
        }
    }

    private func bindUI() {
        DispatchQueue.global(qos: .background).async {
//            self.swipeZoneView
//                .rx
//                .swipeGesture(.right)
//                .when(.recognized)
//                .observeOn(MainScheduler.instance)
//                .bind { [weak self] _ in
//                    self?.hideControlls()
//                }
//                .disposed(by: self.bag)
//
//            self.swipeZoneView
//                .rx
//                .swipeGesture(.left)
//                .when(.recognized)
//                .observeOn(MainScheduler.instance)
//                .bind { [weak self] _ in
//                    guard let self = self
//                    else { return }
//                    guard self.giftButton.isHidden == false
//                    else { return }
//                    if self.userCreatedThisStream {
//                        self.onShowMembersPopUp.accept(())
//                    } else {
//                        self.onShowPopUp.accept(())
//                    }
//                }
//                .disposed(by: self.bag)

//            self.muteButtonTapped
//                .observeOn(MainScheduler.instance)
//                .bind { [weak self] _ in
//                    guard let self = self
//                    else { return }
//                    self.userMutedSound.toggle()
//                    self.microphoneButton.setupIcon(for: .mute, isActive: self.userMutedSound)
//                }
//                .disposed(by: self.bag)

            self.loaderIsActive
                .distinctUntilChanged()
                .observeOn(MainScheduler.instance)
                .bind { [weak self] videoIsDownloading in
                    videoIsDownloading ? self?.hideControlls(animated: true) : Void()
                }
                .disposed(by: self.bag)
        }
    }

    private func showTimeControls(onlyForDurationOf duration: Double, andStartWithAnimation animated: Bool = true) {
        self.showControls(animated: animated)

        let nowDateInSeconds = Date().timeIntervalSince1970
        let hideFireDateInSeconds = nowDateInSeconds + duration
        let hideFireDate = Date(timeIntervalSince1970: hideFireDateInSeconds)

        timerForHidingControls?.invalidate()
        timerForHidingControls = Timer(fire: hideFireDate, interval: 1, repeats: false) { [weak self] timer in
            guard let self = self
            else { return }
            self.hideControlls(animated: true)
        }
        if let timer = timerForHidingControls {
            RunLoop.current.add(timer, forMode: .common)
        }
    }
}
