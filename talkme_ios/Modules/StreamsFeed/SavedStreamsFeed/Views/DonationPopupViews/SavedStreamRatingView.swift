//
//  SavedStreamRatingView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxCocoa
import RxSwift

final class SavedStreamsRatingView: UIView {

    // MARK: - Public Properties

    let ratingRelay = BehaviorRelay<Int?>(value: nil)

    // MARK: - Private Properties

    private var stars: [UIImageView] = []
    private let bag = DisposeBag()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = UIScreen.isSE ? 6 : 8
        return sv
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStars()
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupStars() {
        for tag in 0...4 {
            stars.append(makeStarView(tag))
        }
    }

    private func makeStarView(_ tag: Int) -> UIImageView {
        let starView = UIImageView()
        starView.image = UIImage(named: "emptyStar")
        starView.contentMode = .scaleAspectFill
        starView.tag = tag
        starView.rx
            .tapGesture()
            .when(.recognized)
            .bind { [weak self, weak starView] _ in
                guard let starView = starView, let self = self else { return }
                self.ratingRelay.accept(tag + 1)
                self.stars.forEach {
                    $0.image = $0.tag <= starView.tag
                        ? UIImage(named: "colorStar")
                        : UIImage(named: "emptyStar")
                }
            }
            .disposed(by: bag)

        return starView
    }

    private func setupLayout() {
        addSubviews(stackView)
        stars.forEach(stackView.addArrangedSubview)

        stackView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(5)
            make.leading.trailing.equalToSuperview()
        }
    }
}
