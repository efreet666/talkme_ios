//
//  SavedStreamDonationView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

enum SavedStreamsPrice: String {
    case fifty = "50"
    case hundred = "100"
    case twoHundred = "200"
    case fiveHundred = "500"
}

final class SavedStreamsDonationView: UIView {

    // MARK: - Public Properties

    private(set) lazy var ratingTap = ratingView.ratingRelay
    private(set) lazy var textRelay = enterDonationView.textRelay
    private(set) lazy var sendButtonTap = donateButton.rx.tapGesture().when(.recognized)
    let bag = DisposeBag()

    // MARK: - Private Properties

    private var withRating: Bool!
    private let priceArray: [SavedStreamsPrice] = [.fifty, .hundred, .twoHundred, .fiveHundred]
    private let dataSourceButtonView: [SavedStreamsGenericViewType] = [
        .pay(SavedStreamsPrice.fifty),
        .pay(SavedStreamsPrice.hundred),
        .pay(SavedStreamsPrice.twoHundred),
        .pay(SavedStreamsPrice.fiveHundred)]
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "stream_feed_donate".localized
        label.textColor = TalkmeColors.white
        label.textAlignment = .center
        label.font = UIScreen.isSE ? .montserratBold(ofSize: 18) : .montserratBold(ofSize: 22)
        return label
    }()

    private let donateButton = SavedStreamsRoundedColoredButton(.send)
    private let enterDonationView = SavedStreamsEnterDonationView()
    private let ratingView = SavedStreamsRatingView()
    private lazy var priceButtonView = SavedStreamsRepeatingViewsMakerView(
        viewsData: dataSourceButtonView,
        viewType: SavedStreamPriceButtonView.self,
        size: CGSize(width: UIScreen.isSE ? 50 : 65, height: UIScreen.isSE ? 28 : 36),
        spacing: 7)

    // MARK: - Initializers

    init(frame: CGRect, withRating: Bool) {
        super.init(frame: frame)
        backgroundColor = withRating ? TalkmeColors.grayLabels : TalkmeColors.unselectedTimeItem
        self.withRating = withRating
        setupLayout()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        ratingView.isHidden = !withRating
        addSubviews(titleLabel, ratingView, enterDonationView, priceButtonView, donateButton)

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(withRating ? (UIScreen.isSE ?  20 : 38) : 28)
            make.height.equalTo(UIScreen.isSE ? 18 : 27)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        ratingView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 15 : 17)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 54 : 80)
            make.bottom.equalTo(enterDonationView.snp.top).offset(UIScreen.isSE ? -13 : -22)
        }

        enterDonationView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 35 : 49)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 25 : 44)
            make.top.equalTo(titleLabel.snp.bottom).offset(
                withRating
                ? UIScreen.isSE ? 57 : 77
                : UIScreen.isSE ? 31 : 33
            )
        }

        priceButtonView.snp.makeConstraints { make in
            make.top.equalTo(enterDonationView.snp.bottom).offset(UIScreen.isSE ? 18 : 22)
            make.leading.greaterThanOrEqualToSuperview().inset(30)
            make.trailing.lessThanOrEqualToSuperview().inset(30)
            make.size.equalTo(priceButtonView.bounds.size)
            make.centerX.equalToSuperview()
        }

        donateButton.snp.makeConstraints { make in
            make.top.equalTo(priceButtonView.snp.bottom).offset(UIScreen.isSE ? 16 : 22)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 25 : 45)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -27 : -34)
        }
    }

   private func buttonTouched(_ sender: UIButton) {
        UIButton.animate(
            withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
                sender.alpha = 0.5
            },
            completion: { _ in
                UIButton.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform.identity
                    sender.alpha = 1
                })
            })
    }

    private func selectedPriceView() {
        self.priceButtonView.viewsArray.forEach {
            $0.setSelected(false)
        }
    }

    private func clearSelectedButton() {
        enterDonationView.clearSelectedButton
            .bind { [weak self] _ in
                self?.selectedPriceView()
            }
            .disposed(by: enterDonationView.bag)
    }

    private func bindUI() {
        clearSelectedButton()

        sendButtonTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.buttonTouched(self.donateButton)
            }
            .disposed(by: bag)

        priceButtonView
            .onViewTap
            .bind { [weak self] view in
                guard let self = self else { return }
                self.selectedPriceView()
                view.setSelected(true)
                let text = self.priceArray[view.viewTag].rawValue
                self.enterDonationView.setupText(text)
                self.textRelay.accept(text)
            }
            .disposed(by: bag)
    }
}
