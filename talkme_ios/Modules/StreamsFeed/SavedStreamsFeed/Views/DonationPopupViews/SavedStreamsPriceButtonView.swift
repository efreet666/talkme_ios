//
//  SavedStreamsPriceButtonView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxSwift

final class SavedStreamPriceButtonView: UIButton, SavedStreamsRepeatableViewProtocol {

    // MARK: - Public Properties

    let size: CGSize
    let viewTag: Int
    let bag = DisposeBag()

    // MARK: - Initializers

    init(size: CGSize, isSelected: Bool, viewTag: Int, viewsData: SavedStreamsGenericViewType) {
        self.viewTag = viewTag
        self.size = size
        super.init(frame: CGRect(origin: .zero, size: size))
        switch viewsData {
        case .pay(let title):
            setTitle(title.rawValue, for: .normal)
        default: break
        }
        setupUI()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    // MARK: - Public Methods

    func setSelected(_ selected: Bool) {
        backgroundColor = selected ? TalkmeColors.streamSelectedBackground : .clear
        layer.borderColor = selected ? UIColor.clear.cgColor : TalkmeColors.white.cgColor
    }

    // MARK: - Private Methods

    private func setupUI() {
        titleLabel?.font = .montserratBold(ofSize: 13)
        layer.borderColor = TalkmeColors.white.cgColor
        setTitleColor(TalkmeColors.white, for: .normal)
        layer.borderWidth = 1
    }
}
