//
//  SavedStreamsEnterDonationView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxCocoa
import RxSwift

final class SavedStreamsEnterDonationView: UIView {

    // MARK: - Public Properties

    let textRelay = BehaviorRelay<String?>(value: nil)
    let clearSelectedButton = PublishRelay<String?>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        return view
    }()

    private let coinImage: UIImageView = {
        let coinImage = UIImageView()
        coinImage.image = UIImage(named: "coin")
        coinImage.contentMode = .scaleAspectFill
        return coinImage
    }()

    private let textField: UITextField = {
        let tf = UITextField()
        tf.keyboardType = .numberPad
        tf.attributedPlaceholder = NSAttributedString(
            string: "0",
            attributes: [NSAttributedString.Key.foregroundColor: TalkmeColors.white]
        )
        tf.tintColor = TalkmeColors.white
        tf.font = UIScreen.isSE ? .montserratSemiBold(ofSize: 25) : .montserratSemiBold(ofSize: 30)
        tf.textColor = TalkmeColors.white
        tf.textAlignment = .center
        return tf
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        textField.delegate = self
        setupLayout()
        bindUI()
//        textField.becomeFirstResponder() todo: пока не нужно чтобы клавиатура появлялась по умолчанию
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupText(_ text: String) {
        textField.text = text
    }

    // MARK: - Private Methods

    private func bindUI() {
        textField.rx.text
            .bind(to: textRelay)
            .disposed(by: bag)

        textField.rx.text
            .bind(to: clearSelectedButton)
            .disposed(by: bag)
    }

    private func setupLayout() {
        addSubviews([textField, coinImage, separatorView])

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(textField.snp.bottom).offset(3)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 2 : 3)
            make.bottom.equalToSuperview()
        }

        coinImage.snp.makeConstraints { make in
            make.trailing.equalTo(separatorView.snp.trailing)
            make.bottom.equalTo(separatorView.snp.top).offset(-10)
            make.size.equalTo(UIScreen.isSE ? 21 : 26)
        }

        textField.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.trailing.equalTo(coinImage.snp.leading).offset(-10)
            make.leading.equalToSuperview().inset(30)
        }
    }
}

extension SavedStreamsEnterDonationView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 7
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        guard textField.text?.count != 0 || string != "0" else { return false }
        return newString.length <= maxLength
    }
}

