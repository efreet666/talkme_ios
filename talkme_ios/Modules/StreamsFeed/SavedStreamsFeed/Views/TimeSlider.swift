//
//  TimeSlider.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 23/7/2022.
//

import UIKit

class TimeSlider: UISlider {

    var trackHeight: CGFloat = 4
    var thumbTouchZoneInsetX: CGFloat = 0
    var thumbTouchZoneInsetY: CGFloat = 0

    var thumbRadius: CGFloat = 7 {
        didSet {
            let resizedThumbImage = currentThumbImage?.resized(to: CGSize(width: thumbRadius * 2,
                                                                          height: thumbRadius * 2))
            setThumbImage(resizedThumbImage, for: .normal)
            setThumbImage(resizedThumbImage, for: .highlighted)
        }
    }

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let originY = (bounds.size.height / 2) - (trackHeight / 2)
        let originX = bounds.origin.x
        let height = trackHeight
        let width = bounds.size.width
        return CGRect(x: originX, y: originY, width: width, height: height)
    }

    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        return super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
    }

    // Increase Thumb hot spot clickable area
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var bounds2: CGRect = self.bounds
        bounds2 = bounds2.insetBy(dx: thumbTouchZoneInsetX, dy: thumbTouchZoneInsetY)
        return bounds2.contains(point)
    }
}
