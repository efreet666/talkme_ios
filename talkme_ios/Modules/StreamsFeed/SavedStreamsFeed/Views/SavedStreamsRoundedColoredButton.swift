//
//  SavedStreamsRoundedColoredButton.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

enum SavedStreamButtonType {
    case send
    case buy
    case connect
    case createChat

    var title: String {
        switch self {
        case .send:
            return "profile_setting_send_code".localized
        case .buy:
            return "stream_feed_buy".localized
        case .connect:
            return "stream_feed_connect".localized
        case .createChat:
            return "chats_create_chat".localized
        }
    }
}

class SavedStreamsRoundedColoredButton: UIButton {

    private enum Constants {
        static let buttonFont = UIScreen.isSE ? UIFont.montserratExtraBold(ofSize: 15) : UIFont.montserratExtraBold(ofSize: 17)
    }

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: .zero)
        initialSetup()
    }

    convenience init(_ type: SavedStreamButtonType) {
        self.init()
        setTitle(type.title, for: .normal)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    // MARK: - Public Methods

    func configure(_ type: SavedStreamButtonType) {
        setTitle(type.title, for: .normal)
    }

    // MARK: - Private Methods

    private func initialSetup() {
        clipsToBounds = true
        titleLabel?.font = Constants.buttonFont
        setTitleColor(TalkmeColors.white, for: .normal)
        backgroundColor = TalkmeColors.blueLabels
    }
}

