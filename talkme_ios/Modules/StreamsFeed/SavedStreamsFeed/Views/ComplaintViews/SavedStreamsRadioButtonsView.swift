//
//  SavedStreamsRadioButtonsView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import UIKit

class SavedStreamsRadioButtonsView: UIView {

    private enum Constants {
        static let buttonHeight: CGFloat = UIScreen.isSE ? 35 : 42
        static let buttonExtraOffset: CGFloat = UIScreen.isSE ? 4 : 7
    }

    var currentSelectedComplaintIndex = 0
    var currentComplaint: ComplaintResponse?

    var radioButtons: [SavedStreamsRadioButton] = []
    var complaints: Complaints? {
        didSet {
            setup()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func setup() {
        guard let count = complaints?.complaints?.count,
              let complaints = complaints else { return }

        for index in 0..<count {

            guard let complaint = complaints.complaints?[index] else { return }

            let button = SavedStreamsRadioButton()
            radioButtons.insert(button, at: index)
            addSubview(button)
            if index == currentSelectedComplaintIndex {
                button.isSelected = true
                currentComplaint = complaint
            }
            button.setTitle(complaint.value, for: .normal)
            let coef = CGFloat(index)
            button.frame = CGRect(x: 0,
                                  y: Constants.buttonHeight * coef + Constants.buttonExtraOffset,
                                  width: UIScreen.width * 0.8,
                                  height: Constants.buttonHeight)
            button.addTarget(self, action: #selector(radioButtonAction(sender:)), for: .touchUpInside)
        }
    }
//    private func setup() {
//        for index in 0..<(ComplaintType.allCases.count ) {
//            let button = RadioButton()
//            radioButtons.insert(button, at: index)
//            addSubview(button)
//            if index == currentSelectedCamplaintIndex {
//                button.isSelected = true
//            }
//            let titleForLabel = ("complaint_" + ComplaintType.allCases[index].rawValue).localized
//            button.setTitle(titleForLabel, for: .normal)
//            let coef = CGFloat(index)
//            button.frame = CGRect(x: 0,
//                                  y: Constants.buttonHeight * coef + Constants.buttonExtraOffset,
//                                  width: UIScreen.width * 0.8,
//                                  height: Constants.buttonHeight)
//            button.addTarget(self, action: #selector(radioButtonAction(sender:)), for: .touchUpInside)
//        }
//    }

    @objc func radioButtonAction(sender: SavedStreamsRadioButton) {
        radioButtons[currentSelectedComplaintIndex].isSelected.toggle()
        currentSelectedComplaintIndex = radioButtons.firstIndex(of: sender) ?? 0
        if let complaints = complaints?.complaints?[currentSelectedComplaintIndex] {
            currentComplaint = complaints
        }
        radioButtons[currentSelectedComplaintIndex].isSelected.toggle()

    }
}
