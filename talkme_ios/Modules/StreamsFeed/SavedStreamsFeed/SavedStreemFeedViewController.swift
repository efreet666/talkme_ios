//
//  SavedStreemFeedViewController.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 8/7/2022.
//

import CollectionKit
import RxCocoa
import RxSwift
import UIKit
import NotificationBannerSwift

fileprivate extension Consts {
    // dont push numbers more then 100 nd less then 0 to precentOfPageSizeWichIsALoadBorder
    static var precentOfPageSizeWichIsALoadBorder: Float = 70
}

final class SavedStreamsFeedViewController: UIViewController {

    private enum SwipeState {
        case completeWithLessonChange
        case loading
        case completeWithoutLessonChange
        case canUpate
    }

    // MARK: - Private Properties

    private lazy var collectionView: CollectionView? = {
        let collectionView = CollectionView()
        collectionView.bounces = false
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.isPagingEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()

    private(set) lazy var redyToBePushed = BehaviorRelay<Bool>(value: false)
    private let viewModel: SavedStreamsFeedViewModel
    var initialLessonId: Int { viewModel.currentLessonId ?? 0 }
    private(set) var bag = DisposeBag()
    let removeViewFromSuperviewTeacher = PublishRelay<Void>()
    private var streamViewHeight: CGFloat { UIScreen.height }
    private var cameraIsActive: Bool { viewModel.cameraIsActiveState }

    private var userTypeStored: UserType?
    private var userType: UserType? {
        get {
            if isWatchingSave { return .viewer }
            return userTypeStored
        }
        set { userTypeStored = newValue }
    }

    private var showPopupBlock: (() -> Void)?
    private var currentSwipeState: SwipeState = .completeWithLessonChange
    private var collectionWilldecelerate = false
    private let onSwipe = PublishRelay<Void>()
    private var isWatchingSave: Bool = false
    private(set) var rightPagingBoundRiched = PublishRelay<Int>()
    private var numberOfLastPage = 1

    // MARK: - Initializers

    init(viewModel: SavedStreamsFeedViewModel, watchSave: Bool = false) {
        self.viewModel = viewModel
        self.isWatchingSave = watchSave
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = TalkmeColors.streamBG
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // repeatedLoginRequest()
        bindVM()
        #if DEBUG
        showAlertStreamInfo()
        #endif
        showNotificationChatInfo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTopBottomBarsHidden(true)
        keepScreenAwake(true)
        viewModel.setPlayer(mute: false)
        if let showPopupBlock = showPopupBlock {
            showDonatePopupIfNeeded(showBlock: showPopupBlock)
        }
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setTopBottomBarsHidden(false)
        AppDelegate.deviceOrientation = .portrait
        keepScreenAwake(false)
        viewModel.setPlayer(mute: true)
        viewModel.finishTraking()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        DispatchQueue.main.async {
            guard self.userType == .specialist else { return }
            self.view.endEditing(true)
        }

        if let collectionView = collectionView {
            let offset = collectionView.contentOffset
            let height = collectionView.bounds.size.height
            collectionView.setContentOffset(
                CGPoint(x: offset.x, y: CGFloat.rounded(offset.y / height)() * size.height),
                animated: false
            )
        }
    }

    // MARK: - Public Methods

    func showDonatePopupIfNeeded(showBlock: @escaping () -> Void) {
        guard self.navigationController?.topViewController === self else {
            self.showPopupBlock = showBlock
            return
        }
        showBlock()
        showPopupBlock = nil
    }

    // MARK: - Private Methods

    private func notificationChatInfo(title: String, subTitle: String) {
        let banner = NotificationBanner(title: title, subtitle: subTitle, style: .warning)
        banner.autoDismiss = false
        banner.dismissOnTap = true
        banner.show()
    }

    private func repeatedLoginRequest() {
        let emailOrMobile = KeychainManager.load(service: "MobileService", account: "Mobile") ?? ""
        let password = KeychainManager.load(service: "PasswordService", account: "Password")  ?? ""
        viewModel.repeatedLoginRequest(emailOrMobile: emailOrMobile, password: password)
    }

    private func showNotificationChatInfo() {
        viewModel.showNotification = { [weak self] title, subTitle in
            self?.notificationChatInfo(title: title, subTitle: subTitle)
        }
    }

    private func showAlertStreamInfo() {
        viewModel.dataResponse = { [weak self] model, code in
            let data = "gcoreDashUrl: \(model?.gcoreDashUrl ?? "") \n\ngcoreHlsUrl: \(model?.gcoreHlsUrl ?? "") \n\npushUrl: \(model?.pushUrl ?? "") \n\nuserId: \(model?.userId ?? 0) \n\nstausCode: \(code ?? "")"
            guard self?.currentSwipeState == .completeWithLessonChange else {
                return nil
            }
            return self?.errorAlert(title: "isOwner: \(model?.owner ?? true)", data: data)
        }
    }

    private func errorAlert(title: String, data: String) {
        let alert = UIAlertController(title: title, message: data, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        navigationController?.present(alert, animated: true, completion: nil)
    }

    private func bindVM() {
        self.userType = .viewer
        self.setupLayout()
        self.createStreamView()

        viewModel.onPresentLessonsIsEmptyAlert
            .bind { [weak self] _ in
                self?.presentNoLessonsAlert()
            }.disposed(by: bag)
    }

    private func createStreamView() {
        setupCollectionView()
        viewModel
            .onDataSourceUpdate
            .filter { [weak self] itemsCount in
                guard let self = self
                else { return false }
                return self.viewModel.canUpdateCollection && self.currentSwipeState != .loading && itemsCount != 0
            }
            .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
            .do(onNext: { [weak self]  _ in
                guard let self = self else { return }
                self.collectionView?.contentOffset.y = self.streamViewHeight * CGFloat(self.viewModel.currentIndex)
            })
            .bind { [weak self] _ in self?.collectionView?.reloadData() }
            .disposed(by: bag)
    }

    private func presentNoLessonsAlert() {
        let alert = UIAlertController(title: nil, message: "live_lessons_is_empty".localized, preferredStyle: .alert)
        let alertCancel = UIAlertAction(title: "profile_setting_cancel".localized, style: .default) { [weak self] _ in
            self?.viewModel.handleOnBackTap()
        }
        alert.addAction(alertCancel)
        self.present(alert, animated: true)
    }

    private func setTopBottomBarsHidden(_ isHidden: Bool) {
        navigationController?.isNavigationBarHidden = isHidden
        setCustomTabbarHidden(isHidden)
    }

    private func setupLayout() {
        view.backgroundColor = .black
        guard let collectionView = collectionView else { return }
        view.addSubview(collectionView)

        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        collectionView.layoutIfNeeded()
    }

    private func setupCollectionView() {
        let sourceView = ClosureViewSource(viewGenerator: { data, _ -> SavedStreamFeedView in
            return SavedStreamFeedView(streamModel: data.streamModel)
        }, viewUpdater: { [weak self] (view: SavedStreamFeedView, data: SavedStreamsFeedCellViewModel, _) in
            guard let self = self else { return }
            switch self.currentSwipeState {
            case .canUpate, .completeWithLessonChange, .completeWithoutLessonChange:
                print("[swipe] CONFIG")
                if let lessonDetail = self.viewModel.lessonDetails[data.streamModel.id] {
                    view.configure(streamModel: data.streamModel, lessonDetail: lessonDetail, cameraIsActive: self.viewModel.cameraIsActiveState, isWatchingSavedStream: true, isConnect: self.viewModel.infoStream?.isContact ?? false)
                }
                self.bindViewerStreemFeedView(view, data: data)
                self.viewModel.onReadyToPresentVipLessonPopup.accept(data.streamModel.id)
            case .loading:
                print("[swipe] failed state")
                view.prepareForReuse(streamModel: data.streamModel, andShowingSavedStream: true)
            }
        })
        sourceView.reuseManager.lifeSpan = 0

        let sizes = { (_: Int, _: SavedStreamsFeedCellViewModel, collectionSize: CGSize) -> CGSize in
            return CGSize(width: collectionSize.width, height: collectionSize.height)
        }

        let provider = BasicProvider(dataSource: viewModel.dataSource,
                                     viewSource: sourceView,
                                     sizeSource: sizes)

        provider.layout = InsetLayout(FlowLayout(spacing: 0), insets: UIEdgeInsets.zero)

        guard let collectionView = collectionView else { return }
        collectionView.provider = provider

        collectionView.rx.willBeginDragging
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.collectionWilldecelerate = false
                self.currentSwipeState = .loading
                print("[swipe] STARTED")
            }
            .disposed(by: bag)

        collectionView.rx.didEndDragging
            .bind { [weak self] cellWilldecelerate in
                guard let self = self else { return }
                print("[swipe] DID END DRAGGING")
                self.collectionWilldecelerate = cellWilldecelerate
                print("[swipe] [lesson will changed \(cellWilldecelerate)]")
                guard !cellWilldecelerate else { return }
                self.currentSwipeState = .canUpate
            }
            .disposed(by: bag)

        collectionView.rx
            .contentOffset
            .debounce(.milliseconds(200), scheduler: MainScheduler.instance)
            .bind { [weak self] _ in
                guard let self = self,
                      self.collectionView?.visibleIndexes.count == 1,
                      let indexOfVisibleCell = self.collectionView?.visibleIndexes.first,
                      let visibleCell = self.collectionView?.cell(at: indexOfVisibleCell) as? SavedStreamFeedView,
                      visibleCell.videoIsHidden()
                else { return }
                self.handleScroll(index: indexOfVisibleCell)
            }
            .disposed(by: bag)

        let bottomPagingBoundObserver = viewModel.onDataSourceUpdate
            .map { [weak self] itemsCount -> CGFloat in
                guard let self = self else { return CGFloat(0) }
                self.numberOfLastPage = self.calculateNumberOfLastPage(from: itemsCount, and: self.viewModel.numberOfStreamsInPage)
                return self.culculateOffsetValueForLoadingNextPage(from: itemsCount, and: self.viewModel.numberOfStreamsInPage)
            }

        Observable
            .combineLatest(collectionView.rx.contentOffset, bottomPagingBoundObserver)
            .filter { contentOffset, bottomPagingBound -> Bool in
                guard bottomPagingBound != 0
                else { return false}
                return contentOffset.y > bottomPagingBound
            }
            .map { $0.1 }
            .distinctUntilChanged()
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.loadNextPage()
            }
            .disposed(by: bag)
    }

    private func handleScroll(index: Int) {
        viewModel.canUpdateCollection = false
        self.setSwipestate(index: index)
        print("[swipe] HANDLE SCROLL  INDEX \(index)")
    }

    private func scrollToIndex(index: Int) {
        do {
            try collectionView?.scroll(to: index, animated: false)
        } catch {
            print("[swipe] \(error.localizedDescription)")
        }
    }

    private func visibleCell(cell: UIView) -> Bool {
        guard let collection = collectionView
        else { return false }

        guard cell.frame.minY != collection.contentOffset.y
        else { return true }

        let collectionBottom = collection.contentOffset.y + cell.frame.height

        if cell.frame.minY > collection.contentOffset.y {
            if (collectionBottom - cell.frame.minY) >= (cell.frame.height / 2) {
                return true
            }
        } else if cell.frame.minY < collection.contentOffset.y {
            if (collection.contentOffset.y - cell.frame.minY) <= (cell.frame.height / 2) {
                return true
            }
        }
        return false
    }

    private func setSwipestate(index: Int) {
        currentSwipeState = .completeWithLessonChange
        connectWithIndividualIndex(index)
    }

    private func connectWithIndividualIndex(_ index: Int) {
        viewModel.currentIndex = index
        viewModel.canUpdateCollection = true
        viewModel.connectWithIndex(index)
    }

    private func keepScreenAwake(_ isAwake: Bool) {
        UIApplication.shared.isIdleTimerDisabled = isAwake
    }

    private func culculateOffsetValueForLoadingNextPage(from itemsCount: Int, and numberOfStreamsInPage: Int) -> CGFloat {
        guard numberOfStreamsInPage > 0
        else { return CGFloat(0) }

        let streamsItemWidth = Float(streamViewHeight)
        var numberOfStreamsInLastPage = Float(itemsCount % numberOfStreamsInPage)

        if numberOfStreamsInLastPage == 0 {
            numberOfStreamsInLastPage = Float(numberOfStreamsInPage)
        }

        let lengthBeforeLastPage = streamsItemWidth * (Float(itemsCount) - numberOfStreamsInLastPage)
        let lengthOfLastPage = streamsItemWidth * numberOfStreamsInLastPage

        let pagingBorderInLastPage = lengthOfLastPage * ( Consts.precentOfPageSizeWichIsALoadBorder / Float(100) )
        return CGFloat(lengthBeforeLastPage + pagingBorderInLastPage)
    }

    private func calculateNumberOfLastPage(from itemsCount: Int, and numberOfStreamsInPage: Int) -> Int {
        let result = Int(ceil(Float(itemsCount) / Float(numberOfStreamsInPage)))
        return result
    }

    // MARK: - bind viewer

    private func bindViewerStreemFeedView(_ view: SavedStreamFeedView, data: SavedStreamsFeedCellViewModel) {
        view.viewModel = viewModel

        viewModel
            .onPlayerReadyForDisplay
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak view] (player, duration) in
                view?.setPlayer(player: player, withDuration: duration)
            })
            .disposed(by: view.bag)

        viewModel
            .dismissVipLessonPopup
            .observeOn(MainScheduler.instance)
            .bind { [weak view] _ in
                view?.removeVipLessonPopup()
            }
            .disposed(by: view.bag)

        viewModel
            .onUpdatePlayerSize
            .skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe { [weak view]  size in
                view?.setPlayerSize(size: size)
            }
            .disposed(by: view.bag)
        
        view
            .onAvatarButtonTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.handleProfleInfoButtonTap()
            }
            .disposed(by: view.bag)
        
        view
            .onAddTeacherButtonTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self, weak view] _ in
                guard let self = self, let view = view, let model = self.viewModel.infoStream else { return }
                self.viewModel.fetchData(classNumber: model.specialistInfo.numberClass ?? "")
                model.isContact
                    ? self.viewModel.removeContact(id: model.specialistInfo.id ?? 0)
                    : self.viewModel.addContact(id: model.specialistInfo.id ?? 0)
                view.isContact = model.isContact
                view.isContact.toggle()
                view.setValue(isContact: view.isContact)
                if view.isContact {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        NotificationSubscription.shared.showNotification(.subscription)
                    }
                }
            }
            .disposed(by: view.bag)
        
        view
            .onBackButtonTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.handleOnBackTap()
            }
            .disposed(by: view.bag)
        
        view
            .onHelpButtonTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.handleOnComplaintButtonTap()
            }
            .disposed(by: view.bag)
        
        view
            .onLikeTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                print("like")
            }
            .disposed(by: view.bag)
        
        view
            .onCoinTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.handleThankButtonTap()
            }
            .disposed(by: view.bag)
        
        view
            .onShareTap
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.shareApp()
            }
            .disposed(by: view.bag)
        
        view
            .onGiftButtonTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                print("Gift")
                self?.viewModel.handleGiftButtonTap()
            }
            .disposed(by: view.bag)
        
        view
            .onMicrophoneTap
            .observeOn(MainScheduler.instance)
            .bind { [weak self, weak view] _ in
                guard let self = self, let view = view else { return }
                self.viewModel.microOrPlayerIsActiveState.toggle()
                self.viewModel.handleMicrophoneTap()
                view.isMicroOn.accept(self.viewModel.microOrPlayerIsActiveState)
            }
            .disposed(by: view.bag)

        view
            .swipeThatShowsGiftsPopUp
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.handleOnGiftTap(false)
            }
            .disposed(by: view.bag)

        view
            .playButtonTapped
            .observeOn(MainScheduler.instance)
            .bind { [weak self] _ in
                self?.viewModel.changePlayerStateToOpposite()
            }
            .disposed(by: view.bag)

        view
            .sliderPanEvent
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe { [weak self] (value, isTracking) in
                self?.viewModel.setPlayersCurrentTime(to: value, andPauseTheVideo: isTracking)
            }
            .disposed(by: view.bag)

        viewModel
            .streamIsUnrichable
            .observeOn(MainScheduler.instance)
            .bind(to: view.loaderIsActive)
            .disposed(by: view.bag)

        viewModel
            .playerIsPaused
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak view] playerIsPaused in
                guard let view = view else { return }

                if !playerIsPaused {
                    view.startDisplayingProgres()
                    view.avatarView.isHidden = true
                }
            })
            .disposed(by: view.bag)

        viewModel
            .onRemoveLessonView
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak view] _ in
                view?.removeStreamView()
            })
            .disposed(by: view.bag)

        viewModel
            .onPresentVipLessonPopup
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak view] popup in
                view?.presentVipLessonPopup(popupView: popup)
            })
            .disposed(by: view.bag)

        viewModel
            .onShowLoader
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak view] show in
                view?.loaderIsActive.accept(show)

                if show {
                    view?.showLoader()
                } else {
                    view?.hideLoader()
                }
            })
            .disposed(by: view.bag)
    }
    
    private func shareApp() {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "talkme.page.link"
        components.path = "/stream"

        let lessonIdQueryItem = URLQueryItem(name: "lessonId", value: "\(viewModel.currentLessonId ?? 0)")
        let userIdQueryItem = URLQueryItem(name: "userId", value: "\(viewModel.currentTeacherId ?? UserDefaultsHelper.shared.userId)")
        components.queryItems = [lessonIdQueryItem, userIdQueryItem]

        guard let linkParameter = components.url else { return }

        let vc = UIActivityViewController(activityItems: [linkParameter], applicationActivities: nil)
        present(vc, animated: true)
    }
}

// MARK: - extensions

private extension CollectionView {
    func scroll(to index: Int, animated: Bool) throws {
        guard self.provider as? ComposedProvider == nil else {
            throw(CollectionKitError.unableToScroll)
        }
        guard let provider = self.provider else {
            throw(CollectionKitError.unableToScroll)
        }
        let itemFrame = provider.frame(at: index)
        var targetPoint = itemFrame.origin
        let itemXDiff = self.contentSize.width - itemFrame.origin.x
        let itemYDiff = self.contentSize.height - itemFrame.origin.y
        if itemXDiff < self.visibleFrame.width || itemYDiff < self.visibleFrame.height {
            targetPoint = CGPoint(
                x: self.contentSize.width - self.visibleFrame.width,
                y: self.contentSize.height - self.visibleFrame.height
            )}
        if animated {
            UIView.animate(withDuration: 0.3) {
                self.contentOffset = targetPoint
            }
        } else {
            self.contentOffset = targetPoint
        }
    }
}
