//
//  SavedStreamsFeedView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 8/7/2022.
//

import RxCocoa
import RxSwift
import AVFoundation
import CollectionKit
import UIKit
import AVKit

final class SavedStreamFeedView: UIView {

    // MARK: - Outputs
    private(set) lazy var onAvatarButtonTap = actionsView.onAvatarButtonTap
    private(set) lazy var onAddTeacherButtonTap = actionsView.onAddTeacherButtonTap
    private(set) lazy var onBackButtonTap = actionsView.onBackButtonTap
    private(set) lazy var onHelpButtonTap = actionsView.onHelpButtonTap
    private(set) lazy var onShareTap = actionsView.onShareTap
    private(set) lazy var onCoinTap = actionsView.onCoinTap
    private(set) lazy var onMicrophoneTap = actionsView.onMicrophoneTap
    private(set) lazy var onGiftButtonTap = actionsView.onGiftButtonTap
    private(set) lazy var onLikeTap = actionsView.onLikeTap
    
    private(set) lazy var isMicroOn = actionsView.isMicroOn

//    private(set) lazy var giftButtonTapped = actionsView.giftButtonTapped
    private(set) lazy var playButtonTapped = actionsView.playButtonTapped
    private(set) lazy var swipeThatShowsGiftsPopUp = actionsView.onShowPopUp
//    private(set) lazy var onFullScreenTap = actionsView.fullScreenButtonTapped
    private(set) lazy var onShowMembersPopUp = actionsView.onShowMembersPopUp
    private(set) lazy var sliderPanEvent = actionsView.sliderPanEvent
    private(set) lazy var avatarIsSetted = BehaviorRelay<Bool>(value: false)

    // MARK: - Inputs

    private(set) lazy var loaderIsActive = actionsView.loaderIsActive

    // MARK: - Public Properties

    weak var viewModel: SavedStreamsFeedViewModel?
    var isContact = false

    var bag = DisposeBag()

    private(set) var avatarView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()

    // MARK: - Private Properties

    private var streamView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .clear
        iv.contentMode = .scaleAspectFill
        return iv
    }()

    private var loaderView: SavedStreamLoaderView?
    private let actionsView: SavedStreamControlsView 
    private var playerView = UIView(frame: .init(origin: .zero, size: UIScreen.size))
    private var playerContentSize: CGSize?
    private var avatarImage: UIImage?
    private var showAvatarFromTeacher = false
    private var showAvatarFromLearner = false
    private var popupView: VipLessonPopupViewForSavedStreams?
    private var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    private var lessonId: Int = 0
    private var oldTeacherCameraIsActiveState = false
    private var pictureInPictureController: AVPictureInPictureController?
    private var firstTimeStart: Bool = true

    // MARK: - Initializers

    init(streamModel: SavedStreamModel) {
        let userIsStreamOwner = streamModel.specialistInfo?.id == UserDefaultsHelper.shared.userId
        actionsView = SavedStreamControlsView(forOwner: userIsStreamOwner, type: .viewer, classNumber: streamModel.specialistInfo?.numberClass)
        super.init(frame: .zero)
        setAvatar(streamModel.specialistInfo?.avatarUrlOriginal)
        setupLayout()
        bindUI()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        updateFrames(for: UIScreen.size)

        avatarView.frame = CGRect(x: 0,
                                  y: (UIScreen.height - ((UIScreen.height/3) * 2))/2,
                                  width: UIScreen.width,
                                  height: (UIScreen.height/3) * 2)

        updateVisibleFullScreenButton()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        print("[DEINIT VIEWER VIEW]")
        removePlayerLayer()
    }

    // MARK: - Public Methods
    func setValue(isContact: Bool) {
        self.isContact = isContact
        actionsView.setValue(isContact: self.isContact)
    }

    func startDisplayingProgres() {
        if firstTimeStart {
            actionsView.stratDisplayingProgress()
            playerView.isHidden = false
            presentStream()
            firstTimeStart.toggle()
            avatarView.isHidden = true
        }
    }

    func configure(streamModel: SavedStreamModel, lessonDetail: LessonDetailResponse, cameraIsActive: Bool, isWatchingSavedStream: Bool, isConnect: Bool) {
        bag = .init()
        bindUI()
        setAvatar(streamModel.specialistInfo?.avatarUrlOriginal)
        showFullscreenAvatarPlaceholder(!cameraIsActive)
        actionsView.configure(streamModel: streamModel, lessonDetail: lessonDetail, isContact: isConnect)
    }

    func setPlayer(player: AVPlayer?, withDuration duration: Double) {
        playerView.isHidden = true
        pictureInPictureController?.stopPictureInPicture()
        pictureInPictureController = nil
        self.playerLayer?.removeFromSuperlayer()
        guard let player = player else { return }
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
        playerLayer.contentsGravity = .resizeAspectFill
        self.playerLayer = playerLayer
        playerView.layer.addSublayer(playerLayer)
        self.player = player

        setNeedsLayout()
        layoutIfNeeded()

        pictureInPictureController = AVPictureInPictureController(playerLayer: playerLayer)
        pictureInPictureController?.delegate = self
        actionsView.setPlayer(to: player, withDuration: duration)
    }

    func setPlayerSize(size: CGSize) {
        playerContentSize = size
        setNeedsLayout()
        layoutIfNeeded()
    }

    func presentStream() {
        streamView.subviews.forEach { $0.removeFromSuperview() }
        streamView.addSubview(playerView)
        actionsView.showTimeControls()
    }

    func prepareForReuse(streamModel: SavedStreamModel, andShowingSavedStream showingSavedStream: Bool) {
        firstTimeStart = true
        avatarView.isHidden = false
        player?.pause()
        self.playerLayer?.removeFromSuperlayer()
        self.popupView?.removeFromSuperview()
        let complaintButtonIsVisible = streamModel.specialistInfo?.id != UserDefaultsHelper.shared.userId
        actionsView.stopDisplayingProgress()
        setAvatar(streamModel.specialistInfo?.avatarUrlOriginal)
        hideLoader()
    }

    func setAvatar(_ coverImage: String?) {
        avatarView.image = nil
        avatarImage = nil
        guard let coverImage = coverImage, let imageUrl = URL(string: coverImage)
        else {
            avatarIsSetted.accept(true)
            return
        }
        avatarView.kf.setImage(with: imageUrl) { [weak self] result in
            self?.avatarIsSetted.accept(true)
            switch result {
            case .success(let image):
                self?.avatarImage = image.image
            case .failure:
                break
            }
        }
    }

    func showFullscreenAvatarPlaceholder(_ show: Bool) {
        showAvatarFromLearner = show
        if show {
            playerView.isHidden = show
        } else {
            guard showAvatarFromTeacher == show else { return }
            playerView.isHidden = show
        }
    }

    func hideVideoByTeacher(_ hide: Bool) {
        playerView.isHidden = hide
    }

    func removeStreamView() {
        playerView.removeFromSuperview()
        avatarView.removeFromSuperview()
        actionsView.removeFromSuperview()
    }

    func presentVipLessonPopup(popupView: VipLessonPopupViewForSavedStreams) {
        self.popupView = popupView
        playerLayer?.player?.isMuted = true
        actionsView.addSubview(popupView)
        actionsView.setVipLessonState(true)
        popupView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    func removeVipLessonPopup() {
        popupView?.removeFromSuperview()
        popupView = nil
        actionsView.setVipLessonState(false)
        playerLayer?.player?.isMuted = false
    }

    func showLoader() {
        if loaderView == nil {
            let loaderView = SavedStreamLoaderView()
            self.addSubview(loaderView)
            loaderView.center.x = self.streamView.center.x
            loaderView.center.y = self.streamView.center.y
            loaderView.animate(circle: loaderView.circle1, counter: 1)
            loaderView.animate(circle: loaderView.circle2, counter: 3)
            self.loaderView = loaderView
        }
    }

    func hideLoader() {
        loaderView?.stopAnimating()
        loaderView?.removeFromSuperview()
        loaderView = nil
    }

    func videoIsHidden() -> Bool {
        guard let playerLayer = playerLayer,
              let sublayers = playerView.layer.sublayers
        else { return true}
        return !sublayers.contains(playerLayer) || playerView.isHidden
    }

    // MARK: - Private Methods

    private func removePlayerLayer() {
        playerLayer?.player?.pause()
        playerLayer?.removeFromSuperlayer()
        playerLayer = nil
    }

    private func setupLayout() {
        addSubviews(avatarView,
                    streamView,
                    actionsView)
        bringSubviewToFront(actionsView)
        updateFrames(for: UIScreen.size)
    }

    private func updateFrames(for size: CGSize) {
        playerView.frame = .init(origin: .zero, size: size)
        streamView.frame = .init(origin: .zero, size: size)
        actionsView.frame = .init(origin: .zero, size: size)
        updatePlayerSize(size)
    }

    private func updatePlayerSize(_ screenSize: CGSize) {
        guard let playerLayer = playerLayer else { return }

        guard let playerContentSize = playerContentSize else {
            playerLayer.frame = CGRect(origin: .zero, size: screenSize)
            return
        }

        let isPortrait = UIDevice.current.orientation.isPortrait
        let isLandscape = playerContentSize.width > playerContentSize.height

        switch (isPortrait, isLandscape) {
        case (true, true):
            let ratio = screenSize.width / playerContentSize.width
            let width = screenSize.width
            let height = playerContentSize.height * ratio

            playerLayer.frame = CGRect(origin: CGPoint(x: 0, y: 70), size: CGSize(width: width, height: height))
        case (false, false):
            let ratio = screenSize.height / playerContentSize.height
            let width = playerContentSize.width * ratio
            let height = screenSize.height
            let xOrigin = screenSize.width / 2 - width / 2

            playerLayer.frame = CGRect(origin: CGPoint(x: xOrigin, y: 0), size: CGSize(width: width, height: height))
        case (true, false), (false, true):
            playerLayer.frame = CGRect(origin: .zero, size: screenSize)
        }
    }

    private func bindUI() {
        loaderIsActive
            .skip(2)
            .distinctUntilChanged()
            .bind { [weak self] videoIsLoading in
//                videoIsLoading ? self?.showLoader() : self?.hideLoader()
                if !videoIsLoading { self?.presentStream() }
                self?.hideVideoByTeacher(videoIsLoading)
            }
            .disposed(by: bag)
    }

    private func updateVisibleFullScreenButton() {
        var isHiddenFullScreenButton = UIApplication.shared.statusBarOrientation.isPortrait

        if isHiddenFullScreenButton == true, let playerContentSize = playerContentSize {
            isHiddenFullScreenButton = playerContentSize.height > playerContentSize.width
        }
    }
}

extension SavedStreamFeedView: AVPictureInPictureControllerDelegate {

    func pictureInPictureControllerWillStartPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        guard let viewModel = self.viewModel else { return }
        viewModel.isActivePictureInPicture = true
    }

    func pictureInPictureControllerWillStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        guard let viewModel = self.viewModel else { return }
        if viewModel.isActivePictureInPicture == true {
            viewModel.flow.accept(.onBack(animated: false))
        }
        viewModel.isActivePictureInPicture = false
    }

    func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController,
                                    restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        guard let viewModel = self.viewModel else { return }
        viewModel.isActivePictureInPicture = false
        completionHandler(true)
    }
}
