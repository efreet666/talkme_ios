//
//  SavedStreamsFeedCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 10/7/2022.
//

import RxCocoa
import RxSwift
import AVFoundation

struct SavedStreamModel {
    let id: Int
    var specialistInfo: Owner?
    let lessonDateEnd: Double?
    let categoryId: Int
}

final class SavedStreamsFeedCellViewModel {

    var streamModel: SavedStreamModel
    let bag = DisposeBag()

    init(streamModel: SavedStreamModel) {
        self.streamModel = streamModel
    }
}
