//
//  SavedStreamsRepeatableViewProtocol.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 12/9/2022.
//

import RxCocoa
import RxSwift

protocol SavedStreamsRepeatableViewProtocol: UIView {
    var size: CGSize { get }
    var viewTag: Int { get }
    var bag: DisposeBag { get }

    init(size: CGSize, isSelected: Bool, viewTag: Int, viewsData: SavedStreamsGenericViewType)

    func setSelected(_ selected: Bool)
}

protocol SavedStreamsRepeatingViewsMakerProtocol {
    associatedtype ViewProtocol = RepeatableViewProtocol

    var viewsArray: [ViewProtocol] { get set }
    var count: Int { get }
    var onViewTap: PublishSubject<ViewProtocol> { get}
}

enum SavedStreamsGenericViewType {
    case pay(SavedStreamsPrice)
    case pageControl
}

final class SavedStreamsRepeatingViewsMakerView<T: SavedStreamsRepeatableViewProtocol>: UIView, SavedStreamsRepeatingViewsMakerProtocol {

    var viewsArray: [T] = []
    let count: Int
    let onViewTap = PublishSubject<T>()

    init(viewsData: [SavedStreamsGenericViewType], viewType: T.Type, size: CGSize, spacing: CGFloat) {
        self.count = viewsData.count
        var width: CGFloat = 0
        for index in 0..<self.count {
            let view = T.init(size: size, isSelected: false, viewTag: index, viewsData: viewsData[index])
            let x = CGFloat(index) * (spacing + size.width)
            let origin = CGPoint(x: x, y: 0)
            view.frame = .init(origin: origin, size: size)
            width += view.frame.maxX
            viewsArray.append(view)
        }
        let lastViewX = viewsArray.last?.frame.maxX ?? 0
        super.init(frame: CGRect(x: 0, y: 0, width: lastViewX, height: size.height))
        setupLayout()
        viewsArray.forEach { bindView($0) }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func bindView(_ view: T) {
        view.rx
            .tapGesture()
            .when(.recognized)
            .compactMap { [weak view] _ in
                return view }
            .bind(to: onViewTap)
            .disposed(by: view.bag)
    }

    private func setupLayout() {
        addSubviews(viewsArray)
    }
}
