//
//  PopUp.swift
//  talkme_ios
//
//  Created by andy on 28.07.2022.
//

import RxSwift
import UIKit

private enum PopUpViewState {
    case idle
    case show
    case dismiss
}

final class PopUp {

    // MARK: Private properties

    private let bag = DisposeBag()
    private var popUpView: UIView!
    private let showHideDuration = 0.3

    private lazy var shadowGradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [
            UIColor(red: 0, green: 0, blue: 0, alpha: 0.54).cgColor,
            UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        ]
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer.transform = CATransform3DMakeAffineTransform(
            CGAffineTransform(
                a: 0,
                b: 0.62,
                c: -0.62,
                d: 0,
                tx: 0.81,
                ty: 0.38
            )
        )
        return layer
    }()

    private lazy var shadowView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 115))
        view.layer.masksToBounds = true
        view.layer.addSublayer(shadowGradient)

        let size = view.bounds.size
        shadowGradient.frame = view.bounds.insetBy(
            dx: -0.5 * size.width,
            dy: -0.5 * size.height
        )
        return view
    }()

    private var state: PopUpViewState = .idle {
        didSet {
            guard oldValue != state else { return }
            switch state {
            case .idle: return
            case .show: show()
            case .dismiss: dismiss()
            }
        }
    }

    // MARK: Lifecycle

    init(title: String) {
        popUpView = initPopup(title: title)
        bindUI()
    }

    // MARK: Public methods

    func present() {
        state = .show
    }

    // MARK: Private methods

    private func initPopup(title: String) -> UIView {
        let height = 50
        let screenWidth = Int(UIScreen.main.bounds.width)
        let width = min(screenWidth, 304)
        let xPos = (screenWidth - width) / 2
        return PopUpView(title: title, frame: CGRect(x: xPos, y: -height, width: width, height: height))
    }

    private func bindUI() {
        popUpView
           .rx
           .tapGesture()
           .when(.recognized)
           .subscribe(onNext: { [weak self] _ in
                self?.state = .dismiss
           })
           .disposed(by: bag)
    }

    private func show() {
        let window = UIApplication.shared.keyWindow!
        let topPadding = window.safeAreaInsets.top + 10

        shadowView.alpha = 0

        window.addSubviews([shadowView, popUpView])

        UIView.animate(withDuration: showHideDuration) {
            let newY = topPadding - self.popUpView.frame.origin.y
            self.popUpView.transform = CGAffineTransform(translationX: 0, y: newY)
            self.shadowView.alpha = 1
        } completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.state = .dismiss
            }
        }
    }

    private func dismiss() {
        UIView.animate(withDuration: showHideDuration) {
            self.popUpView.transform = .identity
            self.shadowView.alpha = 0
        } completion: { _ in
            self.popUpView.removeFromSuperview()
            self.shadowView.removeFromSuperview()
        }
    }
}
