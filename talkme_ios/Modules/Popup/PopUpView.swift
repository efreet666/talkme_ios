//
//  PopUpView.swift
//  talkme_ios
//
//  Created by andy on 28.07.2022.
//

import UIKit
import SnapKit

final class PopUpView: UIView {

    // MARK: Private properties

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        return label
    }()

    private lazy var gradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [
            UIColor(red: 0.052, green: 0.73, blue: 0.364, alpha: 1).cgColor,
            UIColor(red: 0.052, green: 0.73, blue: 0.608, alpha: 1).cgColor
        ]
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer.transform = CATransform3DMakeAffineTransform(
            CGAffineTransform(
                a: 0.81,
                b: -0.59,
                c: 0.59,
                d: 35.2,
                tx: -0.17,
                ty: -16.9
            )
        )
        return layer
    }()

    // MARK: Lifecycle

    init(title: String, frame: CGRect) {
        super.init(frame: frame)

        titleLabel.text = title
        confugureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let size = bounds.size
        gradient.frame = bounds.insetBy(
            dx: -0.5 * size.width,
            dy: -0.5 * size.height
        )
    }

    // MARK: Private methods

    private func confugureUI() {
        layer.cornerRadius = 7
        layer.masksToBounds = true

        layer.addSublayer(gradient)

        addSubview(titleLabel)

        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
