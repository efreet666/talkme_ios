//
//  AuthRulesVC.swift
//  talkme_ios
//
//  Created by 1111 on 05.03.2021.
//

import UIKit
import WebKit

final class AppRulesVC: UIViewController {

    // MARK: - Private Properties

    private let webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        return webView
    }()

    private var urlString: String

    // MARK: - init

    init(urlString: String) {
        self.urlString = urlString
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        getRules()
        navigationItem.leftBarButtonItem = .init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction))
    }

    // MARK: - Private Methods

    private func setLayout() {
        view.addSubview(webView)
        webView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func getRules() {
        guard let url = URL(string: urlString) else { return }
        let rulesRequest = URLRequest(url: url)
        webView.load(rulesRequest)
    }

    @objc private func cancelAction() {
        dismiss(animated: true)
    }
}
