//
//  MainCoordinator.swift
//  talkme_ios
//
//  Created by 1111 on 12.02.2021.
//

import RxCocoa
import RxSwift

final class MainCoordinator {

    enum Flow {
        case exitAppToRoot
        case exitToPreviousScreen
        case showTopUpBalance
    }

    private(set) weak var navigationController: BaseNavigationController?
    let bag = DisposeBag()
    let profileTabImage = PublishRelay<String?>()
    let liveLessonsList = PublishSubject<[LiveStream]>()
    let flow = PublishRelay<Flow>()

    // MARK: - Private Properties

    private let lessonCoordinator = LessonCoordinator()
    private let streamCoordinator = StreamCoordinator()
    private let qrCoordinator = QrCodeCoordinator()
    private let chatsCoordinator = ChatsCoordinator()
    private let mainService = MainService()
    private let lessonsService = LessonsService()
    private let accountService = AccountService()
    private let paymentsService = PaymentsService()
    private var lessonsBag = DisposeBag()

    // MARK: - Initializers

    init() {
        bindStreamCoordinator()
    }

    // MARK: - Public Methods

    func start() -> UINavigationController {
        let viewModel = MainViewModel(
            mainService: mainService,
            accountService: accountService,
            lessonService: lessonsService,
            paymentsService: paymentsService
        )
        let controller = MainViewController(viewModel: viewModel)
        let navigationController = BaseNavigationController(rootViewController: controller)
        self.navigationController = navigationController
        bindMainViewModel(viewModel: viewModel)
        bindQrButton(model: viewModel)
        navigationController.setViewControllers([controller], animated: false)
        PresentImagePreloader.shared.getPaymentsTypesData()
        return navigationController
    }

    // MARK: - Private Methods

    private func bindQrButton(model: MainViewModel) {
        guard let navigationController = navigationController else { return }
        navigationController
            .qrButtonTapped
            .bind { [weak self] _ in
                guard let navController = self?.navigationController else { return }
                self?.qrCoordinator.start(from: navController)
                guard let self = self else { return }
                self.qrCoordinator
                    .onDismiss
                    .bind {
                        model.getLessons()
                    }
                    .disposed(by: self.bag)
            }
            .disposed(by: navigationController.bag)
    }

    private func showLessonDetailVC(id: Int) {
        lessonsBag = DisposeBag()
        lessonCoordinator.start(navigationController, lessonId: id)
        bindLessonCoordinator()
    }

    private func bindLessonCoordinator() {
        lessonCoordinator
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .completeLessonInfo:
                    return
                case .topUpBalance:
                    self?.flow.accept(.showTopUpBalance)
                case .onChat(let id):
                    self?.showChat(userId: id)
                case .exitToRoot:
                    self?.navigationController?.popToRootViewController(animated: true)
                case .exitToPreviousScreen:
                    self?.navigationController?.popViewController(animated: true)
                case .makeComplaint(let id):
                    self?.showActionSheet(userId: id)
                }
            }
            .disposed(by: lessonsBag)
    }

    private func showAllCategoriesVC() {
        let viewModel = AllLessonCategoriesViewModel(service: lessonsService)
        let controller  = AllLessonCategoriesVC(viewModel: viewModel)
        bindAllCategoriesViewModel(viewModel: viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showFiltersVC(id: String, filteredLessonsModel: CategoryFilteredLessonsViewModel, isLive: Bool) {
        let viewModel = LessonFiltersViewModel(lessonService: lessonsService, id: id, mainService: mainService, isLive: isLive)
        let controller  = LessonFiltersVC(viewModel: viewModel)
        bindAllFiltersViewModel(viewModel: viewModel, filteredLessonsModel: filteredLessonsModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func bindMainViewModel(viewModel: MainViewModel) {
        viewModel
            .profileTabImage
            .bind(to: profileTabImage)
            .disposed(by: viewModel.bag)

        viewModel
            .liveStreamsList
            .bind(to: liveLessonsList)
            .disposed(by: viewModel.bag)

        viewModel
            .flow
            .bind(onNext: { [weak self] flow in
                switch flow {
                case .onLessonDetail(let id):
                    self?.showLessonDetailVC(id: id)
                case .onAllCategories:
                    self?.showAllCategoriesVC()
                case .onFilteredCategoryVC(let categoryType):
                    self?.showCategoryFilteredLessonsVC(withCategory: categoryType)
                case .watchStream(let lessonId, let numberOfStreamsInOnePage, let countOfLoadedPages, let myBroadcasts):
                    self?.showLiveStreams(listedIn: [],
                                          orReloadFirstPages: countOfLoadedPages,
                                          addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage,
                                          withBroadcasts: myBroadcasts,
                                          startFrom: lessonId)
                case .watchSavedStream(let lessonId, let numberOfStreamsInOnePage, let streamsList):
                    self?.showSavedStreams(listedIn: streamsList,
                                           addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage,
                                           startFrom: lessonId)
                case .onProfileSelect(let classNumber):
                    guard classNumber == UserDefaultsHelper.shared.classNumber else {
                        self?.showUserAccountInfoVC(classNumber: classNumber)
                        return
                    }
                    guard let navigationController = self?.navigationController else { return }
                    self?.showAccountVC(navigationController: navigationController)
                case .onTokenExpiration:
                    self?.flow.accept(.exitAppToRoot)
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func showAccountVC(navigationController: UINavigationController) {
        let accountInfoViewModel = AccountInfoViewModel(service: accountService)
        let accountVC = AccountInfoViewController(viewModel: accountInfoViewModel)
        navigationController.pushViewController(accountVC, animated: true)
    }

    private func showUserAccountInfoVC(classNumber: String?) {
        guard let classNumber = classNumber else { return }
        let viewModel = UserAccountViewModel(service: accountService, classNumber: classNumber)
        let controller = UserAccountViewController(viewModel: viewModel)
        bindUserViewModel(viewModel: viewModel)
        self.navigationController?.pushViewController(controller, animated: true)
    }

    private func bindUserViewModel(viewModel: UserAccountViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self] flow in
                switch flow {
                case .onLessonDetail(let id):
                    self?.showLessonDetailVC(id: id)
                case .makeComplaint(let userId):
                    self?.showActionSheet(userId: userId)
                case .watchStream(let userId, let lessonId, let streamsList, let numberOfStreamsInOnePage):
                    self?.showSavedStreams(listedIn: streamsList,
                                           addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage,
                                           createdBy: userId,
                                           startFrom: lessonId)
                case .onListOfUsersSavedLessons(let userId):
                    self?.showCategoryFilteredLessonsVC(createdBy: userId, withCategory: .saved)
                }
            })
            .disposed(by: viewModel.bag)

        viewModel
            .onCreateChatTapped
            .bind { [weak self] id in
                self?.showChat(userId: id)
            }
            .disposed(by: viewModel.bag)
    }

    private func bindAllCategoriesViewModel(viewModel: AllLessonCategoriesViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self] flow in
                switch flow {
                case .showCategoryFilteredLessons(let categoryType):
                    self?.showCategoryFilteredLessonsVC(withCategory: categoryType)
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func bindAllFiltersViewModel(viewModel: LessonFiltersViewModel, filteredLessonsModel: CategoryFilteredLessonsViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self, weak filteredLessonsModel] flow in
                switch flow {
                case .showFilteredCategoriesVC(let request, let mainCategories):
                    filteredLessonsModel?.configure(request, mainCategories)
                    self?.navigationController?.popViewController(animated: false)
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func showCategoryFilteredLessonsVC(createdBy userId: Int? = nil, withCategory category: LessonCategoryType) {
        let model = CategoryFilteredLessonsViewModel(category, mainService: mainService, lessonsService: lessonsService, streamsOwnerId: userId)
        let vc = CategoryFilteredLessonsViewController(model)
        bindFilteredLessonsModel(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func bindFilteredLessonsModel(_ model: CategoryFilteredLessonsViewModel) {
        model
            .flow
            .bind { [weak self, weak model] event in
                guard let model = model else { return }
                switch event {
                case .showFilters(let id, let isLive):
                    self?.showFiltersVC(id: id, filteredLessonsModel: model, isLive: isLive)
                case .showLessonInfo(let id):
                    self?.showLessonDetailVC(id: id)
                case .showLiveStream(let lessonId, let liveStreamsInfoArray, let numberOfStreamsInOnePage):
                    self?.showLiveStreams(listedIn: liveStreamsInfoArray,
                                          addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage,
                                          withBroadcasts: nil,
                                          startFrom: lessonId)
                case .showAllSavedStreams(let lessonId, let categoriesIds, let streams, let numberOfStreamsInOnePage):
                    self?.showSavedStreams(listedIn: streams, addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage, belongingTo: categoriesIds, startFrom: lessonId)
                case .deleteButtonTapped(let lessonId):
                    self?.showDeleteStreamPopUp(for: lessonId) {
                        model.reloadStreams.accept(())
                    }
                case .onProfileSelect(let classNumber):
                    self?.showUserAccountInfoVC(classNumber: classNumber)
                case .showUsersSavedStreams(let userId, let lessonId, let categoriesIds, let streams, let numberOfStreamsInOnePage):
                    self?.showSavedStreams(listedIn: streams,
                                           addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage,
                                           belongingTo: categoriesIds,
                                           createdBy: userId,
                                           startFrom: lessonId)
                }
            }
            .disposed(by: model.bag)
    }

    private func bindStreamCoordinator() {
        streamCoordinator
            .flow
            .bind { [weak self] event in
                switch event {
                case .exitToRoot:
                    self?.navigationController?.popToRootViewController(animated: true)
                case .exitToPreviousScreen(let exitIsAnimated):
                    self?.navigationController?.popViewController(animated: exitIsAnimated)
                case .toChat(let id):
                    self?.showChat(userId: id)
                case .makeComplaint(let id):
                    self?.showActionSheet(userId: id)
                }
            }
            .disposed(by: bag)

        streamCoordinator
            .createChatProfileTeacher
            .bind { [weak self] id in
                self?.showChat(userId: id)
            }.disposed(by: bag)
    }

    private func showLiveStreams(listedIn streamsList: [LiveStream],
                                 orReloadFirstPages countOfStreamsToReload: Int = 0,
                                 addNewStreamsToListByChunksWithSizeOf countOfStreamsInOnePage: Int,
                                 withBroadcasts myBroadcasts: MyBroadcastsResponse?,
                                 startFrom lessonId: Int
    ) {
        streamCoordinator.start(navigationController,
                                lessonId: lessonId,
                                lessonsInfo: streamsList,
                                countOfLoadedPages: countOfStreamsToReload,
                                numberOfStreamsInPage: countOfStreamsInOnePage,
                                broadcastsList: myBroadcasts,
                                watchSave: false)
    }

    private func showSavedStreams(listedIn streamsList: [LiveStream],
                                  addNewStreamsToListByChunksWithSizeOf countOfStreamsInOnePage: Int,
                                  belongingTo categoriesIds: [Int] = [0],
                                  createdBy userId: Int? = nil,
                                  startFrom lessonId: Int) {
        streamCoordinator.start(navigationController,
                                lessonId: lessonId,
                                lessonsInfo: streamsList,
                                numberOfStreamsInPage: countOfStreamsInOnePage,
                                watchSave: true,
                                watchStreamsCreatedBy: userId,
                                withCategories: categoriesIds)
    }

    private func showDeleteStreamPopUp(for lessonId: Int, action: @escaping () -> Void) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let deleteAction = UIAlertAction(title: "popup_delete_lesson_popup_title".localized, style: .default) { [weak self] _ in
            guard let self = self
            else { return }
            self.lessonsService
                .deleteLesson(withId: lessonId)
                .subscribe { event in
                    switch event {
                    case .success:
                        action()
                    case .error:
                        print("Get Error when try to delete lesson with id \(lessonId)")
                    }
                }.disposed(by: self.bag)
        }
        deleteAction.setValue(UIColor.red, forKey: "titleTextColor")

        let cancelAction = UIAlertAction(title: "popup_delete_profile_no".localized, style: .cancel)
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")

        alert.addAction(deleteAction)
        alert.addAction(cancelAction)

        navigationController?.present(alert, animated: true)
    }

    private func showCategoryFiteredLessonsVC(createdBy userId: Int) {
        let model = CategoryFilteredLessonsViewModel(LessonCategoryType.saved,
                                                     mainService: mainService,
                                                     lessonsService: lessonsService,
                                                     streamsOwnerId: userId)
        let vc = CategoryFilteredLessonsViewController(model)
        bindFilteredLessonsModel(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func showChat(userId: Int) {
        guard let navigationController = navigationController else { return }
        chatsCoordinator.startChat(navigationController, userId: userId)
    }

    private func showReportPopUp(userId: Int) {
        let view = StreamComplaintPopUpView(accountService: accountService, userAbout: userId )
        let drawer = DrawerViewController(contentView: view)

        view.flow
            .bind { [weak self] flow in
                self?.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self?.presentAlertAndCloseViewController()
                case .dissmissPopupWithError:
                    self?.presentAlertWithError()
                }
            }
            .disposed(by: bag)

        navigationController?.present(drawer, animated: true)
    }

    private func presentAlertWithError() {
        let alert = UIAlertController(title: "complaint_alert_error".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "general_ok".localized, style: .cancel)
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func presentAlertAndCloseViewController() {
        let alert = UIAlertController(title: "complaint_alert_success".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "general_ok".localized, style: .cancel) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func showActionSheet(userId: Int) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let reportAction = UIAlertAction(title: "complaint_button".localized, style: .default) { [weak self] _ in
            self?.showReportPopUp(userId: userId)
        }
        reportAction.setValue(UIColor.red, forKey: "titleTextColor")

        let blockAction = UIAlertAction(title: "complaint_block_button_title".localized, style: .default) { [weak self] _ in
            self?.showBlockPopUp(userId: userId)
        }
        blockAction.setValue(UIColor.red, forKey: "titleTextColor")

        let cancelAction = UIAlertAction(title: "complaint_cancel".localized, style: .cancel)
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")

        alert.addAction(blockAction)
        alert.addAction(reportAction)
        alert.addAction(cancelAction)

        navigationController?.present(alert, animated: true)
    }

    private func showBlockPopUp(userId: Int) {
        guard let accountService = accountService as? AccountService else { return }
        let view = StreamBlockUserPopUpView(accountService: accountService, userAbout: userId )
        let drawer = DrawerViewController(contentView: view)

        view.flow
            .bind { [weak self] flow in
                self?.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self?.presentSuccessBlockAndCloseStream()
                case .dissmissPopupWithError:
                    self?.presentAlertWithError()
                case .cancelPopup:
                    self?.dismissDrawer()
                }
            }
            .disposed(by: view.bag)

        navigationController?.present(drawer, animated: true)
    }

    private func presentSuccessBlockAndCloseStream() {
        let alert = UIAlertController(title: "block_alert_success".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "general_ok".localized, style: .cancel) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func dismissDrawer() {
        navigationController?.dismiss(animated: true)
    }
}
