//
//  AllLessonCategoriesViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 19.02.2021.
//

import RxCocoa
import RxSwift

final class AllLessonCategoriesViewModel {

    enum Flow {
        case showCategoryFilteredLessons(LessonCategoryType)
    }

    // MARK: - Public Properties

    let tableDataItems = PublishRelay<[TableCategoryCellVM]>()
    let collectionDataItems = PublishRelay<[CollectionCategoryCellVM]>()
    let bag = DisposeBag()
    let allLessons = AllLessonCategoriesModel()
    let flow = PublishRelay<Flow>()

    // MARK: - Private Properties

    private let service: LessonsServiceProtocol

    // MARK: - Initializers

    init(service: LessonsServiceProtocol) {
        self.service = service
    }

    // MARK: - Public Methods

    func setupTableData(data: [LessonCategoryType]?) {
        var tableItems = [TableCategoryCellVM]()
        var collectionItems = [CollectionCategoryCellVM]()
        if let data = data {
            tableItems = data.map { TableCategoryCellVM(categoryModel: $0) }
            collectionItems  = data.map { CollectionCategoryCellVM(categoryModel: $0) }
        } else {
            tableItems = allLessons.data.map { TableCategoryCellVM(categoryModel: $0) }
            collectionItems  = allLessons.data.map { CollectionCategoryCellVM(categoryModel: $0) }
        }
        tableDataItems.accept(tableItems)
        collectionDataItems.accept(collectionItems)
    }

    func onEnter(text: String) {
        var filteredData = [LessonCategoryType]()
        if text.isEmpty {
            filteredData = allLessons.data
        } else {
            filteredData = allLessons.data.filter { $0.title.contains(text) }
        }
        setupTableData(data: filteredData)
    }
}
