//
//  TableCategoryCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 19.02.2021.
//

import UIKit

final class TableCategoryCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let categoryModel: LessonCategoryType

    // MARK: - Initializers

    init(categoryModel: LessonCategoryType) {
        self.categoryModel = categoryModel
    }

    // MARK: - Public Methods

    func configure(_ cell: TableCategoryCell) {
        cell.configure(categoryModel)
    }
}
