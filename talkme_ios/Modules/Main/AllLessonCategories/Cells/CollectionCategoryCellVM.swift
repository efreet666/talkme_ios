//
//  CollectionCategoryCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 19.02.2021.
//

import UIKit

final class CollectionCategoryCellVM: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let categoryModel: LessonCategoryType

    // MARK: - Initializers

    init(categoryModel: LessonCategoryType) {
        self.categoryModel = categoryModel
    }

    // MARK: - Public Methods

    func configure(_ cell: CollectionCategoryCell) {
        cell.configure(categoryModel)
    }
}
