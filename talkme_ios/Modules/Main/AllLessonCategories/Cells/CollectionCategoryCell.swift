//
//  CollectionCategoryCell.swift
//  talkme_ios
//
//  Created by 1111 on 19.02.2021.
//

import RxSwift

final class CollectionCategoryCell: UICollectionViewCell {

    // MARK: - Private Properties

    private let categoryButton = CategoryRoundView()

    private let label: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        lbl.textColor = TalkmeColors.codeCountry
        lbl.textAlignment = .center
        return lbl
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ type: LessonCategoryType) {
        categoryButton.iconImageView.image = type.whiteBackgroundCategoriesImage
        label.text = type.title
    }

    // MARK: - Private Methods

    private func initialSetup() {
        addSubviews(categoryButton, label)
        categoryButton.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 52 : 68)
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
        }

        label.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
            make.height.equalTo(20)
            make.top.equalTo(categoryButton.snp.bottom)
        }
    }
}
