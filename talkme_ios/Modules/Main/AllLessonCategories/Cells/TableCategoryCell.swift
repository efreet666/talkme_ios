//
//  TableCategoryCell.swift
//  talkme_ios
//
//  Created by 1111 on 19.02.2021.
//

import RxSwift

final class TableCategoryCell: UITableViewCell {

    // MARK: - Private Properties

    private let categoryButton = ClearCategoryRoundView()

    private let label: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.montserratSemiBold(ofSize: 15)
        lbl.textColor = TalkmeColors.grayLabels
        lbl.textAlignment = .left
        return lbl
    }()

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = 8
        return view
    }()

    private let accessoryImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "accessoryView"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialSetup()
        cellStyle()
        backgroundColor = .clear
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ type: LessonCategoryType) {
        categoryButton.iconImageView.image = type.whiteBackgroundCategoriesImage
        label.text = type.title
    }

    // MARK: - Private Methods

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func initialSetup() {
        containerView.addSubviews(categoryButton, label, accessoryImageView)
        addSubview(containerView)
        categoryButton.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.isSE ? 29 : 40)
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(UIScreen.isSE ? 13 : 14)
        }

        label.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalTo(categoryButton.snp.trailing).offset(UIScreen.isSE ? 12 : 15)
            make.centerX.equalToSuperview()
        }

        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().inset(8)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 41 : 54)
        }

        accessoryImageView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 13 : 17)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 12 : 15)
            make.height.equalTo(UIScreen.isSE ? 15 : 19)
            make.width.equalTo(UIScreen.isSE ? 8 : 11)
        }
    }
}
