//
//  AllLessonCategoriesVC.swift
//  talkme_ios
//
//  Created by 1111 on 19.02.2021.
//

import RxCocoa
import RxSwift

final class AllLessonCategoriesVC: UIViewController {

    enum Constants {
        static let inset: CGFloat = UIScreen.isSE ? 28 : 38
        static let collectionCellWidth: CGFloat = (UIScreen.main.bounds.width - inset * 3) / 4
        static let height: CGFloat = UIScreen.isSE ? 88 : 115
    }

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.keyboardDismissMode = .onDrag
        tbv.rowHeight = UITableView.automaticDimension
        tbv.registerCells(withModels: TableCategoryCellVM.self)
        tbv.isUserInteractionEnabled = true
        tbv.isHidden = true
        return tbv
    }()

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: Constants.collectionCellWidth, height: Constants.height)
        layout.scrollDirection = .vertical
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.contentInset = UIScreen.isSE
            ? UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17)
            : UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        cvc.registerCells(withModels: CollectionCategoryCellVM.self)
        return cvc
    }()

    private let searchBar: StreamSearchBar = {
        let bar = StreamSearchBar()
        bar.searchTextField.placeholder = "talkme_stream_list_search".localized
        bar.searchTextField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        bar.searchTextField.minimumFontSize = UIScreen.isSE ? 13 : 15
        bar.backgroundColor = TalkmeColors.white
        bar.layer.cornerRadius = UIScreen.isSE ? 17 : 21
        return bar
    }()

    private let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "talkme"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let allCategoriesLabel: UILabel = {
        let label = UILabel()
        label.text = "main_page_category_all_categories".localized
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let сategoriesLayoutButtons = CategoriesLayoutButtons()
    private let viewModel: AllLessonCategoriesViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: AllLessonCategoriesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.mainAccountBackground
        setupViews()
        bindVM()
        viewModel.setupTableData(data: nil)
        bindUI()
        сategoriesLayoutButtons.setCollectionLayoutActive()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    // MARK: - Private Methods

    private func bindUI() {
        сategoriesLayoutButtons
            .collectionButtonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.сategoriesLayoutButtons.setCollectionLayoutActive()
                self.collectionView.isHidden = false
                self.tableView.isHidden = true
            }
            .disposed(by: bag)

        сategoriesLayoutButtons
            .tableButtonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.сategoriesLayoutButtons.setTableLayoutActive()
                self.collectionView.isHidden = true
                self.tableView.isHidden = false
            }
            .disposed(by: bag)

        collectionView.rx
            .modelSelected(AnyCollectionViewCellModelProtocol.self)
            .compactMap { item in
                if let items = item as? CollectionCategoryCellVM {
                    return AllLessonCategoriesViewModel.Flow.showCategoryFilteredLessons(items.categoryModel)
                }
                return nil
            }
            .bind(to: viewModel.flow)
            .disposed(by: bag)

        tableView.rx
            .modelSelected(AnyTableViewCellModelProtocol.self)
            .compactMap { item in
                if let items = item as? TableCategoryCellVM {
                    return AllLessonCategoriesViewModel.Flow.showCategoryFilteredLessons(items.categoryModel)
                }
                return nil
            }
            .bind(to: viewModel.flow)
            .disposed(by: bag)
    }

    private func bindVM() {
        viewModel
            .tableDataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                tableView.deselectRow(at: indexPath, animated: true)
                return cell
            }
            .disposed(by: bag)

        viewModel
            .collectionDataItems
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        searchBar.searchTextField.rx.text
            .orEmpty
            .bind { [weak viewModel] text in
                viewModel?.onEnter(text: text)
            }
            .disposed(by: bag)
    }

    private func setupViews() {
        view.addSubviews([tableView, searchBar, collectionView, сategoriesLayoutButtons, allCategoriesLabel])

        tableView.snp.makeConstraints { make in
            make.top.equalTo(сategoriesLayoutButtons.snp.bottom).offset(UIScreen.isSE ? 14 : 21)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(additionalSafeAreaInsets)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(сategoriesLayoutButtons.snp.bottom).offset(UIScreen.isSE ? 14 : 21)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(additionalSafeAreaInsets)
        }

        searchBar.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(UIScreen.isSE ? 11 : 18)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 34 : 42)
        }

        сategoriesLayoutButtons.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom).offset(UIScreen.isSE ? 14 : 18)
            make.trailing.equalToSuperview().inset(10)
        }

        allCategoriesLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.trailing.equalTo(сategoriesLayoutButtons.snp.leading)
            make.centerY.equalTo(сategoriesLayoutButtons)
        }
    }

    private func setupNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBarImage")?
            .resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        navigationItem.titleView = logoImageView
    }
}
