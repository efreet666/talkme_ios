//
//  AllLessonCategoriesModel.swift
//  talkme_ios
//
//  Created by 1111 on 19.02.2021.
//

struct AllLessonCategoriesModel {
    let data: [LessonCategoryType] =
    [.all, .live, .nearest, .saved, .talkMeStudio, .mosStream, .education, .entertainment, .fitness, .chatting, .nightlife, .girls, .learning, .cooking,
                                      .kids, .travel, .business, .languages, .music, .yourShow, .games, .other,
     .courses, .masterClass, .sale, .car, .health, .standUp]
}
