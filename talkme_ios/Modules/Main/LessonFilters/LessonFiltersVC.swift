//
//  LessonFiltersVC.swift
//  talkme_ios
//
//  Created by 1111 on 22.02.2021.
//

import RxSwift
import RxCocoa

final class LessonFiltersVC: UIViewController {

    // MARK: - Private properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.backgroundColor = TalkmeColors.mainAccountBackground
        tbv.delaysContentTouches = false
        tbv.registerCells(
            withModels:
            FilterCategoriesViewModel.self,
            FilterSubcategoriesCellVM.self,
            FilterDateCellVM.self,
            FilterTimeStartCellVM.self,
            FilterGenderTableCellVM.self,
            FilterPriceTableCellVM.self)
        return tbv
    }()

    private let viewModel: LessonFiltersViewModel
    private let bag = DisposeBag()

    // MARK: - Init

    init(viewModel: LessonFiltersViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
        setupTableView()
        setupNavigation()
        viewModel.getCategories()
        bindKeyboard()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideCustomTabBarLiveButton(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupTableView() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigation() {
        customNavigationController?.style = .gradientBlue
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                self.tableView.contentInset.bottom = keyboardVisibleHeight
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

}
