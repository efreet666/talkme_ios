//
//  FilterSliderLabel.swift
//  talkme_ios
//
//  Created by 1111 on 02.03.2021.
//

import UIKit

final class FilterSliderLabel: UILabel {

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 13)
        textColor = TalkmeColors.placeholderColor
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
