//
//  FromAndToCoinView.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 24.03.2022.
//

import Foundation
import UIKit

final class FromAndToCoinView: UIView {

    private(set) var textField: UITextField = {
        let tf = UITextField()
        tf.font = UIFont.montserratSemiBold(ofSize: 16)
        tf.textColor = TalkmeColors.blueLabels
        tf.backgroundColor = .clear
        return tf
    }()

    private let titleLabel: UILabel = {
        let lb = UILabel()
        lb.font = UIFont.montserratFontMedium(ofSize: 13)
        lb.textColor = TalkmeColors.placeholderColor
        return lb
    }()

    // MARK: - Initializers

    init(title: String, coin: String) {
        self.titleLabel.text = title
        self.textField.text = coin
        super.init(frame: .zero)
        setupAppearance()
        setUpLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }

    private func setUpLayout() {
        addSubviews(titleLabel, textField)

        titleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(15)
            make.width.equalTo(22)
        }

        textField.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel)
            make.leading.equalTo(titleLabel.snp.trailing).inset(-2)
            make.trailing.equalToSuperview().inset(9)
            make.height.equalTo(25)
        }
    }

    private func setupAppearance() {
        backgroundColor = TalkmeColors.white
    }
}
