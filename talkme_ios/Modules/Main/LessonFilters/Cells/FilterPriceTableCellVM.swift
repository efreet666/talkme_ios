//
//  FilterPriceTableCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 02.03.2021.
//

import RxSwift
import RxCocoa

final class FilterPriceTableCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let costFrom = PublishRelay<Int?>()
    let costTo = PublishRelay<Int?>()
    let isFree = PublishRelay<Bool>()
    let buttonTapped = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String

    // MARK: - Initializers

    init(title: String) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: FilterPriceTableCell) {
        cell.configure(title: title)

        cell.costFrom
            .bind(to: costFrom)
            .disposed(by: cell.bag)

        cell
            .costTo
            .bind(to: costTo)
            .disposed(by: cell.bag)

        cell
            .isFree
            .bind(to: isFree)
            .disposed(by: cell.bag)

        cell
            .buttonTap
            .bind(to: buttonTapped)
            .disposed(by: cell.bag)
    }
}
