//
//  FilterDateCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 25.02.2021.
//

import RxSwift
import RxCocoa

final class FilterDateCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let firstDate = PublishRelay<Date>()
    let secondDate = PublishRelay<Date>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String

    // MARK: - Initializers

    init(title: String) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: FilterDateTableCell) {
        cell.configure(title: title)

        cell
            .firstDate
            .bind(to: firstDate)
            .disposed(by: cell.bag)

        cell
            .secondDate
            .bind(to: secondDate)
            .disposed(by: cell.bag)
    }
}
