//
//  FilterSubcategoriesCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 24.02.2021.
//

import RxCocoa
import RxSwift

final class FilterSubcategoriesCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let bag = DisposeBag()
    let dataItemsRelay = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let dropDownItems = PublishRelay<[Category]?>()
    let warningRelay = PublishRelay<Void>()
    let onClearTextField = PublishRelay<Void>()
    let onEndTextField = PublishRelay<Void>()
    let onSelectedItems = PublishRelay<[Int]>()
    let textRelay = BehaviorRelay<String?>(value: "")

    // MARK: - Private Properties

    private let title: String
    private var textFieldTextString: String
    private var cellItems: [Category] = []
    private var dataItems: [AnyCollectionViewCellModelProtocol] = []
    private var activeSubCategory: String?
    private var selectedItemsId = [Int]()
    private let allSubCategories = Category(name: "Все", slug: nil, id: 0, parent: nil)

    // MARK: - Initializers

    init(
        title: String,
        textFieldTextString: String,
        model: [Category],
        activeSubCategory: String? = nil
    ) {
        self.title = title
        self.textFieldTextString = textFieldTextString
        self.activeSubCategory = activeSubCategory
        var model = model
        model.insert(allSubCategories, at: 0)
        let items = model.map { FiltersCollectionsCellVM(categoryModel: $0, isActive: false) }
        dataItemsRelay.accept(items)
        dropDownItems.accept(model)
        cellItems = model
        dataItems = items
    }

    // MARK: - Public Methods

    func clearDataItems() {
        dataItems.removeAll()
    }

    func configure(_ cell: FilterSubCategoriesTableCell) {

        cell.setupSizes(items: cellItems.map { $0.name ?? "" })
        cell.configure(title: title, text: activeSubCategory, textFieldString: textFieldTextString)

        dataItemsRelay
            .bind(to: cell.dataItems)
            .disposed(by: bag)

        dropDownItems
            .bind { [weak cell] items in
                cell?.setupDropDownItems(items: items?.map { $0.name ?? "" } ?? [])
            }
            .disposed(by: bag)

        cell
            .subCategoryText
            .bind { [weak self] text in
                guard let cellItems = self?.cellItems else { return }
                let filteredItems = cellItems.filter { ($0.name?.hasPrefix(text ?? "") ?? false) }
                self?.dropDownItems.accept(filteredItems)
            }
            .disposed(by: cell.bag)

        onClearTextField
            .bind { [weak cell] _ in
                cell?.clearTextFieldText()
            }
            .disposed(by: bag)

        onEndTextField
            .bind { [weak cell] _ in
                cell?.endTextFieldText()
            }
            .disposed(by: bag)

        cell
            .textFieldText
            .do(onNext: { [weak self] in
                self?.textFieldTextString = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .onItemSelected
            .bind { [weak self] name in
                guard let self = self else { return }
                if name == "Все" {
                    self.selectedItemsId = Set(self.selectedItemsId).contains(self.allSubCategories.id)
                        ? []
                        : self.cellItems.map { $0.id }
                } else {
                    self.cellItems.forEach { category in
                        if category.name == name {
                            Set(self.selectedItemsId).contains(category.id)
                                ? self.selectedItemsId = self.selectedItemsId.filter { $0 != category.id }
                                : self.selectedItemsId.append(category.id)
                        }
                    }
                }
                self.onSelectedItems.accept(self.selectedItemsId)
            }
            .disposed(by: cell.bag)

        onSelectedItems
            .bind { [weak self] idArray in
                guard let self = self else { return }
                var items = [AnyCollectionViewCellModelProtocol]()
                self.cellItems.forEach { category in
                    items.append(FiltersCollectionsCellVM(
                        categoryModel: category,
                        isActive: Set(idArray).contains(category.id)
                    ))
                }
                self.dataItems = items
                self.dataItemsRelay.accept(items)
            }
            .disposed(by: bag)

        dataItemsRelay.accept(dataItems)
    }
}
