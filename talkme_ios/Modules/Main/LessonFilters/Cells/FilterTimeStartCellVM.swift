//
//  FilterTimeStartCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 25.02.2021.
//

import RxSwift
import RxCocoa

final class FilterTimeStartCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let firstTime = PublishRelay<String?>()
    let secondTime = PublishRelay<String?>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String

    // MARK: - Initializers

    init(title: String) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: FilterTimeStartTableCell) {
        cell.configure(title: title)

        cell
            .firstTime
            .bind(to: firstTime)
            .disposed(by: cell.bag)

        cell
            .secondTime
            .bind(to: secondTime)
            .disposed(by: cell.bag)
    }
}
