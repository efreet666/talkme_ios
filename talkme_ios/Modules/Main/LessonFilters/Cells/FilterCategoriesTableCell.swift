//
//  FilterCategoriesTableCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 24.02.2021.
//

import RxSwift
import RxCocoa
import collection_view_layouts

final class FilterCategoriesTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    let onItemSelected = PublishRelay<Category>()
    let onClearTFSubCategoryText = PublishRelay<Void>()
    let dataItems = PublishRelay<[AnyCollectionViewCellModelProtocol]>()

    // MARK: - Private Properties

    private var cellSizes = [CGSize]()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blackLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        return lbl
    }()

    private let customLayout: TagsLayout = {
        let layout = TagsLayout()
        layout.scrollDirection = .horizontal
        layout.cellsPadding = ItemsPadding(horizontal: UIScreen.isSE ? 8 : 10, vertical: UIScreen.isSE ? 10 : 13)
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: customLayout)
        cvc.backgroundColor = .clear
        cvc.registerCells(withModels: FiltersCollectionsCellVM.self)
        cvc.showsHorizontalScrollIndicator = false
        return cvc
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        customLayout.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String) {
        dataItems
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        titleLabel.text = title
        bindCollectionView()
    }

    func setupCategoryNames(names: [String]) {
        cellSizes = names.map { item -> CGSize in
            let width = Double(self.collectionView.bounds.width)
            var size = UIFont.systemFont(ofSize: 13).sizeOfString(string: item, constrainedToWidth: width)
            size.width += UIScreen.isSE ? 40 : 70
            size.height += UIScreen.isSE ? 15 : 25
            return size
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindCollectionView() {
        collectionView.rx
            .modelSelected(FiltersCollectionsCellVM.self)
            .bind { [weak self] item in
                guard let self = self else { return }
                self.onItemSelected.accept((item.categoryModel))
                self.onClearTFSubCategoryText.accept(())
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, collectionView, separatorView])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 28 : 30)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 15 : 19.56)
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 138 : 176.44)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension FilterCategoriesTableCell: LayoutDelegate {

    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.row]
    }
}
