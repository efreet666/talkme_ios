//
//  FilterGenderTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 02.03.2021.
//

import RxSwift
import RxCocoa

final class FilterGenderTableCell: UITableViewCell {

    // MARK: - Public Properties

    var bag = DisposeBag()
    let activeGender = PublishRelay<String?>()

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blackLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        return lbl
    }()

    private let allButton: ButtonWithRoundedIndicator = {
        let button = ButtonWithRoundedIndicator(frame: .zero, text: "talkme_stream_list_all".localized)
        button.becomeActive(true)
        return button
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    private let maleView = GenderViewWithIndicator(frame: .zero, type: .male)
    private let femaleView = GenderViewWithIndicator(frame: .zero, type: .female)

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String) {
        titleLabel.text = title
        bindUI()
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindUI() {
        maleView.buttonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.maleView.becomeActive(true)
                self.allButton.becomeActive(false)
                self.femaleView.becomeActive(false)
                self.activeGender.accept("male")
            }
            .disposed(by: bag)

        femaleView.buttonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.femaleView.becomeActive(true)
                self.allButton.becomeActive(false)
                self.maleView.becomeActive(false)
                self.activeGender.accept("female")
            }
            .disposed(by: bag)

        allButton.buttonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.femaleView.becomeActive(false)
                self.allButton.becomeActive(true)
                self.maleView.becomeActive(false)
                self.activeGender.accept(nil)
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, maleView, femaleView, allButton, separatorView])

        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.centerY.equalTo(allButton)
            make.trailing.equalTo(allButton.snp.leading)
        }

        femaleView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 25 : 34)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 56 : 72)
            make.height.equalTo(UIScreen.isSE ? 35 : 45)
        }

        maleView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 25 : 34)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 56 : 72)
            make.leading.equalTo(femaleView.snp.trailing).offset(UIScreen.isSE ? 10 : 13.17)
            make.height.equalTo(UIScreen.isSE ? 35 : 45)
            make.width.equalTo(femaleView.snp.width)
        }

        allButton.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.isSE ? 86 : 112)
            make.height.equalTo(UIScreen.isSE ? 30 : 39)
            make.top.equalToSuperview().offset(UIScreen.isSE ? 18 : 21)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
