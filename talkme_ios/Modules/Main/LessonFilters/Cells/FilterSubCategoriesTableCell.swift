//
//  FilterSubCategoriesTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 24.02.2021.
//

import RxSwift
import RxCocoa
import DropDown
import collection_view_layouts

final class FilterSubCategoriesTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    private(set) lazy var subCategoryText = textField.rx.text
    let dataItems = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let onItemSelected = PublishRelay<String>()
    let textFieldText = PublishRelay<String>()

    // MARK: - Private Properties

    private var cellSizes = [CGSize]()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blackLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        return lbl
    }()

    private let dropDown = DropDown()

    private let customLayout: TagsLayout = {
        let layout = TagsLayout()
        layout.scrollDirection = .horizontal
        layout.cellsPadding = ItemsPadding(horizontal: UIScreen.isSE ? 8 : 10, vertical: UIScreen.isSE ? 10 : 13)
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: customLayout)
        cvc.backgroundColor = .clear
        cvc.registerCells(withModels: FiltersCollectionsCellVM.self)
        cvc.showsHorizontalScrollIndicator = false
        return cvc
    }()

    private let textField: UITextField = {
        let tf = UITextField()
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: tf.frame.height))
        let textFieldAttributes = [
            NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Medium", size: UIScreen.isSE ? 13 : 15)]
        tf.attributedPlaceholder = NSAttributedString(
            string: "create_lesson_name".localized,
            attributes: textFieldAttributes)
        tf.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        tf.leftView = paddingView
        tf.minimumFontSize = UIScreen.isSE ? 13 : 15
        tf.leftViewMode = .always
        tf.layer.cornerRadius = UIScreen.isSE ? 18 : 23.5
        tf.backgroundColor = TalkmeColors.white
        tf.font = UIFont(name: "Montserrat-Medium", size: UIScreen.isSE ? 13 : 15)
        tf.textColor = TalkmeColors.placeholderColor
        tf.clearsOnBeginEditing = true
        return tf
    }()

    private let arrow: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "arrowTextField")
        img.contentMode = .scaleAspectFit
        return img
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupDropDown()
        textField.delegate = self
        customLayout.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, text: String?, textFieldString: String) {
        titleLabel.text = title
        textField.text = text
        textField.text = textFieldString
        bindCollectionView()
        bindVM()
    }

    func clearTextFieldText() {
        textField.text = ""
    }

    func endTextFieldText() {
        textField.resignFirstResponder()
    }

    func setupDropDownItems(items: [String]) {
        dropDown.dataSource = items
    }

    func setupSizes(items: [String] ) {
        cellSizes = items.map { item -> CGSize in
            let width = Double(self.collectionView.bounds.width)
            var size = UIFont.systemFont(ofSize: 13).sizeOfString(string: item, constrainedToWidth: width)
            size.width += UIScreen.isSE ? 40 : 70
            size.height += UIScreen.isSE ? 15 : 25
            return size
        }
    }

    func showDropDown() {
        dropDown.show()
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindVM() {
        dataItems
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func bindCollectionView() {
        collectionView.rx
            .modelSelected(FiltersCollectionsCellVM.self)
            .bind { [weak self] item in
                self?.onItemSelected.accept(item.categoryModel.name ?? "")
                self?.endTextFieldText()
                self?.textField.text = item.categoryModel.name
                self?.textFieldText.accept(item.categoryModel.name ?? "")
            }
            .disposed(by: bag)
    }

    private func setupDropDown() {
        DropDown.appearance().textColor = TalkmeColors.grayLabels
        DropDown.appearance().textFont = .montserratSemiBold(ofSize: 13)
        DropDown.appearance().backgroundColor = TalkmeColors.white
        DropDown.appearance().cornerRadius = 12
        dropDown.direction = .bottom
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y: 46)

        dropDown.selectionAction = { [weak self] _, name in
            guard let self = self else { return }
            self.textField.text = name
            self.endTextFieldText()
            self.onItemSelected.accept(name)
        }
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, collectionView, textField, arrow, separatorView])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 19 : 25)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(textField.snp.bottom).offset(UIScreen.isSE ? 14 : 18)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 139 : 178)
            make.bottom.equalToSuperview()
        }

        textField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 14 : 18)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(UIScreen.isSE ? 36 : 46.95)
        }

        arrow.snp.makeConstraints { make in
            make.centerY.equalTo(textField.snp.centerY)
            make.trailing.equalTo(textField.snp.trailing).inset(UIScreen.isSE ? 15 : 20)
            make.height.equalTo(UIScreen.isSE ? 6 : 8)
            make.width.equalTo(UIScreen.isSE ? 11 : 14)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension FilterSubCategoriesTableCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField1: UITextField) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.showDropDown()
        }
        if textField1.text != nil {
            textField.text = nil
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.containsEmoji {
                return false
            }
            let substringToReplace = text[textRange]
            let count = text.count - substringToReplace.count + string.count
            return count <= 17
        }
        return true
    }
}

extension FilterSubCategoriesTableCell: LayoutDelegate {

    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.row]
    }
}
