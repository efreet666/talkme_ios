//
//  FilterDateTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 25.02.2021.
//

import RxSwift
import RxCocoa

final class FilterDateTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    lazy var firstDate = firstDatePicker.rx.date
    lazy var secondDate = secondDatePicker.rx.date

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blackLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        return lbl
    }()

    private let firstLessonDateTF = LessonCommonTextField()
    private let secondLessonDateTF = LessonCommonTextField()
    private let todayButton = ButtonWithRoundedIndicator(frame: .zero, text: "talkme_stream_list_today".localized)

    private let firstDateButton: UIButton = {
        let btn = UIButton()
        btn.contentMode = .scaleAspectFit
        btn.setImage(UIImage(named: "calendar"), for: .normal)
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        return btn
    }()

    private let secondDateButton: UIButton = {
        let btn = UIButton()
        btn.contentMode = .scaleAspectFit
        btn.setImage(UIImage(named: "calendar"), for: .normal)
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        return btn
    }()

    private let firstDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let minDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        datePicker.minimumDate = minDate
        datePicker.date = minDate ?? Date()
        return datePicker
    }()

    private let secondDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let minDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        datePicker.minimumDate = minDate
        datePicker.date = minDate ?? Date()
        return datePicker
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    private let firstDoneButton = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)
    private let secondDoneButton = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String) {
        bindUI()
        titleLabel.text = title
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindUI() {
        firstDateButton.rx.tap
            .bind { [weak self] in
                self?.firstLessonDateTF.becomeFirstResponder()
                self?.todayButton.becomeActive(false)
            }
            .disposed(by: bag)

        secondDateButton.rx.tap
            .bind { [weak self] in
                self?.secondLessonDateTF.becomeFirstResponder()
                self?.todayButton.becomeActive(false)
            }
            .disposed(by: bag)

        firstDoneButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.firstLessonDateTF.text = Formatters.transformDateToFilterStringWithFrom(fromDate: self.firstDatePicker.date)
                self.todayButton.becomeActive(false)
                self.contentView.endEditing(true)
                self.secondDatePicker.date = self.firstDatePicker.date > self.secondDatePicker.date
                    ? self.firstDatePicker.date
                    : self.secondDatePicker.date
                self.secondDatePicker.minimumDate = self.firstDatePicker.date > self.secondDatePicker.date
                    ? self.firstDatePicker.date
                    : self.secondDatePicker.date
                self.secondLessonDateTF.text = Formatters.transformDateToFilterStringWithTo(toDate: self.secondDatePicker.date)
            }
            .disposed(by: bag)

        secondDoneButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.secondLessonDateTF.text = Formatters.transformDateToFilterStringWithTo(toDate: self.secondDatePicker.date)
                self.todayButton.becomeActive(false)
                self.contentView.endEditing(true)
            }
            .disposed(by: bag)

        todayButton.buttonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.todayButton.becomeActive(true)
                let currentDate = Date()
                self.firstDatePicker.minimumDate = currentDate
                self.secondDatePicker.minimumDate = currentDate
                self.firstDatePicker.date = currentDate
                self.secondDatePicker.date = currentDate
                self.firstLessonDateTF.text = Formatters.transformDateToFilterStringWithFrom(fromDate: self.firstDatePicker.date)
                self.secondLessonDateTF.text = Formatters.transformDateToFilterStringWithTo(toDate: self.secondDatePicker.date)
            }
            .disposed(by: bag)
    }

    private func setupUI() {
        firstLessonDateTF.inputView = firstDatePicker
        self.firstLessonDateTF.text = Formatters.transformDateToFilterStringWithFrom(fromDate: self.firstDatePicker.date)
        secondLessonDateTF.inputView = secondDatePicker
        self.secondLessonDateTF.text = Formatters.transformDateToFilterStringWithTo(toDate: self.secondDatePicker.date)
        setupToolbars()
    }

    private func setupToolbars() {
        let firstToolBar = UIToolbar()
        let secondToolBar = UIToolbar()
        firstToolBar.sizeToFit()
        secondToolBar.sizeToFit()
        let firstFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let secondFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        firstToolBar.setItems([firstFlexSpace, firstDoneButton], animated: true)
        secondToolBar.setItems([secondFlexSpace, secondDoneButton], animated: true)
        firstLessonDateTF.inputAccessoryView = firstToolBar
        secondLessonDateTF.inputAccessoryView = secondToolBar
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, firstLessonDateTF, secondLessonDateTF, firstDateButton, secondDateButton, todayButton, separatorView])

        titleLabel.snp.makeConstraints { label in
            label.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            label.centerY.equalTo(todayButton)
            label.trailing.equalTo(todayButton.snp.leading)
        }

        firstLessonDateTF.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(UIScreen.isSE ? 36 : 46.95)
            make.bottom.equalTo(secondLessonDateTF.snp.top).offset(UIScreen.isSE ? -9 : -12.2)
        }

        secondLessonDateTF.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(UIScreen.isSE ? 36 : 46.95)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 29 : 33)
        }

        firstDateButton.snp.makeConstraints { button in
            button.centerY.equalTo(firstLessonDateTF.snp.centerY)
            button.trailing.equalTo(firstLessonDateTF).inset(UIScreen.isSE ? 18 : 24)
            button.size.equalTo(UIScreen.isSE ? 15 : 19)
        }

        secondDateButton.snp.makeConstraints { button in
            button.centerY.equalTo(secondLessonDateTF.snp.centerY)
            button.trailing.equalTo(secondLessonDateTF).inset(UIScreen.isSE ? 18 : 24)
            button.size.equalTo(UIScreen.isSE ? 15 : 19)
        }

        todayButton.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.isSE ? 104 : 132)
            make.height.equalTo(UIScreen.isSE ? 30 : 40)
            make.top.equalToSuperview().offset(UIScreen.isSE ? 17 : 22)
            make.bottom.equalTo(firstLessonDateTF.snp.top).offset(UIScreen.isSE ? -9 : -11.74)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
