//
//  FilterCategories.swift
//  talkme_ios
//
//  Created by 1111 on 24.02.2021.
//

import RxCocoa
import RxSwift

final class FilterCategoriesViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let bag = DisposeBag()
    let dataItemsRelay = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let onItemSelected = PublishRelay<Category>()
    let onClearTFSubCategoryText = PublishRelay<Void>()
    let onSelectedItems = PublishRelay<[Int]>()

    // MARK: - Private Properties

    private var dataItems: [AnyCollectionViewCellModelProtocol] = []
    private let title: String
    private var cellItems: [Category] = []
    var selectedItemsId = [Int]()

    // MARK: - Initializers

    init(title: String, model: [Category]) {
        self.title = title
        let items: [FiltersCollectionsCellVM] = model.map { FiltersCollectionsCellVM(categoryModel: $0, isActive: false) }
        dataItemsRelay.accept(items)
        cellItems = model
        dataItems = items
    }

    // MARK: - Public Methods

    func configure(_ cell: FilterCategoriesTableCell) {
        cell.configure(title: title)
        cell.setupCategoryNames(names: cellItems.map { $0.name ?? "" })

        cell
            .onClearTFSubCategoryText
            .bind(to: onClearTFSubCategoryText)
            .disposed(by: cell.bag)

        dataItemsRelay
            .bind(to: cell.dataItems)
            .disposed(by: bag)

        onSelectedItems
            .bind { [weak self] idArray in
                guard let self = self else { return }
                var items = [FiltersCollectionsCellVM]()
                self.cellItems.forEach { category in
                    items.append(FiltersCollectionsCellVM(
                        categoryModel: category,
                        isActive: Set(idArray).contains(category.id)
                    ))
                }
                self.dataItems = items
                self.dataItemsRelay.accept(items)
            }
            .disposed(by: bag)

        dataItemsRelay.accept(dataItems)

        cell
            .onItemSelected
            .bind { [weak self] category in
                guard let self = self else { return }
                Set(self.selectedItemsId).contains(category.id)
                    ? self.selectedItemsId = self.selectedItemsId.filter { $0 != category.id }
                    : self.selectedItemsId.append(category.id)
                self.onSelectedItems.accept(self.selectedItemsId)
            }
            .disposed(by: cell.bag)
    }
}
