//
//  FiltersCollectionsCell.swift
//  talkme_ios
//
//  Created by 1111 on 26.02.2021.
//

import RxSwift

final class FiltersCollectionsCell: UICollectionViewCell {

    // MARK: - Private properties

    private let categoryItem = CategoryButton(type: .regular)

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(categoryModel: Category, isActive: Bool) {
        categoryItem.setTitle(categoryModel.name, for: .normal)
        categoryItem.backgroundColor = isActive ? .clear : TalkmeColors.white
        categoryItem.layer.borderColor = isActive ? TalkmeColors.blueCollectionCell.cgColor : TalkmeColors.separatorView.cgColor
        categoryItem.setTitleColor(isActive ? TalkmeColors.blueCollectionCell : TalkmeColors.grayLabels, for: .normal)
    }

    // MARK: - Private Method

    private func setupLayout() {
        contentView.addSubview(categoryItem)
        categoryItem.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
