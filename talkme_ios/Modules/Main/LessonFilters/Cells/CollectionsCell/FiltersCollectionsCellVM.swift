//
//  FiltersCollectionsCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 26.02.2021.
//

import UIKit

final class FiltersCollectionsCellVM: CollectionViewCellModelProtocol {

   // MARK: - Public Properties

    let categoryModel: Category
    let isActive: Bool

    // MARK: - Initializers

    init(categoryModel: Category, isActive: Bool) {
        self.categoryModel = categoryModel
        self.isActive = isActive
    }

    // MARK: - Public Methods

    func configure(_ cell: FiltersCollectionsCell) {
        cell.configure(categoryModel: categoryModel, isActive: isActive)
    }
}
