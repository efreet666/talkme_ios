//
//  FilterPriceTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 02.03.2021.
//

import RxSwift
import RxCocoa
import UIKit

final class FilterPriceTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var buttonTap = applyButton.rx.tap
    let costFrom = PublishRelay<Int?>()
    let costTo = PublishRelay<Int?>()
    let isFree = PublishRelay<Bool>()
    var bag = DisposeBag()

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blackLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        return lbl
    }()

    private let applyButton: UIButton = {
        let bt = UIButton()
        bt.backgroundColor = TalkmeColors.greenLabels
        bt.titleLabel?.font = .montserratExtraBold(ofSize: UIScreen.isSE ? 15 : 17)
        bt.titleLabel?.textColor = TalkmeColors.white
        bt.setTitle("filter_lesson_apply_filter".localized, for: .normal)
        bt.layer.cornerRadius = UIScreen.isSE ? 21 : 27.5
        return bt
    }()

    private let isFreeButton: ButtonWithRoundedIndicator = {
        let button = ButtonWithRoundedIndicator(frame: .zero, text: "talkme_free_price".localized)
        button.becomeActive(true)
        return button
    }()

    private let costFromTextField = FromAndToCoinView(title: "from_coin".localized, coin: "0")
    private let costToTextField = FromAndToCoinView(title: "before_coin".localized, coin: "")
    private var toCoin = ""
    private var fromCoin = ""

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        costToTextField.textField.delegate = self
        costFromTextField.textField.delegate = self
        setupLayout()
        setupCellStyle()
        costTo.accept(Int(toCoin))
        costFrom.accept(Int(fromCoin))
        isFree.accept(false)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String) {
        titleLabel.text = title
        bindUI()
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindUI() {
        isFreeButton.buttonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.costFromTextField.textField.text = "0"
                self.costToTextField.textField.text = ""
                self.costFrom.accept(0)
                self.costTo.accept(0)
                self.isFreeButton.becomeActive(true)
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        contentView.addSubviews(titleLabel,
                                 costFromTextField,
                                 costToTextField,
                                 isFreeButton,
                                 applyButton)

        titleLabel.snp.makeConstraints { make in
            make.centerY.equalTo(isFreeButton)
            make.leading.trailing.equalToSuperview().inset(15)
        }

        costFromTextField.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.top.equalTo(costToTextField.snp.top)
            make.height.equalTo(UIScreen.isSE ? 35 : 47)

        }

        costToTextField.snp.makeConstraints { make in
            make.top.equalTo(isFreeButton.snp.bottom).offset(9)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(UIScreen.isSE ? 35 : 47)
            make.leading.equalTo(costFromTextField.snp.trailing).offset(UIScreen.isSE ? 10 : 13)
            make.height.equalTo(UIScreen.isSE ? 35 : 45)
            make.width.equalTo(costFromTextField.snp.width)
        }

        isFreeButton.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.isSE ? 121 : 153)
            make.height.equalTo(UIScreen.isSE ? 30 : 39)
            make.top.equalToSuperview().offset(UIScreen.isSE ? 22 : 21)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }

        applyButton.snp.makeConstraints { make in
            make.top.equalTo(costFromTextField.snp.bottom).offset(UIScreen.isSE ? 22 : 32)
            make.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 42 : 55)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension FilterPriceTableCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = nil
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if costFromTextField.textField.text == "" {
            costFromTextField.textField.text = "0"
            isFreeButton.becomeActive(true)
            isFree.accept(true)
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if textField == costToTextField.textField {
            toCoin += string
            isFreeButton.becomeActive(false)
            isFree.accept(false)
            costTo.accept(Int(toCoin))
        } else if textField == costFromTextField.textField {
            fromCoin += string
            costFrom.accept(Int(fromCoin))
            isFreeButton.becomeActive(false)
            isFree.accept(false)
        }

        let newText = (text as NSString).replacingCharacters(in: range, with: string) as String
        let replacing = newText.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
        if let num = Int(replacing), num <= 100000 {
            if textField == costToTextField.textField {
                return backspaceString(textField, shouldChangeCharactersIn: range, replacementString: string)
            } else {
                return backspaceString(textField, shouldChangeCharactersIn: range, replacementString: string)
            }
        } else {
            return false
        }
    }

    private func backspaceString(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .decimal
        formatter.decimalSeparator = "."
        formatter.groupingSeparator = " "
        let textString = textField.text ?? ""
        guard let range = Range(range, in: textString) else { return false }
        let updatedString = textString.replacingCharacters(in: range, with: string)
        let completeString = updatedString.replacingOccurrences(of: formatter.groupingSeparator, with: "")
        guard let value = Double(completeString) else { return false }
        let formattedNumber = formatter.string(from: NSNumber(value: value)) ?? ""
        textField.text = formattedNumber
        return string == formatter.decimalSeparator
    }
}
