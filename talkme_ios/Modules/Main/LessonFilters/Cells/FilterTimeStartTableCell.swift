//
//  FilterTimeStartTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 25.02.2021.
//

import RxSwift
import RxCocoa

final class FilterTimeStartTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    let firstTime = PublishRelay<String?>()
    let secondTime = PublishRelay<String?>()

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blackLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        return lbl
    }()

    private let leftLabel: FilterSliderLabel = {
        let label = FilterSliderLabel()
        label.text = "00:00"
        label.textAlignment = .left
        return label
    }()

    private let rightLabel: FilterSliderLabel = {
        let label = FilterSliderLabel()
        label.text = "23:59"
        label.textAlignment = .right
        return label
    }()

    private lazy var slider: MultiSlider = {
        let slider = MultiSlider()
        slider.orientation = .horizontal
        slider.isTime = true
        slider.minimumValue = 0
        slider.maximumValue = 1439
        slider.snapStepSize = 60
        slider.showsThumbImageShadow = true
        slider.valueLabelFormatter.numberStyle = .none
        slider.outerTrackColor = TalkmeColors.graySlider
        slider.value = [300, 1260]
        slider.valueLabelPosition = .top
        slider.tintColor = TalkmeColors.blueLabels
        slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        slider.trackWidth = UIScreen.isSE ? 6 : 8
        slider.keepsDistanceBetweenThumbs = false
        slider.valueLabelColor = TalkmeColors.blueLabels
        slider.valueLabelFont = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        return slider
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String) {
        titleLabel.text = title
    }

    // MARK: - Private Methods

    @objc func sliderChanged(_ slider: MultiSlider) {
        firstTime.accept(slider.valueLabels[0].text)
        secondTime.accept(slider.valueLabels[1].text)
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, slider, leftLabel, rightLabel, separatorView])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(20)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }

        slider.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 30 : 39)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
        }

        leftLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.trailing.equalTo(rightLabel.snp.leading)
            make.top.equalTo(slider.snp.bottom).offset(UIScreen.isSE ? -10 : -8)
            make.height.equalTo(UIScreen.isSE ? 15 : 19)
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -28 : -32)
        }

        rightLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.leading.equalTo(leftLabel.snp.trailing)
            make.top.equalTo(slider.snp.bottom).offset(UIScreen.isSE ? -10 : -8)
            make.height.equalTo(UIScreen.isSE ? 15 : 19)
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -28 : -32)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
