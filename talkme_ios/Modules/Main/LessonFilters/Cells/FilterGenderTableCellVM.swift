//
//  FilterGenderTableCellVM.swift
//  talkme_ios
//
//  Created by 1111 on 02.03.2021.
//

import RxSwift
import RxCocoa

final class FilterGenderTableCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let activeGender = PublishRelay<String?>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String

    // MARK: - Initializers

    init(title: String) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: FilterGenderTableCell) {
        cell.configure(title: title)

        cell
            .activeGender
            .bind(to: activeGender)
            .disposed(by: cell.bag)
    }
}
