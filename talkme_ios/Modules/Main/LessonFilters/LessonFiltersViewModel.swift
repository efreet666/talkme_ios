//
//  LessonFiltersViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 22.02.2021.
//

import RxSwift
import RxCocoa

final class LessonFiltersViewModel {

    enum Flow {
        case showFilteredCategoriesVC(ExactCategoryRequest, [Int])
    }

    // MARK: - Public Properties

    private(set) lazy var dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    private(set) var bag = DisposeBag()
    var flow = PublishRelay<Flow>()
    var textFieldString = PublishRelay<String>()
    var isLive = false
    var textFieldValue: String = ""
    lazy var subcategories = subcategoriesFullArray.map { $0.name }

    // MARK: - Private Properties

    private let lessonService: LessonsServiceProtocol
    private let mainService: MainServiceProtocol
    private let id: String
    private var isFree = true
    private var selectedMainCategories = [0]
    private var subcategoriesFullArray = [Category]()
    private lazy var filteredRequest = ExactCategoryRequest(
        categoryId: id,
        costFrom: nil,
        costTo: nil,
        dateStart: nil,
        dateEnd: nil,
        free: nil,
        gender: nil,
        subCategory: [],
        timeEnd: nil,
        timeStart: nil
    )

    // MARK: - Initializers

    init(lessonService: LessonsServiceProtocol, id: String, mainService: MainServiceProtocol, isLive: Bool) {
        self.lessonService = lessonService
        self.id = id
        self.isLive = isLive
        self.mainService = mainService
    }

    // MARK: - Public Methods

    func getCategories() {
        mainService
            .categories()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.setupFilters(categories: response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func setupFilters(categories: [Category]) {
        lessonService
            .exactCategory(request: filteredRequest)
            .subscribe { [weak self] event in
                let idIsNumber: Bool = Int(self?.id ?? "") != nil
                switch event {
                case .success(let response):
                    idIsNumber
                        ? self?.setupCellsWithoutCategories(model: response)
                        : self?.setupCellsWithCategories(model: response, categories: categories)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func setupCellsWithoutCategories(model: ExactCategoryResponse) {
        let filterSubcategoryVM = FilterSubcategoriesCellVM(title: "filter_lesson_choose_subcategories".localized,
                                                            textFieldTextString: model.categoryName,
                                                            model: model.subCategories)
        let dataItems: [AnyTableViewCellModelProtocol] = [filterSubcategoryVM]
        setupItems(cellsVM: dataItems)

        filterSubcategoryVM
            .onSelectedItems
            .bind { [weak self] idArray in
                self?.filteredRequest.subCategory = idArray
            }
            .disposed(by: filterSubcategoryVM.bag)

        filterSubcategoryVM
            .textRelay
            .bind { [weak self] text in
                self?.textFieldValue = text ?? ""
            }
            .disposed(by: filterSubcategoryVM.bag)
    }

    private func setupCellsWithCategories(model: ExactCategoryResponse, categories: [Category]) {
        let categoriesId = model.data.map { $0.category?.parent }
        var parentCategories = [Category]()
        Array(Set(categoriesId)).forEach { id in
            categories.forEach { category in
                if category.id == id {
                    parentCategories.append(category)
                }
            }
        }
        var subcategories = [Category]()
        model.data.forEach { lesson in
            guard let subcategory = lesson.category else { return }
            subcategories.append(subcategory)
        }
        subcategories = Array(Set(subcategories))
        subcategoriesFullArray = subcategories
        let filterCategoryVM = FilterCategoriesViewModel(title: "filter_lesson_choose_categories".localized,
                                                         model: parentCategories)
        var filterSubcategoryVM = FilterSubcategoriesCellVM(title: "filter_lesson_choose_subcategories".localized,
                                                            textFieldTextString: textFieldValue,
                                                            model: subcategories)
        let dataItems: [AnyTableViewCellModelProtocol] = [filterCategoryVM, filterSubcategoryVM]
        setupItems(cellsVM: dataItems)

        filterCategoryVM
            .onSelectedItems
            .bind { idArray in
                var filteredCategories = [Category]()
                idArray.forEach { id in
                    subcategories.forEach { category in
                        if category.parent == id {
                            filteredCategories.append(category)
                        }
                    }
                }
                self.selectedMainCategories = idArray.isEmpty ? [0] : idArray
                filterSubcategoryVM = idArray.isEmpty
                    ? FilterSubcategoriesCellVM(title: "filter_lesson_choose_subcategories".localized,
                                            textFieldTextString: self.textFieldValue, model: self.subcategoriesFullArray)
                    : FilterSubcategoriesCellVM(title: "filter_lesson_choose_subcategories".localized,
                                            textFieldTextString: self.textFieldValue, model: filteredCategories)
                let dataItems: [AnyTableViewCellModelProtocol] = [filterCategoryVM, filterSubcategoryVM]
                self.setupItems(cellsVM: dataItems)
            }
            .disposed(by: filterCategoryVM.bag)

        filterCategoriesAndSubcategories(filterSubcategoryVM: filterSubcategoryVM)

        filterCategoryVM
            .onSelectedItems
            .bind { [weak self] _ in // idArray in
                self?.filterCategoriesAndSubcategories(filterSubcategoryVM: filterSubcategoryVM)
            }
            .disposed(by: filterCategoryVM.bag)
    }

    private func filterCategoriesAndSubcategories(filterSubcategoryVM: FilterSubcategoriesCellVM) {
        filterSubcategoryVM
            .onSelectedItems
            .bind { [weak self] idArray in
                self?.filteredRequest.subCategory = idArray
                self?.subcategoriesFullArray.map { self?.textFieldValue = $0.name ?? "" }
            }
            .disposed(by: filterSubcategoryVM.bag)
    }

    private func setupItems(cellsVM: [AnyTableViewCellModelProtocol]) {
        let filterDateCellVM = FilterDateCellVM(title: "create_lesson_stream_date".localized)
        let filterTimeStartCellVM = FilterTimeStartCellVM(title: "filter_lesson_choose_time_start".localized)
        let filterGenderCellVM = FilterGenderTableCellVM(title: "filter_lesson_choose_gender".localized)
        let filterPriceCellVM = FilterPriceTableCellVM(title: "filter_lesson_choose_price".localized)
        let commonItems: [AnyTableViewCellModelProtocol] = isLive
            ? [filterGenderCellVM, filterPriceCellVM]
            : [filterDateCellVM, filterTimeStartCellVM, filterGenderCellVM, filterPriceCellVM]
        let dataItems = cellsVM + commonItems
        self.dataItems.accept(dataItems)

        filterDateCellVM
            .firstDate
            .map { date -> Int in
                let interval = date.timeIntervalSince1970
                let roundedInterval = Int(interval)
                return roundedInterval
            }
            .bind { [weak self] interval in
                self?.filteredRequest.dateStart = interval
            }
            .disposed(by: filterDateCellVM.bag)

        filterDateCellVM
            .secondDate
            .map { date -> Int in
                let interval = date.timeIntervalSince1970
                let roundedInterval = Int(interval)
                return roundedInterval
            }
            .bind { [weak self] interval in
                self?.filteredRequest.dateEnd = interval
            }
            .disposed(by: filterDateCellVM.bag)

        filterTimeStartCellVM
            .firstTime
            .bind { [weak self] time in
                self?.filteredRequest.timeStart = time
            }
            .disposed(by: filterTimeStartCellVM.bag)

        filterTimeStartCellVM
            .secondTime
            .bind { [weak self] time in
                self?.filteredRequest.timeEnd = time
            }
            .disposed(by: filterTimeStartCellVM.bag)

        filterGenderCellVM
            .activeGender
            .bind { [weak self] gender in
                self?.filteredRequest.gender = gender
            }
            .disposed(by: filterGenderCellVM.bag)

        filterPriceCellVM
            .costFrom
            .bind { [weak self] cost in
                self?.filteredRequest.costFrom = cost
                self?.isFree = true
            }
            .disposed(by: filterPriceCellVM.bag)

        filterPriceCellVM
            .costTo
            .bind { [weak self] cost in
                self?.filteredRequest.costTo = cost
                self?.isFree = true
            }
            .disposed(by: filterPriceCellVM.bag)

        filterPriceCellVM
            .isFree
            .bind { [weak self] isFree in
                guard let self = self else { return }
                self.isFree = isFree
            }
            .disposed(by: filterPriceCellVM.bag)

        filterPriceCellVM
            .buttonTapped.map {
                self.filteredRequest.free = self.isFree
                return LessonFiltersViewModel.Flow.showFilteredCategoriesVC(
                    self.filteredRequest,
                    self.selectedMainCategories
                ) }
            .bind(to: flow)
            .disposed(by: filterPriceCellVM.bag)
    }
}
