//
//  CrossView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 27/7/2022.
//

import UIKit

class CrossImageView: UIImageView {

    // MARK: - private properties

    let blackCircleImageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "blackCircle"))
        view.alpha = 0.5
        return view
    }()

    let whiteCrossImageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "whiteCross"))
        return view
    }()

    // MARK: - life cicle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            return bounds.insetBy(dx: -10, dy: -10).contains(point)
        }

    // MARK: - private funcs

    private func setupLayout() {
        addSubviews(blackCircleImageView,
                    whiteCrossImageView)

        blackCircleImageView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }

        whiteCrossImageView.snp.makeConstraints { make in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6))
        }
    }
}
