//
//  RoundedImageView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 03.03.2021.
//

import UIKit

class RoundedImageView: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = min(bounds.width, bounds.height) / 2
        clipsToBounds = true
    }
}
