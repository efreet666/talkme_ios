//
//  CategoryModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 01.03.2021.
//

struct CategoryModel {
    let id: Int
    let name: String
}
