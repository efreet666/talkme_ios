//
//  LessonCollectionCellViewModelProtocol.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 18.03.2021.
//

import Foundation

protocol LessonCollectionCellViewModelProtocol {
    var lesson: LiveStream { get }
}
