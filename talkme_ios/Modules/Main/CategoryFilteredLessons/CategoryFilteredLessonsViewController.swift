//
//  CategoryFilteredLessonsViewController.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 02.03.2021.
//

import RxSwift
import RxCocoa
import RxDataSources
import CoreGraphics

fileprivate extension Consts {
    // dont push numbers more then 100 nd less then 0 to precentOfPageSizeWichIsALoadBorder
    static var precentOfPageSizeWichIsALoadBorder: Int = 70
}

final class CategoryFilteredLessonsViewController: UIViewController {

    enum Constants {
        static let screenWidth = UIScreen.main.bounds.width
        static let inset: CGFloat = UIScreen.isSE ? 10 : 12
        static let headerToCellsSpacing: CGFloat = UIScreen.isSE ? 12 : 14
        static let headerAspectRatio: CGFloat = 2.64
        static let smallCellAspectRatio: CGFloat = 0.63
        static let bigCellAspectRatio: CGFloat = 1.7
        static let bigCellSpacing: CGFloat = UIScreen.isSE ? 7 : 12
        static let smallCellSpacing: CGFloat = UIScreen.isSE ? 14 : 18
        static let itemSizeForTableLayout: CGSize = CGSize(width: screenWidth - inset * 2,
                                                           height: (screenWidth - inset * 2) / bigCellAspectRatio)
        static let itemSizeForCollectionLayout: CGSize = CGSize(width: (screenWidth - inset * 2 - smallCellSpacing) / 2,
                                                                height: (screenWidth - inset * 2 - smallCellSpacing) / 2 / smallCellAspectRatio)
    }

    // MARK: - Private Properties

    private lazy var dataSource = RxCollectionViewSectionedReloadDataSource<SectionModel<String, AnyCollectionViewCellModelProtocol>>(
        configureCell: { (_, collectionView, indexPath, viewModel) -> UICollectionViewCell in
            let cell = collectionView.dequeueReusableCell(withModel: viewModel, for: indexPath)
            viewModel.configureAny(cell)
            return cell
        },
        configureSupplementaryView: { [weak self] (_, collectionView, _, indexPath) -> UICollectionReusableView in
            let model = self?.viewModel.headerModel
            let header = collectionView.dequeueReusableHeader(for: indexPath, headerType: LessonCollectionHeader.self)
            model?.configure(header)
            return header
        })

    private lazy var collectionView: UICollectionView = {
        let layout = tableLayout
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.registerCells(
            withModels: LessonBigCollectionCellViewModel.self,
            LessonSmallCollectionCellViewModel.self
        )
        collectionView.register(headerType: LessonCollectionHeader.self)
        collectionView.backgroundColor = .clear
        return collectionView
    }()

    private lazy var collectionLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        var aspectRatio: CGFloat = Constants.headerAspectRatio
        layout.sectionInset.top = Constants.headerToCellsSpacing
        layout.headerReferenceSize = CGSize(width: Constants.screenWidth, height: Constants.screenWidth / aspectRatio)
        layout.sectionInset.left = Constants.inset
        layout.sectionInset.right = Constants.inset
        layout.itemSize = Constants.itemSizeForCollectionLayout
        layout.minimumLineSpacing = Constants.smallCellSpacing
        layout.minimumInteritemSpacing = Constants.smallCellSpacing
        return layout
    }()

    private lazy var tableLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        var aspectRatio: CGFloat = Constants.headerAspectRatio
        layout.sectionInset.top = Constants.headerToCellsSpacing
        layout.headerReferenceSize = CGSize(width: Constants.screenWidth, height: Constants.screenWidth / aspectRatio)
        layout.itemSize = Constants.itemSizeForTableLayout
        layout.minimumLineSpacing = Constants.bigCellSpacing
        return layout
    }()

    private let viewModel: CategoryFilteredLessonsViewModel

    private let bag = DisposeBag()

    private var botttomPagingBound: CGFloat {
        let cellHeight = viewModel.isCollection.value ? Constants.itemSizeForCollectionLayout.height : Constants.itemSizeForTableLayout.height
        let pageLengthInPoints = Float(cellHeight) * Float(viewModel.numberOfStreamsInPage)
        return CGFloat(pageLengthInPoints *
                       Float(viewModel.numberOfLastPage - 1) +
                       pageLengthInPoints *
                       (Float(Consts.precentOfPageSizeWichIsALoadBorder) / 100.0) )
    }

    // MARK: - Initializers

    init(_ viewModel: CategoryFilteredLessonsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        bindVM()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if viewModel.numberOfLastPage == 1 {
            viewModel.getLessons()
        } else {
            viewModel.reloadPageThatContainsSelectedStream()
        }
        setUpNavigationBar()
        hideCustomTabBarLiveButton(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
    }

    // MARK: - Private Methods

    private func setUpView() {
        view.backgroundColor = TalkmeColors.mainAccountBackground
        view.addSubview(collectionView)

        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func bindVM() {
        viewModel
            .items
            .map { (itemModels) -> [SectionModel<String, AnyCollectionViewCellModelProtocol>] in
                return [SectionModel(model: "", items: itemModels)]
            }
            .do { [weak self] _ in
                self?.changeCollectionViewOffsetAfterLasPageWasReloaded()
            }
            .observeOn(MainScheduler.instance)
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: bag)

        viewModel
            .isCollection
            .bind { [weak self] isCollection in
                guard let self = self else { return }
                self.viewModel.items.accept([])
                self.collectionView
                    .setCollectionViewLayout(
                        isCollection ? self.collectionLayout : self.tableLayout,
                        animated: false,
                        completion: { [weak self] _ in
                            self?.viewModel.getLessons()
                        }
                    )
            }.disposed(by: viewModel.bag)

        collectionView
            .rx
            .modelSelected(AnyCollectionViewCellModelProtocol.self)
            .compactMap { $0 as? LessonCollectionCellViewModelProtocol }
            .bind(to: viewModel.modelSelected)
            .disposed(by: bag)

        collectionView.rx
            .contentOffset
            .filter { [weak self] contentOffset -> Bool in
                guard let self = self else { return false}
                return contentOffset.y > self.botttomPagingBound
            }
            .map { [weak self] _ -> CGFloat in
                guard let self = self else { return CGFloat(0)}
                return self.botttomPagingBound
            }
            .distinctUntilChanged()
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.loadNextPage()
            }
            .disposed(by: bag)
    }

    private func setUpNavigationBar() {
        customNavigationController?.style = .gradientBlue
    }

    private func changeCollectionViewOffsetAfterLasPageWasReloaded() {
        let cellHeight = viewModel.isCollection.value ? Constants.itemSizeForCollectionLayout.height : Constants.itemSizeForTableLayout.height
        let offsetOfremovedStreams = cellHeight * CGFloat( viewModel.countOfStreamsRemovedAfterPageReloadBeforeSelectedStream )

        collectionView.contentOffset.y = collectionView.contentOffset.y - offsetOfremovedStreams
    }
}
