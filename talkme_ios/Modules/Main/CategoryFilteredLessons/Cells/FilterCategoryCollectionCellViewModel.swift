//
//  FilterCategoryCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 01.03.2021.
//

final class FilterCategoryCollectionCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public properties

    let categoryName: String
    let categoryId: Int
    let isSelected: Bool

    // MARK: - Initializers

    init(_ id: Int, _ name: String, isSelected: Bool) {
        categoryName = name
        categoryId = id
        self.isSelected = isSelected
    }

    // MARK: - Public Methods

    func configure(_ cell: FilterCategoryCollectionCell) {
        cell.configure(categoryName, isSelected: isSelected)
    }
}
