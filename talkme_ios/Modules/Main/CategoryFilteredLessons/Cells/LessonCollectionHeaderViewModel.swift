//
//  LessonCollectionHeaderViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 01.03.2021.
//

import RxSwift
import RxCocoa

final class LessonCollectionHeaderViewModel {

    enum Flow {
        case collectionButtonTapped
        case tableButtonTapped
        case filterButtonTapped
    }

    // MARK: - Private Properties

    private let items: [CategoryModel]
    private var collectionOffset: CGFloat = UIScreen.isSE ? -10 : -12

    // MARK: - Public properties

    let category: LessonCategoryType?
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    let selectedCategoryId = PublishRelay<Int>()
    var selectedCategories = [0]

    // MARK: - Initializers

    init(_ categories: [CategoryModel], categoryId: Int) {
        items = categories
        self.category = LessonCategoryType(rawValue: categoryId)
    }

    init(_ categories: [CategoryModel], category: LessonCategoryType) {
        items = categories
        self.category = category
    }

    // MARK: - Public Methods

    func configure(_ header: LessonCollectionHeader) {
        guard let category = category else {
            return
        }
        var cellModels = items.map { FilterCategoryCollectionCellViewModel(
            $0.id,
            $0.name,
            isSelected: selectedCategories.contains($0.id)
        )
        }
        cellModels.insert(
            FilterCategoryCollectionCellViewModel(
                0,
                "main_page_category_all".localized,
                isSelected: selectedCategories.contains(0)
            ),
            at: 0
        )
        header.configure(
            cellModels,
            category: category,
            collectionOffset: collectionOffset
        )
        bindHeader(header)
    }

    private func bindHeader(_ header: LessonCollectionHeader) {
        header.modelSelected
            .do { [weak self] element in
                self?.collectionOffset = element.1
            }
            .map { $0.0.categoryId }
            .bind(to: selectedCategoryId)
            .disposed(by: header.bag)

        header.collectionButtonTapped
            .map { .collectionButtonTapped }
            .bind(to: flow)
            .disposed(by: header.bag)

        header.tableButtonTapped
            .map { .tableButtonTapped }
            .bind(to: flow)
            .disposed(by: header.bag)

        header.filterButtonTapped
            .map { .filterButtonTapped}
            .bind(to: flow)
            .disposed(by: header.bag)
    }
}
