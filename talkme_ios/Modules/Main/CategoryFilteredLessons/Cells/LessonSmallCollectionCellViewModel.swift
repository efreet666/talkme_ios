//
//  LessonSmallCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 26.02.2021.
//

import RxCocoa
import RxSwift

final class LessonSmallCollectionCellViewModel: CollectionViewCellModelProtocol, LessonCollectionCellViewModelProtocol {

    // MARK: - Public Properties

    let lesson: LiveStream
    let onAvatarTap = PublishRelay<Void>()
    let deleteButtonTapped = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private properties

    private var deleteButtonIsShown = false

    // MARK: - Initializers

    init(_ model: LiveStream, andShowDeleteButton show: Bool) {
        lesson = model
        self.deleteButtonIsShown = show
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonSmallCollectionCell) {
        cell.configure(lesson, andShowDeleteButton: deleteButtonIsShown)

        cell
            .avatarTapped
            .map { _ in () }
            .bind(to: onAvatarTap)
            .disposed(by: cell.bag)

        cell
            .deleteButtonTapped
            .map { _ in () }
            .bind(to: deleteButtonTapped)
            .disposed(by: cell.bag)
    }
}
