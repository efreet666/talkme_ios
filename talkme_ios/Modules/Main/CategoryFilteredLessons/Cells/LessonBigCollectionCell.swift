//
//  LessonBigCollectionCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 24.02.2021.
//

import RxSwift
import Kingfisher
import UIKit

final class LessonBigCollectionCell: UICollectionViewCell {

    // MARK: - Public properties

    private(set) lazy var avatarTapped = avatarButton.rx.tap
    private(set) lazy var deleteButtonTapped = deleteStreamButton.rx.tapGesture().when(.recognized)
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let backgroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 12
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private let playImagView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "savedStreamMark"))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private let deleteStreamButton = CrossImageView(frame: .zero)

    private let avatarImage: RoundedImageView = {
        let imageView = RoundedImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var avatarButton: UIButton = {
        let button = UIButton()
        button.setTitle(nil, for: .normal)
        return button
    }()

    private let lessonTitle: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: 15)
        label.textColor = .white
        label.numberOfLines = 3
        return label
    }()

    private let priceLabel = StreamPriceView()
    private let categoryImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let containerView: RoundedView = {
        let view = RoundedView()
        view.backgroundColor = TalkmeColors.lessonTimeAlphaGray50
        view.clipsToBounds = true
        return view
    }()

    private let liveLabel: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "liveCat")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private let clockIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "clockNearest")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private let calendarIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "whiteCalendar")
        return imageView
    }()

    private let timeLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.redLessonLabel
        return label
    }()

    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = .white
        return label
    }()

    private let gradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [
            TalkmeColors.nearestGradientMain.cgColor,
            TalkmeColors.secondGradientColor.cgColor
        ]
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer.transform = CATransform3DMakeAffineTransform(
            CGAffineTransform(
                a: 0.19,
                b: -0.61,
                c: 0.58,
                d: 0.02,
                tx: -0.01,
                ty: 0.72
            )
        )
        return layer
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ model: LiveStream, andShowDeleteButton showDeleteButton: Bool) {
        deleteStreamButton.isHidden = !showDeleteButton
        liveLabel.isHidden = !(model.isStarted && !model.isFinish)
        playImagView.isHidden = !(model.savedStream ?? false) || showDeleteButton
        let lessonCostIsZero = (model.cost ?? 0) == 0
        let attributedString = NSMutableAttributedString(string: model.name)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.15
        paragraphStyle.alignment = .left
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length))
        lessonTitle.attributedText = attributedString
        timeLabel.text = Formatters.hoursMinutesWithoutLetters(date: model.lessonTime)
//        timeLabel.text = Formatters.setupMinutesAndSecondsWithout0(model.lessonTime) ?? "00:00"
        dateLabel.text = Formatters.convertDate3LettersMonth(model.date)
        priceLabel.configure(viewType: lessonCostIsZero ? .bigFreeStream : .bigMoneyStream(price: String(model.cost ?? 0)))
        if let id = model.category?.parent {
            categoryImage.image = LessonCategoryType(rawValue: id)?.image
        }
        if let stringUrl = model.owner.avatarUrl, let url = URL(string: stringUrl) {
            avatarImage.kf.setImage(with: ImageResource(downloadURL: url, cacheKey: url.absoluteString))
        } else {
            avatarImage.image = UIImage(named: "teacherPlaceholder")
        }
        if let stringUrl = model.tileImage, let url = URL(string: stringUrl) {
            backgroundImage.kf.setImage(with: ImageResource(downloadURL: url, cacheKey: url.absoluteString))
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let size = contentView.bounds.size
        gradient.frame = contentView.bounds.insetBy(
            dx: -0.2 * size.width,
            dy: -0.2 * size.height
        )
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImage.image = nil
        backgroundImage.image = nil
    }

    // MARK: - Private Methods

    private func setUpView() {
        backgroundImage.layer.addSublayer(gradient)

        contentView.addSubviews(
            backgroundImage,
            avatarImage,
            lessonTitle,
            priceLabel,
            clockIcon,
            calendarIcon,
            liveLabel,
            timeLabel,
            dateLabel,
            containerView,
            avatarButton,
            playImagView,
            deleteStreamButton
        )
        containerView.addSubview(categoryImage)

        setUpConstraints()
    }

    private func setUpConstraints() {
        backgroundImage.snp.makeConstraints { make in
            make.edges.equalTo(contentView)
        }

        playImagView.snp.makeConstraints { make in
            make.width.equalTo(playImagView.snp.height).multipliedBy(1.3)
            make.height.equalTo(avatarImage.snp.height).dividedBy(2)
            make.trailing.equalTo(contentView.snp.trailing).offset(-20)
            make.top.equalTo(contentView.snp.bottom).dividedBy(UIScreen.isSE ? 14.13 : 14)
        }

        deleteStreamButton.snp.makeConstraints { make in
            make.width.equalTo(deleteStreamButton.snp.height)
            make.height.equalTo(avatarImage.snp.height).dividedBy(2)
            make.trailing.equalTo(contentView.snp.trailing).offset(-20)
            make.top.equalTo(contentView.snp.bottom).dividedBy(UIScreen.isSE ? 11.13 : 14)
        }

        avatarImage.snp.makeConstraints { make in
            make.width.equalToSuperview().dividedBy(UIScreen.isSE ? 9.1 : 7.5)
            make.height.equalTo(avatarImage.snp.width)
            make.leading.equalTo(contentView.snp.trailing).dividedBy(UIScreen.isSE ? 20 : 19.5)
            make.top.equalTo(contentView.snp.bottom).dividedBy(UIScreen.isSE ? 13.54 : 13.47)
        }

        avatarButton.snp.makeConstraints { make in
            make.size.equalTo(avatarImage.snp.width).multipliedBy(1.2)
            make.center.equalTo(avatarImage.snp.center)
        }

        priceLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarImage)
            make.bottom.equalTo(contentView).dividedBy(UIScreen.isSE ? 1.09 : 1.1)
            make.height.equalToSuperview().dividedBy(UIScreen.isSE ? 7.04 : 6.94)
            make.width.equalToSuperview().dividedBy(UIScreen.isSE ? 3.37 : 3.36)
        }

        clockIcon.snp.makeConstraints { make in
            make.centerY.equalTo(priceLabel)
            make.leading.equalTo(priceLabel.snp.trailing).offset(UIScreen.isSE ? 13 : 17)
            make.size.equalTo(contentView.snp.width).dividedBy(UIScreen.isSE ? 25 : 24.38)
        }

        timeLabel.snp.makeConstraints { make in
            make.leading.equalTo(clockIcon.snp.trailing).offset(UIScreen.isSE ? 4 : 5)
            make.centerY.equalTo(clockIcon)
            make.width.equalToSuperview().dividedBy(UIScreen.isSE ? 8.11 : 8.13)
        }

        calendarIcon.snp.makeConstraints { make in
            make.centerY.equalTo(timeLabel)
            make.leading.equalTo(timeLabel.snp.trailing).offset(UIScreen.isSE ? 6 : 8)
            make.size.equalTo(contentView.snp.width).dividedBy(30)
        }

        dateLabel.snp.makeConstraints { make in
            make.leading.equalTo(calendarIcon.snp.trailing).offset(UIScreen.isSE ? 6 : 8)
            make.centerY.equalTo(calendarIcon)
            make.width.equalToSuperview().dividedBy(UIScreen.isSE ? 5.66 : 5.65)
        }

        containerView.snp.makeConstraints { make in
            make.size.equalTo(contentView.snp.width).dividedBy(UIScreen.isSE ? 10.34 : 10.26)
            make.centerY.equalTo(dateLabel)
            make.trailing.equalToSuperview().offset(-20)
        }

        categoryImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.height.equalToSuperview().dividedBy(1.8125)
        }

        liveLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView.snp.bottom).dividedBy(UIScreen.isSE ? 9.78 : 9.16)
            make.trailing.equalTo(contentView).dividedBy(UIScreen.isSE ? 1.05 : 1.06)
            make.height.equalToSuperview().dividedBy(UIScreen.isSE ? 7.33 : 8.18)
            make.width.equalToSuperview().dividedBy(UIScreen.isSE ? 7.32 : 8.48)
        }

        lessonTitle.snp.makeConstraints { make in
            make.leading.equalTo(avatarImage)
            make.bottom.equalTo(priceLabel.snp.top).offset(UIScreen.isSE ? -13 : -17)
            make.trailing.equalToSuperview().dividedBy(1.23)
        }
    }
}
