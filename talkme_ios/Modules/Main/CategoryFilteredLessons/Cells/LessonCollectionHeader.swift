//
//  LessonCollectionHeader.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 01.03.2021.
//

import RxSwift
import RxCocoa
import SnapKit

final class LessonCollectionHeader: UICollectionReusableView {

    // MARK: - Public properties

    private(set) lazy var modelSelected = categoryCollection.rx
        .modelSelected(
            FilterCategoryCollectionCellViewModel.self)
        .map { ($0, self.categoryCollection.contentOffset.x) }
    private(set) lazy var itemIndexSelected = PublishRelay<(IndexPath, CGFloat)>()
    private(set) lazy var collectionButtonTapped = layoutButtons.collectionButtonTapped
    private(set) lazy var tableButtonTapped = layoutButtons.tableButtonTapped
    private(set) lazy var filterButtonTapped = filterButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let categoryIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let categoryLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let filterButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "filter"), for: .normal)
        return button
    }()

    private let layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = CGSize(width: 1, height: 1)
        layout.minimumInteritemSpacing = UIScreen.isSE ? 8 : 10
        return layout
    }()

    private lazy var categoryCollection: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.contentInset = UIScreen.isSE
            ? UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            : UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        collection.registerCells(withModels: FilterCategoryCollectionCellViewModel.self)
        return collection
    }()

    private let layoutButtons: CategoriesLayoutButtons = {
        let buttons = CategoriesLayoutButtons()
        buttons.setTableLayoutActive()
        return buttons
    }()

    private var categoryCollectionHeightConstraint: Constraint? = nil
    private var categoryCollectionTopConstraint: Constraint? = nil
    private var layoutButtonsTopConstraint: Constraint? = nil

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: .zero)
        isHidden = true
        setUpConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ items: [AnyCollectionViewCellModelProtocol], category: LessonCategoryType, collectionOffset: CGFloat) {
        Observable.just(items)
            .bind(to: categoryCollection.rx.items) { collectionView, row, model in
                let index = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: model, for: index)
                model.configureAny(cell)
                return cell
            }.disposed(by: bag)
        categoryIcon.image = category.image
        categoryLabel.text = category.title
        bindUI()
        DispatchQueue.main.async {
            self.categoryCollection.contentOffset.x = collectionOffset
            guard !items.isEmpty else { return }
            self.isHidden = false
        }
        if category == .saved {
            self.isHidden = false
            filterButton.isHidden = true
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func bindUI() {
        categoryCollection.rx
            .itemSelected
            .map { ($0, self.categoryCollection.contentOffset.x) }
            .bind(to: itemIndexSelected)
            .disposed(by: bag)
    }

    private func setUpConstraints() {
        addSubviews(categoryIcon, categoryLabel, filterButton, categoryCollection, layoutButtons)

        let screenWidth = UIScreen.main.bounds.width
        categoryIcon.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 24 : screenWidth / 12.9375)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : screenWidth / 34.5)
            make.top.equalTo(snp.bottom).dividedBy(UIScreen.isSE ? 7.1176 : 7.4762)
        }

        categoryLabel.snp.makeConstraints { make in
            make.centerY.equalTo(categoryIcon)
            make.leading.equalTo(categoryIcon.snp.trailing).offset(UIScreen.isSE ? 9 : 12)
        }

        filterButton.snp.makeConstraints { make in
            make.centerY.equalTo(categoryIcon)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 14 : screenWidth / 25.875)
            make.width.equalTo(UIScreen.isSE ? 16 : screenWidth / 19.7143)
            make.height.equalTo(filterButton.snp.width).dividedBy(1.3125)
        }

        categoryCollection.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            categoryCollectionTopConstraint = make.top.equalTo(categoryIcon.snp.bottom).offset(UIScreen.isSE ? 10 : 12).constraint
            categoryCollectionHeightConstraint = make.height.equalTo(UIScreen.isSE ? 30 : screenWidth / 10.6154).constraint
        }

        layoutButtons.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            layoutButtonsTopConstraint = make.top.equalTo(categoryCollection.snp.bottom).offset(UIScreen.isSE ? 12 : 16).constraint
            make.height.equalTo(UIScreen.isSE ? 28 : screenWidth / 11.1892)
        }
    }
}
