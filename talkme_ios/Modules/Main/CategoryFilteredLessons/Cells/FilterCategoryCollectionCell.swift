//
//  FilterCategoryCollectionCell.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 01.03.2021.
//

import UIKit

final class FilterCategoryCollectionCell: UICollectionViewCell {

    // MARK: - Private Properties

    private let title: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        label.textColor = TalkmeColors.blueLabels
        return label
    }()

    private var cellSelected: Bool = false {
        didSet {
            if cellSelected {
                contentView.backgroundColor = TalkmeColors.blueLabels
                title.textColor = .white
            } else {
                contentView.backgroundColor = .white
                title.textColor = TalkmeColors.blueLabels
            }
        }
    }

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setUpView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.cornerRadius = contentView.bounds.height / 2
    }

    func configure(_ title: String, isSelected: Bool) {
        self.title.text = title
        cellSelected = isSelected
    }

    // MARK: - Private Methods

    private func setUpView() {
        contentView.backgroundColor = .white
        contentView.addSubview(title)

        title.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 15 : 21)
            make.top.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 30 : UIScreen.main.bounds.width / 10.62)
        }
    }
}
