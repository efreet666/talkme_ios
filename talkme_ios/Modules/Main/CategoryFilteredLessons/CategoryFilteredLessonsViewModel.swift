//
//  CategoryFilteredLessonsViewModel.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 02.03.2021.
//

import RxSwift
import RxCocoa
import ObjectiveC

fileprivate extension Consts {
    static let numberOfStreamsInPage: Int = 50
}

final class CategoryFilteredLessonsViewModel {

    enum ScreenType {
        case categories(LessonCategoryType)
        case subcategories(LessonCategoryType)
    }

    enum Flow {
        case showLiveStream(id: Int, streams: [LiveStream], numberOfLiveStreamsInPage: Int)
        case showLessonInfo(id: Int)
        case showFilters(id: String, isLive: Bool)
        case showAllSavedStreams(lessonId: Int, categoriesIds: [Int], streams: [LiveStream], numberOfSavedStreamsInPage: Int)
        case showUsersSavedStreams(userId: Int, lessonId: Int, categoriesIds: [Int], streams: [LiveStream], numberOfSavedStreamsInPage: Int)
        case onProfileSelect(classNumber: String?)
        case deleteButtonTapped(lessonId: Int)
    }

    // MARK: - Public properties

    let items = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let modelSelected = PublishRelay<LessonCollectionCellViewModelProtocol>()
    let reloadStreams =  PublishRelay<Void>()
    private(set) var headerModel: LessonCollectionHeaderViewModel?
    let screenType: ScreenType
    let isCollection = BehaviorRelay<Bool>(value: false)
    let bag = DisposeBag()
    var isLive = false
    let flow = PublishRelay<Flow>()
    let numberOfStreamsInPage = Consts.numberOfStreamsInPage
    private(set) var numberOfStreamsInLastPage = 0
    private var lastSelectedStreamId = 0
    private(set) var countOfStreamsRemovedAfterPageReloadBeforeSelectedStream = 0
    var numberOfLastPage: Int {
        streamsGeneralDescriptions.isEmpty ? 1 : Int(ceil(Float(streamsGeneralDescriptions.count) / Float(Consts.numberOfStreamsInPage)))
    }

    // MARK: - Private Properties

    private let mainService: MainServiceProtocol
    private let lessonsService: LessonsServiceProtocol
    private var selectedCategoryId = BehaviorRelay<[Int]>(value: [0])
    private var colletionModels = [LessonSmallCollectionCellViewModel]()
    private var tableModels = [LessonBigCollectionCellViewModel]()
    private var mainCategoriesSelected = [Int]()
    private let streamsOwnerId: Int?
    private var streamsGeneralDescriptions: [LiveStream] = []
    private var filteredRequest: ExactCategoryRequest {
        didSet {
            switch screenType {
            case .categories:
                selectedCategoryId.accept(mainCategoriesSelected)
            case .subcategories:
                selectedCategoryId.accept(
                    filteredRequest.subCategory.isEmpty
                        ? [0]
                        : filteredRequest.subCategory
                )
            }
        }
    }
    private var allShownLessonsBelongsToUser: Bool {
        guard let streamsCreatorId = streamsOwnerId
        else { return false }
        return streamsCreatorId == UserDefaultsHelper.shared.userId
    }

    // MARK: - Initializers

    init(_ category: LessonCategoryType,
         mainService: MainServiceProtocol,
         lessonsService: LessonsServiceProtocol,
         streamsOwnerId: Int? = nil
    ) {
        switch category {
        case .nearest, .live, .popular, .saved:
            screenType = .categories(category)
        default:
            screenType = .subcategories(category)
        }
        if category == .live {
            self.isLive = true
        }
        self.mainService = mainService
        self.lessonsService = lessonsService
        filteredRequest = ExactCategoryRequest(
            categoryId: category.stringId,
            costFrom: nil,
            costTo: nil,
            dateStart: nil,
            dateEnd: nil,
            free: nil,
            gender: nil,
            subCategory: [],
            timeEnd: nil,
            timeStart: nil
        )
        self.streamsOwnerId = streamsOwnerId
        bindModelSelection()
    }

    // MARK: - Public Methods

    func getLessons() {
        switch screenType {
        case .categories(let category):
            getCategoryLessons(category)
        case .subcategories(let category):
            getSubCategoryLessons(category)
        }
    }

    func configure(_ request: ExactCategoryRequest, _ mainCategories: [Int]) {
        mainCategoriesSelected = mainCategories
        filteredRequest = request
    }

    func loadNextPage() {
        loadStreams(fromPage: numberOfLastPage + 1, withCountOf: Consts.numberOfStreamsInPage)
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInitiated))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let streamsFromNewPage):
                    if streamsFromNewPage.count != 0 {
                        self.numberOfStreamsInLastPage = streamsFromNewPage.count
                    }
                    self.streamsGeneralDescriptions += streamsFromNewPage
                    self.setItems(from: self.streamsGeneralDescriptions)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    func reloadPageThatContainsSelectedStream() {
        guard let selectedStreamIdsIndex = streamsGeneralDescriptions.firstIndex(where: { $0.id == lastSelectedStreamId })
        else { return }
        let numberOfPageContainigSelectedStream = getNumberOfPageForReload(from: selectedStreamIdsIndex,
                                                                           numberOfStreamsInPage: Consts.numberOfStreamsInPage)
//        let countOfStreamsToLoad = getNumberOfStreamsInPage(withNumber: numberOfPageContainigSelectedStream)

        loadStreams(fromPage: numberOfPageContainigSelectedStream, withCountOf: Consts.numberOfStreamsInPage)
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInitiated))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let newStreamsForRloadedPage):

                    var streamsFromReloadedPageBeforeReload: [LiveStream] = []

                    if numberOfPageContainigSelectedStream == self.numberOfLastPage {
                        streamsFromReloadedPageBeforeReload = Array(self.streamsGeneralDescriptions.suffix(self.numberOfStreamsInLastPage))
                    } else {
                        let firstIndex = (numberOfPageContainigSelectedStream - 1) * Consts.numberOfStreamsInPage
                        let lastIndex = numberOfPageContainigSelectedStream * Consts.numberOfStreamsInPage - 1
                        streamsFromReloadedPageBeforeReload = Array(self.streamsGeneralDescriptions[firstIndex...lastIndex])
                    }

                    let count = self.numberOfStreamsRemoved(from: streamsFromReloadedPageBeforeReload,
                                                           before: self.lastSelectedStreamId,
                                                           afterLoad: newStreamsForRloadedPage)
                    self.countOfStreamsRemovedAfterPageReloadBeforeSelectedStream = count

                    if numberOfPageContainigSelectedStream == self.numberOfLastPage {
                        let countOfStreamsBeforeReloadedPage = self.streamsGeneralDescriptions.count - self.numberOfStreamsInLastPage
                        let streamsDescriptionsWithoutLastPage = Array(self.streamsGeneralDescriptions.prefix(countOfStreamsBeforeReloadedPage))
                        self.streamsGeneralDescriptions = streamsDescriptionsWithoutLastPage + newStreamsForRloadedPage
                    } else {
                        let firstIndex = (numberOfPageContainigSelectedStream - 1) * Consts.numberOfStreamsInPage
                        let lastIndex = numberOfPageContainigSelectedStream * Consts.numberOfStreamsInPage - 1

                        let streamsDescriptionsBeforeReloadedPage = Array(self.streamsGeneralDescriptions.prefix(firstIndex))
                        let suffixCount = self.streamsGeneralDescriptions.count - lastIndex + 1
                        let streamsDescriptionsAfterReloadedPage = Array(self.streamsGeneralDescriptions.suffix(suffixCount))

                        self.streamsGeneralDescriptions = streamsDescriptionsBeforeReloadedPage + newStreamsForRloadedPage + streamsDescriptionsAfterReloadedPage
                    }
                    self.setItems(from: self.streamsGeneralDescriptions)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    // MARK: - Private Methods

    private func setCollectionLayout() {
        guard !isCollection.value else { return }
        isCollection.accept(true)
    }

    private func setTableLayout() {
        guard isCollection.value else { return }
        isCollection.accept(false)
    }

    private func bindHeaderVM(_ model: LessonCollectionHeaderViewModel) {
        model.flow
            .bind { [weak self, weak model] event in
                guard let self = self else { return }
                switch event {
                case .collectionButtonTapped:
                    self.setCollectionLayout()
                case .tableButtonTapped:
                    self.setTableLayout()
                case .filterButtonTapped:
                    guard let category = model?.category else { return }
                    self.flow.accept(.showFilters(id: category.stringId, isLive: self.isLive))
                }
            }.disposed(by: model.bag)

        model.selectedCategoryId
            .bind { [weak self] id in
                guard let self = self else { return }
                var ids = self.selectedCategoryId.value
                if !ids.contains(id), id != 0 {
                    ids.append(id)
                    ids.removeAll { $0 == 0 }
                } else if id == 0 {
                    ids = [0]
                } else {
                    ids.removeAll { $0 == id }
                    if ids.isEmpty {
                        ids = [0]
                    }
                }
                self.selectedCategoryId.accept(ids)
                switch self.screenType {
                case .subcategories:
                    self.filteredRequest.subCategory = id == 0 ? [] : ids.filter { $0 != 0 }
                default:
                    break
                }
                self.updateItems(with: self.filteredRequest)
            }.disposed(by: model.bag)

        selectedCategoryId
            .bind { [weak model] ids in
                model?.selectedCategories = ids
            }
            .disposed(by: bag)
    }

    private func filteredItems(_ items: [LiveStream]) -> [LiveStream] {
        var items = items
        switch self.screenType {
        case .categories:
            let ids = self.selectedCategoryId.value
            if !ids.contains(0) {
                items = items.filter {
                    guard let parentId = $0.category?.parent else { return false }
                    return ids.contains(parentId)
                }
            }
        default:
            break
        }
        return items
    }

    private func setItems(from liveStreams: [LiveStream]) {
        let filteredStreams = filteredItems(liveStreams)
        tableModels = lessonBigCells(from: filteredStreams)
        colletionModels = lessonSmallCells(from: filteredStreams)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.isCollection.value
            ? self.items.accept(self.colletionModels)
            : self.items.accept(self.tableModels)
        }
    }

    private func lessonSmallCells(from items: [LiveStream]) -> [LessonSmallCollectionCellViewModel] {
        return items.map { item in
            let model = LessonSmallCollectionCellViewModel(item,
                                                           andShowDeleteButton: allShownLessonsBelongsToUser)
            model
                .onAvatarTap
                .map { item.owner.numberClass }
                .map(Flow.onProfileSelect)
                .bind(to: flow)
                .disposed(by: model.bag)

            model
                .deleteButtonTapped
                .map { _ in Flow.deleteButtonTapped(lessonId: item.id)}
                .bind(to: flow)
                .disposed(by: model.bag)
            return model
        }
    }

    private func lessonBigCells(from items: [LiveStream]) -> [LessonBigCollectionCellViewModel] {
        return items.map { item in
            let model = LessonBigCollectionCellViewModel(item,
                                                         andShowDeleteButton: allShownLessonsBelongsToUser)
            model
                .onAvatarTap
                .map { item.owner.numberClass }
                .map(Flow.onProfileSelect)
                .bind(to: flow)
                .disposed(by: model.bag)

            model
                .deleteButtonTapped
                .map { _ in Flow.deleteButtonTapped(lessonId: item.id)}
                .bind(to: flow)
                .disposed(by: model.bag)
            return model
        }
    }

    private func getCategoryLessons(_ category: LessonCategoryType) {
        mainService
            .allCategories()
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInitiated))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    if self.headerModel == nil {
                        let categories = response.map { CategoryModel(id: $0.id, name: $0.name)}
                        let model = LessonCollectionHeaderViewModel(categories, categoryId: category.rawValue)
                        self.bindHeaderVM(model)
                        self.headerModel = model
                    }
                    self.updateItems(with: self.filteredRequest)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func getSubCategoryLessons(_ category: LessonCategoryType) {
        mainService
            .subCategory(parent: category.stringId)
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInitiated))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    if self.headerModel == nil {
                        let subcategories = response.map { CategoryModel(id: $0.id, name: $0.name ?? "")}
                        let model = LessonCollectionHeaderViewModel(subcategories, categoryId: category.rawValue)
                        self.bindHeaderVM(model)
                        self.headerModel = model
                    }
                    self.updateItems(with: self.filteredRequest)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func updateItems(with request: ExactCategoryRequest) {
        loadStreams(fromPage: 1, withCountOf: Consts.numberOfStreamsInPage)
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .userInitiated))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.streamsGeneralDescriptions = response
                    self.setItems(from: response)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func bindModelSelection() {
        modelSelected
            .do { [weak self] model in
                self?.lastSelectedStreamId = model.lesson.id
            }
            .bind { [weak self] model in
                guard let self = self else { return }
                switch self.screenType {
                case .categories(category: .saved):
                     if let ownerId = self.streamsOwnerId {
                        self.flow.accept(.showUsersSavedStreams(userId: ownerId,
                                                                lessonId: model.lesson.id,
                                                                categoriesIds: self.selectedCategoryId.value,
                                                                streams: self.streamsGeneralDescriptions,
                                                                numberOfSavedStreamsInPage: Consts.numberOfStreamsInPage))
                    } else {
                        self.flow.accept(.showAllSavedStreams(lessonId: model.lesson.id,
                                                              categoriesIds: self.selectedCategoryId.value,
                                                              streams: self.streamsGeneralDescriptions,
                                                              numberOfSavedStreamsInPage: Consts.numberOfStreamsInPage))
                    }
                case .categories(let category) where category != .saved:
                    fallthrough
                case .subcategories:
                    guard model.lesson.isStarted && !model.lesson.isFinish else {
                        self.flow.accept(.showLessonInfo(id: model.lesson.id))
                        return
                    }
                    if model.lesson.owner.numberClass == UserDefaultsHelper.shared.classNumber {
                        self.flow.accept(.showLiveStream(id: model.lesson.id,
                                                         streams: [model.lesson],
                                                         numberOfLiveStreamsInPage: Consts.numberOfStreamsInPage))
                        return
                    }
                    if model.lesson.date < Date().timeIntervalSince1970 {
                        self.showStreamOrLesson(lessonId: model.lesson.id)
                    } else {
                        self.flow.accept(.showLessonInfo(id: model.lesson.id))
                    }
                default:
                    break
                }
            }
            .disposed(by: bag)

        reloadStreams
            .bind { [weak self] _ in
                guard let self = self
                else { return }
                self.updateItems(with: self.filteredRequest)
            }
            .disposed(by: bag)
    }

    private func showStreamOrLesson(lessonId: Int) {
        lessonsService
            .lessonDetail(id: lessonId, saved: false)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.cost == 0 else {
                        response.isSubscribe
                        ? self?.flow.accept(.showLiveStream(id: response.id,
                                                            streams: self?.streamsGeneralDescriptions ?? [],
                                                            numberOfLiveStreamsInPage: Consts.numberOfStreamsInPage))
                            : self?.flow.accept(.showLessonInfo(id: response.id))
                        return
                    }
                    self?.flow.accept(.showLiveStream(id: response.id,
                                                      streams: self?.streamsGeneralDescriptions ?? [],
                                                      numberOfLiveStreamsInPage: Consts.numberOfStreamsInPage))
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func loadStreams(fromPage pageNumber: Int, withCountOf countOfStreamsToLoad: Int) -> Single<[LiveStream]> {
        var streamsObservable = Observable<[LiveStream]>.from([]).asSingle()
        switch screenType {
        case .categories(category: .saved):
                streamsObservable = lessonsService
                    .savedStreams(withOwnersId: streamsOwnerId, page: pageNumber, count: countOfStreamsToLoad)
                    .map { $0.data ?? [] }
        case .categories(let category) where category != .saved:
            fallthrough
        case .subcategories:
            var request = filteredRequest
            request.page = pageNumber
            request.pageSize = countOfStreamsToLoad
            streamsObservable = lessonsService.exactCategory(request: request).map { $0.data }
        default:
            break
        }
        return streamsObservable
    }

    private func numberOfStreamsRemoved(from oldReloadedPageStreams: [LiveStream],
                                before selectedStreamId: Int,
                                afterLoad newReloadedPageStreams: [LiveStream]) -> Int {
        let countBeforeReload = oldReloadedPageStreams.firstIndex(where: { $0.id == selectedStreamId }) ?? 0

        var countAfterReload = 0
        var indexOflastNonClosedStreamBeforeSelected = countBeforeReload
        var idOflastNonClosedStreamBeforeSelected = selectedStreamId

        repeat {
            countAfterReload = newReloadedPageStreams.firstIndex(where: { $0.id == idOflastNonClosedStreamBeforeSelected }) ?? 0
            indexOflastNonClosedStreamBeforeSelected -= 1
            if indexOflastNonClosedStreamBeforeSelected > 0 {
                idOflastNonClosedStreamBeforeSelected = oldReloadedPageStreams[indexOflastNonClosedStreamBeforeSelected].id
            }
        } while (newReloadedPageStreams.firstIndex(where: { $0.id == idOflastNonClosedStreamBeforeSelected }) == nil &&
                 indexOflastNonClosedStreamBeforeSelected > 0)

        let result = countBeforeReload - countAfterReload
        return result
    }

    private func getNumberOfPageForReload(from selectedStreamIdsIndex: Int, numberOfStreamsInPage: Int) -> Int {
        let a = Float(selectedStreamIdsIndex) / Float(numberOfStreamsInPage)
        let result = a == 0 ? 2 : Int(ceil(a))
        return result
    }

    private func getNumberOfStreamsInPage(withNumber pageNumber: Int) -> Int {
        var result = 0
        let pageCount = Int(ceil(Float(streamsGeneralDescriptions.count) / Float(Consts.numberOfStreamsInPage)))
        result = pageNumber == pageCount ? numberOfStreamsInLastPage : Consts.numberOfStreamsInPage
        return result
    }
}
