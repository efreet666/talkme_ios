//
//  MainViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 08.02.2021.
//

import RxSwift
import RxCocoa
import Moya

fileprivate extension Consts {
    static let numberOfLiveStreamsInPage = 50
    static let numberOfNearestdStreamsInPage = 50
    static let numberOfSavedStreamsInPage = 50
}

final class MainViewModel {

    enum Flow {
        case onLessonDetail(Int)
        case watchStream(lessonId: Int, numberOfStreamsInPage: Int, countOfLoadedPages: Int, myBroadcasts: MyBroadcastsResponse?)
        case watchSavedStream(lessonId: Int, numberOfStreamsInPage: Int, streamsList: [LiveStream])
        case onAllCategories
        case onFilteredCategoryVC(LessonCategoryType)
        case onProfileSelect(classNumber: String?)
        case onTokenExpiration
    }

    // MARK: - Inputs

    let flow = PublishRelay<Flow>()

    // MARK: - Outputs

    let profileTabImage = PublishRelay<String?>()
    let liveStreamsList = BehaviorRelay<[LiveStream]>(value: [])
    private(set) var dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let classNumberRelay = PublishRelay<String>()
    let lessonLoadError = PublishRelay<(String, String)>()

    // MARK: - Public Properties

    let bag = DisposeBag()
    var bannerIndexPath: IndexPath?
    var nearestIndexPath: IndexPath?
    var popularIndexPath: IndexPath?
    var categoryIndexPath: IndexPath?

    // MARK: - Private properties

    private let mainService: MainServiceProtocol
    private let accountService: AccountServiceProtocol
    private let lessonService: LessonsServiceProtocol
    private let paymentsService: PaymentsServiceProtocol
    private let bannerTableCellViewModel = MainPageBannerTableCellViewModel()
    private let lessonsNearestCellViewModel = MainPageNearestLessonsCellViewModel()
    private let lessonsPopularCellViewModel = MainPagePopularTableCellViewModel()
    private let lessonCategoriesCellViewModel = MainPageCategoriesTableCellViewModel()
    private lazy var lessonsLiveCellViewModel = MainPageLiveTableCellViewModel(lessonService: lessonService,
                                                                               numberOfStreamsInPage: Consts.numberOfLiveStreamsInPage)
    private lazy var lessonSavedCellViewModel = MainPageSavedLessonTableCellViewModel(lessonService: lessonService,
                                                                                      numberOfStreamsInPage: Consts.numberOfSavedStreamsInPage)
    private let iapManager = IAPManager.shared
    private var savedStreamsList: [LiveStream] = []
    private var myBroadcasts: MyBroadcastsResponse?
    private var timer: Timer?

    // MARK: - Initializers

    init(mainService: MainService,
         accountService: AccountService,
         lessonService: LessonsService,
         paymentsService: PaymentsServiceProtocol
    ) {
        self.mainService = mainService
        self.accountService = accountService
        self.lessonService = lessonService
        self.paymentsService = paymentsService
        getDifferenceTimeWithServer()
        bind()
        bindAppState()
        updateCollectinEveryMinute()
    }

    // MARK: - Public Methods

    func scrollToBannersIndexPath() {
        bannerTableCellViewModel.scrollToIndexPath.accept(bannerIndexPath)
        bannerIndexPath = nil
    }

    func scrollToNearestIndexPath() {
        lessonsNearestCellViewModel.scrollToIndexPath.accept(nearestIndexPath)
        nearestIndexPath = nil
    }

    func scrollToPopularIndexPath() {
        lessonsPopularCellViewModel.scrollToIndexPath.accept(popularIndexPath)
        popularIndexPath = nil
    }

    func scrollToCategoryIndexPath() {
        lessonCategoriesCellViewModel.scrollToIndexPath.accept(categoryIndexPath)
        categoryIndexPath = nil
    }

    func getProfileInfo() {
        accountService
            .getProfileInfo()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.profileTabImage.accept(response.avatarUrl)
                    if let isSpecialist = response.isSpecialist,
                       isSpecialist,
                       let classNumber = response.numberClass {
                        self.classNumberRelay.accept(classNumber)
                        guard let classNumber = response.numberClass else { return }
                        self.fetchOwnId(classNumber: classNumber)
                    } else {
                        UserDefaultsHelper.shared.userId = response.id
                    }
                case .error(let error):
                    guard let errorResponse = error as? Moya.MoyaError, let statusCode = errorResponse.response?.statusCode else { return }
                    if statusCode == 401 {
                        self.flow.accept(.onTokenExpiration)
                    }
                    print(error.localizedDescription)
                }
            }
            .disposed(by: bag)
    }

    func getLessons() {
        Single.zip(mainService.nearestLessons(pageSize: Consts.numberOfNearestdStreamsInPage),
                   lessonService.live(pageNumber: 1, lessonsInPage: Consts.numberOfLiveStreamsInPage),
                   lessonService.myBroadcasts(page: 20),
                   mainService.getBannerImg(),
                   lessonService.savedStreams(page: 1, count: Consts.numberOfSavedStreamsInPage))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success((let nearest, let live, let myLessons, let bannerImg, let saved)):

                    self.liveStreamsList.accept(live.data ?? [])
                    self.savedStreamsList = saved.data ?? []
                    self.myBroadcasts = myLessons

                    var items: [AnyTableViewCellModelProtocol] = []
                    self.bannerTableCellViewModel.setDataModel(bannerList: bannerImg)
                    items.append(self.bannerTableCellViewModel)

                    self.lessonsLiveCellViewModel.setDataModel(lessons: live.data ?? [])
                    if live.data?.isEmpty == false {
                        items.append(self.lessonsLiveCellViewModel)
                    }
                    self.lessonCategoriesCellViewModel.setDataModel()
                    items.append(self.lessonCategoriesCellViewModel)

                    self.lessonsNearestCellViewModel.setDataModel(lessons: nearest)
                    if !nearest.isEmpty {
                        items.append(self.lessonsNearestCellViewModel)
                    }

                    self.lessonSavedCellViewModel.setDataModel(lessons: saved.data ?? [], andShowDeleteButton: false)
                    if saved.data?.isEmpty == false {
                        items.append(self.lessonSavedCellViewModel)
                    }

                    self.dataItems.accept(items)

                    let idLive = live.data?.map({ stream -> Int in return stream.id })
                    let idMy = myLessons.lessonsSoon?.map { $0.id }
                    let ids = Array(Set((idLive ?? []) + (idMy ?? [])))
                    self.getAllLiveLessonsDetails(ids: ids)
                case .error(let error):
                    guard let errorResponse = error as? Moya.MoyaError, let statusCode = errorResponse.response?.statusCode else {
                        let text = ""
                        return self.lessonLoadError.accept((text, error.localizedDescription))
                    }
                    let text = "error_code".localized + " " + "\(statusCode)"
                    self.lessonLoadError.accept((text, errorResponse.localizedDescription))
                }
            }
            .disposed(by: bag)
    }

    func getPurchases() {
        paymentsService.getPlan()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.iapManager.purchases = response
                    var inAppProducts = [String]()
                    response.forEach { plan in
                        guard let planId = plan.iosInAppId else { return }
                        inAppProducts.append(String(planId))
                    }
                    self.iapManager.setupPurchases { success in
                        guard success else { return }
                        self.iapManager.getProducts(identifiers: inAppProducts)
                    }
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func getDifferenceTimeWithServer() {
        let date = Date()

        let currentDateString = Formatters.dateFormatterDefault.string(from: date)
        let currentTimeString = Formatters.hourMinuteSecondsFormatter.string(from: date)
        let currentString = currentDateString + " " + currentTimeString
        let currentDate = Formatters.createLessonTimeFormatter.date(from: currentString)
        let currStamp = currentDate?.timeIntervalSince1970 ?? 0

        mainService.serverTime()
            .subscribe { event in
                switch event {
                case .success(let response):
                    var diffBetweenServer = ((response.currentTime ?? 0) - currStamp)
                    diffBetweenServer = abs(diffBetweenServer) > 60 ? diffBetweenServer : 0
                    Date.diffrenceBetweenRealTime = diffBetweenServer
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func fetchOwnId(classNumber: String) {
        accountService
            .publicProfile(classNumber: classNumber)
            .subscribe { event in
                switch event {
                case .success(let response):
                    guard let userId = response.specialistInfo.id else { return }
                    UserDefaultsHelper.shared.userId = userId
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func bind() {
        lessonsPopularCellViewModel
            .onLessonDetail
            .map { $0.id }
            .map { id in .onLessonDetail(id) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonsNearestCellViewModel
            .itemSelected
            .bind { [weak self] value in
                guard let value = value else { return }
                self?.nearestIndexPath = value
            }
            .disposed(by: lessonsNearestCellViewModel.bag)

        lessonsPopularCellViewModel
            .itemSelected
            .bind { [weak self] value in
                guard let value = value else { return }
                self?.popularIndexPath = value
            }
            .disposed(by: lessonsPopularCellViewModel.bag)

        lessonCategoriesCellViewModel
            .itemSelected
            .bind { [weak self] value in
                guard let value = value else { return }
                self?.categoryIndexPath = value
            }
            .disposed(by: lessonCategoriesCellViewModel.bag)

        lessonsPopularCellViewModel
            .onAvatarTap
            .map { .onProfileSelect(classNumber: $0) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonsNearestCellViewModel
            .onAvatarTap
            .map { .onProfileSelect(classNumber: $0) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonsNearestCellViewModel
            .modelSelected
            .map { $0.id }
            .map { id in .onLessonDetail(id) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonsLiveCellViewModel
            .onLessonDetail
            .bind { [weak self] model in
                guard let self = self else { return }
                let stream = model.currentStream
                self.flow.accept(.watchStream(lessonId: stream.id,
                                              numberOfStreamsInPage: Consts.numberOfSavedStreamsInPage,
                                              countOfLoadedPages: self.lessonsLiveCellViewModel.lessons.count,
                                              myBroadcasts: self.myBroadcasts))
            }
            .disposed(by: bag)

        lessonsNearestCellViewModel
            .onButtonTap
            .map { _ in .onFilteredCategoryVC(.nearest) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonsPopularCellViewModel
            .onButtonTapped
            .map { _ in .onFilteredCategoryVC(.popular) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonsLiveCellViewModel
            .onButtonTap
            .map { _ in .onFilteredCategoryVC(.live) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonCategoriesCellViewModel
            .onButtonTapped
            .map { _ in .onAllCategories }
            .bind(to: flow)
            .disposed(by: bag)

        lessonCategoriesCellViewModel
            .onAllCategories
            .map { _ in .onAllCategories }
            .bind(to: flow)
            .disposed(by: bag)

        lessonCategoriesCellViewModel
            .onFilteredCategories
            .map { type in .onFilteredCategoryVC(type) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonSavedCellViewModel
            .showFilteredStreamsButtonTapped
            .map { _ in .onFilteredCategoryVC(.saved) }
            .bind(to: flow)
            .disposed(by: bag)

        lessonSavedCellViewModel
            .onLessonDetail
            .bind { [weak self] model in
                guard let self = self else { return }
                let stream = model.currentStream
                self.flow.accept(.watchSavedStream(lessonId: stream.id,
                                                   numberOfStreamsInPage: Consts.numberOfSavedStreamsInPage,
                                                   streamsList: self.lessonSavedCellViewModel.lessons))
            }
            .disposed(by: bag)

        lessonSavedCellViewModel
            .avatarTapped
            .map { .onProfileSelect(classNumber: $0) }
            .bind(to: flow)
            .disposed(by: bag)
    }

    private func bindAppState() {
        UIApplication.shared.rx
            .applicationWillEnterForeground
            .bind { [weak self] _ in
                self?.getLessons()
            }
            .disposed(by: bag)

    }
    
    private func updateCollectinEveryMinute() {
        timer?.invalidate()
        timer = Timer(timeInterval: 30, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.getLessons()
        }
        if let timerForUpdate = timer {
            RunLoop.main.add(timerForUpdate, forMode: .default)
        }
    }
    
    private func getAllLiveLessonsDetails(ids: [Int]) {
        AllLiveLessonsModel.liveLessonsArray.removeAll()
        ids.forEach { id in
            lessonService
                .lessonDetail(id: id, saved: false)
                .subscribe { event in
                    switch event {
                    case .success(let response):
                        if !AllLiveLessonsModel.liveLessonsArray.contains(response) {
                            AllLiveLessonsModel.liveLessonsArray.append(response)
                        }
                    case .error(let error):
                        print(error)
                    }
                }.disposed(by: bag)
        }
    }
}
