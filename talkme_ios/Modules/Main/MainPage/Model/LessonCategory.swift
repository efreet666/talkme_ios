//
//  LessonCategoryType.swift
//  talkme_ios
//
//  Created by Майя Калицева on 11.02.2021.
//

import UIKit

enum LessonCategoryType: Int {
    case all
    case live
    case education = 2
    case nearest
    case popular
    case saved = 930
    case entertainment = 7
    case fitness = 10
    case chatting = 19
    case nightlife = 23
    case girls = 218
    case learning = 71
    case cooking = 14
    case kids = 20
    case travel = 27
    case business = 29
    case languages = 49
    case music = 184
    case yourShow = 192
    case games = 211
    case other = 26
    case courses = 181
    case masterClass = 600
    case sale = 579
    case car = 598
    case health = 595
    case talkMeStudio = 924
    case mosStream = 786
    case standUp = 926

    var image: UIImage? {
        switch self {
        case .all:
            return UIImage(named: "allCat")
        case .live:
            return UIImage(named: "liveCat")
        case .nearest:
            return UIImage(named: "nearestCat")
        case .popular:
            return UIImage(named: "popularCat")
        case .saved:
            return UIImage(named: "savedCat")
        case .education:
            return UIImage(named: "educationCat")
        case .entertainment:
            return UIImage(named: "funCat")
        case .fitness:
            return UIImage(named: "fitnessCat")
        case .chatting:
            return UIImage(named: "communicationCat")
        case .nightlife:
            return UIImage(named: "nightLiveCat")
        case .girls:
            return UIImage(named: "womenCat")
        case .learning:
            return UIImage(named: "studyCat")
        case .cooking:
            return UIImage(named: "cookingCat")
        case .kids:
            return UIImage(named: "kidsCat")
        case .travel:
            return UIImage(named: "travelCat")
        case .business:
            return UIImage(named: "businessCat")
        case .languages:
            return UIImage(named: "languagesCat")
        case .music:
            return UIImage(named: "musicCat")
        case .yourShow:
            return UIImage(named: "yourShowCat")
        case .games:
            return UIImage(named: "gamesCat")
        case .other:
            return UIImage(named: "differentCat")
        case .courses:
            return UIImage(named: "coursesCat")
        case .sale:
            return UIImage(named: "saleCat")
        case .car:
            return UIImage(named: "carCat")
        case .talkMeStudio:
            return UIImage(named: "talkMeStudioCat")
        case .mosStream:
            return UIImage(named: "mosStreamCat")
        case .health:
            return UIImage(named: "healthCat")
        case .masterClass:
            return UIImage(named: "masterClassCat")
        case .standUp:
            return UIImage(named: "standUpCat")
        }
    }

    var whiteBackgroundCategoriesImage: UIImage? {
        switch self {
        case .all:
            return UIImage(named: "allCatWhite")
        case .live:
            return UIImage(named: "liveCatWhite")
        case .nearest:
            return UIImage(named: "nearestCatWhite")
        case .popular:
            return UIImage(named: "popularCatWhite")
        case .saved:
            return UIImage(named: "savedCatWhite")
        case .education:
            return UIImage(named: "educationCatWhite")
        case .entertainment:
            return UIImage(named: "funCatWhite")
        case .fitness:
            return UIImage(named: "fitnessCatWhite")
        case .chatting:
            return UIImage(named: "communicationCatWhite")
        case .nightlife:
            return UIImage(named: "nightLiveCatWhite")
        case .girls:
            return UIImage(named: "womenCatWhite")
        case .learning:
            return UIImage(named: "studyCatWhite")
        case .cooking:
            return UIImage(named: "cookingCatWhite")
        case .kids:
            return UIImage(named: "kidsCatWhite")
        case .travel:
            return UIImage(named: "travelCatWhite")
        case .business:
            return UIImage(named: "businessCatWhite")
        case .languages:
            return UIImage(named: "languagesCatWhite")
        case .music:
            return UIImage(named: "musicCatWhite")
        case .yourShow:
            return UIImage(named: "yourShowCatWhite")
        case .games:
            return UIImage(named: "gamesCatWhite")
        case .other:
            return UIImage(named: "differentCatWhite")
        case .courses:
            return UIImage(named: "coursesCatWhite")
        case .sale:
            return UIImage(named: "saleCatWhite")
        case .car:
            return UIImage(named: "carCatWhite")
        case .talkMeStudio:
            return UIImage(named: "talkMeStudioCatWhite")
        case .mosStream:
            return UIImage(named: "mosStreamCatWhite")
        case .masterClass:
            return UIImage(named: "masterClassCatWhite")
        case .health:
            return UIImage(named: "healthCatWhite")
        case .standUp:
            return UIImage(named: "standUpCatWhite")
        }
    }

var title: String {
    switch self {
    case .all:
        return "main_page_category_all".localized
    case .live:
        return "main_page_category_live".localized
    case .nearest:
        return "main_page_nearest_lessons_streams".localized
    case .popular:
        return "main_page_popular_lessons_streams".localized
    case .saved:
        return "main_page_saved_lessons_streams".localized
    case .education:
        return "lesson_attribute_category_learning".localized
    case .entertainment:
        return "lesson_attribute_category_entertainment".localized
    case .fitness:
        return "lesson_attribute_category_fintess".localized
    case .chatting:
        return "lesson_attribute_category_chatting".localized
    case .nightlife:
        return "lesson_attribute_category_nightlife".localized
    case .girls:
        return "lesson_attribute_category_girls".localized
    case .learning:
        return "lesson_attribute_category_education".localized
    case .cooking:
        return "lesson_attribute_category_cooking".localized
    case .kids:
        return "lesson_attribute_category_kids".localized
    case .travel:
        return "lesson_attribute_category_travel".localized
    case .business:
        return "lesson_attribute_category_business".localized
    case .languages:
        return "lesson_attribute_category_languages".localized
    case .music:
        return "lesson_attribute_category_music".localized
    case .yourShow:
        return "lesson_attribute_category_yourShow".localized
    case .games:
        return "lesson_attribute_category_games".localized
    case .other:
        return "lesson_attribute_category_other".localized
    case .courses:
        return "lesson_attribute_category_courses".localized
    case .sale:
        return "lesson_attribute_category_sale".localized
    case .car:
        return "lesson_attribute_category_car".localized
    case .masterClass:
        return "lesson_attribute_category_master_class".localized
    case .talkMeStudio:
        return "lesson_attribute_category_talkme_studio".localized
    case .mosStream:
        return "lesson_attribute_category_mos_stream".localized
    case .health:
        return "lesson_attribute_category_health".localized
    case .standUp:
        return "lesson_attribute_category_stand_up".localized
    }
}

var color: UIColor {
    switch self {
    case .fitness:
        return TalkmeColors.turquoiseCategoryIcon
    case .learning:
        return TalkmeColors.purpleCategoryIcon
    case .entertainment, .other:
        return TalkmeColors.cyanCategoryIcon
    case .chatting:
        return TalkmeColors.yellowCategoryIcon
    case .nightlife, .courses:
        return TalkmeColors.redCategoryIcon
    case .girls, .kids:
        return TalkmeColors.pinkCategoryIcon
    case .education:
        return TalkmeColors.blueCategoryIcon
    case .cooking:
        return TalkmeColors.greenCategoryIcon
    case .travel:
        return TalkmeColors.darkYellowCategoryIcon
    case .business:
        return TalkmeColors.darkBlueCategoryIcon
    case .languages:
        return TalkmeColors.darkCyanCategoryIcon
    case .music:
        return TalkmeColors.darkTurquoiseCategoryIcon
    case .yourShow:
        return TalkmeColors.lightPurpleCategoryIcon
    case .games:
        return TalkmeColors.darkGreenCategoryIcon
    case .all, .nearest, .popular, .live, .saved:
        return TalkmeColors.white
    case .sale:
        return TalkmeColors.white
    case .masterClass:
        return TalkmeColors.white
    case .car:
        return TalkmeColors.white
    case .talkMeStudio:
        return TalkmeColors.white
    case .mosStream:
        return TalkmeColors.white
    case .health:
        return TalkmeColors.white
    case .standUp:
        return TalkmeColors.white
    }
}

    var stringId: String {
        switch self {
        case .nearest:
            return "nearest"
        case .popular:
            return "popular"
        case .live:
            return "live"
        default:
            return "\(self.rawValue)"
        }
    }
}
