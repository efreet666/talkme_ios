//
//  PaginationView.swift
//  talkme_ios
//
//  Created by VladimirCH on 28.03.2022.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

final class PaginationView: UIStackView {

    var numberOfPages: Int = 0 {
        didSet {
            addItems()
            setupUI()
        }
    }

    var currentPage: Int = 0 {
        didSet {
            changeColorItems()
        }
    }

    private var items = [UIView]()

    func changeColorItems() {
        if numberOfPages > 0 {
            for index in 0..<items.count {
                items[index].backgroundColor = index == currentPage ? .white : .white.withAlphaComponent(0.4)
            }
        }
    }

    private func addItems() {
        items = []
        for tag in 0..<numberOfPages {
            let paginationItem = PaginatioinItem()
            paginationItem.tag = tag
            paginationItem.backgroundColor = tag == 0 ? .white : .white.withAlphaComponent(0.4)
            self.items.append(paginationItem)
        }
    }

    private func setupUI() {
        for view in subviews {
            view.removeFromSuperview()
        }
        items.forEach(addArrangedSubview)
        self.spacing = UIScreen.isSE ? 4 : 6
        self.axis = .horizontal
    }

}

final class PaginatioinItem: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    private func configure() {
        self.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.isSE ? 9 : 11)
            make.height.equalTo(UIScreen.isSE ? 2 : 3)
        }

        self.layer.cornerRadius = UIScreen.isSE ? 1 : 1.5
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
