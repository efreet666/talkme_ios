//
//  MainPageNearestView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 08.02.2021.
//

import RxCocoa
import RxSwift

final class MainPageNearestView: UIView {

    // MARK: - Public Properties

    private(set) lazy var avatarTapped = avatarButton.rx.tap

    let lessonNameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.numberOfLines = 0
        label.textColor = TalkmeColors.white
        return label
    }()

    // MARK: - Private Properties

    private let backgroundImage: UIImageView = {
        let iw = UIImageView()
        iw.layer.cornerRadius = 12
        iw.contentMode = .scaleAspectFill
        iw.clipsToBounds = true
        return iw
    }()

    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = UIScreen.isSE ? 16.5 : 26
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var avatarButton: UIButton = {
        let button = UIButton()
        button.setTitle(nil, for: .normal)
        return button
    }()

    private let gradient = CAGradientLayer(
        start: .nearestStart,
        end: .nearestEnd,
        colors: [TalkmeColors.secondMainGradient.cgColor, TalkmeColors.gradient4.cgColor],
        locations: [0.25, 0.81])

    private let priceLabel = StreamPriceView()
    private let dateLabel = ImageAndTitleView()

    private let imageContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.lessonTimeAlphaGray50
        view.layer.cornerRadius = UIScreen.isSE ? 12.5 : 16.5
        return view
    }()

    private let categoryImage: UIImageView = {
        let iw = UIImageView()
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private var lessonStartTimeLabel: ImageAndTitleView = {
        let view = ImageAndTitleView()
        return view
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(lessonModel: LiveStream) {
        let lessonCostIsZero = (lessonModel.cost ?? 0) == 0
        priceLabel.configure(viewType: lessonCostIsZero ? .freeStream : .moneyStream(price: String(lessonModel.cost ?? 0)))
        lessonNameLabel.text = lessonModel.name
        categoryImage.image = LessonCategoryType(rawValue: lessonModel.category?.parent ?? 0)?.image
        dateLabel.configure(.date(Formatters.convertDate(seconds: lessonModel.date)))
        let date = Date(timeIntervalSince1970: TimeInterval(lessonModel.date))
        lessonStartTimeLabel.configure(.time(Formatters.timeFormatter.string(from: date)))
        guard let tileImage = lessonModel.tileImage, let url = URL(string: tileImage) else { return }
        backgroundImage.kf.setImage(with: url)
        guard let avatar = lessonModel.owner.avatarUrl, let avatarUrl = URL(string: avatar) else {
            avatarImageView.image = UIImage(named: "noAvatar")
            return
        }
        avatarImageView.kf.setImage(with: avatarUrl)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = backgroundImage.bounds
    }

    // MARK: - Private Methods

    private func initialSetup() {
        imageContainerView.addSubview(categoryImage)
        backgroundImage.addSubviews([avatarImageView, priceLabel, dateLabel, lessonNameLabel, lessonStartTimeLabel, imageContainerView])
        self.addSubviews(backgroundImage, avatarButton)
        backgroundImage.layer.insertSublayer(gradient, at: 0)

        backgroundImage.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 222 : 291)
            make.width.equalTo(UIScreen.isSE ? 132 : 173)
            make.edges.equalToSuperview()
        }

        avatarImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 33: 52)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 11 : 14)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 8 : 10)
        }

        avatarButton.snp.makeConstraints { make in
            make.size.equalTo(55)
            make.center.equalTo(avatarImageView.snp.center)
        }

        lessonNameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.top.greaterThanOrEqualToSuperview().inset(UIScreen.isSE ? 80 : 117)
            make.bottom.equalTo(lessonStartTimeLabel.snp.top).inset(UIScreen.isSE ? -10 : -15)
        }

        lessonStartTimeLabel.snp.makeConstraints { make in
            make.top.greaterThanOrEqualTo(lessonNameLabel.snp.bottom).inset(UIScreen.isSE ? 10 : 15)
            make.bottom.equalTo(priceLabel.snp.top).inset(UIScreen.isSE ? -10 : -15)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 8 : 10)
            make.trailing.equalTo(dateLabel.snp.leading)
        }

        dateLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(lessonStartTimeLabel)
            make.leading.equalTo(lessonStartTimeLabel.snp.trailing)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 2 : 20)
        }

        priceLabel.snp.makeConstraints { make in
            make.top.equalTo(dateLabel.snp.bottom).inset(UIScreen.isSE ? 6 : 9)
            make.trailing.equalTo(imageContainerView.snp.leading).inset(UIScreen.isSE ? -11 : -16)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 17 : 22)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 8 : 11)
        }

        imageContainerView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 25 : 33)
            make.top.bottom.equalTo(priceLabel)
            make.leading.equalTo(priceLabel.snp.trailing).inset(UIScreen.isSE ? 11 : 16)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 9 : 10)
        }

        categoryImage.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIScreen.isSE ? 6 : 8)
        }
    }
}
