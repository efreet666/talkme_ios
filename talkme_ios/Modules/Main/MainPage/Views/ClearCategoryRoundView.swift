//
//  ClearCategoryRoundView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 03.03.2021.
//

import UIKit

final class ClearCategoryRoundView: UIView {

    // MARK: - Public Properties

    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupLayout()
        clipsToBounds = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(iconImageView)
        iconImageView.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? 52 : 70)
        }
    }
}
