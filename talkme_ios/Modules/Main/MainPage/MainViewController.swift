//
//  MainViewController.swift
//  talkme_ios
//
//  Created by 1111 on 08.02.2021.
//

import RxSwift
import RxCocoa

final class MainViewController: UIViewController {

    // MARK: - Private properties

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private let bag = DisposeBag()

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.delaysContentTouches = false
        tbv.keyboardDismissMode = .onDrag
        tbv.backgroundColor = TalkmeColors.mainAccountBackground
        tbv.registerCells(
            withModels:
            MainPageBannerTableCellViewModel.self,
            MainPageLiveTableCellViewModel.self,
            MainPageCategoriesTableCellViewModel.self,
            MainPageNearestLessonsCellViewModel.self,
            MainPageSavedLessonTableCellViewModel.self)
        return tbv
    }()

    private let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "talkme"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        return refresh
    }()

    private let viewModel: MainViewModel

    // MARK: - Init

    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        bindVM()
        viewModel.getProfileInfo()
        viewModel.getLessons()
        viewModel.getPurchases()
        tableView.refreshControl = refreshControl
        setupNotificationCenter()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        scrollToLastSelectedIfNeeded()
        viewModel.getLessons()
        setupNavigationBar()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    // MARK: - Public Methods

    @objc func movedToForeground() {
        viewModel.getLessons()
    }

    @objc func refresh() {
        viewModel.getLessons()
        tableView.refreshControl?.endRefreshing()
    }

    // MARK: - Private Methods

    private func setupNotificationCenter() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(movedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    private func scrollToLastSelectedIfNeeded() {
        if viewModel.nearestIndexPath != nil {
            viewModel.scrollToNearestIndexPath()
        } else if viewModel.popularIndexPath != nil {
            viewModel.scrollToPopularIndexPath()
        } else if viewModel.categoryIndexPath != nil {
            viewModel.scrollToCategoryIndexPath()
        } else {
            viewModel.getLessons()
            viewModel.getProfileInfo()
        }
    }

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        viewModel
            .classNumberRelay
            .bind { [weak self] classNumber in
                guard !classNumber.isEmpty else {
                    self?.customNavigationController?.style = .gradientBlueWithQR
                    return
                }
                self?.customNavigationController?.style = .gradientBlueWithQRAndClassNumber(classNumber)
            }
            .disposed(by: bag)

        // #if DEBUG
        viewModel
            .lessonLoadError
            .bind { [weak self] code, errorMessage in
                let alert = UIAlertController(title: code, message: errorMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self?.present(alert, animated: true, completion: nil)
            }
        // #endif
    }

    private func setupLayout() {
        view.addSubviews(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigationBar() {
        customNavigationController?.style = UserDefaultsHelper.shared.isSpecialist
            ? .gradientBlueWithQRAndClassNumber(UserDefaultsHelper.shared.classNumber)
            : .gradientBlueWithQR
    }
}
