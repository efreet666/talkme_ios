//
//  MainPagePopularViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 10.02.2021.
//

import RxCocoa
import RxSwift

final class MainPagePopularViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let lessonsModel: LiveStream
    let onAvatarTap = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(lessonsModel: LiveStream) {
        self.lessonsModel = lessonsModel
    }

    // MARK: - Public Methods

    func configure(_ cell: MainPagePopularCell) {
        cell.configure(lessonModel: lessonsModel)

        cell
            .avatarTapped
            .map { _ in () }
            .bind(to: onAvatarTap)
            .disposed(by: cell.bag)
    }
}
