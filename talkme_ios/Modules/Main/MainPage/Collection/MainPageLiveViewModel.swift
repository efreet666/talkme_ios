//
//  MainPageLiveViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 13.02.2021.
//

import UIKit

final class MainPageLiveViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let lessonsModel: LiveStream

    // MARK: - Initializers

    init(lessonsModel: LiveStream) {
        self.lessonsModel = lessonsModel
    }

    // MARK: - Public Methods

    func configure(_ cell: MainPageLiveCell) {
        cell.configure(lessonsModel: lessonsModel)
    }
}
