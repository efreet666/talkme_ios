//
//  BannerImageCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by VladimirCH on 24.03.2022.
//

import RxRelay
import RxSwift

final class BannerImageCollectionCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let bannerModel: String
    private var referenceLink: String = ""

    // MARK: - Initializers

    init(bannerModel: String?, referenceLink: String?) {
        self.bannerModel = bannerModel ?? ""
        self.referenceLink = referenceLink ?? ""
    }

    // MARK: - Public Methods

    func configure(_ cell: BannerImageCollectionCell) {
        cell.configure(bannerModel: bannerModel)

        cell
            .onButtonTapped
            .bind { [weak self] _ in
                self?.setUrlOnBrauser()
            }
            .disposed(by: cell.bag)
    }

    func setUrlOnBrauser() {
        guard let url = URL(string: self.referenceLink) else { return }
            UIApplication.shared.open(url)
    }
}
