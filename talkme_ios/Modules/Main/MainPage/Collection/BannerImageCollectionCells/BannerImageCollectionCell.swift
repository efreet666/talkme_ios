//
//  BannerImageCollectionCell.swift
//  talkme_ios
//
//  Created by VladimirCH on 24.03.2022.
//

import RxSwift
import Kingfisher

final class BannerImageCollectionCell: UICollectionViewCell {

    // MARK: - Private Properties

    private(set) lazy var onButtonTapped = buttonTapBanner.rx.tap
    private(set) var bag = DisposeBag()

    private let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 12
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()

    private let buttonTapBanner: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.isExclusiveTouch = true
        return button
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(bannerModel: String?) {
        guard let bannerImg = bannerModel,
              let url = URL(string: bannerImg) else { return }
                backgroundImage.kf.setImage(with: url)
        // uncommit this code if you whant some retry stratagy and not to cache image
//        let retry = DelayRetryStrategy(maxRetryCount: 5, retryInterval: .seconds(1))
//        ImageCache.default.removeImage(forKey: url.absoluteString)
//        backgroundImage.kf.setImage(with: url, options: [.retryStrategy(retry)])
    }

    // MARK: - Private Methods

    private func initialSetup() {
        contentView.addSubviews(backgroundImage, buttonTapBanner)
        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        buttonTapBanner.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
