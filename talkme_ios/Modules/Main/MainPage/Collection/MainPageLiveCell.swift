//
//  MainPageLiveCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 13.02.2021.
//

import RxSwift
import Kingfisher

final class MainPageLiveCell: UICollectionViewCell {

    // MARK: - Private Properties

    private let lessonNameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.numberOfLines = 0
        label.textColor = TalkmeColors.white

        return label
    }()

    private let liveLabel: UILabel = {
        let label = RoundedLabel(withInsets: 6, 6, 4, 4)
        label.text = "Live"
        label.font = UIScreen.isSE ? .montserratSemiBold(ofSize: 11) : .montserratSemiBold(ofSize: 15)
        label.textColor = TalkmeColors.white
        label.layer.backgroundColor = TalkmeColors.greenLabels.cgColor
        label.textAlignment = .center
        label.layer.cornerRadius = UIScreen.isSE ? 12 : 17
        return label
    }()

    private let backgroundImage: UIImageView = {
        let iw = UIImageView()
        iw.layer.cornerRadius = 12
        iw.contentMode = .scaleAspectFill
        iw.clipsToBounds = true
        return iw
    }()

    private var priceLabel: StreamPriceView = {
        let view = StreamPriceView()
        view.layer.cornerRadius = UIScreen.isSE ? 12 : 15.5
        return view
    }()

    private let imageContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.lessonTimeAlphaGray80
        view.layer.cornerRadius = UIScreen.isSE ? 17 : 18.5
        return view
    }()

    private let categoryImage: UIImageView = {
        let iw = UIImageView()
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let gradient = CAGradientLayer(
        start: .liveStart,
        end: .liveEnd,
        colors: [TalkmeColors.liveGradient.cgColor, TalkmeColors.gradient4.cgColor],
        locations: [0, 1])

    private let lessonTime = ImageAndTitleView()

    private let backgroundTime: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.lessonTimeAlphaGray80
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        return view
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = bounds
    }

    func configure(lessonsModel: LiveStream) {
        let lessonCostIsZero = (lessonsModel.cost ?? 0) == 0
        priceLabel.configure(viewType: lessonCostIsZero ? .freeStream : .moneyStream(price: String(lessonsModel.cost ?? 0)))
        let attributedString = NSMutableAttributedString(string: lessonsModel.name)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.15
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSMakeRange(0, attributedString.length))
        self.lessonNameLabel.attributedText = attributedString
        let date = Date(timeIntervalSince1970: TimeInterval(lessonsModel.date))
        lessonTime.configure(.bigTime(Formatters.timeFormatter.string(from: date)))
        categoryImage.image = LessonCategoryType(rawValue: lessonsModel.category?.parent ?? 0)?.image
        guard let tileImage = lessonsModel.tileImage, let url = URL(string: tileImage) else { return }
        backgroundImage.kf.setImage(with: url)
    }

    // MARK: - Private Methods

    private func initialSetup() {
        imageContainerView.addSubview(categoryImage)
        backgroundImage.addSubviews([liveLabel, priceLabel, backgroundTime, lessonTime, lessonNameLabel, imageContainerView])
        contentView.addSubview(backgroundImage)
        backgroundImage.layer.insertSublayer(gradient, at: 0)

        backgroundImage.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 267: 313)
            make.width.equalTo(UIScreen.isSE ? 190 : 223)
            make.edges.equalToSuperview()
        }

        liveLabel.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.isSE ? 37 : 53)
            make.height.equalTo(UIScreen.isSE ? 24 : 34)
            make.top.leading.equalToSuperview().inset(UIScreen.isSE ? 15 : 16)
        }

        priceLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(15)
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -19 : -17)
            make.height.equalTo(UIScreen.isSE ? 24 : 31)
            make.width.equalTo(UIScreen.isSE ? 81 : 106)
        }

        backgroundTime.snp.makeConstraints { make in
            make.centerY.equalTo(liveLabel)
            make.trailing.equalToSuperview().inset(14)
            make.width.equalTo(UIScreen.isSE ? 64 : 74)
            make.height.equalTo(24)
        }

        lessonNameLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 15 : 16)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 20 : 30)
            make.top.greaterThanOrEqualToSuperview().inset(100)
            make.bottom.equalTo(priceLabel.snp.top).inset(UIScreen.isSE ? -12 : -14)
        }

        imageContainerView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 34 : 37)
            make.trailing.bottom.equalToSuperview().inset(14)
        }

        categoryImage.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(9)
        }

        lessonTime.snp.makeConstraints { make in
            make.centerX.centerY.equalTo(backgroundTime)
        }
    }
}
