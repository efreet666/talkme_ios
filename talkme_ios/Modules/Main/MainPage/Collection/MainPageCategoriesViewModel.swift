//
//  MainPageCategoriesViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 11.02.2021.
//

import UIKit

final class MainPageCategoriesViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let categoryModel: LessonCategoryType

    // MARK: - Initializers

    init(categoryModel: LessonCategoryType) {
        self.categoryModel = categoryModel
    }

    // MARK: - Public Methods

    func configure(_ cell: MainPageCategoriesCell) {
        cell.configure(categoryModel)
    }
}
