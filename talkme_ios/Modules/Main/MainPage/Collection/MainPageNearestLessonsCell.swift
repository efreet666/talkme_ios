//
//  MainPageNearestLessonsCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 08.02.2021.
//

import RxSwift
import RxCocoa

final class MainPageNearestLessonsCell: UICollectionViewCell {

    // MARK: - Public properties

    private(set) lazy var avatarTapped = itemView.avatarTapped
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let itemView = MainPageNearestView()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overriden Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Public Methods

    func configure(lessonsModel: LiveStream) {
        itemView.configure(lessonModel: lessonsModel)
    }

    // MARK: - Private Method

    private func setupLayout() {
        contentView.addSubview(itemView)
        itemView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
