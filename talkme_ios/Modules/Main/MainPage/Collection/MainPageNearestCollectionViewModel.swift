//
//  MainPageNearestCollectionViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 08.02.2021.
//

import RxRelay
import RxSwift

final class MainPageNearestCollectionViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let lessonsModel: LiveStream
    let onAvatarTap = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(lessonsModel: LiveStream) {
        self.lessonsModel = lessonsModel
    }

    // MARK: - Public Methods

    func configure(_ cell: MainPageNearestLessonsCell) {
        cell.configure(lessonsModel: lessonsModel)

        cell
            .avatarTapped
            .map { _ in () }
            .bind(to: onAvatarTap)
            .disposed(by: cell.bag)
    }
}
