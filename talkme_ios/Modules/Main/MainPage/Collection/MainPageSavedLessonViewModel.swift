//
//  MainPageSavedLessonViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 8/6/2022.
//

import RxRelay
import RxSwift

final class MainPageSavedLessonViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let lessonsModel: LiveStream
    let avatarTapped = PublishRelay<Void>()
    let deleteButtonTapped = PublishRelay<Void>()
    var deleteButtonIsShown = false
    let bag = DisposeBag()

    // MARK: - Initializers

    init(lessonsModel: LiveStream, showDeleteButton: Bool) {
        self.lessonsModel = lessonsModel
        self.deleteButtonIsShown = showDeleteButton
    }

    // MARK: - Public Methods

    func configure(_ cell: MainPageSavedLessonCell) {
        cell.configure(lessonsModel: lessonsModel, showDeleteButton: deleteButtonIsShown)

        cell
            .avatarTapped
            .map { _ in () }
            .bind(to: avatarTapped)
            .disposed(by: cell.bag)

        cell
            .deleteButtonTapped
            .map { _ in () }
            .bind(to: deleteButtonTapped)
            .disposed(by: cell.bag)
    }
}
