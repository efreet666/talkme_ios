//
//  MainPagePopularCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 10.02.2021.
//

import RxSwift

final class MainPagePopularCell: UICollectionViewCell {

    // MARK: - Public properties

    private(set) lazy var avatarTapped = avatarButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let gradient = CAGradientLayer(
        start: .popularStart,
        end: .popularEnd,
        colors: [TalkmeColors.liveGradient.cgColor, TalkmeColors.gradient4.cgColor],
        locations: [0, 1])

    private let lessonNameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.numberOfLines = 0
        label.textColor = TalkmeColors.white
        return label
    }()

    private let backgroundImage: UIImageView = {
        let iw = UIImageView()
        iw.layer.cornerRadius = 12
        iw.contentMode = .scaleAspectFill
        iw.clipsToBounds = true
        return iw
    }()

    private lazy var avatarButton: UIButton = {
        let button = UIButton()
        button.setTitle(nil, for: .normal)
        return button
    }()

    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = UIScreen.isSE ? 18 : 26
        imageView.clipsToBounds = true
        return imageView
    }()

    private var priceLabel: StreamPriceView = {
        let view = StreamPriceView()
        view.configure(viewType: .bigFreeStream)
        view.layer.cornerRadius = UIScreen.isSE ? 15 : 18
        return view
    }()

    private let dateLabel = ImageAndTitleView()

    private var lessonStartTimeLabel = ImageAndTitleView()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overriden Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Public Methods

    func configure(lessonModel: LiveStream) {
        let lessonCostIsZero = (lessonModel.cost ?? 0) == 0
        priceLabel.configure(viewType: lessonCostIsZero ? .freeStream : .moneyStream(price: String(lessonModel.cost ?? 0)))
        lessonNameLabel.text = lessonModel.name
        dateLabel.configure(.date(Formatters.convertDate(seconds: lessonModel.date)))
        let date = Date(timeIntervalSince1970: TimeInterval(lessonModel.date))
        lessonStartTimeLabel.configure(.time(Formatters.timeFormatter.string(from: date)))
        guard let tileImage = lessonModel.tileImage, let url = URL(string: tileImage) else { return }
        backgroundImage.kf.setImage(with: url)
        guard let avatar = lessonModel.owner.avatarUrl, let avatarUrl = URL(string: avatar) else {
            avatarImageView.image = UIImage(named: "noAvatar")
            return
        }
        avatarImageView.kf.setImage(with: avatarUrl)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = bounds
    }

    // MARK: - Private Methods

    private func initialSetup() {
        backgroundImage.addSubviews([avatarImageView, priceLabel, dateLabel, lessonNameLabel, lessonStartTimeLabel])
        contentView.addSubviews(backgroundImage, avatarButton)
        backgroundImage.layer.insertSublayer(gradient, at: 0)

        backgroundImage.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 244 : 312)
            make.width.equalTo(UIScreen.isSE ? 174 : 222)
            make.edges.equalToSuperview()
        }

        avatarImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 36 : 52)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 14 : 18)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 11 : 14)
        }

        avatarButton.snp.makeConstraints { make in
            make.size.equalTo(55)
            make.center.equalTo(avatarImageView.snp.center)
        }

        lessonNameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(12)
            make.top.greaterThanOrEqualToSuperview().inset(UIScreen.isSE ? 89 : 163)
            make.bottom.equalTo(priceLabel.snp.top).inset(UIScreen.isSE ? -15 : -16)
        }

        priceLabel.snp.makeConstraints { make in
            make.top.equalTo(lessonNameLabel.snp.bottom).inset(16)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 80 : 95)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 15 : 19)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 9 : 12)
            make.height.equalTo(UIScreen.isSE ? 28 : 36)
        }

        lessonStartTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(priceLabel)
            make.bottom.equalTo(dateLabel.snp.top).inset(UIScreen.isSE ? -6 : -8)
            make.leading.equalTo(dateLabel.snp.leading)
            make.trailing.greaterThanOrEqualToSuperview()
        }

        dateLabel.snp.makeConstraints { make in
            make.top.equalTo(lessonStartTimeLabel.snp.bottom).inset(UIScreen.isSE ? 6 : 8)
            make.bottom.equalTo(priceLabel)
            make.trailing.greaterThanOrEqualToSuperview()
            make.leading.equalTo(priceLabel.snp.trailing).inset(UIScreen.isSE ? -9 : -11)
        }
    }
}
