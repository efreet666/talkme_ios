//
//  MainPageSavedLessonCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 8/6/2022.
//

import RxSwift
import Kingfisher
import UIKit

final class MainPageSavedLessonCell: UICollectionViewCell {

    // MARK: - Public Properties

    private(set) lazy var avatarTapped = avatarButton.rx.tap
    private(set) lazy var deleteButtonTapped = deleteStreamButton.rx.tapGesture().when(.recognized)
    private(set) var bag = DisposeBag()

    private(set) var lessonNameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.numberOfLines = 0
        label.textColor = TalkmeColors.white

        return label
    }()

    // MARK: - Private Properties

    private let backgroundImage: UIImageView = {
        let iw = UIImageView()
        iw.layer.cornerRadius = 12
        iw.contentMode = .scaleAspectFill
        iw.clipsToBounds = true
        return iw
    }()

    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = UIScreen.isSE ? 16.5 : 26
        imageView.clipsToBounds = true
        return imageView
    }()

    private let playImagView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "savedStreamMark"))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private let deleteStreamButton = CrossImageView(frame: .zero)

    private lazy var avatarButton: UIButton = {
        let button = UIButton()
        button.setTitle(nil, for: .normal)
        return button
    }()

    private var priceLabel: StreamPriceView = {
        let view = StreamPriceView()
        view.layer.cornerRadius = UIScreen.isSE ? 12 : 15.5
        return view
    }()

    private let gradient = CAGradientLayer(
        start: .liveStart,
        end: .liveEnd,
        colors: [TalkmeColors.liveGradient.cgColor, TalkmeColors.gradient4.cgColor],
        locations: [0, 1])

    private let lessonEndDateLabel = DateView()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        backgroundImage.image = nil
        avatarImageView.image = nil
        bag = .init()
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        if gradient.bounds != bounds {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            gradient.frame = self.bounds
            CATransaction.commit()
        }
    }

    func configure(lessonsModel: LiveStream, showDeleteButton: Bool) {
        deleteStreamButton.isHidden = !showDeleteButton
        playImagView.isHidden = showDeleteButton
        let lessonCostIsZero = (lessonsModel.cost ?? 0) == 0
        priceLabel.configure(viewType: lessonCostIsZero ? .freeStream : .moneyStream(price: String(lessonsModel.cost ?? 0)))
        lessonNameLabel.text = lessonsModel.name
        lessonEndDateLabel.setDate(to: lessonsModel.date)
        lessonEndDateLabel.setDuration(to: Formatters.setupHourAndMinute(date: lessonsModel.lessonTime) )

        if let tileImage = lessonsModel.tileImage,
              let url = URL(string: tileImage) {
            backgroundImage.kf.setImage(with: url)
        }
        guard let avatar = lessonsModel.owner.avatarUrl,
              let avatarUrl = URL(string: avatar)
        else {
            avatarImageView.image = UIImage(named: "noAvatar")
            return
        }
        avatarImageView.kf.setImage(with: avatarUrl)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        backgroundImage.addSubviews([avatarImageView,
                                     lessonNameLabel,
                                     priceLabel,
                                     lessonEndDateLabel,
                                     playImagView
                                    ])
        contentView.addSubviews([backgroundImage, avatarButton, deleteStreamButton])
        backgroundImage.layer.insertSublayer(gradient, at: 0)

        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        deleteStreamButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 11 : 14)
            make.trailing.equalTo(contentView.snp.trailing).offset(UIScreen.isSE ? -8 : -10)
            make.width.equalTo(deleteStreamButton.snp.height)
            make.height.equalTo(avatarImageView.snp.height).dividedBy(2.2)
        }

        playImagView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 11 : 14)
            make.trailing.equalTo(contentView.snp.trailing).offset(UIScreen.isSE ? -8 : -10)
            make.width.equalTo(playImagView.snp.height).multipliedBy(1.3)
            make.height.equalTo(avatarImageView.snp.height).dividedBy(3)
        }

        avatarImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 11 : 14)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 8 : 10)
            make.size.equalTo(UIScreen.isSE ? 33: 52)
        }

        avatarButton.snp.makeConstraints { make in
            make.center.equalTo(avatarImageView.snp.center)
            make.size.equalTo(55)
        }

        lessonNameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalTo(priceLabel.snp.top).inset(UIScreen.isSE ? -7 : -9)
        }

        priceLabel.snp.makeConstraints { make in
            make.trailing.equalTo(lessonEndDateLabel.snp.leading).inset(UIScreen.isSE ? -5 : -8)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 8 : 11)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 17 : 22)
        }

        lessonEndDateLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(priceLabel)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 8 : 11)
        }
    }
}
