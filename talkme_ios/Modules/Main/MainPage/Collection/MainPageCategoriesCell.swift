//
//  MainPageCategoriesCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 11.02.2021.
//

import RxSwift

final class MainPageCategoriesCell: UICollectionViewCell {

    // MARK: - Public Properties

    let label: UILabel = {
        let lbl = UILabel()
        lbl.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.textColor = TalkmeColors.codeCountry
        lbl.textAlignment = .center
        return lbl
    }()

    // MARK: - Private Properties

    private let categoryView = CategoryRoundView()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ type: LessonCategoryType) {
        categoryView.iconImageView.image = type.whiteBackgroundCategoriesImage
        label.text = type.title
    }

    // MARK: - Private Methods

    private func initialSetup() {
        addSubviews(categoryView, label)
        categoryView.snp.makeConstraints { make in
            make.height.width.equalTo(UIScreen.isSE ? 52 : 70)
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(label.snp.top).offset(UIScreen.isSE ? -5 : -7)
        }
        label.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.centerX.equalTo(categoryView)
            make.top.equalTo(categoryView.snp.bottom).inset(UIScreen.isSE ? 5 : 7)
        }
    }
}
