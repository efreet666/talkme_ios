//
//  MainPageCategoriesTableCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 11.02.2021.
//

import RxCocoa
import RxSwift

final class MainPageCategoriesTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let modelSelected = PublishRelay<LessonCategoryType>()
    let onButtonTapped = PublishRelay<Void>()
    let onAllCategories = PublishRelay<Void>()
    let onFilteredCategories = PublishRelay<LessonCategoryType>()
    let itemSelected = PublishRelay<IndexPath?>()
    let scrollToIndexPath = PublishRelay<IndexPath?>()
    let bag = DisposeBag()

    // MARK: - Private properties

    private let dataModel = LessonCollectionModelCategories()
    private var dataItems: [AnyCollectionViewCellModelProtocol] = []

    // MARK: Public Methods

    func setDataModel() {
        let items: [AnyCollectionViewCellModelProtocol] = dataModel.data.map { MainPageCategoriesViewModel(categoryModel: $0) }
        dataItems = items
    }

    func configure(_ cell: MainPageCategoriesTableCell) {
        cell.setMainItems(dataItems: dataItems)

        cell
            .onButtonTapped
            .bind(to: onButtonTapped)
            .disposed(by: cell.bag)

        cell
            .onAllCategories
            .bind(to: onAllCategories)
            .disposed(by: cell.bag)

        cell
            .onFilteredCategories
            .bind(to: onFilteredCategories)
            .disposed(by: cell.bag)

        cell
            .lastItemSelected
            .bind { [weak self] value in
                self?.itemSelected.accept(value)
            }
            .disposed(by: cell.bag)

        scrollToIndexPath
            .bind { [weak cell] value in
                cell?.scrollToIndexPath(value: value)
            }
            .disposed(by: bag)
    }
}
