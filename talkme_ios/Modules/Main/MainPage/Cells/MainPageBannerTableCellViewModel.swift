//
//  MainPageBannerTableCellViewModel.swift
//  talkme_ios
//
//  Created by VladimirCH on 24.03.2022.
//

import RxCocoa
import RxSwift

final class MainPageBannerTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let itemSelected = PublishRelay<IndexPath?>()
    let onButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()
    let scrollToIndexPath = PublishRelay<IndexPath?>()

    // MARK: - Private properties

    private var dataItems: [AnyCollectionViewCellModelProtocol]?
    private var banners: [BannerList] = []
    private var numberOfPages: Int = 0

    // MARK: Public Methods

    func setDataModel(bannerList: [BannerList]) {
        self.banners = bannerList
        numberOfPages = banners.count
        bannerList.forEach { _ in
            let items: [AnyCollectionViewCellModelProtocol]? = bannerList.map {
                let referenceLink = $0.advertiserLink != nil ?  $0.advertiserLink : ""
                let model = BannerImageCollectionCellViewModel(
                    bannerModel: UIScreen.isSE ? $0.bannerImg440 : $0.bannerImg,
                    referenceLink: referenceLink
                )
                return model
            }
            dataItems = items
        }
    }

    func configure(_ cell: MainPageBannerTableCell) {
        cell.setMainItems(dataItems: dataItems)
        cell.numberOfPages = numberOfPages

        cell
            .lastItemSelected
            .bind { [weak self] value in
                self?.itemSelected.accept(value)
            }
            .disposed(by: cell.bag)

        scrollToIndexPath
            .bind { [weak cell] value in
                cell?.scrollToIndexPath(value: value)
            }
            .disposed(by: bag)
    }
}
