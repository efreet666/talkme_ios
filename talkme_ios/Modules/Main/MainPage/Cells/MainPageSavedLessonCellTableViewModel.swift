//
//  MainPageSavedLessonCellViewModel.swift
//  talkme_ios
//
//  Created by nikita on 17.05.2022.
//

import RxCocoa
import RxSwift

final class MainPageSavedLessonTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties
    
    enum CauseOfUpdate {
        case periodicUpdate
        case newPageLoaded
    }

    let onLessonDetail = PublishRelay<(StreamdDetailModel)>()
    let showFilteredStreamsButtonTapped = PublishRelay<Void>()
    let avatarTapped = PublishRelay<String?>()
    let deleteButtonTapped = PublishRelay<Int>()
    let bag = DisposeBag()

    // MARK: - Private properties

    private var lessonService: LessonsServiceProtocol
    private var streamsCanBeDeleted: Bool = false
    private var dataItems = BehaviorRelay<([AnyCollectionViewCellModelProtocol], CauseOfUpdate)>(value: ([], .newPageLoaded))
    private var numberOfStreamsInOnePage = BehaviorRelay<Int>(value: 0)
    private(set) var lessons: [LiveStream] = []
    private var streamOwnersId: Int?
    private var timer: Timer?

    // MARK: - life cycle

    init(lessonService: LessonsServiceProtocol, numberOfStreamsInPage: Int, streamOwnersId: Int? = nil) {
        self.lessonService = lessonService
        self.numberOfStreamsInOnePage.accept(numberOfStreamsInPage)
        self.streamOwnersId = streamOwnersId
    }

    // MARK: Public Methods

    func setDataModel(lessons: [LiveStream], andShowDeleteButton deleteButtonIsShown: Bool) {
        self.lessons = lessons
        streamsCanBeDeleted = deleteButtonIsShown
        let items: [AnyCollectionViewCellModelProtocol] = lessons.map {
            let model = MainPageSavedLessonViewModel(lessonsModel: $0, showDeleteButton: deleteButtonIsShown)
            bindVM(model, classNumber: $0.owner.numberClass, lessonId: $0.id)
            return model
        }
        dataItems.accept((items, .newPageLoaded))
//        updateCollectinEveryMinute()
    }

    func configure(_ cell: MainPageSavedLessonTableCell) {
        cell
            .onLessonDetail
            .compactMap { [weak self] model in
                guard let lessons = self?.lessons.map(LiveStreamShort.init),
                      let index = lessons.firstIndex(where: { $0.id == model.id }) else { return nil }
                return StreamdDetailModel(allStreamsShort: lessons, currentStream: LiveStreamShort(model: model), index: index)
            }
            .bind(to: onLessonDetail)
            .disposed(by: cell.bag)

        cell
            .onButtonTapped
            .bind(to: showFilteredStreamsButtonTapped)
            .disposed(by: cell.bag)

        // we get true or false evry time user swipe the scree
        cell
            .rightPagingBoundRiched
            .distinctUntilChanged()
            .bind { [weak self] nextPageNumber in
                guard let self = self else { return }
                self.loadPage(withNumber: nextPageNumber)
            }
            .disposed(by: cell.bag)

        dataItems
            .bind(to: cell.dataItemsForCollectionView)
            .disposed(by: cell.bag)

        numberOfStreamsInOnePage
            .bind(to: cell.numberOfStreamsInPage)
            .disposed(by: cell.bag)
    }

    // MARK: - Private Methods

    private func bindVM(_ viewModel: MainPageSavedLessonViewModel, classNumber: String?, lessonId: Int) {
        viewModel
            .avatarTapped
            .map { classNumber }
            .bind(to: avatarTapped)
            .disposed(by: viewModel.bag)

        viewModel
            .deleteButtonTapped
            .map { lessonId }
            .bind(to: deleteButtonTapped)
            .disposed(by: viewModel.bag)
    }

    private func loadPage(withNumber pageNumber: Int) {
        lessonService.savedStreams(withOwnersId: streamOwnersId, page: pageNumber, count: numberOfStreamsInOnePage.value)
            .subscribe { [weak self] event in
                guard let self = self
                else { return }

                switch event {
                case .success(let response):
                    let streamsFromNewPage = response.data ?? []
                    let cellsFromRecievedStreams: [AnyCollectionViewCellModelProtocol] = streamsFromNewPage.map {
                        let model = MainPageSavedLessonViewModel(lessonsModel: $0, showDeleteButton: self.streamsCanBeDeleted)
                        self.bindVM(model, classNumber: $0.owner.numberClass, lessonId: $0.id)
                        return model
                    }
                    self.lessons += streamsFromNewPage
                    self.dataItems.accept((self.dataItems.value.0 + cellsFromRecievedStreams, .newPageLoaded))
                case .error:
                    break
                }
            }
            .disposed(by: bag)
    }

    private func updateCollectinEveryMinute() {
        timer?.invalidate()
        timer = Timer(timeInterval: 30, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.updateSavedLessons()
        }
        if let timerForUpdate = timer {
            RunLoop.main.add(timerForUpdate, forMode: .default)
        }
    }

    private func updateSavedLessons() {
        let countOfLessonsToDownload = lessons.count + numberOfStreamsInOnePage.value
        lessonService.savedStreams(withOwnersId: nil, page: 1, count: countOfLessonsToDownload)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let savedStreams):

                    let recievedStreams = savedStreams.data ?? []
                    let cellsFromRecievedStreams: [AnyCollectionViewCellModelProtocol] = recievedStreams.map {
                        let model = MainPageSavedLessonViewModel(lessonsModel: $0, showDeleteButton: self.streamsCanBeDeleted)
                        self.bindVM(model, classNumber: $0.owner.numberClass, lessonId: $0.id)
                        return model
                    }
                    self.lessons = recievedStreams
                    let newItemsForCollectionView = cellsFromRecievedStreams
                    self.dataItems.accept((newItemsForCollectionView, .periodicUpdate))
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }
}
