//
//  MainPageLiveTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 13.02.2021.
//

import RxSwift
import RxCocoa

fileprivate extension Consts {
    static let cellSize: CGSize = CGSize(width: UIScreen.isSE ? 190 : 223,
                                         height: UIScreen.isSE ? 267 : 313)
    static let minimumLineSpacing: CGFloat = UIScreen.isSE ? 10 : 17

    // dont push numbers more then 100 nd less then 0 to precentOfPageWidthWichIsALoadBorder
    static let precentOfPageWidthWichIsALoadBorder: Float = 70
}

final class MainPageLiveTableCell: UITableViewCell {

    // MARK: - Inputs

    private(set) var dataItemsForCollectionView = BehaviorRelay<([AnyCollectionViewCellModelProtocol],
                                                                 MainPageLiveTableCellViewModel.CauseOfUpdate)>(value: ([], .newPageLoaded))
    private(set) var numberOfStreamsInPage = BehaviorRelay<Int>(value: 0)

    // MARK: - Outputs

    lazy var onLessonDetail = lessonsCollectionView.rx.modelSelected(MainPageLiveViewModel.self).map { $0.lessonsModel }
    lazy var onButtonTapped = liveButton.rx.tap
    private(set) lazy var lastItemSelected = lessonsCollectionView.rx.itemSelected
    private(set) var rightPagingBoundRiched = PublishRelay<Int>()

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private var numberOfLastPage: Int = 1

    private let liveLessonsLabel: UILabel = {
        let label = UILabel()
        label.text = "main_page_live_lessons_streams".localized
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let lessonsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = Consts.cellSize
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = Consts.minimumLineSpacing
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.showsHorizontalScrollIndicator = false
        cvc.contentInset = UIEdgeInsets(top: 0, left: UIScreen.isSE ? 10 : 12, bottom: 0, right: 0)
        cvc.registerCells(withModels: MainPageLiveViewModel.self)
        return cvc
    }()

    private let liveButton: UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: UIScreen.isSE ? "mainPageNext" : "bigBlueNext"), for: .normal)
        return bt
    }()

    private func culculateOffsetValueForLoadingNextPage(from itemsCount: Int, and numberOfStreamsInPage: Int) -> CGFloat {
        guard numberOfStreamsInPage > 0
        else { return CGFloat(0) }

        let streamsItemWidth = Float(Consts.cellSize.width + Consts.minimumLineSpacing)
        var numberOfStreamsInLastPage = Float(itemsCount % numberOfStreamsInPage)

        if numberOfStreamsInLastPage == 0 {
            numberOfStreamsInLastPage = Float(numberOfStreamsInPage)
        }

        let lengthBeforeLastPage = streamsItemWidth * (Float(itemsCount) - numberOfStreamsInLastPage)
        let lengthOfLastPage = streamsItemWidth * numberOfStreamsInLastPage

        let pagingBorderInLastPage = lengthOfLastPage * ( Consts.precentOfPageWidthWichIsALoadBorder / Float(100) )
        return CGFloat(lengthBeforeLastPage + pagingBorderInLastPage)
    }

    private func makeNumberOfLastPage(from itemsCount: Int, and numberOfStreamsInPage: Int) -> Int {
        let result = Int(ceil(Float(itemsCount) / Float(numberOfStreamsInPage)))
        return result
    }

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
        bindUIElements()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
        bindUIElements()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]?) {
       guard let mainItems = dataItems else { return }
        Observable.just(mainItems)
            .bind(to: lessonsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([liveLessonsLabel, lessonsCollectionView, liveButton])

        liveLessonsLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 18 : 16)
            make.leading.equalToSuperview().offset(10)
            make.trailing.greaterThanOrEqualTo(liveButton.snp.leading).inset(24)
        }

        liveButton.snp.makeConstraints { make in
            make.centerY.equalTo(liveLessonsLabel)
            make.trailing.equalToSuperview().inset(-5)
            make.width.height.equalTo(40)
        }

        lessonsCollectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(liveLessonsLabel.snp.bottom).offset(UIScreen.isSE ? 11 : 21)
            make.height.equalTo(UIScreen.isSE ? 267 : 313)
        }
    }

    private func bindUIElements() {
        let rightPagingBoundObserver = Observable
            .combineLatest(dataItemsForCollectionView, numberOfStreamsInPage)
            .filter { $0.1 != 0 }
            .map { [weak self] (itemsArrayAndCauseOfUpdate, numberOfStreamsInPage) -> CGFloat in
                self?.numberOfLastPage = self?.makeNumberOfLastPage(from: itemsArrayAndCauseOfUpdate.0.count, and: numberOfStreamsInPage) ?? 0
                return self?.culculateOffsetValueForLoadingNextPage(from: itemsArrayAndCauseOfUpdate.0.count, and: numberOfStreamsInPage) ?? 0
            }

        Observable
            .combineLatest(lessonsCollectionView.rx.contentOffset, rightPagingBoundObserver)
            .filter { [weak self] contentOffset, rightPagingBoundInPoints -> Bool in
                guard let self = self,
                      rightPagingBoundInPoints != 0
                else { return false}
                return contentOffset.x > rightPagingBoundInPoints
            }
            .map { $0.1 }
            .distinctUntilChanged()
            .map { [weak self] _ -> Int in
                guard let self = self else { return 1}
                self.numberOfLastPage += 1
                return self.numberOfLastPage
            }
            .bind(to: rightPagingBoundRiched)
            .disposed(by: bag)

        dataItemsForCollectionView
            .do { [weak self] items, causeOfUpdate in
                switch causeOfUpdate {
                case .newPageLoaded:
                    // change contentOffset here if you whant
                    break
                case .periodicUpdate:
                    self?.lessonsCollectionView.contentOffset.x = 0
                    break
                }
            }
            .map { dataItemsForCells, _ in dataItemsForCells }
            .bind(to: lessonsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }
}
