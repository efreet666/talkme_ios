//
//  MainPageSavedLessonCell.swift
//  talkme_ios
//
//  Created by nikita on 17.05.2022.
//

import RxSwift
import RxCocoa
import CoreGraphics

fileprivate extension Consts {
    static let cellSize: CGSize = CGSize(width: UIScreen.isSE ? 183 : 183,
                                         height: UIScreen.isSE ? 222 : 291)
    static let minimumLineSpacing: CGFloat = UIScreen.isSE ? 10 : 12

    // dont push numbers more then 100 nd less then 0 to precentOfPageWidthWichIsALoadBorder
    static let precentOfPageWidthWichIsALoadBorder: Float = 70
}

final class MainPageSavedLessonTableCell: UITableViewCell {

    // MARK: - Inputs

    private(set) var dataItemsForCollectionView = BehaviorRelay<([AnyCollectionViewCellModelProtocol],
                                                                 MainPageSavedLessonTableCellViewModel.CauseOfUpdate)>(value: ([], .newPageLoaded))
    private(set) var numberOfStreamsInPage = BehaviorRelay<Int>(value: 0)

    // MARK: - Outputs

    lazy var onLessonDetail = lessonsCollectionView.rx.modelSelected(MainPageSavedLessonViewModel.self).map { $0.lessonsModel }
    private(set) lazy var lastItemSelected = lessonsCollectionView.rx.itemSelected
    lazy var onButtonTapped = savedButton.rx.tap
    private(set) var rightPagingBoundRiched = PublishRelay<Int>()

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private var numberOfLastPage = 1

    private let savedLessonsLabel: UILabel = {
        let label = UILabel()
        label.text = "main_page_saved_lessons_streams".localized
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let lessonsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = Consts.cellSize
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = Consts.minimumLineSpacing
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.contentInset = UIEdgeInsets(top: 0, left: UIScreen.isSE ? 10 : 12, bottom: 0, right: UIScreen.isSE ? 10 : 12)
        cvc.showsHorizontalScrollIndicator = false
        cvc.registerCells(withModels: MainPageSavedLessonViewModel.self)
        return cvc
    }()

    private let savedButton: UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: UIScreen.isSE ? "mainPageNext" : "bigBlueNext"), for: .normal)
        return bt
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
        bindUIElements()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
        bindUIElements()
    }

    // MARK: - Private method

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([savedLessonsLabel, lessonsCollectionView, savedButton])

        savedLessonsLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 23 : 28)
            make.leading.equalToSuperview().offset(10)
            make.trailing.lessThanOrEqualToSuperview().offset(-10)
        }

        savedButton.snp.makeConstraints { make in
            make.centerY.equalTo(savedLessonsLabel)
            make.trailing.equalToSuperview().inset(-5)
            make.width.height.equalTo(40)
        }

        lessonsCollectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(savedLessonsLabel.snp.bottom).offset(UIScreen.isSE ? 16 : 20)
            make.height.equalTo(UIScreen.isSE ? 222 : 291)
        }
    }

    private func culculateOffsetValueForLoadingNextPage(from itemsCount: Int, and numberOfStreamsInPage: Int) -> CGFloat {
        guard numberOfStreamsInPage > 0
        else { return CGFloat(0) }

        let streamsItemWidth = Float(Consts.cellSize.width + Consts.minimumLineSpacing)
        var numberOfStreamsInLastPage = Float(itemsCount % numberOfStreamsInPage)

        if numberOfStreamsInLastPage == 0 {
            numberOfStreamsInLastPage = Float(numberOfStreamsInPage)
        }

        let lengthBeforeLastPage = streamsItemWidth * (Float(itemsCount) - numberOfStreamsInLastPage)
        let lengthOfLastPage = streamsItemWidth * numberOfStreamsInLastPage

        let pagingBorderInLastPage = lengthOfLastPage * ( Consts.precentOfPageWidthWichIsALoadBorder / Float(100) )
        return CGFloat(lengthBeforeLastPage + pagingBorderInLastPage)
    }

    private func makeNumberOfLastPage(from itemsCount: Int, and numberOfStreamsInPage: Int) -> Int {
        let result = Int(ceil(Float(itemsCount) / Float(numberOfStreamsInPage)))
        return result
    }

    private func bindUIElements() {

        let rightPagingBoundObserver = Observable
            .combineLatest(dataItemsForCollectionView, numberOfStreamsInPage)
            .filter { $0.1 != 0 }
            .map { [weak self] (itemsArrayAndCauseOfUpdate, numberOfStreamsInPage) -> CGFloat in
                self?.numberOfLastPage = self?.makeNumberOfLastPage(from: itemsArrayAndCauseOfUpdate.0.count, and: numberOfStreamsInPage) ?? 0
                return self?.culculateOffsetValueForLoadingNextPage(from: itemsArrayAndCauseOfUpdate.0.count, and: numberOfStreamsInPage) ?? 0
            }

        Observable
            .combineLatest(lessonsCollectionView.rx.contentOffset, rightPagingBoundObserver)
            .filter { [weak self] contentOffset, rightPagingBoundInPoints -> Bool in
                guard let self = self,
                      rightPagingBoundInPoints != 0
                else { return false}
                return contentOffset.x > rightPagingBoundInPoints
            }
            .map { $0.1 }
            .distinctUntilChanged()
            .map { [weak self] _ -> Int in
                guard let self = self else { return 1}
                self.numberOfLastPage += 1
                return self.numberOfLastPage
            }
            .bind(to: rightPagingBoundRiched)
            .disposed(by: bag)

        dataItemsForCollectionView
            .do { [weak self] items, causeOfUpdate in
                switch causeOfUpdate {
                case .newPageLoaded:
                    // change contentOffset here if you whant
                    break
                case .periodicUpdate:
                    self?.lessonsCollectionView.contentOffset.x = 0
                    break
                }
            }
            .map { dataItemsForCells, _ in dataItemsForCells }
            .bind(to: lessonsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }
}
