//
//  MainPageNearestLessonsCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 08.02.2021.
//

import RxCocoa
import RxSwift

final class MainPageNearestLessonsCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let modelSelected = PublishRelay<LiveStream>()
    let bag = DisposeBag()
    let onButtonTap = PublishRelay<Void>()
    let itemSelected = PublishRelay<IndexPath?>()
    let scrollToIndexPath = PublishRelay<IndexPath?>()
    let onAvatarTap = PublishRelay<String?>()

    // MARK: - Private properties

    private var collectionTitle = "main_page_nearest_lessons_streams".localized
    private var dataItems: [AnyCollectionViewCellModelProtocol]?

    // MARK: Public Methods

    func setDataModel(lessons: [LiveStream], collectionTitle: String? = nil) {
        let items: [AnyCollectionViewCellModelProtocol] = lessons.map {
            let model = MainPageNearestCollectionViewModel(lessonsModel: $0)
            bindVM(model, classNumber: $0.owner.numberClass)
            return model
        }
        dataItems = items
        if let title = collectionTitle {
            self.collectionTitle = title
        }
    }

    func configure(_ cell: MainPageNearestLessonCell) {
        cell.setMainItems(dataItems: dataItems, title: collectionTitle)

        cell.modelSelected
            .map({ $0.lessonsModel })
            .bind(to: modelSelected)
            .disposed(by: cell.bag)

        cell
            .lastItemSelected
            .bind { [weak self] value in
                self?.itemSelected.accept(value)
            }
            .disposed(by: cell.bag)

        scrollToIndexPath
            .bind { [weak cell] value in
                cell?.scrollToIndexPath(value: value)
            }
            .disposed(by: bag)

        cell
            .onButtonTapped
            .bind(to: onButtonTap)
            .disposed(by: cell.bag)

    }

    // MARK: - Private Methods

    private func bindVM(_ viewModel: MainPageNearestCollectionViewModel, classNumber: String?) {
        viewModel
            .onAvatarTap
            .map { classNumber }
            .bind(to: onAvatarTap)
            .disposed(by: viewModel.bag)
    }
}
