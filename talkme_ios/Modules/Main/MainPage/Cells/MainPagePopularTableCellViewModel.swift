//
//  MainPagePopularTableCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 10.02.2021.
//

import RxCocoa
import RxSwift

final class MainPagePopularTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let bag = DisposeBag()
    let onLessonDetail = PublishRelay<LiveStream>()
    let onButtonTapped = PublishRelay<Void>()
    let onAvatarTap = PublishRelay<String?>()
    let itemSelected = PublishRelay<IndexPath?>()
    let scrollToIndexPath = PublishRelay<IndexPath?>()

    // MARK: - Private properties

    private var dataItems: [AnyCollectionViewCellModelProtocol]?

    // MARK: Public Methods

    func setDataModel(lessons: [LiveStream]) {
        let items: [AnyCollectionViewCellModelProtocol] = lessons.map {
            let model = MainPagePopularViewModel(lessonsModel: $0)
            bindVM(model, classNumber: $0.owner.numberClass)
            return model
        }
        dataItems = items
    }

    func configure(_ cell: MainPagePopularTableCell) {
        cell.setMainItems(dataItems: dataItems)

        cell
            .onLessonDetail
            .bind(to: onLessonDetail)
            .disposed(by: cell.bag)

        cell
            .onButtonTapped
            .bind(to: onButtonTapped)
            .disposed(by: cell.bag)

        cell
            .lastItemSelected
            .bind { [weak self] value in
                self?.itemSelected.accept(value)
            }
            .disposed(by: cell.bag)

        scrollToIndexPath
            .bind { [weak cell] value in
                cell?.scrollToIndexPath(value: value)
            }
            .disposed(by: bag)
    }

    private func bindVM(_ viewModel: MainPagePopularViewModel, classNumber: String?) {
        viewModel
            .onAvatarTap
            .map { classNumber }
            .bind(to: onAvatarTap)
            .disposed(by: viewModel.bag)
    }
}
