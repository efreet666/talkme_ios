//
//  MainPageCategoriesTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 11.02.2021.
//

import RxSwift
import RxCocoa

final class MainPageCategoriesTableCell: UITableViewCell {

    // MARK: - Public Properties

    var bag = DisposeBag()
    lazy var onButtonTapped = categoryButton.rx.tap
    let onAllCategories = PublishRelay<Void>()
    let onFilteredCategories = PublishRelay<LessonCategoryType>()
    private(set) lazy var lastItemSelected = categoryCollectionView.rx.itemSelected

    // MARK: - Private properties

    private let categoryLabel: UILabel = {
        let label = UILabel()
        label.text = "main_page_category_lessons_streams".localized
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let categoryCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 21
        layout.itemSize = CGSize(width: UIScreen.isSE ? 52 : 70, height: UIScreen.isSE ? 68 : 90)
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.showsHorizontalScrollIndicator = false
        cvc.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        cvc.registerCells(withModels: MainPageCategoriesViewModel.self)
        return cvc
    }()

    private let categoryButton: UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: UIScreen.isSE ? "mainPageNext" : "bigBlueNext"), for: .normal)
        return bt
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]) {
        Observable.just(dataItems)
            .bind(to: categoryCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
        bindUI()
    }

    func scrollToIndexPath(value: IndexPath?) {
        guard let value = value else { return }
        categoryCollectionView.scrollToItem(at: value, at: .top, animated: true)
    }

    // MARK: - Private Methods

    private func bindUI() {
        categoryCollectionView.rx
            .modelSelected(MainPageCategoriesViewModel.self)
            .bind { [weak self] item in
                guard let self = self else { return }
                if item.categoryModel == .all {
                    self.onAllCategories.accept(())
                } else {
                    self.onFilteredCategories.accept(item.categoryModel)
                }
            }
            .disposed(by: bag)
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([categoryLabel, categoryCollectionView, categoryButton])

        categoryLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 23 : 25)
            make.leading.equalToSuperview().offset(10)
            make.trailing.greaterThanOrEqualTo(categoryButton.snp.leading).inset(24)
        }

        categoryButton.snp.makeConstraints { make in
            make.centerY.equalTo(categoryLabel)
            make.trailing.equalToSuperview().inset(-5)
            make.width.height.equalTo(40)
        }

        categoryCollectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(categoryLabel.snp.bottom).inset(UIScreen.isSE ? -15 : -16)
            make.height.equalTo(UIScreen.isSE ? 68 : 90)
        }
    }
}
