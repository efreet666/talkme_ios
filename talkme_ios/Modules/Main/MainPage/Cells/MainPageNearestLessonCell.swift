//
//  MainPageNearestLessonCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 08.02.2021.
//

import RxSwift
import RxCocoa

final class MainPageNearestLessonCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var modelSelected = lessonsCollectionView.rx.modelSelected(MainPageNearestCollectionViewModel.self)
    private(set) lazy var lastItemSelected = lessonsCollectionView.rx.itemSelected
    private(set) lazy var onButtonTapped = nearestButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let nearestLessonsLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let lessonsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.isSE ? 132 : 163, height: UIScreen.isSE ? 222 : 291)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = UIScreen.isSE ? 10 : 12
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.contentInset = UIEdgeInsets(top: 0, left: UIScreen.isSE ? 10 : 12, bottom: 0, right: 0)
        cvc.showsHorizontalScrollIndicator = false
        cvc.registerCells(withModels: MainPageNearestCollectionViewModel.self)
        return cvc
    }()

    private let nearestButton: UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: UIScreen.isSE ? "mainPageNext" : "bigBlueNext"), for: .normal)
        return bt
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]?, title: String) {
       guard let mainItems = dataItems else { return }
        Observable.just(mainItems)
            .bind(to: lessonsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
        nearestLessonsLabel.text = title
    }

    func scrollToIndexPath(value: IndexPath?) {
        guard let value = value else { return }
        lessonsCollectionView.scrollToItem(at: value, at: .top, animated: true)
    }

    // MARK: - Private Methods

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([nearestLessonsLabel, lessonsCollectionView, nearestButton])

        nearestLessonsLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 23 : 28)
            make.leading.equalToSuperview().offset(10)
            make.trailing.lessThanOrEqualToSuperview().offset(-10)
        }

        nearestButton.snp.makeConstraints { make in
            make.centerY.equalTo(nearestLessonsLabel)
            make.trailing.equalToSuperview().inset(-5)
            make.width.height.equalTo(40)
        }

        lessonsCollectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(nearestLessonsLabel.snp.bottom).offset(UIScreen.isSE ? 16 : 20)
            make.height.equalTo(UIScreen.isSE ? 222 : 291)
        }
    }
}
