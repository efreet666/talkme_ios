//
//  MainPageLiveTableCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 13.02.2021.
//

import RxCocoa
import RxSwift

final class MainPageLiveTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties
    
    enum CauseOfUpdate {
        case periodicUpdate
        case newPageLoaded
    }

    let onLessonDetail = PublishRelay<(StreamdDetailModel)>()
    let onButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private properties

    private var lessonService: LessonsServiceProtocol
    private var dataItems = BehaviorRelay<([AnyCollectionViewCellModelProtocol], CauseOfUpdate)>(value: ([], .newPageLoaded))
    private var numberOfStreamsInOnePage = BehaviorRelay<Int>(value: 0)
    private(set) var lessons: [LiveStream] = []
    private var numberOfLastPage: Int = 1
    private var timer: Timer?

    // MARK: - life cycle

    init(lessonService: LessonsServiceProtocol, numberOfStreamsInPage: Int) {
        self.lessonService = lessonService
        self.numberOfStreamsInOnePage.accept(numberOfStreamsInPage)
    }

    // MARK: Public Methods

    func setDataModel(lessons: [LiveStream]) {
        self.lessons = lessons
        let items: [AnyCollectionViewCellModelProtocol] = lessons.map {
            let model = MainPageLiveViewModel(lessonsModel: $0)
            return model
        }
        dataItems.accept((items, .newPageLoaded))
//        updateCollectinEveryMinute()
    }

    func configure(_ cell: MainPageLiveTableCell) {
        cell
            .onLessonDetail
            .compactMap { [weak self] model in
                guard let lessons = self?.lessons.map(LiveStreamShort.init),
                      let index = lessons.firstIndex(where: { $0.id == model.id }) else { return nil }
                return StreamdDetailModel(allStreamsShort: lessons, currentStream: LiveStreamShort(model: model), index: index)
            }
            .bind(to: onLessonDetail)
            .disposed(by: cell.bag)

        cell
            .onButtonTapped
            .bind(to: onButtonTap)
            .disposed(by: cell.bag)

        cell
            .rightPagingBoundRiched
            .distinctUntilChanged()
            .bind { [weak self] nextPageNumber in
                guard let self = self else { return }
                self.loadPage(withNumber: nextPageNumber)
            }
            .disposed(by: cell.bag)

        dataItems
            .bind(to: cell.dataItemsForCollectionView)
            .disposed(by: cell.bag)

        numberOfStreamsInOnePage
            .bind(to: cell.numberOfStreamsInPage)
            .disposed(by: cell.bag)
    }

    private func loadPage(withNumber pageNumber: Int) {
        lessonService.live(pageNumber: pageNumber, lessonsInPage: numberOfStreamsInOnePage.value)
            .subscribe { [weak self] event in
                guard let self = self
                else { return }

                switch event {
                case .success(let response):
                    let streamsFromNewPage = response.data ?? []
                    let cellsFromRecievedStreams: [AnyCollectionViewCellModelProtocol] = streamsFromNewPage.map {
                        let model = MainPageLiveViewModel(lessonsModel: $0)
                        return model
                    }
                    self.lessons += streamsFromNewPage
                    let newItemsForCollectionView = self.dataItems.value.0 + cellsFromRecievedStreams
                    self.dataItems.accept((newItemsForCollectionView, .newPageLoaded))
                    self.numberOfLastPage = pageNumber
                case .error:
                    break
                }
            }
            .disposed(by: bag)
    }

    private func updateCollectinEveryMinute() {
        timer?.invalidate()
        timer = Timer(timeInterval: 30, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.updateLiveLessons()
        }
        if let timerForUpdate = timer {
            RunLoop.main.add(timerForUpdate, forMode: .default)
        }
    }

    private func updateLiveLessons() {
        let countOfLessonsToDownload = lessons.count + numberOfStreamsInOnePage.value
        lessonService.live(pageNumber: 1, lessonsInPage: countOfLessonsToDownload)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let liveResponse):

                    let recievedStreams = liveResponse.data ?? []
                    let cellsFromRecievedStreams: [AnyCollectionViewCellModelProtocol] = recievedStreams.map {
                        let model = MainPageLiveViewModel(lessonsModel: $0)
                        return model
                    }
                    self.lessons = recievedStreams
                    let newItemsForCollectionView = cellsFromRecievedStreams
                    self.dataItems.accept((newItemsForCollectionView, .periodicUpdate))
                    self.numberOfLastPage = 1
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }
}
