//
//  MainPagePopularTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 10.02.2021.
//

import RxSwift
import RxCocoa

final class MainPagePopularTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    lazy var onLessonDetail = lessonsCollectionView.rx .modelSelected(MainPagePopularViewModel.self).map { $0.lessonsModel }
    lazy var onButtonTapped = popularButton.rx.tap
    private(set) lazy var lastItemSelected = lessonsCollectionView.rx.itemSelected

    // MARK: - Private properties

    private let popularLessonsLabel: UILabel = {
        let label = UILabel()
        label.text = "main_page_popular_lessons_streams".localized
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let lessonsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.isSE ? 173 : 222, height: UIScreen.isSE ? 244 : 312)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 12
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.showsHorizontalScrollIndicator = false
        cvc.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        cvc.registerCells(withModels: MainPagePopularViewModel.self)
        return cvc
    }()

    private let popularButton: UIButton = {
        let bt = UIButton()
        bt.setImage(UIImage(named: UIScreen.isSE ? "mainPageNext" : "bigBlueNext"), for: .normal)
        return bt
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]?) {
       guard let mainItems = dataItems else { return }
        Observable.just(mainItems)
            .bind(to: lessonsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    func scrollToIndexPath(value: IndexPath?) {
        guard let value = value else { return }
        lessonsCollectionView.scrollToItem(at: value, at: .top, animated: true)
    }

    // MARK: - Private Methods

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([popularLessonsLabel, lessonsCollectionView, popularButton])

        popularLessonsLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(25)
            make.leading.equalToSuperview().offset(10)
            make.trailing.greaterThanOrEqualTo(popularButton.snp.leading).inset(24)
        }

        popularButton.snp.makeConstraints { make in
            make.centerY.equalTo(popularLessonsLabel)
            make.trailing.equalToSuperview().inset(-5)
            make.width.height.equalTo(40)
        }

        lessonsCollectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(popularLessonsLabel.snp.bottom).offset(UIScreen.isSE ? 16 : 19)
            make.height.equalTo(UIScreen.isSE ? 244 : 312)
        }
    }
}
