//
//  MainPageBannerTableCell.swift
//  talkme_ios
//
//  Created by VladimirCH on 24.03.2022.
//

import RxSwift
import RxCocoa

final class MainPageBannerTableCell: UITableViewCell {

    private enum Constants {
        static let collectionImageSize = CGSize(width: widthCollectionCell, height: widthCollectionCell * 7 / 12)
        static let imageCollectionLineSpasing: CGFloat = UIScreen.isSE ? 12 : 14
        static let widthCollectionCell: CGFloat = (UIScreen.main.bounds.size.width - imageCollectionLineSpasing )
        static let scrollWidthCellTimer: CGFloat = (UIScreen.main.bounds.size.width)
    }

    // MARK: - Public Properties

    var numberOfPages: Int = 0 {
        didSet {
            paginationView.numberOfPages = numberOfPages
        }
    }

    // MARK: - Private properties

    private(set) lazy var onBannerDetail = imagesCollection.rx.modelSelected(BannerImageCollectionCellViewModel.self).map { $0.bannerModel }
    private(set) lazy var lastItemSelected = imagesCollection.rx.itemSelected
    private(set) var bag = DisposeBag()

    private let paginationView = PaginationView()
    private var timer: Timer?

    private let imagesCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = Constants.collectionImageSize
        layout.minimumLineSpacing = Constants.imageCollectionLineSpasing
        layout.sectionInset = UIEdgeInsets(
            top: 0,
            left: Constants.imageCollectionLineSpasing/2,
            bottom: 0,
            right: Constants.imageCollectionLineSpasing/2
        )
        layout.scrollDirection = .horizontal
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.isPagingEnabled = true
        cvc.showsHorizontalScrollIndicator = false
        cvc.registerCells(withModels: BannerImageCollectionCellViewModel.self)
        return cvc
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        imagesCollection.delegate = self
        paginationView.numberOfPages = 1
        paginationView.currentPage = 0
        cellStyle()
        setupLayout()
        startTimer()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]?) {
        guard let mainItems = dataItems else { return }
        Observable.just(mainItems)
            .bind(to: imagesCollection.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews(imagesCollection, paginationView)
        imagesCollection.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 10 : 12)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 172 : 228)
            make.bottom.equalToSuperview()
        }

        paginationView.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-12)
            make.centerX.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 2 : 3)
        }
    }

    private func startTimer() {
            timer?.invalidate()
            timer = nil
            timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
        }

        @objc private func scrollToNextCell() {
            paginationView.currentPage += 1
            var animated = true
            if paginationView.currentPage >= paginationView.numberOfPages {
                paginationView.currentPage = 0
                imagesCollection.scrollRectToVisible(
                    CGRect(x: 0, y: 0, width: Constants.collectionImageSize.width, height: Constants.collectionImageSize.height),
                    animated: animated
                )
                animated = false
                startTimer()
            }
            imagesCollection.scrollRectToVisible(
                CGRect(x: Constants.scrollWidthCellTimer * CGFloat(paginationView.currentPage) + Constants.imageCollectionLineSpasing,
                y: 0, width: Constants.collectionImageSize.width, height: Constants.collectionImageSize.height),
                animated: animated
            )
        }

}

extension MainPageBannerTableCell: UICollectionViewDelegate {
    func scrollToIndexPath(value: IndexPath?) {
        guard let value = value else { return }
        imagesCollection.scrollToItem(at: value, at: .top, animated: true)
    }

    func scrollViewWillEndDragging(
        _ scrollView: UIScrollView,
        withVelocity velocity: CGPoint,
        targetContentOffset: UnsafeMutablePointer<CGPoint>
    ) {
        let page = Int(targetContentOffset.pointee.x / (Constants.widthCollectionCell))
        paginationView.currentPage = page
    }
}

