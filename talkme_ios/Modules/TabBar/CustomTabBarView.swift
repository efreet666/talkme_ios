//
//  CustomTabBarImageView.swift
//  talkme_ios
//
//  Created by 1111 on 28.12.2020.
//

import RxSwift
import RxCocoa
import Kingfisher

final class CustomTabBarView: UIView {

    enum Flow {
        case home
        case search
        case live(streamsList: [LiveStream])
        case chat
        case profile
    }

    // MARK: - Outputs

    let flow = PublishRelay<Flow>()
    let onItemTapped = PublishRelay<Int>()


    // MARK: - Inputs

    let profileTabImage = PublishRelay<UIImage?>()
    let liveLessonsList = BehaviorRelay<[LiveStream]>(value: [])

    // MARK: - Public properties

    let bag = DisposeBag()

    // MARK: - Private properties

    private var buttons: [UIButton] = []
    private let invisibleHeight: CGFloat = 38
    private var shapeLayer: CALayer?
    private var activeItem: Int = 0
    private let gradient = CAGradientLayer(
        start: .streamStart,
        end: .streamEnd,
        colors: [TalkmeColors.deepBlue.cgColor, TalkmeColors.blueLabels.cgColor],
        locations: [0, 1])

    private var liveButton: UIButton? {
        return buttons[2]
    }

    // MARK: - Initializers

    convenience init(menuItems: [TabItem]) {
        self.init(frame: .zero)
        layer.addSublayer(gradient)
        setupTabItems(menuItems: menuItems)
        isUserInteractionEnabled = true
        setNeedsLayout()
        layoutIfNeeded()
        activateTab(tab: 0)
        buttons[0].isSelected = true
    }

    // MARK: Public Methods

    func hideLiveButton(_ isHidden: Bool) {
        guard let liveButton = liveButton else { return }
        liveButton.isHidden = isHidden
    }

    func activateProfileTab() {
        self.deselectAll()
        self.switchTab(to: HideChats.chatIsHidden ? 3 : 4)
    }

    // MARK: - Private Methods

    override func draw(_ rect: CGRect) {
        self.addShapeLayer(rect)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = bounds
    }

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let pointX = point.x
        let pointY = point.y
        let width = UIScreen.main.bounds.width
        guard let liveButton = liveButton else { return false }
        var isPoint = false

        if liveButton.isHidden {
            let isAreaTap = (pointX >= 0 && pointX <= self.frame.maxX) && (pointY < invisibleHeight)
            if pointY < 0 {
                isPoint = false
            } else if isAreaTap {
                isPoint =  false
            } else {
                isPoint =  true
            }
        } else {
            let isPointFirstRect = (pointX >= 0 && pointX < liveButton.frame.origin.x) && (pointY < invisibleHeight)
            let isPointTwoRect = (pointX > liveButton.frame.maxX && pointX <= width) && (pointY < invisibleHeight)
            let isAreaTap = (pointX >= liveButton.frame.origin.x && pointX <= liveButton.frame.maxX) && (pointY < liveButton.frame.origin.y)
            if pointY < 0 {
                isPoint = false
            } else if isPointFirstRect {
                isPoint =  false
            } else if isPointTwoRect {
                isPoint =  false
            } else if isAreaTap {
                isPoint =  false
            } else {
                isPoint =  true
            }
        }
        return isPoint
    }

    private func addShapeLayer(_ rect: CGRect) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = createPath(rect)
        self.shapeLayer = shapeLayer
        gradient.mask = shapeLayer
    }

    private func createPath(_ rect: CGRect) -> CGPath {
        let height: CGFloat = 30.0
        let path = UIBezierPath()
        let centerWidth = rect.width / 2

        path.move(to: CGPoint(x: 0, y: invisibleHeight))
        path.addLine(to: CGPoint(x: (centerWidth - height * 2), y: invisibleHeight))
        path.addCurve(to: CGPoint(x: centerWidth, y: height + invisibleHeight),
                      controlPoint1: CGPoint(x: (centerWidth - height), y: invisibleHeight),
                      controlPoint2: CGPoint(x: centerWidth - 35, y: height + invisibleHeight))
        path.addCurve(to: CGPoint(x: (centerWidth + height * 2), y: invisibleHeight),
                      controlPoint1: CGPoint(x: centerWidth + 35, y: height + invisibleHeight),
                      controlPoint2: CGPoint(x: (centerWidth + height), y: invisibleHeight))
        path.addLine(to: CGPoint(x: rect.width, y: invisibleHeight))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height))
        path.addLine(to: CGPoint(x: 0, y: rect.height))
        path.close()

        return path.cgPath
    }

    private func setupTabItems(menuItems: [TabItem]) {
        for item in 0 ..< menuItems.count {
            let centerXAnchor = (UIScreen.main.bounds.width / 5) * (CGFloat(item) + 1) - (UIScreen.main.bounds.width / 9.6)
            let tabBarItem = self.createTabItem(item: menuItems[item])
            let tabBarItemEdgeInsets = UIScreen.isSE
                ? UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
                : UIEdgeInsets(top: 9.5, left: 9.5, bottom: 9.5, right: 9.5)
            tabBarItem.tag = item
            tabBarItem.translatesAutoresizingMaskIntoConstraints = false
            tabBarItem.clipsToBounds = true
            self.addSubview(tabBarItem)
            self.buttons.append(tabBarItem)
            if tabBarItem.tag == 2 {
                tabBarItem.snp.makeConstraints { make in
                    make.width.equalToSuperview().multipliedBy(0.15)
                    make.height.equalTo(tabBarItem.snp.width)
                    make.centerX.equalToSuperview()
                    make.bottom.equalToSuperview().offset(-45)
                }
            } else if tabBarItem.tag == (HideChats.chatIsHidden ? 3 : 4) {
                tabBarItem.contentEdgeInsets = UIEdgeInsets(top: 8.5, left: 8.5, bottom: 8.5, right: 8.5)
                tabBarItem.snp.makeConstraints { make in
                    make.size.equalTo(44)
                    make.centerX.equalTo(centerXAnchor)
                    make.centerY.equalTo(snp.bottom).offset(UIScreen.isSE ? -46 : -35)
                }
                tabBarItem.imageView?.layer.cornerRadius = 13.5
            } else {
                tabBarItem.contentEdgeInsets = tabBarItemEdgeInsets
                tabBarItem.snp.makeConstraints { make in
                    make.size.equalTo(44)
                    make.centerX.equalTo(centerXAnchor)
                    make.centerY.equalTo(snp.bottom).offset(UIScreen.isSE ? -46 : -35)
                }
            }
        }
    }

    private func createTabItem(item: TabItem) -> UIButton {
        let tabBarButton = UIButton(frame: CGRect.zero)
        tabBarButton.isUserInteractionEnabled = true
        if item == .profile {
            setupProfileTabItem(button: tabBarButton, item: .profile)
        } else {
            tabBarButton.setImage(item.icon, for: .normal)
            tabBarButton.setImage(item.iconForActive, for: .selected)
        }
        tabBarButton.contentHorizontalAlignment = .fill
        tabBarButton.contentVerticalAlignment = .fill
        tabBarButton.contentMode = .scaleAspectFit
        tabBarButton.translatesAutoresizingMaskIntoConstraints = false
        tabBarButton.clipsToBounds = true
        bindButton(tabBarButton)
        return tabBarButton
    }

    private func setupProfileTabItem(button: UIButton, item: TabItem) {
        profileTabImage
            .bind { [weak button] image in
                button?.setImage(image, for: .normal)
                button?.imageView?.contentMode = .scaleAspectFill
        }
        .disposed(by: bag)
    }

    private func bindButton(_ sender: UIButton) {
        sender.rx.tap
            .do(onNext: { [weak sender, weak self] in
                guard let sender = sender
                else { return }

                self?.switchTab(to: sender.tag)
            })
            .compactMap { [weak sender, weak self] _ -> Flow? in
                guard let sender = sender,
                let self = self
                else { return nil }

                self.deselectAll()
                sender.isSelected = true
                switch sender.tag {
                case 0:
                    return Flow.home
                case 1:
                    return Flow.search
                case 2:
                    return Flow.live(streamsList: self.liveLessonsList.value)
                case 3:
                    return Flow.chat
                case 4:
                    return Flow.profile
                default:
                    return Flow.home
                }
            }
            .bind(to: flow)
            .disposed(by: bag)
    }

    private func switchTab(to: Int) {
        self.activateTab(tab: to)
    }

    private func activateTab(tab: Int) {
        onItemTapped.accept(tab)
        self.activeItem = tab
    }

    private func deselectAll() {
        buttons.forEach {
            $0.isSelected = false
        }
    }
}
