//
//  TabBarController.swift
//  talkme_ios
//
//  Created by 1111 on 28.12.2020.
//

import RxSwift
import RxCocoa
import Kingfisher

final class TabBarController: UITabBarController {

    // MARK: - Inputs

    let profileTabImage = PublishRelay<String?>()
    let liveLessonsList = PublishSubject<[LiveStream]>()

    // MARK: - Public properties

    let bag = DisposeBag()

    // MARK: - Private properties

    private let allChatsService = SocketAllChatsServiceFactory.make()
    private var timer: Timer?

    private let customTabbarHeight: CGFloat = 108
    private let tabItems: [TabItem] = HideChats.chatIsHidden
        ? [.home, .search, .live, .profile]
        : [.home, .search, .live, .chat, .profile]

    lazy var customTabBarView: CustomTabBarView = {
        let iv = CustomTabBarView(menuItems: tabItems)
        iv.contentMode = .scaleAspectFill
        return iv
    }()

    override var childForStatusBarStyle: UIViewController? {
        return selectedViewController
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCustomTabBar()
        bindCustomTabbarView()
        setupCustomSafeAreaInsets()

        profileTabImage
            .bind { [weak self] text in
                guard let url = URL(string: text ?? "") else {
                    self?.customTabBarView.profileTabImage.accept(UIImage(named: "profileActive"))
                    return
                }
                self?.setupProfileTabImage(with: url)
            }
            .disposed(by: bag)
    }

    // MARK: - Public Methods

    func setTabbarHidden(_ isHidden: Bool) {
        customTabBarView.isHidden = isHidden
    }

    func hideLiveButton(_ isHidden: Bool) {
        customTabBarView.hideLiveButton(isHidden)
    }

    func activateProfileTab() {
        self.customTabBarView.activateProfileTab()
    }

    // MARK: - Private Methods

    private func setupCustomSafeAreaInsets() {
        additionalSafeAreaInsets.bottom = customTabbarHeight
    }

    private func setupProfileTabImage(with url: URL) {
        KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let value):
                self.customTabBarView.profileTabImage.accept(value.image)
            case .failure:
                self.customTabBarView.profileTabImage.accept(UIImage(named: "profile"))
            }
        }
    }

    private func setupCustomTabBar() {
        tabBar.isHidden = true
        view.addSubview(customTabBarView)

        customTabBarView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(customTabbarHeight)
        }
        selectedIndex = 0
    }

    private func bindCustomTabbarView() {
        customTabBarView
            .onItemTapped
            .bind { [weak self] tab in
                self?.selectedIndex = tab
            }
            .disposed(by: customTabBarView.bag)

        liveLessonsList
            .bind(to: customTabBarView.liveLessonsList)
            .disposed(by: bag)
    }
}
