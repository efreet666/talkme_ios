//
//  TabItem.swift
//  talkme_ios
//
//  Created by 1111 on 28.12.2020.
//

import UIKit

enum TabItem: String, CaseIterable {
    case home = "home"
    case search = "searh"
    case live = "live"
    case chat = "chat"
    case profile = "profile"

    var icon: UIImage? {
        switch self {
        case .home:
            return UIImage(named: "home")
        case .search:
            return UIImage(named: "search")
        case .live:
            return UIImage(named: "live")
        case .chat:
            return UIImage(named: "chat")
        case .profile:
            return UIImage(named: "profile")
        }
    }

    var iconForActive: UIImage? {
        switch self {
        case .home:
            return UIImage(named: "homeActive")
        case .search:
            return UIImage(named: "search")
        case .live:
            return UIImage(named: "live")
        case .chat:
            return UIImage(named: "chatActive")
        case .profile:
            return UIImage(named: "profileActive")
        }
    }
}
