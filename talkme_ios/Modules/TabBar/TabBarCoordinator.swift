//
//  TabBarCoordinator.swift
//  talkme_ios
//
//  Created by 1111 on 28.12.2020.
//

import RxSwift
import RxCocoa

final class TabBarCoordinator {

    enum Flow {
        case exitAppToRoot
    }

    // MARK: Public properties

    let profileCoordinator = ProfileCoordinator()
    let searchCoordinator = SearchCoordinator()
    let mainCoordinator = MainCoordinator()
    let streamCoordinator = StreamCoordinator()
    let chatsCoordinator = ChatsCoordinator()
    let onCreateChatTapped = PublishRelay<Void>()
    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()

    // MARK: Private properties

    private let tabBarController = TabBarController()
    private let liveViewController = UIViewController()
    private let lessonService = LessonsService()
    private let userStatusService = SocketUserStatusServiceFactory.make()

    // MARK: Public methods

    @discardableResult
    func start() -> UITabBarController {
        liveViewController.view.backgroundColor = .white
        bind()
        setTabControllers(for: tabBarController)
        coordinatorsPopToRoot()
        tabBarController.selectedIndex = 0
        return tabBarController
    }

    func pushToTopUpBalance() {
        self.tabBarController.activateProfileTab()
        self.profileCoordinator.showMyBalanceScreen()
    }

    // MARK: Private methods

    private func setTabControllers(for tabBarController: TabBarController) {
        tabBarController.viewControllers = HideChats.chatIsHidden
            ? [
                mainCoordinator.start(),
                searchCoordinator.start(),
                liveViewController,
                profileCoordinator.start()
            ]
            : [
                mainCoordinator.start(),
                searchCoordinator.start(),
                liveViewController,
                chatsCoordinator.start(),
                profileCoordinator.start()
            ]
    }

    private func bind() {
        profileCoordinator
            .profileTabImage
            .bind(to: tabBarController.profileTabImage)
            .disposed(by: bag)

        mainCoordinator
            .profileTabImage
            .bind(to: tabBarController.profileTabImage)
            .disposed(by: bag)

        mainCoordinator
            .liveLessonsList
            .bind(to: tabBarController.liveLessonsList)
            .disposed(by: bag)

        streamCoordinator
            .flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .exitToRoot, .exitToPreviousScreen:
                    self.tabBarController.selectedIndex = 0
                    self.tabBarController.viewControllers?[2] = self.liveViewController
                case .toChat(let userId):
                    guard let navigationController = self.streamCoordinator.navigationController else { return }
                    self.chatsCoordinator.startChat(navigationController, userId: userId)
                case .makeComplaint:
                    return
                }
            }
            .disposed(by: bag)

        searchCoordinator
            .flow
            .bind { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .showTopUpBalance:
                    self.pushToTopUpBalance()
                case .exitAppToRoot:
                    self.searchCoordinator.navigationController?.popViewController(animated: false)
                }
            }
            .disposed(by: searchCoordinator.bag)

        userStatusService
            .userOnline
            .subscribe(onNext: { [weak self] userModel in
                self?.chatsCoordinator.userStatus.onNext(UserStatus(user: userModel.user, isOnline: true))
            })
            .disposed(by: bag)

        userStatusService
            .userOffline
            .subscribe(onNext: { [weak self] userModel in
                self?.chatsCoordinator.userStatus.onNext(UserStatus(user: userModel.user, isOnline: false))
            })
            .disposed(by: bag)
    }

    private func coordinatorsPopToRoot() {
        tabBarController
            .customTabBarView
            .flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                switch flow {
                case .home:
                    self.mainCoordinator.navigationController?.popToRootViewController(animated: false)
                case .search:
                    return
                case .live:
                    self.presentLiveStreams(from: [])
                case .chat:
                    return
                case .profile:
                    self.profileCoordinator.navigationController?.popToRootViewController(animated: false)
                }
            }
            .disposed(by: tabBarController.customTabBarView.bag)
    }

    private func presentLiveStreams(from streamsList: [LiveStream]) {
        let liveVc = streamCoordinator.start(nil, lessonId: nil, lessonsInfo: streamsList, numberOfStreamsInPage: 50)
        tabBarController.viewControllers?[2] = liveVc
    }
}
