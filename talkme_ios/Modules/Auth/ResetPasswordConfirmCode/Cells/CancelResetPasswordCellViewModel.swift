//
//  CancelResetPasswordCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.03.2021.
//

import RxCocoa
import RxSwift

final class CancelResetPasswordCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { 50 }

    let cancelResetPasswordButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: Private Properties

    private let title: String

    // MARK: - Initializers

    init(title: String) {
        self.title = title
    }
    // MARK: Public Methods

    func configure(_ cell: CancelResetPasswordTableCell) {
        cell.configure(buttonTitle: title)
        cell
            .cancelResetPasswordTap
            .bind(to: cancelResetPasswordButtonTap)
            .disposed(by: cell.bag)
    }
}
