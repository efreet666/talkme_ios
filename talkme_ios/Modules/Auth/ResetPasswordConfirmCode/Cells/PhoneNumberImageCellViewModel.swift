//
//  PhoneNumberImageCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 13/9/2022.
//

import RxCocoa
import RxSwift

final class PhoneNumberImageCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { PhoneNumberImageCell.Constants.cellHeight }
    let heightUpdate = BehaviorRelay<CGFloat>(value: UIScreen.isSE ? 59 : 79)

    // MARK: - Private Properties

    // MARK: - Initializers

    // MARK: - Public Methods

    func configure(_ cell: PhoneNumberImageCell) {
        heightUpdate
            .bind { [weak cell] height in
                cell?.configureHeight(height)
            }
            .disposed(by: cell.bag)
    }
}
