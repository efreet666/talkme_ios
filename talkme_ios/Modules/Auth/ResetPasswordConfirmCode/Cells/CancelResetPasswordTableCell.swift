//
//  CancelResetPasswordTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.03.2021.
//

import RxCocoa
import RxSwift

final class CancelResetPasswordTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var cancelResetPasswordTap = cancelResetPasswordButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let cancelResetPasswordButton = UIButton(type: .system)

    // MARK: - Initializers

     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
         super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
     }

     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }

    // MARK: - Public Methods

    func configure(buttonTitle: String) {
        let attributes: [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: UIScreen.isSE ? 12 : 15),
        NSAttributedString.Key.foregroundColor: TalkmeColors.white,
        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedString = NSMutableAttributedString(
            string: buttonTitle, attributes: attributes)
        cancelResetPasswordButton.setAttributedTitle(attributedString, for: .normal)
        cancelResetPasswordButton.setTitleColor(TalkmeColors.white, for: .normal)
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubviews(cancelResetPasswordButton)

        cancelResetPasswordButton.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-5)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
