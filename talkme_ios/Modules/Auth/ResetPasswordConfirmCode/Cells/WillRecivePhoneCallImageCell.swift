//
//  WillRecivePhoneCallImageCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 14/9/2022.
//

import UIKit
import RxSwift

final class WillRecivePhoneCallImageCell: UITableViewCell {

    enum Constants {
        static let cellHeight: CGFloat = UIScreen.isSE ? 59 : 79// todo: return 35 : 56 after it is possible to enter and register via email
    }

    // MARK: - Private Properties

    private(set) var bag = DisposeBag()

    private var phoneNumberView: UIImageView = {
        let image = UIImage(named: "willReciveCall")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configureHeight(_ height: CGFloat) {
        contentView.snp.updateConstraints { make in
            make.height.equalTo(height)
        }
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(phoneNumberView)

        contentView.snp.makeConstraints { make in
            make.height.equalTo(Constants.cellHeight)
            make.edges.equalToSuperview()
        }

        phoneNumberView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(50)
            make.trailing.equalToSuperview().offset(-50)
        }
    }
}
