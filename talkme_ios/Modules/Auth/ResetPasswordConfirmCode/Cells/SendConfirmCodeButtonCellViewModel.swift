//
//  SendConfirmCodeButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 18.03.2021.
//

import RxCocoa

final class SendConfirmCodeButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { UIScreen.isSE ? 120 : 130 }

    let error = PublishRelay<AuthError>()
    let textRelay = PublishRelay<String>()
    let buttonIsActive = BehaviorRelay<Bool>(value: true)
    let sendCodeAgainButtonTap = PublishRelay<Void>()

    // MARK: Public Methods

    func configure(_ cell: SendConfirmCodeButtonTableCell) {
        textRelay
            .bind(to: cell.textRelay)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        cell
            .sendCodeAgainButtonTap
            .bind(to: sendCodeAgainButtonTap)
            .disposed(by: cell.bag)

        buttonIsActive
            .bind(to: cell.sendCodeAgainButtonEnabled)
            .disposed(by: cell.bag)

        buttonIsActive
            .bind { [weak cell] buttonIsEnabled in
                cell?.sendCodeAgainButton.alpha = buttonIsEnabled ? 1 : 0.3
            }
            .disposed(by: cell.bag)
    }
}
