//
//  ConfirmCodeCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 18.03.2021.
//

import RxCocoa
import RxSwift

final class ConfirmCodeCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { UIScreen.isSE ? 212 : 272 }

    let codeValue = BehaviorRelay<String?>(value: nil)
    let sendConfirmCodeButtonIsHidden = BehaviorRelay<Bool>(value: true)
    let buttonIsActive = BehaviorRelay<Bool>(value: true)
    let setConfirmCodeButtonTitle = PublishRelay<String?>()
    let sendConfirmCodeButtonTap = PublishRelay<Void>()

    // MARK: Public Methods

    func configure(_ cell: ConfirmCodeTableCell) {
        cell
            .sendConfirmCodeButtonTap
            .bind(to: sendConfirmCodeButtonTap)
            .disposed(by: cell.bag)

        cell
            .codeViewValue
            .bind(to: codeValue)
            .disposed(by: cell.bag)

        buttonIsActive
            .bind(to: cell.sendConfirmCodeButtonIsEnabled)
            .disposed(by: cell.bag)

        sendConfirmCodeButtonIsHidden
            .bind(to: cell.sendConfirmCodeButtonIsHidden)
            .disposed(by: cell.bag)

        setConfirmCodeButtonTitle
            .bind(to: cell.setConfirmCodeButtonTitle)
            .disposed(by: cell.bag)
    }
}
