//
//  ConfirmCodeTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 18.03.2021.
//

import RxSwift
import UIKit

final class ConfirmCodeTableCell: UITableViewCell {

    private enum Constants {
        static let codeLength = 4
    }

    // MARK: Public Properties

    private(set) lazy var setConfirmCodeButtonTitle = sendConfirmCodeButton.rx.title(for: .normal)
    private(set) lazy var sendConfirmCodeButtonTap = sendConfirmCodeButton.rx.tap
    private(set) lazy var sendConfirmCodeButtonIsEnabled = sendConfirmCodeButton.rx.isEnabled
    private(set) lazy var sendConfirmCodeButtonIsHidden = sendConfirmCodeButton.rx.isHidden

    private(set) lazy var codeViewValue = codeView.codeValue
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let codeView = ConfirmationCodeView(codeLength: Constants.codeLength)

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "talkme_сonfirm_сode_enter_code_text".localized
        label.textColor = TalkmeColors.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.montserratBold(ofSize: 17)
        return label
    }()

    private let sendConfirmCodeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        btn.setTitle("profile_setting_send_code".localized, for: .normal)
        btn.titleLabel?.font = .montserratExtraBold(ofSize: UIScreen.isSE ? 13 : 15)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = TalkmeColors.white.cgColor
        btn.layer.cornerRadius = UIScreen.isSE ? 21 : 26
        return btn
    }()

    private let lastFourNumbersImageView: UIImageView = {
        let image = UIImage(named: "lastFourNumbers")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel,
                                 lastFourNumbersImageView,
                                 codeView,
                                 sendConfirmCodeButton
                                ])

//        titleLabel.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(32)
//            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 23 : 47)
//        }

        lastFourNumbersImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 15 : 25)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 23 : 47)
        }

        codeView.snp.makeConstraints { make in
            make.top.equalTo(lastFourNumbersImageView.snp.bottom).offset(UIScreen.isSE ? 15 : 25)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 21 : 30)
            make.height.equalTo(50)
        }

        sendConfirmCodeButton.snp.makeConstraints { make in
            make.top.equalTo(codeView.snp.bottom).offset(UIScreen.isSE ? 15 : 20)
            make.trailing.leading.equalToSuperview().inset(35)
            make.height.equalTo(UIScreen.isSE ? 32 : 52)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
