//
//  SendConfirmCodeButtonTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 18.03.2021.
//

import RxSwift

final class SendConfirmCodeButtonTableCell: UITableViewCell {

    // MARK: Public Properties

    private(set) lazy var textRelay = warningLabel2.rx.text
    private(set) lazy var sendCodeAgainButtonEnabled = sendCodeAgainButton.rx.isEnabled
    private(set) lazy var sendCodeAgainButtonTap = sendCodeAgainButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    let sendCodeAgainButton: UIButton = {
        let buttont = UIButton(type: .system)
        buttont.setTitle("talkme_сonfirm_сode_send_code_again".localized, for: .normal)
        buttont.setTitleColor(TalkmeColors.white, for: .normal)
        buttont.backgroundColor = TalkmeColors.deepBlue
        buttont.titleLabel?.font = .montserratExtraBold(ofSize: 15)
        buttont.titleLabel?.textAlignment = .center
        buttont.backgroundColor = TalkmeColors.blueLabels
        buttont.alpha = 1
        return buttont
    }()

    private let warningLabel: UILabel = {
       let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .center
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        lbl.alpha = 0
        return lbl
    }()

    private let warningLabel2: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Public Methods

    func setupError(_ error: AuthError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if sendCodeAgainButton.layer.cornerRadius == 0 {
            sendCodeAgainButton.layer.cornerRadius = sendCodeAgainButton.bounds.height / 2
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubviews([warningLabel,
                                 warningLabel2,
                                 sendCodeAgainButton])

        warningLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 10 : 15)
        }

        warningLabel2.snp.makeConstraints { make in
            make.top.equalTo(warningLabel.snp.bottom).offset(UIScreen.isSE ? 0 : 25)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 23 : 40)
            make.height.equalTo(40)
        }

        sendCodeAgainButton.snp.makeConstraints { make in
            make.top.equalTo(warningLabel2.snp.bottom).offset(UIScreen.isSE ? 5 : 20)
            make.leading.trailing.equalToSuperview().inset(35)
            make.height.equalTo(UIScreen.isSE ? 35 : 52)
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -10 : -15)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
