//
//  ResetPasswordEmailViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.05.2021.
//

import RxCocoa
import RxSwift

final class ResetPasswordEmailViewModel {

    enum ResetPasswordFlow {
        case cancelResetPassword
        case backToAuth
    }

    // MARK: - Public Properties

    let flow = PublishRelay<ResetPasswordFlow>()
    let bag = DisposeBag()
    let topLogoInset = PublishRelay<CGFloat>()
    let email: String
    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>
        .just([
            emptyLogoCellViewModel,
            talkMeLogoCellViewModel,
            emptyTitleCellViewModel,
            titleLabelCellViewModel,
            resetPasswordTitleViewModel,
            emptyCellViewModel,
            confirmButtonViewModel,
            emptySocialCellViewModel,
            socialCellViewModel,
            loginCellViewModel
        ])

    // MARK: - Private Properties

    private let appleService: AppleIDSignInService
    private let googleService: GoogleSignInService
    private let service: AuthServiceProtocol
    private let talkMeLogoCellViewModel = TalkMeLogoCellViewModel()
    private let titleLabelCellViewModel = TitleTableCellViewModel(
        titleText: "talkme_auth_reset_password".localized)
    private lazy var emptyCellViewModel = EmptyTableCellViewModel(cellHeight: emptySpaceHeight)
    private lazy var emptyTitleCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? 30 : 60)
    private lazy var emptySocialCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? 0 : 30)
    private lazy var emptyLogoCellViewModel = EmptyTableCellViewModel(cellHeight: 5)
    private lazy var resetPasswordTitleViewModel = ResetPasswordTitleCellViewModel(email: email)
    private let confirmButtonViewModel = ConfirmResetButtonTableCellViewModel()
    private let loginCellViewModel = CancelResetPasswordCellViewModel(
        title: "talkme_auth_remembered_password".localized)
    private let socialCellViewModel = SocialsTableCellViewModel(cellType: .signIn)
    private let confirmButton = SendConfirmCodeButtonCellViewModel()

    private lazy var emptySpaceHeight: CGFloat = {
        let allHeights = talkMeLogoCellViewModel.height
            + titleLabelCellViewModel.height
            + resetPasswordTitleViewModel.height
            + loginCellViewModel.height
            + CGFloat(UIScreen.isSE ? 50 : 20)
        let navBarHeight = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.navigationBar.frame.height ?? 0
        let topInset = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
        let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return UIScreen.main.bounds.height
            - topInset
            - bottomInset
            - navBarHeight
            - allHeights
    }()

    // MARK: - Initializers

    init(service: AuthServiceProtocol, appleService: AppleIDSignInService, googleService: GoogleSignInService, email: String) {
        self.service = service
        self.appleService = appleService
        self.googleService = googleService
        self.email = email
        bindViewModels()
    }

    // MARK: - Private Methods

    private func bindViewModels() {

        loginCellViewModel
            .cancelResetPasswordButtonTap
            .map { .cancelResetPassword }
            .bind(to: flow)
            .disposed(by: loginCellViewModel.bag)

        socialCellViewModel
            .buttonTapRelay
            .bind { [weak self] type in
//                self?.socialSignIn(with: type) // TODO: after connect socials
            }
            .disposed(by: socialCellViewModel.bag)

        topLogoInset
            .bind(to: talkMeLogoCellViewModel.topUpdate)
            .disposed(by: bag)

        confirmButtonViewModel
            .onButtonTap
            .bind { [weak self] _ in
                self?.flow.accept(.backToAuth)
            }
            .disposed(by: confirmButtonViewModel.bag)
    }

    private func socialSignIn(with type: SocialButtonType) {
        switch type {
        case .google:
            break
        case .appleID:
            if #available(iOS 13.0, *) { appleService.signIn() }
        case .facebook:
            FacebookSignInService.signIn()
        case .yandex:
            break
        case .mailru:
            break
        case .vkontakte:
            break
        default:
            break
        }
    }
}
