//
//  ResetPasswordTitleCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.05.2021.
//

import RxCocoa
import RxSwift

final class ResetPasswordTitleCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { UIScreen.isSE ? 150 : 210 }
    let textRelay = PublishRelay<String>()
    let bag = DisposeBag()
    let email: String

    // MARK: Public Methods

    init(email: String) {
        self.email = email
    }

    func configure(_ cell: ResetPasswordTitleCell) {
        cell.configure(email)
    }
}
