//
//  ResetPasswordTitleCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.05.2021.
//

import RxSwift

final class ResetPasswordTitleCell: UITableViewCell {

    // MARK: Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func configure(_ model: String) {
        titleLabel.text = "talkme_auth_check_email".localizeWithArgument(model)
        let attributedStr = boldenParts(string: titleLabel.text ?? "", boldCharactersRanges: [[20, 20 + model.count]],
                                        regularFont: UIFont(name: "Montserrat-Medium", size: UIScreen.isSE ? 13 : 15),
                                        boldFont: UIFont(name: "Montserrat-Bold", size: UIScreen.isSE ? 13 : 15))
        titleLabel.attributedText = attributedStr
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(titleLabel)

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 23 : 35)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func boldenParts(string: String, boldCharactersRanges: [[Int]], regularFont: UIFont?, boldFont: UIFont?) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string, attributes:
                                                            [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 12)])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: boldFont ?? UIFont.boldSystemFont(ofSize: regularFont?.pointSize ?? UIFont.systemFontSize)]
        for range in boldCharactersRanges {
            let currentRange = NSRange(location: range[0], length: range[1]-range[0]+1)
            attributedString.addAttributes(boldFontAttribute, range: currentRange)
        }
        return attributedString
    }
}
