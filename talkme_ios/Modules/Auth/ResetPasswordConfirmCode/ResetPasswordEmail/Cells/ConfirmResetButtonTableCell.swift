//
//  ConfirmButtonTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.05.2021.
//

import RxSwift

final class ConfirmResetButtonTableCell: UITableViewCell {

    // MARK: - Public Properties

    let bag = DisposeBag()
    lazy var onButtonTap = confirmButton.rx.tap

    // MARK: - Private Properties

    private let confirmButton: UIButton = {
        let button = ConfirmationButton(type: .system)
        button.setTitleColor(TalkmeColors.white, for: .normal)
        button.setTitle("reset_password_understand".localized, for: .normal)
        button.titleLabel?.font = .montserratExtraBold(ofSize: UIScreen.isSE ? 13 : 15)
        return button
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(confirmButton)
        confirmButton.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 33 : 35)
            make.height.equalTo(UIScreen.isSE ? 42 : 52)
        }
        confirmButton.layer.cornerRadius = UIScreen.isSE ? 21 : 27
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
