//
//  ConfirmButtonTableCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.05.2021.
//

import RxCocoa
import RxSwift

final class ConfirmResetButtonTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { UIScreen.isSE ? 50 : 80 }
    let onButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: Public Methods

    func configure(_ cell: ConfirmResetButtonTableCell) {
        cell
            .onButtonTap
            .bind(to: onButtonTap)
            .disposed(by: cell.bag)
    }
}
