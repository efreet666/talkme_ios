//
//  ResetPasswordConfirmCodeViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 18.03.2021.
//

import RxCocoa
import RxSwift
import Moya

final class ResetPasswordConfirmCodeViewModel {

    enum PasswordConfirmCodeFlow {
        case onCodeConfirm(userToken: String, phoneNumber: String)
        case cancelResetPassword
        case networkErrorPopUp(errorMessage: String?)
    }

    // MARK: - Public Properties

    let flow = PublishRelay<PasswordConfirmCodeFlow>()
    let bag = DisposeBag()
    lazy var haveSocial: [AnyTableViewCellModelProtocol] = [
        talkMeLogoCellViewModel,
        willRecivePhoneCallImageViewModel,
//        descriptionTextCellViewModel,
        phoneNumberImageCellViewModel,
//        titleLabelCellViewModel,
        сonfirmCodeCellViewModel,
        emptyCellViewModel,
        showButtonTableCellViewModel,
        socialCellViewModel
    ]
    lazy var dontHaveSocial: [AnyTableViewCellModelProtocol] = [
        talkMeLogoCellViewModel,
        willRecivePhoneCallImageViewModel,
//        descriptionTextCellViewModel,
        phoneNumberImageCellViewModel,
//        titleLabelCellViewModel,
        сonfirmCodeCellViewModel,
        emptyCellViewModel,
        showButtonTableCellViewModel
    ]
    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>
        .just(AppDelegate.addSocialSignIn ? haveSocial : dontHaveSocial)

    // MARK: - Private Properties

    private let phoneNumber: String
    private var rxTimer: Disposable?
    private let appleService: AppleIDSignInService
    private let googleService: GoogleSignInService
    private let service: AuthServiceProtocol
    private let talkMeLogoCellViewModel = TalkMeLogoCellViewModel()
    private let titleLabelCellViewModel = TitleTableCellViewModel(titleText: "talkme_сonfirm_сode_enter_code_text".localized)
    private let willRecivePhoneCallImageViewModel = WillRecivePhoneCallImageViewModel()
    private let сonfirmCodeCellViewModel = ConfirmCodeCellViewModel()
    private let showButtonTableCellViewModel = SendConfirmCodeButtonCellViewModel()
    private lazy var emptyCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? 0: emptySpaceHeight)
    private let socialCellViewModel = SocialsTableCellViewModel(cellType: .signIn)
    private let rememberButtonCellViewModel = CancelResetPasswordCellViewModel(title: "talkme_auth_remembered_password".localized)
    private let descriptionTextCellViewModel = DescriptionRegistrationTableCellViewModel(title: "talkme_сonfirm_сode_main_text".localized)
    private let phoneNumberImageCellViewModel = PhoneNumberImageCellViewModel()

    private var maxCountOfNetworkRetries = 1
    private var currentCountOfNetworkRetries = 0

    private lazy var emptySpaceHeight: CGFloat = {
        if AppDelegate.addSocialSignIn {
        let allHeights = talkMeLogoCellViewModel.height
            + descriptionTextCellViewModel.height
            + phoneNumberImageCellViewModel.height
            + сonfirmCodeCellViewModel.height
            + showButtonTableCellViewModel.height
        let navBarHeight = UIApplication.topInset
        let topInset = UIApplication.topInset
        let bottomInset = UIApplication.bottomInset
        return UIScreen.main.bounds.height
            - topInset
            - bottomInset
            - navBarHeight
            - allHeights
        } else {
            let allHeights = talkMeLogoCellViewModel.height
            + descriptionTextCellViewModel.height
            + phoneNumberImageCellViewModel.height
            + сonfirmCodeCellViewModel.height
            + showButtonTableCellViewModel.height
            let navBarHeight = UIApplication.topInset
            let topInset = UIApplication.topInset
            let bottomInset = UIApplication.bottomInset
            return UIScreen.main.bounds.height
                - topInset
                - bottomInset
                - navBarHeight
                - allHeights
        }
    }()

    // MARK: - Initializers

    init(phoneNumber: String, service: AuthServiceProtocol, appleService: AppleIDSignInService, googleService: GoogleSignInService) {
        self.service = service
        self.appleService = appleService
        self.googleService = googleService
        self.phoneNumber = phoneNumber
        bindViewModels()
        setupTimer()
    }

    // MARK: - Private Methods

    private func bindViewModels() {
        сonfirmCodeCellViewModel.codeValue
            .map { ($0?.count ?? 0) == 4 }
            .bind { [weak self] isValid in
                self?.сonfirmCodeCellViewModel.setConfirmCodeButtonTitle.accept(
                    "talkme_сonfirm_сode_confirm".localized)
                self?.сonfirmCodeCellViewModel.sendConfirmCodeButtonIsHidden.accept(!isValid)
            }
            .disposed(by: bag)

        сonfirmCodeCellViewModel
            .sendConfirmCodeButtonTap
            .withLatestFrom(сonfirmCodeCellViewModel.codeValue)
            .bind { [weak self] text in
                guard let code = text else { return }
                self?.passwordReset(code)
            }
            .disposed(by: bag)

        showButtonTableCellViewModel
            .sendCodeAgainButtonTap
            .bind { [weak self] _ in
                self?.setupTimer()
                self?.resetPassword()
            }
            .disposed(by: bag)

        rememberButtonCellViewModel
            .cancelResetPasswordButtonTap
            .map { .cancelResetPassword }
            .bind(to: flow)
            .disposed(by: bag)

        socialCellViewModel
            .buttonTapRelay
            .bind { [weak self] type in
                self?.socialSignIn(with: type)
            }
            .disposed(by: bag)
    }

    private func setupTimer() {
        rxTimer = Observable<Int>.interval(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .map {-$0 + 59}
            .map { [weak self] timerValue -> String in
                guard let self = self else { return "" }
                let seconds = (timerValue % 3600) % 60
                let stringFormatted = String(format: "%02d", seconds)

                if timerValue != 0 {
                    self.showButtonTableCellViewModel.buttonIsActive.accept(false)
                    return stringFormatted
                } else {
                    self.rxTimer?.dispose()
                    self.showButtonTableCellViewModel.textRelay.accept("")
                    self.showButtonTableCellViewModel.buttonIsActive.accept(true)
                    self.showButtonTableCellViewModel.error.accept(.none)
                    return ""
                }
            }.subscribe(onNext: { [weak self] value in
                self?.showButtonTableCellViewModel.textRelay.accept(
                    "talkme_auth_error_request_code".localizeWithArgument(value))
            })
    }

    private func passwordReset(_ code: String) {
        let request = SendMobileAndCodeRequest(code: code, mobile: phoneNumber)
        service
            .resetPassword(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let self = self else { return }
                    guard let user = response.user else {
                        self.showButtonTableCellViewModel.error.accept(.apiError(response.msg))
                        self.setupTimer()
                        return
                    }
                    self.flow.accept(.onCodeConfirm(userToken: user, phoneNumber: self.phoneNumber))
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.passwordReset(code)},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "31")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func resetPassword() {
        let request = SendMobileAndCodeRequest(code: nil, mobile: phoneNumber)
        service
            .resetPassword(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.mobile != nil else {
                        self?.showButtonTableCellViewModel.error.accept(.apiError(response.msg))
                        return
                    }
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.resetPassword()},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "32")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func socialSignIn(with type: SocialButtonType) {
        switch type {
        case .google:
            break
        case .appleID:
            if #available(iOS 13.0, *) { appleService.signIn() }
        case .facebook:
            FacebookSignInService.signIn()
        case .yandex:
            break
        case .mailru:
            break
        case .vkontakte:
            break
        default:
            break
        }
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)"))
            }
        }
    }
}
