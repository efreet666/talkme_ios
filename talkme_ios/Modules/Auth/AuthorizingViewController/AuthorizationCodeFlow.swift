//
//  RegistrationConfirmCodeFlow.swift
//  talkme_ios
//
//  Created by Майя Калицева on 25.12.2020.
//

enum AuthorizationCodeFlow: Equatable {
    case onShowLogin
    case onShowRegistration
    case onAuthRules
}
