//
//  AuthorizationViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 25.12.2020.
//

import RxCocoa
import RxSwift

final class AuthorizationViewModel {

    // MARK: - Private Properties

    private let service: AuthServiceProtocol
    let bag = DisposeBag()
    public let flow = PublishRelay<AuthorizationCodeFlow>()

    // MARK: - Initializers

    init(service: AuthServiceProtocol) {
        self.service = service
    }

    // MARK: - Public Methods

    func onRegisterTap() {
        flow.accept(.onShowRegistration)
    }
    func onEnterTap() {
        flow.accept(.onShowLogin)
    }
}
