//
//  AuthorizingViewController.swift
//  talkme_ios
//
//  Created by Майя Калицева on 24.12.2020.
//

import RxCocoa
import RxSwift

final class AuthorizationViewController: UIViewController {

    // MARK: - Private Properties

    private let isSwitchUrlEnable = Consts.isSwitchUrlEnable // TODO: включать если нужен интерфейс переключения

    private let bag = DisposeBag()
    private let viewModel: AuthorizationViewModel

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fill
        return sv
    }()

    private let logoImage: UIImageView = {
        let logo = UIImageView()
        logo.image = UIImage(named: "talkme")
        return logo
    }()

    private let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "mainBackground")
        image.contentMode = UIView.ContentMode.scaleAspectFill
        return image
    }()

    private let phoneOrMailButton: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("talkme_auth_login_start_screen".localized, for: .normal)
        bt.setTitleColor(TalkmeColors.white, for: .normal)
        bt.backgroundColor = TalkmeColors.deepBlue
        bt.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 16)
        bt.titleLabel?.textAlignment = .center
        bt.layer.cornerRadius = UIScreen.isSE ? 21 : 26
        return bt
    }()

    private let registrationButton: UIButton = {
        let bt = UIButton(type: .system)
        bt.setTitle("talkme_auth_registration".localized, for: .normal)
        bt.setTitleColor(TalkmeColors.white, for: .normal)
        bt.backgroundColor = TalkmeColors.deepBlue
        bt.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 16)
        bt.titleLabel?.textAlignment = .center
        bt.layer.cornerRadius = UIScreen.isSE ? 21 : 26
        return bt
    }()

    private let appleButton: UIButton = {
        let bt = UIButton()
        bt.backgroundColor = TalkmeColors.black
        bt.setTitle("talkme_auth_apple_id".localized, for: .normal)
        bt.layer.cornerRadius = 21
        bt.setImage(UIImage(named: "apple"), for: .normal)
        bt.imageEdgeInsets.left = 55
        bt.imageEdgeInsets.right = 185
        bt.imageEdgeInsets.top = 10
        bt.imageEdgeInsets.bottom = 10
        bt.titleLabel?.font = .montserratSemiBold(ofSize: 12)
        return bt
    }()

    private let confirmButton: UIButton = {
        let bt = UIButton()
        let attributes: [NSAttributedString.Key: Any] =
            [NSAttributedString.Key.font: UIFont.montserratFontRegular(ofSize: UIScreen.isSE ? 10 : 12),
             NSAttributedString.Key.foregroundColor: TalkmeColors.lightBlue]

        let attributedString = NSMutableAttributedString(
            string: "talkme_auth_accept".localized, attributes: attributes)
        let anotherAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: UIFont.montserratFontBlack(ofSize: UIScreen.isSE ? 10 : 12),
            NSAttributedString.Key.foregroundColor: TalkmeColors.white]
        let anotherString = NSMutableAttributedString(
            string: "talkme_auth_rules".localized, attributes: anotherAttributes)
        attributedString.append(anotherString)
        bt.titleLabel?.numberOfLines = 0
        bt.setAttributedTitle(attributedString, for: .normal)
        bt.titleLabel?.textAlignment = .center
        return bt
    }()

    private lazy var urlButton: UIButton = {
        let bt = UIButton()
        bt.titleLabel?.font = UIFont.montserratFontRegular(ofSize: 12)
        bt.titleLabel?.textColor = .white
        bt.titleLabel?.textAlignment = .center
        bt.isHidden = !isSwitchUrlEnable
        return bt
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        urlButton.setTitle(Configuration.base, for: .normal)
        setConstraints()
        bindUI()
        setupExclusiveTouches()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = nil
    }

    // MARK: - Initializers

    init(viewModel: AuthorizationViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupExclusiveTouches() {
        phoneOrMailButton.isExclusiveTouch = true
        registrationButton.isExclusiveTouch = true
    }

    private func setConstraints() {
        view.addSubviewsWithoutAutoresizing(backgroundImage, logoImage, stackView)
        [ // appleButton, // TODO: after connect apple registration
         phoneOrMailButton,
         registrationButton,
         urlButton,
         confirmButton
        ].forEach(stackView.addArrangedSubview)

//        stackView.setCustomSpacing(13, after: appleButton)
        stackView.setCustomSpacing(UIScreen.isSE ? 13 : 22, after: phoneOrMailButton)
        stackView.setCustomSpacing(13, after: registrationButton)
        stackView.setCustomSpacing(26, after: confirmButton)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(greaterThanOrEqualTo: logoImage.bottomAnchor),
            stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),

            backgroundImage.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            logoImage.topAnchor.constraint(equalTo: view.topAnchor, constant: UIScreen.isSE ? 78 : 128),
            logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: UIScreen.isSE ? 76 : 86),
            logoImage.heightAnchor.constraint(equalToConstant: UIScreen.isSE ? 100 : 136),
            logoImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: UIScreen.isSE ? -76 : -86),

//            appleButton.heightAnchor.constraint(equalToConstant: 42),

            phoneOrMailButton.heightAnchor.constraint(equalToConstant: UIScreen.isSE ? 42 : 56),
            phoneOrMailButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: UIScreen.isSE ? 30 : 36),
            phoneOrMailButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: UIScreen.isSE ? -30 : -36 ),

            registrationButton.heightAnchor.constraint(equalToConstant: UIScreen.isSE ? 42 : 56),
            registrationButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: UIScreen.isSE ? 30 : 36),
            registrationButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: UIScreen.isSE ? -30 : -36),

            urlButton.heightAnchor.constraint(equalToConstant: 30),
            urlButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: UIScreen.isSE ? 30 : 36),
            urlButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: UIScreen.isSE ? -30 : -36),

            confirmButton.heightAnchor.constraint(equalToConstant: 40),
            confirmButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: UIScreen.isSE ? -20 : -32)
        ])
    }

    private func bindUI() {
        registrationButton.rx
            .tap
            .bind { [weak self] _ in
                self?.viewModel.onRegisterTap()
            }
            .disposed(by: bag)

        urlButton
            .rx
            .longPressGesture { gesture, _ in gesture.minimumPressDuration = 5 }
            .when(.began)
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                UserDefaultsHelper.shared.isProd.toggle()
                self.urlButton.setTitle(Configuration.base, for: .normal)
                exit(0)
            })
            .disposed(by: bag)

        phoneOrMailButton.rx
            .tap
            .bind { [weak self] _ in
                self?.viewModel.onEnterTap()
            }
            .disposed(by: bag)

        confirmButton.rx.tap
            .map { .onAuthRules }
            .bind(to: viewModel.flow)
            .disposed(by: bag)
    }

    func setupUI() {
        navigationController?.navigationBar.backItem?.title = ""// "talkme_navigation_back".localized
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .clear
    }
}
