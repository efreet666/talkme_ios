//
//  PasswordRecoveryViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.03.2021.
//

import RxCocoa
import RxSwift
import Moya

final class ResetPasswordViewModel {

    enum ResetPasswordFlow {
        case onEnterSMSCode(phoneNumber: String)
        case onRegistrationTap
        case onShowCountries
        case cancelResetPassword
        case email(email: String)
        case networkErrorPopUp(errorMessage: String?)
    }

    // MARK: - Public Properties

    let flow = PublishRelay<ResetPasswordFlow>()
    let bag = DisposeBag()
    let topLogoInset = PublishRelay<CGFloat>()
    let emptyTopInset = PublishRelay<CGFloat>()
    lazy var haveSocial: [AnyTableViewCellModelProtocol] = [
        talkMeLogoCellViewModel,
        emptyLogoCellViewModel,
        titleLabelCellViewModel,
        // uncommit to enable chousing between entering phone number or email
//        emailOrPhoneNumberViewModel,
        emptyLogoCellViewModel,
        descriptionTextCellViewModel,
        emptyCellBeforeEnteringPasswordViewModel,
        enterPasswordCellViewModel,
        emptyCellViewModel,
        socialCellViewModel,
        loginCellViewModel
    ]

    lazy var dontHaveSocial: [AnyTableViewCellModelProtocol] =
    [
        talkMeLogoCellViewModel,
        emptyLogoCellViewModel,
        titleLabelCellViewModel,
        // uncommit to enable chousing between entering phone number or email
        // emailOrPhoneNumberViewModel,
        descriptionTextCellViewModel,
        emptyCellBeforeEnteringPasswordViewModel,
        enterPasswordCellViewModel,
        emptyCellViewModel,
        loginCellViewModel
    ]
    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>
        .just(AppDelegate.addSocialSignIn ? haveSocial : dontHaveSocial)

    // MARK: - Private Properties

    private let appleService: AppleIDSignInService
    private let googleService: GoogleSignInService
    private let service: AuthServiceProtocol
    private var country = CountryType.russia.model
    private let talkMeLogoCellViewModel = TalkMeLogoCellViewModel()
    private let titleLabelCellViewModel: TitleTableCellViewModel = {
        let titleView = TitleTableCellViewModel(titleText: "talkme_auth_reset_password".localized)
        titleView.heightUpdate.accept(65)
        return titleView
    }()
    private let descriptionTextCellViewModel = DescriptionRegistrationTableCellViewModel(title: "talkme_registration_restore_passwodr".localized)
    private let enterPasswordCellViewModel = EnterPasswordCellViewModel()
    private lazy var emptyCellViewModel = EmptyTableCellViewModel(cellHeight: emptySpaceHeight)
    private lazy var emptyLogoCellViewModel = EmptyTableCellViewModel(cellHeight: 10) // UIScreen.isSE ? emptySpaceHeight : 50)
    private lazy var emptyCellBeforeEnteringPasswordViewModel = EmptyTableCellViewModel(cellHeight: 5)
    private let socialCellViewModel = SocialsTableCellViewModel(cellType: .signIn)
    private let loginCellViewModel = LoginCellViewModel(
        title: "talkme_sign_in_not_registrated".localized,
        buttonTitle: "talkme_auth_registration".localized)
    private let emailOrPhoneNumberViewModel = UserRegistrationButtonsCellViewModel()
    private let isEmailReset = BehaviorRelay<Bool>(value: false)
    private var maxCountOfNetworkRetries = 1
    private var currentCountOfNetworkRetries = 0

    private lazy var emptySpaceHeight: CGFloat = {
        if AppDelegate.addSocialSignIn {
        let allHeights = talkMeLogoCellViewModel.height
            + titleLabelCellViewModel.height
            + enterPasswordCellViewModel.height
            + loginCellViewModel.height
            + socialCellViewModel.height
            + emailOrPhoneNumberViewModel.height
        let navBarHeight = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.navigationBar.frame.height ?? 0
        let topInset = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
        let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return UIScreen.main.bounds.height
            - topInset
            - bottomInset
            - navBarHeight
            - allHeights
        } else {
            let allHeights = talkMeLogoCellViewModel.height
                + titleLabelCellViewModel.height
                + enterPasswordCellViewModel.height
                + loginCellViewModel.height
                + emailOrPhoneNumberViewModel.height
            let navBarHeight = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.navigationBar.frame.height ?? 0
            let topInset = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return UIScreen.main.bounds.height
                - topInset
                - bottomInset
                - navBarHeight
                - allHeights
        }
    }()

    // MARK: - Initializers

    init(service: AuthServiceProtocol, appleService: AppleIDSignInService, googleService: GoogleSignInService) {
        self.service = service
        self.appleService = appleService
        self.googleService = googleService
        bindViewModels()
    }

    // MARK: - Public Methods

    func changeCountry(counrty: CountryType) {
        self.country = counrty.model
        enterPasswordCellViewModel.countryRelay.accept(counrty)
    }

    // MARK: - Private Methods

    private func bindViewModels() {
        enterPasswordCellViewModel
            .sendCodeTapRelay
            .withLatestFrom(enterPasswordCellViewModel.textRelay)
            .filter { !$0.isEmpty }
            .bind { [weak self] text in
                guard let self = self else { return }
                self.isEmailReset.value
                    ? self.emailResetPassword(text)
                    : self.mobileResetPassword(text)
            }
            .disposed(by: bag)

        enterPasswordCellViewModel
            .forgotButtonTap
            .map { .cancelResetPassword }
            .bind(to: flow)
            .disposed(by: enterPasswordCellViewModel.bag)

        enterPasswordCellViewModel
            .onCountryButtonTap
            .map { .onShowCountries }
            .bind(to: flow)
            .disposed(by: enterPasswordCellViewModel.bag)

        loginCellViewModel
            .tapLoginRelay
            .map { .onRegistrationTap }
            .bind(to: flow)
            .disposed(by: loginCellViewModel.bag)

        socialCellViewModel
            .buttonTapRelay
            .bind { [weak self] type in
                self?.socialSignIn(with: type) // TODO: after connect socials
            }
            .disposed(by: bag)

        emailOrPhoneNumberViewModel
            .tapEmailRelay
            .map { true }
            .bind(to: isEmailReset)
            .disposed(by: bag)

        emailOrPhoneNumberViewModel
            .tapNubmerRelay
            .map { false }
            .bind(to: isEmailReset)
            .disposed(by: bag)

        isEmailReset
            .bind(to: enterPasswordCellViewModel.isEmailReset)
            .disposed(by: bag)

        topLogoInset
            .bind(to: talkMeLogoCellViewModel.topUpdate)
            .disposed(by: bag)

        emptyTopInset
            .bind(to: emptyLogoCellViewModel.heightUpdate)
            .disposed(by: bag)
    }

    private func mobileResetPassword(_ mobile: String) {
        let request = SendMobileAndCodeRequest(code: nil, mobile: country.code + mobile)
        service
            .resetPassword(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let phoneNumber = response.mobile else {
                        self?.enterPasswordCellViewModel.error.accept(.apiError(response.msg))
                        return
                    }
                    self?.flow.accept(.onEnterSMSCode(phoneNumber: phoneNumber))
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.mobileResetPassword(mobile)},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "21")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func socialSignIn(with type: SocialButtonType) {
        switch type {
        case .google:
            break
        case .appleID:
            if #available(iOS 13.0, *) { appleService.signIn() }
        case .facebook:
            FacebookSignInService.signIn()
        case .yandex:
            break
        case .mailru:
            break
        case .vkontakte:
            break
        default:
            break
        }
    }

    private func emailResetPassword(_ email: String) {
        let request = ResetPasswordRequest(email: email)
        service
            .resetPasswordEmail(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.isEmail else {
                        self?.enterPasswordCellViewModel.error.accept(.apiError(response.text))
                        return
                    }
                    self?.flow.accept(.email(email: response.text))
                    // todo: implement universal link redirection
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.emailResetPassword(email) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "22")
                    print(error)
                default:
                    return
                }
            }
            .disposed(by: bag)
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)"))
            }
        }
    }
}
