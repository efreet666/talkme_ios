//
//  EnterPasswordTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.03.2021.
//

import RxSwift
import RxCocoa

final class EnterPasswordTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var sendCodeTap = registrationButtonView.sendCodeTap
    private(set) lazy var buttonIsEnabled = registrationButtonView.buttonIsEnabled
    private(set) lazy var buttonBackgroundColor = registrationButtonView.buttonBackgroundColor
    private(set) lazy var forgotButtonTap = forgotPasswordButton.rx.tap

    private(set) lazy var textRelay = PublishRelay<String>()
    private(set) lazy var selectCountryTap = phoneTextFieldView.selectCountryTap
    private(set) lazy var countryCodeText = phoneTextFieldView.countryCodeText
    private(set) lazy var countryCodeImage = phoneTextFieldView.countryCodeImage
    private(set) var bag = DisposeBag()
    let buttonTapped = PublishRelay<Void>()
    let isEmailReset = PublishRelay<Bool>()

    var isValidNumber: Bool {
        phoneTextFieldView.isValidNumber != (emailTextField.textField.text?.count ?? 0 > 0)
    }

    var currentCountry: CountryType = .russia {
        didSet {
            phoneTextFieldView.region = currentCountry.model.regionCode
        }
    }

    // MARK: - Private Properties

    private let registrationButtonView = RegistrationButton()
    private let phoneTextFieldView = NumberTextField()
    private let emailTextField: TextField = {
        let textField = TextField()
        textField.textField.font = .montserratFontMedium(ofSize: 12)
        textField.textField.placeholder = "talkme_auth_enter_data_email".localized
        textField.setSecure(false)
        textField.isHidden = true
        textField.textField.setEmailTextField()
        return textField
    }()
    private let buttonFont: UIFont = .montserratExtraBold(ofSize: UIScreen.isSE ? 12 : 15)

    private let containerView: UIView = {
       let view = UIView()
        view.layer.backgroundColor = TalkmeColors.white.cgColor
        view.layer.cornerRadius = 21
        return view
    }()

    private let warningLabel: UILabel = {
       let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .center
        lbl.font = .montserratFontMedium(ofSize: 12)
        lbl.numberOfLines = 0
        lbl.adjustsFontSizeToFitWidth = true
        lbl.alpha = 0
        return lbl
    }()

    private let forgotPasswordButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("talkme_auth_remembered_password".localized, for: .normal)
        btn.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        btn.titleLabel?.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 14)
        return btn
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
        emailTextField.textField.returnKeyType = .send
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure() {
        bindUI()
        bindTextField()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func setupError(_ error: AuthError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    private func setupUI() {
        registrationButtonView.configure(text: "profile_setting_send_code".localized, font: buttonFont)
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        containerView.addSubviews(
            phoneTextFieldView,
            emailTextField,
            registrationButtonView,
            warningLabel,
            forgotPasswordButton
        )
        contentView.addSubview(containerView)

        containerView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
        }

        phoneTextFieldView.snp.makeConstraints { make in
            make.top.equalTo(25)
            make.leading.trailing.equalToSuperview().inset(24)
        }

        emailTextField.snp.makeConstraints { make in
            make.edges.equalTo(phoneTextFieldView)
        }

        forgotPasswordButton.snp.makeConstraints { make in
            make.top.equalTo(phoneTextFieldView.snp.bottom).offset(UIScreen.isSE ? 11 : 16)
            make.leading.equalToSuperview().inset(24)
        }

        registrationButtonView.snp.makeConstraints { make in
            make.top.equalTo(forgotPasswordButton.snp.bottom).offset(16)
            make.leading.trailing.equalToSuperview().inset(23)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 35 : 40)
        }

        warningLabel.snp.makeConstraints { (make) in
            make.top.equalTo(registrationButtonView.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(14)
        }
    }

    private func bindUI() {
        phoneTextFieldView
            .phoneString
            .bind(to: textRelay)
            .disposed(by: bag)

        emailTextField
            .text
            .bind(to: textRelay)
            .disposed(by: bag)

        isEmailReset.bind { [weak self] isEmail in
            guard let self = self else { return }
            self.emailTextField.isHidden = !isEmail
            self.emailTextField.textField.text = ""
            self.phoneTextFieldView.isHidden = isEmail
            self.phoneTextFieldView.setText("")
            self.textRelay.accept("")
        }
        .disposed(by: bag)
    }
}

extension EnterPasswordTableCell: UITextFieldDelegate {

    func bindTextField() {
        emailTextField.textField.delegate = self

        emailTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe(onNext: { [weak self] in
                self?.buttonTapped.accept(())
            }).disposed(by: bag)
    }
}
