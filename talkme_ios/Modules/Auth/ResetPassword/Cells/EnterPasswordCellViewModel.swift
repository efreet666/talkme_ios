//
//  EnterPasswordCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.03.2021.
//

import RxCocoa
import RxSwift

final class EnterPasswordCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { 200 }

    let textRelay = PublishRelay<String>()
    let countryRelay = PublishRelay<CountryType>()
    let sendCodeTapRelay = PublishRelay<Void>()
    let buttonColorRelay = PublishRelay<ConfirmButtonColor>()
    let onCountryButtonTap = PublishRelay<Void>()
    let error = PublishRelay<AuthError>()
    let isEmailReset = PublishRelay<Bool>()
    let forgotButtonTap = PublishRelay<Void>()
    let buttonTapped = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: EnterPasswordTableCell) {
        cell
            .textRelay
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .sendCodeTap
            .bind(to: sendCodeTapRelay)
            .disposed(by: cell.bag)

        cell
            .textRelay
            .filter { !$0.isEmpty }
            .map { _ in AuthError.none }
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        cell
            .buttonTapped
            .bind(to: sendCodeTapRelay)
            .disposed(by: cell.bag)

        cell
            .textRelay
            .filter { $0.isEmpty }
            .map { _ in AuthError.dataRequirement }
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        cell
            .textRelay
            .map { [weak cell] _ in
                cell?.isValidNumber ?? false
            }
            .bind(to: cell.buttonIsEnabled)
            .disposed(by: cell.bag)

        cell
            .textRelay
            .map { [weak cell] _ -> ConfirmButtonColor in
                cell?.isValidNumber ?? false ? .activeColor : .inactiveColor
            }
            .map { $0.backgroundColor }
            .bind(to: cell.buttonBackgroundColor)
            .disposed(by: cell.bag)

        cell
            .selectCountryTap
            .bind(to: onCountryButtonTap)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        countryRelay
            .bind { [weak cell] type in
                cell?.currentCountry = type
            }
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.code)
            .bind(to: cell.countryCodeText)
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.image)
            .bind(to: cell.countryCodeImage)
            .disposed(by: cell.bag)

        cell
            .forgotButtonTap
            .bind(to: forgotButtonTap)
            .disposed(by: cell.bag)

        isEmailReset
            .bind(to: cell.isEmailReset)
            .disposed(by: cell.bag)

        cell.configure()
    }
}
