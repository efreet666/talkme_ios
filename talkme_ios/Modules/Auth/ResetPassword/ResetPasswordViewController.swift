//
//  PasswordRecoveryController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.03.2021.
//

import RxCocoa
import RxSwift

final class ResetPasswordViewController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.layer.backgroundColor = UIColor.clear.cgColor
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        if AppDelegate.addSocialSignIn {
        tbv.registerCells(
            withModels: TalkMeLogoCellViewModel.self,
            TitleTableCellViewModel.self,
            DescriptionRegistrationTableCellViewModel.self,
            EmptyTableCellViewModel.self,
            EnterPasswordCellViewModel.self,
            SocialsTableCellViewModel.self,
            LoginCellViewModel.self,
//            EmptyTableCellViewModel.self
            UserRegistrationButtonsCellViewModel.self
        )
        } else {
            tbv.registerCells(
                withModels: TalkMeLogoCellViewModel.self,
                TitleTableCellViewModel.self,
                DescriptionRegistrationTableCellViewModel.self,
                EmptyTableCellViewModel.self,
                EnterPasswordCellViewModel.self,
                LoginCellViewModel.self,
//                EmptyTableCellViewModel.self
                UserRegistrationButtonsCellViewModel.self
            )
        }
        return tbv
    }()

    private let backgroundImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "mainBackground")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let viewModel: ResetPasswordViewModel
    private let bag = DisposeBag()
    private let smallTopInset: CGFloat = UIScreen.isSE ? -100 : -150
    private let largeTopInset: CGFloat = 0

    // MARK: - Initializers

    init(viewModel: ResetPasswordViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        setupUI()
        setupImageView()
        bindVM()
        setupTableView()
        bindKeyboard()
        hideKeyboardWhenTappedAround()
    }

    // MARK: - Public Methods

    @objc func dismissKeyboard() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .items
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupImageView() {
        view.addSubview(backgroundImage)

        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupTableView() {
        view.addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalToSuperview()
        }
    }

    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let isHide = keyboardVisibleHeight == 0
                let emptyConstants: CGFloat = UIScreen.isSE ? 1 : 40
                self.viewModel.topLogoInset.accept(!isHide ? self.smallTopInset : self.largeTopInset)
                self.viewModel.emptyTopInset.accept(!isHide ? emptyConstants : 0)
                self.tableView.contentInset.bottom = !isHide
                    ? self.tableView.rowHeight - keyboardVisibleHeight
                    : keyboardVisibleHeight
                self.tableView.snp.updateConstraints { make in
                    make.bottom.equalToSuperview().inset(keyboardVisibleHeight)
                }
                if !isHide {
                    self.tableView.scrollTo(edge: .bottom, animated: true)
                }
                UIView.animate(withDuration: 0) {
                    self.tableView.beginUpdates()
                    self.view.layoutIfNeeded()
                    self.tableView.endUpdates()
                }
            })
            .disposed(by: bag)
    }

    func setupUI() {
        navigationController?.navigationBar.backItem?.title = ""// "talkme_navigation_back".localized
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .clear
    }
}
