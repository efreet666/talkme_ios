//
//  RegistrationConfirmCodeViewController.swift
//  talkme_ios
//
//  Created by dream.one on 14.12.2020.
//

import RxCocoa
import RxSwift
import UIKit

final class RegistrationConfirmCodeViewController: UIViewController {

    private enum Constants {
        static let codeLength = 4
    }

    // MARK: - Private Properties

    private var rxTimer: Disposable?
    private let viewModel: RegistrationConfirmCodeViewModel
    private var phoneBottomConstraint = NSLayoutConstraint()
    private let bag = DisposeBag()
    private let codeView: ConfirmationCodeView = {
        let cv = ConfirmationCodeView(codeLength: Constants.codeLength)
        return cv
    }()

    private var useLastFourNumbersLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        label.text = "talkme_сonfirm_сode_enter_code_text".localized
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = .montserratBold(ofSize: 17)
        return label
    }()

    // uncommit this to enable fully customizeble phoneNumberView, that is not just a picture
    // private var phoneNumberView = IncomingCallView()

    private lazy var phoneNumberView: UIImageView = {
        let image = UIImage(named: "incomingCallFull")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 32
        imageView.layer.masksToBounds = true
        return imageView
    }()

    private lazy var containerForCalImageView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 12
        view.layer.masksToBounds = true
        view.addSubview(phoneNumberView)
        phoneNumberView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        return view
    }()

    private let logoImage: UIImageView = {
        let logo = UIImageView()
        logo.image = UIImage(named: "talkme")
        logo.contentMode = .scaleAspectFit
        return logo
    }()

    private let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "mainBackgroundWithoutGradient")
        image.contentMode = UIView.ContentMode.scaleAspectFill
        return image
    }()

    private let mainTextLabel: UILabel = {
        let label = UILabel()
        label.text = "talkme_сonfirm_сode_main_text".localized
        label.textColor = TalkmeColors.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.montserratFontMedium(ofSize: 15)
        return label
    }()

    private let sendCodeAgainButton: UIButton = {
        let buttont = UIButton(type: .system)
        buttont.setTitle("talkme_сonfirm_сode_send_code_again".localized, for: .normal)
        buttont.setTitleColor(TalkmeColors.white, for: .normal)
        buttont.backgroundColor = TalkmeColors.deepBlue
        buttont.titleLabel?.font = .montserratExtraBold(ofSize: 15)
        buttont.titleLabel?.textAlignment = .center
        buttont.backgroundColor = TalkmeColors.blueLabels
        buttont.alpha = 0.3
        return buttont
    }()

    private let confirmButton: UIButton = {
        let button = ConfirmationButton(type: .system)
        button.setTitleColor(TalkmeColors.white, for: .normal)
        button.setTitle("talkme_сonfirm_сode_confirm".localized, for: .normal)
        button.alpha = 0
        return button
    }()

    private let errorLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.error
        lbl.font = UIFont.montserratFontMedium(ofSize: 11)
        lbl.textAlignment = .center
        lbl.text = ""
        return lbl
    }()

    private let timerLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.white
        lbl.text = ""
        lbl.numberOfLines = 0
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    private let lastFourNumbersImageView: UIImageView = {
        let image = UIImage(named: "lastFourNumbers")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let willReciveCallImageView: UIImageView = {
        let image = UIImage(named: "willReciveCall")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        bindUI()
        setConstraints()
        bindKeyboard()
        bindVM()
        setupTimer()
        // uncomit this to set users phone number to phoneNumberView
        // phoneNumberView.setPhoneNumber(to: viewModel.phoneNumber)
        view.layoutIfNeeded()
        hideKeyboardWhenTappedAround()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if sendCodeAgainButton.layer.cornerRadius == 0 {
            sendCodeAgainButton.layer.cornerRadius = sendCodeAgainButton.bounds.height / 2
        }
    }

    // MARK: - Initializers

    init(viewModel: RegistrationConfirmCodeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .error
            .bind { [weak self] in
                self?.animateError($0)
            }
            .disposed(by: bag)
    }

    private func animateError(_ error: AuthError) {
        let text = error.title
        guard text != errorLabel.text else { return }

        let isHidden = text == nil
        UIView.transition(
            with: errorLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.errorLabel.alpha = isHidden ? 0 : 1
            self.errorLabel.text = text
        }
    }

    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    private func animateButton(isHidden: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.confirmButton.alpha = isHidden ? 0 : 1
        }
    }

    private func bindUI() {
        codeView
            .unfinishedCodeValue
            .filter { $0 != nil }
            .bind { [weak self] _ in
                self?.animateError(.none)
            }
            .disposed(by: bag)

        codeView
            .codeValue
            .bind { [weak self] code in
                self?.animateButton(isHidden: code == nil)
            }
            .disposed(by: bag)

        confirmButton.rx
            .tap
            .withLatestFrom(codeView.codeValue)
            .bind { [weak self] code in
                guard let code = code else { return }
                self?.viewModel.confirmCode(code)
            }
            .disposed(by: bag)

        sendCodeAgainButton.rx
            .tap
            .bind { [weak self] _ in
                self?.sendCodeAgainButton.alpha = 0.3
                self?.setupTimer()
                self?.viewModel.sendMobileCode()
            }
            .disposed(by: bag)
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { _ in // [weak self] keyboardVisibleHeight in
//                guard let self = self else { return }
//                let isVisible = keyboardVisibleHeight > 0
//                self.phoneBottomConstraint.isActive = !isVisible
//                self.mainTextLabel.isHidden = isVisible
//                self.phoneNumberLabel.isHidden = isVisible
            })
            .disposed(by: bag)
    }

    private func setupTimer() {
        rxTimer = Observable<Int>.interval(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .map {-$0 + 59}
            .map { [weak self] timerValue in
                guard let self = self else { return "" }
                let seconds = (timerValue % 3600) % 60
                let stringFormatted = "talkme_сonfirm_сode_next_call_try_is_enable".localized + "\n" + String(format: "00:%02d", seconds)
                if timerValue != 0 {
                    self.sendCodeAgainButton.isEnabled = false
                    return stringFormatted
                } else {
                    self.rxTimer?.dispose()
                    self.timerLabel.text = ""
                    self.errorLabel.text = "talkme_сonfirm_сode_error_minute".localized
                    self.sendCodeAgainButton.isEnabled = true
                    self.sendCodeAgainButton.alpha = 1
                    return ""
                }
            }.subscribe(onNext: { [weak self] value in
                self?.timerLabel.text = value
            })
    }

    private func setConstraints() {
        view.addSubviewsWithoutAutoresizing(
            backgroundImage,
            logoImage,
//            mainTextLabel,
//            phoneNumberView,
            containerForCalImageView,
            codeView,
//            useLastFourNumbersLabel,
            errorLabel,
            confirmButton,
            sendCodeAgainButton,
            timerLabel,
            lastFourNumbersImageView,
            willReciveCallImageView
        )

        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            logoImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 66),
            logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImage.heightAnchor.constraint(equalToConstant: 71),
            logoImage.widthAnchor.constraint(equalToConstant: 123),

            // uncommit this if you whant to replace lastFourNumbersImageView to mainTextLabel
//            mainTextLabel.topAnchor.constraint(equalTo: logoImage.bottomAnchor, constant: 39),
//            mainTextLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//            mainTextLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            lastFourNumbersImageView.topAnchor.constraint(equalTo: logoImage.bottomAnchor, constant: UIScreen.isSE ? 15 : 25),
            lastFourNumbersImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: UIScreen.isSE ? 23 : 47),
            lastFourNumbersImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: UIScreen.isSE ? -23 : -47),

            containerForCalImageView.topAnchor.constraint(equalTo: lastFourNumbersImageView.bottomAnchor, constant: 15),
            containerForCalImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25),
            containerForCalImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25),
            containerForCalImageView.heightAnchor.constraint(equalToConstant: 79),

            // uncommit this if you whant to replace willReciveCallImageView to useLastFourNumbersLabel
//            useLastFourNumbersLabel.topAnchor.constraint(equalTo: phoneNumberView.bottomAnchor, constant: 32),
//            useLastFourNumbersLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            willReciveCallImageView.topAnchor.constraint(equalTo: phoneNumberView.bottomAnchor, constant: 32),
            willReciveCallImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            willReciveCallImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),

            codeView.topAnchor.constraint(equalTo: willReciveCallImageView.bottomAnchor, constant: 16),
            codeView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            codeView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            codeView.bottomAnchor.constraint(equalTo: errorLabel.topAnchor, constant: -6),
            codeView.heightAnchor.constraint(equalToConstant: 50),

            errorLabel.topAnchor.constraint(equalTo: codeView.bottomAnchor, constant: 6),
            errorLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            errorLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            errorLabel.heightAnchor.constraint(equalToConstant: 10),
            errorLabel.bottomAnchor.constraint(equalTo: confirmButton.topAnchor, constant: -4),

            confirmButton.heightAnchor.constraint(equalToConstant: 42),
            confirmButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 32),
            confirmButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -32),

            timerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            sendCodeAgainButton.topAnchor.constraint(equalTo: timerLabel.bottomAnchor, constant: 13),
            sendCodeAgainButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 32),
            sendCodeAgainButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -32),
            sendCodeAgainButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -43),
            sendCodeAgainButton.heightAnchor.constraint(equalToConstant: 52)
        ])

        codeView.becomeFirstResponder()
    }
}
