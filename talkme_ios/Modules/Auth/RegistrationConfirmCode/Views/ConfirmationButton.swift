//
//  ConfirmationButton.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.12.2020.
//

import UIKit

final class ConfirmationButton: UIButton {

    private enum Constants {
        static let cornerRadius: CGFloat = 21
        static let borderWidth: CGFloat = 1
        static let borderColor = TalkmeColors.separatorView.cgColor
        static let fontStyle = UIFont.montserratFontBlack(ofSize: 13)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func initialSetup() {
        clipsToBounds = true
        layer.cornerRadius = Constants.cornerRadius
        layer.borderColor = Constants.borderColor
        layer.borderWidth = Constants.borderWidth
        titleLabel?.font = Constants.fontStyle
    }
}
