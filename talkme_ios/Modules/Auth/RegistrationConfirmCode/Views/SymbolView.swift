//
//  ConfirmationCodeView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 21.10.2020.
//  Copyright © 2020 organization. All rights reserved.

import UIKit

final class SymbolView: UIView {

    // MARK: - Public Properties

    var text: String? {
        didSet { symbolLabel.text = text }
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: 50, height: 50)
    }

    // MARK: - Private Properties

    private let symbolLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.montserratFontBlack(ofSize: 24)
        label.textColor = TalkmeColors.white
        label.backgroundColor = .clear
        label.contentMode = .center
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        return label
    }()

    // MARK: - Lifecycle

    init() {
        super.init(frame: .zero)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func configure() {
        backgroundColor = TalkmeColors.white
        clipsToBounds = true
        isUserInteractionEnabled = false
        layer.cornerRadius = 25
        symbolLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(symbolLabel)
        NSLayoutConstraint.activate([
            symbolLabel.widthAnchor.constraint(equalToConstant: 50),
            symbolLabel.heightAnchor.constraint(equalToConstant: 50),
            symbolLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            symbolLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}
