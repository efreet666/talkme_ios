//
//  IncomingCallView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 9/9/2022.
//

import UIKit
import SnapKit

fileprivate extension Consts {
    static let cornerRadius = 12

    static let phoneNumberLabelFontSize: CGFloat = 20
    static let incomeCallLabelFontSize: CGFloat = 15

    static let phoneImageViewTopInset: CGFloat = 21
    static let phoneImageViewBottomInset: CGFloat = -21
    static let phoneImageViewLeadingInset: CGFloat = 20
    static let phoneImageViewHeight: CGFloat = 37

    static let phoneNumberLabelViewTopInset: CGFloat = 16
    static let phoneNumberLabelLeadingInset: CGFloat = 16
    static let phoneNumberLabelTrailingInset: CGFloat = -10// -103

    static let incomeCallLabelViewTopInset: CGFloat = 6
    static let incomeCallLabelLeadingInset: CGFloat = 16
    static let incomeCallLabelViewTrailingInset: CGFloat = -10 // -144
    static let incomeCallLabelViewBottomInset: CGFloat = -16
}

class IncomingCallView: UIView {
    // MARK: - private propertyes

    private var phoneImageView: UIImageView = {
        let phoneImage = UIImage(named: "incomingCall")
        let imageView = UIImageView(image: phoneImage)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .montserratSemiBold(ofSize: Consts.phoneNumberLabelFontSize)
        return label
    }()

    private var incomeCallLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.black
        label.textAlignment = .left
        label.text = "talkme_сonfirm_сode_incoming_call".localized
        label.numberOfLines = 0
        label.font = .montserratFontRegular(ofSize: Consts.incomeCallLabelFontSize)
        return label
    }()

    // MARK: - inits

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 12
        backgroundColor = TalkmeColors.white
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - private methods

    private func setupLayout() {
        addSubviews([phoneImageView,
                     phoneNumberLabel,
                     incomeCallLabel
        ])

        phoneImageView.snp.makeConstraints { make in
            make.top.equalTo(self).offset(Consts.phoneImageViewTopInset)
            make.leading.equalTo(self).offset(Consts.phoneImageViewLeadingInset)
            make.bottom.equalTo(self).offset(Consts.phoneImageViewBottomInset)
            make.height.width.equalTo(Consts.phoneImageViewHeight)
        }

        phoneNumberLabel.snp.makeConstraints { make in
            make.top.equalTo(self).offset(Consts.phoneNumberLabelViewTopInset)
            make.leading.equalTo(phoneImageView.snp.trailing).offset(Consts.phoneNumberLabelLeadingInset)
            make.trailing.equalTo(self).offset(Consts.phoneNumberLabelTrailingInset)
        }

        incomeCallLabel.snp.makeConstraints { make in
            make.top.equalTo(phoneNumberLabel.snp.bottom).offset(Consts.incomeCallLabelViewTopInset)
            make.trailing.equalTo(self).offset(Consts.incomeCallLabelViewTrailingInset)
            make.leading.equalTo(phoneNumberLabel)
            make.bottom.equalTo(self).offset(Consts.incomeCallLabelViewBottomInset)
        }
    }

    // MARK: - public methods

    func setPhoneNumber(to phoneNumber: String) {
        phoneNumberLabel.text = phoneNumber
    }
}
