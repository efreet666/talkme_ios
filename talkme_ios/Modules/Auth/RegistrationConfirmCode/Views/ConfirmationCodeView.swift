//
//  ConfirmationCodeView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.12.2020.

import RxSwift
import RxCocoa

final class ConfirmationCodeView: UIView {

    // MARK: - Public Properties

    let codeValue = BehaviorRelay<String?>(value: nil)
    let unfinishedCodeValue = PublishRelay<String?>()

    override var isFirstResponder: Bool {
        return fakeTextField.isFirstResponder
    }

    override var canBecomeFirstResponder: Bool { return fakeTextField.canBecomeFirstResponder }

    // MARK: - Private Properties

    private let codeLength: Int
    private let symbolViews: [SymbolView]

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillEqually
        sv.spacing = 7
        sv.isUserInteractionEnabled = false
        return sv
    }()

    private lazy var fakeTextField: FakeTextField = {
        let tf = FakeTextField()
        tf.alpha = 0
        tf.keyboardType = .numberPad
        tf.delegate = self
        tf.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        return tf
    }()

    private var currentCodeValue: String?

    // MARK: - Initializers

    init(codeLength: Int) {
        self.codeLength = codeLength
        self.symbolViews = (0..<codeLength).map { _ in return SymbolView() }
        super.init(frame: .zero)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func clear() {
        fakeTextField.text = nil
        fakeTextField.sendActions(for: .editingChanged)
    }

    @discardableResult override func becomeFirstResponder() -> Bool {
        return fakeTextField.becomeFirstResponder()
    }

    @discardableResult override func resignFirstResponder() -> Bool {
        return fakeTextField.resignFirstResponder()
    }

    // MARK: - Private Methods

    @objc private func tapAction() {
        let menuController = UIMenuController.shared
        let rect = CGRect(origin: CGPoint(x: bounds.width / 2, y: 0), size: .zero)
        menuController.setTargetRect(rect, in: self)
        menuController.setMenuVisible(true, animated: true)
        becomeFirstResponder()
    }

    private func configure() {
        addSubview(fakeTextField)
        addSubview(stackView)

        stackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
        }
        symbolViews.forEach(stackView.addArrangedSubview)

        fakeTextField.snp.makeConstraints { make in
            make.edges.equalTo(stackView)
        }

        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapAction)))
    }
}

extension ConfirmationCodeView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.count > 1 {
            fakeTextField.text = String(string.prefix(codeLength))
            fakeTextField.sendActions(for: .editingChanged)
            return true
        } else {
            let currCount = textField.text?.count ?? 0
            let endCount = currCount - range.length + string.count
            return endCount <= codeLength
        }
    }

    // MARK: - Private Methods

    @objc
    private func textDidChange(_ tf: UITextField) {
        if !((tf.text?.rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789"), options: .caseInsensitive)) != nil) {
            tf.text = ""
        }
        guard tf.text != currentCodeValue else { return }

        UIMenuController.shared.setMenuVisible(false, animated: true)
        let becomeCompleted = (tf.text?.count ?? 0) == codeLength
        let currentCompleted = (currentCodeValue?.count ?? 0) == codeLength
        currentCodeValue = tf.text ?? ""
        updateVisible()
        updateCursor()
        if becomeCompleted {
            codeValue.accept(currentCodeValue)
        } else if currentCompleted {
            codeValue.accept(nil)
        }
        unfinishedCodeValue.accept(currentCodeValue)
    }

    private func updateVisible() {
        symbolViews.forEach {
            $0.text = nil
            $0.backgroundColor = TalkmeColors.white
        }
        let symbols = Array(currentCodeValue ?? "")
        zip(symbolViews, symbols).forEach {
            $0.0.text = String($0.1)
            $0.0.backgroundColor = TalkmeColors.symbolView
        }
    }

    private func updateCursor() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            self.fakeTextField.selectedTextRange = self.fakeTextField.textRange(
                from: self.fakeTextField.endOfDocument,
                to: self.fakeTextField.endOfDocument)
        }
    }
}
