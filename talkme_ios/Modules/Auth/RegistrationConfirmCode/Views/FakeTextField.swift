//  FakeTextField.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.12.2020.
//
//

import UIKit

final class FakeTextField: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(paste(_:))
    }
}
