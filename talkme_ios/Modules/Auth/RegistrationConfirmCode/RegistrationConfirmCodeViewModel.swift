//
//  RegistrationConfirmCodeViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 24.12.2020.
//
import RxCocoa
import RxSwift
import Moya

enum ConfirmCodeFlow: Equatable {
    case onCodeConfirm(phoneNumber: String)
    case networkErrorPopUp(errorMessage: String)
}

final class RegistrationConfirmCodeViewModel {

    // MARK: - Private properties

    private let service: AuthServiceProtocol
    private var maxCountOfNetworkRetries = 1
    private var currentCountOfNetworkRetries = 0

    // MARK: - Public properties

    let phoneNumber: String
    let flow = PublishRelay<ConfirmCodeFlow>()
    let bag = DisposeBag()

    let error = PublishRelay<AuthError>()

    init(service: AuthServiceProtocol, phoneNumber: String) {
        self.service = service
        self.phoneNumber = phoneNumber
    }

    func confirmCode(_ code: String) {
        let request = SendMobileAndCodeRequest(code: code, mobile: phoneNumber)
        service
            .sendMobileAndCode(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let phoneNumber = response.mobile else {
                        self?.error.accept(.apiError(response.msg))
                        return
                    }
                    self?.flow.accept(.onCodeConfirm(phoneNumber: phoneNumber))
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.confirmCode(code)},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "61")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func sendMobileCode() {
        let request = SendMobileRequest(mobile: phoneNumber)
        service
            .getMobileCode(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    if let errorMessage = response.msg {
                        self?.error.accept(.apiError(errorMessage))
                        return
                    }
                    print(response)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.sendMobileCode()},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "62")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)"))
            }
        }
    }
}
