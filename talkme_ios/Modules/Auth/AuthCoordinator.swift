//
//  AuthCoordinator.swift
//  talkme_ios
//
//  Created by Yura Fomin on 23.12.2020.
//

import RxCocoa
import RxSwift

final class AuthCoordinator {

    // MARK: - Private Properties

    private weak var onLoggedIn: PublishRelay<Void>?
    private let service: AuthServiceProtocol = AuthService()
    private let appleService = AppleIDSignInService()
    private let googleService = GoogleSignInService()
    private let vkService = VKSwiftySignInService()
    private let yandexService = YandexSignInService()
    private let mailRuService = MailRuSignInService()
    private weak var navigationController: UINavigationController?
    private weak var authController: AuthorizationViewController?

    // MARK: - Public Methods

    func start(onLoggedIn: PublishRelay<Void>) -> UINavigationController {
        self.onLoggedIn = onLoggedIn
        let viewModel = AuthorizationViewModel(service: service)
        let controller = AuthorizationViewController(viewModel: viewModel)
        let navigationController = SimpleNavController(rootViewController: controller)
        navigationController.setupUI()
        self.navigationController = navigationController
        self.authController = controller
        bindAuthorizationViewModel(viewModel: viewModel)
        return navigationController
    }

    // MARK: - Private Methods

    private func createAuthController() -> AuthorizationViewController {
        let viewModel = AuthorizationViewModel(service: service)
        let controller = AuthorizationViewController(viewModel: viewModel)
        controller.setupUI()
        bindAuthorizationViewModel(viewModel: viewModel)
        self.authController = controller
        return controller
    }

    private func showAuthEnterData(phoneNumber: String? = nil, email: String? = nil) {
        let viewModel = AuthEnterDataViewModel(service: service, phoneNumber: phoneNumber, email: email)
        let controller = AuthEnterDataController(viewModel: viewModel)
        bindAuthEnterDataViewModel(viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func registrationVC() {
        let viewModel = RegistrationControllerViewModel(service: service)
        let controller = RegistrationController(viewModel: viewModel)
        bindEnterPhoneViewModel(viewModel: viewModel)
        let authController = createAuthController()
        navigationController?.setViewControllers([authController, controller], animated: true)
    }

    private func emailChangePassword(_ email: String) {
        let viewModel = ResetPasswordEmailViewModel(service: service, appleService: appleService, googleService: googleService, email: email)
        let viewController = ResetPasswordEmailViewController(viewModel: viewModel)
        bindResetPasswordEmailViewModel(viewModel: viewModel)
        navigationController?.setViewControllers([viewController], animated: true)
    }

    private func showCountriesScreen(regViewModel: RegistrationControllerViewModel) {
        let viewModel = CountryCodesViewModel()
        let controller = CountryCodesViewController(viewModel: viewModel)
        bindCountryCodeViewModel(viewModel: viewModel, regViewModel: regViewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showCountriesSingIn(regViewModel: SignInViewModel) {
        let viewModel = CountryCodesViewModel()
        let controller = CountryCodesViewController(viewModel: viewModel)
        bindCountryCodeSignInViewModel(viewModel: viewModel, regViewModel: regViewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showCountriesResetPassword(passwordViewModel: ResetPasswordViewModel) {
        let viewModel = CountryCodesViewModel()
        let controller = CountryCodesViewController(viewModel: viewModel)
        bindCountryCodeResetPassword(viewModel: viewModel, passwordViewModel: passwordViewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showCodeVC(phoneNumber: String) {
        let viewModel = RegistrationConfirmCodeViewModel(service: service, phoneNumber: phoneNumber)
        let controller = RegistrationConfirmCodeViewController(viewModel: viewModel)
        bindEnterCodeViewModel(viewModel: viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showSignIn() {
        let viewModel = SignInViewModel(service: service,
                                        appleService: appleService,
                                        googleService: googleService,
                                        vkService: vkService,
                                        yandexService: yandexService,
                                        mailRuService: mailRuService)
        let controller = SignInController(viewModel: viewModel)
        let authController = createAuthController()
        bindBackRegistration(viewModel: viewModel, authController: authController)
        navigationController?.setViewControllers([authController, controller], animated: true)
    }

    private func showResetPassword() {
        let viewModel = ResetPasswordViewModel(service: service, appleService: appleService, googleService: googleService)
        let controller = ResetPasswordViewController(viewModel: viewModel)
        bindResetPasswordViewModel(viewModel: viewModel)
        let authController = createAuthController()
         navigationController?.setViewControllers([authController, controller], animated: true)
    }

    private func showResetPassowrdConfirmCode(phoneNumber: String) {
        let viewModel = ResetPasswordConfirmCodeViewModel(
            phoneNumber: phoneNumber,
            service: service,
            appleService: appleService,
            googleService: googleService)
        let controller = ResetPasswordConfirmCodeVC(viewModel: viewModel)
        bindResetPasswordConfirmCodeViewModel(viewModel: viewModel)
        navigationController?.pushViewController(controller, animated: true)
        // navigationController?.setViewControllers([controller], animated: true)
    }

    private func showPasswordChange(userToken: String, phoneNumber: String) {
        let viewModel = ChangePasswordViewModel(
            userToken: userToken,
            phoneNumber: phoneNumber,
            service: service,
            appleService: appleService,
            googleService: googleService)
        let controller = ChangePasswordController(viewModel: viewModel)
        bindPasswordChangeViewModel(viewModel: viewModel)
        navigationController?.pushViewController(controller, animated: true)
//        navigationController?.setViewControllers([controller], animated: true)
    }

    private func showAppRules() {
        let controller = AppRulesVC(urlString: "https://\(Configuration.base)/media/main/TALKME.pdf")
        let navVC = UINavigationController(rootViewController: controller)
        navVC.modalPresentationStyle = .fullScreen
        authController?.present(navVC, animated: true)
    }

    private func bindEnterPhoneViewModel(viewModel: RegistrationControllerViewModel) {
        viewModel
            .flow
            .bind { [weak self, weak viewModel] flow in
                guard let regViewModel = viewModel else { return }
                switch flow {
                case .onEnterSMSCode(let phoneNumber):
                    self?.showCodeVC(phoneNumber: phoneNumber)
                case .onShowCountries:
                    self?.showCountriesScreen(regViewModel: regViewModel)
                case .onSignIn:
                    self?.showSignIn()
                case .onSignUp(let email):
                    self?.showAuthEnterData(email: email)
                case .networkErrorPopUp(let errorMessage):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: {})
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindResetPasswordEmailViewModel(viewModel: ResetPasswordEmailViewModel) {
        viewModel
            .flow
            .bind { [weak self, weak viewModel] flow in
                guard viewModel != nil else { return }
                switch flow {
                case .backToAuth:
                    self?.showSignIn()
                case .cancelResetPassword:
                    self?.showSignIn()
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindEnterCodeViewModel(viewModel: RegistrationConfirmCodeViewModel) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .onCodeConfirm(let phoneNumber):
                    self?.showAuthEnterData(phoneNumber: phoneNumber)
                case .networkErrorPopUp(let errorMessage):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: {})
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindAuthEnterDataViewModel(_ viewModel: AuthEnterDataViewModel) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .signIn:
                    self?.onLoggedIn?.accept(())
                    AmplitudeProvider.shared.loginTracking()
                    AppsFlyerProvider.shared.trackUserCompletedLoggin()
                case .networkErrorPopUp(let errorMessage):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: {})
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindBackRegistration(viewModel: SignInViewModel, authController: AuthorizationViewController) {
        viewModel
            .flow
            .bind { [weak self, weak viewModel] flow in
                guard let viewModel = viewModel else { return }
                switch flow {
                case .onRegistrationTap:
                    self?.registrationVC()
                case .onLoggedIn:
                    self?.onLoggedIn?.accept(())
                case .onShowCountries:
                    self?.showCountriesSingIn(regViewModel: viewModel)
                case .onResetPassword:
                    self?.showResetPassword()
                case .networkErrorPopUp(let errorMessage):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: {})
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindAuthorizationViewModel(viewModel: AuthorizationViewModel) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .onShowRegistration:
                    self?.registrationVC()
                case .onShowLogin:
                    self?.showSignIn()
                case .onAuthRules:
                    self?.showAppRules()
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindCountryCodeViewModel(viewModel: CountryCodesViewModel, regViewModel: RegistrationControllerViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self, weak regViewModel] flow in
                guard let regViewModel = regViewModel else { return }
                switch flow {
                case .setImageAndCode(let country):
                    regViewModel.changeCountry(counrty: country)
                    self?.navigationController?.popViewController(animated: false)
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func bindCountryCodeSignInViewModel(viewModel: CountryCodesViewModel, regViewModel: SignInViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self, weak regViewModel] flow in
                guard let regViewModel = regViewModel else { return }
                switch flow {
                case .setImageAndCode(let country):
                    regViewModel.changeCountry(counrty: country)
                    self?.navigationController?.popViewController(animated: false)
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func bindCountryCodeResetPassword(viewModel: CountryCodesViewModel, passwordViewModel: ResetPasswordViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self, weak passwordViewModel] flow in
                guard let passwordViewModel = passwordViewModel else { return }
                switch flow {
                case .setImageAndCode(let country):
                    passwordViewModel.changeCountry(counrty: country)
                    self?.navigationController?.popViewController(animated: false)
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func bindResetPasswordViewModel(viewModel: ResetPasswordViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self, weak viewModel] flow in
                guard let viewModel = viewModel else { return }
                switch flow {
                case .onEnterSMSCode(let phoneNumber):
                    self?.showResetPassowrdConfirmCode(phoneNumber: phoneNumber)
                case .onRegistrationTap:
                    self?.registrationVC()
                case .onShowCountries:
                    self?.showCountriesResetPassword(passwordViewModel: viewModel)
                case .cancelResetPassword:
                    self?.showSignIn()
                case .email(let email):
                    self?.emailChangePassword(email)
                case .networkErrorPopUp(let errorMessage):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: {})
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func bindResetPasswordConfirmCodeViewModel(viewModel: ResetPasswordConfirmCodeViewModel) {
            viewModel
                .flow
                .bind(onNext: { [weak self] flow in
                    switch flow {
                    case .onCodeConfirm(let userToken, let phoneNumber):
                        self?.showPasswordChange(userToken: userToken, phoneNumber: phoneNumber)
                    case .cancelResetPassword:
                        self?.showSignIn()
                    case .networkErrorPopUp(let errorMessage):
                        AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: {})
                    }
                })
                .disposed(by: viewModel.bag)
    }

    private func bindPasswordChangeViewModel(viewModel: ChangePasswordViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self] flow in
                switch flow {
                case .onLoggedIn:
                    self?.onLoggedIn?.accept(())
                case .onSignIn:
                    self?.showSignIn()
                case .onRegistrationTap:
                    self?.registrationVC()
                case .networkErrorPopUp(let errorMessage):
                    AlertControllerHelper.showNetworkErrorAlert(errorMessage: errorMessage, action: {})
                }
            })
            .disposed(by: viewModel.bag)
    }
}
