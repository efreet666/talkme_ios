//
//  SignInViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.12.2020.
//

import RxCocoa
import RxSwift
import Moya

enum SignInFlow {
    case onRegistrationTap
    case onLoggedIn
    case onShowCountries
    case onResetPassword
    case networkErrorPopUp(errorMessage: String)
}

final class SignInViewModel {

    // MARK: - Public Properties

    let flow = PublishRelay<SignInFlow>()
    let bag = DisposeBag()
    let heightLogoUpdate = PublishRelay<CGFloat>()
    let topLogoUpdate = PublishRelay<CGFloat>()
    let heightTitleCellUpdate = PublishRelay<CGFloat>()
    let heightUserRegistratinCellUpdate = PublishRelay<CGFloat>()
    let backHeihgtUserRegistrationCell = PublishRelay<Void>()

    // MARK: - Private Properties

    private let appleService: AppleIDSignInService
    private let googleService: GoogleSignInService
    private let vkService: VKSwiftySignInService
    private let yandexService: YandexSignInService
    private let mailRuService: MailRuSignInService
    private var service: AuthServiceProtocol
    private let talkMeLogoCellViewModel = TalkMeLogoCellViewModel()
    private let emptyUpdateCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? 1 : 0)
    private let titleLabelCellViewModel = TitleTableCellViewModel(titleText: "talkme_auth_login".localized)
    private let signInLoginCellViewModel = SignInCellViewModel()
    private let emailOrPhoneNumberViewModel = UserRegistrationButtonsCellViewModel()
    private lazy var emptyCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? emptySpaceHeight + 30                                                                 : emptySpaceHeight - 56)
    private let loginCellViewModel = LoginCellViewModel(title: "talkme_sign_in_not_registrated".localized,
                                                        buttonTitle: "talkme_auth_registration".localized)
    private var country = CountryType.russia.model
    private let socialCellViewModel = SocialsTableCellViewModel(cellType: .signIn)
    private var maxCountOfNetworkRetries = 1
    private var currentCountOfNetworkRetries = 0

    private lazy var emptySpaceHeight: CGFloat = {
        if AppDelegate.addSocialSignIn {
            let allHeights = talkMeLogoCellViewModel.height
                + emptyUpdateCellViewModel.height
                + titleLabelCellViewModel.height
                + signInLoginCellViewModel.height
                + loginCellViewModel.height
                + socialCellViewModel.height
                + emailOrPhoneNumberViewModel.height
            let items = [
                talkMeLogoCellViewModel.height,
                emptyUpdateCellViewModel.height,
                titleLabelCellViewModel.height,
                emailOrPhoneNumberViewModel.height,
                signInLoginCellViewModel.height,
                loginCellViewModel.height
            ]
            let navBarHeight = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.navigationBar.frame.height ?? 0
            let topInset = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return UIScreen.main.bounds.height
                - topInset
                - bottomInset
                - navBarHeight
                - allHeights
        } else {
        let allHeights = talkMeLogoCellViewModel.height
            + emptyUpdateCellViewModel.height
            + titleLabelCellViewModel.height
            + signInLoginCellViewModel.height
            + loginCellViewModel.height

        let items = [
            talkMeLogoCellViewModel.height,
            emptyUpdateCellViewModel.height,
            titleLabelCellViewModel.height,
            signInLoginCellViewModel.height,
            loginCellViewModel.height
        ]
            let navBarHeight = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.navigationBar.frame.height ?? 0
            let topInset = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return UIScreen.main.bounds.height
                - topInset
                - bottomInset
                - navBarHeight
                - allHeights
        }
    }()

    private let isEmailLogin = BehaviorRelay<Bool>(value: false)

    lazy var haveSocial: [AnyTableViewCellModelProtocol] = [
        talkMeLogoCellViewModel,
        emptyUpdateCellViewModel,
        titleLabelCellViewModel,
        emailOrPhoneNumberViewModel,
        signInLoginCellViewModel,
        emptyCellViewModel,
        socialCellViewModel,
        loginCellViewModel
    ]

    lazy var dontHaveSocial: [AnyTableViewCellModelProtocol] =
    [
        talkMeLogoCellViewModel,
        emptyUpdateCellViewModel,
        titleLabelCellViewModel,
        signInLoginCellViewModel,
        emptyCellViewModel,
        loginCellViewModel
    ]

    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>
        .just(AppDelegate.addSocialSignIn ? haveSocial : dontHaveSocial)

    // MARK: - Initializers

    init(service: AuthServiceProtocol,
         appleService: AppleIDSignInService,
         googleService: GoogleSignInService,
         vkService: VKSwiftySignInService,
         yandexService: YandexSignInService,
         mailRuService: MailRuSignInService
    ) {
        self.service = service
        self.appleService = appleService
        self.googleService = googleService
        self.vkService = vkService
        self.yandexService = yandexService
        self.mailRuService = mailRuService
        bind()
    }

    // MARK: - Public Methods

    func changeCountry(counrty: CountryType) {
        self.country = counrty.model
        signInLoginCellViewModel.countryRelay.accept(counrty)
    }

    // MARK: - Private Methods

    private func bind() {
        signInLoginCellViewModel
            .buttonTapRelay
            .bind { [weak self] _ in
                guard let self = self else { return }

                let login = self.signInLoginCellViewModel.loginRelay.value
                let password = self.signInLoginCellViewModel.passwordRelay.value
                self.login(
                    emailOrMobile: self.isEmailLogin.value
                        ? login
                        : self.country.code + login,
                    password: password
                )
            }
            .disposed(by: bag)

        loginCellViewModel
            .tapLoginRelay
            .bind { [weak self] _ in
                self?.flow.accept(.onRegistrationTap)
            }
            .disposed(by: bag)

        signInLoginCellViewModel
            .onCountrySelectRelay
            .map { .onShowCountries }
            .bind(to: flow)
            .disposed(by: bag)

        signInLoginCellViewModel
            .forgotPasswordTap
            .map { .onResetPassword }
            .bind(to: flow)
            .disposed(by: bag)

        if AppDelegate.addSocialSignIn {
            socialCellViewModel
                .buttonTapRelay
                .bind { type in
                    self.socialSignIn(with: type)
                }
                .disposed(by: bag)
        }

        emailOrPhoneNumberViewModel
            .tapEmailRelay
            .map { true }
            .bind(to: isEmailLogin)
            .disposed(by: bag)

        emailOrPhoneNumberViewModel
            .tapNubmerRelay
            .map { false }
            .bind(to: isEmailLogin)
            .disposed(by: bag)

        isEmailLogin
            .bind(to: signInLoginCellViewModel.isEmailLogin)
            .disposed(by: bag)

        heightLogoUpdate
            .bind(to: emptyUpdateCellViewModel.heightUpdate)
            .disposed(by: bag)

        topLogoUpdate
            .bind(to: talkMeLogoCellViewModel.topUpdate)
            .disposed(by: bag)

        heightTitleCellUpdate
            .bind(to: titleLabelCellViewModel.heightUpdate)
            .disposed(by: bag)

        heightUserRegistratinCellUpdate
            .bind(to: emailOrPhoneNumberViewModel.heightUserRegistretion)
            .disposed(by: bag)

        backHeihgtUserRegistrationCell
            .bind(to: emailOrPhoneNumberViewModel.backHeihgtUserRegistrationCell)
            .disposed(by: bag)
    }

    private func login(emailOrMobile: String, password: String) {
        let request = LoginRequest(emailOrMobile: emailOrMobile, password: password, rememberMe: true)
        service
            .login(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.authToken != nil else {
                        self?.signInLoginCellViewModel.error.accept(.apiError(response.errorMessages?.first))
                        return
                    }
                    KeychainManager.save(service: "MobileService", account: "Mobile", data: emailOrMobile)
                    KeychainManager.save(service: "PasswordService", account: "Password", data: password)
                    self?.flow.accept(.onLoggedIn)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.login(emailOrMobile: emailOrMobile, password: password) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "71")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func loginVK(socialToken: String, userId: String) {
        service
            .loginVK(request: SocialSignUpRequest(accessToken: socialToken, code: userId))
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.onLoggedIn)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.loginVK(socialToken: socialToken, userId: socialToken) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "72")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func loginGoogle(socialToken: String, userId: String) {
        service
            .loginGoogle(request: SocialSignUpRequest(accessToken: socialToken, code: userId))
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.onLoggedIn)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.loginGoogle(socialToken: socialToken, userId: userId) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "73")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func loginYandex(socialToken: String, userId: String) {
        service
            .loginYandex(request: SocialSignUpRequest(accessToken: socialToken, code: userId))
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.onLoggedIn)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.loginYandex(socialToken: socialToken, userId: userId) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "74")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func loginMailRu(socialToken: String, userId: String) {
        service
            .loginMailRu(request: SocialSignUpRequest(accessToken: socialToken, code: userId))
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.onLoggedIn)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.loginMailRu(socialToken: socialToken, userId: userId) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "75")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func socialSignIn(with type: SocialButtonType) {
        switch type {
        case .google:
            googleService.signIn()
            socialCellViewModel.returnTokenInViewModelGoogleService(completion: { [weak self] token, idToken in
                guard !token.isEmpty else { return }
                self?.loginGoogle(socialToken: token, userId: idToken)
            })
        case .appleID:
            if #available(iOS 13.0, *) { appleService.signIn() }
        case .facebook:
            FacebookSignInService.signIn()
        case .yandex:
            yandexService.signIn(completion: { [weak self] token, code in
                guard !token.isEmpty else { return }
                self?.loginYandex(socialToken: token, userId: code)
            })
        case .mailru:
            mailRuService.signIn(completion: { [weak self] token, code in
                guard !token.isEmpty else { return }
                self?.loginMailRu(socialToken: token, userId: code)
            })
        case .vkontakte:
            vkService.authorize(completion: { [weak self] token, userId in
                guard !token.isEmpty else { return }
                self?.loginVK(socialToken: token, userId: userId)
            })

        default:
            break
        }
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)"))
            }
        }
    }
}
