//
//  SignInController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.12.2020.
//

import RxCocoa
import RxSwift

final class SignInController: UIViewController {

    // MARK: - Public Properties

    let viewModel: SignInViewModel

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        if AppDelegate.addSocialSignIn {
        tbv.registerCells(
            withModels:
            TalkMeLogoCellViewModel.self,
            EmptyTableCellViewModel.self,
            TitleTableCellViewModel.self,
            SignInCellViewModel.self,
            SocialsTableCellViewModel.self,
            LoginCellViewModel.self,
//            EmptyTableCellViewModel.self
            UserRegistrationButtonsCellViewModel.self
        )
        } else {
            tbv.registerCells(
                withModels:
            TalkMeLogoCellViewModel.self,
            EmptyTableCellViewModel.self,
            TitleTableCellViewModel.self,
            SignInCellViewModel.self,
            LoginCellViewModel.self,
//            EmptyTableCellViewModel.self
            UserRegistrationButtonsCellViewModel.self
                )
        }
        return tbv
    }()

    private let backgroundImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "mainBackground")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let bag = DisposeBag()
    private let heightLogoLarge: CGFloat = UIScreen.isSE ? 1 : 6
    private let heightLogoSmall: CGFloat = 1
    private let topLogoSmallInset: CGFloat = UIScreen.isSE ? -74 : -140
    private let topLogoLargeInset: CGFloat = 5

    // MARK: - Initializers

    init(viewModel: SignInViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        setupImageView()
        setupTableView()
        setupTableViewBinding()
        bindKeyboard()
        setupUI()
        bindUI()
        hideKeyboardWhenTappedAround()
    }

    // MARK: - Public Methods

    @objc func dismissKeyboard() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }

    // MARK: - Private Methods

    private func setupTableViewBinding() {
        viewModel
            .items
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupImageView() {
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundImage)

        backgroundImage.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
    }

    private func setupTableView() {
        view.addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalToSuperview()
        }
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let isHide = keyboardVisibleHeight == 0
                self.viewModel.heightLogoUpdate.accept(!isHide ? self.heightLogoLarge : self.heightLogoSmall)
                self.viewModel.topLogoUpdate.accept(!isHide ? self.topLogoSmallInset : self.topLogoLargeInset)
                self.viewModel.heightTitleCellUpdate.accept(UIScreen.isSE ? 30 : 0)
                self.viewModel.heightUserRegistratinCellUpdate.accept(UIScreen.isSE ? 50 : 60)
                if isHide {
                    self.viewModel.backHeihgtUserRegistrationCell.accept(())
                    }
                self.tableView.contentInset.bottom = !isHide
                    ? self.tableView.rowHeight - keyboardVisibleHeight
                    : keyboardVisibleHeight
                self.tableView.snp.updateConstraints { make in
                    make.bottom.equalToSuperview().inset(keyboardVisibleHeight)
                }
                UIView.animate(withDuration: 0) {
                    self.tableView.beginUpdates()
                    self.view.layoutIfNeeded()
                    self.tableView.endUpdates()
                }
            })
            .disposed(by: bag)
    }

    private func bindUI() {
        view.rx
            .swipeGesture(.down)
            .bind { [weak self] _ in
                self?.view.endEditing(true)
            }
            .disposed(by: bag)
    }

    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    func setupUI() {
        navigationController?.navigationBar.backItem?.title = "talkme_navigation_back".localized
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .clear
    }
}
