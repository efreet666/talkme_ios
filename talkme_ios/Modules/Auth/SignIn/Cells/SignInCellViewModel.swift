//
//  SignInLoginCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.12.2020.
//

import RxCocoa
import RxSwift

final class SignInCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let loginRelay = BehaviorRelay<String>(value: "")
    let passwordRelay = BehaviorRelay<String>(value: "")
    let buttonTapRelay = PublishRelay<Void>()
    let buttonIsActive = BehaviorRelay<Bool>(value: false)
    var height: CGFloat { 207 } // todo: нужно переверстывать под 2 экрана, пока так.
    let error = PublishRelay<AuthError>()
    let countryRelay = PublishRelay<CountryType>()
    let onCountrySelectRelay = PublishRelay<Void>()
    let forgotPasswordTap = PublishRelay<Void>()
    let isEmailLogin = PublishRelay<Bool>()
    let buttonTapped = PublishRelay<Void>()

    // MARK: - Public Methods

    func configure(_ cell: SignInTableCell) {
        cell
            .loginText
            .compactMap { $0 }
            .bind(to: loginRelay)
            .disposed(by: cell.bag)

        cell
            .passwordText
            .bind(to: passwordRelay)
            .disposed(by: cell.bag)

        Observable.combineLatest(cell.loginText, cell.passwordText)
            .map { [weak cell] in
                cell?.isValidNumber ?? false && $0.1.count > 7
            }
            .bind(to: buttonIsActive)
            .disposed(by: cell.bag)

        buttonIsActive
            .bind(to: cell.buttonIsEnabled)
            .disposed(by: cell.bag)

        buttonIsActive
            .bind { [weak cell] _ in
                cell?.setupError(.none)
//                cell?.setupError(active ? .none : .dataRequired)
            }
            .disposed(by: cell.bag)

        buttonIsActive
            .map { $0 ? ConfirmButtonColor.activeColor : ConfirmButtonColor.inactiveColor
            }
            .map { $0.backgroundColor }
            .bind(to: cell.buttonBackgroundColor)
            .disposed(by: cell.bag)

        cell
            .onLoginButtonTap
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)

        cell
            .buttonTapped
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)

        cell
            .onCountrySelect
            .bind(to: onCountrySelectRelay)
            .disposed(by: cell.bag)

        countryRelay
            .bind { [weak cell] type in
                cell?.currentCountry = type
            }
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.code)
            .bind(to: cell.countryCodeText)
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.image)
            .bind(to: cell.countryCodeImage)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        cell
            .forgotPasswordTap
            .bind(to: forgotPasswordTap)
            .disposed(by: cell.bag)

        isEmailLogin
            .bind(to: cell.isEmailLogin)
            .disposed(by: cell.bag)

        cell.configure()
    }
}
