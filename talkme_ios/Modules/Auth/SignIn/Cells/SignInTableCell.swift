//
//  SignInTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.12.2020.
//

import Foundation
import RxCocoa
import RxSwift

final class SignInTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var onLoginButtonTap = registrationButtonView.sendCodeTap
    private(set) lazy var buttonIsEnabled = registrationButtonView.buttonIsEnabled
    private(set) lazy var buttonBackgroundColor = registrationButtonView.buttonBackgroundColor
    let loginText = PublishRelay<String>()
    let isEmailLogin = PublishRelay<Bool>()
    let buttonTapped = PublishRelay<Void>()
    private(set) lazy var onCountrySelect = phoneTextFieldView.selectCountryTap
    private(set) lazy var countryCodeText = phoneTextFieldView.countryCodeText
    private(set) lazy var countryCodeImage = phoneTextFieldView.countryCodeImage
    private(set) lazy var passwordText = passwordTextField.textField.rx.text.orEmpty
    private(set) lazy var forgotPasswordTap = forgotPasswordButton.rx.tap
    private(set) var bag = DisposeBag()

    var isValidNumber: Bool {
        phoneTextFieldView.isValidNumber != (emailTextField.textField.text?.count ?? 0 > 0)
    }

    var currentCountry: CountryType = .russia {
        didSet {
            phoneTextFieldView.region = currentCountry.model.regionCode
        }
    }

    // MARK: - Private Properties

    private let containerView: UIView = {
        let view = UIView()
        view.layer.backgroundColor = TalkmeColors.white.cgColor
        view.layer.cornerRadius = 21
        return view
    }()

    private let forgotPasswordButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("talkme_sign_in_forget_password".localized, for: .normal)
        btn.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        btn.titleLabel?.font = .montserratFontMedium(ofSize: 14)
        return btn
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .center
        lbl.font = .montserratFontMedium(ofSize: 12)
        lbl.alpha = 0
        return lbl
    }()

    private let invalidPasswordLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .left
        lbl.font = .montserratFontMedium(ofSize: 12)
        lbl.text = "talkme_auth_error_password".localized
        return lbl
    }()

    private let registrationButtonView = RegistrationButton()
    private let phoneTextFieldView = NumberTextField()
    private let passwordTextField = TextField()
    private let emailTextField: TextField = {
        let textField = TextField()
        textField.textField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        textField.textField.placeholder = "talkme_auth_enter_data_email".localized
        textField.textField.setEmailTextField()
        textField.setSecure(false)
        return textField
    }()
    private let buttonFont: UIFont = .montserratExtraBold(ofSize: UIScreen.isSE ? 12 : 15)

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure() {
        bindUI()
        bindMailTextField()
        bindPasswordTextField()
        emailTextField.textField.setNextResponder(passwordTextField.textField, disposeBag: bag)
        passwordTextField.textField.returnKeyType = .send
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func setupError(_ error: AuthError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupUI() {
        emailTextField.isHidden = true
        passwordTextField.textField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        passwordTextField.textField.placeholder = "talkme_auth_enter_data_password".localized
        passwordTextField.textField.isSecureTextEntry = true
        registrationButtonView.configure(text: "talkme_auth_login".localized, font: buttonFont)
        invalidPasswordLabel.isHidden = true
    }

    private func bindUI() {
        phoneTextFieldView
            .phoneString
            .bind(to: loginText)
            .disposed(by: bag)

        emailTextField
            .text
            .bind(to: loginText)
            .disposed(by: bag)

        isEmailLogin.bind { [weak self] isEmail in
            guard let self = self else { return }
            self.emailTextField.isHidden = !isEmail
            self.emailTextField.textField.text = ""
            self.phoneTextFieldView.isHidden = isEmail
            self.phoneTextFieldView.setText("")
            self.loginText.accept("")
            self.passwordText.on(.next(""))
        }
        .disposed(by: bag)
    }

    private func setupLayout() {

        [forgotPasswordButton, registrationButtonView,
         phoneTextFieldView, emailTextField, passwordTextField,
         warningLabel, invalidPasswordLabel].forEach(containerView.addSubview(_:))

        contentView.addSubview(containerView)

        containerView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
        }

        phoneTextFieldView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 20 : 25)
            make.leading.trailing.equalToSuperview().inset(25)
            make.bottom.equalTo(passwordTextField.snp.top).inset(UIScreen.isSE ? -20 : -30)
        }

        emailTextField.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 20 : 25)
            make.leading.trailing.equalToSuperview().inset(25)
            make.bottom.equalTo(passwordTextField.snp.top).inset(-20)
        }

        passwordTextField.snp.makeConstraints { make in
            make.top.equalTo(phoneTextFieldView.snp.bottom).inset(10)
            make.top.equalTo(emailTextField.snp.bottom).inset(10)
            make.leading.trailing.equalToSuperview().inset(25)
            make.bottom.equalTo(forgotPasswordButton.snp.top).inset(UIScreen.isSE ? -8 : -13)
        }

        forgotPasswordButton.snp.makeConstraints { make in
            make.top.equalTo(passwordTextField.snp.bottom).inset(UIScreen.isSE ? 8 : 13)
            make.height.equalTo(16)
            make.leading.equalToSuperview().inset(25)
        }

        invalidPasswordLabel.snp.makeConstraints { make in
            make.top.equalTo(passwordTextField.snp.bottom).offset(15)
            make.trailing.equalToSuperview().inset(25)
        }

        registrationButtonView.snp.makeConstraints { make in
            make.top.equalTo(forgotPasswordButton.snp.bottom).inset(UIScreen.isSE ? -14 : -16)
            make.leading.trailing.equalToSuperview().inset(24)
            make.bottom.equalTo(warningLabel.snp.top).inset(UIScreen.isSE ? -4 : -10)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(registrationButtonView.snp.bottom).inset(1)
            make.bottom.equalTo(containerView.snp.bottom).inset(10)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(16)
        }
    }
}

extension SignInTableCell: UITextFieldDelegate {

    func bindPasswordTextField() {
        passwordTextField.textField.delegate = self

        passwordTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe(onNext: { [weak self] in
                self?.buttonTapped.accept(())
            }).disposed(by: bag)
    }

    func bindMailTextField() {
        emailTextField.textField.delegate = self

        passwordTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe(onNext: { [weak self] in
                self?.buttonTapped.accept(())
            }).disposed(by: bag)
    }
}
