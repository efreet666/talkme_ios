//
//  CodeCountryChooseViewController.swift
//  talkme_ios
//
//  Created by Майя Калицева on 27.12.2020.
//
import RxCocoa
import RxSwift

final class CountryCodesViewController: UIViewController {

    // MARK: - Private properties

    private let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = TalkmeColors.deepBlue
        tv.tableFooterView = UIView()
        tv.registerCells(withModels: CountryCodeCellViewModel.self)
        return tv
    }()

    private let titleLabel: UILabel = {
        let ttl = UILabel()
        ttl.textColor = TalkmeColors.white
        ttl.text = "talkme_countrycode_choose_country".localized
        ttl.font = .montserratFontBlack(ofSize: 17)
        return ttl
    }()

    private let bag = DisposeBag()
    private let viewModel: CountryCodesViewModel

    // MARK: - Initializers

    init(viewModel: CountryCodesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.deepBlue
        bindVM()
        setupTableView()
        selectItem()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .dataItems
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupTableView() {
        view.addSubview(tableView)
        view.addSubview(titleLabel)

        tableView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom)
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.top.equalTo(view.safeAreaLayoutGuide)
        }
    }

    private func selectItem() {
        tableView.rx
            .modelSelected(AnyTableViewCellModelProtocol.self)
            .compactMap { model in
                if let item = model as? CountryCodeCellViewModel {
                    return CountryCodesFlow.setImageAndCode(country: item.country)
                }
                return nil
            }
            .bind(to: viewModel.flow)
            .disposed(by: bag)
    }
}
