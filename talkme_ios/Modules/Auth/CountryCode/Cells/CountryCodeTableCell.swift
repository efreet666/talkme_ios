//
//  CountryCodeTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 27.12.2020.
//

import UIKit

final class CountryCodeTableCell: UITableViewCell {

    // MARK: - Private Properties

    private let countryImageView = UIImageView()
    private let countryTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        return label
    }()
    private let countryCode: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.white
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = TalkmeColors.deepBlue
        setConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(country: CountryType) {
        countryImageView.image = country.model.image
        countryTitleLabel.text = country.model.title
        countryCode.text = country.model.code
        countryCode.textAlignment = .right
    }

    // MARK: - Private Methods

    private func setConstraints() {
        addSubviewsWithoutAutoresizing(countryImageView, countryTitleLabel, countryCode)
        NSLayoutConstraint.activate([
            countryImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            countryImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            countryImageView.widthAnchor.constraint(equalToConstant: 20),
            countryImageView.heightAnchor.constraint(equalToConstant: 20),

            countryTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            countryTitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 45),
            countryTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -186),
            countryTitleLabel.heightAnchor.constraint(equalTo: heightAnchor, constant: 25),

            countryCode.centerYAnchor.constraint(equalTo: centerYAnchor),
            countryCode.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 242),
            countryCode.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -9),
            countryCode.heightAnchor.constraint(equalTo: heightAnchor, constant: 25)
        ])
    }
}
