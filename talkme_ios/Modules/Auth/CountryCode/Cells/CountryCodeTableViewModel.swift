//
//  CountryCodeTableViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 28.12.2020.
//

final class CountryCodeCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let country: CountryType

    // MARK: - Initializers

    init(country: CountryType) {
        self.country = country
    }

    // MARK: - Public Methods

    func configure(_ cell: CountryCodeTableCell) {
        cell.configure(country: country)
    }
}
