//
//  CountryCodesViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 29.12.2020.
//
import RxCocoa
import RxSwift

enum CountryCodesFlow: Equatable {
    case setImageAndCode(country: CountryType)
}

final class CountryCodesViewModel {

    // MARK: - Private properties

    private lazy var items: [CountryCodeCellViewModel] = dataModel.data.map {
        CountryCodeCellViewModel(country: $0) }
    private let dataModel = CountryCodeModel()

    // MARK: - Public properties

    private(set) lazy var dataItems = Driver<[CountryCodeCellViewModel]>
        .just(self.raisingСountryByIndex(indexOf: 0, country: "talkme_countrycode_russia".localized))
    let flow = PublishRelay<CountryCodesFlow>()
    let bag = DisposeBag()

    private func raisingСountryByIndex(indexOf: Int, country: String) -> [CountryCodeCellViewModel] {
        var newItems = items.sorted(by: { $0.country.model.title < $1.country.model.title })
        let index = newItems.firstIndex(where: { $0.country.model.title == country})
        let countryRussia = newItems.remove(at: index ?? 0)
        newItems.insert(countryRussia, at: indexOf)
        return newItems
    }
}
