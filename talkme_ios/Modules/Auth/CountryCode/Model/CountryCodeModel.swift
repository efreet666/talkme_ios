//
//  CountryCodeModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 29.12.2020.
//

import UIKit

struct CountryCodeModel {

    let data = CountryType.allCases
}

enum CountryType: CaseIterable {
    case russia
    case belarus
    case usa
    case germany
    case cyprus
    case france
    case italy
    case spain
    case hungary
    case georgia
    case emirates
    case israel
    case azerbaijan
    case kazakhstan
    case uzbekistan
    case serbia
    case armenia
    case egypt
    case latvia
    case estonia
    case lithuania
    case greatBritain
    case sweden
    case austria

    var model: CountryCode {
        switch self {
        case .russia:
            return CountryCode(image: UIImage(named: "Russia"), title: "talkme_countrycode_russia".localized, code: "+7", regionCode: "RU")
        case .belarus:
            return CountryCode(image: UIImage(named: "Belarus"), title: "talkme_countrycode_belarus".localized, code: "+375", regionCode: "BY")
        case .usa:
            return CountryCode(image: UIImage(named: "USA"), title: "talkme_countrycode_usa".localized, code: "+1", regionCode: "US")
        case .germany:
            return CountryCode(image: UIImage(named: "Germany"), title: "talkme_countrycode_germany".localized, code: "+49", regionCode: "DE")
        case .cyprus:
            return CountryCode(image: UIImage(named: "Cyprus"), title: "talkme_countrycode_cyprus".localized, code: "+357", regionCode: "CY")
        case .france:
            return CountryCode(image: UIImage(named: "France"), title: "talkme_countrycode_france".localized, code: "+33", regionCode: "FR")
        case .italy:
            return CountryCode(image: UIImage(named: "Italy"), title: "talkme_countrycode_italy".localized, code: "+39", regionCode: "IT")
        case .spain:
            return CountryCode(image: UIImage(named: "Spain"), title: "talkme_countrycode_spain".localized, code: "+34", regionCode: "ES")
        case .hungary:
            return CountryCode(image: UIImage(named: "Hungary"), title: "talkme_countrycode_hungary".localized, code: "+36", regionCode: "HU")
        case .georgia:
            return CountryCode(image: UIImage(named: "Georgia"), title: "talkme_countrycode_georgia".localized, code: "+995", regionCode: "GE")
        case .emirates:
            return CountryCode(image: UIImage(named: "Emirates"), title: "talkme_countrycode_emirates".localized, code: "+971", regionCode: "AE")
        case .israel:
            return CountryCode(image: UIImage(named: "Israel"), title: "talkme_countrycode_israel".localized, code: "+972", regionCode: "IL")
        case .azerbaijan:
            return CountryCode(image: UIImage(named: "Azerbaijan"), title: "talkme_countrycode_azerbaijan".localized, code: "+994", regionCode: "AZ")
        case .kazakhstan:
            return CountryCode(image: UIImage(named: "Kazakhstan"), title: "talkme_countrycode_kazakhstan".localized, code: "+7", regionCode: "KZ")
        case .uzbekistan:
            return CountryCode(image: UIImage(named: "Uzbekistan"), title: "talkme_countrycode_uzbekistan".localized, code: "+998", regionCode: "UZ")
        case .serbia:
            return CountryCode(image: UIImage(named: "Serbia"), title: "talkme_countrycode_serbia".localized, code: "+381", regionCode: "RS")
        case .armenia:
            return CountryCode(image: UIImage(named: "Armenia"), title: "talkme_countrycode_armenia".localized, code: "+374", regionCode: "AM")
        case .egypt:
            return CountryCode(image: UIImage(named: "Egypt"), title: "talkme_countrycode_egypt".localized, code: "+20", regionCode: "EG")
        case .latvia:
            return CountryCode(image: UIImage(named: "Latvia"), title: "talkme_countrycode_latvia".localized, code: "+371", regionCode: "LV")
        case .estonia:
            return CountryCode(image: UIImage(named: "Estonia"), title: "talkme_countrycode_estonia".localized, code: "+372", regionCode: "EE")
        case .lithuania:
            return CountryCode(image: UIImage(named: "Lithuania"), title: "talkme_countrycode_lithuania".localized, code: "+370", regionCode: "LT")
        case .greatBritain:
            return CountryCode(image: UIImage(named: "GreatBritain"), title: "talkme_countrycode_great_britain".localized, code: "+44", regionCode: "GB")
        case .sweden:
            return CountryCode(image: UIImage(named: "Sweden"), title: "talkme_countrycode_sweden".localized, code: "+41", regionCode: "CH")
        case .austria:
            return CountryCode(image: UIImage(named: "Austria"), title: "talkme_countrycode_austria".localized, code: "+43", regionCode: "AT")
        }
    }
}
