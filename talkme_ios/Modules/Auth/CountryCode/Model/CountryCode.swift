//
//  CountryCode.swift
//  talkme_ios
//
//  Created by Майя Калицева on 27.12.2020.
//

import UIKit

struct CountryCode: Equatable {

    let image: UIImage?
    let title: String
    let code: String
    let regionCode: String
}
