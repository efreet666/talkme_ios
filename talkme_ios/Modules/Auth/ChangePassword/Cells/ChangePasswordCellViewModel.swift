//
//  ChangePasswordCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 20.03.2021.
//

import RxCocoa
import RxSwift

final class ChangePasswordCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { UIScreen.isSE ? 200 : 230 }

    let forgotPasswordButtonTap = PublishRelay<Void>()
    let passwordTextRelay = BehaviorRelay<String?>(value: nil)
    let repeatPasswordTextRelay = BehaviorRelay<String?>(value: nil)
    let isIncorrectPassword = PublishRelay<Bool>()
    let sendPasswordTap = PublishRelay<Void>()
    let error = PublishRelay<AuthError>()

    // MARK: - Public Methods

    func configure(_ cell: ChangePasswordTableCell) {
        cell
            .forgotPasswordButtonTap
            .bind(to: forgotPasswordButtonTap)
            .disposed(by: cell.bag)

        cell
            .sendPasswordTap
            .bind(to: sendPasswordTap)
            .disposed(by: cell.bag)

        cell
            .buttonTapped
            .bind(to: sendPasswordTap)
            .disposed(by: cell.bag)

        cell
            .passwordText
            .do(onNext: { [weak cell] in
                guard !$0.isEmpty else { return }
                cell?.setupError(AuthError.none)
            })
            .bind(to: passwordTextRelay)
            .disposed(by: cell.bag)

        cell
            .repeatPasswordText
            .bind(to: repeatPasswordTextRelay)
            .disposed(by: cell.bag)

        cell
            .repeatPasswordText
            .do(onNext: { [weak cell] in
                guard !$0.isEmpty else { return }
                cell?.setupError(AuthError.none)
            })
            .withLatestFrom(cell.passwordText) { ( $0, $1) }
            .filter { !$0.0.isEmpty && !$0.1.isEmpty }
            .map { $0.0 != $0.1 }
            .bind(to: isIncorrectPassword)
            .disposed(by: cell.bag)

        isIncorrectPassword
            .filter { $0 }
            .map { _ in AuthError.incorrectPassword }
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }
}
