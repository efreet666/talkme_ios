//
//  ChangePasswordTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 20.03.2021.
//

import RxSwift
import RxCocoa

final class ChangePasswordTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var sendPasswordTap = onShowButtonView.sendCodeTap
    private(set) lazy var forgotPasswordButtonTap = forgotPasswordButton.rx.tap

    private(set) lazy var passwordText = passwordTextField.textField.rx.text.orEmpty
    private(set) lazy var repeatPasswordText = repeatPasswordTextField.textField.rx.text.orEmpty
    let buttonTapped = PublishRelay<Void>()
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let passwordTextField = TextField()
    private let repeatPasswordTextField = TextField()
    private let onShowButtonView = RegistrationButton()
    private let buttonFont: UIFont = .montserratExtraBold(ofSize: UIScreen.isSE ? 12 : 15)

    private let containerView: UIView = {
       let view = UIView()
        view.layer.backgroundColor = TalkmeColors.white.cgColor
        view.layer.cornerRadius = 21
        return view
    }()

    private let forgotPasswordButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("talkme_auth_remembered_password".localized, for: .normal)
        btn.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        btn.titleLabel?.font =  .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 14)
        return btn
    }()

    private let warningLabel: UILabel = {
       let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .right
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.numberOfLines = 0
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupTextField()
        setupCellStyle()
        bindTextField()
        passwordTextField.textField.setNextResponder(repeatPasswordTextField.textField, disposeBag: bag)
        repeatPasswordTextField.textField.returnKeyType = .send
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupError(_ error: AuthError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupTextField() {
        passwordTextField.textField.placeholder = "talkme_auth_enter_data_new_password".localized
        repeatPasswordTextField.textField.placeholder = "talkme_auth_enter_data_repeat_password".localized

        let preventPasswordSuggestionContentType: UITextContentType
        if #available(iOS 12.0, *) {
            preventPasswordSuggestionContentType = .oneTimeCode
        } else {
            preventPasswordSuggestionContentType = .init(rawValue: "")
        }
        passwordTextField.textField.textContentType = preventPasswordSuggestionContentType
        repeatPasswordTextField.textField.textContentType = preventPasswordSuggestionContentType

        passwordTextField.textField.isSecureTextEntry = true
        repeatPasswordTextField.textField.isSecureTextEntry = true
        warningLabel.alpha = 0

        [passwordTextField, repeatPasswordTextField].forEach {
            $0.textField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)}

        onShowButtonView.configure(text: "profile_setting_send_code".localized, font: buttonFont)
    }

    private func setupLayout() {
        contentView.addSubviews(containerView)
        containerView.addSubviews([passwordTextField, repeatPasswordTextField, warningLabel, onShowButtonView, forgotPasswordButton])

        containerView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
        }

        passwordTextField.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(30)
            make.leading.trailing.equalToSuperview().inset(24)
        }

        repeatPasswordTextField.snp.makeConstraints { make in
            make.top.equalTo(passwordTextField.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(24)
        }

        forgotPasswordButton.snp.makeConstraints { make in
            make.top.equalTo(repeatPasswordTextField.snp.bottom).offset(13)
            make.leading.equalToSuperview().inset(24)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(repeatPasswordTextField.snp.bottom).offset(UIScreen.isSE ? 4 : 6)
            make.trailing.equalToSuperview().inset(24)
        }

        onShowButtonView.snp.makeConstraints { make in
            make.top.equalTo(forgotPasswordButton.snp.bottom).offset(UIScreen.isSE ? 15 : 16)
            make.leading.trailing.equalToSuperview().inset(23)
            make.bottom.equalToSuperview().inset(23)
        }
    }
}

extension ChangePasswordTableCell: UITextFieldDelegate {

    func bindTextField() {
        repeatPasswordTextField.textField.delegate = self

        repeatPasswordTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe(onNext: { [weak self] in
                self?.buttonTapped.accept(())
            }).disposed(by: bag)
    }
}
