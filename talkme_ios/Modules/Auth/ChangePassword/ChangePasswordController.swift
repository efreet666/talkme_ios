//
//  ChangePasswordController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 20.03.2021.
//

import RxCocoa
import RxSwift

final class ChangePasswordController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.layer.backgroundColor = UIColor.clear.cgColor
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        tbv.registerCells(
            withModels: TalkMeLogoCellViewModel.self,
            TitleTableCellViewModel.self,
            SocialsTableCellViewModel.self,
            LoginCellViewModel.self,
            EmptyTableCellViewModel.self,
            ChangePasswordCellViewModel.self)
        return tbv
    }()

    private let backgroundImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "mainBackground")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let viewModel: ChangePasswordViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: ChangePasswordViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        setupUI()
        bindVM()
        bindKeyboard()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .items
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupUI() {
        setupImageView()
        setupTableView()
        setupNavigation()
    }

    private func setupImageView() {
        view.addSubview(backgroundImage)

        backgroundImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupTableView() {
        view.addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                self.tableView.snp.updateConstraints { make in
                    make.bottom.equalTo(self.view.safeAreaLayoutGuide).inset(keyboardVisibleHeight)
                }
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    private func setupNavigation() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .clear
    }
}
