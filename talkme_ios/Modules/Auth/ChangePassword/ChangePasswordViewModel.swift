//
//  ChangePasswordViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 20.03.2021.
//

import RxCocoa
import RxSwift
import Moya

final class ChangePasswordViewModel {

    enum PasswordChangeFlow {
        case onLoggedIn
        case onSignIn
        case onRegistrationTap
        case networkErrorPopUp(errorMessage: String?)
    }

    // MARK: - Public Properties

    let flow = PublishRelay<PasswordChangeFlow>()
    let bag = DisposeBag()
    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>
        .just([
            talkMeLogoCellViewModel,
            titleLabelCellViewModel,
            passwordChangeCellViewModel,
            emptyCellViewModel,
            // uncommit this to enable list of socials in the bottom of the screen
            // socialCellViewModel,
            loginCellViewModel
        ])

    // MARK: - Private Properties

    private let userToken: String
    private let phoneNumber: String
    private let service: AuthServiceProtocol
    private let appleService: AppleIDSignInService
    private let googleService: GoogleSignInService
    private let talkMeLogoCellViewModel = TalkMeLogoCellViewModel()
    private let titleLabelCellViewModel = TitleTableCellViewModel(
        titleText: "talkme_auth_reset_password".localized)
    private let passwordChangeCellViewModel = ChangePasswordCellViewModel()
    private lazy var emptyCellViewModel = EmptyTableCellViewModel(cellHeight: emptySpaceHeight)
    private let socialCellViewModel = SocialsTableCellViewModel(cellType: .signIn)
    private let loginCellViewModel = LoginCellViewModel(
        title: "talkme_sign_in_not_registrated".localized,
        buttonTitle: "talkme_auth_registration".localized)
    private var maxCountOfNetworkRetries = 1
    private var currentCountOfNetworkRetries = 0

    private lazy var emptySpaceHeight: CGFloat = {
        let allHeights = talkMeLogoCellViewModel.height
            + titleLabelCellViewModel.height
            + passwordChangeCellViewModel.height
            + loginCellViewModel.height
            + socialCellViewModel.height
        let navBarHeight = UIApplication.topInset
        let topInset = UIApplication.topInset
        let bottomInset = UIApplication.bottomInset
        return UIScreen.main.bounds.height
            - topInset
            - bottomInset
            - navBarHeight
            - allHeights
    }()

    // MARK: - Initializers

    init(userToken: String,
         phoneNumber: String,
         service: AuthServiceProtocol,
         appleService: AppleIDSignInService,
         googleService: GoogleSignInService
    ) {
        self.service = service
        self.appleService = appleService
        self.googleService = googleService
        self.userToken = userToken
        self.phoneNumber = phoneNumber
        bindUI()
    }

    // MARK: - Private Methods

    private func bindUI() {
        passwordChangeCellViewModel
            .sendPasswordTap
            .withLatestFrom(
                Observable.combineLatest(
                    passwordChangeCellViewModel.passwordTextRelay.compactMap { $0 },
                    passwordChangeCellViewModel.repeatPasswordTextRelay.compactMap { $0 }))
            .bind { [weak self] password, repeatPassword in
                self?.passwordChange(pass: password, repeatPass: repeatPassword)
            }
            .disposed(by: bag)

        passwordChangeCellViewModel
            .forgotPasswordButtonTap
            .map { .onSignIn}
            .bind(to: flow)
            .disposed(by: bag)

        loginCellViewModel
            .tapLoginRelay
            .map { .onRegistrationTap }
            .bind(to: flow)
            .disposed(by: bag)

        socialCellViewModel
            .buttonTapRelay
            .bind { [weak self] type in
                self?.socialSignIn(with: type)
            }
            .disposed(by: bag)
    }

    private func passwordChange(pass: String, repeatPass: String) {
        let request = MobilePasswordConfirmRequest(user: userToken, newPassword: pass, newRepeatPassword: repeatPass)
        service
            .passwordChangeConfirm(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    if response.msg != nil && response.msg != "Пароль изменен" {
                        self?.passwordChangeCellViewModel.error.accept(.apiError(response.msg))
                    } else {
                        self?.login(password: pass)
                    }
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.passwordChange(pass: pass, repeatPass: repeatPass) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "11")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func login(password: String) {
        let request = LoginRequest(emailOrMobile: phoneNumber, password: password, rememberMe: true)
        service
            .login(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.onLoggedIn)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.login(password: password) },
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "12")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func socialSignIn(with type: SocialButtonType) {
        switch type {
        case .google:
            break
        case .appleID:
            if #available(iOS 13.0, *) { appleService.signIn() }
        case .facebook:
            FacebookSignInService.signIn()
        case .yandex:
            break
        case .mailru:
            break
        case .vkontakte:
            break
        default:
            break
        }
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)"))
            }
        }
    }
}
