//
//  AuthError.swift
//  talkme_ios
//
//  Created by Elina Efremova on 05.01.2021.
//

enum AuthError {
    case dataRequired
    case incorrectPassword
    case apiError(String?)
    case dataRequirement
    case dataRequirementField
    case shortPassword
    case none

    var title: String? {
        switch self {
        case .dataRequired:
            return "talkme_auth_error_data".localized
        case .incorrectPassword:
            return "talkme_auth_error_password".localized
        case .apiError(let text):
            return text
        case .dataRequirement:
            return "talkme_auth_error_enter_data".localized
        case .dataRequirementField:
            return "talkme_auth_error_enter_data_field".localized
        case .shortPassword:
            return "talkme_auth-error_short_password".localized
        case .none:
            return nil
        }
    }
}
