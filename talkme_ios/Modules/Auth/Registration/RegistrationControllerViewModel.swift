//
//  RegistrationControllerViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa
import RxSwift
import Moya

enum RegistrationFlow: Equatable {
    case onEnterSMSCode(phoneNumber: String)
    case onSignIn
    case onShowCountries
    case onSignUp(email: String)
    case networkErrorPopUp(errorMessage: String?)
}

final class RegistrationControllerViewModel {

    // MARK: - Public Properties

    let flow = PublishRelay<RegistrationFlow>()
    let bag = DisposeBag()
    let logoBottomOffset = PublishRelay<CGFloat>()
    let logoTopInset = PublishRelay<CGFloat>()

    // MARK: - Private Properties

    private var country = CountryType.russia.model
    private let service: AuthServiceProtocol
    private let talkMeLogoCellViewModel = TalkMeLogoCellViewModel()
    private let emptyUpdateCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? 10 : 0)
    private let titleLabelCellViewModel = TitleLabelCellViewModel(titleText: "talkme_auth_registration".localized)
    private let descriptionTextCellViewModel = DescriptionRegistrationTableCellViewModel(title: "talkme_registration_will_send_code".localized)
    private let registrationCellViewModel = RegistrationTableCellViewModel()
    private lazy var emptyCellViewModel = EmptyTableCellViewModel(cellHeight: UIScreen.isSE ? emptySpaceHeight - 6
                                                            : emptySpaceHeight - 48)
    private let loginCellViewModel = LoginCellViewModel(title: "talkme_registration_already_registrated".localized,
                                                        buttonTitle: "talkme_registration_login".localized)
    private let isEmailRegistration = BehaviorRelay<Bool>(value: false)
    private let emailOrPhoneNumberViewModel = UserRegistrationButtonsCellViewModel()
    private var maxCountOfNetworkRetries = 1
    private var currentCountOfNetworkRetries = 0

    private lazy var emptySpaceHeight: CGFloat = {
        let allHeights = talkMeLogoCellViewModel.height
            + emptyUpdateCellViewModel.height
            + titleLabelCellViewModel.height
            + registrationCellViewModel.height
            + loginCellViewModel.height
            + descriptionTextCellViewModel.height
            + emailOrPhoneNumberViewModel.height
        let navBarHeight = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.navigationBar.frame.height ?? 0
        let topInset = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
        let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return UIScreen.main.bounds.height
            - topInset
            - bottomInset
            - navBarHeight
            - allHeights
    }()

    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>
        .just([
            talkMeLogoCellViewModel,
            emptyUpdateCellViewModel,
            titleLabelCellViewModel,
            descriptionTextCellViewModel,
            registrationCellViewModel,
            emptyCellViewModel,
            loginCellViewModel
        ])

    // MARK: - Initializers

    init(service: AuthServiceProtocol) {
        self.service = service
        bindViewModel()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func bindViewModel() {
        registrationCellViewModel
            .sendCodeTapRelay
            .withLatestFrom(registrationCellViewModel.textRelay)
            .filter({!$0.isEmpty})
            .bind { [weak self] text in
                guard let self = self else { return }
                self.isEmailRegistration.value
                    ? self.checkEmail(text)
                    : self.getCodeFor(text)
            }
            .disposed(by: bag)

        loginCellViewModel
            .tapLoginRelay
            .bind { [weak self] _ in
                self?.flow.accept(.onSignIn)
            }
            .disposed(by: bag)

        registrationCellViewModel
            .countryButtonRelay
            .map {_ in .onShowCountries}
            .bind(to: flow)
            .disposed(by: bag)

        emailOrPhoneNumberViewModel
            .tapEmailRelay
            .map { true }
            .bind(to: isEmailRegistration)
            .disposed(by: bag)

        emailOrPhoneNumberViewModel
            .tapNubmerRelay
            .map { false }
            .bind(to: isEmailRegistration)
            .disposed(by: bag)

        isEmailRegistration
            .bind(to: registrationCellViewModel.isEmailRegistration)
            .disposed(by: bag)

        logoBottomOffset
            .bind(to: emptyUpdateCellViewModel.heightUpdate)
            .disposed(by: bag)

        logoTopInset
            .bind(to: talkMeLogoCellViewModel.topUpdate)
            .disposed(by: bag)
    }

    private func getCodeFor(_ text: String) {
        let request = SendMobileRequest(mobile: country.code + text)
        service
            .getMobileCode(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let phoneNumber = response.mobile else {
                        self?.registrationCellViewModel.error.accept(.apiError(response.msg))
                        return
                    }
                    self?.flow.accept(.onEnterSMSCode(phoneNumber: phoneNumber))
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.getCodeFor(text)},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "51")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func checkEmail(_ text: String) {
        let request = SignUpRequest(email: text)
        service
            .checkEmail(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    response.isEmail
                        ? self?.flow.accept(.onSignUp(email: response.text))
                        : self?.registrationCellViewModel.error.accept(.apiError(response.text))
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.checkEmail(text)},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "52")
                    print(error)
                default:
                    return
                }
            }
            .disposed(by: bag)
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)"))
            }
        }
    }

    // MARK: - Public Methods

    func changeCountry(counrty: CountryType) {
        self.country = counrty.model
        registrationCellViewModel.countryRelay.accept(counrty)
    }
}
