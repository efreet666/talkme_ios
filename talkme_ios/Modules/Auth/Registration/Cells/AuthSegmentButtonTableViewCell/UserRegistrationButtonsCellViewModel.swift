//
//  AuthButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa

final class UserRegistrationButtonsCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let tapNubmerRelay = PublishRelay<Void>()
    let tapEmailRelay = PublishRelay<Void>()
    let heightUserRegistretion = PublishRelay<CGFloat>()
    let backHeihgtUserRegistrationCell = PublishRelay<Void>()

    var height: CGFloat {
        return UserRegistrationButtonsTableViewCell.Constants.buttonHeight
            + UserRegistrationButtonsTableViewCell.Constants.buttonToBottom
    }

    // MARK: - Public Methods

    func configure(_ cell: UserRegistrationButtonsTableViewCell) {

        heightUserRegistretion
            .bind { [weak cell] height in
                cell?.configureHeight(height)
            }
            .disposed(by: cell.bag)

        backHeihgtUserRegistrationCell
            .bind { [weak cell] in
                cell?.setupStandartLayout()
            }
            .disposed(by: cell.bag)

        cell
            .numberButtonTap
            .bind(to: tapNubmerRelay)
            .disposed(by: cell.bag)

        cell
            .emailButtonTap
            .bind(to: tapEmailRelay)
            .disposed(by: cell.bag)

        cell.configure()
    }
}
