//
//  ButtonTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 16.12.2020.
//

import RxCocoa
import RxSwift

final class UserRegistrationButtonsTableViewCell: UITableViewCell {

    enum Constants {
        static let buttonHeight: CGFloat = UIScreen.isSE ? 39 : 49
        static let buttonToBottom: CGFloat = UIScreen.isSE ? 18 : 25
        static let leadingTrailingInsets: CGFloat = UIScreen.isSE ? 10 : 12
    }

    // MARK: - Private Properties

    private(set) lazy var numberButtonTap = numberButton.rx.tap
    private(set) lazy var emailButtonTap = emailButton.rx.tap
    private(set) var bag = DisposeBag()

    private let stackViewButton: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = UIScreen.isSE ? 7 : 9
        return sv
    }()

    private let numberButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("talkme_registration_phone".localized, for: .normal)
        btn.setImage(UIImage(named: "phone"), for: .normal)
        btn.backgroundColor = TalkmeColors.blueLabels
        btn.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = TalkmeColors.blueLabels.cgColor
        btn.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -6, bottom: 0, right: 6)
        btn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 6, bottom: 0, right: -6)
        btn.layer.cornerRadius = Constants.buttonHeight / 2
        return btn
    }()

    private let emailButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("talkme_registration_mail".localized, for: .normal)
        btn.setImage(UIImage(named: "mail"), for: .normal)
        btn.layer.borderColor = TalkmeColors.blueLabels.cgColor
        btn.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
        btn.layer.borderWidth = 1
        btn.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -6, bottom: 0, right: 6)
        btn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 6, bottom: 0, right: -6)
        btn.layer.cornerRadius = Constants.buttonHeight / 2
        return btn
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupButtonLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure() {
        bindUI()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func configureHeight(_ height: CGFloat) {
        contentView.snp.updateConstraints { make in
            make.height.equalTo(height)
        }
            configStackView()
    }

    func configStackView() {
        stackViewButton.snp.updateConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().inset(Constants.leadingTrailingInsets)
            make.trailing.equalToSuperview().inset(Constants.leadingTrailingInsets)
            make.bottom.equalToSuperview().inset(Constants.buttonToBottom - 5)
        }

        numberButton.layer.cornerRadius = numberButton.bounds.height / 2
        emailButton.layer.cornerRadius = emailButton.bounds.height / 2
    }

    func setupStandartLayout() {

        contentView.snp.updateConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 57 : 70)
            }

        stackViewButton.snp.updateConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().inset(Constants.leadingTrailingInsets)
            make.trailing.equalToSuperview().inset(Constants.leadingTrailingInsets)
            make.bottom.equalToSuperview().inset(Constants.buttonToBottom)
        }

        numberButton.layer.cornerRadius = Constants.buttonHeight / 2
        emailButton.layer.cornerRadius = Constants.buttonHeight / 2
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupButtonLayout() {
        stackViewButton.translatesAutoresizingMaskIntoConstraints = false
        stackViewButton.addArrangedSubview(numberButton)
        stackViewButton.addArrangedSubview(emailButton)
        contentView.addSubview(stackViewButton)

        contentView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 57 : 70)
            make.edges.equalToSuperview()
        }

        numberButton.snp.makeConstraints { make in
            make.height.equalTo(Constants.buttonHeight)
        }

        emailButton.snp.makeConstraints { make in
            make.height.equalTo(numberButton.snp.height)
        }

        stackViewButton.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().inset(Constants.leadingTrailingInsets)
            make.trailing.equalToSuperview().inset(Constants.leadingTrailingInsets)
            make.bottom.equalToSuperview().inset(Constants.buttonToBottom)
        }
    }

    private func bindUI() {
        numberButtonTap.bind { [weak self] in
            self?.numberButton.backgroundColor = TalkmeColors.blueLabels
            self?.emailButton.backgroundColor = .clear
        }
        .disposed(by: bag)

        emailButtonTap.bind { [weak self] in
            self?.numberButton.backgroundColor = .clear
            self?.emailButton.backgroundColor = TalkmeColors.blueLabels
        }
        .disposed(by: bag)
    }
}
