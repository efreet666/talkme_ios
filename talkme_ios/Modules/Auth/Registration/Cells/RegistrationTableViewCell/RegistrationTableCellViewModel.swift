//
//  RegistrationTableCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa

final class RegistrationTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { RegistrationTableViewCell.Constants.containerHeight }

    let textRelay = PublishRelay<String>()
    let countryRelay = PublishRelay<CountryType>()
    let sendCodeTapRelay = PublishRelay<Void>()
    let buttonColorRelay = PublishRelay<ConfirmButtonColor>()
    let countryButtonRelay = PublishRelay<Void>()
    let isEmailRegistration = PublishRelay<Bool>()
    let error = PublishRelay<AuthError>()

    // MARK: - Public Methods

    func configure(_ cell: RegistrationTableViewCell) {
        cell
            .textRelay
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .sendCodeTap
            .bind(to: sendCodeTapRelay)
            .disposed(by: cell.bag)

        cell
            .textRelay
            .filter { !$0.isEmpty }
            .map { _ in AuthError.none }
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        cell
            .textRelay
            .filter { $0.isEmpty }
            .map { _ in AuthError.dataRequired }
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        cell
            .textRelay
            .map { [weak cell] _ in
                cell?.isValidNumber ?? false
            }
            .bind(to: cell.buttonIsEnabled)
            .disposed(by: cell.bag)

        cell
            .textRelay
            .map { [weak cell] _ -> ConfirmButtonColor in
                cell?.isValidNumber ?? false ? .activeColor : .inactiveColor
            }
            .map { $0.backgroundColor }
            .bind(to: cell.buttonBackgroundColor)
            .disposed(by: cell.bag)

        cell
            .selectCountryTap
            .bind(to: countryButtonRelay)
            .disposed(by: cell.bag)

        cell
            .buttonTapped
            .bind(to: sendCodeTapRelay)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        countryRelay
            .bind { [weak cell] type in
                cell?.currentCountry = type
            }
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.code)
            .bind(to: cell.countryCodeText)
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.image)
            .bind(to: cell.countryCodeImage)
            .disposed(by: cell.bag)

        isEmailRegistration
            .bind(to: cell.isEmailRegistration)
            .disposed(by: cell.bag)

        cell.configure()
    }
}
