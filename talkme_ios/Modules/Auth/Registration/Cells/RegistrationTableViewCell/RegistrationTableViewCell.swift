//
//  RegistrationTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 16.12.2020.
//

import RxCocoa
import RxSwift

enum ConfirmButtonColor {
    case activeColor
    case inactiveColor

    var backgroundColor: UIColor {
        switch self {
        case .activeColor:
            return TalkmeColors.blueLabels
        case .inactiveColor:
            return TalkmeColors.noActivatedButton
        }
    }
}

final class RegistrationTableViewCell: UITableViewCell {

    enum Constants {
        static let containerHeight: CGFloat = UIScreen.isSE ? 153 : 181
        static let leadingInset: CGFloat = UIScreen.isSE ? 10 : 12
    }

    // MARK: - Public Properties

    private(set) lazy var sendCodeTap = registrationButtonView.sendCodeTap
    private(set) lazy var buttonIsEnabled = registrationButtonView.buttonIsEnabled
    private(set) lazy var buttonBackgroundColor = registrationButtonView.buttonBackgroundColor

    private(set) lazy var selectCountryTap = phoneTextFieldView.selectCountryTap
    private(set) lazy var countryCodeText = phoneTextFieldView.countryCodeText
    private(set) lazy var countryCodeImage = phoneTextFieldView.countryCodeImage
    let textRelay = PublishRelay<String>()
    let buttonTapped = PublishRelay<Void>()
    var isValidNumber: Bool {
        phoneTextFieldView.isValidNumber != (emailTextField.textField.text?.count ?? 0 > 0)
    }
    let isEmailRegistration = PublishRelay<Bool>()
    var currentCountry: CountryType = .russia {
        didSet {
            phoneTextFieldView.region = currentCountry.model.regionCode
        }
    }

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let registrationButtonView: RegistrationButton = {
        let btn = RegistrationButton()
        return btn
    }()

    private let phoneTextFieldView = NumberTextField()
    private let emailTextField: TextField = {
        let textField = TextField()
        textField.textField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        textField.textField.placeholder = "talkme_auth_enter_data_email".localized
        textField.setSecure(false)
        textField.isHidden = true
        textField.textField.setEmailTextField()
        return textField
    }()

    private let containerView: UIView = {
       let view = UIView()
        view.layer.backgroundColor = TalkmeColors.white.cgColor
        view.layer.cornerRadius = 21
        return view
    }()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fillEqually
        sv.spacing = 20
        return sv
    }()

    private let warningLabel: UILabel = {
       let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .center
        lbl.font = .montserratFontMedium(ofSize: 10)
        lbl.alpha = 0
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        emailTextField.textField.returnKeyType = .send
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func setupError(_ error: AuthError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }
        phoneTextFieldView.setErrorColor(errorText != nil)
        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    func configure() {
        bindUI()
        bindTextField()
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.addSubviews(
            phoneTextFieldView,
            emailTextField,
            warningLabel,
            registrationButtonView
        )

        containerView.snp.makeConstraints { make in
            make.height.equalTo(Constants.containerHeight)
            make.top.bottom.equalToSuperview()
            make.leading.equalTo(Constants.leadingInset)
            make.trailing.equalTo(UIScreen.isSE ? -10 : -12)
        }

        phoneTextFieldView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 25 : 23)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 22 : 23)
        }

        warningLabel.snp.makeConstraints { make in
            make.height.equalTo(20)
            make.top.equalTo(phoneTextFieldView.snp.bottom).inset(UIScreen.isSE ? 4 : 0)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        registrationButtonView.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 42 : 52)
            make.leading.trailing.equalTo(phoneTextFieldView)
            make.top.equalTo(warningLabel.snp.bottom)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 41 : 42)
        }

        emailTextField.snp.makeConstraints { make in
            make.edges.equalTo(phoneTextFieldView)
        }
    }

    private func setupItemOnStackView() {
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(
                equalTo: containerView.leadingAnchor, constant: 25),
            stackView.topAnchor.constraint(
                equalTo: containerView.topAnchor, constant: 25),
            stackView.trailingAnchor.constraint(
                equalTo: containerView.trailingAnchor, constant: -25),
            stackView.bottomAnchor.constraint(
                equalTo: contentView.bottomAnchor, constant: -25)
        ])
    }

    private func bindUI() {
        phoneTextFieldView
            .phoneString
            .bind(to: textRelay)
            .disposed(by: bag)

        emailTextField
            .text
            .bind(to: textRelay)
            .disposed(by: bag)

        isEmailRegistration.bind { [weak self] isEmail in
            guard let self = self else { return }
            self.registrationButtonView.configure(
                text: isEmail
                    ? "talkme_registration_confirm".localized
                    : "talkme_registration_button".localized,
                font: .montserratExtraBold(ofSize: 15)
            )
            self.emailTextField.isHidden = !isEmail
            self.emailTextField.textField.text = ""
            self.phoneTextFieldView.isHidden = isEmail
            self.phoneTextFieldView.setText("")
            self.textRelay.accept("")
        }
        .disposed(by: bag)
    }
}

extension RegistrationTableViewCell: UITextFieldDelegate {

    func bindTextField() {
        emailTextField.textField.delegate = self

        emailTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe(onNext: { [weak self] in
                self?.buttonTapped.accept(())
            }).disposed(by: bag)
    }
}
