//
//  TitleLabelCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa

final class TitleLabelCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat {
        return TitleLabelTableViewCell.Constants.labelToBottom
    }

    // MARK: - Private Properties

    private let titleText: String

    // MARK: - Init
    init(titleText: String) {
        self.titleText = titleText
    }

    // MARK: - Public Methods

    func configure(_ cell: TitleLabelTableViewCell) {
        cell.configure(title: titleText)
    }
}
