//
//  TitleLabelTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.12.2020.
//

import UIKit

final class TitleLabelTableViewCell: UITableViewCell {

    enum Constants {
        static let labelToBottom: CGFloat = 20
        static let cellHeight: CGFloat = UIScreen.isSE ? 35 : 56
    }

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .montserratFontBlack(ofSize: UIScreen.isSE ? 17 : 23)
        lbl.textColor = TalkmeColors.white
        lbl.textAlignment = .center
        return lbl
    }()

    // MARK: - Initializers

     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
         super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
     }

     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }

    // MARK: - Public Methods

    func configure(title: String) {
        titleLabel.text = title
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(titleLabel)

        contentView.snp.makeConstraints { make in
            make.height.equalTo(Constants.cellHeight)
            make.edges.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.top.equalToSuperview()
        }
    }
}
