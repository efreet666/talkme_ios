//
//  LoginTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.12.2020.
//

import RxCocoa
import RxSwift

final class LoginTableViewCell: UITableViewCell {

    // MARK: - Private Properties

    private(set) lazy var loginTap = loginButton.rx.tap
    private(set) var bag = DisposeBag()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fillEqually
        return sv
    }()

    private let titleLabel: UILabel = {
       let lbl = UILabel()
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        lbl.textAlignment = .center
        return lbl
    }()

    private let loginButton = UIButton(type: .system)

    // MARK: - Public Methods

    func configure(title: String, buttonTitle: String) {
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: UIFont.montserratBold(ofSize: UIScreen.isSE ? 12 : 15),
            NSAttributedString.Key.foregroundColor: TalkmeColors.white,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedString = NSMutableAttributedString(
            string: buttonTitle, attributes: attributes)
        titleLabel.text = title
        loginButton.setAttributedTitle(attributedString, for: .normal)
        loginButton.setTitleColor(TalkmeColors.white, for: .normal)
    }

    // MARK: - Initializers

     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
         super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
     }

     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(loginButton)
        contentView.addSubview(stackView)

        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: loginButton.topAnchor, constant: UIScreen.isSE ? -8 : -25 ),
            stackView.heightAnchor.constraint(equalToConstant: UIScreen.isSE ? 36 : 52),
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: UIScreen.isSE ? 12 : 15),
            stackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: UIScreen.isSE ? 0 : -20)
        ])
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
