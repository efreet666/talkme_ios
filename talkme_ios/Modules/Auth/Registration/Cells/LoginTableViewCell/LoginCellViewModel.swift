//
//  LoginCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa
import RxSwift

final class LoginCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { 90 }
    let tapLoginRelay = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: Private Properties

    private let title: String
    private let buttonTitle: String

    // MARK: - Initializers

    init(title: String, buttonTitle: String) {
        self.title = title
        self.buttonTitle = buttonTitle
    }

    // MARK: - Public Methods

    func configure(_ cell: LoginTableViewCell) {
        cell.configure(title: title, buttonTitle: buttonTitle)
        cell
            .loginTap
            .bind(to: tapLoginRelay)
            .disposed(by: cell.bag)
    }
}
