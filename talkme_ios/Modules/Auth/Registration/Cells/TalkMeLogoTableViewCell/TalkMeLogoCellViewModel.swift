//
//  TalkMeLogoCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa
import RxSwift

final class TalkMeLogoCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let topHeight: CGFloat
    let topUpdate = PublishRelay<CGFloat>()
    let bag = DisposeBag()

    var height: CGFloat {
        return TalkMeLogoTableViewCell.Constants.logoToBottom
            + TalkMeLogoTableViewCell.Constants.logoToTop
            + TalkMeLogoTableViewCell.Constants.logoSize.height
    }

    // MARK: - Initializers

    init(topHeight: CGFloat = 5) {
        self.topHeight = topHeight
    }

    // MARK: - Public Methods

    func configure(_ cell: TalkMeLogoTableViewCell) {
        cell.configureTop(topHeight)

        topUpdate
            .bind { [weak cell] top in
                cell?.configureTop(top)
            }
            .disposed(by: cell.bag)
    }
}
