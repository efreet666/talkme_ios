//
//  EmptyTableCell.swift
//  talkme_ios
//
//  Created by Elina Efremova on 24.12.2020.
//

import RxSwift

final class EmptyTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let emptyView = UIView()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Public Methods

    func configureHeight(_ height: CGFloat) {
        emptyView.snp.updateConstraints { make in
            make.height.equalTo(height)
        }
    }

    // MARK: - Private Methods

    private func setupUI() {
        selectionStyle = .none
        backgroundColor = .clear

        contentView.addSubview(emptyView)

        emptyView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(25)
        }
    }
}
