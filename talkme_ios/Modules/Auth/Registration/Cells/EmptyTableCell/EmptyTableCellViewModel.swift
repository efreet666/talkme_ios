//
//  EmptyTableCellViewModel.swift
//  talkme_ios
//
//  Created by Elina Efremova on 24.12.2020.
//

import RxCocoa
import RxSwift

final class EmptyTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let cellHeight: CGFloat
    let heightUpdate = PublishRelay<CGFloat>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(cellHeight: CGFloat = 0) {
        self.cellHeight = cellHeight
    }

    func configure(_ cell: EmptyTableCell) {
        cell.configureHeight(cellHeight)

        heightUpdate
            .bind { [weak cell] height in
                cell?.configureHeight(height)
            }
            .disposed(by: cell.bag)
    }
}
