//
//  DescriptionRegistrationTableViewCell.swift
//  talkme_ios
//
//  Created by Dmitry Bobrov on 27.12.2021.
//

import RxCocoa

final class DescriptionRegistrationTableViewCell: UITableViewCell {

    enum Constants {
        static let cellHeight: CGFloat = 60
    }

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.white
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        return lbl
    }()

    func configure(title: String) {
        titleLabel.text = title
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupLayout() {
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(24)
            make.bottom.top.equalToSuperview().inset(20)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
