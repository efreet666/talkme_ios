//
//  DescriptionRegistrationTableViewCellViewModel.swift
//  talkme_ios
//
//  Created by Dmitry Bobrov on 27.12.2021.
//

import RxCocoa

final class DescriptionRegistrationTableCellViewModel: TableViewCellModelProtocol {

    var height: CGFloat {
        return DescriptionRegistrationTableViewCell.Constants.cellHeight
    }

    private let title: String

    init(title: String) {
        self.title = title
    }

    func configure(_ cell: DescriptionRegistrationTableViewCell) {
        cell.configure(title: title)
    }
}
