//
//  RegistrationController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 16.12.2020.
//

import RxCocoa
import RxSwift

final class RegistrationController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.layer.backgroundColor = UIColor.clear.cgColor
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        tbv.registerCells(
            withModels: TalkMeLogoCellViewModel.self,
            EmptyTableCellViewModel.self,
            TitleLabelCellViewModel.self,
            DescriptionRegistrationTableCellViewModel.self,
            UserRegistrationButtonsCellViewModel.self,
            RegistrationTableCellViewModel.self,
            LoginCellViewModel.self,
            EmptyTableCellViewModel.self,
            UserRegistrationButtonsCellViewModel.self
        )
        return tbv
    }()

    private let backgroundImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "mainBackground")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let viewModel: RegistrationControllerViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: RegistrationControllerViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        setupUI()
        setupImageView()
        setupTableViewBinding()
        setupTableView()
        bindKeyboard()
        bindUI()
        hideKeyboardWhenTappedAround()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        setupUI()
    }

    // MARK: - Public Methods

    @objc func dismissKeyboard() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }

    // MARK: - Private Methods

    private func setupTableViewBinding() {
        viewModel
            .items
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupImageView() {
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backgroundImage)

        NSLayoutConstraint.activate([
            backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImage.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }

    private func setupTableView() {
        view.addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalToSuperview()
        }
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let keyboardHidden = keyboardVisibleHeight == 0
                self.decreaseLogoBottomOffset(!keyboardHidden)
                self.tableView.contentInset.bottom = !keyboardHidden
                    ? self.tableView.rowHeight - keyboardVisibleHeight
                    : keyboardVisibleHeight
                self.tableView.snp.updateConstraints { make in
                    make.bottom.equalToSuperview().inset(keyboardVisibleHeight)
                }
                if !keyboardHidden {
                    self.tableView.scrollTo(edge: .bottom, animated: true)
                }
                UIView.animate(withDuration: 0) {
                    self.tableView.beginUpdates()
                    self.view.layoutIfNeeded()
                    self.tableView.endUpdates()
                }
            })
            .disposed(by: bag)
    }

    private func decreaseLogoBottomOffset(_ isSmallOffset: Bool) {
        let smallOffset: CGFloat = UIScreen.isSE ? 9 : 30
        let largeOffset: CGFloat = UIScreen.isSE ? 0 : 0
        self.viewModel.logoBottomOffset.accept(isSmallOffset ? smallOffset : largeOffset)
        let smallInset: CGFloat = UIScreen.isSE ? -7 : -5
        let largeInset: CGFloat = 5
        self.viewModel.logoTopInset.accept(isSmallOffset ? smallInset : largeInset)
    }

    private func bindUI() {
        view.rx
            .swipeGesture(.down)
            .bind { [weak self] _ in
                self?.view.endEditing(true)
            }
            .disposed(by: bag)
    }

    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    private func setupUI() {
        navigationController?.navigationBar.backItem?.title = "talkme_navigation_back".localized
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .clear
    }
}
