//
//  TextField.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.12.2020.
//

import RxSwift
import RxCocoa

final class TextField: UIView {

    // MARK: - Public Properties

    enum TextFieldMode {
        case showErrorMessage
        case showEyeButton
        case selectDate
        case none
    }

    let eyesButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "eyesdeactive"), for: .normal)
        btn.setImage(UIImage(named: "eyesactive"), for: .selected)
        return btn
    }()

    // MARK: - Private Properties

    private var showErrorMessage: Bool = false
    private var showEyeButton: Bool = true
    private let bag = DisposeBag()
    private(set) lazy var text = textField.rx.text.orEmpty

    private(set) var wrongPasswordsLabel: UILabel = {
       let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .right
        lbl.font = .montserratFontMedium(ofSize: 12)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.4
        lbl.numberOfLines = 1
        return lbl
    }()

    private(set) var textField: UITextField = {
      let tf = UITextField()
       tf.borderStyle = .none
       return tf
   }()

    let separator: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = TalkmeColors.separatorView.cgColor
        return view
    }()

    private let datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar

        components.year = -150
        let minDate = calendar.date(byAdding: components, to: currentDate)!

        datePicker.minimumDate = minDate
        datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        return datePicker
    }()

    private let doneButton = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)

    // MARK: - Initializers

    init(withMode mode: TextFieldMode) {
        super.init(frame: .zero)
        switch mode {
        case .showErrorMessage:
            showErrorMessage = true
        case .showEyeButton:
            showErrorMessage = false
            showEyeButton = true
            bindEyesButton()
        case .selectDate:
            showErrorMessage = false
            showEyeButton = false
            textField = LessonCommonTextField(leftInset: 0)
            textField.inputView = datePicker
            setupToolbar()
            bindDateSelectionDoneButton()
        case .none:
            showErrorMessage = false
            showEyeButton = false
        }
        setupBtn()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupBtn()
        bindEyesButton()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setSecure(_ isSecure: Bool) {
        textField.isSecureTextEntry = isSecure
        eyesButton.isHidden = !isSecure
        eyesButton.isSelected = !isSecure
    }

    func setErrorText(to title: String) {
        wrongPasswordsLabel.text = title
    }

    // MARK: - Private Methods

    private func bindDateSelectionDoneButton() {
        doneButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.textField.text = Formatters.lessonDateformatter.string(from: self.datePicker.date).replacingOccurrences(of: "..", with: ".")
                self.textField.resignFirstResponder()
            }
            .disposed(by: bag)
    }

    private func bindEyesButton() {
        eyesButton.rx.tap
        .bind { [weak self] in
            guard let self = self else { return }
            self.textField.isSecureTextEntry = self.eyesButton.isSelected
            self.eyesButton.isSelected.toggle()
        }
        .disposed(by: bag)
    }

    private func setupToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: true)
        textField.inputAccessoryView = toolBar
    }

    private func setupBtn() {
        textField.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(textField)
        addSubview(separator)

        if showErrorMessage {
            addSubviewsWithoutAutoresizing([wrongPasswordsLabel])

            textField.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
            wrongPasswordsLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)

            NSLayoutConstraint.activate([
                textField.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                textField.topAnchor.constraint(equalTo: self.topAnchor),
                textField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),

                wrongPasswordsLabel.leadingAnchor.constraint(equalTo: textField.trailingAnchor, constant: 5),
                wrongPasswordsLabel.topAnchor.constraint(equalTo: self.topAnchor),
                wrongPasswordsLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                wrongPasswordsLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
            ])
        } else if showEyeButton {
            addSubviewsWithoutAutoresizing([eyesButton])

            NSLayoutConstraint.activate([
                textField.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                textField.topAnchor.constraint(equalTo: self.topAnchor),
                textField.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                textField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),

                eyesButton.centerYAnchor.constraint(equalTo: textField.centerYAnchor),
                eyesButton.trailingAnchor.constraint(equalTo: trailingAnchor)
            ])
        } else {
            NSLayoutConstraint.activate([
                textField.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                textField.topAnchor.constraint(equalTo: self.topAnchor),
                textField.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                textField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
            ])
        }

        NSLayoutConstraint.activate([
            separator.heightAnchor.constraint(equalToConstant: 1),
            separator.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            separator.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 9),
            separator.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            separator.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
}
