//
//  CustomItem.swift
//  talkme_ios
//
//  Created by Yura Fomin on 16.12.2020.
//

import RxSwift
import RxCocoa
import PhoneNumberKit

final class NumberTextField: UIView {

    // MARK: - Public Properties
    private let bag = DisposeBag()

    private(set) lazy var selectCountryTap = countryButton.rx.tap
    private(set) lazy var countryCodeText = codeCountryLabel.rx.text
    private(set) lazy var countryCodeImage = flagIcon.rx.image

    var isValidNumber: Bool { numberTextField.isValidNumber }

    var region: String = "RU" {
        didSet {
            numberTextField.partialFormatter.defaultRegion = region
            numberTextField.text = ""
        }
    }

    lazy var phoneString = numberTextField
        .rx
        .text
        .map { [weak self] _ -> String in
            guard let self = self, let number = self.numberTextField.phoneNumber else { return "" }
            return String(number.nationalNumber)
        }

    var textfieldFont: UIFont = .montserratSemiBold(ofSize: 15) {
        didSet {
            numberTextField.font = textfieldFont
        }
    }

    // MARK: - Private Properties

    private let numberTextField: PhoneNumberTextField = {
        let phoneTextField = PhoneNumberTextField()
        phoneTextField.withPrefix = false
        phoneTextField.font = .montserratSemiBold(ofSize: 15)
        phoneTextField.borderStyle = .none
        phoneTextField.keyboardType = .numberPad
        phoneTextField.textContentType = .telephoneNumber
        phoneTextField.isPartialFormatterEnabled = true
        phoneTextField.partialFormatter.defaultRegion = "RU"
        phoneTextField.maxDigits = 10
        return phoneTextField
    }()

    private let separator: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = TalkmeColors.separatorView.cgColor
        return view
    }()

    private let dropDownIcon: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "dropDownIcon")
        img.contentMode = .scaleAspectFit
        return img
    }()

    private let flagIcon: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "flag")
        img.contentMode = .scaleAspectFit
        return img
    }()

    private let codeCountryLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "+7"
        lbl.font = .montserratSemiBold(ofSize: 15)
        lbl.textColor = TalkmeColors.codeCountry
        lbl.textAlignment = .center
        return lbl
   }()

    private let countryButton = UIButton()

    func setErrorColor(_ set: Bool) {
        numberTextField.textColor = set ? TalkmeColors.warningColor: .black
    }

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        numberTextField.delegate = self
        setupBtn()
        setupObserver()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(font: UIFont) {
        codeCountryLabel.font = font
    }

    func setText(_ text: String) {
        numberTextField.text = text
    }

    private func setupObserver() {
        numberTextField
            .rx
            .text
            .changed
            .bind { [weak self] _ in
                self?.setErrorColor(false)
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func setupBtn() {
        flagIcon.translatesAutoresizingMaskIntoConstraints = false
        dropDownIcon.translatesAutoresizingMaskIntoConstraints = false
        codeCountryLabel.translatesAutoresizingMaskIntoConstraints = false
        numberTextField.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        countryButton.translatesAutoresizingMaskIntoConstraints = false

        addSubview(flagIcon)
        addSubview(dropDownIcon)
        addSubview(codeCountryLabel)
        addSubview(numberTextField)
        addSubview(separator)
        addSubview(countryButton)

        NSLayoutConstraint.activate([
            flagIcon.widthAnchor.constraint(equalToConstant: UIScreen.isSE ? 22 : 28),
            flagIcon.heightAnchor.constraint(equalTo: flagIcon.widthAnchor),
            flagIcon.leadingAnchor.constraint(equalTo: leadingAnchor),
            flagIcon.topAnchor.constraint(equalTo: topAnchor),

            dropDownIcon.widthAnchor.constraint(equalToConstant: UIScreen.isSE ? 5 : 12),
            dropDownIcon.heightAnchor.constraint(equalToConstant: 10),
            dropDownIcon.leadingAnchor.constraint(
                equalTo: flagIcon.trailingAnchor, constant: 5),
            dropDownIcon.centerYAnchor.constraint(
                equalTo: flagIcon.centerYAnchor),

            codeCountryLabel.widthAnchor.constraint(equalToConstant: 39),
            codeCountryLabel.heightAnchor.constraint(equalToConstant: 19),
            codeCountryLabel.leadingAnchor.constraint(
                equalTo: dropDownIcon.trailingAnchor, constant: 3),
            codeCountryLabel.centerYAnchor.constraint(
                equalTo: dropDownIcon.centerYAnchor),

            numberTextField.widthAnchor.constraint(equalToConstant: 125),
            numberTextField.heightAnchor.constraint(equalToConstant: 19),
            numberTextField.leadingAnchor.constraint(
                equalTo: codeCountryLabel.trailingAnchor, constant: 4),
            numberTextField.centerYAnchor.constraint(
                equalTo: codeCountryLabel.centerYAnchor),
            numberTextField.trailingAnchor.constraint(
                equalTo: trailingAnchor, constant: -60),

            separator.heightAnchor.constraint(equalToConstant: 1),
            separator.leadingAnchor.constraint(equalTo: leadingAnchor),
            separator.topAnchor.constraint(equalTo: flagIcon.bottomAnchor, constant: 5),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),

            countryButton.topAnchor.constraint(equalTo: topAnchor),
            countryButton.leadingAnchor.constraint(equalTo: leadingAnchor),
            countryButton.widthAnchor.constraint(equalToConstant: 40),
            countryButton.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
}

extension NumberTextField: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        var newText = (text as NSString).replacingCharacters(in: range, with: string) as String
        newText = newText.trimmingCharacters(in: .whitespacesAndNewlines)
        let set = CharacterSet(charactersIn: "0123456789-+ ")
        let hasCharacterInRange = newText.rangeOfCharacter(from: set.inverted) != nil
        return !hasCharacterInRange
    }
}
