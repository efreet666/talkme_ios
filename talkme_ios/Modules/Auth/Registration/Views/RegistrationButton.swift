//
//  RegistrationButtonView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 17.12.2020.
//

import UIKit

final class RegistrationButton: UIView {

    // MARK: - Public Properties

    private(set) lazy var sendCodeTap = sendСodeButton.rx.tap
    private(set) lazy var buttonIsEnabled = sendСodeButton.rx.isEnabled
    private(set) lazy var buttonBackgroundColor = sendСodeButton.rx.backgroundColor

    // MARK: - Pirvate Properties

    private let sendСodeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        btn.setTitle("talkme_registration_button".localized, for: .normal)
        btn.titleLabel?.font = .montserratExtraBold(ofSize: 15)
        btn.backgroundColor = TalkmeColors.blueLabels
        return btn
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupBtn()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - life cycle
    override func layoutSubviews() {
        super.layoutSubviews()
        if layer.bounds != .zero && layer.shadowPath == nil {
            layer.cornerRadius = bounds.height / 2
            sendСodeButton.layer.cornerRadius = bounds.height / 2
            setupShadow()
        }
    }

    // MARK: - Public Methods

    func configure(text: String, font: UIFont) {
        sendСodeButton.setTitle(text, for: .normal)
        sendСodeButton.titleLabel?.font = font
    }

    // MARK: - Private Methods

    private func setupBtn() {
        sendСodeButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(sendСodeButton)

        NSLayoutConstraint.activate([
            sendСodeButton.heightAnchor.constraint(equalToConstant: UIScreen.isSE ? 40 : 52),
            sendСodeButton.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            sendСodeButton.topAnchor.constraint(equalTo: self.topAnchor),
            sendСodeButton.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            sendСodeButton.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }

    private func setupShadow() {
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 4)
        layer.shadowColor = TalkmeColors.shadow.cgColor // UIColor(red: 0, green: 0.596, blue: 1, alpha: 0.4).cgColor
        layer.shadowOpacity = 0.4
    }
}
