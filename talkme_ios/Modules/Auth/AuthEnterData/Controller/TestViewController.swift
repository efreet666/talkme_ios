//
//  TestViewController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 28.12.2020.
//

import UIKit

class TestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
    }
}
