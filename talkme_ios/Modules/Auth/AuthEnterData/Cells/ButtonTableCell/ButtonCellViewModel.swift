//
//  ButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa

final class ButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let buttonIsEnabled = BehaviorRelay<Bool>(value: true)
    let tapRelay = PublishRelay<Void>()

    // MARK: - Private Properties

    private let title: String

    // MARK: - Initializers

    init(title: String) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: ButtonTableViewCell) {
        cell.configure(title: title)
        cell
            .buttonTap
            .bind(to: tapRelay)
            .disposed(by: cell.bag)

        buttonIsEnabled
            .bind(to: cell.buttonIsEnabled)
            .disposed(by: cell.bag)
    }
}
