//
//  ButtonTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa
import RxSwift

final class ButtonTableViewCell: UITableViewCell {

    // MARK: - Private Properties

    private(set) lazy var buttonTap = button.rx.tap
    private(set) lazy var buttonIsEnabled = button.rx.isEnabled
    private(set) var bag = DisposeBag()

    private let button: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = TalkmeColors.white.cgColor
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        btn.titleLabel?.font = .montserratExtraBold(ofSize: 13)
        btn.layer.cornerRadius = 20
        return btn
    }()

    // MARK: - Public Methods

    func configure(title: String) {
        button.setTitle(title, for: .normal)
    }

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(button)

        button.snp.makeConstraints { (make) in
            make.height.equalTo(42)
            make.top.equalToSuperview().inset(30)
            make.bottom.equalToSuperview().inset(30)
            make.leading.trailing.equalToSuperview().inset(40)
        }
    }
}
