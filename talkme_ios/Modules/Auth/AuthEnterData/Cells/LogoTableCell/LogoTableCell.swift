//
//  LogoTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 22.12.2020.
//

import RxSwift

final class LogoTableCell: UITableViewCell {

    enum Constants {
        static let logoSize = UIScreen.isSE ? CGSize(width: 123, height: 71) : CGSize(width: 151, height: 87)
    }

    let bag = DisposeBag()

    // MARK: - Private Properties

    private let talkMeLogoImage: UIImageView = {
       let img = UIImageView()
        img.image = UIImage(named: "talkme")
        img.contentMode = .scaleAspectFit
        return img
    }()

   // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLogoLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configureTop(_ top: CGFloat) {
        talkMeLogoImage.snp.updateConstraints { make in
            make.top.equalToSuperview().inset(top)
        }
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLogoLayout() {
        contentView.addSubview(talkMeLogoImage)

        talkMeLogoImage.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(5)
            make.bottom.equalToSuperview().inset(20)
            make.size.equalTo(Constants.logoSize)
        }
    }
}
