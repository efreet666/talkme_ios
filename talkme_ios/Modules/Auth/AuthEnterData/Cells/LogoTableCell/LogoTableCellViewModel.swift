//
//  LogoTableCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 22.12.2020.
//

import RxSwift
import RxCocoa

final class LogoTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let topHeight: CGFloat
    let topUpdate = PublishRelay<CGFloat>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(topHeight: CGFloat = 5) {
        self.topHeight = topHeight
    }

    // MARK: - Public Methods

    func configure(_ cell: LogoTableCell) {
        cell.configureTop(topHeight)

        topUpdate
            .bind { [weak cell] top in
                cell?.configureTop(top)
            }
            .disposed(by: cell.bag)

    }
}
