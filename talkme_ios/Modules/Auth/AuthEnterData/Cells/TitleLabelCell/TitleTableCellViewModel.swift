//
//  TitleTableCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 22.12.2020.
//

import RxCocoa
import RxSwift

final class TitleTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var height: CGFloat { TitleTableCell.Constants.cellHeight }
    let heightUpdate = BehaviorRelay<CGFloat>(value: UIScreen.isSE ? 35 : 44)

    // MARK: - Private Properties

    private let titleText: String

    // MARK: - Initializers

    init(titleText: String) {
        self.titleText = titleText
    }

    // MARK: - Public Methods

    func configure(_ cell: TitleTableCell) {
        cell.configure(title: titleText)

        heightUpdate
            .bind { [weak cell] height in
                cell?.configureHeight(height)
            }
            .disposed(by: cell.bag)
    }
}
