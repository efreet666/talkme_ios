//
//  TitleTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 22.12.2020.
//

import UIKit
import RxSwift

final class TitleTableCell: UITableViewCell {

    enum Constants {
        static let cellHeight: CGFloat = UIScreen.isSE ? 35 : 44 // todo: return 35 : 56 after it is possible to enter and register via email
    }

    // MARK: - Private Properties

    private(set) var bag = DisposeBag()

    private let titleLabel: UILabel = {
       let lbl = UILabel()
        lbl.font = .montserratFontBlack(ofSize: UIScreen.isSE ? 17 : 23)
        lbl.textColor = TalkmeColors.white
        lbl.textAlignment = .center
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String) {
        titleLabel.text = title
    }

    func configureHeight(_ height: CGFloat) {
        contentView.snp.updateConstraints { make in
            make.height.equalTo(height)
        }
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(titleLabel)

        contentView.snp.makeConstraints { make in
            make.height.equalTo(Constants.cellHeight)
            make.edges.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
        }
    }
}
