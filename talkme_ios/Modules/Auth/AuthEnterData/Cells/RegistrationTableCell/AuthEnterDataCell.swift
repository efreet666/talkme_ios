//
//  AuthEnterDataCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa
import RxSwift
import UIKit

final class AuthEnterDataCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var firstNameText = nameTextField.textField.rx.text
    private(set) lazy var lastNameText = lastNameTextField.textField.rx.text
    private(set) lazy var passwordText = passwordTextField.textField.rx.text.orEmpty
    private(set) lazy var repeatPasswordText = repeatPasswordTextField.textField.rx.text.orEmpty
    let buttonTapped = PublishRelay<Void>()
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let nameTextField = TextField()
    private let lastNameTextField = TextField(withMode: .none)
    private let passwordTextField = TextField(withMode: .none)
    private let repeatPasswordTextField = TextField(withMode: .showErrorMessage)

    private let containerView: UIView = {
       let view = UIView()
        view.layer.backgroundColor = TalkmeColors.white.cgColor
        view.layer.cornerRadius = 21
        return view
    }()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fillEqually
        sv.spacing = 22
        return sv
    }()

    private let warningLabel: UILabel = {
       let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.textAlignment = .right
        lbl.font = .montserratFontMedium(ofSize: 12)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.4
        lbl.numberOfLines = 1
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupTextField()
        setupCellStyle()
        bindTextField()
        textFieldSettings()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setupError(_ error: AuthError) {
        let errorText = error.title
        let isHidden = errorText == nil

        UIView.transition(
            with: repeatPasswordTextField.wrongPasswordsLabel,
            duration: 0.0,
            options: .transitionCrossDissolve
        ) {
            switch error {
            case .incorrectPassword:
                self.repeatPasswordTextField.wrongPasswordsLabel.alpha = isHidden ? 0 : 1
                self.repeatPasswordTextField.wrongPasswordsLabel.text = errorText
            default:
                self.repeatPasswordTextField.wrongPasswordsLabel.alpha = 0
            }
        }

        UIView.transition(
            with: warningLabel,
            duration: 0.0,
            options: .transitionCrossDissolve
        ) {
            switch error {
            case .incorrectPassword, .none:
                self.warningLabel.alpha = 0
            default:
                self.warningLabel.alpha = isHidden ? 0 : 1
                self.warningLabel.text = errorText
            }
        }

        switch error {
        case .none:
            repeatPasswordTextField.separator.layer.borderColor = TalkmeColors.separatorView.cgColor
        default:
            repeatPasswordTextField.separator.layer.borderColor = TalkmeColors.warningColor.cgColor
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupTextField() {
        nameTextField.textField.placeholder = "talkme_auth_enter_data_name".localized
        lastNameTextField.textField.placeholder = "talkme_auth_enter_data_surname".localized
        passwordTextField.textField.placeholder = "talkme_auth_enter_data_password".localized
        repeatPasswordTextField.textField.placeholder = "talkme_auth_enter_data_confirm_password".localized

        let preventPasswordSuggestionContentType: UITextContentType
        if #available(iOS 12.0, *) {
            preventPasswordSuggestionContentType = .oneTimeCode
        } else {
            preventPasswordSuggestionContentType = .init(rawValue: "")
        }
        passwordTextField.textField.textContentType = preventPasswordSuggestionContentType
        repeatPasswordTextField.textField.textContentType = preventPasswordSuggestionContentType

        passwordTextField.textField.isSecureTextEntry = true
        repeatPasswordTextField.textField.isSecureTextEntry = true
        nameTextField.eyesButton.isHidden = true
        lastNameTextField.eyesButton.isHidden = true
        warningLabel.alpha = 0

        [nameTextField,
         lastNameTextField,
         passwordTextField,
         repeatPasswordTextField].forEach {
            $0.textField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 15)}
    }

    private func textFieldSettings() {
        nameTextField.textField.setNextResponder(lastNameTextField.textField, disposeBag: bag)
        lastNameTextField.textField.setNextResponder(passwordTextField.textField, disposeBag: bag)
        passwordTextField.textField.setNextResponder(repeatPasswordTextField.textField, disposeBag: bag)
        repeatPasswordTextField.textField.returnKeyType = .send
    }

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.addSubview(warningLabel)
        containerView.addSubview(stackView)

        [nameTextField,
         lastNameTextField,
         passwordTextField,
         repeatPasswordTextField
        ].forEach(stackView.addArrangedSubview)

        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(5)
            make.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(stackView.snp.bottom).offset(6)
            make.leading.trailing.equalToSuperview().inset(25)
            make.bottom.equalToSuperview().inset(17)
            make.height.greaterThanOrEqualTo(16)
        }

        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(30)
            make.leading.trailing.equalToSuperview().inset(25)
        }
    }
}

extension AuthEnterDataCell: UITextFieldDelegate {

    func bindTextField() {
        nameTextField.textField.delegate = self
        lastNameTextField.textField.delegate = self
        repeatPasswordTextField.textField.delegate = self
        passwordTextField.textField.delegate = self

        repeatPasswordTextField.textField.rx.controlEvent([.editingDidEndOnExit])
            .subscribe(onNext: { [weak self] in
                self?.buttonTapped.accept(())
            }).disposed(by: bag)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == nameTextField.textField || textField == lastNameTextField.textField {
            let currentCharacterCount = textField.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 30
        } else {
            guard let text = textField.text else { return false }
            var newText = (text as NSString).replacingCharacters(in: range, with: string) as String
            newText = newText.trimmingCharacters(in: .whitespacesAndNewlines)
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+~:{}|\"?><\\`,\"./;'[]=-")
            let hasCharacterInRange = newText.rangeOfCharacter(from: set.inverted) != nil
            return !hasCharacterInRange
        }
    }
}
