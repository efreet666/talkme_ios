//
//  AuthEnterDataCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa

final class AuthEnterDataCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let nameTextRelay = BehaviorRelay<String?>(value: nil)
    let lastNameTextelay = BehaviorRelay<String?>(value: nil)
    let passwordTextRelay = BehaviorRelay<String?>(value: nil)
    let repeatPasswordRelay = BehaviorRelay<String?>(value: nil)
    let isIncorrectPassword = PublishRelay<Bool>()
    let buttonIsActive = BehaviorRelay<Bool>(value: false)
    let onButtonTapped = PublishRelay<Void>()
    let error = PublishRelay<AuthError>()

    // MARK: - Private Properties

    private let minPasswordLength = 8

    // MARK: - Public Methods

    func configure(_ cell: AuthEnterDataCell) {
        cell
            .firstNameText
            .compactMap { $0 }
            .do(onNext: { [weak cell] in
                guard !$0.isEmpty else { return }
                cell?.setupError(AuthError.none)
            })
            .bind(to: nameTextRelay)
            .disposed(by: cell.bag)

        cell
            .lastNameText
            .compactMap { $0 }
            .do(onNext: { [weak cell] in
                guard !$0.isEmpty else { return }
                cell?.setupError(AuthError.none)
            })
            .bind(to: lastNameTextelay)
            .disposed(by: cell.bag)

        cell
            .passwordText
            .do(onNext: { [weak cell] in
                guard !$0.isEmpty else { return }
                cell?.setupError(AuthError.none)
            })
            .bind(to: passwordTextRelay)
            .disposed(by: cell.bag)

        cell
            .passwordText
            .do(onNext: { [weak cell] in
                if $0.count == 0 {
                    cell?.setupError(AuthError.none)
                } else if $0.count < self.minPasswordLength {
                    cell?.setupError(AuthError.shortPassword)
                }
            })
            .bind(to: passwordTextRelay)
            .disposed(by: cell.bag)

        cell
            .passwordText
            .map { $0.count >= self.minPasswordLength }
            .bind(to: buttonIsActive)
            .disposed(by: cell.bag)

        cell
            .repeatPasswordText
            .bind(to: repeatPasswordRelay)
            .disposed(by: cell.bag)

        cell
            .repeatPasswordText
            .do(onNext: { [weak cell] in
                guard !$0.isEmpty && $0.count >= self.minPasswordLength else {
                    cell?.setupError(AuthError.shortPassword)
                    return
                }
                cell?.setupError(AuthError.none)
            })
            .withLatestFrom(cell.passwordText) { ( $0, $1) }
            .filter { !$0.0.isEmpty && !$0.1.isEmpty }
            .map { $0.0 != $0.1 }
            .bind(to: isIncorrectPassword)
            .disposed(by: cell.bag)

        cell
            .buttonTapped
            .bind(to: onButtonTapped)
            .disposed(by: cell.bag)

        isIncorrectPassword
            .filter { $0 }
            .map { _ in AuthError.incorrectPassword }
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }
}
