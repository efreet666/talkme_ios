//
//  AuthEnterDataController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa
import RxSwift
import SnapKit

final class AuthEnterDataController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.separatorStyle = .none
        tbv.rowHeight = UITableView.automaticDimension
        tbv.tableFooterView = UIView()
        tbv.backgroundColor = .clear
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        tbv.keyboardDismissMode = .interactive
        tbv.registerCells(withModels: LogoTableCellViewModel.self,
                          TitleTableCellViewModel.self,
                          AuthEnterDataCellViewModel.self,
                          ButtonCellViewModel.self)
        return tbv
    }()

    private let backgroundImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "mainBackground")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let bag = DisposeBag()
    private let smallTopInset: CGFloat = UIScreen.isSE ? -90 : -160
    private let largeTopInset: CGFloat = 5

    // MARK: - Public Properties

    let viewModel: AuthEnterDataViewModel

    // MARK: - Initializers

    init(viewModel: AuthEnterDataViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupImageView()
        setupTableView()
        bindViewModel()
        bindKeyboard()
        setupUI()
        hideKeyboardWhenTappedAround()
    }

    // MARK: - Public Methods

    @objc func dismissKeyboard() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }

    // MARK: - Private Methods

    private func bindViewModel() {
        viewModel
            .items
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupTableView() {
        view.addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }

    private func setupImageView() {
        view.addSubview(backgroundImage)

        backgroundImage.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
    }

    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                let isHide = keyboardVisibleHeight == 0
                let heightScreenUnderSafeArea = self.view.frame.maxY - self.view.safeAreaLayoutGuide.layoutFrame.maxY
                self.viewModel.logoTopInset.accept(!isHide ? self.smallTopInset : self.largeTopInset)
                self.tableView.snp.updateConstraints { make in
                    make.bottom.equalTo(self.view.safeAreaLayoutGuide).inset(keyboardVisibleHeight - heightScreenUnderSafeArea)
                }
                if !isHide {
                    self.tableView.scrollTo(edge: .bottom, animated: false)
                }
                    self.tableView.beginUpdates()
                    self.view.layoutIfNeeded()
                    self.tableView.endUpdates()
            })
            .disposed(by: bag)
    }

    private func setupUI() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.tintColor = TalkmeColors.white
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        view.backgroundColor = .clear
    }
}
