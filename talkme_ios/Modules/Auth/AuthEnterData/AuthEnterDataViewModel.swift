//
//  AuthEnterDataViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import RxCocoa
import RxSwift
import Moya

enum AuthEnterDataFlow {
    case signIn
    case networkErrorPopUp(errorMessage: String?)
}

final class AuthEnterDataViewModel {

    // MARK: - Public Properties

    let phoneNumber: String?
    let email: String?
    let flow = PublishRelay<AuthEnterDataFlow>()
    let bag = DisposeBag()
    let logoTopInset = PublishRelay<CGFloat>()

    // MARK: - Private Properties

    private var service: AuthServiceProtocol
    private let talkMeLogoCellViewModel = LogoTableCellViewModel()
    private let buttonCellViewModel = ButtonCellViewModel(title: "talkme_auth_login".localized)
    private let titleTableCellViewModel: TitleTableCellViewModel = {
        let titleView = TitleTableCellViewModel(titleText: "talkme_auth_registration".localized)
        titleView.heightUpdate.accept(55)
        return titleView
    }()
    private let authEnterDataCellViewModel = AuthEnterDataCellViewModel()
    private var maxCountOfNetworkRetries = 1
    private var currentCountOfNetworkRetries = 0

    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>.just([
        talkMeLogoCellViewModel,
        titleTableCellViewModel,
        authEnterDataCellViewModel,
        buttonCellViewModel
    ])

    // MARK: - Initializers

    init(service: AuthServiceProtocol, phoneNumber: String? = nil, email: String? = nil) {
        self.service = service
        self.phoneNumber = phoneNumber
        self.email = email
        bindViewModel()
        bindTap()
    }

    // MARK: - Private Methods

    private func bindViewModel() {
        logoTopInset
            .bind(to: talkMeLogoCellViewModel.topUpdate)
            .disposed(by: bag)
    }

    private func bindTap() {

        authEnterDataCellViewModel
            .onButtonTapped
            .bind(to: buttonCellViewModel.tapRelay)
            .disposed(by: bag)

        authEnterDataCellViewModel
            .buttonIsActive
            .bind(to: buttonCellViewModel.buttonIsEnabled)
            .disposed(by: bag)

        buttonCellViewModel
          .tapRelay
          .withLatestFrom(
            Observable.combineLatest(
            authEnterDataCellViewModel.nameTextRelay.compactMap { $0 },
            authEnterDataCellViewModel.lastNameTextelay.compactMap { $0 },
            authEnterDataCellViewModel.passwordTextRelay.compactMap { $0 },
            authEnterDataCellViewModel.repeatPasswordRelay.compactMap { $0 }))
            .bind(onNext: { [weak self] firstName, lastName, password, repeatPassword in
                guard !firstName.isEmpty && !lastName.isEmpty && !password.isEmpty && !repeatPassword.isEmpty else {
                    self?.authEnterDataCellViewModel.error.accept(.apiError(AuthError.dataRequirementField.title))
                    return
                }
                guard password == repeatPassword else { return }
                self?.signUp(firstName: firstName, lastName: lastName, password: password)
            })
            .disposed(by: bag)
    }

    private func signUp(firstName: String, lastName: String, password: String) {
        let request = SignUpRequest(firstName: firstName, lastName: lastName, mobile: phoneNumber, email: email, password: password)
        service
            .signUp(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard response.authToken?.refresh != nil,
                          response.authToken?.access != nil else {
                        let error = response.password?.first ?? response.msg
                        self?.authEnterDataCellViewModel.error.accept(.apiError(error))
                        AmplitudeProvider.shared.signUpTracking()
                        return
                    }
                    self?.flow.accept(.signIn)
                case .error(let error):
                    self?.restartNetwork(function: { [weak self] in self?.signUp(firstName: firstName, lastName: lastName, password: password)},
                                         ifFailedShowAlertWithError: error, andOurErrorCode: "41")
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func restartNetwork(function: @escaping () -> Void, ifFailedShowAlertWithError error: Error, andOurErrorCode ourCode: String) {
        var statusCode = ""
        if let moyaError = error as? MoyaError,
        let recievedStatusCode = moyaError.response?.statusCode {
            statusCode = String(recievedStatusCode)
        }

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if self.maxCountOfNetworkRetries > self.currentCountOfNetworkRetries {
                self.currentCountOfNetworkRetries += 1
                function()
            } else {
                self.currentCountOfNetworkRetries = 0
                self.flow.accept(.networkErrorPopUp(errorMessage: "\(ourCode)-\(statusCode)"))
            }
        }
    }
}
