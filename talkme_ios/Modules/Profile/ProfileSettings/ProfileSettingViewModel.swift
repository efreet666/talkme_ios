//
//  ProfileSettingViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift
import Moya
import UIKit

enum ProfileSettingFlow {
    case onShowCountries
    case onProfile
    case updateMainInfo
    case onPopup(phoneNumber: String)
    case onUserGeoTap(UserGeoType)
    case onSocials(SocialMedia, Bool)
    case showNumberPopUp
}

final class ProfileSettingViewModel {

    // MARK: - Public Properties

    private(set) var dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    var errorMobileCheck = ""
    var backProfile = PublishRelay<Void>()
    var sendPassword = PublishRelay<Void>()
    let flow = PublishRelay<ProfileSettingFlow>()
    let onButtontTapped = PublishRelay<Void>()
    let bag = DisposeBag()
    let height = PublishRelay<Void>()
    let isIncorrectPassword = PublishRelay<Bool>()
    let onPickImage = PublishRelay<UIImage>()
    let onCountryCode = PublishRelay<CountryType>()
    let onSelectedCountry = PublishRelay<UserCountryModel>()
    let error = PublishRelay<IndexPath>()
    let onChangeSocial = PublishRelay<ContactsResponse>()
    let reloadSocialCell = PublishRelay<IndexPath>()

    // MARK: - Private properties

    private let profileEditData: ProfileEditData
    private let apiError = PublishRelay<String>()
    private let errorMobile = PublishRelay<String>()
    private var buttonState: SettingButtonType
    private var newAvatarImage: UIImage?
    private var avatarImage = UIImage(named: "noAvatar")
    private var countryCode = CountryType.russia.model.code
    private let service: AccountServiceProtocol
    lazy var socials = profileEditData.contactsResponse

    // MARK: - Initializers

    init(service: AccountServiceProtocol, profileResponse: ProfileResponse, contactsResponse: ContactsResponse, buttonState: SettingButtonType) {
        self.service = service
        self.buttonState = buttonState
        profileEditData = .init(profileResponse: profileResponse, contactsResponse: contactsResponse)
        fetchCountries()
        UserDefaultsHelper.shared.userTypeOfNumberClass = profileResponse.numberClass ?? ""
    }

    // MARK: - Public Methods

    func setupItems() {
        let birthDay = Formatters.formatter.string(
            from: Formatters.dateFormatterDefault.date(from: profileEditData.birthday ?? "") ?? Date())
        let socialMediaViewModel = SocialMediaViewModel(contacts: socials)
        let avatarCellViewModel = AvatarCellViewModel(imageString: profileEditData.avatarUrl)
        let userNameCellViewModel = UserInfoCellViewModel(
            title: "talkme_auth_enter_data_name".localized,
            placeholder: "talkme_auth_enter_data_name".localized,
            text: profileEditData.name, isOnDelegateTF: true)
        let userLastNameCellViewModel = UserInfoCellViewModel(
            title: "talkme_auth_enter_data_surname".localized,
            placeholder: "talkme_auth_enter_data_surname".localized,
            text: profileEditData.lastName, isOnDelegateTF: true)
        let gender = Gender(rawValue: profileEditData.gender ?? "")
        let genderCellViewModel = GenderCellViewModel(gender: gender)
        let countryCellViewModel = CountryCityCellViewModel(
            geoType: .country(profileEditData.country))
        let cityCellViewModel = CountryCityCellViewModel(
            geoType: .city(name: profileEditData.city))
        let dateInfoCellViewModel = DateInfoCellViewModel(
            title: "profile_setting_birthday".localized,
            text: birthDay)
        let userMailCellViewModel = UserInfoCellViewModel(
            title: "profile_setting_mail".localized,
            placeholder: "mail@mail.ru",
            text: profileEditData.email, isOnDelegateTF: false)
        let aboutMeCellViewModel = AboutmeCellViewModel(
            text: profileEditData.bio)
        let idCellViewModel = PasswordCellViewModel(
            title: "profile_setting_id".localized,
            isHidden: true,
            isSecureTextEntry: false,
            text: profileEditData.userId)
        let numberCellViewModel = NumberCellViewModel(mobile: socials.mobile)
        let passwordCellViewModel = PasswordCellViewModel(
            title: "talkme_auth_enter_data_password".localized,
            text: nil)
        let repeatPasswordCellViewModel = PasswordCellViewModel(
            title: "talkme_auth_enter_data_confirm_password".localized,
            text: nil)
        let showButtonCellViewModel = ShowButtonCellViewModel()

        let dataItems: [AnyTableViewCellModelProtocol] = [
            avatarCellViewModel,
            userNameCellViewModel,
            userLastNameCellViewModel,
            genderCellViewModel,
            countryCellViewModel,
            cityCellViewModel,
            dateInfoCellViewModel,
            userMailCellViewModel,
            aboutMeCellViewModel,
            idCellViewModel,
            numberCellViewModel,
            passwordCellViewModel,
            repeatPasswordCellViewModel,
            socialMediaViewModel,
            showButtonCellViewModel
        ]
        showButtonCellViewModel.setupButton(isTeacher: buttonState.isState)

        self.dataItems.accept(dataItems)

        onPickImage(avatarCellViewModel: avatarCellViewModel)
        onCountryCode(numberCellViewModel: numberCellViewModel)
        onSelectedCountry(cityCellViewModel: cityCellViewModel, countryCellViewModel: countryCellViewModel)
        apiError(userMailCellViewModel: userMailCellViewModel)
        isIncorrectPassword(repeatPasswordCellViewModel: repeatPasswordCellViewModel, passwordCellViewModel: passwordCellViewModel)
        onShowCountries(numberCellViewModel: numberCellViewModel)
        setupData(
            userNameCellViewModel: userNameCellViewModel,
            userLastNameCellViewModel: userLastNameCellViewModel,
            countryCellViewModel: countryCellViewModel,
            cityCellViewModel: cityCellViewModel,
            dateInfoCellViewModel: dateInfoCellViewModel,
            userMailCellViewModel: userMailCellViewModel,
            aboutMeCellViewModel: aboutMeCellViewModel,
            genderCellViewModel: genderCellViewModel,
            idCellViewModel: idCellViewModel,
            avatarCellViewModel: avatarCellViewModel,
            socialsCellViewModel: socialMediaViewModel)

        aboutMeCellViewModel.heightChanged
            .bind(to: height)
            .disposed(by: aboutMeCellViewModel.bag)

        avatarCellViewModel.photoButtonTap
            .bind(to: onButtontTapped)
            .disposed(by: avatarCellViewModel.bag)

        socialMediaViewModel.onSocialTapped
            .map { .onSocials($0.0, $0.1) }
            .bind(to: flow)
            .disposed(by: socialMediaViewModel.bag)

        showButtonCellViewModel
            .tapRelay
            .bind(onNext: { [weak self] _ in
                guard let self = self else { return }
                let requestModel = self.profileEditRequest()
                guard !self.showErrors(
                        for: requestModel,
                        avatarCellViewModel: avatarCellViewModel,
                        userNameCellViewModel: userNameCellViewModel,
                        userLastNameCellViewModel: userLastNameCellViewModel,
                        countryCellViewModel: countryCellViewModel,
                        cityCellViewModel: cityCellViewModel,
                        dateInfoCellViewModel: dateInfoCellViewModel,
                        userMailCellViewModel: userMailCellViewModel,
                        numberCellViewModel: numberCellViewModel) else { return }
                guard let geoId = requestModel.geoId else { return }
                UserDefaultsHelper.shared.geoId = geoId

                if let number = numberCellViewModel.textRelay.value, !number.isEmpty {
                    let mobile = self.countryCode + number
                    if mobile != self.socials.mobile {
                        self.getMobileCode(number)
                        self.sendProfileEdit(request: requestModel)
                    } else {
                        self.sendProfileEdit(request: requestModel)
                        self.backProfile.accept(())
                    }
                } else {
                    self.sendProfileEdit(request: requestModel)
                    self.backProfile.accept(())
                }

            })
            .disposed(by: showButtonCellViewModel.bag)

        backProfile
            .bind {
                self.flow.accept(.onProfile)
            }.disposed(by: bag)

        sendPassword
            .bind {
                guard let password = passwordCellViewModel.passwordTextRelay.value,
                      let repeatpassword = repeatPasswordCellViewModel.passwordTextRelay.value,
                      !password.isEmpty,
                      !repeatpassword.isEmpty else { return }
                self.passwordChange(password: password, repeatPassword: repeatpassword)
            }.disposed(by: bag)

        onChangeSocial
            .bind { [weak self] social in
                socialMediaViewModel.updateInfo(social)
                self?.reloadSocialCell.accept(IndexPath(item: 13, section: 0))
            }
            .disposed(by: bag)
    }

    func onCountryTap(geoType: UserGeoType) {
        flow.accept(.onUserGeoTap(geoType))
    }

    // MARK: - Private Methods

    func onAddSocial(_ social: SocialMedia, url: String?) {
        let needReloadTable: Bool
        switch social {
        case .vk:
            needReloadTable = socials.vk != url
            socials.vk = url
        case .telegram:
            needReloadTable = socials.telegram != url
            socials.telegram = url
        case .instagram:
            needReloadTable = socials.instagram != url
            socials.instagram = url
        case .facebook:
            needReloadTable = socials.facebook != url
            socials.facebook = url
        }
        guard needReloadTable else { return }
        onChangeSocial.accept(socials)
    }

    func onRemoveSocial(_ social: SocialMedia) {
        switch social {
        case .vk:
            socials.vk = nil
        case .telegram:
            socials.telegram = nil
        case .instagram:
            socials.instagram = nil
        case .facebook:
            socials.facebook = nil
        }
        onChangeSocial.accept(socials)
    }

    private func onPickImage(avatarCellViewModel: AvatarCellViewModel) {
        onPickImage
            .do(onNext: { [weak self] image in
                self?.newAvatarImage = image
            })
            .bind(to: avatarCellViewModel.imageRelay)
            .disposed(by: bag)
    }

    private func onCountryCode(numberCellViewModel: NumberCellViewModel) {
        onCountryCode
            .bind { [weak self, weak numberCellViewModel] country in
                numberCellViewModel?.countryRelay.accept(country)
                self?.countryCode = country.model.code
            }
            .disposed(by: bag)
    }

    private func onSelectedCountry(cityCellViewModel: CountryCityCellViewModel, countryCellViewModel: CountryCityCellViewModel) {
        onSelectedCountry
            .bind { [weak cityCellViewModel, weak countryCellViewModel] country in
                switch country.cityCountry {
                case .city(let text):
                    cityCellViewModel?.geoType.accept(.city(name: text))
                case .country(let text):
                    countryCellViewModel?.geoType.accept(.country(text))
                    cityCellViewModel?.update(geoType: .city(name: nil))
                    guard let iso = country.iso else { return }
                    UserDefaultsHelper.shared.countryCode = iso
                }
                guard let geo = country.geoId else { return }
                UserDefaultsHelper.shared.geoId = geo
            }
            .disposed(by: bag)
    }

    private func apiError(userMailCellViewModel: UserInfoCellViewModel) {
        apiError
            .bind { [weak userMailCellViewModel] error in
                userMailCellViewModel?.setupError(apiError: error)
            }
            .disposed(by: bag)
    }

    private func isIncorrectPassword(repeatPasswordCellViewModel: PasswordCellViewModel, passwordCellViewModel: PasswordCellViewModel) {
        repeatPasswordCellViewModel.passwordTextRelay
            .withLatestFrom(passwordCellViewModel.passwordTextRelay) {($0, $1)}
            .filter { !($0.0?.isEmpty ?? true) && !($0.1?.isEmpty ?? true) }
            .map { $0.0 != $0.1 }
            .bind(to: isIncorrectPassword)
            .disposed(by: repeatPasswordCellViewModel.bag)

        isIncorrectPassword
            .filter {$0}
            .map { _ in ProfileError.incorrectPassword }
            .bind(to: repeatPasswordCellViewModel.error)
            .disposed(by: bag)
    }

    private func onShowCountries(numberCellViewModel: NumberCellViewModel) {
        numberCellViewModel
            .countryButtonRelay
            .map { _ in .onShowCountries }
            .bind(to: flow)
            .disposed(by: bag)
    }

    private func showErrors(
        for model: ProfileEditRequest,
        avatarCellViewModel: AvatarCellViewModel,
        userNameCellViewModel: UserInfoCellViewModel,
        userLastNameCellViewModel: UserInfoCellViewModel,
        countryCellViewModel: CountryCityCellViewModel,
        cityCellViewModel: CountryCityCellViewModel,
        dateInfoCellViewModel: DateInfoCellViewModel,
        userMailCellViewModel: UserInfoCellViewModel,
        numberCellViewModel: NumberCellViewModel
    ) -> Bool {
        var hasError = false

        if model.firstName?.isEmpty ?? true {
            hasError = true
            userNameCellViewModel.error.accept(.name)
            let indexPath = IndexPath(row: 1, section: 0)
            error.accept(indexPath)
        }
        if model.lastName?.isEmpty ?? true {
            hasError = true
            userLastNameCellViewModel.error.accept(.lastName)
            let indexPath = IndexPath(row: 2, section: 0)
            error.accept(indexPath)
        }

        if !UserDefaultsHelper.shared.userTypeOfNumberClass.isEmpty {
            if newAvatarImage == nil && avatarImage == UIImage(named: "noAvatar") {
                hasError = true
                avatarCellViewModel.error.accept(.avatar)
                let indexPath = IndexPath(row: 0, section: 0)
                error.accept(indexPath)
            }

            if model.city?.isEmpty ?? true {
                hasError = true
                cityCellViewModel.error.accept(.city)
                let indexPath = IndexPath(row: 3, section: 0)
                error.accept(indexPath)
            }
            if model.country?.isEmpty ?? true {
                hasError = true
                countryCellViewModel.error.accept(.country)
                let indexPath = IndexPath(row: 4, section: 0)
                error.accept(indexPath)
            }
            if model.geoId?.isEmpty ?? true {
                hasError = true
                cityCellViewModel.error.accept(.city)
                let indexPath = IndexPath(row: 5, section: 0)
                error.accept(indexPath)
            }
            if model.birthday?.isEmpty ?? true {
                hasError = true
                dateInfoCellViewModel.error.accept(.birthday)
                let indexPath = IndexPath(row: 6, section: 0)
                error.accept(indexPath)
            }
            if model.email?.isEmpty ?? true {
                hasError = true
                userMailCellViewModel.error.accept(.email)
                let indexPath = IndexPath(row: 7, section: 0)
                error.accept(indexPath)
            }
        }

        self.errorMobile.bind { [weak self] text in
            numberCellViewModel.error.accept(.number(text))
            hasError = true
            self?.errorMobileCheck = text
            let indexPath = IndexPath(row: 8, section: 0)
            self?.error.accept(indexPath)
        } .disposed(by: bag)

        return hasError
    }

    private func sendProfileEdit(request: ProfileEditRequest) {
        service
            .sendProfileEdit(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    print(response)
                    self?.handleProfileEditResponse(response)
                    self?.sendPassword.accept(())
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func handleProfileEditResponse(_ response: ProfileEditResponse) {
        var hasError = false

        if !errorMobileCheck.isEmpty {
            self.errorMobileCheck = ""
            hasError = true
        }

        if let emailError = response.emailErrors?.first {
            hasError = true
            apiError.accept(emailError)
        }

        if !hasError {
            flow.accept(.showNumberPopUp)
            flow.accept(.updateMainInfo)
        }
    }

    private func passwordChange(password: String, repeatPassword: String) {
        let request = PasswordChangeRequest(newPassword: password, repeatNewPassword: repeatPassword)
        service
            .sendPasswordChange(request: request)
            .subscribe { event in
                switch event {
                case .success(let response):
                    print(response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func getMobileCode(_ mobile: String) {
        let request = SendMobileRequest(mobile: self.countryCode + mobile)
        service
            .getMobileCode(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                        guard let number = response.mobile else { return }
                        self?.flow.accept(.onPopup(phoneNumber: number))
                case .error(let error):
                    do {
                        let errorResponse = error as? Moya.MoyaError
                        guard let body = try errorResponse?.response?.mapString() else { return print("Error") }
                        let errorMobile = body.replacingOccurrences(of: "[msg:{}\"]", with: "", options: [.regularExpression, .caseInsensitive])
                        self?.errorMobile.accept(errorMobile)
                    } catch {
                        print(error)
                    }
                }
            }
            .disposed(by: bag)
    }

    func removeSocialsRequest(type: SocialMedia) {
        service
            .removeSocial(request: type)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.onRemoveSocial(type)
                case .error:
                    return
                }
            }
            .disposed(by: bag)
    }

    private func setupData(
        userNameCellViewModel: UserInfoCellViewModel,
        userLastNameCellViewModel: UserInfoCellViewModel,
        countryCellViewModel: CountryCityCellViewModel,
        cityCellViewModel: CountryCityCellViewModel,
        dateInfoCellViewModel: DateInfoCellViewModel,
        userMailCellViewModel: UserInfoCellViewModel,
        aboutMeCellViewModel: AboutmeCellViewModel,
        genderCellViewModel: GenderCellViewModel,
        idCellViewModel: PasswordCellViewModel,
        avatarCellViewModel: AvatarCellViewModel,
        socialsCellViewModel: SocialMediaViewModel
    ) {

        avatarCellViewModel
            .onLoadImage
            .bind { [weak self] image in
                self?.avatarImage = image
            }
            .disposed(by: avatarCellViewModel.bag)

        userNameCellViewModel
            .textRelay
            .bind { [weak self] text in
                guard text != nil else { return }
                self?.profileEditData.name = text
            }
            .disposed(by: userNameCellViewModel.bag)

        userLastNameCellViewModel
            .textRelay
            .bind { [weak self] text in
                guard text != nil else { return }
                self?.profileEditData.lastName = text
            }
            .disposed(by: userLastNameCellViewModel.bag)

        countryCellViewModel
            .geoType
            .bind { [weak self] text in
                self?.profileEditData.country = text.textValue
                self?.profileEditData.city = ""
            }
            .disposed(by: countryCellViewModel.bag)

        cityCellViewModel
            .geoType
            .bind { [weak self] text in
                guard text.textValue != nil else { return }
                self?.profileEditData.city = text.textValue
            }
            .disposed(by: cityCellViewModel.bag)

        dateInfoCellViewModel
            .dateRelay
            .bind { [weak self] date in
                self?.profileEditData.birthday = date
            }
            .disposed(by: dateInfoCellViewModel.bag)

        userMailCellViewModel
            .textRelay
            .bind { [weak self] text in
                guard text != nil else { return }
                self?.profileEditData.email = text
            }
            .disposed(by: userMailCellViewModel.bag)

        aboutMeCellViewModel
            .textRelay
            .bind { [weak self] text in
                self?.profileEditData.bio = text
            }
            .disposed(by: aboutMeCellViewModel.bag)

        genderCellViewModel
            .genderRelay
            .bind { [weak self] gender in
                self?.profileEditData.gender = gender?.rawValue
            }
            .disposed(by: genderCellViewModel.bag)

        idCellViewModel
            .passwordTextRelay
            .bind { [weak self] text in
                self?.profileEditData.userId = text
            }
            .disposed(by: idCellViewModel.bag)
    }

    func profileEditRequest() -> ProfileEditRequest {
        let name = profileEditData.name
        let lastName = profileEditData.lastName
        let country = profileEditData.country
        let city = profileEditData.city
        let date = profileEditData.birthday
        let mail = profileEditData.email
        let bio = profileEditData.bio
        let gender = profileEditData.gender
        let id = profileEditData.userId
        let image = newAvatarImage?.jpegData(compressionQuality: 0.3)
        let geoId = UserDefaultsHelper.shared.geoId
        let socials = profileEditData.contactsResponse
        self.socials = socials
        return ProfileEditRequest(
            lastName: lastName,
            firstName: name,
            bio: bio,
            birthday: date,
            email: mail,
            gender: gender,
            username: id,
            avatarUrl: image,
            country: country,
            city: city,
            geoId: geoId,
            socials: socials)
    }

    private func fetchCountries() {
        service
            .fetchCountries()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let countries = response.countries else { return }
                    let filteredCountry = countries.first {
                        $0.country == self?.profileEditData.country
                    }
                    guard let isoCode = filteredCountry?.countryCode else { return }
                    self?.fetchCities(isoCode: isoCode)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func fetchCities(isoCode: String) {
        service
            .fetchCities(isoCode: isoCode)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let cities = response.cities else { return }
                    let filteredCity = cities.first {
                        $0.city == self?.profileEditData.city
                    }
                    guard let geoId = filteredCity?.geoId else { return }
                    UserDefaultsHelper.shared.geoId = String(geoId)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
}
