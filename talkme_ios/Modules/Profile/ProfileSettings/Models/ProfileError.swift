//
//  ProfileError.swift
//  talkme_ios
//
//  Created by Yura Fomin on 14.01.2021.
//

enum ProfileError {
    case name
    case lastName
    case gender
    case country
    case city
    case birthday
    case email
    case id
    case apiError(String?)
    case incorrectPassword
    case none
    case avatar
    case number(String?)

    var title: String? {
        switch self {
        case .avatar:
            return "profile_setting_error_ava".localized
        case .name:
            return "profile_setting_error_name".localized
        case .lastName:
            return "profile_setting_error_last_name".localized
        case .gender:
            return "profile_setting_error_gender".localized
        case .country:
            return "profile_setting_error_country".localized
        case .birthday:
             return "profile_setting_error_birthday".localized
        case .email:
            return "profile_setting_error_email".localized
        case .id:
            return "profile_setting_error_id".localized
        case .city:
            return "profile_setting_error_city".localized
        case .apiError(let text):
            return text
        case .incorrectPassword:
            return "talkme_auth_error_password".localized
        case .none:
            return nil
        case .number(let text):
            return text
        }
    }
}
