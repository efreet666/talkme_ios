//
//  UserGeoTypes.swift
//  talkme_ios
//
//  Created by Yura Fomin on 25.01.2021.
//

enum CityCountry {
    case city(String)
    case country(String)

    var value: String {
        switch self {
        case .city(let value):
            return value
        case .country(let value):
            return value
        }
    }
}

struct UserCountryModel {
    var cityCountry: CityCountry
    var iso: String?
    var geoId: String?
}
