//
//  SocialsCollectionModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 11.03.2021.
//

struct SocialsCollectionModel {
    let data: [SocialMedia] = [.vk, .telegram]
}
