//
//  ProfileEditData.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.02.2021.
//

import UIKit

final class ProfileEditData {
    var name: String?
    var lastName: String?
    var gender: String?
    var birthday: String?
    var avatarUrl: String?
    var country: String?
    var city: String?
    var bio: String?
    var userId: String?
    var email: String?
    var contactsResponse: ContactsResponse

    init(profileResponse: ProfileResponse, contactsResponse: ContactsResponse) {
        self.name = profileResponse.firstName
        self.lastName = profileResponse.lastName
        self.gender = profileResponse.gender
        self.birthday = profileResponse.birthday
        self.avatarUrl = profileResponse.avatarUrl
        self.country = profileResponse.country
        self.city = profileResponse.city
        self.bio = profileResponse.bio
        self.userId = profileResponse.username
        self.email = contactsResponse.email
        self.contactsResponse = contactsResponse
    }
}
