//
//  SettingButtonView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxSwift
import UIKit

enum SettingButtonType {
    case becomeTeacher
    case save
    case createLesson
    case confirm
    case exitProfile
    case deleteProfile

    var title: String {
        switch self {
        case .becomeTeacher:
            return "profile_setting_become_teacher".localized
        case .save:
            return "profile_setting_button_save".localized
        case .createLesson:
            return "profile_setting_post_lesson".localized
        case .confirm:
            return "talkme_сonfirm_сode_confirm".localized
        case .exitProfile:
            return "profile_setting_exit_from_profile".localized
        case .deleteProfile:
            return "profile_setting_delete_profile".localized
        }
    }

    var backgroundColor: UIColor {
        switch self {
        case .becomeTeacher:
            return TalkmeColors.blueLabels
        case .save, .createLesson, .confirm:
            return TalkmeColors.greenLabels
        case .exitProfile, .deleteProfile:
            return TalkmeColors.white
        }
    }

    var isState: Bool {
        switch self {
        case .save:
            return true
        case .becomeTeacher:
            return false
        default:
            return true
        }
    }

    var textColor: UIColor {
        switch self {
        case .exitProfile:
            return TalkmeColors.black
        case .deleteProfile:
            return TalkmeColors.black
        default:
            return TalkmeColors.white
        }
    }

    var font: UIFont {
        switch self {
        case .exitProfile:
            return .montserratSemiBold(ofSize: 15)
        case .deleteProfile:
            return .montserratSemiBold(ofSize: 15)
        default:
            return .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 16)
        }
    }
}

final class SettingButtonView: UIView {

    // MARK: - Private Properties

    let showButton: UIButton = {
        let showButton = UIButton(type: .system)
        showButton.titleLabel?.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 16)
        showButton.setTitleColor(TalkmeColors.white, for: .normal)
        showButton.layer.cornerRadius = 12
        return showButton
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(buttonType: SettingButtonType) {
        showButton.backgroundColor = buttonType.backgroundColor
        let attributedTitle = NSAttributedString(string: buttonType.title,
                                                 attributes: [.foregroundColor: buttonType.textColor,
                                                              .font: buttonType.font ])
        showButton.setAttributedTitle(attributedTitle, for: .normal)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(showButton)

        showButton.snp.makeConstraints { button in
            button.height.equalTo(40)
            button.edges.equalToSuperview()
        }
    }
}
