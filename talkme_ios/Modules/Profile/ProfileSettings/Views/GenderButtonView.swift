//
//  GenderButtonView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 11.01.2021.
//

import UIKit

enum GenderButtonType: String {
    case male
    case female

    var title: String {
        switch self {
        case .male:
            return "profile_setting_gender_men".localized
        case .female:
            return "profile_setting_gender_woman".localized
        }
    }
}

final class GenderButtonView: UIView {

    // MARK: - Public Properties

    private(set) lazy var tap = genderButton.rx.tap

    // MARK: - Private Properties

    private let genderButton: UIButton = {
        let btn = UIButton()
        btn.setTitleColor(TalkmeColors.blackLabels, for: .normal)
        btn.titleLabel?.font = .montserratSemiBold(ofSize: 13)
        btn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -10)
        return btn
    }()

    private let activeImage = UIImage(named: "Ellipse")
    private let inactiveImage = UIImage(named: "Ellipseblack")

    // MARK: - Initializers

    init(type: GenderButtonType) {
        super.init(frame: .zero)
        setupLayout()
        configure(buttonType: type)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setActive(_ isActive: Bool) {
        genderButton.setImage(isActive ? activeImage : inactiveImage, for: .normal)
    }

    private func configure(buttonType: GenderButtonType) {
        genderButton.setTitle(buttonType.title, for: .normal)
        genderButton.setImage(inactiveImage, for: .normal)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(genderButton)

        genderButton.snp.makeConstraints { button in
            button.height.equalTo(17)
            button.edges.equalToSuperview()
        }
    }
}
