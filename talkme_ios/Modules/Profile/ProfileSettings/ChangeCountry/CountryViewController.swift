//
//  CountryViewController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.01.2021.
//

import RxCocoa
import RxSwift

final class CountryViewController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.rowHeight = UITableView.automaticDimension
        tbv.registerCells(withModels: CountryCellViewModel.self)
        return tbv
    }()

    private let searchBar = StreamSearchBar()
    private let viewModel: CountryCityProtocol
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: CountryCityProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.white
        setupViews()
        bindVM()
        viewModel.fetchData()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                tableView.deselectRow(at: indexPath, animated: true)
                return cell
            }
            .disposed(by: bag)

        tableView.rx
            .modelSelected(AnyTableViewCellModelProtocol.self)
            .compactMap { model in
                if let item = model as? CountryCellViewModel {
                    return .onItemSelected(item.country)
                }
                return nil
            }
            .bind(to: viewModel.flow)

            .disposed(by: bag)
        searchBar.searchTextField.rx.text
            .orEmpty
            .bind { [weak viewModel] in
                viewModel?.onEnter(text: $0)
            }
            .disposed(by: bag)
    }

    private func setupViews() {
        setupTableView()
        setupSearchBar()
        setupNavigation()
    }

    private func setupTableView() {
        view.addSubviews([tableView, searchBar])

        tableView.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom).offset(10)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }

    private func setupSearchBar() {
        searchBar.backgroundColor = TalkmeColors.graySearchBar
        searchBar.layer.cornerRadius = 20

        searchBar.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(11)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(40)
        }
    }

    private func setupNavigation() {
        customNavigationController?.style = .plainWhite(title: viewModel.navigationTitle)
    }
}
