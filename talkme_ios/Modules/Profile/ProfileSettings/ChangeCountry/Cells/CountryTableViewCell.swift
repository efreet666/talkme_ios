//
//  CountryTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.01.2021.
//

import RxCocoa
import RxSwift

final class CountryTableViewCell: UITableViewCell {

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.blackLabels
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.numberOfLines = 0
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(model: UserCountryModel) {
        titleLabel.text = model.cityCountry.value
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(titleLabel)

        titleLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(16)
            make.leading.trailing.equalToSuperview().inset(10)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
