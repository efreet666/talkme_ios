//
//  CountryCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.01.2021.
//

final class CountryCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let country: UserCountryModel

    // MARK: - Initializers

    init(country: UserCountryModel) {
        self.country = country
    }

    // MARK: - Public Methods

    func configure(_ cell: CountryTableViewCell) {
        cell.configure(model: country)
    }
}
