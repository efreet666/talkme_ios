//
//  CountryViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.01.2021.
//

import RxCocoa
import RxSwift

protocol CountryCityProtocol: AnyObject {
    var dataItems: PublishRelay<[CountryCellViewModel]> { get set }
    var flow: PublishRelay<CountryCityFlow> { get }
    var bag: DisposeBag { get }
    var navigationTitle: String { get }

    func fetchData()
    func onEnter(text: String)
}

enum CountryCityFlow {
    case onItemSelected(UserCountryModel)
}

final class CountryViewModel: CountryCityProtocol {

    // MARK: - Public Properties

    let navigationTitle = NavigationTitle.country.title
    lazy var dataItems = PublishRelay<[CountryCellViewModel]>()
    let flow = PublishRelay<CountryCityFlow>()
    var countries: [CountryCellViewModel] = []
    var bag = DisposeBag()

    // MARK: - Private Properties

    private let service: AccountServiceProtocol

    // MARK: - Initializers

    init(service: AccountServiceProtocol) {
        self.service = service
    }

    // MARK: - Private Methods

    func fetchData() {
        service
            .fetchCountries()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let countries = response.countries else { return }
                    let items = countries
                        .map { UserCountryModel(cityCountry: .country($0.country), iso: $0.countryCode) }
                        .compactMap(CountryCellViewModel.init)
                    self?.dataItems.accept(items)
                    self?.countries = items
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func onEnter(text: String) {
        guard !text.isEmpty else {
            dataItems.accept(countries)
            return
        }
        let firstCountry = "profile_setting_error_country".localized
        var filteredCities = countries.filter { $0.country.cityCountry.value.contains(text) }
        let item = CountryCellViewModel.init(country: .init(cityCountry: .country(firstCountry), iso: "", geoId: "0"))
        filteredCities.insert(item, at: 0)
        dataItems.accept(filteredCities)
    }
}
