//
//  CitiesViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 25.01.2021.
//

import RxCocoa
import RxSwift

final class CitiesViewModel: CountryCityProtocol {

    // MARK: - Public Properties

    let navigationTitle = NavigationTitle.city.title
    lazy var dataItems = PublishRelay<[CountryCellViewModel]>()
    let flow = PublishRelay<CountryCityFlow>()
    var bag = DisposeBag()
    var isoCode: String
    var cities: [CountryCellViewModel] = []

    // MARK: - Private Properties

    private let service: AccountServiceProtocol

    // MARK: - Initializers

    init(service: AccountServiceProtocol, isoCode: String) {
        self.service = service
        self.isoCode = isoCode
    }

    // MARK: - Private Methods

    func fetchData() {
        service
            .fetchCities(isoCode: isoCode)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let cities = response.cities else { return }
                    let items = cities
                        .map {UserCountryModel(cityCountry: .city($0.city), iso: nil, geoId: String($0.geoId))}
                        .compactMap(CountryCellViewModel.init)
                    self?.dataItems.accept(items)
                    self?.cities = items
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func onEnter(text: String) {
        guard !text.isEmpty else {
            dataItems.accept(cities)
            return
        }
        let firstCity = "profile_setting_error_city".localized
        var filteredCities = cities.filter { $0.country.cityCountry.value.contains(text) }
        let item = CountryCellViewModel.init(country: .init(cityCountry: .city(firstCity), iso: "", geoId: "0"))
        filteredCities.insert(item, at: 0)
        dataItems.accept(filteredCities)
    }
}
