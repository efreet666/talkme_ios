//
//  PopUpSocialsViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 20.01.2021.
//

import RxCocoa
import RxSwift

enum PopupSocialsFlow: Equatable {
    case onAdd(social: SocialMedia, url: String?)
}

final class PopupSocialsViewModel {

    // MARK: - Public properties

    let flow = PublishRelay<PopupSocialsFlow>()
    let bag = DisposeBag()
    let service: AccountServiceProtocol
    let socialMedia: SocialMedia
    let textFieldString = BehaviorRelay<String?>(value: "")

    // MARK: - Initializers

    init(service: AccountServiceProtocol, social: SocialMedia) {
        self.service = service
        self.socialMedia = social
    }

    // MARK: - Public Methods

    func onSend() {
        var str = textFieldString.value ?? ""
        str = str.trimmingCharacters(in: .whitespaces)
        var url = socialMedia.textFieldMask + str
        if socialMedia == .instagram {
            var text = textFieldString.value!
            if text.hasPrefix("@") {
                text.remove(at: text.startIndex)
            }
            url = socialMedia.textFieldMask + text
        }
        service
            .addSocial(request: socialMedia, url: url)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success:
                    self.flow.accept(.onAdd(social: self.socialMedia, url: url))
                case .error:
                    return
                }
            }
            .disposed(by: bag)
    }
}
