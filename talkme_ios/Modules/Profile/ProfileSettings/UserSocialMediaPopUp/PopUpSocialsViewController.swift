//
//  PopUpSocialsViewController.swift
//  talkme_ios
//
//  Created by Майя Калицева on 20.01.2021.
//

import RxCocoa
import RxSwift

final class PopUpSocialsViewController: UIViewController {

    // MARK: - Public Properties

    let bag = DisposeBag()
    lazy var onSendTap = sendButton.rx.tap

    // MARK: - Private Properties

    private let buttonsSeparator: UIView = {
        let sep = UIView()
        sep.backgroundColor = TalkmeColors.separatorGray
        sep.alpha = 0.36
        return sep
    }()

    private let separatorView: UIView = {
        let sep = UIView()
        sep.backgroundColor = TalkmeColors.separatorGray
        sep.alpha = 0.36
        return sep
    }()

    private let viewModel: PopupSocialsViewModel
    private let cancelButton = AlertButton(frame: .zero, type: .regular)
    private let sendButton = AlertButton(frame: .zero, type: .primary)

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.font = .montserratSemiBold(ofSize: 15)
        return lbl
    }()

    private let socialsTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = TalkmeColors.white
        tf.font = .montserratFontMedium(ofSize: 13)
        tf.textColor = TalkmeColors.black
        tf.layer.cornerRadius = 13
        tf.leftViewMode = .always
        return tf
    }()

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.lightGrayPopup
        view.layer.cornerRadius = 15
        return view
    }()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = 1
        return sv
    }()

    private var errorLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: 13)
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        return lbl
    }()

    private var maskLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .montserratFontMedium(ofSize: 13)
        lbl.textColor = TalkmeColors.black
        return lbl
    }()

    // MARK: - Initializers

    init(viewModel: PopupSocialsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.popupBackground
        setupLayout()
        titleLabel.text = viewModel.socialMedia.title
        maskLabel.text = "  " + viewModel.socialMedia.textFieldMask
        socialsTextField.leftView = maskLabel
        socialsTextField.attributedPlaceholder = viewModel.socialMedia.textFieldPlaceholder
        bindUI()
        socialsTextField.becomeFirstResponder()
    }

    // MARK: - Private Methods

    func bindUI() {
        socialsTextField.rx.text
            .withLatestFrom(socialsTextField.rx.text)
            .bind(to: viewModel.textFieldString)
            .disposed(by: bag)

        socialsTextField
            .rx
            .controlEvent(.allEditingEvents)
            .debounce(.milliseconds(30), scheduler: MainScheduler.instance)
            .bind(onNext: { [weak errorLabel, weak socialsTextField] _ in
                errorLabel?.text = nil
                socialsTextField?.textColor = TalkmeColors.black
            })
            .disposed(by: bag)

        cancelButton.rx
            .tap
            .bind { [weak self] _ in
                self?.dismiss(animated: true)
            }
            .disposed(by: bag)

        sendButton.rx
            .tap
            .bind { [weak self] _ in
                guard let self = self else { return }
                let text = self.socialsTextField.text ?? ""
                guard
                    !text.isEmpty else {
                    self.socialsTextField.textColor = TalkmeColors.warningColor
                    self.errorLabel.text = self.viewModel.socialMedia.errorText
                    return
                }
                self.socialsTextField.textColor = TalkmeColors.black
                self.viewModel.onSend()
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        view.addSubview(containerView)
        stackView.addArrangedSubview(cancelButton)
        stackView.addArrangedSubview(sendButton)
        containerView.addSubviews(titleLabel, socialsTextField, buttonsSeparator, separatorView, stackView, errorLabel)

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(25)
            make.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(25)
            make.leading.trailing.equalToSuperview().inset(15)
        }

        socialsTextField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(13)
            make.bottom.equalTo(errorLabel.snp.top).inset(-5)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(30)
        }

        errorLabel.snp.makeConstraints { make in
            make.top.equalTo(socialsTextField.snp.bottom).inset(5)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(separatorView.snp.top).inset(-10)
        }

        buttonsSeparator.snp.makeConstraints { make in
            make.top.equalTo(separatorView.snp.bottom)
            make.bottom.centerX.equalToSuperview()
            make.width.equalTo(1)
        }

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(errorLabel.snp.bottom).inset(10)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        stackView.snp.makeConstraints { make in
            make.top.equalTo(separatorView.snp.bottom).offset(7)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(7)
        }
    }
}
