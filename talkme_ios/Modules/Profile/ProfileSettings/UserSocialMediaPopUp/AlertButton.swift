//
//  AlertButton.swift
//  talkme_ios
//
//  Created by Майя Калицева on 20.01.2021.
//

import UIKit

final class AlertButton: UIButton {

    enum ButtonType {
        case primary
        case regular

        var title: String {
            switch self {
            case .regular:
                return "profile_setting_cancel".localized
            case .primary:
                return "profile_setting_button_save".localized
            }
        }

        var textStyle: UIFont {
            switch self {
            case .primary:
                return .montserratBold(ofSize: 15)
            case .regular:
                return .montserratFontRegular(ofSize: 15)
            }
        }

        var textColor: UIColor {
            switch self {
            case .primary, .regular:
                return TalkmeColors.shadow
            }
        }
    }

    // MARK: - Lifecycle

    init(frame: CGRect, type: ButtonType) {
        super.init(frame: frame)
        setTitle(type.title, for: .normal)
        setTitleColor(type.textColor, for: .normal)
        titleLabel?.font = type.textStyle
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
