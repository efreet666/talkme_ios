//
//  PopupCodeViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 20.01.2021.
//

import RxCocoa
import RxSwift

enum PopupCodeFlow: Equatable {
    case onCodeConfirm(code: String, phoneNumber: String)
    case backOnProfile
}

final class PopupCodeViewModel {

    // MARK: - Public properties

    let flow = PublishRelay<PopupCodeFlow>()
    let bag = DisposeBag()
    let error = PublishRelay<AuthError>()

    // MARK: - Private properties

    private let phoneNumber: String
    private let service: AccountServiceProtocol

    // MARK: - Initializers

    init(service: AccountServiceProtocol, phoneNumber: String) {
        self.service = service
        self.phoneNumber = phoneNumber
    }

    // MARK: - Public Methods

    func confirmCode(_ code: String) {
        let request = SendMobileAndCodeRequest(code: code, mobile: phoneNumber)
        service
            .sendMobileAndCode(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let number = response.mobile else { return }
                    self?.flow.accept(.onCodeConfirm(code: code, phoneNumber: number))
                    self?.flow.accept(.backOnProfile)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func sendMobileCode() {
        let request = SendMobileRequest(mobile: phoneNumber)
        service
            .getMobileCode(request: request)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    if let errorMessage = response.msg {
                        self?.error.accept(.apiError(errorMessage))
                        return
                    }
                    print(response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
}
