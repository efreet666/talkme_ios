//
//  PopupCodeViewController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 19.01.2021.
//

import RxSwift
import RxCocoa

final class PopupCodeViewController: UIViewController {

    private enum Constants {
        static let codeLength = 5
    }

    // MARK: - Private Properties

    private var rxTimer: Disposable?
    private let codeView: ConfirmationCodeView = {
        let cv = ConfirmationCodeView(codeLength: Constants.codeLength)
        return cv
    }()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "profile_setting_popup_message".localized
        lbl.textColor = .black
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.font = .montserratSemiBold(ofSize: 15)
        return lbl
    }()

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.lightGrayPopup
        view.layer.cornerRadius = 14
        return view
    }()

    private let sendCodeAgainButton: UIButton = {
        let button = UIButton(type: .system)
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: UIFont.montserratFontMedium(ofSize: 12),
            NSAttributedString.Key.foregroundColor: TalkmeColors.blueLabels,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedString = NSMutableAttributedString(
            string: "profile_setting_send_code_again".localized, attributes: attributes)
        button.setTitleColor(.black, for: .normal)
        button.setAttributedTitle(attributedString, for: .normal)
        return button
    }()

    private let errorLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.error
        lbl.font = .montserratFontMedium(ofSize: 11)
        lbl.textAlignment = .center
        return lbl
    }()

    private let timerLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontRegular(ofSize: 13)
        lbl.textAlignment = .center
        return lbl
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorGray
        view.alpha = 0.36
        return view
    }()

    private let sendCodeButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("profile_setting_send_code".localized, for: .normal)
        btn.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        btn.setTitleColor(.white, for: .highlighted)
        btn.titleLabel?.font = .montserratBold(ofSize: 15)
        return btn
    }()

    private let viewModel: PopupCodeViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: PopupCodeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.popupBackground
        setupLayout()
        bindKeyboard()
        bindUI()
        bindVM()
        codeView.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        rxTimer?.dispose()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .error
            .bind { [weak self] in
                self?.animateError($0)
            }
            .disposed(by: bag)
    }

    private func bindUI() {
        codeView
            .unfinishedCodeValue
            .filter { $0 != nil }
            .bind { [weak self] _ in
                self?.animateError(.none)
            }
            .disposed(by: bag)

        view.rx.tapGesture()
            .bind { _ in
                self.codeView.resignFirstResponder()
            } .disposed(by: bag)

        containerView.rx.tapGesture()
            .bind {_ in
                self.codeView.becomeFirstResponder()
            } .disposed(by: bag)

        sendCodeButton.rx
            .tap
            .withLatestFrom(codeView.codeValue)
            .bind { [weak self] code in
                guard let code = code else { return }
                self?.viewModel.confirmCode(code)
            }
            .disposed(by: bag)

        sendCodeAgainButton.rx
            .tap
            .bind { [weak self] _ in
                self?.setupTimer()
                self?.viewModel.sendMobileCode()
            }
            .disposed(by: bag)
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                self.containerView.snp.updateConstraints { make in
                    make.centerY.equalToSuperview().inset(keyboardVisibleHeight)
                }
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    private func setupTimer() {
        rxTimer = Observable<Int>.interval(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .map {-$0 + 59}
            .map { [weak self] timerValue in
                guard let self = self else { return "" }
                let seconds = (timerValue % 3600) % 60
                let stringFormatted = String(format: "%02d", seconds)

                if timerValue != 0 {
                    self.sendCodeAgainButton.isEnabled = false
                    return stringFormatted
                } else {
                    self.rxTimer?.dispose()
                    self.timerLabel.text = ""
                    self.sendCodeAgainButton.isEnabled = true
                    return ""
                }
            }.subscribe(onNext: { [weak self] value in
                self?.timerLabel.text = value
            })
    }

    private func animateError(_ error: AuthError) {
        let text = error.title
        guard text != errorLabel.text else { return }

        let isHidden = text == nil
        UIView.transition(
            with: errorLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.errorLabel.alpha = isHidden ? 0 : 1
            self.errorLabel.text = text
        }
    }

    private func setupLayout() {
        view.addSubview(containerView)
        containerView.addSubviews(titleLabel, codeView, errorLabel, sendCodeAgainButton, timerLabel, separatorView, sendCodeButton)

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(15)
            make.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(25)
            make.leading.trailing.equalToSuperview().inset(15)
        }

        codeView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(17)
            make.leading.trailing.equalToSuperview().inset(25)
        }

        errorLabel.snp.makeConstraints { make in
            make.top.equalTo(codeView.snp.bottom).offset(55)
            make.leading.trailing.equalToSuperview()
        }

        sendCodeAgainButton.snp.makeConstraints { make in
            make.top.equalTo(errorLabel.snp.bottom).offset(3)
            make.centerX.equalToSuperview()
        }

        timerLabel.snp.makeConstraints { make in
            make.centerY.equalTo(sendCodeAgainButton.snp.centerY)
            make.leading.equalTo(sendCodeAgainButton.snp.trailing).offset(2)
        }

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(sendCodeAgainButton.snp.bottom).offset(16)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        sendCodeButton.snp.makeConstraints { make in
            make.top.equalTo(separatorView.snp.bottom).offset(7)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(7)
        }
    }
}
