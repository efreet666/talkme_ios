//
//  AboutmeCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 10.01.2021.
//

import RxCocoa
import RxSwift

final class AboutmeCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = BehaviorRelay<String?>(value: nil)
    let heightChanged = PublishRelay<Void>()
    let bag = DisposeBag()
    var text: String?

    init(text: String?) {
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: AboutmeTableViewCell) {
        cell.configure(text: text)

        cell
            .aboutMeText
            .do(onNext: { [weak self] in
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .sizeChanged
            .bind(to: heightChanged)
            .disposed(by: cell.bag)
    }
}
