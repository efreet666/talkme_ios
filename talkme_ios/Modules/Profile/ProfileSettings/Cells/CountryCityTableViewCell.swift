//
//  CountryCityTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 22.01.2021.
//

import RxSwift

enum UserGeoType {
    case country(String?)
    case city(name: String?)

    var title: String {
        switch self {
        case .country:
            return "profile_setting_country".localized
        case .city:
            return "profile_setting_city".localized
        }
    }

    var placeholder: String {
        switch self {
        case .country:
            return "profile_setting_error_country".localized
        case .city:
            return "profile_setting_error_city".localized
        }
    }

    var textValue: String? {
        switch self {
        case .country(let name):
            return name
        case .city(let name):
            return name
        }
    }
}

final class CountryCityTableViewCell: UITableViewCell {

    // MARK: - Public properties

    private(set) lazy var countryText = countryLabel.rx.text
    private(set) lazy var warningText = warningLabel.rx.text
    var bag = DisposeBag()

    // MARK: - Private Properties

    private let countryLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.blackLabels
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        return lbl
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.alpha = 0
        return lbl
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorView
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(type: UserGeoType) {
        titleLabel.text = type.title
        countryLabel.text = type.textValue ?? type.placeholder
    }

    func setupError(_ error: ProfileError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupLayout() {
        contentView.addSubviews([isRequiredImage, titleLabel, countryLabel, separatorView, warningLabel])

        isRequiredImage.snp.makeConstraints { image in
            image.top.equalToSuperview().inset(UIScreen.isSE ? 15 : 25)
            image.leading.equalToSuperview().inset(10)
            image.size.equalTo(5)
        }

        titleLabel.snp.makeConstraints { label in
            label.centerY.equalTo(isRequiredImage.snp.centerY)
            label.leading.equalTo(isRequiredImage.snp.trailing).offset(5)
            label.trailing.lessThanOrEqualToSuperview().inset(10)
        }

        countryLabel.snp.makeConstraints { label in
            label.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 4 : 8)
            label.leading.trailing.equalToSuperview().inset(10)
            label.bottom.equalToSuperview()
        }

        warningLabel.snp.makeConstraints { make in
            make.centerY.equalTo(isRequiredImage.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
            make.leading.lessThanOrEqualTo(titleLabel.snp.trailing)
        }

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(warningLabel.snp.bottom).offset(UIScreen.isSE ? 10 : 40)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(1)
            make.bottom.equalToSuperview().inset(-5)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
