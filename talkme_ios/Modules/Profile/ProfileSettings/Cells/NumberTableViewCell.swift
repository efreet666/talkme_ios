//
//  NumberTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxSwift

final class NumberTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var textRelay = numberField.phoneString
    private(set) lazy var selectCountryTap = numberField.selectCountryTap
    private(set) lazy var countryCodeText = numberField.countryCodeText
    private(set) lazy var countryCodeImage = numberField.countryCodeImage
    private(set) var bag = DisposeBag()

    var isValidNumber: Bool { numberField.isValidNumber }
    var currentCountry: CountryType = .russia {
        didSet {
            numberField.region = currentCountry.model.regionCode
        }
    }

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "profile_setting_phone_number".localized
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        return lbl
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 9 : 11)
        lbl.alpha = 0
        return lbl
    }()

    private let numberField = NumberTextField()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    func configure(mobile: String) {
        numberField.setText(mobile)
    }

    func setupError(_ error: ProfileError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupUI() {
        numberField.textfieldFont = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        numberField.configure(font: .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15))
    }

    private func setupLayout() {
        contentView.addSubviews([warningLabel, titleLabel, numberField])

        titleLabel.snp.makeConstraints { label in
            label.top.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 18)
        }

        warningLabel.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
            make.leading.lessThanOrEqualTo(titleLabel.snp.trailing)
        }

        numberField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 5 : 8)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 18)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
