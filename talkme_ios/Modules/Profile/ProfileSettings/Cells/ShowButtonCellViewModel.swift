//
//  ShowButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift

final class ShowButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let tapRelay = PublishRelay<Void>()
    let buttonIsEnabled = BehaviorRelay<Bool>(value: true)
    let bag = DisposeBag()

    private var isTeacher: Bool?

    // MARK: - Public Methods

    func setupButton(isTeacher: Bool) {
        self.isTeacher = isTeacher
    }

    func configure(_ cell: ShowButtonTableViewCell) {
        cell.setButton(isTeacher: isTeacher)

        cell
            .buttonTap
            .bind(to: tapRelay)
            .disposed(by: cell.bag)

        buttonIsEnabled
            .bind(to: cell.buttonIsEnabled)
            .disposed(by: cell.bag)
    }
}
