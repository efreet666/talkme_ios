//
//  PasswordTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxSwift

final class PasswordTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var passwordText = password.textField.rx.text
    private(set) lazy var warningText = warningLabel.rx.text
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        return lbl
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.alpha = 0
        return lbl
    }()

    private let password = TextField()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, isHidden: Bool, isSecureTextEntry: Bool, text: String?) {
        titleLabel.text = title
        password.eyesButton.isHidden = isHidden
        password.textField.isSecureTextEntry = isSecureTextEntry
        password.textField.text = text
    }

    func setupError(_ error: ProfileError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupUI() {
        password.textField.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        let preventPasswordSuggestionContentType: UITextContentType
        if #available(iOS 12.0, *) {
            preventPasswordSuggestionContentType = .oneTimeCode
        } else {
            preventPasswordSuggestionContentType = .init(rawValue: "")
        }
        password.textField.textContentType = preventPasswordSuggestionContentType
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, password, warningLabel])

        titleLabel.snp.makeConstraints { label in
            label.top.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 18)
        }

        password.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 15 : 18)
        }

        warningLabel.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
            make.leading.lessThanOrEqualTo(titleLabel.snp.trailing)
        }
    }
}
