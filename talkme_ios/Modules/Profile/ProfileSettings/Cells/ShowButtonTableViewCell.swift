//
//  ShowButtonTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxSwift

final class ShowButtonTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var buttonTap = saveButton.showButton.rx.tap
    private(set) lazy var buttonIsEnabled = saveButton.showButton.rx.isEnabled
    private(set) lazy var buttonBackgroundColor = saveButton.showButton.rx.backgroundColor
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let saveButton = SettingButtonView()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setButton(isTeacher: Bool?) {
        guard let isTeacher = isTeacher else { return saveButton.isHidden = true }
        saveButton.configure(buttonType: isTeacher ? .save : .becomeTeacher)
        saveButton.layer.cornerRadius = UIScreen.isSE ? 20 : 23
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupLayout() {
        contentView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(10)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 40 : 46)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
