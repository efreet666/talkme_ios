//
//  NumberCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift

final class NumberCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = BehaviorRelay<String?>(value: nil)
    let countryRelay = PublishRelay<CountryType>()
    let error = PublishRelay<ProfileError>()
    let countryButtonRelay = PublishRelay<Void>()
    let bag = DisposeBag()
    var mobile: String?

    // MARK: - Public Properties

    init(mobile: String?) {
        self.mobile = mobile
    }

    // MARK: - Public Methods

    func configure(_ cell: NumberTableViewCell) {
        cell.configure(mobile: mobile ?? "")

        cell
            .textRelay
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .selectCountryTap
            .bind(to: countryButtonRelay)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        countryRelay
            .bind { [weak cell] type in
                cell?.currentCountry = type
            }
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.code)
            .bind(to: cell.countryCodeText)
            .disposed(by: cell.bag)

        countryRelay
            .map(\.model.image)
            .bind(to: cell.countryCodeImage)
            .disposed(by: cell.bag)
    }
}
