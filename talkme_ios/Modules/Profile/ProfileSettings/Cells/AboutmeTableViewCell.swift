//
//  AboutmeTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 10.01.2021.
//

import RxSwift
import RxCocoa
import GrowingTextView
import UIKit

final class AboutmeTableViewCell: UITableViewCell {

    private enum Constants {
        static let textInsetV: CGFloat = UIScreen.isSE ? 2 : 4
    }

    // MARK: - Public Properties

    private(set) lazy var aboutMeText = aboutMeTextView.rx.text
    private(set) var bag = DisposeBag()
    private(set) lazy var sizeChanged = ControlEvent(events: heightChanged)

    // MARK: - Private Properties

    private let heightChanged = PublishRelay<Void>()

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.buttonGrayColor
        view.layer.cornerRadius = 11
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
        return view
    }()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "profile_setting_about_me".localized
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        return lbl
    }()

    private let aboutMeTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.backgroundColor = TalkmeColors.buttonGrayColor
        textView.textColor = TalkmeColors.blackLabels
        textView.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
        textView.isHidden = false
        textView.attributedPlaceholder = NSAttributedString(
            string: "profile_setting_info_about_me".localized,
            attributes:
                [NSAttributedString.Key.foregroundColor: TalkmeColors.grayDescription,
                 NSAttributedString.Key.font: UIFont.montserratFontRegular(ofSize: UIScreen.isSE ? 13 : 15)
                ])
        textView.textContainerInset = UIEdgeInsets(
            top: Constants.textInsetV,
            left: 0,
            bottom: Constants.textInsetV,
            right: 0)
        textView.maxLength = 600
        textView.textContainer.lineFragmentPadding = 0.0
        return textView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        aboutMeTextView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Properties

    func configure(text: String?) {
        aboutMeTextView.text = text
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        containerView.addSubviews([titleLabel, aboutMeTextView])
        contentView.addSubview(containerView)

        containerView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
            make.height.greaterThanOrEqualTo(145)
        }

        titleLabel.snp.makeConstraints { label in
            label.top.equalToSuperview().offset(UIScreen.isSE ? 17 : 22)
            label.leading.trailing.equalToSuperview().inset(15)
            label.height.equalTo(15)
        }

        aboutMeTextView.snp.makeConstraints { textView in
            textView.top.equalTo(titleLabel.snp.bottom).offset(6)
            textView.leading.trailing.equalToSuperview().inset(14)
            textView.bottom.equalToSuperview().inset(UIScreen.isSE ? 17 : 25)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension AboutmeTableViewCell: GrowingTextViewDelegate {

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.numberOfLines() > 20 && !text.isBackspace() {
            return false
        }
        return true
    }

    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        heightChanged.accept(())
    }
}

extension UITextView {

    func numberOfLines() -> Int {
        if let fontUnwrapped = self.font {
            return Int(self.contentSize.height / fontUnwrapped.lineHeight)
        }
        return 0
    }
}
