//
//  AvatarTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift
import Kingfisher

final class AvatarTableViewCell: UITableViewCell {

    // MARK: Public Properties

    private(set) lazy var onPhotoTap = photoButton.rx.tap
    private(set) lazy var onPickImage = avatarImage.rx.image
    let onLoadImage = PublishRelay<UIImage>()
    private(set) lazy var warningText = warningLabel.rx.text
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let photoButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "camera"), for: .normal)
        btn.imageEdgeInsets = .init(top: 12, left: 13, bottom: 12, right: 13)
        return btn
    }()

    private let avatarImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.layer.cornerRadius = UIScreen.isSE ? 39 : 50
        return img
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.alpha = 0
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Properties

    func configure(imageString: String?, image: UIImage?) {
        if let image = image {
            avatarImage.image = image
            onLoadImage.accept(image)
            return
        }
        let noAvatarImage = UIImage(named: "noAvatar")
        guard let imageString = imageString,
              !imageString.isEmpty,
              let url = URL(string: imageString)  else {
            avatarImage.image = noAvatarImage
            return
        }
        avatarImage.kf.setImage(with: url, placeholder: noAvatarImage) { [weak self] result in
            switch result {
            case .success(let value):
                self?.onLoadImage.accept(value.image)
            case .failure:
                break
            }
        }
    }

    func setupError(_ error: ProfileError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Properties

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
        isRequiredImage.isHidden = UserDefaultsHelper.shared.userTypeOfNumberClass.isEmpty
    }

    private func setupLayout() {
        contentView.addSubviews([isRequiredImage, avatarImage, photoButton, warningLabel])

        photoButton.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
            make.width.height.equalTo(UIScreen.isSE ? 78 : 101)
        }

        avatarImage.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(15)
            make.size.equalTo(UIScreen.isSE ? 78 : 101)
            make.centerX.equalToSuperview()
        }

        isRequiredImage.snp.makeConstraints { make in
            make.top.equalTo(avatarImage)
            make.trailing.equalTo(avatarImage)
        }

        warningLabel.snp.makeConstraints { make in
            make.centerY.equalTo(avatarImage.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
        }
    }
}
