//
//  UserInfoTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxSwift

final class UserInfoTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var textFieldText = inputDataField
        .textField
        .rx
        .text
        .distinctUntilChanged()
        .debounce(.milliseconds(250), scheduler: MainScheduler.instance)
    private(set) lazy var warningText = warningLabel.rx.text
    private(set) var bag = DisposeBag()
    var isOnDelegateTF = false

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        return lbl
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.alpha = 0
        return lbl
    }()

    private let inputDataField = TextField()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorView
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        inputDataField.textField.delegate = self
        setupLayout()
        setupUI()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, isHidden: Bool, placeholder: String, text: String?) {
        titleLabel.text = title
        inputDataField.eyesButton.isHidden = isHidden
        inputDataField.textField.placeholder = placeholder
        inputDataField.textField.text = text
        inputDataField.separator.isHidden = true
        isRequiredImage.isHidden = titleLabel.text == "profile_setting_mail".localized && UserDefaultsHelper.shared.userTypeOfNumberClass.isEmpty
    }

    func setupError(_ error: ProfileError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupUI() {
        inputDataField.textField.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
    }

    private func setupLayout() {
        contentView.addSubviews([isRequiredImage, titleLabel, inputDataField, warningLabel, separatorView])

        isRequiredImage.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel)
            make.leading.equalToSuperview().inset(10)
            make.size.equalTo(5)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 10 : 18)
            make.leading.equalTo(isRequiredImage.snp.trailing).offset(5)
            make.trailing.lessThanOrEqualToSuperview().inset(10)
        }

        inputDataField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(3)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalTo(separatorView.snp.top).inset(UIScreen.isSE ? 0 : -8)
        }

        warningLabel.snp.makeConstraints { make in
            make.centerY.equalTo(isRequiredImage.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
            make.leading.lessThanOrEqualTo(titleLabel.snp.trailing)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension UserInfoTableViewCell: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if isOnDelegateTF {
            let currentCharacterCount = textField.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 30
        }
        return true
    }
}
