//
//  UserInfoCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift
import UIKit

final class UserInfoCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = BehaviorRelay<String?>(value: nil)
    let error = PublishRelay<ProfileError>()
    let warningRelay = PublishRelay<String>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private let isHidden: Bool
    private let placeholder: String
    private var text: String?
    private let isOnDelegateTF: Bool

    // MARK: - Initializers

    init(title: String, isHidden: Bool = true, placeholder: String, text: String?, isOnDelegateTF: Bool) {
        self.title = title
        self.isHidden = isHidden
        self.placeholder = placeholder
        self.text = text
        self.isOnDelegateTF = isOnDelegateTF
    }

    // MARK: - Public Methods

    func setupError(apiError: String) {
        error.accept(.apiError(apiError))
    }

    func configure(_ cell: UserInfoTableViewCell) {
        cell.configure(title: title, isHidden: isHidden, placeholder: placeholder, text: text)

        cell
            .textFieldText
            .do(onNext: { [weak self, weak cell] in
                cell?.setupError(ProfileError.none)
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        warningRelay
            .bind(to: cell.warningText)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)

        if isOnDelegateTF {
            cell.isOnDelegateTF = true
        }
    }
}
