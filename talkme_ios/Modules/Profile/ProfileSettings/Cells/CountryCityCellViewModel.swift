//
//  CountryCityCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 22.01.2021.
//

import RxCocoa
import RxSwift

final class CountryCityCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let geoType: BehaviorRelay<UserGeoType>
    let warningRelay = PublishRelay<String>()
    let error = PublishRelay<ProfileError>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(geoType: UserGeoType) {
        self.geoType = .init(value: geoType)
    }

    // MARK: - Public Methods

    func configure(_ cell: CountryCityTableViewCell) {

        geoType
            .do(onNext: { [weak cell] in
                guard !($0.textValue == nil) else { return }
                cell?.setupError(ProfileError.none)
            })
            .bind { [weak cell] in
                cell?.configure(type: $0)
            }
            .disposed(by: cell.bag)

        warningRelay
            .bind(to: cell.warningText)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }

    func update(geoType: UserGeoType) {
        self.geoType.accept(geoType)
    }
}
