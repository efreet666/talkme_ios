//
//  DateInfoTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 11.01.2021.
//

import RxSwift
import RxCocoa

final class DateInfoTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var onButtonTap = PublishRelay<Void>()
    private(set) lazy var dateText = dateTextField.rx.text
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        return lbl
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorView
        return view
    }()

    private let dateTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "01.01.2000"
        tf.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        tf.textColor = TalkmeColors.blackLabels
        return tf
    }()

    private let dateButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "calendar"), for: .normal)
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        return btn
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.alpha = 0
        return lbl
    }()

    private let datePicker = UIDatePicker()
    private let doneButton = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupUI()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, text: String?) {
        titleLabel.text = title
        dateTextField.text = text
        bindUI()
        updateDatePicker(text: text)
    }

    func setupError(_ error: ProfileError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func updateDatePicker(text: String?) {
        guard let text = text,
              let date = Formatters.formatter.date(from: text) else {
            return
        }
        datePicker.setDate(date, animated: true)
    }

    private func bindUI() {
        dateButton.rx.tap
            .do(onNext: { [weak self] in
                guard let self = self else { return }
                self.dateTextField.becomeFirstResponder()
            })
            .bind(to: onButtonTap)
            .disposed(by: bag)

        doneButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.dateTextField.text = Formatters.formatter.string(from: self.datePicker.date)
                self.contentView.endEditing(true)
            }
            .disposed(by: bag)
    }

    private func setupUI() {
        dateTextField.inputView = datePicker
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }

        let minDate = Calendar.current.date(byAdding: .year, value: -90, to: Date())
        let maxDate = Calendar.current.date(byAdding: .year, value: -5, to: Date())
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        setupToolbar()
    }

    private func setupToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: true)
        dateTextField.inputAccessoryView = toolBar
    }

    private func setupLayout() {
        contentView.addSubviews([isRequiredImage, titleLabel, dateTextField, dateButton, separatorView, warningLabel])

        isRequiredImage.snp.makeConstraints { image in
            image.top.equalToSuperview().inset(UIScreen.isSE ? 15 : 25)
            image.leading.equalToSuperview().inset(10)
            image.size.equalTo(5)
        }

        titleLabel.snp.makeConstraints { label in
            label.centerY.equalTo(isRequiredImage.snp.centerY)
            label.leading.equalTo(isRequiredImage.snp.trailing).offset(5)
            label.trailing.lessThanOrEqualToSuperview().inset(10)
        }

        dateTextField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 4 : 7)
            make.leading.equalToSuperview().inset(10)
            make.trailing.lessThanOrEqualToSuperview().inset(20)
        }

        dateButton.snp.makeConstraints { button in
            button.centerY.equalTo(dateTextField.snp.centerY)
            button.trailing.equalToSuperview().inset(10)
            button.size.equalTo(UIScreen.isSE ? 15 : 19)
        }

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(dateTextField.snp.bottom).offset(UIScreen.isSE ? 10 : 18)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }

        warningLabel.snp.makeConstraints { make in
            make.centerY.equalTo(isRequiredImage.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
            make.leading.lessThanOrEqualTo(titleLabel.snp.trailing)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
