//
//  GenderTableViewCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift

enum Gender: String {
    case male
    case female
}

final class GenderTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var onButtonTap = PublishRelay<Gender?>()
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "profile_setting_gender".localized
        lbl.textColor = TalkmeColors.blueLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        return lbl
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 12)
        lbl.alpha = 0
        return lbl
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorView
        return view
    }()

    private let maleButton = GenderButtonView(type: .male)
    private let femaleButton = GenderButtonView(type: .female)

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        maleButton.setActive(true)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(gender: Gender?) {
        maleButton.setActive(gender == .male)
        femaleButton.setActive(gender == .female)
        bindUI()
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func bindUI() {
        maleButton.tap
            .do(onNext: { [weak self] in
                self?.maleButton.setActive(true)
                self?.femaleButton.setActive(false)
            })
            .map { _ in .male }
            .bind(to: onButtonTap)
            .disposed(by: bag)

        femaleButton.tap
            .do(onNext: { [weak self] in
                self?.maleButton.setActive(false)
                self?.femaleButton.setActive(true)
            })
            .map { _ in .female }
            .bind(to: onButtonTap)
            .disposed(by: bag)
    }

    private func setupLayout() {
        contentView.addSubviews([isRequiredImage, titleLabel, maleButton, femaleButton, separatorView, warningLabel])

        isRequiredImage.snp.makeConstraints { image in
            image.top.equalToSuperview().inset(15)
            image.leading.equalToSuperview().inset(10)
            image.size.equalTo(5)
        }

        titleLabel.snp.makeConstraints { label in
            label.centerY.equalTo(isRequiredImage.snp.centerY)
            label.leading.equalTo(isRequiredImage.snp.trailing).offset(5)
            label.trailing.lessThanOrEqualTo(warningLabel.snp.trailing)
        }

        maleButton.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 13)
            make.leading.equalToSuperview().offset(10)
        }

        femaleButton.snp.makeConstraints { make in
            make.top.equalTo(maleButton.snp.bottom).offset(UIScreen.isSE ? 10 : 13)
            make.leading.equalToSuperview().offset(10)
        }

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(femaleButton.snp.bottom).offset(UIScreen.isSE ? 10 : 16)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }

        warningLabel.snp.makeConstraints { make in
            make.centerY.equalTo(isRequiredImage.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
            make.leading.lessThanOrEqualTo(titleLabel.snp.trailing)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
