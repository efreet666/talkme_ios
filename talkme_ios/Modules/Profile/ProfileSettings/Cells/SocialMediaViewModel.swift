//
//  SocialMediaViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 13.01.2021.
//

import RxSwift
import RxCocoa

final class SocialMediaViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let bag = DisposeBag()
    let onSocialTapped = PublishRelay<(SocialMedia, Bool)>()

    // MARK: - Private properties

    private let dataModel = SocialsCollectionModel()
    private var dataItems: [AnyCollectionViewCellModelProtocol] = []
    private let contacts: ContactsResponse?

    // MARK: Public Methods

    init(contacts: ContactsResponse? = nil) {
        self.contacts = contacts
        dataItems = dataModel.data.map { SocialsCollectionViewModel(categoryModel: $0, resp: contacts) }
    }

    // MARK: Public Methods

    func configure(_ cell: SocialMediaViewCell) {
        cell.setMainItems(dataItems: dataItems)
        cell
            .onSocialTap
            .bind(to: onSocialTapped)
            .disposed(by: cell.bag)
    }

    func updateInfo(_ contacts: ContactsResponse? = nil) {
        dataItems = dataModel.data.map { SocialsCollectionViewModel(categoryModel: $0, resp: contacts) }
    }
}
