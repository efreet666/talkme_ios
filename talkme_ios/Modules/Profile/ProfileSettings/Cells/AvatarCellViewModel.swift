//
//  AvatarCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift

final class AvatarCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let onLoadImage = PublishRelay<UIImage>()
    let imageRelay = PublishRelay<UIImage>()
    let photoButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()
    let error = PublishRelay<ProfileError>()
    let warningRelay = PublishRelay<String>()
    let onPickImage = PublishRelay<String>()

    // MARK: - Private Properties

    private let imageString: String?
    private var avatarImage: UIImage?

    // MARK: - Initializers

    init(imageString: String?) {
        self.imageString = imageString
    }

    // MARK: - Public  Methods

    func configure(_ cell: AvatarTableViewCell) {
        bindCell(cell)
        cell.configure(imageString: imageString, image: avatarImage)
    }

    func setupError(apiError: String) {
        error.accept(.apiError(apiError))
    }

    func bindCell(_ cell: AvatarTableViewCell) {
        cell
            .onLoadImage
            .bind(to: onLoadImage)
            .disposed(by: cell.bag)

        cell
            .onPhotoTap
            .bind(to: photoButtonTap)
            .disposed(by: cell.bag)

        imageRelay
            .do(onNext: { [weak self, weak cell] image in
                self?.avatarImage = image
                cell?.setupError(.none)
            })
            .bind(to: cell.onPickImage)
            .disposed(by: cell.bag)

        warningRelay
            .bind(to: cell.warningText)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }
}
