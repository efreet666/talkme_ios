//
//  PasswordCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift

final class PasswordCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let passwordTextRelay = BehaviorRelay<String?>(value: nil)
    let error = PublishRelay<ProfileError>()
    let warningRelay = PublishRelay<String>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private let isHidden: Bool
    private let isSecureTextEntry: Bool
    private var text: String?

    // MARK: - Initializers

    init(title: String, isHidden: Bool = false, isSecureTextEntry: Bool = true, text: String?) {
        self.title = title
        self.isHidden = isHidden
        self.isSecureTextEntry = isSecureTextEntry
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: PasswordTableViewCell) {
        cell.configure(title: title, isHidden: isHidden, isSecureTextEntry: isSecureTextEntry, text: text)

        cell
            .passwordText
            .do(onNext: { [weak self, weak cell] in
                guard !($0?.isEmpty ?? true) else { return }
                cell?.setupError(ProfileError.none)
                self?.text = $0
            })
            .bind(to: passwordTextRelay)
            .disposed(by: cell.bag)

        warningRelay
            .bind(to: cell.warningText)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }
}
