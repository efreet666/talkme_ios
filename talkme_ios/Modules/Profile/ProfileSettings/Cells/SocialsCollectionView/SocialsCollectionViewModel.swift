//
//  SocialsCollectionViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 10.03.2021.
//

import RxCocoa
import RxSwift

final class SocialsCollectionViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public Properties

    let socialsModel: SocialMedia
    let resp: ContactsResponse?
    let bag = DisposeBag()

    var isActive: Bool {
        switch socialsModel {
        case .vk:
            return !(resp?.vk?.isEmpty ?? true)
        case .telegram:
            return !(resp?.telegram?.isEmpty ?? true)
        case .instagram:
            return !(resp?.instagram?.isEmpty ?? true)
        case .facebook:
            return !(resp?.facebook?.isEmpty ?? true)
        }
    }

    // MARK: - Initializers

    init(categoryModel: SocialMedia, resp: ContactsResponse? = nil) {
        self.socialsModel = categoryModel
        self.resp = resp
    }

    // MARK: - Public Methods

    func configure(_ cell: SocialsCollectionCell) {
        cell.configure(socialsModel, isEnabled: isActive)
    }
}
