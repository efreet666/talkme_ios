//
//  SocialsCollectionView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 10.03.2021.
//

import RxCocoa
import RxSwift

final class SocialsCollectionCell: UICollectionViewCell {

    // MARK: - Public Properties

    var bag = DisposeBag()

    // MARK: - Private Properties

    private lazy var socialsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private let statusOfSocialsImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        return img
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ type: SocialMedia, isEnabled: Bool) {
        socialsImageView.image = type.icon
        socialsImageView.alpha = isEnabled ? 1 : 0.5
        statusOfSocialsImage.image = UIImage(named: isEnabled ? "redWhiteCross" : "blueWhitePlus")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Private Methods

    private func initialSetup() {
        contentView.addSubviews(socialsImageView, statusOfSocialsImage)
        socialsImageView.snp.makeConstraints { make in
            make.height.width.equalTo(UIScreen.isSE ? 35 : 45)
            make.leading.trailing.top.equalToSuperview()
        }
        statusOfSocialsImage.snp.makeConstraints { make in
            make.trailing.equalTo(socialsImageView)
            make.bottom.equalTo(socialsImageView).inset(-5)
            make.size.equalTo(UIScreen.isSE ? 15 : 19)
        }
    }
}
