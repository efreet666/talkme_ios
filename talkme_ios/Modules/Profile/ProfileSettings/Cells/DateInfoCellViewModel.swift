//
//  DateInfoCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 11.01.2021.
//

import RxCocoa
import RxSwift

final class DateInfoCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let dateRelay = BehaviorRelay<String?>(value: nil)
    let tapRelay = PublishRelay<Void>()
    let error = PublishRelay<ProfileError>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private var text: String?

    // MARK: - Initializers

    init(title: String, text: String?) {
        self.title = title
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: DateInfoTableViewCell) {
        cell.configure(title: title, text: text)

        cell
            .onButtonTap
            .bind(to: tapRelay)
            .disposed(by: cell.bag)

        cell
            .dateText
            .do(onNext: { [weak self, weak cell] in
                guard !($0?.isEmpty ?? true) else { return }
                cell?.setupError(ProfileError.none)
                self?.text = $0
            })
            .bind(to: dateRelay)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }
}
