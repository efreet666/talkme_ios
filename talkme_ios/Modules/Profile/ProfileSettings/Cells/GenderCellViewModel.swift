//
//  GenderCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift

final class GenderCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let genderRelay = BehaviorRelay<Gender?>(value: .none)
    let bag = DisposeBag()

    // MARK: - Private Properties

    private var gender: Gender?

    // MARK: - Initializers

    init(gender: Gender?) {
        self.gender = gender
        genderRelay.accept(gender)
    }

    // MARK: - Public Methods

    func configure(_ cell: GenderTableViewCell) {
        cell.configure(gender: gender)

        cell
            .onButtonTap
            .do(onNext: { [weak self] gender in
                self?.gender = gender
            })
            .bind(to: genderRelay)
            .disposed(by: cell.bag)
    }
}
