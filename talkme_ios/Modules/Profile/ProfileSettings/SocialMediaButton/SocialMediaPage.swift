//
//  SocialMediaPage.swift
//
//
//  Created by Майя Калицева on 12.01.2021.
//

import UIKit

enum SocialMedia: String {

    case vk
    case instagram
    case facebook
    case telegram

    var icon: UIImage? {
        switch self {
        case .vk:
            return UIImage(named: "vk")
        case .instagram:
            return UIImage(named: "instagram")
        case .facebook:
            return UIImage(named: "facebook")
        case .telegram:
            return UIImage(named: "telegram")
        }
    }

    var textFieldMask: String {
        switch self {
        case .vk:
            return "https://vk.com/"
        case .instagram:
            return "https://www.instagram.com/"
        case .facebook:
            return "https://www.facebook.com/"
        case .telegram:
            return "https://t.me/"
        }
    }

    var textFieldPlaceholder: NSAttributedString {
        switch self {
        case .vk:
            return NSAttributedString(string: "id000000000")
        case .instagram:
            return NSAttributedString(string: "nickname")
        case .facebook:
            return NSAttributedString(string: "profile_id")
        case .telegram:
            return NSAttributedString(string: "nickname")
        }
    }

    var title: String {
        switch self {
        case .telegram:
            return "user_social_media_popup_enter_your_link".localizeWithArgument("Telegram")
        case .vk:
            return "user_social_media_popup_enter_your_link".localizeWithArgument("VK")
        case .facebook:
            return "user_social_media_popup_enter_your_link".localizeWithArgument("Facebook")
        case .instagram:
            return "user_social_media_popup_enter_your_link".localizeWithArgument("Instagram")
        }
    }

    var urlSubstring: String {
        switch self {
        case .telegram:
            return "https://t.me"
        case .facebook:
            return "https://www.facebook.com"
        case .instagram:
            return "https://www.instagram.com"
        case .vk:
            return "https://vk.com"
        }
    }
}

extension SocialMedia {

    var errorText: String {
        switch self {
        case .instagram:
            return "user_social_media_error".localizeWithArgument("https://www.instagram.com/username")
        case .facebook:
            return "user_social_media_error".localizeWithArgument("https://www.facebook.com/username")
        case .telegram:
            return "user_social_media_error".localizeWithArgument("https://t.me/username")
        case .vk:
            return "user_social_media_error".localizeWithArgument("https://vk.com/username")
        }
    }
}
