//
//  SocialMediaButton.swift
//  talkme_ios
//
//  Created by Майя Калицева on 12.01.2021.
//

import UIKit

final class SocialMediaButton: UIButton {

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    convenience init(type: SocialMedia) {
        self.init(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        setImage(type.icon, for: .normal)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        layer.cornerRadius = 20
    }
}
