//
//  ProfileSettingViewController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.01.2021.
//

import RxCocoa
import RxSwift
import GrowingTextView

final class ProfileSettingViewController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = true
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        tbv.registerCells(withModels:
                            AvatarCellViewModel.self,
                          UserInfoCellViewModel.self,
                          GenderCellViewModel.self,
                          NumberCellViewModel.self,
                          PasswordCellViewModel.self,
                          ShowButtonCellViewModel.self,
                          AboutmeCellViewModel.self,
                          DateInfoCellViewModel.self,
                          SocialMediaViewModel.self,
                          CountryCityCellViewModel.self)
        return tbv
    }()

    private let imagePicker = UIImagePickerController()
    private let viewModel: ProfileSettingViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: ProfileSettingViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.white
        setupTableView()
        bindVM()
        bindKeyboard()
        bindGrow()
        imagePicker.delegate = self
        viewModel.setupItems()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideCustomTabBarLiveButton(true)
        setupNavigation()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
    }

    // MARK: - Private Methods

    private func bindGrow() {
        viewModel.height
            .bind { [weak self] _ in
                self?.updateTableOnTextEnter()
            }
            .disposed(by: bag)
    }

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: viewModel.bag)

        viewModel.onButtontTapped
            .bind { [weak self] _ in
                self?.showImagePickerAlert()
            }
            .disposed(by: viewModel.bag)

        tableView.rx
            .modelSelected(AnyTableViewCellModelProtocol.self)
            .bind { [weak self] item in
                guard let cellViewModel = item as? CountryCityCellViewModel else { return }
                self?.viewModel.onCountryTap(geoType: cellViewModel.geoType.value)
            }
            .disposed(by: bag)

        viewModel.reloadSocialCell
            .bind { [weak self] indexPath in
                self?.tableView.reloadRows(at: [indexPath], with: .none)
            }
            .disposed(by: viewModel.bag)

        viewModel.error
            .bind { [weak self ] value in
                self?.tableView.scrollToRow(at: value, at: .top, animated: true)
            }
            .disposed(by: viewModel.bag)
    }

    private func setupTableView() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func updateTableOnTextEnter() {
        UIView.performWithoutAnimation {
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }

    private func bindKeyboard() {
        RxKeyboard
            .instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                guard let self = self else { return }
                self.tableView.snp.updateConstraints { make in
                    make.bottom.equalToSuperview().inset(keyboardVisibleHeight)
                }
                UIView.animate(withDuration: 0) {
                    self.view.layoutIfNeeded()
                }
            })
            .disposed(by: bag)
    }

    private func showImagePickerAlert() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "profile_setting_photo_library".localized, style: .default
        ) { [weak self] _ in
            guard let self = self else { return }
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true)
        })
        alert.addAction(UIAlertAction(title: "profile_setting_camera".localized, style: .default
        ) { [weak self] _ in
            guard let self = self else { return }
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true)
        })
        alert.addAction(UIAlertAction(title: "profile_setting_cancel".localized, style: .cancel))

        self.present(alert, animated: true)
    }

    private func setupNavigation() {
        customNavigationController?.style = .plainWhite(title: "profile_setting_profile".localized)
    }
}

extension ProfileSettingViewController: UIImagePickerControllerDelegate,
                                        UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let imagePicked = info[.originalImage] as? UIImage {
            viewModel.onPickImage.accept(imagePicked)
        }
        dismiss(animated: true, completion: nil)
    }
}
