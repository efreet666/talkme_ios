//
//  PushNotificationTableCell.swift
//  talkme_ios
//
//  Created by Yura Fomin on 25.03.2021.
//

import RxSwift

final class PushNotificationTableCell: UITableViewCell {

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        label.numberOfLines = 0
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let uiSwitch = UISwitch()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ text: String) {
        titleLabel.text = text
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, uiSwitch])

        titleLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 18)
            make.leading.equalToSuperview().inset(10)
            make.trailing.lessThanOrEqualTo(uiSwitch.snp.leading).offset(UIScreen.isSE ? -60 : -30)
        }

        uiSwitch.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
        }
    }
}
