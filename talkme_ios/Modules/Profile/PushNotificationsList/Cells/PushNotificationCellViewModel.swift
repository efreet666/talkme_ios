//
//  PushNotificationCellViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 25.03.2021.
//

import RxCocoa

final class PushNotificationCellViewModel: TableViewCellModelProtocol {

    // MARK: - Private Properties

    private let title: String

    // MARK: - Initializers

    init(title: String) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: PushNotificationTableCell) {
        cell.configure(title)
    }
}
