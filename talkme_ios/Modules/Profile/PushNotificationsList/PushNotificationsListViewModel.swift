//
//  PushNotificationsListViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 25.03.2021.
//

import RxCocoa
import RxSwift

final class PushNotificationsListViewModel {

    // MARK: - Public Properties

    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>.just([
        pushNotificationCellViewModel,
        pushNotificationCellViewModel2,
        pushNotificationCellViewModel3,
        pushNotificationCellViewModel4
    ])
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let pushNotificationCellViewModel = PushNotificationCellViewModel(
        title: "Уведомление за час до начала эфиров")
    private let pushNotificationCellViewModel2 = PushNotificationCellViewModel(
        title: "Уведомление за час до начала эфиров")
    private let pushNotificationCellViewModel3 = PushNotificationCellViewModel(
        title: "Уведомление за час до начала эфиров")
    private let pushNotificationCellViewModel4 = PushNotificationCellViewModel(
        title: "Уведомление за час до начала эфиров")
}
