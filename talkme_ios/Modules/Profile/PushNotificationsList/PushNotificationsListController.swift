//
//  PushNotificationsListController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 25.03.2021.
//

import RxCocoa
import RxSwift

final class PushNotificationsListController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.delaysContentTouches = false
        tbv.separatorStyle = .singleLine
        tbv.separatorColor = TalkmeColors.separatorView
        tbv.separatorInset = .zero
        tbv.registerCells(withModels: PushNotificationCellViewModel.self)
        return tbv
    }()

    private let viewModel: PushNotificationsListViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: PushNotificationsListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.mainAccountBackground
        setupTableView()
        bindVM()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .items
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupTableView() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigation() {
        customNavigationController?.style = .plainWhiteWithQR(
            title: "profile_setting_notifications".localized)
    }
}
