//
//  UnsubscribeTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 14.01.2021.
//

import RxSwift
import RxCocoa

final class UnsubscribeTableCell: UITableViewCell {

    // MARK: - Public properties

    let onAddOrRemoveContactTapped = PublishRelay<Void>()
    lazy var onCreateChatTapped = messageButton.tap
    var bag = DisposeBag()

    // MARK: - Private properties

    private let messageButton = AccountUnsubscribeButton(buttonType: .message)
    private var teachersButton = AccountUnsubscribeButton(buttonType: .add)

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        cellStyle()
        messageButton.isHidden = false
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func setUserInfo(isTeacher: Bool, isContact: Bool?) {
        teachersButton.isHidden = !isTeacher
        teachersButton.configure(buttonType: (isContact ?? false) ? .unsubscribe : .add)

        teachersButton.rx
            .tapGesture()
            .when(.recognized)
            .map { _ in }
            .bind(to: onAddOrRemoveContactTapped)
            .disposed(by: bag)
    }

    // MARK: - Private methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubviews([messageButton, teachersButton])

        messageButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 15 : 21)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 15 : 21)
            make.height.equalTo(39)
            make.trailing.equalTo(contentView.snp.centerX).offset(-5)
            make.leading.equalToSuperview().inset(10)
        }

        teachersButton.snp.makeConstraints { make in
            make.height.bottom.top.equalTo(messageButton)
            make.leading.equalTo(contentView.snp.centerX).offset(5)
            make.trailing.equalToSuperview().inset(10)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
