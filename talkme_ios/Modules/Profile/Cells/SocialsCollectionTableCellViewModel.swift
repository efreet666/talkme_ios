//
//  SocialsCollectionTableCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 20.03.2021.
//

import RxSwift
import RxCocoa

final class SocialsCollectionTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let bag = DisposeBag()
    let onSocialTapped = PublishRelay<(SocialMedia, Bool)>()

    // MARK: - Private properties

    private let dataModel = SocialsCollectionModel()
    private var dataItems: [AnyCollectionViewCellModelProtocol] = []
    private let contacts: ContactsResponse?

    // MARK: Init

    init(contacts: ContactsResponse? = nil) {
        self.contacts = contacts
        let items: [AnyCollectionViewCellModelProtocol] = dataModel.data.map {
            AccountSocialsCollectionCellViewModel(categoryModel: $0, resp: contacts)
        }
        dataItems = items
    }

    // MARK: Public Methods

    func configure(_ cell: SocialsCollectionTableCell) {
        cell.setMainItems(dataItems: dataItems)
        cell
            .onSocialTap
            .bind(to: onSocialTapped)
            .disposed(by: cell.bag)
    }
}
