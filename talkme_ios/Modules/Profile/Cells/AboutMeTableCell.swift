//
//  AboutMeTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 26.12.2020.
//

import RxSwift

final class AboutMeTableCell: UITableViewCell {

    enum Constants {
        static let aboutMeFont: CGFloat = UIScreen.isSE ? 12 : 14
    }

    // MARK: - Private properties

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Обо мне"
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let aboutMeLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = .montserratFontRegular(ofSize: UIScreen.isSE ? 14 : 15)
        lbl.textColor = TalkmeColors.blackLabels
        lbl.numberOfLines = 20
        return lbl
    }()

    private let containerView: UIView = {
        let cv = UIView()
        cv.backgroundColor = TalkmeColors.white
        cv.layer.cornerRadius = 11
        return cv
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func setBio(bio: String?) {
        if bio?.isEmpty == false {
            aboutMeLabel.text = bio
        }
    }

    // MARK: - Private Methods

    private func setupLayout() {
        containerView.addSubview(aboutMeLabel)
        contentView.addSubviews([titleLabel, containerView])

        containerView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).inset(-10)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
            make.height.greaterThanOrEqualTo(aboutMeLabel)
        }

        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(31)
            make.top.equalToSuperview()
            make.leading.equalToSuperview().offset(10)
        }

        aboutMeLabel.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.top).inset(Constants.aboutMeFont)
            make.bottom.equalToSuperview().inset(Constants.aboutMeFont)
            make.leading.equalToSuperview().offset(10)
            make.trailing.equalToSuperview().offset(-10)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
