//
//  MainInformationCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxCocoa
import RxSwift

final class MainInformationTableCellViewModel: TableViewCellModelProtocol {

    let flow = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: Private Properties

    private let lessons: AchievementsResponse
    private let profileInfo: ProfileResponse

    // MARK: Public methods

    init(profileInfo: ProfileResponse, lessons: AchievementsResponse) {
        self.profileInfo = profileInfo
        self.lessons = lessons
    }

    func configure(_ cell: MainInformationTableCell) {
        cell.setUserInfo(
            firstName: profileInfo.firstName,
            lastName: profileInfo.lastName,
            idDescription: profileInfo.username,
            classNumber: profileInfo.numberClass,
            rating: profileInfo.rating,
            avatar: profileInfo.avatarUrl,
            isSpecialist: profileInfo.isSpecialist,
            lessonsPassed: lessons.lessonPassed,
            profileId: profileInfo.id)
        cell.flow
            .bind(to: flow)
            .disposed(by: cell.bag)
    }
}
