//
//  SubscribersAndSubscriptionsTableCell.swift
//  talkme_ios
//
//  Created by VladimirCH on 30.03.2022.
//

import RxSwift
import RxCocoa

final class SubscribersSubscriptionsCell: UITableViewCell {

    // MARK: - Private properties

    private var subscribers = SubscriptionItemView(
        titleText: "subscribers".localized,
        countText: "0"
    )
    private var subscriptions = SubscriptionItemView(
        titleText: "subscriptions".localized,
        countText: "0"
    )

    private let stackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .center
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        stack.spacing = UIScreen.isSE ? 10 : 12
        return stack
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        setupAppearance()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func configure(subscribersCount: Int, subscriptionsCount: Int) {
        subscribers.configure(text: Formatters.checkingForMillionAndThousand(count: subscribersCount))
        subscriptions.configure(text: Formatters.checkingForMillionAndThousand(count: subscriptionsCount))

    }

    // MARK: - Private Method

    private func setupAppearance() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(stackView)
        stackView.addArrangedSubviews([subscribers, subscriptions])

        stackView.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 11)
            make.height.equalTo(UIScreen.isSE ? 65 :70)
            make.top.equalToSuperview().offset(24)
            make.bottom.equalToSuperview()
        }
    }
}
