//
//  LessonsCollectionTableCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

import RxCocoa
import RxSwift

final class LessonsCollectionCellViewModel: TableViewCellModelProtocol {

    // MARK: - Private properties

    let onLessonDetail = PublishRelay<LiveStream>()
    private let dataModel: [LiveStream]
    private var dataItems: [AnyCollectionViewCellModelProtocol]?

    // MARK: Public methods

    init(dataItems: [LiveStream]) {
        self.dataModel = dataItems
        setDataModel(lessons: dataModel)
    }

    // MARK: - Public properties

    let bag = DisposeBag()
    let flow = PublishRelay<LiveStream>()

    // MARK: Public Methods

    func setDataModel(lessons: [LiveStream]) {
        let items: [AnyCollectionViewCellModelProtocol] = lessons.map {
            let model = LessonsCollectionTableCellViewModel(lessonsModel: $0)
            return model
        }
        dataItems = items
    }

    func configure(_ cell: LessonsCollectionViewCell) {
        cell.setMainItems(dataItems: dataItems)
        cell.modelSelected
            .map({ $0.lessonsModel })
            .bind(to: onLessonDetail)
            .disposed(by: cell.bag)
    }
}
