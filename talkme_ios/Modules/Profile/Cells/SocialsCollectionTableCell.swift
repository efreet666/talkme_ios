//
//  SocialsCollectionTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 20.03.2021.
//

import RxCocoa
import RxSwift

final class SocialsCollectionTableCell: UITableViewCell {

    // MARK: Pubic Properties

    private(set) var bag = DisposeBag()
    lazy var onSocialTap = socialsCollectionView.rx.modelSelected(AccountSocialsCollectionCellViewModel.self).map { ($0.socialsModel, $0.isActive) }

    // MARK: Private Properties

    private let socialsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = UIScreen.isSE ? 13 : 28
        layout.itemSize = CGSize(width: UIScreen.isSE ? 35 : 41, height: UIScreen.isSE ? 35 : 41)
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear
        cvc.showsHorizontalScrollIndicator = false
        cvc.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        cvc.registerCells(withModels: AccountSocialsCollectionCellViewModel.self)
        return cvc
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]?) {
        guard let mainItems = dataItems else { return }
        Observable.just(mainItems)
            .bind(to: socialsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    // MARK: Private Methods

    private func setupLayout() {
        contentView.addSubviews(socialsCollectionView)
        socialsCollectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 18 : 21)
            make.height.equalTo(UIScreen.isSE ? 35 : 41)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
