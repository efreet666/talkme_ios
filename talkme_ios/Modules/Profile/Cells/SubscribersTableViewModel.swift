//
//  SubscribersTableViewModel.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 23.03.2022.
//

import Foundation

class SubscribersTableViewModel: TableViewCellModelProtocol {

    // MARK: Private Properties

    let model: PublicProfileResponse?

    init(model: PublicProfileResponse) {
        self.model = model
    }

    func configure(_ cell: SubscribersTableViewCell) {
        guard let model = model else { return }
        cell.configure(model: model)
    }
}
