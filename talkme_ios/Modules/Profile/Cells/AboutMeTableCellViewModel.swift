//
//  AboutMeTableCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 26.12.2020.
//

import RxCocoa

final class AboutMeTableCellViewModel: TableViewCellModelProtocol {

    // MARK: Private Properties

    private var bio: String?

    // MARK: Public methods

    init(bio: String?) {
        self.bio = bio
    }

    func configure(_ cell: AboutMeTableCell) {
        cell.setBio(bio: bio)
    }
}
