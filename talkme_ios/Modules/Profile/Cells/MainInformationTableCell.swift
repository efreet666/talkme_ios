//
//  MainInformationCell.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxSwift
import RxCocoa
import Kingfisher

final class MainInformationTableCell: UITableViewCell {

    enum Constants {
        static let fontStyle = UIFont.montserratBold(ofSize: UIScreen.isSE ? 15 : 17)
    }

    let flow = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: - Private properties

    let firstNameLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.fontStyle
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let lastNameLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.fontStyle
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let lessonsPassedLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.greenLabels
        return label
    }()

    private let idLabel: UILabel = {
        let label = UILabel()
        label.text = "ID"
        label.font = .montserratSemiBold(ofSize: 14)
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let idDescriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        label.textColor = TalkmeColors.blueLabels
        return label
    }()

    private let cupImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "cup")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private var complaintButton: SideActionButtonView = {
        let button = SideActionButtonView(type: .complaintBlack,
                                          size: CGSize(width: UIScreen.isSE ? 26 : 28,
                                                       height: UIScreen.isSE ? 26 : 28))
        button.contentMode = .scaleAspectFit
        return button
    }()

    private let classLabel: ClassNumberLabel = {
        let label = ClassNumberLabel()
        label.configure(type: .mainInfo)
        return label
    }()

    private let avatarRatingView: AvatarRatingView = {
        let view = AvatarRatingView()
        return view
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        cellStyle()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func setUserInfo (
        firstName: String?, lastName: String?,
        idDescription: String?, classNumber: String?,
        rating: Double?, avatar: String?, isSpecialist: Bool?,
        lessonsPassed: Int?, profileId: Int?
    ) {
        firstNameLabel.text = firstName
        lastNameLabel.text = lastName
        idDescriptionLabel.text = idDescription
        classLabel.text = "№ \(classNumber ?? "")"
        avatarRatingView.configure(rating: rating)
        lessonsPassedLabel.text = String(lessonsPassed ?? 0)
        classLabel.isHidden = !(isSpecialist ?? false)
        avatarRatingView.hideRating(!(isSpecialist ?? false))
        cupImageView.isHidden = !(isSpecialist ?? false)
        lessonsPassedLabel.isHidden = !(isSpecialist ?? false)
        if profileId == UserDefaultsHelper.shared.userId {
            complaintButton.isHidden = true
        } else {
            classLabel.isHidden = true
        }
        if let avatar = avatar, let url = URL(string: avatar) {
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil) { [weak self] result in
                switch result {
                case .success(let value):
                    self?.avatarRatingView.setImage(image: value.image)
                case .failure:
                    self?.avatarRatingView.setImage(image: UIImage(named: "noAvatar"))
                }
            }
        } else {
            avatarRatingView.setImage(image: UIImage(named: "noAvatar"))
        }
    }

    // MARK: - Private Method

    private func setupLayout() {
        contentView.addSubviews([
            firstNameLabel,
            lastNameLabel,
            lessonsPassedLabel,
            idLabel,
            idDescriptionLabel,
            cupImageView,
//            classLabel,
            avatarRatingView,
            complaintButton
        ])

        avatarRatingView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.equalToSuperview().offset(10)
            make.bottom.equalToSuperview()
        }

        firstNameLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarRatingView.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-10)
            make.top.equalToSuperview().offset(22)
        }

        lastNameLabel.snp.makeConstraints { make in
            make.leading.equalTo(firstNameLabel.snp.leading)
            make.trailing.equalToSuperview().offset(-10)
            make.top.equalTo(firstNameLabel.snp.bottom).inset(-4)
        }

        idLabel.snp.makeConstraints { make in
            make.leading.equalTo(lastNameLabel.snp.leading)
            make.top.greaterThanOrEqualTo(lastNameLabel.snp.bottom).offset(5)
        }

        idDescriptionLabel.snp.makeConstraints { make in
            make.height.equalTo(idLabel.snp.height)
            make.trailing.lessThanOrEqualToSuperview().offset(-10)
            make.centerY.equalTo(idLabel.snp.centerY)
            make.leading.equalTo(idLabel.snp.trailing).offset(6)
        }

        cupImageView.snp.makeConstraints { make in
            make.top.greaterThanOrEqualTo(idLabel.snp.bottom).offset(5)
            make.leading.equalTo(firstNameLabel)
            make.bottom.equalTo(avatarRatingView.snp.bottom)
        }

        lessonsPassedLabel.snp.makeConstraints { make in
            make.leading.equalTo(cupImageView.snp.trailing).offset(6)
            make.trailing.equalToSuperview().offset(-10)
            make.centerY.equalTo(cupImageView.snp.centerY)
            make.bottom.equalTo(cupImageView)
        }

//        classLabel.snp.makeConstraints { make in
//            make.width.equalTo(UIScreen.isSE ? 84 : 95)
//            make.height.equalTo(30)
//            make.top.equalToSuperview().inset(UIScreen.isSE ? 20 : 24)
//            make.trailing.equalToSuperview().offset(-10)
//        }

        complaintButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 17 : 19)
            make.trailing.equalToSuperview().offset(-11)
            make.height.equalTo(complaintButton.frame.size.height)
            make.width.equalTo(complaintButton.frame.size.width)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func bindUI() {
        complaintButton.rx
            .tapGesture()
            .when(.recognized)
            .map { _ in ()}
            .bind(to: flow)
            .disposed(by: bag)
    }
}
