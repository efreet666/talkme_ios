//
//  SubscribersTableViewCell.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 23.03.2022.
//

import UIKit

final class SubscribersTableViewCell: UITableViewCell {

    private let separatorView = UIView()
    private let subscribersView = SubscribersView(title: "subscribers".localized)
    private let subscriptionsView = SubscribersView(title: "subscriptions".localized)

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupAppearance()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func configure(model: PublicProfileResponse) {
        subscribersView.setInfo(subscribers: "\(model.specialistInfo.subscribersCount ?? 0)")
        subscriptionsView.setInfo(subscribers: "\(model.specialistInfo.subscriptionsCount ?? 0)")
    }

    // MARK: - Private Method

    private func setupLayout() {
        contentView.addSubviews(separatorView, subscribersView, subscriptionsView)

        separatorView.snp.makeConstraints { make in
            make.centerX.top.bottom.equalToSuperview()
            make.height.equalTo(100)
            make.width.equalTo(2)
        }

        subscribersView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(11)
            make.trailing.equalTo(separatorView.snp.leading).inset(-8)
            make.height.equalTo(70)
            make.top.equalToSuperview().inset(24)
        }

        subscriptionsView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(11)
            make.leading.equalTo(separatorView.snp.trailing).inset(-8)
            make.height.equalTo(70)
            make.top.equalToSuperview().inset(24)
        }
    }

    private func setupAppearance() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
