//
//  SubscribersAndSubscriptionsTableCellViewModel.swift
//  talkme_ios
//
//  Created by VladimirCH on 30.03.2022.
//

final class SubscribersSubscriptionsCellViewModel: TableViewCellModelProtocol {

    // MARK: Public properties

    var subscribersCount: Int
    var subscriptionsCount: Int

    init(publicProfileResponse: PublicProfileResponse?) {
        subscribersCount = publicProfileResponse?.specialistInfo.subscribersCount ?? 0
        subscriptionsCount =  publicProfileResponse?.specialistInfo.subscriptionsCount ?? 0
    }

    // MARK: Public Methods

    func configure(_ cell: SubscribersSubscriptionsCell) {
        cell.configure(
            subscribersCount: subscribersCount,
            subscriptionsCount: subscriptionsCount
        )
    }
}
