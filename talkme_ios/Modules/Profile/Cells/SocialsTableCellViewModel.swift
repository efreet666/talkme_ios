//
//  SocialsCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxCocoa
import RxSwift

enum SocialCellType {
    case signIn
    case userProfile
}

final class SocialsTableCellViewModel: TableViewCellModelProtocol {

    // MARK: Public Properties

    let onSocialFlow = PublishRelay<SocialMedia>()
    let buttonTapRelay = PublishRelay<SocialButtonType>()
    var height: CGFloat { UIScreen.isSE ? 44 : 76 }
    let bag = DisposeBag()

    // MARK: Private Properties

    private var completion: ((String, String) -> Void)?
    private var cellType: SocialCellType
    private let contacts: ContactsResponse?
    private let appleService = AppleIDSignInService()
    private let googleService = GoogleSignInService()

    // MARK: Public Methods

    init(contacts: ContactsResponse? = nil, cellType: SocialCellType = .userProfile) {
        self.contacts = contacts
        self.cellType = cellType
    }

    func returnTokenInViewModelGoogleService (completion: ((String, String) -> Void)?) {
        googleService.completion = { [] token, idToken in
            completion?(token, idToken)
        }
    }

    func configure(_ cell: SocialsTableCell) {
        cell.setUserInfo(model: contacts, cellType: cellType)

        cell.vkButtonTap
            .compactMap { .vkontakte }
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)

        cell.facebookButtonTap
            .compactMap { .facebook }
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)

        cell.mailRuButtonTap
            .compactMap { .mailru }
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)

        cell.yandexButtonTap
            .compactMap { .yandex }
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)

        cell.googleButtonTap
            .compactMap { .google }
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)

        cell.appleButtonTap
            .compactMap { .appleID }
            .bind(to: buttonTapRelay)
            .disposed(by: cell.bag)
    }
}
