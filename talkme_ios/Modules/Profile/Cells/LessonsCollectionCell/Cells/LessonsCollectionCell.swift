//
//  LessonsCollecrionCell.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

import RxSwift

final class LessonsCollectionTableCell: UICollectionViewCell {

    // MARK: - Private properties

    private let itemView = LessonItemView()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(lessonsModel: LiveStream) {
        itemView.configure(lessonModel: lessonsModel)
    }

    // MARK: - Private Method

    private func setupLayout() {
        contentView.addSubview(itemView)
        itemView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
