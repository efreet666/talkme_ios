//
//  LessonsCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

import UIKit

final class LessonsCollectionTableCellViewModel: CollectionViewCellModelProtocol {

   // MARK: - Public Properties

    let lessonsModel: LiveStream

    // MARK: - Initializers

    init(lessonsModel: LiveStream) {
        self.lessonsModel = lessonsModel
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonsCollectionTableCell) {
        cell.configure(lessonsModel: lessonsModel)
    }
}
