//
//  LessonsCollectionModel.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

struct LessonCollectionModelCategories {
    let data: [LessonCategoryType] =
    [.all, .live, .nearest, .saved, .talkMeStudio, .mosStream, .education, .entertainment, .fitness, .chatting, .nightlife, .girls, .learning, .cooking,
                                      .kids, .travel, .business, .languages, .music, .yourShow, .games, .other,
     .courses, .masterClass, .sale, .car, .health, .standUp]
}
