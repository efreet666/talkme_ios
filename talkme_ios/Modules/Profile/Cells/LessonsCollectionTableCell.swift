//
//  LessonsCollectionTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

import RxSwift
import RxCocoa

final class LessonsCollectionViewCell: UITableViewCell {

    // MARK: - Public Properties

    let dataItems = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let flow = PublishRelay<ProfileMenuModel>()

    private(set) lazy var modelSelected = lessonsCollectionView.rx.modelSelected(LessonsCollectionTableCellViewModel.self)
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let allLessonsLabel: UILabel = {
        let label = UILabel()
        label.text = "public_profile_all_lessons".localized
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private lazy var lessonsCollectionView: UICollectionView = {
        let clv = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionLayout)
        clv.backgroundColor = .clear
        clv.showsHorizontalScrollIndicator = false
        clv.registerCells(withModels: LessonsCollectionTableCellViewModel.self)
        return clv
    }()

    private lazy var collectionLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.isSE ? 132 : 173, height: UIScreen.isSE ? 210 : 291)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = UIScreen.isSE ? 8 : 11
        return layout
      }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Method

    func setMainItems(dataItems: [AnyCollectionViewCellModelProtocol]?) {
       guard let mainItems = dataItems else { return }
        Observable.just(mainItems)
            .bind(to: lessonsCollectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    // MARK: - Private Method

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([allLessonsLabel, lessonsCollectionView])

        allLessonsLabel.snp.makeConstraints { make in
            make.height.equalTo(31)
            make.top.equalToSuperview().offset(15)
            make.leading.equalToSuperview().offset(10)
            make.trailing.lessThanOrEqualToSuperview().offset(-10)
        }

        lessonsCollectionView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().inset(10)
            make.trailing.equalToSuperview().inset(10)
            make.top.equalTo(allLessonsLabel.snp.bottom).offset(7)
            make.height.equalTo(UIScreen.isSE ? 210 : 291)
        }
    }
}
