//
//  AgeAndLocationCell.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxSwift
import RxCocoa

final class AgeAndLocationTableCell: UITableViewCell {

    enum Constants {
        static let fontStyle = UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
    }

    // MARK: - Private properties

    private let ageLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.fontStyle
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let locationLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.fontStyle
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let genderImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Method

    private func setupLayout() {

        contentView.addSubviews([ageLabel, locationLabel, genderImageView])

        genderImageView.snp.makeConstraints { make in
            make.height.equalTo(13)
            make.top.equalToSuperview().offset(15)
            make.leading.equalTo(ageLabel.snp.trailing).offset(6)
            make.trailing.lessThanOrEqualToSuperview().offset(-10)
        }

        ageLabel.snp.makeConstraints { make in
            make.centerY.equalTo(genderImageView.snp.centerY)
            make.height.equalTo(16)
            make.leading.equalToSuperview().offset(10)
        }

        locationLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(10)
            make.height.equalTo(17)
            make.top.equalTo(ageLabel.snp.bottom).offset(10)
            make.bottom.equalToSuperview().inset(21)
            make.trailing.lessThanOrEqualToSuperview().offset(-15)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    // MARK: - Public Method

    func setUserInfo(location: String?, age: String?, gender: String?) {
        guard let location = location, let age = age else { return }
        locationLabel.text = location
        ageLabel.text = age
        genderImageView.isHidden = gender?.isEmpty ?? true
        guard let gender = gender else { return }
        genderImageView.image = UIImage(named: gender)
    }

}
