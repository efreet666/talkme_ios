//
//  UnsubscribeTableCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 14.01.2021.
//

import RxCocoa
import RxSwift

final class UnsubscribeTableCellViewModel: TableViewCellModelProtocol {

    // MARK: Public Properties

    let onAddOrRemoveContactTapped = PublishRelay<Void>()
    let onCreateChatTapped = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: Private Properties

    private let isTeacher: Bool?
    private let isContact: Bool?

    // MARK: Initialization

    init(isTeacher: Bool?, isContact: Bool?) {
        self.isTeacher = isTeacher
        self.isContact = isContact
    }

    func configure(_ cell: UnsubscribeTableCell) {
        guard let isTeacher = isTeacher else { return }
        cell.setUserInfo(isTeacher: isTeacher, isContact: isContact)

        cell
            .onAddOrRemoveContactTapped
            .bind(to: onAddOrRemoveContactTapped)
            .disposed(by: cell.bag)

        cell
            .onCreateChatTapped
            .bind { [weak self] _ in
                self?.onCreateChatTapped.accept(())
            }
            .disposed(by: cell.bag)
    }
}
