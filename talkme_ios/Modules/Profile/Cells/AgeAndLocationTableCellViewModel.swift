//
//  AgeAndLocationCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxCocoa

final class AgeAndLocationTableCellViewModel: TableViewCellModelProtocol {

    // MARK: Private Properties

    private var location: String?
    private var age: String?
    private var gender: String?

    // MARK: Public Methods

    init(profileInfo: ProfileResponse, publicInfoAge: Int?) {
        location = "\(profileInfo.country ?? "Страна" ), \(profileInfo.city ?? "город")"
        gender = profileInfo.gender
        guard let publicInfoAge = publicInfoAge else {
            getAgeFromBirthday(birtday: profileInfo.birthday)
            return
        }
        let dateComponents = DateComponents(year: publicInfoAge)
        age = Formatters.dateComponentsFormatter.string(from: dateComponents)
    }

    func configure(_ cell: AgeAndLocationTableCell) {
        cell.setUserInfo(location: location, age: age, gender: gender)
    }

    // MARK: Private Methods

    private func getAgeFromBirthday(birtday: String?) {
        guard let date = Formatters.dateFormatterDefault.date(from: birtday ?? ""),
              let age = Formatters.dateComponentsFormatter.string(from: date, to: Date()) else { return self.age =  "0 лет" }
        self.age = age
    }
}
