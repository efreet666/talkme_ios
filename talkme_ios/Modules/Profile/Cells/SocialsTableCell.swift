//
//  SocialsCell.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxSwift
import RxCocoa

final class SocialsTableCell: UITableViewCell {

    private enum Constants {
        static let cellWidth = UIScreen.main.bounds.width
    }

    // MARK: - Public properties

    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let buttonsStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .equalSpacing
        sv.spacing = 13
        return sv
    }()

    private let vkButton = SocialsButton(type: .vkontakte)
    private let instagramButton = SocialsButton(type: .instagram)
    private let facebookButton = SocialsButton(type: .facebook)
    private let tiktokButton = SocialsButton(type: .tiktok)
    private let telegramButton = SocialsButton(type: .telegram)
    private let okButton = SocialsButton(type: .odnoclassniki)
    private let mailRuButton = SocialsButton(type: .mailru)
    private let yandexButton = SocialsButton(type: .yandex)
    private let googleButton = SocialsButton(type: .google)
    private let appleButton = SocialsButton(type: .appleID)

    private(set) lazy var vkButtonTap = vkButton.rx.tap
    private(set) lazy var facebookButtonTap = facebookButton.rx.tap
    private(set) lazy var mailRuButtonTap = mailRuButton.rx.tap
    private(set) lazy var yandexButtonTap = yandexButton.rx.tap
    private(set) lazy var googleButtonTap = googleButton.rx.tap
    private(set) lazy var appleButtonTap = appleButton.rx.tap

    private var cellType: SocialCellType?

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func setUserInfo(model: ContactsResponse?, cellType: SocialCellType = .userProfile) {
        self.cellType = cellType
        switch cellType {
        case .userProfile:
            vkButton.alpha = !(model?.vk?.isEmpty ?? true) ? 1 : 0.5
            instagramButton.alpha = !(model?.instagram?.isEmpty ?? true) ? 1 : 0.5
            facebookButton.alpha = !(model?.facebook?.isEmpty ?? true) ? 1: 0.5
            telegramButton.alpha = !(model?.telegram?.isEmpty ?? true) ? 1: 0.5
        case .signIn:
            break
        }
        setupLayout(cellType: cellType)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Private Method

    private func setupLayout(cellType: SocialCellType) {
        var buttonsArray = [UIButton]()
        switch cellType {
        case .userProfile:
            buttonsArray = [vkButton, instagramButton, facebookButton, telegramButton]
        case .signIn:
            buttonsArray = [vkButton, yandexButton, mailRuButton, googleButton, facebookButton, appleButton]
        }
        contentView.addSubview(buttonsStackView)
        buttonsStackView.addArrangedSubviews(buttonsArray)

        buttonsStackView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(Constants.cellWidth / 17)
            make.trailing.equalToSuperview().offset(-Constants.cellWidth / 17)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(UIScreen.isSE ? -4 : -20)
            make.height.equalTo(UIScreen.isSE ? 35 : 46)
        }

        buttonsArray.forEach { button in
            button.snp.makeConstraints { make in
                make.width.equalTo(button.snp.height)
            }
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
