//
//  AlertManager.swift
//  talkme_ios
//
//  Created by Yura Fomin on 05.02.2021.
//

import UIKit

enum AlertManager {

    static func showDeleteLessonAlert(from controller: UIViewController, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "popup_delete_lesson_yes".localized, style: .default) { _ in
            completion()
        }
        let declineAction = UIAlertAction(title: "popup_delete_lesson_no".localized, style: .cancel, handler: .none)
        let titleFont = [NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 15)]
        let messageFont = [NSAttributedString.Key.font: UIFont.montserratFontRegular(ofSize: 13)]
        let titleAlert = NSMutableAttributedString(string: "popup_delete_lesson_popup_title".localized, attributes: titleFont)
        let messageAlert = NSMutableAttributedString(string: "popup_delete_lesson_popup_main".localized, attributes: messageFont)

        alertController.setValue(titleAlert, forKey: "attributedTitle")
        alertController.setValue(messageAlert, forKey: "attributedMessage")

        alertController.addAction(declineAction)
        alertController.addAction(confirmAction)
        alertController.preferredAction = confirmAction

        controller.present(alertController, animated: true, completion: nil)
    }

    static func showUnsubscribeFromTeacherAlert(from controller: UIViewController, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "popup_delete_lesson_yes".localized, style: .default) { _ in
            completion()
        }
        let declineAction = UIAlertAction(title: "profile_setting_cancel".localized, style: .cancel, handler: .none)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.2
        paragraphStyle.alignment = .center
        let title = NSAttributedString(
            string: "alert_delete_teacher_from_list".localized,
            attributes:
                [NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 15),
                 NSAttributedString.Key.paragraphStyle: paragraphStyle
                ])

        alertController.setValue(title, forKey: "attributedTitle")
        alertController.addAction(declineAction)
        alertController.addAction(confirmAction)
        alertController.preferredAction = confirmAction

        controller.present(alertController, animated: true, completion: nil)
    }
}
