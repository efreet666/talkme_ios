//
//  MyStreamsViewModel.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 05.01.2021.
//

import RxCocoa
import RxSwift
import RxDataSources

fileprivate extension Consts {
    static let numberOfLiveStreamsInPage = 50
}

protocol MyStreamsProtocol: AnyObject {
    var dataItems: BehaviorRelay<[SectionModel<String, MyFutureStreamsCellViewModel>]> { get set }
    var bag: DisposeBag { get }
    var navigationTitle: String { get }
    var todayItems: [SectionModel<String, MyFutureStreamsCellViewModel>] { get }
    var allItems: [SectionModel<String, MyFutureStreamsCellViewModel>] { get set }
    var flow: PublishRelay<MyStreamsFlow> { get }
    var onDeleteLesson: PublishRelay<Int> { get }

    func сellViewModel(_ lesson: StreamInfoViewType, id: Int) -> MyFutureStreamsCellViewModel
    func fetchStreams()
    func onDeleteLessonTap(id: Int)
}

enum MyStreamsFlow {
    case onWatch(lessonId: Int, numberOfLiveStreamsInPage: Int)
    case edit(id: Int)
    case lessonInfo(id: Int)
}

final class MyStreamsViewModel: MyStreamsProtocol {

    // MARK: - Public properties

    var dataItems = BehaviorRelay<[SectionModel<String, MyFutureStreamsCellViewModel>]>(value: [])
    let navigationTitle = NavigationTitle.myStreams.title

    let bag = DisposeBag()
    var todayItems: [SectionModel<String, MyFutureStreamsCellViewModel>] = []
    var allItems: [SectionModel<String, MyFutureStreamsCellViewModel>] = []
    var flow = PublishRelay<MyStreamsFlow>()
    var onDeleteLesson = PublishRelay<Int>()

    // MARK: - Private properties

    private let service: LessonsServiceProtocol
    private var lessonsArray: [Lesson] = []
    private var myBroadcasts: MyBroadcastsResponse?

    // MARK: - Initializers

    init(service: LessonsServiceProtocol) {
        self.service = service
    }

    // MARK: - Public Methods

    func onDeleteLessonTap(id: Int) {
        service
            .cancelLesson(id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.fetchStreams()
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func fetchStreams() {
        Single.zip(service.live(pageNumber: 1, lessonsInPage: 100), service.myBroadcasts(page: 100))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(( _, let lessons)):
                    self.myBroadcasts = lessons
                    self.removeAllStreams()
                    self.setupLessons(from: lessons)
                    guard let lesson = lessons.lessons,
                          let lessonsSoon = lessons.lessonsSoon else { return }
                    self.lessonsArray.append(contentsOf: lessonsSoon)
                    self.lessonsArray.append(contentsOf: lesson)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func сellViewModel(_ lesson: StreamInfoViewType, id: Int) -> MyFutureStreamsCellViewModel {
        let cellViewModel = MyFutureStreamsCellViewModel(myStream: lesson, id: id)
        cellViewModel
            .onEditBtnRelay
            .map { .edit(id: id)}
            .bind(to: flow)
            .disposed(by: cellViewModel.bag)

        cellViewModel
            .onDeleteBtnRelay
            .map { id }
            .bind(to: onDeleteLesson)
            .disposed(by: cellViewModel.bag)

        cellViewModel
            .onStreamBecomeLive
            .bind { [weak self] _ in
                self?.fetchStreams()
            }
            .disposed(by: cellViewModel.bag)

        cellViewModel
            .onWatchButtonTap
            .map { _ in
                    .onWatch(lessonId: id, numberOfLiveStreamsInPage: Consts.numberOfLiveStreamsInPage)
            }
            .bind(to: flow)
            .disposed(by: cellViewModel.bag)

        cellViewModel
            .onWatchLessonInfo
            .map { .lessonInfo(id: id) }
            .bind(to: flow)
            .disposed(by: cellViewModel.bag)

        return cellViewModel
    }

    // MARK: - Private Methods

    private func removeAllStreams() {
        allItems.removeAll()
        todayItems.removeAll()
    }

    private func setupLessons(from lessons: MyBroadcastsResponse) {
        self.setupLessonSoonAllStreams(from: lessons)
        self.setupLessonAllStreams(from: lessons)
        self.setupLessonSoonTodayStreams(from: lessons)
        self.setupLessonTodayStreams(from: lessons)
        self.dataItems.accept(allItems)
    }

    private func setupLessonAllStreams(from lessons: MyBroadcastsResponse) {
        guard let lessons = lessons.lessons else { return }
        let items = lessons
            .map { сellViewModel(.myStreams($0), id: $0.id) }

        allItems.append(.init(model: "", items: items))
    }

    private func setupLessonsSoonStreams(_ lesson: Lesson) -> MyFutureStreamsCellViewModel {
        return сellViewModel(
            .myNearestStreams(
                name: lesson.name,
                date: (lesson.date ?? 0) - Date.diffrenceBetweenRealTime,
                subscribers: lesson.subscribers,
                avatar: lesson.tileImage),
            id: lesson.id)
    }

    private func setupLessonsLiveStreams(_ lesson: Lesson) -> MyFutureStreamsCellViewModel {
        return сellViewModel(
            .myLiveStreams(
                name: lesson.name,
                date: (lesson.date ?? 0) - Date.diffrenceBetweenRealTime,
                subscribers: lesson.subscribers,
                avatar: lesson.tileImage),
            id: lesson.id)
    }

    private func setupLessonSoonAllStreams(from lessons: MyBroadcastsResponse) {
        guard let lessonLive = lessons.lessonsSoon else { return }
        let nowTime = Date().timeIntervalSince1970 + Date.diffrenceBetweenRealTime
        var lessonsSoon: [MyFutureStreamsCellViewModel] = []
        var lessonsLive: [MyFutureStreamsCellViewModel] = []
        lessonLive.forEach {
            guard let item = $0.date else { return }
            if item >= nowTime {
                lessonsSoon.append(setupLessonsSoonStreams($0))
            } else {
                lessonsLive.append(setupLessonsLiveStreams($0))
            }
        }
        if !lessonsSoon.isEmpty { allItems.append(.init(model: "", items: lessonsSoon)) }
        if !lessonsLive.isEmpty { allItems.append(.init(model: "", items: lessonsLive)) }
    }

    private func setupLessonTodayStreams(from lessons: MyBroadcastsResponse) {
        guard let lessons = lessons.lessons else { return }
        let filteredLessonDate = lessons.filter { item in
            guard let responseDate = item.date else { return false }
            let calendar = Calendar.current
            let date = Date(timeIntervalSince1970: responseDate )
            return calendar.isDateInToday(date)
        }
        let itemsToday = filteredLessonDate
            .map { сellViewModel( .myStreams($0), id: $0.id) }
        todayItems.append(.init(model: "", items: itemsToday))
    }

    private func setupLessonSoonTodayStreams(from lessons: MyBroadcastsResponse) {
        guard let lessonSoon = lessons.lessonsSoon else { return }
        let filteredLessonSoon = lessonSoon.filter { item in
            guard let responseDate = item.date else { return false }
            let calendar = Calendar.current
            let date = Date(timeIntervalSince1970: responseDate )
            return calendar.isDateInToday(date)
        }

        let nowTime = Date().timeIntervalSince1970
        var lessonsSoon: [MyFutureStreamsCellViewModel] = []
        var lessonsLive: [MyFutureStreamsCellViewModel] = []

        filteredLessonSoon.forEach {
            guard let item = $0.date else { return }
            if item >= nowTime {
                lessonsSoon.append(setupLessonsSoonStreams($0))
            } else {
                lessonsLive.append(setupLessonsLiveStreams($0))
            }
        }
        if !lessonsSoon.isEmpty { todayItems.append(.init(model: "", items: lessonsSoon)) }
        if !lessonsLive.isEmpty { todayItems.append(.init(model: "", items: lessonsLive)) }
    }
}
