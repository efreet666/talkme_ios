//
//  StreamsViewController.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 05.01.2021.
//

import RxCocoa
import RxSwift
import SnapKit
import RxDataSources

enum NavigationTitle {
    case myStreams
    case mySybscriptions
    case country
    case city

    var title: String {
        switch self {
        case .myStreams:
            return "talkme_stream_list_my_streams".localized
        case .mySybscriptions:
            return "talkme_stream_all_my_subscriptions".localized
        case .country:
            return "profile_setting_selected_country".localized
        case .city:
            return "profile_setting_selected_city".localized
        }
    }
}

final class StreamsViewController: UIViewController {

    // MARK: - Private properties

    private let bag = DisposeBag()
    private let viewModel: MyStreamsProtocol

    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.delaysContentTouches = false
        tableView.registerCells(withModels: MyFutureStreamsCellViewModel.self)
        return tableView
    }()

    private let scanQRButton: UIBarButtonItem = {
        let scanQRButton = UIButton(type: .custom)
        let navigationButton = UIBarButtonItem(customView: scanQRButton)
        scanQRButton.setImage(UIImage(named: "streamQR"), for: .normal)
        return navigationButton
    }()

    private let items = BehaviorRelay<[SectionModel<String, MyFutureStreamsCellViewModel>]>(value: [])
    private let tableHeaderView = StreamTableViewHeader()

    // MARK: - Init

    init(viewModel: MyStreamsProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        bindUI()
        setupUI()
        setupLayout()
        bindAppState()
        viewModel.fetchStreams()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    // MARK: - Private Methods

    private func showAlert(id: Int) {
        AlertManager.showDeleteLessonAlert(from: self) { [weak self] in
            self?.viewModel.onDeleteLessonTap(id: id)
        }
    }

    private func bindAppState() {
        UIApplication.shared.rx
            .applicationDidEnterBackground
            .bind { [weak self] _ in
                self?.viewModel.fetchStreams()
            }
            .disposed(by: bag)
    }

    private func bindUI() {
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, MyFutureStreamsCellViewModel>>(
            configureCell: { _, tableView, indexPath, element in
                let cell = tableView.dequeueReusableCell(withModel: element, for: indexPath)
                element.configureAny(cell)
                return cell
            })

        dataSource.titleForHeaderInSection = { dataSource, index in
            return dataSource.sectionModels[index].model
        }

        viewModel.dataItems
            .bind(to: items)
            .disposed(by: bag)

        items
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)

        tableView
          .rx.setDelegate(self)
          .disposed(by: bag)

        tableHeaderView
            .onSelectTodayStreams
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.items.accept(self.viewModel.todayItems)
            }
            .disposed(by: bag)

        tableHeaderView
            .onSelectAllStreams
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.items.accept(self.viewModel.allItems)
            }
            .disposed(by: bag)

        viewModel
            .onDeleteLesson
            .bind {[weak self] id in
                self?.showAlert(id: id)
            }
            .disposed(by: bag)
    }

    private func setupUI() {
        navigationItem.rightBarButtonItem = scanQRButton
        view.backgroundColor = TalkmeColors.mainAccountBackground
    }

    private func setupNavigationBar() {
        customNavigationController?.style = .plainWhiteWithQR(title: viewModel.navigationTitle)
    }

    private func setupLayout() {
        view.addSubviews([tableView, tableHeaderView])

        tableHeaderView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(10)
            make.leading.trailing.equalToSuperview()
        }

        tableView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(tableHeaderView.snp.bottom)
            make.bottom.equalToSuperview()
        }
    }
}

extension StreamsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        guard section <= viewModel.dataItems.value.endIndex,
              let currentSectionItem = viewModel.dataItems.value[section].items.first?.myStream else { return UIView() }

        let headerView = StreamSectionHeaderView()
        guard let title = currentSectionItem.headerTitle else { return UIView() }
        headerView.configure(title: title)
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard section <= viewModel.dataItems.value.endIndex,
              let currentSectionItem = viewModel.dataItems.value[section].items.first?.myStream else { return 0 }

        switch currentSectionItem {
        case .myLiveStreams, .myStreams, .liveSubscribedStreams, .subscribedStreams:
            return 25
        default:
            return 0
        }
    }
}
