//
//  MyFutureStreamsCellViewModel.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 05.01.2021.
//

import RxCocoa
import RxSwift

final class MyFutureStreamsCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let id: Int
    private var timer: Double?
    let myStream: StreamInfoViewType
    let onWatchButtonTap = PublishSubject<Void>()
    let onEditBtnRelay = PublishRelay<Void>()
    let onDeleteBtnRelay = PublishRelay<Void>()
    let onStreamBecomeLive = PublishRelay<Void>()
    let onWatchLessonInfo = PublishSubject<Void>()
    var bag = DisposeBag()

    // MARK: - Initializers

    init(myStream: StreamInfoViewType, id: Int) {
        self.id = id
        self.myStream = myStream
        switch myStream {
        case .myStreams, .myLiveStreams, .subscribedStreams, .nearestSubscribedStreams, .liveSubscribedStreams:
            break
        case .myNearestStreams(let model):
            timer = model.date
        }
    }

    func configure(_ cell: MyFutureStreamsTableCell) {
        cell.setupUI(viewType: myStream)

        cell
            .onEditBtnTap
            .bind(to: onEditBtnRelay)
            .disposed(by: cell.bag)

        cell
            .onDeleteBtnTap
            .bind(to: onDeleteBtnRelay)
            .disposed(by: cell.bag)

        cell
            .onStreamBecomeLive
            .bind(to: onStreamBecomeLive)
            .disposed(by: cell.bag)

        switch myStream {
        case .liveSubscribedStreams, .myLiveStreams, .myNearestStreams, .nearestSubscribedStreams:
            cell
                .onWatchButtonTap
                .filter { [weak self]  in
                    if ((self?.timer ?? 0) - Date().timeIntervalSince1970) / 60 > 1 {
                        self?.showAlert()
                        return false
                    }
                    return true
                }
                .bind(to: onWatchButtonTap)
                .disposed(by: cell.bag)
        case .myStreams, .subscribedStreams:
            cell
                .onWatchButtonTap
                .bind(to: onWatchLessonInfo)
                .disposed(by: cell.bag)

            cell
                .onAvatarTap
                .bind(to: onWatchLessonInfo)
                .disposed(by: cell.bag)
        }
    }

    private func showAlert() {
        AlertControllerHelper.showSimpleOkAlert(title: "one_minute_havent_passed".localized)
    }
}
