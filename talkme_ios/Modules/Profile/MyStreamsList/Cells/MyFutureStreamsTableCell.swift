//
//  MyFutureStreamsTableCell.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 05.01.2021.
//

import RxSwift
import SnapKit

final class MyFutureStreamsTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var onWatchButtonTap = streamMainView.onWatchButtonTap
    private(set) lazy var onEditBtnTap = streamMainView.onEditBtnTap
    private(set) lazy var onDeleteBtnTap = streamMainView.onDeleteBtnTap
    private(set) lazy var onAvatarTap = streamMainView.onStreamImageTap
    private(set) lazy var onStreamBecomeLive = streamMainView.onStreamBecomeLive
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let streamMainView = StreamMainView()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overriden Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Public Methods

    func setupUI(viewType: StreamInfoViewType) {
        contentView.backgroundColor = .clear
        backgroundColor = TalkmeColors.mainAccountBackground
        streamMainView.configure(viewType: viewType)
        selectionStyle = .none
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(streamMainView)

        streamMainView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 4 : 6)
            make.leading.trailing.equalToSuperview().inset(10)
        }
    }
}
