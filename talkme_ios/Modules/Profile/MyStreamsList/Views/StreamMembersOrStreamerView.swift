//
//  StreamMembersOrStreamerView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import UIKit
import SnapKit

final class StreamMembersOrStreamerView: UIView {

    // MARK: - Public properties

    enum StreamMembersOrStreamerType {
        case membersStream
        case nameStream
    }

    // MARK: - Public methods

    func configure(viewType: StreamMembersOrStreamerType) {
        switch viewType {
        case .nameStream:
            let streamerName = StreamerNameView()
            setupLayout(view: streamerName)
        case .membersStream:
            let membersStream = MembersStreamView()
            membersStream.configure(viewType: .futureStream(subscribers: 0))
            setupLayout(view: membersStream)
        }
    }

    // MARK: - Private methods

    private func setupLayout(view: UIView) {
        addSubview(view)

        view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
