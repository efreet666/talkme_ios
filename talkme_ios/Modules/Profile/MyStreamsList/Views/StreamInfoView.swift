//
//  StreamInfoView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import UIKit
import SnapKit

enum StreamInfoViewType {

    case myStreams(Lesson?)
    case myNearestStreams(name: String?, date: Double?, subscribers: Int?, avatar: String?)
    case myLiveStreams(name: String? = nil, date: Double? = nil, subscribers: Int? = nil, avatar: String? = nil)
    case subscribedStreams(name: String?, cost: Int?, date: Double?, teacherName: String?, avatar: String?)
    case nearestSubscribedStreams
    case liveSubscribedStreams(name: String?, date: Double?, teacherName: String?, avatar: String?)

    var headerTitle: String? {
        switch self {
        case .myStreams:
            return "talkme_stream_all_streams".localized
        case .myNearestStreams, .nearestSubscribedStreams:
            return nil
        case .myLiveStreams, .liveSubscribedStreams:
            return "talkme_stream_live_stream".localized
        case .subscribedStreams:
            return "talkme_stream_all_subscribes".localized
        }
    }
}

final class StreamInfoView: UIView {

    // MARK: - Private properties

    private let membersAndDurationView = StreamMembersAndDurationView()
    private let dateAndTimeView = StreamDateAndTimeView()
    private let streamerNameView = StreamerNameView()
    private let membersStreamView = MembersStreamView()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        hideAllViews()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(viewType: StreamInfoViewType) {
        hideAllViews()
        switch viewType {
        case .myStreams(let model):
            membersAndDurationView.configure(time: model?.lessonTime ?? "", subscribers: model?.subscribers ?? 0)
            dateAndTimeView.configure(date: model?.date ?? 0)
            membersAndDurationView.isHidden = false
            dateAndTimeView.isHidden = false
        case .subscribedStreams(_, _, let date, _, _):
            dateAndTimeView.isHidden = false
            streamerNameView.isHidden = false
            streamerNameView.configure(viewType: viewType)
            dateAndTimeView.configure(date: date ?? 0 )
            updateForSubscribedStreams()
        case .myNearestStreams(_, _, let subscribers, _):
            membersStreamView.isHidden = false
            membersStreamView.configure(viewType: .futureStream(subscribers: subscribers ?? 0))
        case .myLiveStreams(_, _, let subscribers, _):
            membersStreamView.configure(viewType: .futureStream(subscribers: subscribers ?? 0))
            membersStreamView.isHidden = false
            membersStreamView.configure(viewType: .liveStream(subscribers: subscribers ?? 0))
        case .liveSubscribedStreams:
            streamerNameView.isHidden = false
            streamerNameView.configure(viewType: viewType)
        default:
            break
        }
    }

    // MARK: - Private Methods

    private func hideAllViews() {
        membersAndDurationView.isHidden = true
        dateAndTimeView.isHidden = true
        streamerNameView.isHidden = true
        membersStreamView.isHidden = true
    }

    private func updateForSubscribedStreams() {
        dateAndTimeView.snp.updateConstraints { make in
            make.leading.trailing.top.equalToSuperview()
        }
    }

    private func setupLayout() {
      addSubviews(membersAndDurationView, dateAndTimeView, streamerNameView, membersStreamView)

        membersAndDurationView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(12)
        }

        dateAndTimeView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(22)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(12)
        }

        membersStreamView.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.width.equalTo(65)
            make.height.equalTo(28)
            make.centerY.equalToSuperview()
        }

        streamerNameView.snp.makeConstraints { make in
            make.top.equalTo(membersAndDurationView.snp.bottom).offset(10)
            make.bottom.equalToSuperview()
            make.leading.equalTo(membersAndDurationView)
            make.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 15 : 18)
        }
    }
}
