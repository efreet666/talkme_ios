//
//  MembersStreamView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import UIKit
import SnapKit

final class MembersStreamView: UIView {

    // MARK: - Public properties

    enum MembersStreamViewType {
        case futureStream(subscribers: Int)
        case liveStream(subscribers: Int)
    }

    // MARK: - Private properties

    private let membersCountLabel: UILabel = {
        let membersCountLabel = UILabel()
        membersCountLabel.font = .montserratSemiBold(ofSize: 13)
        membersCountLabel.textColor = TalkmeColors.shadow
        return membersCountLabel
    }()

    private let membersImage: UIImageView = {
        let membersImage = UIImageView()
        membersImage.image = UIImage(named: "streamMembers")
        membersImage.contentMode = .scaleAspectFill
        return membersImage
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func configure(viewType: MembersStreamViewType) {
        layer.cornerRadius = 15
        switch viewType {
        case .futureStream(let subscribers):
            membersCountLabel.text = String(subscribers)
            backgroundColor = TalkmeColors.buttonGrayColor
        case .liveStream(let subscribers):
            membersCountLabel.text = String(subscribers)
            backgroundColor = TalkmeColors.liveStreamMembersBackground
        }
    }

    // MARK: - Private methods

    private func setupLayout() {
        addSubview(membersCountLabel)
        addSubview(membersImage)

        membersImage.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(14)
            make.centerY.equalToSuperview()
            make.size.equalTo(12)
        }

        membersCountLabel.snp.makeConstraints { make in
            make.left.equalTo(membersImage.snp.right).offset(9)
            make.centerY.equalTo(membersImage)
        }
    }
}
