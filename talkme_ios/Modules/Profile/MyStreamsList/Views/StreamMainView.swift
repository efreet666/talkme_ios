//
//  StreamMainView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 07.01.2021.
//

import RxSwift
import SnapKit

final class StreamMainView: UIView {

    // MARK: - Public Properties

    private(set) lazy var onWatchButtonTap = streamShowButton.onShowButtonTap
    private(set) lazy var onEditBtnTap = buttonsView.editButtonTap
    private(set) lazy var onDeleteBtnTap = buttonsView.deleteBtnTap
    private(set) lazy var onStreamBecomeLive = streamTimerView.onStreamBecomeLive
    private(set) lazy var onStreamImageTap = streamImageView.streamImageTap

    // MARK: - Private properties

    private let gradient = CAGradientLayer(
        start: .streamStart,
        end: .streamEnd,
        colors: [TalkmeColors.deepBlue.cgColor, TalkmeColors.blueLabels.cgColor],
        locations: [0, 1])

    private let nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.textColor = TalkmeColors.codeCountry
        nameLabel.font =  .montserratBold(ofSize: UIScreen.isSE ? 15 : 16)
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.numberOfLines = 3
        return nameLabel
    }()

    private let liveLabel: UILabel = {
        let label = RoundedLabel(withInsets: 6, 6, 4, 4)
        label.text = "Live"
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        label.textColor = TalkmeColors.white
        label.layer.backgroundColor = TalkmeColors.greenLabels.cgColor
        label.textAlignment = .center
        label.layer.cornerRadius = 12
        return label
    }()

    private let streamInfoView = StreamInfoView()
    private let streamImageView = StreamImageView()
    private let streamTimerView = StreamTimerView()
    private let streamLiveTimerView = StreamLiveTimerView()
    private let streamShowButton = StreamShowButtonView()
    private let streamPriceView = StreamPriceView()
    private let buttonsView = StreamEditButtonsView()
    private let bag = DisposeBag()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = bounds
        gradient.cornerRadius = 12
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(viewType: StreamInfoViewType) {
        setupDefaultState()
        setupViews(viewType: viewType)
        streamInfoView.configure(viewType: viewType)
        buttonsView.configure(viewType: viewType)
        streamShowButton.configure(viewType: viewType)
    }

    // MARK: - Private Methods

    private func setupViews(viewType: StreamInfoViewType) {
        switch viewType {
        case .myStreams(let model):
            setupMyStreams(model: model)
        case .myNearestStreams(let name, date: let date, _, let avatar):
            setupMyNearestStreams(name: name, date: date, avatar: avatar)
        case .myLiveStreams(let name, let date, _, let avatar):
            setupLiveStreams(name: name, date: date, avatar: avatar)
        case .subscribedStreams(let name, let cost, _, _, let avatar):
            setupSubscribedStreams(name: name, cost: cost, avatar: avatar)
        case .liveSubscribedStreams(let name, let date, _, let avatar):
            setupliveSubscribedStreams(name: name, date: date, avatar: avatar)
        default:
            break
        }
    }

    private func setupMyStreams(model: Lesson?) {
        guard let title = model?.name else { return }
        nameLabel.text = title
        nameLabel.textColor = TalkmeColors.codeCountry
        if (model?.cost ?? 0) != 0 {
            streamPriceView.configure(viewType: .moneyStream(price: String(model?.cost ?? 0)))
        } else {
            streamPriceView.configure(viewType: .freeStream)
        }
        streamImageView.configure(image: model?.tileImage)
        backgroundColor = TalkmeColors.white
        buttonsView.isHidden = false
        streamPriceView.isHidden = false
    }

    private func setupLiveStreams(name: String?, date: Double?, avatar: String?) {
        layer.insertSublayer(gradient, at: 0)
        nameLabel.text = name
        backgroundColor = TalkmeColors.blueLabels
        nameLabel.textColor = TalkmeColors.white
        buttonsView.isHidden = true
        streamLiveTimerView.isHidden = false
        streamImageView.configure(image: avatar)
        liveLabel.isHidden = false
        streamLiveTimerView.configure(time: date ?? 0)
        streamPriceView.isHidden = true
        streamLiveTimerView.isHidden = false
    }

    private func setupliveSubscribedStreams(name: String?, date: Double?, avatar: String?) {
        layer.insertSublayer(gradient, at: 0)
        nameLabel.text = name
        backgroundColor = TalkmeColors.blueLabels
        nameLabel.textColor = TalkmeColors.white
        buttonsView.isHidden = true
        streamLiveTimerView.isHidden = false
        streamLiveTimerView.isHidden = false
        streamImageView.configure(image: avatar)
        liveLabel.isHidden = false
        streamLiveTimerView.configure(time: date ?? 0)
        streamPriceView.isHidden = true
    }

    private func setupSubscribedStreams(name: String?, cost: Int?, avatar: String?) {
        nameLabel.text = name
        backgroundColor = TalkmeColors.white
        buttonsView.isHidden = false
        streamImageView.configure(image: avatar)
        if (cost ?? 0) != 0 {
            streamPriceView.configure(viewType: .moneyStream(price: String(cost ?? 0)))
        } else {
            streamPriceView.configure(viewType: .freeStream)
        }
        streamPriceView.isHidden = false
    }

    private func setupMyNearestStreams(name: String?, date: Double?, avatar: String?) {
        nameLabel.text = name
        layer.borderWidth = 2
        layer.borderColor = TalkmeColors.greenLabels.cgColor
        backgroundColor = TalkmeColors.white
        streamImageView.configure(image: avatar)
        streamTimerView.configure(time: date ?? 0)
        streamTimerView.isHidden = false
        streamPriceView.isHidden = true
        liveLabel.isHidden = true
    }

    private func setupDefaultState() {
        gradient.removeFromSuperlayer()
        streamLiveTimerView.isHidden = true
        streamTimerView.isHidden = true
        streamPriceView.isHidden = true
        layer.borderWidth = 0
        liveLabel.isHidden = true
    }

    private func setupLayout() {
        layer.cornerRadius = 12
        addSubviews([nameLabel,
                     streamImageView,
                     streamInfoView,
                     streamShowButton,
                     buttonsView,
                     streamPriceView,
                     streamTimerView,
                     streamLiveTimerView,
                     liveLabel])

        streamImageView.snp.makeConstraints { make in
            make.top.left.equalToSuperview().offset(11)
            make.height.equalTo(UIScreen.isSE ? 96 : 114)
            make.width.equalTo(UIScreen.isSE ? 89 : 106)
        }

        streamLiveTimerView.snp.makeConstraints { make in
            make.top.equalTo(streamImageView.snp.bottom).offset(UIScreen.isSE ? 14 : 22)
            make.leading.trailing.equalTo(streamImageView)
            make.bottom.equalToSuperview().inset(22)

        }

        streamShowButton.snp.makeConstraints { showButton in
            showButton.top.equalTo(streamInfoView.snp.bottom).offset(12)
            showButton.left.equalTo(streamImageView.snp.right).offset(UIScreen.isSE ? 14 : 25)
            showButton.height.equalTo(39)
            showButton.right.equalTo(buttonsView.snp.right)
            showButton.bottom.equalToSuperview().offset(UIScreen.isSE ? -11 : -15)
        }

        buttonsView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(11)
            make.left.equalTo(nameLabel.snp.right).offset(15)
            make.right.equalToSuperview().offset(-14)
        }

        streamInfoView.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 15)
            make.left.equalTo(streamImageView.snp.right).offset(UIScreen.isSE ? 15 : 25)
            make.right.equalToSuperview().offset(-10)
        }

        nameLabel.snp.makeConstraints { nameLabel in
            nameLabel.top.equalToSuperview().offset(UIScreen.isSE ? 13 : 14)
            nameLabel.left.equalTo(streamImageView.snp.right).offset(UIScreen.isSE ? 14 : 25)
            nameLabel.right.equalTo(buttonsView.snp.left).offset(UIScreen.isSE ? -12 : -27)
        }

        streamPriceView.snp.makeConstraints { make in
            make.top.equalTo(streamImageView.snp.bottom).offset(UIScreen.isSE ? 14 : 10)
            make.left.equalTo(streamImageView.snp.left)
            make.bottom.equalToSuperview().inset(26)
            make.width.equalTo(streamImageView)
            make.height.equalTo(UIScreen.isSE ? 26 : 30)
        }

        streamTimerView.snp.makeConstraints { make in
            make.top.equalTo(streamImageView.snp.bottom).offset(6)
            make.leading.trailing.equalTo(streamImageView)
            make.bottom.equalToSuperview().offset(-28)
        }

        liveLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(streamImageView).inset(UIScreen.isSE ? 26 : 32)
            make.centerY.equalTo(streamImageView.snp.bottom)
        }
    }
}
