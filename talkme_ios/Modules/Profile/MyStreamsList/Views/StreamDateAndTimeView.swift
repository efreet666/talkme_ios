//
//  StreamDateAndTimeView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import UIKit
import SnapKit

final class StreamDateAndTimeView: UIView {

    // MARK: - Private properties

    private let labelSizeFont: UIFont = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)

    private let dateImage: UIImageView = {
        let dateImage = UIImageView()
        dateImage.image = UIImage(named: "streamDate")
        dateImage.contentMode = .scaleAspectFill
        return dateImage
    }()

    private let dateLabel: UILabel = {
        let dateLabel = UILabel()
        dateLabel.textColor = TalkmeColors.etherDataColor
        return dateLabel
    }()

    private let timeImage: UIImageView = {
        let timeImage = UIImageView()
        timeImage.image = UIImage(named: "streamTime")
        timeImage.contentMode = .scaleAspectFill
        return timeImage
    }()

    private let timeLabel: UILabel = {
        let timeLabel = UILabel()
        timeLabel.textColor = TalkmeColors.etherDataColor
        return timeLabel
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        dateLabel.font = labelSizeFont
        timeLabel.font = labelSizeFont
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(date: Double) {
        dateLabel.text = Formatters.convertDate(seconds: date)
        timeLabel.text = Formatters.convertTime(seconds: date)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        let viewsArray = [dateImage, dateLabel, timeImage, timeLabel]
        addSubviews(viewsArray)

        dateImage.snp.makeConstraints { make in
            make.top.leading.bottom.equalToSuperview()
            make.size.equalTo(11)
        }

        dateLabel.snp.makeConstraints { make in
            make.centerY.equalTo(dateImage.snp.centerY)
            make.left.equalTo(dateImage.snp.right).offset(8)
        }

        timeImage.snp.makeConstraints { make in
            make.centerY.equalTo(dateLabel.snp.centerY)
            make.left.equalTo(dateLabel.snp.left).offset(UIScreen.isSE ? 60 : 70)
            make.size.equalTo(11)
        }

        timeLabel.snp.makeConstraints { make in
            make.centerY.equalTo(timeImage.snp.centerY)
            make.left.equalTo(timeImage.snp.right).offset(7)
            make.right.equalToSuperview()
        }
    }
}
