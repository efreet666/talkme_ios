//
//  StreamerNameView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import UIKit
import SnapKit

final class StreamerNameView: UIView {

    // MARK: - Private properties

    private let streamerName: UILabel = {
        let streamerName = UILabel()
        streamerName.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        return streamerName
    }()

    private let streamerImage: UIImageView = {
        let streamerImage = UIImageView()
        streamerImage.contentMode = .scaleAspectFill
        return streamerImage
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func configure(viewType: StreamInfoViewType) {
        switch viewType {
        case .subscribedStreams(_, _, _, let teacherName, _):
            streamerImage.image = UIImage(named: "streamerNameBlue")
            streamerName.textColor = TalkmeColors.etherDataColor
            streamerName.text = teacherName
        case .nearestSubscribedStreams:
            streamerImage.image = UIImage(named: "streamerNameBlue")
            streamerName.textColor = TalkmeColors.etherDataColor
        case .liveSubscribedStreams(_, _, let teacherName, _):
            streamerImage.image = UIImage(named: "streamerNameWhite")
            streamerName.textColor = TalkmeColors.white
            streamerName.text = teacherName
        default:
            break
        }
    }

    // MARK: - Private methods

    private func setupLayout() {
        addSubview(streamerImage)
        addSubview(streamerName)

        streamerImage.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.width.equalTo(11)
            make.height.equalTo(14)
        }

        streamerName.snp.makeConstraints { make in
            make.left.equalTo(streamerImage.snp.right).offset(9)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.right.equalToSuperview()
        }
    }
}
