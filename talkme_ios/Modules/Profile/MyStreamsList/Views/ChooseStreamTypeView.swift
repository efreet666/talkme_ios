//
//  ChooseStreamTypeView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 08.01.2021.
//

import UIKit
import RxCocoa
import RxSwift

final class ChooseStreamTypeView: UIView {

    enum ButtonType {
        case todayStreamsButton
        case allStreamsButton
    }

    // MARK: - Public Properties

    private(set) lazy var onTodayBtnTap = todayStreamsButton.rx.tap
    private(set) lazy var onAllBtnTap = allStreamsButton.rx.tap
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let todayStreamsButton: UIButton = {
        let todayStreamsButton = UIButton()
        todayStreamsButton.setTitle("talkme_stream_list_today".localized, for: .normal)
        todayStreamsButton.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        todayStreamsButton.layer.cornerRadius = 20
        todayStreamsButton.titleLabel?.font = .montserratSemiBold(ofSize: 15)
        todayStreamsButton.backgroundColor = .clear
        todayStreamsButton.layer.borderWidth = 1
        todayStreamsButton.layer.borderColor = TalkmeColors.grayButtonBorder.cgColor
        return todayStreamsButton
    }()

    private let allStreamsButton: UIButton = {
        let allStreamsButton = UIButton()
        allStreamsButton.setTitle("talkme_stream_list_all".localized, for: .normal)
        allStreamsButton.setTitleColor(TalkmeColors.white, for: .normal)
        allStreamsButton.titleLabel?.font = .montserratSemiBold(ofSize: 15)
        allStreamsButton.layer.cornerRadius = 20
        allStreamsButton.backgroundColor = TalkmeColors.blueLabels
        return allStreamsButton
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupLayout()
        bindUI()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupLayout() {
        addSubviews([todayStreamsButton, allStreamsButton])

        todayStreamsButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.centerY.equalToSuperview()
            make.trailing.equalTo(snp.centerX).offset(-3)
            make.leading.equalToSuperview()
        }

        allStreamsButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.centerY.equalToSuperview()
            make.leading.equalTo(snp.centerX).offset(3)
            make.trailing.equalToSuperview()
        }
    }

    private func setupButtonState(buttonType: ButtonType) {
        switch buttonType {
        case .todayStreamsButton:
            todayStreamsButton.backgroundColor = TalkmeColors.blueLabels
            todayStreamsButton.setTitleColor(TalkmeColors.white, for: .normal)
            allStreamsButton.layer.borderWidth = 1
            allStreamsButton.layer.borderColor = TalkmeColors.grayButtonBorder.cgColor
            allStreamsButton.setTitleColor(TalkmeColors.blueLabels, for: .normal)
            allStreamsButton.backgroundColor = .clear
        case .allStreamsButton:
            allStreamsButton.backgroundColor = TalkmeColors.blueLabels
            allStreamsButton.setTitleColor(TalkmeColors.white, for: .normal)
            todayStreamsButton.layer.borderWidth = 1
            todayStreamsButton.layer.borderColor = TalkmeColors.grayButtonBorder.cgColor
            todayStreamsButton.setTitleColor(TalkmeColors.blueLabels, for: .normal)
            todayStreamsButton.backgroundColor = .clear
        }
    }

    private func bindUI() {
        todayStreamsButton.rx.tap.bind { [weak self] in
            self?.setupButtonState(buttonType: .todayStreamsButton)
        }.disposed(by: bag)

        allStreamsButton.rx.tap.bind { [weak self] in
            self?.setupButtonState(buttonType: .allStreamsButton)
        }.disposed(by: bag)
    }
}
