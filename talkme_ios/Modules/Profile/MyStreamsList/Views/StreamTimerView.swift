//
//  StreamTimerView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 07.01.2021.
//

import RxCocoa
import RxSwift
import SnapKit

final class StreamTimerView: UIView {

    // MARK: Public Properties

    let onStreamBecomeLive = PublishRelay<Void>()

    // MARK: - Private Properties

    private var timer: Timer?
    private let beforeStreamLabel: UILabel = {
        let beforeStreamLabel = UILabel()
        beforeStreamLabel.textColor = TalkmeColors.blackLabels
        beforeStreamLabel.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 13)
        beforeStreamLabel.textAlignment = .center
        beforeStreamLabel.text = "talkme_stream_list_before_stream".localized
        return beforeStreamLabel
    }()

    private let timerImage: UIImageView = {
        let timerImage = UIImageView()
        timerImage.image = UIImage(named: "redTimerImage")
        timerImage.contentMode = .scaleAspectFill
        return timerImage
    }()

    private let timerLabel: UILabel = {
        let timerLabel = UILabel()
        timerLabel.font = .montserratSemiBold(ofSize: 15)
        timerLabel.textAlignment = .center
        timerLabel.textColor = TalkmeColors.warningColor
        return timerLabel
    }()
    private let bag = DisposeBag()

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(time: TimeInterval) {
        setupLabel(timeInterval: time)
        setupTimer(initial: time)
    }

    // MARK: - Private Methods

    private func setupTimer(initial interval: TimeInterval) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.setupLabel(timeInterval: interval - 1)
        }
    }

    private func setupLabel(timeInterval: TimeInterval) {
        let time = TimerManager.diffFromFutureTime(timeInterval: timeInterval)
        guard time >= 0 else {
            timer?.invalidate()
            onStreamBecomeLive.accept(())
            return
        }
        timerLabel.text = Formatters.hourMinuteSecondsString(interval: time)
    }

    private func setupLayout() {
        addSubviews([beforeStreamLabel, timerImage, timerLabel])

        beforeStreamLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview().inset(UIScreen.isSE ? 1 : 9)
            make.width.equalTo(89)
            make.height.equalTo(14)
        }

        timerImage.snp.makeConstraints { make in
            make.left.equalTo(beforeStreamLabel.snp.left).offset(5)
            make.width.equalTo(11)
            make.height.equalTo(13)
            make.bottom.equalToSuperview()
        }

        timerLabel.snp.makeConstraints { make in
            make.left.equalTo(timerImage.snp.right).offset(UIScreen.isSE ? 5 : 6)
            make.centerY.equalTo(timerImage.snp.centerY)
            make.height.equalTo(timerImage.snp.height)
        }
    }
}
