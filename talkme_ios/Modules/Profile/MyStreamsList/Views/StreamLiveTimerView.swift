//
//  StreamLiveTimerView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 14.01.2021.
//

import RxSwift

final class StreamLiveTimerView: UIView {

    // MARK: - Private properties

    private var timer: Timer?
    private let timerLabel: UILabel = {
        let timerLabel = UILabel()
        timerLabel.font = .montserratSemiBold(ofSize: 15)
        timerLabel.textColor = TalkmeColors.white
        timerLabel.textAlignment = .center
        return timerLabel
    }()

    private let timerImage: UIImageView = {
        let timerImage = UIImageView()
        timerImage.image = UIImage(named: "whiteTimer")
        timerImage.contentMode = .scaleAspectFill
        return timerImage
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(time: TimeInterval) {
        setupLabel(timeInterval: time)
        setupTimer(initial: time)
    }

    // MARK: - Private Methods

    private func setupTimer(initial interval: TimeInterval) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.setupLabel(timeInterval: interval + 1)
        }
    }

    private func setupLabel(timeInterval: TimeInterval) {
        let time = TimerManager.diffFromNowTime(timeInterval: timeInterval)
        guard time > 0 else { return }
        timerLabel.text = Formatters.hourMinuteSecondsString(interval: time)
    }

    private func setupLayout() {
        addSubviews([timerImage, timerLabel])

        timerImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 6 : 15)
            make.width.equalTo(UIScreen.isSE ? 11 : 13)
            make.height.equalTo(UIScreen.isSE ? 15 : 17)
        }

        timerLabel.snp.makeConstraints { make in
            make.left.equalTo(timerImage.snp.right).offset(5)
            make.centerY.equalTo(timerImage.snp.centerY)
        }
    }
}
