//
//  StreamImageView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import UIKit
import SnapKit
import RxSwift

final class StreamImageView: UIView {

    // MARK: - Public properties

    lazy var streamImageTap = streamButton.rx.tap

    let streamImage: UIImageView = {
        let streamImage = UIImageView()
        streamImage.layer.cornerRadius = 12
        streamImage.layer.masksToBounds = true
        streamImage.contentMode = .scaleAspectFill
        return streamImage
    }()

    // MARK: - Private Properties

    private let streamButton: UIButton = {
       let button = UIButton()
       button.setTitle(nil, for: .normal)
       return button
   }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(image: String?) {
        guard let avatar = image else { return }
        let urlString = avatar
        let url = URL(string: urlString)
        streamImage.kf.setImage(with: url)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([streamButton, streamImage])

        streamImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        streamButton.snp.makeConstraints { make in
            make.edges.equalTo(streamImage.center)
        }
    }
}
