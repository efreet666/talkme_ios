//
//  StreamTableViewHeader.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 10.01.2021.
//

import RxCocoa
import RxSwift

final class StreamTableViewHeader: UIView {

    // MARK: - Public Properties

    let onSelectTodayStreams = PublishRelay<Void>()
    let onSelectAllStreams = PublishRelay<Void>()

    // MARK: - Private Properties

    private let streamSearchBarView = StreamSearchBar()
    private let chooseStreamTypeView = ChooseStreamTypeView()
    private let bag = DisposeBag()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupLayout()
        bindUI()
        streamSearchBarView.isHidden = true
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private methods

    private func bindUI() {
        chooseStreamTypeView
            .onTodayBtnTap
            .bind(to: onSelectTodayStreams)
            .disposed(by: bag)

        chooseStreamTypeView
            .onAllBtnTap
            .bind(to: onSelectAllStreams)
            .disposed(by: bag)
    }

    private func setupUI() {
        backgroundColor = TalkmeColors.mainAccountBackground
    }

    private func setupLayout() {
        addSubviews([streamSearchBarView, chooseStreamTypeView])

        streamSearchBarView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 34 : 42)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        chooseStreamTypeView.snp.makeConstraints { make in
            make.top.equalTo(streamSearchBarView.snp.bottom).offset(15)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(39)
            make.bottom.equalToSuperview().inset(17)
        }
    }
}
