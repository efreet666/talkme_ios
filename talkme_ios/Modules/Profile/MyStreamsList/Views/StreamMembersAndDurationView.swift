//
//  StreamMembersAndDurationView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import UIKit
import SnapKit

final class StreamMembersAndDurationView: UIView {

    // MARK: - Private properties

    private let labelSizeFont: UIFont = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)

    private let membersCountLabel: UILabel = {
        let membersCountLabel = UILabel()
        membersCountLabel.textColor = TalkmeColors.etherDataColor
        return membersCountLabel
    }()

    private let membersCountImage: UIImageView = {
        let membersCountImage = UIImageView()
        membersCountImage.image = UIImage(named: "streamMembersPurple")
        membersCountImage.contentMode = .scaleAspectFill
        return membersCountImage
    }()

    private let durationImage: UIImageView = {
        let durationImage = UIImageView()
        durationImage.image = UIImage(named: "streamDuration")
        durationImage.contentMode = .scaleAspectFill
        return durationImage
    }()

    private let durationLabel: UILabel = {
        let durationLabel = UILabel()
        durationLabel.textColor = TalkmeColors.etherDataColor
        return durationLabel
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        membersCountLabel.font = labelSizeFont
        durationLabel.font = labelSizeFont
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(time: String, subscribers: Int) {
        membersCountLabel.text = String(subscribers)
        durationLabel.text = Formatters.setupHourAndMinute(date: time)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        let viewsArray = [membersCountImage, membersCountLabel, durationImage, durationLabel]
        addSubviews(viewsArray)

        membersCountImage.snp.makeConstraints { make in
            make.top.leading.bottom.equalToSuperview()
            make.width.equalTo(12)
            make.width.equalTo(11)
        }

        membersCountLabel.snp.makeConstraints { make in
            make.centerY.equalTo(membersCountImage.snp.centerY)
            make.left.equalTo(membersCountImage.snp.right).offset(8)
        }

        durationImage.snp.makeConstraints { make in
            make.centerY.equalTo(membersCountLabel.snp.centerY)
            make.left.equalTo(membersCountLabel.snp.left).offset(UIScreen.isSE ? 60 : 70)
            make.width.equalTo(11)
            make.width.equalTo(13)
        }

        durationLabel.snp.makeConstraints { make in
            make.centerY.equalTo(durationImage.snp.centerY)
            make.left.equalTo(durationImage.snp.right).offset(7)
            make.right.equalToSuperview()
        }
    }
}
