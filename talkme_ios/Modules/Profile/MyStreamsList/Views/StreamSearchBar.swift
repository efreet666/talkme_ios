//
//  StreamSearchBar.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 08.01.2021.
//

import UIKit

final class StreamSearchBar: UIView {

    // MARK: - Private properties

    private let searchIcon: UIImageView = {
        let searchIcon = UIImageView()
        searchIcon.image = UIImage(named: "searchIcon")
        searchIcon.contentMode = .scaleAspectFill
        return searchIcon
    }()

    let searchTextField: UITextField = {
        let searchTextField = UITextField()
        searchTextField.placeholder = "talkme_stream_list_search".localized
        return searchTextField
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
    }

    private func setupUI() {
        backgroundColor = .white
        let textFieldAttributes = [
            NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 12)!]
        searchTextField.attributedPlaceholder = NSAttributedString(
            string: "talkme_stream_list_search".localized,
            attributes: textFieldAttributes)
        searchTextField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
    }

    private func setupLayout() {
        addSubviews([searchIcon, searchTextField])

        searchIcon.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(14)
            make.size.equalTo(UIScreen.isSE ? 14 : 20)
        }

        searchTextField.snp.makeConstraints { make in
            make.leading.equalTo(searchIcon.snp.trailing).offset(UIScreen.isSE ? 12 : 13)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 12 : 13)
            make.height.equalToSuperview()
            make.centerY.equalTo(searchIcon)
        }
    }
}
