//
//  StreamSectionHeaderView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 10.01.2021.
//

import UIKit

final class StreamSectionHeaderView: UIView {

    // MARK: - Public Properties

    let sectionLabel: UILabel = {
        let sectionLabel = UILabel()
        sectionLabel.font = .montserratBold(ofSize: 14)
        sectionLabel.textColor = TalkmeColors.grayLabels
        return sectionLabel
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func configure(title: String, isHidden: Bool = false) {
        sectionLabel.text = title
        sectionLabel.isHidden = isHidden
        backgroundColor = TalkmeColors.mainAccountBackground
    }

    // MARK: - Private methods

    private func setupLayout() {
        addSubview(sectionLabel)

        sectionLabel.snp.makeConstraints { make in
            make.top.trailing.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(10)
        }
    }
}
