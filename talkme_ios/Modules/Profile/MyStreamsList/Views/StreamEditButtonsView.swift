//
//  StreamEditButtonsView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 06.01.2021.
//

import RxSwift
import SnapKit

final class StreamEditButtonsView: UIView {

    // MARK: - Public Properties

    private(set) lazy var editButtonTap = editButton.rx.tap
    private(set) lazy var deleteBtnTap = dismissButton.rx.tap

    // MARK: - Private Properties

   private let editButton: UIButton = {
        let editButton = UIButton()
        editButton.backgroundColor = TalkmeColors.buttonGrayColor
        editButton.setImage(UIImage(named: "streamEdit"), for: .normal)
        editButton.layer.cornerRadius = 15
        return editButton
    }()

    private let dismissButton: UIButton = {
        let dismissButton = UIButton()
        dismissButton.backgroundColor = TalkmeColors.buttonGrayColor
        dismissButton.setImage(UIImage(named: "dismissStream"), for: .normal)
        dismissButton.layer.cornerRadius = 15
        return dismissButton
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(viewType: StreamInfoViewType) {
      editButton.isHidden = true
      dismissButton.isHidden = true
        switch viewType {
        case .subscribedStreams:
            editButton.isHidden = true
            dismissButton.isHidden = false
        case .myNearestStreams, .myStreams, .nearestSubscribedStreams:
          editButton.isHidden = false
          dismissButton.isHidden = false
        case .myLiveStreams, .liveSubscribedStreams:
          editButton.isHidden = true
          dismissButton.isHidden = true
        }
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(editButton)
        addSubview(dismissButton)

        dismissButton.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? 28 : 34)
        }

        editButton.snp.makeConstraints { make in
            make.top.equalTo(dismissButton.snp.bottom).offset(UIScreen.isSE ? 14 : 17)
            make.bottom.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? 28 : 34)
        }
    }
}
