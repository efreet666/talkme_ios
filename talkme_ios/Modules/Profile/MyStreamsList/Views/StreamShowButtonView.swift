//
//  StreamShowButtonView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 07.01.2021.
//

import UIKit
import SnapKit

final class StreamShowButtonView: UIView {

    // MARK: - Public Properties

    private(set) lazy var onShowButtonTap = showButton.rx.tap

    // MARK: - Private properties

    private let showButton: UIButton = {
        let showButton = UIButton(type: .system)
        showButton.setTitleColor(TalkmeColors.white, for: .normal)
        showButton.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        showButton.layer.cornerRadius = 20
        return showButton
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func configure(viewType: StreamInfoViewType) {
        showButton.backgroundColor = .clear
        showButton.layer.borderWidth = 0
        switch viewType {
        case .myNearestStreams, .nearestSubscribedStreams, .myLiveStreams:
            setupLiveStreamButton()
            showButton.backgroundColor = TalkmeColors.greenLabels
        case .liveSubscribedStreams:
            setupLiveStreamButton()
            showButton.layer.borderWidth = 1
            showButton.layer.borderColor = TalkmeColors.white.cgColor
        case .myStreams, .subscribedStreams:
            showButton.setTitle("talkme_stream_list_see".localized, for: .normal)
            showButton.setTitleColor(TalkmeColors.shadow, for: .normal)
            showButton.layer.borderWidth = 1
            showButton.layer.borderColor = TalkmeColors.shadow.cgColor
        }
    }

    // MARK: - Private methods

    private func setupLiveStreamButton() {
        showButton.setTitle("talkme_stream_list_go_to_stream".localized, for: .normal)
        showButton.setTitleColor(TalkmeColors.white, for: .normal)
    }

    private func setupLayout() {
        addSubview(showButton)

        showButton.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
