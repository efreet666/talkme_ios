//
//  StreamPriceView.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 07.01.2021.
//

import UIKit

final class StreamPriceView: UIView {

    // MARK: - Public properties

    enum StreamPriceType {
        case moneyStream(price: String)
        case freeStream
        case bigMoneyStream(price: String)
        case bigFreeStream
        case similarLessonPrice(price: String)
        case similarLessonFree
        case allLessonsPrice(price: String)
        case allLessonsFree
        case streamFeedBalance(price: String)
    }

    // MARK: - Private Properties

    private let priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.textColor = TalkmeColors.white
        priceLabel.textAlignment = .center
        priceLabel.adjustsFontSizeToFitWidth = true
        return priceLabel
    }()

    private let coinImage: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "minicoin")
        img.contentMode = .scaleAspectFill
        return img
    }()

    private let priceStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .equalCentering
        sv.spacing = 2
        sv.alignment = .center
        return sv
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        layer.cornerRadius = UIScreen.isSE ? 12.5 : 16.5
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
    }

    func configure(viewType: StreamPriceType) {
        switch viewType {
        case .moneyStream(let price):
            priceLabel.textColor = TalkmeColors.white
            priceLabel.text = price
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
            backgroundColor = TalkmeColors.purpleColor
            coinImage.isHidden = false
        case .freeStream:
            priceLabel.textColor = TalkmeColors.greenLabels
            priceLabel.text = "talkme_free_price".localized
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
            backgroundColor = TalkmeColors.greenPriceLabel
            coinImage.isHidden = true
        case .bigMoneyStream(let price):
            priceLabel.textColor = TalkmeColors.white
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
            priceLabel.text = price
            backgroundColor = TalkmeColors.purpleColor
            coinImage.isHidden = false
        case .bigFreeStream:
            priceLabel.textColor = TalkmeColors.greenLabels
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
            priceLabel.text = "talkme_free_price".localized
            backgroundColor = TalkmeColors.greenPriceLabel
            coinImage.isHidden = true
        case .similarLessonPrice(let price):
            priceLabel.textColor = TalkmeColors.white
            priceStack.spacing = UIScreen.isSE ? 3 : 4
            priceLabel.font = .montserratSemiBold(ofSize: 13)
            priceLabel.text = price
            backgroundColor = TalkmeColors.purpleColor
            coinImage.isHidden = false
        case .similarLessonFree:
            priceLabel.textColor = TalkmeColors.greenLabels
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
            priceLabel.text = "talkme_free_price".localized
            backgroundColor = TalkmeColors.greenPriceLabel
            coinImage.isHidden = true
        case .allLessonsFree:
            priceLabel.textColor = TalkmeColors.greenLabels
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 10 : 13)
            priceLabel.text = "talkme_free_price".localized
            backgroundColor = TalkmeColors.greenPriceLabel
            coinImage.isHidden = true
        case .allLessonsPrice(let price):
            priceLabel.textColor = TalkmeColors.white
            priceStack.spacing = UIScreen.isSE ? 3 : 4
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 10 : 13)
            priceLabel.text = price
            backgroundColor = TalkmeColors.purpleColor
            coinImage.isHidden = false
        case .streamFeedBalance(let price):
            priceLabel.textColor = TalkmeColors.white
            priceStack.spacing = UIScreen.isSE ? 7 : 9
            priceLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 17 : 20)
            priceLabel.text = price
            backgroundColor = .clear
            coinImage.isHidden = false
            coinImage.image = UIImage(named: "coin")
            coinImage.contentMode = .scaleAspectFit

        }
    }

    // MARK: - Private Methods

    private func setupLayout() {
        priceStack.addArrangedSubview(priceLabel)
        priceStack.addArrangedSubview(coinImage)
        addSubview(priceStack)

        priceStack.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
    }
}
