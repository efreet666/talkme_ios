//
//  MySubscribsViewModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 31.01.2021.
//

import RxCocoa
import RxSwift
import RxDataSources

fileprivate extension Consts {
    static let numberOfLiveStreamsInPage = 50
}

final class MySubscribsViewModel: MyStreamsProtocol {

    // MARK: - Public Properties

    let navigationTitle = NavigationTitle.mySybscriptions.title
    var dataItems = BehaviorRelay<[SectionModel<String, MyFutureStreamsCellViewModel>]>(value: [])

    let bag = DisposeBag()
    var todayItems: [SectionModel<String, MyFutureStreamsCellViewModel>] = []
    var allItems: [SectionModel<String, MyFutureStreamsCellViewModel>] = []
    var flow = PublishRelay<MyStreamsFlow>()
    var onDeleteLesson = PublishRelay<Int>()

    // MARK: - Private properties

    private let service: LessonsServiceProtocol
    private var liveLessons: [MySubscriptions] = []

    // MARK: - Initializers

    init(service: LessonsServiceProtocol, profileResponse: ProfileResponse) {
        self.service = service
    }

    // MARK: - Public Methods

    func onDeleteLessonTap(id: Int) {
        service
            .lessonUnsubscribe(id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.fetchStreams()
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func fetchStreams() {
        service
            .mySubscriptions(page: 100)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.removeAllStreams()
                    self.setupLiveStreams(from: response.live)
                    self.setupLessons(from: response.data)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func сellViewModel(_ lesson: StreamInfoViewType, id: Int) -> MyFutureStreamsCellViewModel {
        let cellViewModel = MyFutureStreamsCellViewModel(myStream: lesson, id: id)
        cellViewModel
            .onDeleteBtnRelay
            .map { id }
            .bind(to: onDeleteLesson)
            .disposed(by: bag)

        cellViewModel
            .onWatchLessonInfo
            .map { .lessonInfo(id: id) }
            .bind(to: flow)
            .disposed(by: cellViewModel.bag)

        return cellViewModel
    }

    // MARK: - Private Methods

    private func removeAllStreams() {
        allItems.removeAll()
        todayItems.removeAll()
    }

    private func setupLiveStreams(from live: [MySubscriptions]?) {
        liveLessons = live ?? []
        self.setupLiveAllStreams(from: live)
        self.setupLiveTodayStreams(from: live)
    }

    private func setupLessons(from lessons: [MySubscriptions]?) {
        self.setupLessonAllStreams(from: lessons)
        self.setupLessonTodayStreams(from: lessons)
        self.dataItems.accept(allItems)
    }

    private func setupLiveAllStreams(from live: [MySubscriptions]?) {
        guard let liveStreams = live else { return }
        let itemsLive = liveStreams.map { self.liveSubscribedStreamItem(stream: $0) }
        allItems.append(.init(model: "", items: itemsLive))
    }

    private func liveSubscribedStreamItem(stream: MySubscriptions) -> MyFutureStreamsCellViewModel {
        let streamType = StreamInfoViewType.liveSubscribedStreams(
            name: stream.name,
            date: stream.date,
            teacherName: stream.owner,
            avatar: stream.tileImage)
        let item = MyFutureStreamsCellViewModel.init(
            myStream: streamType,
            id: stream.id)

        item
            .onWatchButtonTap
            .compactMap { [weak self] _ in
                guard let self = self,
                      self.liveLessons.firstIndex(where: { $0.id == stream.id }) != nil
                else { return nil }
                return .onWatch(lessonId: stream.id, numberOfLiveStreamsInPage: Consts.numberOfLiveStreamsInPage)
            }
            .bind(to: flow)
            .disposed(by: item.bag)

        return item
    }

    private func setupLessonAllStreams(from lesson: [MySubscriptions]?) {
        guard let lessons = lesson else { return }
        let items = lessons
            .map { сellViewModel(
                .subscribedStreams(
                    name: $0.name,
                    cost: $0.cost,
                    date: $0.date,
                    teacherName: $0.owner,
                    avatar: $0.tileImage),
                id: $0.id) }
        allItems.append(.init(model: "", items: items))
    }

    private func setupLiveTodayStreams(from live: [MySubscriptions]?) {
        guard let liveStreams = live else { return }
        let filteredLessonDate = liveStreams.filter { item in
            guard let responseDate = item.date else { return false }
            let calendar = Calendar.current
            let date = Date(timeIntervalSince1970: responseDate)
            return calendar.isDateInToday(date)
        }
        let itemsLiveToday = filteredLessonDate
            .map { MyFutureStreamsCellViewModel(
                myStream:
                    .liveSubscribedStreams(
                        name: $0.name,
                        date: $0.date,
                        teacherName: $0.owner,
                        avatar: $0.tileImage),
                id: $0.id) }
        todayItems.append(.init(model: "", items: itemsLiveToday))
    }

    private func setupLessonTodayStreams(from lesson: [MySubscriptions]?) {
        guard let lessons = lesson else { return }
        let filteredLessonDate = lessons.filter { item in
            guard let responseDate = item.date else { return false }
            let calendar = Calendar.current
            let date = Date(timeIntervalSince1970: responseDate)
            return calendar.isDateInToday(date)
        }
        let itemsToday = filteredLessonDate
            .map { сellViewModel(
                .subscribedStreams(
                    name: $0.name,
                    cost: $0.cost,
                    date: $0.date,
                    teacherName: $0.owner,
                    avatar: $0.tileImage),
                id: $0.id) }
        todayItems.append(.init(model: "", items: itemsToday))
    }
}
