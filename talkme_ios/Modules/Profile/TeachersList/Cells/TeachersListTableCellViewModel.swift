//
//  TeachersListTableCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.03.2021.
//

import RxSwift
import RxCocoa

final class TeachersListTableCellViewModel: TableViewCellModelProtocol {

    // MARK: Private Properties

    let lessonsModel: TeacherResponse
    let onUnsubscribeButtonRelay = PublishRelay<Void>()
    let onCellTap = PublishRelay<Void>()
    let onAvatarTap = PublishRelay<String?>()
    let onMessageButtonTap = PublishRelay<Void>()
    let bag = DisposeBag()
    let id: Int

    // MARK: - Initializers

    init(lessonsModel: TeacherResponse, id: Int) {
        self.lessonsModel = lessonsModel
        self.id = id
        bindVM(classNumber: lessonsModel.classNumber ?? "")
    }

    // MARK: - Public Methods

    func configure(_ cell: TeachersListTableCell) {
        cell.configure(model: lessonsModel)

        cell
            .onUnsubscribeButtonTap
            .bind(to: onUnsubscribeButtonRelay)
            .disposed(by: cell.bag)

        cell
            .onCellTap
            .map { _ in () }
            .bind(to: onCellTap)
            .disposed(by: cell.bag)

        cell
            .onMessageTap
            .map { _ in () }
            .bind(to: onMessageButtonTap)
            .disposed(by: cell.bag)
    }

    // MARK: - Private Methods

    private func bindVM(classNumber: String) {
        onCellTap
            .map { classNumber }
            .bind(to: onAvatarTap)
            .disposed(by: bag)
    }
}
