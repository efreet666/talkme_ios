// MARK: - Public Properties
//
//  TeachersListTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.03.2021.
//

import RxSwift
import RxCocoa
import Kingfisher

final class TeachersListTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    private(set) lazy var onUnsubscribeButtonTap = closeButton.rx.tap
    private(set) lazy var onCellTap = containerView.rx.tapGesture().when(.recognized)
    private(set) lazy var onMessageTap = mesButton.rx.tap

    // MARK: - Private Properties

    private let streamImageView: UIImageView = {
        let streamImage = UIImageView()
        streamImage.layer.cornerRadius = UIScreen.isSE ? 35 : 45
        streamImage.layer.masksToBounds = true
        streamImage.contentMode = .scaleAspectFill
        return streamImage
    }()

    private let ratingView = ImageAndTitleView()

    private let ratingContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.orangeViewBackground
        view.layer.cornerRadius = UIScreen.isSE ? 10 : 13
        return view
    }()

    private let mesButton: UIButton = {
        let mesButton = UIButton()
        mesButton.backgroundColor = TalkmeColors.blueLabels
        mesButton.setImage(UIImage(named: UIScreen.isSE ? "message" : "bigMessage"), for: .normal)
        mesButton.layer.cornerRadius = UIScreen.isSE ? 16 : 22
        mesButton.setTitle("profile_setting_message".localized, for: .normal)
        mesButton.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        mesButton.imageEdgeInsets.right = UIScreen.isSE ? 13 : 17
        mesButton.imageEdgeInsets.top = UIScreen.isSE ? 11 : 16
        mesButton.imageEdgeInsets.bottom = UIScreen.isSE ? 10 : 12
        mesButton.titleEdgeInsets.left = UIScreen.isSE ? 13 : 17
        return mesButton
    }()

    private let closeButton: UIButton = {
        let dismissButton = UIButton()
        dismissButton.backgroundColor = TalkmeColors.buttonGrayColor
        dismissButton.setImage(UIImage(named: "dismissStream"), for: .normal)
        dismissButton.layer.cornerRadius = UIScreen.isSE ? 14 : 18
        return dismissButton
    }()

    private let streamerName: UILabel = {
        let streamerName = UILabel()
        streamerName.font = .montserratBold(ofSize: UIScreen.isSE ? 14 : 17)
        streamerName.numberOfLines = 0
        streamerName.textColor = TalkmeColors.codeCountry
        return streamerName
    }()

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(model: TeacherResponse) {
        streamerName.text = "\(model.firstName) \(model.lastName)"
        ratingView.configure(.rating(model.rating))
        let noAvatarImage = UIImage(named: "noAvatar")
        guard let avatar = model.avatar,
              let url = URL(string: avatar) else {
            streamImageView.image = UIImage(named: "noAvatar")
            return
        }
        streamImageView.kf.setImage(with: url, placeholder: noAvatarImage)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.addSubviews([mesButton, streamerName, closeButton, streamImageView, ratingContainerView])
        ratingContainerView.addSubview(ratingView)

        streamerName.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 23 : 30)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 45 : 84)
            make.leading.equalTo(streamImageView.snp.trailing).inset(UIScreen.isSE ? -15 : -20)
            make.bottom.equalTo(mesButton.snp.top).inset(UIScreen.isSE ? -16 : -17)
        }
        mesButton.snp.makeConstraints { make in
            make.top.equalTo(streamerName.snp.bottom).inset(UIScreen.isSE ? 16 : 17)
            make.leading.equalTo(streamerName)
            make.height.equalTo(UIScreen.isSE ? 33 : 45)
            make.width.equalTo(UIScreen.isSE ? 136 : 178)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 13 : 20)
        }
        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 12 : 14)
        }
        closeButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 8 : 11)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 9 : 12)
            make.leading.equalTo(streamerName.snp.trailing).inset(UIScreen.isSE ? -8 : -36)
            make.size.equalTo(UIScreen.isSE ? 28 : 36)
        }
        streamImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 70 : 90)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 15)
            make.top.equalToSuperview().inset(UIScreen.isSE ? 12 : 16)
        }
        ratingContainerView.snp.makeConstraints { make in
            make.centerX.equalTo(streamImageView)
            make.width.equalTo(UIScreen.isSE ? 53 : 69)
            make.height.equalTo(UIScreen.isSE ? 22 : 27)
            make.top.equalTo(streamImageView.snp.bottom).inset(UIScreen.isSE ? 10 : 11)
        }
        ratingView.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
