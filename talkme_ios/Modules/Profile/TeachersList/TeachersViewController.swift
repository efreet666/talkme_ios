//
//  TeachersViewController.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.03.2021.
//

import RxSwift
import RxCocoa

final class TeachersViewController: UIViewController {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.keyboardDismissMode = .onDrag
        tbv.backgroundColor = TalkmeColors.buttonGrayColor
        tbv.delaysContentTouches = false
        tbv.registerCells(withModels: TeachersListTableCellViewModel.self)
        return tbv
    }()

    private let viewModel: TeachersViewModel

    // MARK: - Init

    init(viewModel: TeachersViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.buttonGrayColor
        setupTableView()
        bindVM()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
        viewModel.fetchData()
    }

    // MARK: - Private Methods

    private func setupTableView() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).inset(UIScreen.isSE ? 17 : 21)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: viewModel.bag)

        viewModel
            .onUnsubscribe
            .bind { [weak self] id in
                self?.showAlert(id: id)
            }
            .disposed(by: viewModel.bag)
    }

    private func setupNavigation() {
        customNavigationController?.style = .plainWhite(title: "profile_menu_teachers".localized)
    }

    private func showAlert(id: Int) {
        AlertManager.showUnsubscribeFromTeacherAlert(from: self) { [weak self] in
            self?.viewModel.onUnsubscribeTap(id: id)
        }
    }
}
