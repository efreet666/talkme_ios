//
//  TeachersViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 22.03.2021.
//

import RxSwift
import RxCocoa

final class TeachersViewModel {

    enum Flow {
        case onProfileSelect(classNumber: String?)
        case onMessegeButtonTap(id: Int)
    }

    // MARK: - Public Properties

    let flow = PublishRelay<Flow>()
    let dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let bag = DisposeBag()
    var onUnsubscribe = PublishRelay<Int>()

    // MARK: - Private Properties

    private let service: AccountServiceProtocol

    // MARK: - Init

    init(service: AccountServiceProtocol) {
        self.service = service
    }

    // MARK: Public Methods

    func сellViewModel(resp: TeacherResponse, id: Int) -> TeachersListTableCellViewModel {
        let cellViewModel = TeachersListTableCellViewModel(lessonsModel: resp, id: id)

        cellViewModel
            .onUnsubscribeButtonRelay
            .map { id }
            .bind(to: onUnsubscribe)
            .disposed(by: cellViewModel.bag)

        cellViewModel
            .onAvatarTap
            .map { .onProfileSelect(classNumber: $0) }
            .bind(to: flow)
            .disposed(by: cellViewModel.bag)

        cellViewModel
            .onMessageButtonTap
            .map { .onMessegeButtonTap(id: id) }
            .bind(to: flow)
            .disposed(by: cellViewModel.bag)

        return cellViewModel
    }

    private func setupLessonAllStreams(from response: MyNetworkResponse) {
        let info = response.data
        let items = info
            .map { сellViewModel(resp: $0, id: $0.id)}
        dataItems.accept(items)
    }

    func onUnsubscribeTap(id: Int) {
        service
            .removeContact(id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.fetchData()
                case .error:
                    return
                }
            }
            .disposed(by: bag)
    }

    func fetchData() {
        service.myNetwork(pageNumber: 1, pageSize: 20)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.setupLessonAllStreams(from: response)
                case .error(let error):
                    print("Get teacher list error: \(error.localizedDescription)")
                    return
                }
            }
            .disposed(by: bag)
    }
}
