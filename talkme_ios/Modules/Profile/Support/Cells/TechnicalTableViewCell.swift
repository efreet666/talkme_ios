//
//  TechnicalTableViewCell.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 22.03.2022.
//

import UIKit

class TechnicalTableViewCell: UITableViewCell {

    private let containerView: UIView = {
        let vw = UIView()
        vw.backgroundColor = .white
        vw.layer.cornerRadius = 13
        vw.clipsToBounds = false
        vw.layer.shadowColor = TalkmeColors.shadowSupport.cgColor
        vw.layer.shadowOffset = CGSize(width: 0, height: 4)
        vw.layer.shadowOpacity = 1
        vw.layer.shadowRadius = 40
        return vw
    }()

    private let titleLabel: UILabel = {
        let lb = UILabel()
        lb.text = "for_technical_questions".localized
        lb.textColor = TalkmeColors.streamPopUpGray
        lb.font = UIFont.montserratFontRegular(ofSize: UIScreen.isSE ? 17 : 20)
        lb.textAlignment = .center
        lb.numberOfLines = 0
        return lb
    }()

    private let subTitleLabel: UILabel = {
        let lb = UILabel()
        lb.text = "support@talkme.live"
        lb.textColor = TalkmeColors.blueLabels
        lb.font = UIFont.montserratBold(ofSize: UIScreen.isSE ? 16 : 18)
        lb.textAlignment = .center
        lb.numberOfLines = 0
        return lb
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        contentView.addSubviews(containerView)
        containerView.addSubviews(titleLabel, subTitleLabel)

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.top.equalToSuperview().inset(8)
        }

        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(27)
        }

        subTitleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).inset(-10)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
