//
//  SocialChatTableViewCell.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 22.03.2022.
//

import UIKit

class SocialChatTableViewCell: UITableViewCell {

    private let containerView: UIView = {
        let vw = UIView()
        vw.backgroundColor = .white
        vw.layer.cornerRadius = 13
        vw.clipsToBounds = false
        vw.layer.shadowColor = TalkmeColors.shadowSupport.cgColor
        vw.layer.shadowOffset = CGSize(width: 0, height: 4)
        vw.layer.shadowOpacity = 1
        vw.layer.shadowRadius = 40
        return vw
    }()

    private let titleLabel: UILabel = {
        let lb = UILabel()
        lb.text = "for_all_questions".localized
        lb.textColor = TalkmeColors.streamPopUpGray
        lb.font = UIFont.montserratFontRegular(ofSize: UIScreen.isSE ? 17 : 20)
        lb.textAlignment = .center
        lb.numberOfLines = 0
        return lb
    }()

    private let numberPhoneOneView = SocialSupportView(number: "+7 (915) 296-90-71")
    private let numberPhoneTwoView = SocialSupportView(number: "+7 (965) 246-22-22")

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        contentView.addSubviews(containerView)
        containerView.addSubviews(titleLabel, numberPhoneOneView, numberPhoneTwoView)

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(8)
            make.top.equalToSuperview().inset(18)
        }

        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().inset(27)
        }

        numberPhoneOneView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).inset(-21)
            make.centerX.equalToSuperview()
            make.height.equalTo(48)
            make.width.equalTo(UIScreen.isSE ? 233 : 263)
        }

        numberPhoneTwoView.snp.makeConstraints { make in
            make.top.equalTo(numberPhoneOneView.snp.bottom).inset(-11)
            make.centerX.equalToSuperview()
            make.height.equalTo(48)
            make.width.equalTo(UIScreen.isSE ? 233 : 263)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
