//
//  SocialChatTableViewModel.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 22.03.2022.
//

import RxCocoa
import RxSwift

final class SocialChatTableViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    var bag = DisposeBag()

    // MARK: - Public Method

    func configure(_ cell: SocialChatTableViewCell) {
    }
}
