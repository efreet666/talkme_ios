//
//  SupportViewController.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 22.03.2022.
//

import RxSwift

class SupportViewController: UIViewController {

    private let tableView: UITableView = {
        let tv = UITableView()
        tv.separatorStyle = .none
        tv.rowHeight = UITableView.automaticDimension
        tv.tableFooterView = UIView()
        tv.backgroundColor = .clear
        tv.registerCells(withModels:
                         SocialChatTableViewModel.self,
                         AccountingTableViewModel.self,
                         TechnicalTableViewModel.self)
        return tv
    }()

    private let bag = DisposeBag()
    private let viewModel: SupportViewModel

    init(viewModel: SupportViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.mainAccountBackground
        bindVM()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    private func bindVM() {
        viewModel.items
            .drive(tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        tableView
            .rx.setDelegate(self)
            .disposed(by: bag)
    }

    private func setupUI() {
        view.addSubviews(tableView)

        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigationBar() {
        customNavigationController?.style = .plainWhite(title: "Помощь")
    }
}

extension SupportViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 250
        } else {
            return 127
        }
    }
}
