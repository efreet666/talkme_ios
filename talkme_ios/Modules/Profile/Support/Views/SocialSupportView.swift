//
//  SocialSupportView.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 22.03.2022.
//

import UIKit

class SocialSupportView: UIView {

    // MARK: - Private properties

    private let numberPhoneLabel: UILabel = {
        let lb = UILabel()
        lb.textColor = TalkmeColors.blueLabels
        lb.font = UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 15 : 17)
        lb.textAlignment = .center
        lb.numberOfLines = 0
        return lb
    }()

    private let whatsAppImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = UIImage(named: "whatsApp")
        return img
    }()

    private let telegramImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = UIImage(named: "telegram")
        return img
    }()

    // MARK: - Initializers

    init(number: String) {
        self.numberPhoneLabel.text = number
        super.init(frame: .zero)
        setupLayout()
        setupStyle()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    // MARK: - Private methods

    private func setupStyle() {
        backgroundColor = TalkmeColors.lightBlueView
    }

    private func setupLayout() {
        addSubviews(numberPhoneLabel, whatsAppImageView, telegramImageView)

        numberPhoneLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(23)
        }

        telegramImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(21)
            make.height.width.equalTo(UIScreen.isSE ? 20 : 24)
        }

        whatsAppImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(telegramImageView.snp.leading).inset(-3)
            make.height.width.equalTo(UIScreen.isSE ? 20 : 24)
        }
    }
}
