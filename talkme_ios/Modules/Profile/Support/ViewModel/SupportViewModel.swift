//
//  SupportViewModel.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 22.03.2022.
//

import RxCocoa
import RxSwift

class SupportViewModel {

    private let socialsTableCellViewModel = SocialChatTableViewModel()
    private let accountingTableCellViewModel = AccountingTableViewModel()
    private let technicalTableCellViewModel = TechnicalTableViewModel()

    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>.just([
        socialsTableCellViewModel,
        accountingTableCellViewModel,
        technicalTableCellViewModel
    ])
}
