//
//  reloadButtomPopUp.swift
//  talkme_ios
//
//  Created by Vladislav on 12.07.2021.
//

import UIKit

enum ReloadButtonType {
    case reloadView
    case reloadTeacherView

    var cornerRadius: CGFloat {
        switch self {
        case .reloadView:
            return UIScreen.isSE ? 16 : 18
        case .reloadTeacherView:
            return UIScreen.isSE ? 16 : 18
        }
    }
}

final class ReloadButtomPopUp: UIView {

    // MARK: - Public properties

    private(set) lazy var tap = rx.tapGesture().when(.recognized)

    var fontSize: CGFloat {
        get {
            return titleLabel.font.pointSize
        }
        set {
            titleLabel.font = .montserratSemiBold(ofSize: newValue)
        }
    }

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    private let imageView: UIImageView = {
        let iw = UIImageView()
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillProportionally
        sv.spacing = UIScreen.isSE ? 6 : 8
        return sv
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
    }

    convenience init(buttonType: ReloadButtonType) {
        self.init(frame: .zero)
        buttonType == .reloadView ? setupLayoutForLearner() : setupLayoutforTeacher()
        titleLabel.text = "stream_participants_popup_reload".localized
        titleLabel.textColor = TalkmeColors.white
        imageView.image = UIImage(named: "reloadButton")
        backgroundColor = TalkmeColors.grayReloadButton
        layer.cornerRadius = buttonType.cornerRadius
    }

    // MARK: - Public Methods

    func configure(buttonType: ReloadButtonType) {
        titleLabel.text = "stream_participants_popup_reload".localized
        titleLabel.textColor = TalkmeColors.white
        imageView.image = UIImage(named: "reloadButton")
        backgroundColor = TalkmeColors.grayLabel
    }

    // MARK: - Private Methods

    private func setupLayoutForLearner() {
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
        addSubview(stackView)
            stackView.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.height.equalTo(39)
            }
        }

    private func setupLayoutforTeacher() {
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(imageView)
        addSubview(stackView)
            stackView.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.height.equalTo(39)
            }
    }

    @available(*, unavailable) required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
