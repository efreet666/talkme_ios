//
//  LessonCollectionItem.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

import UIKit
import RxSwift

final class LessonItemView: UIView {

    // MARK: - Private Properties

    private let bag = DisposeBag()

    private let backgroundImage: UIImageView = {
        let iw = UIImageView()
        iw.layer.cornerRadius = 12
        iw.contentMode = .scaleAspectFill
        iw.clipsToBounds = true
        return iw
    }()

    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = UIScreen.isSE ? 18 : 19.5
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()

    private var priceLabel = StreamPriceView()

    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 10 : 13)
        label.textAlignment = .right
        label.textColor = TalkmeColors.white
        return label
    }()

    private let lessonTimeLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 10 : 13)
        label.textAlignment = .right
        label.textColor = TalkmeColors.white
        return label
    }()

    let lessonNameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.numberOfLines = 0
        label.textColor = TalkmeColors.white
        return label
    }()

    private let gradient = CAGradientLayer(
        start: .nearestStart,
        end: .nearestEnd,
        colors: [TalkmeColors.secondMainGradient.cgColor, TalkmeColors.gradient4.cgColor],
        locations: [0.35, 0.85])

    private let startTime: UILabel = {
        let lbl = UILabel()
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        lbl.textColor = TalkmeColors.warningColor
        return lbl
    }()

    private let timerIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "clock")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = backgroundImage.bounds
    }

    func bindUI() {
        avatarImageView
            .rx
            .tapGesture()
            .when(.recognized)
            .bind {_ in
                return
            }
            .disposed(by: bag)
    }

    func configure(lessonModel: LiveStream) {
        let lessonCostIsZero = (lessonModel.cost ?? 0) == 0
        priceLabel.configure(viewType: lessonCostIsZero ? .allLessonsFree : .allLessonsPrice(price: String(lessonModel.cost ?? 0)))
        let attributedString = NSMutableAttributedString(string: lessonModel.name)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.09
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSMakeRange(0, attributedString.length))
        self.lessonNameLabel.attributedText = attributedString
        dateLabel.text = Formatters.convertDate(seconds: lessonModel.date)
        lessonTimeLabel.text = Formatters.setupHourAndMinute(date: lessonModel.lessonTime)
        let date = Date(timeIntervalSince1970: TimeInterval(lessonModel.date))
        startTime.text = Formatters.timeFormatter.string(from: date)
        guard let tileImage = lessonModel.tileImage, let url = URL(string: tileImage) else { return }
        backgroundImage.kf.setImage(with: url)
        guard let avatar = lessonModel.owner.avatarUrl, let avatarUrl = URL(string: avatar) else {
            avatarImageView.image = UIImage(named: "noAvatar")
            return
        }
        avatarImageView.kf.setImage(with: avatarUrl)
    }

    // MARK: - Private Methods

    private func initialSetup() {
        backgroundImage.addSubviews([priceLabel, lessonTimeLabel, dateLabel, lessonNameLabel, startTime, timerIcon])
        self.addSubviews(backgroundImage, avatarImageView)
        backgroundImage.layer.insertSublayer(gradient, at: 0)

        backgroundImage.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 210 : 228)
            make.width.equalTo(UIScreen.isSE ? 132 : 143)
            make.edges.equalToSuperview()
        }

        avatarImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 36 : 39)
            make.top.equalToSuperview().inset(11)
            make.leading.equalToSuperview().inset(8)
        }

        priceLabel.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 25 : 32)
            make.width.equalTo(UIScreen.isSE ? 71 : 88)
            make.bottom.equalToSuperview().inset(18)
            make.leading.equalToSuperview().inset(8)
        }

        lessonTimeLabel.snp.makeConstraints { make in
            make.bottom.equalTo(priceLabel)
            make.trailing.equalToSuperview().inset(10)
            make.leading.equalTo(priceLabel.snp.trailing).inset(8)
        }

        dateLabel.snp.makeConstraints { make in
            make.top.equalTo(priceLabel)
            make.bottom.equalTo(lessonTimeLabel.snp.top)
            make.trailing.equalToSuperview().inset(10)
            make.leading.equalTo(priceLabel.snp.trailing).inset(8)
        }

        lessonNameLabel.snp.makeConstraints { make in
            make.top.equalTo(startTime.snp.bottom).inset(UIScreen.isSE ? -6 : -10)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalTo(priceLabel.snp.top).inset(UIScreen.isSE ? -10 : -15)
        }

        startTime.snp.makeConstraints { make in
            make.centerY.equalTo(timerIcon)
            make.leading.equalTo(timerIcon.snp.trailing).offset(5)
        }

        timerIcon.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 11, height: 12))
            make.leading.equalTo(avatarImageView)
        }
    }
}
