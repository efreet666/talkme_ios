//
//  SubscribersView.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 23.03.2022.
//

import UIKit

final class SubscribersView: UIView {

    private let titleLabel: UILabel = {
        let lb = UILabel()
        lb.textColor = TalkmeColors.streamPopUpGray
        lb.font = UIFont.montserratFontRegular(ofSize: UIScreen.isSE ? 12 : 15)
        lb.textAlignment = .center
        lb.numberOfLines = 0
        return lb
    }()

    private let subTitleLabel: UILabel = {
        let lb = UILabel()
        lb.textColor = TalkmeColors.blueLabels
        lb.font = UIFont.montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        lb.textAlignment = .center
        lb.numberOfLines = 0
        return lb
    }()

    init(title: String) {
        self.titleLabel.text = title
        super.init(frame: .zero)
        styleView()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setInfo(subscribers: String) {
        subTitleLabel.text = subscribers
    }

    private func setupLayout() {
        addSubviews(titleLabel, subTitleLabel)

        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(6)
        }

        subTitleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).inset(-10)
        }
    }

    private func styleView() {
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = TalkmeColors.separatorFilter.cgColor
    }
}
