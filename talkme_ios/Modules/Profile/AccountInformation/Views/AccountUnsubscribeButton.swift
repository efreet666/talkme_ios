//
//  AccountUnsubscribeButton.swift
//  talkme_ios
//
//  Created by Майя Калицева on 14.01.2021.
//

import UIKit

enum UnsubscribeButtonType {
    case unsubscribe
    case add
    case message
    case greenAdd
    case messageTeachers
    var title: String {
        switch self {
        case .unsubscribe:
            return "profile_setting_unsubscribe".localized
        case .add, .greenAdd:
            return "profile_setting_add".localized
        case .message, .messageTeachers:
            return "profile_setting_message".localized
        }
    }

    var image: UIImage? {
        switch self {
        case .message, .messageTeachers:
            return UIImage(named: "message")
        case .unsubscribe:
            return UIImage(named: "cross")
        case .add:
            return UIImage(named: "plusSocialMedia")
        case .greenAdd:
            return UIImage(named: "plusSocialMediaWhite")
        }
    }

    var backgroundColor: UIColor {
        switch self {
        case .message, .messageTeachers:
            return TalkmeColors.blueLabels
        case .add:
            return TalkmeColors.classView
        case .unsubscribe:
            return TalkmeColors.classView
        case .greenAdd:
            return TalkmeColors.greenLabels
        }
    }

    var titleColor: UIColor {
        switch self {
        case .message, .messageTeachers:
            return TalkmeColors.white
        case .add:
            return TalkmeColors.shadow
        case .unsubscribe:
            return TalkmeColors.shadow
        case .greenAdd:
            return TalkmeColors.white
        }
    }

    var cornerRadius: CGFloat {
        switch self {
        case .message, .add, .unsubscribe, .greenAdd:
            return 20
        case .messageTeachers:
            return UIScreen.isSE ? 16 : 22
        }
    }
}

final class AccountUnsubscribeButton: UIView {

    // MARK: - Public properties

    private(set) lazy var tap = rx.tapGesture().when(.recognized)

    var fontSize: CGFloat {
        get {
            return titleLabel.font.pointSize
        }
        set {
            titleLabel.font = .montserratSemiBold(ofSize: newValue)
        }
    }

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        return label
    }()

    private let imageView: UIImageView = {
        let iw = UIImageView()
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillProportionally
        sv.spacing = UIScreen.isSE ? 12 : 16
        return sv
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
        setupLayout()
    }

    convenience init(buttonType: UnsubscribeButtonType) {
        self.init(frame: .zero)
        titleLabel.text = buttonType.title
        titleLabel.textColor = buttonType.titleColor
        imageView.image = buttonType.image
        backgroundColor = buttonType.backgroundColor
        layer.cornerRadius = buttonType.cornerRadius
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(buttonType: UnsubscribeButtonType) {
        titleLabel.text = buttonType.title
        titleLabel.textColor = buttonType.titleColor
        imageView.image = buttonType.image
        backgroundColor = buttonType.backgroundColor
    }

    // MARK: - Private Methods

    private func setupLayout() {
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
        addSubview(stackView)

        stackView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(39)
        }
    }
}
