//
//  AccountInfoViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxCocoa
import RxSwift

fileprivate extension Consts {
    static let numberOfSavedStreamsInPage = 50
}

final class AccountInfoViewModel {

    var showController = BehaviorRelay<Bool>(value: false)
    let isLoading = PublishRelay<Bool>()
    let reloadSavedStreams = PublishRelay<Void>()

    enum Flow {
        case updateMainInfo
        case watchStream(lessonId: Int, streamsList: [LiveStream], numberOfSavedStreamsInPage: Int)
        case deleteSavedStream(lessonId: Int)
        case onListOfUsersSavedLessons(userId: Int)
    }

    // MARK: - Private properties

    private let service: AccountServiceProtocol
    private var profileResponse: ProfileResponse?
    private var contactsResponse: ContactsResponse?
    private let lessonService = LessonsService()
    private var streamsList: [LiveStream] = []
    private let numberOfStreamsInPage = 50
    private var streamsOwnerId: Int = 0

    // MARK: - Public properties

    private(set) var dataItems = BehaviorRelay<[AnyTableViewCellModelProtocol]>(value: [])
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(service: AccountServiceProtocol, profileResponse: ProfileResponse? = nil, contactsResponse: ContactsResponse? = nil) {
        self.service = service
        self.profileResponse = profileResponse
        self.contactsResponse = contactsResponse
        bind()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func fetchData() {
        fetchAccountInfo()
    }

    // MARK: - Private methods

    private func setupItems(lessons: AchievementsResponse, profileResponse: ProfileResponse, publicProfileResponse: PublicProfileResponse) {
        streamsOwnerId = profileResponse.id
        let informationVM = MainInformationTableCellViewModel(profileInfo: profileResponse, lessons: lessons)
        let subscribersVM = SubscribersSubscriptionsCellViewModel(publicProfileResponse: publicProfileResponse)
        let ageAndLocationVM = AgeAndLocationTableCellViewModel(profileInfo: profileResponse, publicInfoAge: nil)
        let socialsVM = SocialsCollectionTableCellViewModel(contacts: contactsResponse)
        redirectToSocial(socialsVM)
        let aboutMeVM = AboutMeTableCellViewModel(bio: profileResponse.bio)
        let lessonSavedCellViewModel = MainPageSavedLessonTableCellViewModel(lessonService: lessonService,
                                                                             numberOfStreamsInPage: numberOfStreamsInPage,
                                                                             streamOwnersId: streamsOwnerId)
        var items: [AnyTableViewCellModelProtocol] = [
            informationVM,
            subscribersVM,
            ageAndLocationVM,
            socialsVM
        ]

        if profileResponse.bio?.isEmpty == false {
            items.append(aboutMeVM)
        }

        dataItems.accept(items)

        lessonService
            .savedStreams(withOwnersId: UserDefaultsHelper.shared.userId, page: 1, count: 50)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let responce):
                    self.streamsList = responce.data ?? []
                    lessonSavedCellViewModel.setDataModel(lessons: responce.data ?? [], andShowDeleteButton: true)
                    if responce.data?.isEmpty == false {
                        items.append(lessonSavedCellViewModel)
                    }
                    self.dataItems.accept(items)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)

        lessonSavedCellViewModel
            .onLessonDetail
            .bind { [weak self] model in
                guard let self = self else { return }
                let stream = model.currentStream
                self.flow.accept(.watchStream(lessonId: stream.id,
                                              streamsList: self.streamsList,
                                              numberOfSavedStreamsInPage: Consts.numberOfSavedStreamsInPage))
            }
            .disposed(by: bag)

        lessonSavedCellViewModel
            .showFilteredStreamsButtonTapped
            .bind { [weak self] in
                guard let self = self else { return }
                self.flow.accept(.onListOfUsersSavedLessons(userId: self.profileResponse?.id ?? 0))
            }
            .disposed(by: bag)

        lessonSavedCellViewModel
            .deleteButtonTapped
            .bind { [weak self] lessonId in
                guard let self = self else { return }
                self.flow.accept(.deleteSavedStream(lessonId: lessonId))
            }
            .disposed(by: bag)
    }

    private func fetchSubscribersAndSubscriptions(lessons: AchievementsResponse, profileResponse: ProfileResponse) {
        service.publicProfile(classNumber: String(profileResponse.id) ?? "")
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.setupItems(lessons: lessons, profileResponse: profileResponse, publicProfileResponse: response)
                case .error(let error) :
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func fetchAcheivments(profileResponse: ProfileResponse) {
        isLoading.accept(true)
        service.getAchievements()
            .subscribe { [weak self] event in

                switch event {
                case .success(let response):
                    self?.fetchSubscribersAndSubscriptions(lessons: response, profileResponse: profileResponse)
                case .error:
                    break
                }
                self?.isLoading.accept(false)
            }
            .disposed(by: bag)
    }

    private func fetchAccountInfo() {
        isLoading.accept(true)
        Single.zip(
            service.getProfileInfo(),
            service.getContacts(),
            service.getAchievements()
        )
            .subscribe { [weak self] event in

                switch event {
                case .success(let response):
                    let (profileInfo, contacts, achievments) = response
                    self?.profileResponse = profileInfo
                    self?.contactsResponse = contacts
                    self?.fetchSubscribersAndSubscriptions(lessons: achievments, profileResponse: profileInfo)
                case .error:
                    break
                }
                self?.isLoading.accept(false)
            }
            .disposed(by: bag)
    }

    private func bind() {
        flow
            .bind { [weak self] flow in
            switch flow {
            case .updateMainInfo:
                self?.fetchData()
            case .watchStream:
                return
            case .onListOfUsersSavedLessons:
                break
            case .deleteSavedStream:
                break
            }
        }
        .disposed(by: bag)

        reloadSavedStreams
            .throttle(.seconds(10), scheduler: MainScheduler.instance)
            .bind { [weak self] in
                self?.reloadSavedStreamsCell()
            }
            .disposed(by: bag)
    }

    private func reloadSavedStreamsCell() {
        var oldItems = dataItems.value

        let savedStreamsCellIndex = oldItems.firstIndex(where: { $0 is MainPageSavedLessonTableCellViewModel })
        let userId = profileResponse?.id ?? UserDefaultsHelper.shared.userId

        lessonService
            .savedStreams(withOwnersId: userId, page: 1, count: 50)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let responce):
                    self.streamsList = responce.data ?? []
                    // if we have savedStreamsCell on screen
                    if let index = savedStreamsCellIndex,
                       let savedStreamsCell = oldItems[index] as? MainPageSavedLessonTableCellViewModel {
                        // if we have deleted all streams
                        if self.streamsList.isEmpty {
                            oldItems.remove(at: index)
                        } else {
                            savedStreamsCell.setDataModel(lessons: responce.data ?? [], andShowDeleteButton: true)
                        }
                    } // if we have not savedStreamsCell on screen
                    else {
                        if !self.streamsList.isEmpty {
                            let lessonSavedCellViewModel = MainPageSavedLessonTableCellViewModel(lessonService: self.lessonService,
                                                                                                 numberOfStreamsInPage: self.numberOfStreamsInPage,
                                                                                                 streamOwnersId: self.streamsOwnerId)
                            oldItems.append(lessonSavedCellViewModel)
                            lessonSavedCellViewModel.setDataModel(lessons: responce.data ?? [], andShowDeleteButton: true)
                        }
                    }
                    self.dataItems.accept(oldItems)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func redirectToSocial(_ vm: SocialsCollectionTableCellViewModel) {
        vm.onSocialTapped
            .bind { [weak self] type in
                guard let self = self, let socials = self.contactsResponse else { return }
                switch type.0 {
                case .vk:
                    guard let url = socials.vk else { return }
                    SocialRedirectionHelper.vk.redirect(to: url)
                case .instagram:
                    guard let url = socials.instagram else { return }
                    SocialRedirectionHelper.instagram.redirect(to: url)
                case .facebook:
                    guard let url = socials.facebook else { return  }
                    SocialRedirectionHelper.facebook.redirect(to: url)
                case .telegram:
                    guard let url = socials.telegram else { return }
                    SocialRedirectionHelper.telegram.redirect(to: url)
                }
            }
            .disposed(by: vm.bag)
    }
}
