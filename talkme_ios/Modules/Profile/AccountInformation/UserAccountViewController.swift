//
//  TeacherAccount.swift
//  talkme_ios
//
//  Created by 1111 on 14.01.2021.
//

import RxSwift

final class UserAccountViewController: UIViewController {

    // MARK: - Private properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.separatorStyle = .none
        tbv.rowHeight = UITableView.automaticDimension
        tbv.tableFooterView = UIView()
        tbv.backgroundColor = .clear
        tbv.registerCells(
            withModels: MainInformationTableCellViewModel.self,
            AgeAndLocationTableCellViewModel.self,
            SubscribersTableViewModel.self,
            UnsubscribeTableCellViewModel.self,
            SocialsCollectionTableCellViewModel.self,
            AboutMeTableCellViewModel.self,
            MainPageSavedLessonTableCellViewModel.self,
            LessonsCollectionCellViewModel.self
        )
        return tbv
    }()

    private let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "talkme"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let bag = DisposeBag()
    private let viewModel: UserAccountViewModel
    private let service = MainService()

    // MARK: - Init

    init(viewModel: UserAccountViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.mainAccountBackground
        bindVM()
        setupTableView()
        viewModel.fetchData()
        viewModel.searchFetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupTableView() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBarImage")?
            .resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        navigationItem.titleView = logoImageView
    }
}
