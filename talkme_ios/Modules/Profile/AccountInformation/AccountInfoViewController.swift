//
//  AccountInfoViewController.swift
//  talkme_ios
//
//  Created by 1111 on 25.12.2020.
//

import RxSwift
import RxCocoa

final class AccountInfoViewController: UIViewController {

    // MARK: - Private properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.separatorStyle = .none
        tbv.rowHeight = UITableView.automaticDimension
        tbv.tableFooterView = UIView()
        tbv.backgroundColor = .clear
        tbv.registerCells(
            withModels: MainInformationTableCellViewModel.self,
            AgeAndLocationTableCellViewModel.self,
            SubscribersSubscriptionsCellViewModel.self,
            SocialsCollectionTableCellViewModel.self,
            MainPageSavedLessonTableCellViewModel.self,
            AboutMeTableCellViewModel.self
        )
        return tbv
    }()

    private let bag = DisposeBag()
    private let viewModel: AccountInfoViewModel

    // MARK: - Init

    init(viewModel: AccountInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.mainAccountBackground
        bindVM()
        setupLayout()
        viewModel.fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.reloadSavedStreams.accept(())
        setupNavigationBar()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel.dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        viewModel
            .isLoading
            .bind { isLoading in
                isLoading
                    ? LoaderFullViewManager.startAnimating()
                    : LoaderFullViewManager.stopAnimating()
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        view.addSubviews(tableView)

        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigationBar() {
        customNavigationController?.style = .plainWhiteWithQR(title: "Мой профиль")
    }
}
