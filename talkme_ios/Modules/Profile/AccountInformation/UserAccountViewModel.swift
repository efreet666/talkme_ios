//
//  TeacherAccountViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 14.01.2021.
//

import RxCocoa
import RxSwift

fileprivate extension Consts {
    static let numberOfSavedStreamsInPage = 50
}

final class UserAccountViewModel {

    enum Flow {
        case onLessonDetail(lessonId: Int)
        case onListOfUsersSavedLessons(userId: Int)
        case makeComplaint(Int)
        case watchStream(userId: Int, lessonId: Int, streamsList: [LiveStream], numberOfStreamsInPage: Int)
    }

    // MARK: - Public properties

    let dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()
    let onCreateChatTapped = PublishRelay<Int>()

    // MARK: - Private properties

    private let service: AccountServiceProtocol
    private let lessonService = LessonsService()
    private let classNumber: String
    private var streamsList: [LiveStream] = []
    private let numberOfStreamsInPage = 50

    // MARK: - Initializers

    init(service: AccountServiceProtocol, classNumber: String) {
        self.service = service
        self.classNumber = classNumber
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func searchFetchData() {
        service
            .publicProfile(classNumber: classNumber)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.setupItems(info: response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func fetchData() {
        service
            .streamPublicProfile(classNumber: classNumber)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.setupItems(info: response)
                case .error(let error):
                    self?.searchFetchData()
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    // MARK: - Private methods

    private func setupItems(info: PublicProfileResponse) {
        let profileInfo = ProfileResponse(model: info)
        let contacts = ContactsResponse(contacts: info)
        let achievements = AchievementsResponse(lessonPassed: info.specialistInfo.lessonPassed)
        let mainInformationCellViewModel = MainInformationTableCellViewModel(profileInfo: profileInfo, lessons: achievements)
        bindInformationVM(model: mainInformationCellViewModel, profileInfo: profileInfo)
        let ageAndLocationCellViewModel = AgeAndLocationTableCellViewModel(profileInfo: profileInfo, publicInfoAge: info.specialistInfo.age)
        let subscribersTableViewModel = SubscribersTableViewModel(model: info)
        let aboutMeTableCellViewModel = AboutMeTableCellViewModel(bio: info.specialistInfo.bio)
        let unsubscribeTableCellViewModel = UnsubscribeTableCellViewModel(isTeacher: true, isContact: info.isContact)
        let lessonsCollectionCellViewModel = LessonsCollectionCellViewModel(dataItems: info.allLessons ?? [])
        let socialsCellViewModel = SocialsCollectionTableCellViewModel(contacts: contacts)
        let lessonSavedCellViewModel = MainPageSavedLessonTableCellViewModel(lessonService: lessonService,
                                                                             numberOfStreamsInPage: numberOfStreamsInPage,
                                                                             streamOwnersId: UserDefaultsHelper.shared.userId)
        redirectToSocial(socialsCellViewModel, socials: info)

        var items: [AnyTableViewCellModelProtocol] = [
            mainInformationCellViewModel,
            subscribersTableViewModel,
            unsubscribeTableCellViewModel,
            ageAndLocationCellViewModel,
            socialsCellViewModel
        ]

        if profileInfo.bio?.isEmpty == false {
            items.append(aboutMeTableCellViewModel)
        }

        if info.allLessons?.isEmpty == false {
            lessonsCollectionCellViewModel.setDataModel(lessons: info.allLessons ?? [])
            lessonsCollectionCellViewModel
                .onLessonDetail
                .map { $0.id }
                .map { id in .onLessonDetail(lessonId: id) }
                .bind(to: flow)
                .disposed(by: lessonsCollectionCellViewModel.bag)
            items.append(lessonsCollectionCellViewModel)
        }
        dataItems.accept(items)

        guard let id = info.specialistInfo.id else { return }

        unsubscribeTableCellViewModel
            .onAddOrRemoveContactTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                if info.isContact {
                    self.removeContact(id: id)
                } else {
                    self.addContact(id: id)
                }
            }
            .disposed(by: unsubscribeTableCellViewModel.bag)

        unsubscribeTableCellViewModel
            .onCreateChatTapped
            .bind { [weak self] _ in
                self?.onCreateChatTapped.accept(id)
            }
            .disposed(by: unsubscribeTableCellViewModel.bag)

        lessonService
            .savedStreams(withOwnersId: profileInfo.id, page: 1, count: 200)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let responce):
                    self.streamsList = responce.data ?? []
                    lessonSavedCellViewModel.setDataModel(lessons: responce.data ?? [], andShowDeleteButton: false)
                    if responce.data?.isEmpty == false {
                        items.append(lessonSavedCellViewModel)
                    }
                    self.dataItems.accept(items)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)

        lessonSavedCellViewModel
            .onLessonDetail
            .bind { [weak self] model in
                guard let self = self else { return }
                let stream = model.currentStream
                self.flow.accept(.watchStream(userId: profileInfo.id,
                                              lessonId: stream.id,
                                              streamsList: self.streamsList,
                                              numberOfStreamsInPage: Consts.numberOfSavedStreamsInPage))
            }
            .disposed(by: bag)

        lessonSavedCellViewModel
            .showFilteredStreamsButtonTapped
            .bind { [weak self] in
                guard let self = self else { return }
                self.flow.accept(.onListOfUsersSavedLessons(userId: profileInfo.id))
            }
            .disposed(by: bag)
    }

    private func bindInformationVM(model: MainInformationTableCellViewModel, profileInfo: ProfileResponse) {
        model.flow
            .bind { [weak self ] _ in
                self?.flow.accept(.makeComplaint(profileInfo.id))
            }.disposed(by: model.bag)
    }

    private func addContact(id: Int) {
        service
            .addContact(id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.fetchData()
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func removeContact(id: Int) {
        service
            .removeContact(id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.fetchData()
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func redirectToSocial(_ vm: SocialsCollectionTableCellViewModel, socials: PublicProfileResponse) {
        vm.onSocialTapped
            .bind {  type in
                let socials = socials.specialistInfo
                switch type.0 {
                case .vk:
                    guard let url = socials.vk else { return }
                    SocialRedirectionHelper.vk.redirect(to: url)
                case .instagram:
                    guard let url = socials.instagram else { return }
                    SocialRedirectionHelper.instagram.redirect(to: url)
                case .facebook:
                    guard let url = socials.facebook else { return  }
                    SocialRedirectionHelper.facebook.redirect(to: url)
                case .telegram:
                    guard let url = socials.telegram else { return }
                    SocialRedirectionHelper.telegram.redirect(to: url)
                }
            }
            .disposed(by: vm.bag)
    }
}
