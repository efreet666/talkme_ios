//
//  ProfileMenuViewController.swift
//  talkme_ios
//
//  Created by 1111 on 30.12.2020.
//

import RxSwift

final class ProfileMenuViewController: UIViewController {

    // MARK: - Private properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.separatorStyle = .none
        tbv.rowHeight = UITableView.automaticDimension
        tbv.tableFooterView = UIView()
        tbv.backgroundColor = .clear
        tbv.registerCells(
            withModels: AvatarTableCellViewModel.self,
            ButtonTableCellViewModel.self,
            MenuTableViewCellViewModel.self,
            ActionsTableViewCellViewModel.self
        )
        return tbv
    }()

    private let bag = DisposeBag()
    private let viewModel: ProfileMenuViewModel

    // MARK: - Init

    init(viewModel: ProfileMenuViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.mainAccountBackground
        setupUI()
        bindVM()
        bindTableView()
        viewModel.getProfileInfo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .items
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func setupUI() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigationBar() {
        customNavigationController?.style = UserDefaultsHelper.shared.isSpecialist
            ? .gradientBlueWithQRAndClassNumber(UserDefaultsHelper.shared.classNumber)
            : .gradientBlueWithQR
    }

    private func bindTableView() {
        tableView.rx
            .modelSelected(AnyTableViewCellModelProtocol.self)
            .bind { [weak viewModel] model in
                if model is AvatarTableCellViewModel {
                    viewModel?.onAvatarCellTap()
                }
            }
            .disposed(by: bag)
    }
}
