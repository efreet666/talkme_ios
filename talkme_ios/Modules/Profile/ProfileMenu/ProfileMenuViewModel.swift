//
//  ProfileMenuViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 30.12.2020.
//

import RxCocoa
import RxSwift
import UIKit

final class ProfileMenuViewModel {

    enum Flow: Equatable {
        case setUserInfo(profileResponse: ProfileResponse, contactsResponse: ContactsResponse)
        case showPopUp(profileResponse: ProfileResponse, contactsResponse: ContactsResponse)
        case showCreateLesson(profileResponse: ProfileResponse)
        case setLive(profileResponse: ProfileResponse)
        case mySubscribes(profileResponse: ProfileResponse)
        case teachers(profileResponse: ProfileResponse)
        case myBalance
        case settings(profileResponse: ProfileResponse, contactsResponse: ContactsResponse)
        case notifications
        case exitFromScreen
        case updateMainInfo
        case support
        case exitFromProfile
        case deleteProfile
    }

    // MARK: - Private properties

    private let service: AccountServiceProtocol
    private var responses: (ProfileResponse?, ContactsResponse?)
    private var isSpecialist: Bool {
        responses.0?.isSpecialist ?? false
    }

    // MARK: - Public properties

    let profileTabImage = PublishRelay<String?>()
    private(set) lazy var items = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let classNumberRelay = BehaviorRelay<String>(value: "")
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    var classNumber = ""

    // MARK: - Initializers

    init(service: AccountServiceProtocol) {
        self.service = service
        bind()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func onAvatarCellTap() {
        guard let profileResponse = responses.0, let contactsResponse = responses.1 else { return }
        flow.accept(.setUserInfo(profileResponse: profileResponse, contactsResponse: contactsResponse ))
    }

    func getProfileInfo() {
        Single.zip(service.getProfileInfo(), service.getContacts())
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    self.responses = response
                    self.setupItems(response.0)
                    self.profileTabImage.accept(response.0.avatarUrl)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func setupItems(_ response: ProfileResponse) {

        // uncommit this to enable becomeTeacher procedure
//        let userIsTeacher2 = (response.isSpecialist ?? false) ? true : false
        let userIsTeacher = true
        let createLessonViewModel = ButtonTableCellViewModel(buttonType: userIsTeacher ? .createLesson : .becomeTeacher)
        bindCreateLessonViewModel(createLessonViewModel)

        let menuTableViewCellViewModel = MenuTableViewCellViewModel(isTeacher: true)
        bindMenuCellModel(menuTableViewCellViewModel)

        let actionsViewModel = ActionsTableViewCellViewModel()
        bindActionsViewModel(actionsViewModel)

        let items: [AnyTableViewCellModelProtocol] = [
            AvatarTableCellViewModel(userInfo: response),
            createLessonViewModel,
            menuTableViewCellViewModel,
            actionsViewModel
        ]

        self.items.accept(items)
    }

    // MARK: - Private Methods

    private func bindMenuCellModel(_ model: MenuTableViewCellViewModel) {
        guard let response = responses.0, let contactsResponse = responses.1 else { return }
        model
            .flow
            .bind { [weak self] model in
                switch model {
                case .myBroadcasts:
                    self?.flow.accept(.setLive(profileResponse: response))
                case .mySubscriptions:
                    self?.flow.accept(.mySubscribes(profileResponse: response))
                case .teachers:
                    self?.flow.accept(.teachers(profileResponse: response))
                case .myBalance:
                    self?.flow.accept(.myBalance)
                case .profileSettings:
                    self?.flow.accept(.settings(profileResponse: response, contactsResponse: contactsResponse))
                case .savedStream:
                    print("")
                case .notifications:
                    self?.flow.accept(.notifications)
                case .support:
                    self?.flow.accept(.support)
                }
            }
            .disposed(by: model.bag)
    }

    private func createClassNumberAndShowCreateLesson(profileResponse: ProfileResponse) {
        service
            .classNumberCreate()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    var updatedProfileResponse = profileResponse
                    updatedProfileResponse.numberClass = response.numberClass
                    updatedProfileResponse.isSpecialist = true
                    self?.flow.accept(.showCreateLesson(profileResponse: updatedProfileResponse))
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func bindCreateLessonViewModel(_ model: ButtonTableCellViewModel) {
        guard let response = responses.0  else { return }
        model
            .flow
            .bind { [weak self] _ in
                guard let isTeacher = self?.isSpecialist,
                let self = self
                else { return }
                if isTeacher {
                    self.flow.accept(.showCreateLesson(profileResponse: response))
                } else {
                    self.createClassNumberAndShowCreateLesson(profileResponse: response)
                }
            }
            .disposed(by: model.bag)
    }

    private func bindActionsViewModel(_ model: ActionsTableViewCellViewModel) {
        model
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .exitFromAccount:
                    self?.flow.accept(.exitFromProfile)
                case .deleteAccount:
                    self?.flow.accept(.deleteProfile)
                }
            }
            .disposed(by: model.bag)
    }

    private func bind() {
        flow
            .bind { [weak self] flow in
                switch flow {
                case .setUserInfo:
                    return
                case .showPopUp:
                    return
                case .showCreateLesson:
                    return
                case .setLive:
                    return
                case .mySubscribes:
                    return
                case .teachers:
                    return
                case .myBalance:
                    return
                case .settings:
                    return
                case .notifications:
                    return
                case .exitFromScreen:
                    return
                case .updateMainInfo:
                    self?.getProfileInfo()
                case .support:
                    return
                case .exitFromProfile:
                    return
                case .deleteProfile:
                    return
                }
            }
            .disposed(by: bag)
    }

    private func logout() {
        service.logout()
            .subscribe { event in
                switch event {
                case .success(let response):
                    print(response)
                case .error(let response):
                    print(response)
                }
            }
            .disposed(by: bag)
    }
}
