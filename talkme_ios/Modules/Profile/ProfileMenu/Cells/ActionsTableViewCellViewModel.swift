//
//  ActionsTableViewCellViewModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 13/7/2022.
//

import RxCocoa
import RxSwift

final class ActionsTableViewCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let bag = DisposeBag()
    let flow = PublishRelay<ActionsTableViewFlow>()

    func configure(_ cell: ActionsTableViewCell) {
        cell
            .flow
            .bind(to: flow)
            .disposed(by: cell.bag)
    }
}
