//
//  MenuTableViewCell.swift
//  talkme_ios
//
//  Created by 1111 on 10.01.2021.
//

import RxSwift
import RxCocoa

final class MenuTableViewCell: UITableViewCell {

    enum Flow {
        case exit
    }

    // MARK: - Public Properties

    let dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let flow = PublishRelay<ProfileMenuModel>()
    let exitFlow = PublishRelay<Flow>()
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let menuTableView: UITableView = {
        let tbv = UITableView()
        tbv.layer.cornerRadius = 12
        tbv.isScrollEnabled = false
        tbv.backgroundColor = .clear
        tbv.tableFooterView = UIView()
        tbv.separatorStyle = .singleLine
        tbv.separatorInset = .zero
        tbv.registerCells(withModels: ProfileMenuTableCellViewModel.self, ExitTableCellViewModel.self)
        return tbv
    }()

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func updateLayout(itemsCount: Int) {
        menuTableView.snp.updateConstraints { make in
            make.height.equalTo(itemsCount * 55)
        }
    }

    func bindVM() {
        dataItems
            .bind(to: menuTableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    func bindTableView() {
        menuTableView.rx
            .modelSelected(AnyTableViewCellModelProtocol.self)
            .bind { [weak self] item in
                if let profileMenuItem = item as? ProfileMenuTableCellViewModel {
                    self?.flow.accept(profileMenuItem.profileMenuModel)
                }
            }
            .disposed(by: bag)
    }

    // MARK: - Private Method

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubview(menuTableView)

        menuTableView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.top.bottom.equalToSuperview()
            make.height.equalTo(330)
        }
    }
}
