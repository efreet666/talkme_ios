//
//  AvatarTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 10.01.2021.
//

import RxSwift
import RxCocoa
import Kingfisher

final class AvatarTableCell: UITableViewCell {

    // MARK: - Private properties

    private let profileView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = 12
        return view
    }()

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: 15)
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let avatarImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "noAvatar"))
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private let accessoryImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "accessoryView"))
        return imageView
    }()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Method

    private func setupLayout() {
        profileView.addSubviews([avatarImageView, nameLabel, accessoryImageView])
        contentView.addSubview(profileView)

        profileView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(67)
            make.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(14)
        }

        avatarImageView.snp.makeConstraints { make in
            make.leading.equalTo(profileView.snp.leading).offset(14)
            make.bottom.top.equalTo(profileView).inset(9)

            make.width.equalTo(avatarImageView.snp.height)
        }

        nameLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarImageView.snp.trailing).offset(13)
            make.centerY.equalTo(avatarImageView.snp.centerY)
            make.trailing.lessThanOrEqualTo(accessoryImageView.snp.leading).offset(-10)
        }

        accessoryImageView.snp.makeConstraints { make in
            make.top.bottom.equalTo(profileView).inset(26)
            make.trailing.equalTo(profileView).inset(15)
            make.height.equalTo(15)
            make.width.equalTo(8)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    // MARK: - Public Methods

    func setUserInfo(name: String?, avatar: String?) {
        nameLabel.text = name ?? ""
        guard let avatar = avatar else { return avatarImageView.image = UIImage(named: "noAvatar") }
        let url = URL(string: avatar)
        avatarImageView.kf.setImage(with: url)
    }
}
