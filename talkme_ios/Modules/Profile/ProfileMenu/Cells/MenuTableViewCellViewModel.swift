//
//  MenuTableViewCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 10.01.2021.
//

import RxCocoa
import RxSwift

final class MenuTableViewCellViewModel: TableViewCellModelProtocol {

    enum Flow {
        case exit
    }

    // MARK: - Private properties

    private let dataModel = ProfileMenuLearnerModel()
    private let dataTeacherModel = ProfileMenuTeacherModel()
    private var dataItems: [AnyTableViewCellModelProtocol]?
//    private let exitTableCellViewModel = ExitTableCellViewModel()

    // MARK: - Public properties

    let bag = DisposeBag()
    let flow = PublishRelay<ProfileMenuModel>()
    let exitFlow = PublishRelay<Flow>()

    // MARK: Public Methods

    init(isTeacher: Bool) {
        let items: [AnyTableViewCellModelProtocol] = isTeacher
            ? dataTeacherModel.data.map { ProfileMenuTableCellViewModel(profileMenuModel: $0) }
            : dataModel.data.map { ProfileMenuTableCellViewModel(profileMenuModel: $0) }
//        items.append(exitTableCellViewModel)
        dataItems = items
    }

    func configure(_ cell: MenuTableViewCell) {
        cell
            .flow
            .bind(to: flow)
            .disposed(by: cell.bag)

//        cell
//            .exitFlow
//            .bind { [weak self] flow in
//                switch flow {
//                case .exit:
//                    AlertControllerHelper.showLogoutAlert {
//                        UserDefaultsHelper.shared.deleteTokenAndUserInfo()
//                        self?.exitFlow.accept(.exit)
//                    }
//                }
//            }
//            .disposed(by: cell.bag)

        cell.bindVM()
        cell.bindTableView()

        guard let dataItems = dataItems else { return }
        cell.dataItems.accept(dataItems)
        cell.updateLayout(itemsCount: dataItems.count)
    }
}
