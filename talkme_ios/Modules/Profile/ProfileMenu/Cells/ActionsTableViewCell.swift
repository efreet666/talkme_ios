//
//  ActionsWithProfileTableViewCell.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 13/7/2022.
//

import RxSwift
import RxCocoa
import UIKit

final class ActionsTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - outputs
    let dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let flow = PublishRelay<ActionsTableViewFlow>()

    // MARK: - Private properties

    private var appVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
    }

    private lazy var exitFromProfileButton: SettingButtonView = {
        let button =  SettingButtonView()
        button.configure(buttonType: .exitProfile)
        return button
    }()

    private lazy var deleteProfileButton: SettingButtonView = {
        let button =  SettingButtonView()
        button.configure(buttonType: .deleteProfile)
        return button
    }()

    private let talkMeLogoImageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "talkmepopup"))
        view.contentMode = .scaleAspectFit
        return view
    }()

    private lazy var appVersionLabel: UILabel = {
        let label = UILabel()
        label.text = "talkme_app_version".localized + ": \(appVersion)"
        label.font = UIFont.systemFont(ofSize: UIScreen.isSE ? 12 : 16)
        return label
    }()

    // MARK: - Life cycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier )
        cellStyle()
        setupLayout()
        bindCell()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public methods

    func bindCell() {
        exitFromProfileButton.showButton.rx
            .tap
            .bind { [weak self] _ in
                self?.flow.accept(.exitFromAccount)
            }
            .disposed(by: bag)

        deleteProfileButton.showButton.rx
            .tap
            .bind { [weak self] _ in
                self?.flow.accept(.deleteAccount)
            }
            .disposed(by: bag)
    }

    // MARK: - Private Method

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews(exitFromProfileButton,
                                talkMeLogoImageView,
                                appVersionLabel,
                                deleteProfileButton)

        exitFromProfileButton.snp.makeConstraints { make in
            make.top.equalTo(contentView).offset(12)
            make.leading.equalTo(contentView).offset(10)
            make.trailing.equalTo(contentView).offset(-10)
            make.height.equalTo(55)
        }

        talkMeLogoImageView.snp.makeConstraints { make in
            make.top.equalTo(exitFromProfileButton.snp.bottom).offset(15)
            make.centerX.equalTo(self)
            make.height.equalTo(55)
        }

        appVersionLabel.snp.makeConstraints { make in
            make.top.equalTo(talkMeLogoImageView.snp.bottom).offset(8)
            make.centerX.equalTo(self)
        }

        deleteProfileButton.snp.makeConstraints { make in
            make.top.equalTo(appVersionLabel.snp.bottom).offset(15)
            make.leading.equalTo(contentView).offset(10)
            make.trailing.equalTo(contentView).offset(-10)
            make.bottom.equalTo(contentView).offset(-20)
            make.height.equalTo(55)
        }
    }
}
