//
//  ButtonTableCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 10.01.2021.
//

import RxCocoa
import RxSwift

final class ButtonTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Private properties

    private let buttonType: SettingButtonType

    // MARK: - Public properties

    let flow = PublishRelay<Void>()
    let bag = DisposeBag()

    // MARK: Public Methods

    init(buttonType: SettingButtonType) {
        self.buttonType = buttonType
    }

    func configure(_ cell: ButtonTableCell) {
        cell.configure(for: buttonType)

        cell.onButtonTapped
            .bind(to: flow)
            .disposed(by: cell.bag)
    }
}
