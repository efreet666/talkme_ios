//
//  ActionsTableViewFlow.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 13/7/2022.
//

import Foundation

enum ActionsTableViewFlow {
    case exitFromAccount
    case deleteAccount
}
