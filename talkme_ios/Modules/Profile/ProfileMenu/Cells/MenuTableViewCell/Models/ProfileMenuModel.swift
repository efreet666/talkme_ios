//
//  profileMenuCellModel.swift
//  talkme_ios
//
//  Created by 1111 on 05.01.2021.
//

import UIKit

enum ProfileMenuModel {
    case myBroadcasts
    case mySubscriptions
    case teachers
    case myBalance
    case profileSettings
    case notifications
    case savedStream
    case support

    var icon: UIImage? {
        switch self {
        case .myBroadcasts:
            return UIImage(named: "menuLive")
        case .mySubscriptions:
            return UIImage(named: "menuSchool")
        case .teachers:
            return UIImage(named: "menuPeoples")
        case .myBalance:
            return UIImage(named: "menuBalance")
        case .profileSettings:
            return UIImage(named: "menuSettings")
        case .notifications:
            return UIImage(named: "menuBell")
        case .savedStream:
            return UIImage(named: "menuSupport")
        case .support:
            return UIImage(named: "menuSupport")
        }
    }

    var title: String {
        switch self {
        case .myBroadcasts:
            return "Мои эфиры"
        case .mySubscriptions:
            return "Избранные эфиры"
        case .teachers:
            return "Мои подписки"
        case .myBalance:
            return "Мой кошелек"
        case .profileSettings:
            return "Настройки профиля"
        case .notifications:
            return "Уведомления"
        case .savedStream:
            return "main_page_saved_lessons_streams".localized
        case .support:
            return "Помощь"
        }
    }
}
