//
//  ProfileMenuCell.swift
//  talkme_ios
//
//  Created by 1111 on 05.01.2021.
//

struct ProfileMenuLearnerModel {

    let data: [ProfileMenuModel] = [
        .mySubscriptions,
        .teachers,
        .myBalance,
        .profileSettings,
        .support
    ]
}
