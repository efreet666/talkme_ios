//
//  ProfileMenuTeacherModel.swift
//  talkme_ios
//
//  Created by 1111 on 05.01.2021.
//

struct ProfileMenuTeacherModel {

    let data: [ProfileMenuModel] = [
        .myBroadcasts,
        .mySubscriptions,
        .teachers,
        .myBalance,
        .profileSettings,
//        .savedStream,
        .support
       // .notifications
    ]
}
