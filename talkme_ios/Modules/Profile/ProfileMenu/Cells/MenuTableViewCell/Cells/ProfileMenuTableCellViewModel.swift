//
//  ProfileMenuTableCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 04.01.2021.
//

final class ProfileMenuTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let profileMenuModel: ProfileMenuModel

    // MARK: - Initializers

    init(profileMenuModel: ProfileMenuModel) {
        self.profileMenuModel = profileMenuModel
    }

    // MARK: - Public Methods

    func configure(_ cell: ProfileMenuTableCell) {
        cell.configure(profileMenuModel: profileMenuModel)
    }
}
