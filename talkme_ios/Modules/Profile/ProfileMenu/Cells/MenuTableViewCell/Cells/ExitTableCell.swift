//
//  ExitTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 08.01.2021.
//

import UIKit

final class ExitTableCell: UITableViewCell {

    // MARK: - Private Properties

    private let exitLabel: UILabel = {
        let label = UILabel()
        label.text = "Выход"
        label.textColor = TalkmeColors.redLabels
        label.font = UIFont.montserratSemiBold(ofSize: 15)
        return label
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        selectionStyle = .none
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(exitLabel)

        exitLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(15)
            make.trailing.lessThanOrEqualToSuperview().offset(-15)
            make.height.equalTo(19)
            make.bottom.top.equalToSuperview().inset(18)
        }
    }
}
