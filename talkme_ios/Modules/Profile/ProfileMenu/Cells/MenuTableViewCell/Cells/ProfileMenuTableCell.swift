//
//  ProfileMenuTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 04.01.2021.
//

import UIKit

final class ProfileMenuTableCell: UITableViewCell {

    private enum Constants {
        static let cellWidth = UIScreen.main.bounds.width
    }

    // MARK: - Private Properties

    private let menuImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let menuLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.blackLabels
        label.font = .montserratSemiBold(ofSize: 15)
        return label
    }()

    private let accessoryImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "accessoryView"))
        return imageView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
        selectionStyle = .none
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(profileMenuModel: ProfileMenuModel) {
        menuImageView.image = profileMenuModel.icon
        menuLabel.text = profileMenuModel.title
    }

    // MARK: - Private Methods

    private func setConstraints() {
        addSubviewsWithoutAutoresizing(menuImageView, menuLabel, accessoryImageView)
        menuImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(15)
            make.top.equalToSuperview().offset(15.5)
            make.bottom.equalToSuperview().offset(-15.5)
            make.height.equalTo(24)
            make.width.equalTo(menuImageView.snp.height)
        }

        menuLabel.snp.makeConstraints { make in
            make.leading.equalTo(menuImageView.snp.trailing).offset(15)
            make.trailing.lessThanOrEqualTo(accessoryImageView.snp.leading).offset(-15)
            make.height.equalTo(19)
            make.top.equalToSuperview().offset(18)
            make.bottom.equalToSuperview().offset(-18)
        }

        accessoryImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-20)
            make.trailing.equalToSuperview().inset(15)
            make.height.equalTo(15)
            make.width.equalTo(8)
        }
    }
}
