//
//  AvatarTableCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 10.01.2021.
//

import UIKit

final class AvatarTableCellViewModel: TableViewCellModelProtocol {

    // MARK: - Private properties

    private var name: String?
    private var avatar: String?

    // MARK: Public Methods

    init(userInfo: ProfileResponse) {
        name = "\(userInfo.firstName ?? "") \(userInfo.lastName ?? "")"
        avatar = userInfo.avatarUrl
    }

    func configure(_ cell: AvatarTableCell) {
        cell.setUserInfo(name: name, avatar: avatar)
    }
}
