//
//  ButtonTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 10.01.2021.
//

import RxSwift
import RxCocoa
import UIKit

final class ButtonTableCell: UITableViewCell {

    // MARK: - Public properties

    lazy var onButtonTapped = profileButton.showButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let profileButton = SettingButtonView()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(for buttonType: SettingButtonType) {
        profileButton.configure(buttonType: buttonType)
    }

    // MARK: - Private Method

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubview(profileButton)

        profileButton.snp.makeConstraints { make in
            make.width.equalToSuperview().offset(-20)
            make.top.bottom.equalToSuperview().inset(12)
            make.centerX.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 40 : 42)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
