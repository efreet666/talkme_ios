//
//  QrCodeFlow.swift
//  talkme_ios
//
//  Created by Bogdan Lesnikov on 07.01.2021.
//

enum QrCodePopupFlow {
    case share(String)
    case showScanner
    case dismiss
}

enum QrCodeScannerFlow {
    case success(qrString: String)
}
