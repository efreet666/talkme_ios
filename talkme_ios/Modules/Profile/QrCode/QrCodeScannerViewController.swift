//
//  QrCodeScannerViewController.swift
//  talkme_ios
//
//  Created by Bogdan Lesnikov on 07.01.2021.
//

import AVFoundation
import RxCocoa
import RxSwift
import UIKit

final class QrCodeScannerViewController: UIViewController {

    // MARK: - Override Properties

    override var prefersStatusBarHidden: Bool { true }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask { .portrait }

    // MARK: - Public Properties

    let flow = PublishRelay<QrCodeScannerFlow>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let blurEffectView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        let blurEffect = UIBlurEffect(style: .light)
        view.effect = blurEffect
        return view
    }()

    private let borderView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 14
        view.layer.borderWidth = 2
        view.layer.borderColor = TalkmeColors.shadow.cgColor
        return view
    }()

    private let label: UILabel = {
        let label = UILabel()
        label.text = "talkme_account_info_scan_qr".localized
        label.textColor = .white
        label.font = .montserratBold(ofSize: 17)
        label.textAlignment = .center
        return label
    }()

    private let containerView = UIView()
    private let captureSession = AVCaptureSession()
    private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)

    // MARK: - Override Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        configCaptureSession()
        setupLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !captureSession.isRunning {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if captureSession.isRunning {
            captureSession.stopRunning()
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        maskBlur()
    }

    // MARK: - Private methods

    private func configCaptureSession() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video),
              let videoInput = try? AVCaptureDeviceInput(device: videoCaptureDevice) else { return }

        guard captureSession.canAddInput(videoInput) else { return }
        captureSession.addInput(videoInput)

        let metadataOutput = AVCaptureMetadataOutput()

        guard captureSession.canAddOutput(metadataOutput) else { return }
        captureSession.addOutput(metadataOutput)

        metadataOutput.setMetadataObjectsDelegate(self, queue: .main)
        metadataOutput.metadataObjectTypes = [.qr]

        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        captureSession.startRunning()
    }

    private func setupLayout() {
        view.addSubview(containerView)
        containerView.addSubviews(blurEffectView, borderView, label)

        containerView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.trailing.bottom.equalToSuperview()
        }

        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        borderView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(35)
            make.width.equalTo(borderView.snp.height)
        }

        label.snp.makeConstraints { make in
            make.bottom.equalTo(borderView.snp.top).inset(-24)
            make.centerX.equalToSuperview()
        }
    }

    private func maskBlur() {
        view.layoutIfNeeded()
        let maskLayer = CAShapeLayer()
        let path = CGMutablePath()
        path.addRect(blurEffectView.bounds)
        path.addPath(UIBezierPath(roundedRect: borderView.frame, cornerRadius: borderView.layer.cornerRadius).cgPath)
        maskLayer.path = path
        maskLayer.fillRule = .evenOdd
        blurEffectView.layer.mask = maskLayer
    }

    private func setupNavigationBar() {
        customNavigationController?.style = .popup
    }
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate

extension QrCodeScannerViewController: AVCaptureMetadataOutputObjectsDelegate {

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        guard let metadataObject = metadataObjects.first,
              let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject,
              let stringValue = readableObject.stringValue else { return }
        flow.accept(.success(qrString: stringValue))
    }
}
