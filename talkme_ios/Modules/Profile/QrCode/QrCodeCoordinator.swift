//
//  QrCodeCoordinator.swift
//  talkme_ios
//
//  Created by Bogdan Lesnikov on 07.01.2021.
//

import RxSwift
import RxRelay

final class QrCodeCoordinator {

    private weak var navigationController: UINavigationController?
    private let accountService = AccountService()
    private let bag = DisposeBag()
    let onDismiss = PublishRelay<Void>()

    // MARK: - Public Methods

    func start(from parent: UINavigationController) {
        navigationController = parent
        let popupVC = QrCodePopupViewController()
        popupVC.modalPresentationStyle = .overFullScreen
        bindQrCodePopup(vc: popupVC)
        popupVC
            .onDismiss
            .bind(to: onDismiss)
            .disposed(by: bag)
        parent.present(popupVC, animated: false)
    }

    // MARK: - Private Methods

    private func bindQrCodePopup(vc popupVC: QrCodePopupViewController) {
        popupVC.flow
            .bind { [weak self, weak popupVC] flow in
                guard let self = self, let popupVC = popupVC else { return }
                switch flow {
                case .share(let sharedLink):
                    self.showActivityController(on: popupVC, sharedLink: sharedLink)
                case .showScanner:
                    self.showScanner(on: popupVC)
                case .dismiss:
                    popupVC.dismiss(animated: false)
                }
            }
            .disposed(by: popupVC.bag)
    }

    private func showScanner(on popupVC: QrCodePopupViewController) {
        let scannerVC = QrCodeScannerViewController()
        let nc = BaseNavigationController(rootViewController: scannerVC)
        nc.modalPresentationStyle = .fullScreen
        bindQrCodeScanner(scannerVC: scannerVC, popupVC: popupVC)
        popupVC.present(nc, animated: false)
    }

    private func showActivityController(on popupVC: QrCodePopupViewController, sharedLink: String) {
        guard let url = URL(string: sharedLink) else { return }
        let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        popupVC.present(activityViewController, animated: true, completion: nil)
    }

    private func bindQrCodeScanner(scannerVC: QrCodeScannerViewController, popupVC: QrCodePopupViewController) {
        scannerVC.flow
            .bind { [weak scannerVC, weak popupVC, weak self] flow in
                guard let scannerVC = scannerVC, let popupVC = popupVC else { return }
                switch flow {
                case .success(let stringUrl):
                    scannerVC.dismiss(animated: false, completion: popupVC.animatedDismiss)
                    guard let url = URL(string: stringUrl) else { return }
                    self?.showSpcialistScreen(url.lastPathComponent)
                }
            }
            .disposed(by: scannerVC.bag)
    }

    private func showSpcialistScreen(_ classNumber: String) {
        let model = UserAccountViewModel(service: accountService, classNumber: classNumber)
        let vc = UserAccountViewController(viewModel: model)
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
