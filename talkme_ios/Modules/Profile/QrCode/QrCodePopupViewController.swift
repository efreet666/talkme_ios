//
//  QrCodePopupViewController.swift
//  talkme_ios
//
//  Created by Bogdan Lesnikov on 07.01.2021.
//

import RxCocoa
import RxGesture
import RxSwift
import UIKit
import Kingfisher

final class QrCodePopupViewController: UIViewController {

    // MARK: - Public Properties

    let flow = PublishRelay<QrCodePopupFlow>()
    let bag = DisposeBag()
    let onDismiss = PublishRelay<Void>()

    // MARK: - Private Properties

    private let animationDuration = 0.2
    private let sharedLink = "https://\(Configuration.base)/qrcode/\(UserDefaultsHelper.shared.userId)"

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 14
        return view
    }()

    private let dismissButtonTapArea = UIView()

    private let dismissButtonImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "dismissButton"))
        iw.contentMode = .scaleAspectFill
        return iw
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "talkme_account_info_share_qr".localized
        label.numberOfLines = 2
        label.textColor = TalkmeColors.blackLabels
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 20)
        label.textAlignment = .center
        return label
    }()

    private lazy var qrCodeView = UIImageView(image: generateQRCode(from: sharedLink))
    private let shareButton = PopupActionButton(frame: .zero, type: .share)
    private let scanButton = PopupActionButton(frame: .zero, type: .scan)

    // MARK: - Override Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
        bind()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleAppear()
    }

    // MARK: - Public Methods

    func animatedDismiss() {
        UIView.animate(
            withDuration: animationDuration,
            animations: { self.view.alpha = 0 },
            completion: { [weak self] _ in
                self?.flow.accept(.dismiss)
            }
        )
    }

    // MARK: - Private Methods

    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        guard let filter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

        guard let output = filter.outputImage?.transformed(by: transform) else { return nil }
                return UIImage(ciImage: output)
    }

    private func setupView() {
        view.backgroundColor = TalkmeColors.popupBackground
        view.alpha = 0
    }

    private func handleAppear() {
        UIView.animate(
            withDuration: animationDuration,
            animations: { self.view.alpha = 1 }
        )
    }

    private func bind() {
        dismissButtonTapArea.rx.tapGesture().when(.recognized)
            .bind { [weak self] _ in
                self?.dismissButtonTapArea.isHidden = true
                self?.animatedDismiss()
                self?.onDismiss.accept(())
            }
            .disposed(by: bag)

        Observable.merge(
            shareButton.buttonTapped.map { [weak self] _ in
                .share(self?.sharedLink ?? "")
            },
            scanButton.buttonTapped.map { _ in .showScanner }
        )
            .bind(to: flow)
            .disposed(by: bag)
    }

    private func setupLayout() {
        view.addSubviews(containerView, dismissButtonTapArea)
        dismissButtonTapArea.addSubview(dismissButtonImageView)
        containerView.addSubviews(titleLabel, qrCodeView, shareButton, scanButton)

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 35 : 28)
            make.centerY.equalToSuperview()
        }

        dismissButtonTapArea.snp.makeConstraints { make in
            make.size.equalTo(44)
            make.trailing.equalTo(-20)
            make.bottom.equalTo(containerView.snp.top).offset(4)
        }

        dismissButtonImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? 14 : 20)
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 39 : 62)
            make.top.equalToSuperview().offset(UIScreen.isSE ? 31 : 41)
        }

        qrCodeView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 5 : 15)
            make.centerX.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? 155 : 215)
        }

        shareButton.snp.makeConstraints { make in
            make.top.equalTo(qrCodeView.snp.bottom).offset(UIScreen.isSE ? 19 : 15)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 30 : 42)
            make.height.equalTo(UIScreen.isSE ? 39 : 56)
        }

        scanButton.snp.makeConstraints { make in
            make.top.equalTo(shareButton.snp.bottom).offset(UIScreen.isSE ? 11 : 16)
            make.height.equalTo(shareButton)
            make.leading.trailing.equalTo(shareButton)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 28 : 45)
        }
    }
}
