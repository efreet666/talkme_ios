//
//  File.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 04.05.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceRefillCollectionCell: UICollectionViewCell {

    // MARK: - Private Properties

    override var isSelected: Bool {
        didSet {
            containerView.backgroundColor = isSelected ? TalkmeColors.blackLabels : .white
            nameLabel.textColor = isSelected ? .white : TalkmeColors.blackLabels
        }
    }

    private let containerView = UIView()
    private let icon = UIImageView()
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .leading
        return stackView
    }()

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let coinsButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "coinWithShadow"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.semanticContentAttribute = UIApplication.shared
              .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        button.setTitleColor(TalkmeColors.blueCollectionCell, for: .normal)
        button.titleLabel?.font = .montserratBold(ofSize: 15)
        button.isUserInteractionEnabled = false
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        return button
    }()

    private let priceButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = UIScreen.isSE ? 10 : 13
        button.backgroundColor = TalkmeColors.blueLabels
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        button.contentEdgeInsets = .init(top: 0, left: 6, bottom: 0, right: 6)
        button.isUserInteractionEnabled = false
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        return button
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(with model: GetPlansResponse) {
        icon.kf.setImage(with: URL(string: model.image))
        nameLabel.text = model.title
        coinsButton.setTitle(model.iosCoinAmount?.description, for: .normal)
        guard let purchaseId = model.iosInAppId else { return }
        let priceText = IAPManager.shared.price(for: String(purchaseId))
        priceButton.setTitle(priceText, for: .normal)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.addSubview(icon)
        containerView.addSubview(stackView)
        stackView.addArrangedSubviews([nameLabel, coinsButton, priceButton])

        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 90 : 119)
        }

        icon.snp.makeConstraints { make in
            make.centerY.equalTo(containerView.snp.centerY)
            make.leading.equalToSuperview().inset(12)
            make.trailing.equalTo(stackView.snp.leading).inset(-11)
            make.width.equalTo(UIScreen.isSE ? 50 : 65)
            make.height.equalTo(UIScreen.isSE ? 53 : 68)
        }

        stackView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(12)
            make.trailing.equalToSuperview()
        }

        priceButton.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 19 : 24)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear

        containerView.layer.cornerRadius = 8
        containerView.backgroundColor = .white
    }
}
