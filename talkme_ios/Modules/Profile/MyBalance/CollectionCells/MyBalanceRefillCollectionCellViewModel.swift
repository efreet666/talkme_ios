//
//  MyBalanceRefillVariantCollectionCellViewModel.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 04.05.2021.
//

import UIKit
import RxSwift
import RxCocoa

final class MyBalanceRefillCollectionCellViewModel: CollectionViewCellModelProtocol {

    // MARK: - Public properties

    let type: GetPlansResponse
    let bag = DisposeBag()

    // MARK: - Initializers

    init(type: GetPlansResponse) {
        self.type = type
    }

    // MARK: - Public methods

    func configure(_ cell: MyBalanceRefillCollectionCell) {
        cell.configure(with: type)
    }
}
