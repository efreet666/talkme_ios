//
//  MyBalanceSumOfPaymentCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 31.03.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceSumOfPaymentCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var sumOfPaymentTextRelay = sumOfPaymentTextField.textRelay
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let sumOfPaymentTextField = PaymentEnterDataView(placeholder: "100 ₽", formatPattern: "")

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.codeCountry
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 13 : 17)
        return lbl
    }()

    private let minumumSumLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.placeholderColor
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 13)
        lbl.text = "my_balance_min_sum_of_payment".localized
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    func configure(title: String) {
        titleLabel.text = title
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, sumOfPaymentTextField, minumumSumLabel])

        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(sumOfPaymentTextField)
            make.trailing.top.equalToSuperview()
            make.bottom.equalTo(sumOfPaymentTextField.snp.top).inset(UIScreen.isSE ? -6 : -13)
        }
        sumOfPaymentTextField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(UIScreen.isSE ? 36 : 46)
            make.bottom.equalTo(minumumSumLabel.snp.top).inset(UIScreen.isSE ? -7 : -9)
        }
        minumumSumLabel.snp.makeConstraints { make in
            make.top.equalTo(sumOfPaymentTextField.snp.bottom).inset(UIScreen.isSE ? 7 : 9)
            make.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(15)
        }
    }
}
