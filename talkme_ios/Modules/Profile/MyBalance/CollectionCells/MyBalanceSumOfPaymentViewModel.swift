//
//  MyBalanceSumOfPaymentViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 31.03.2021.
//

import RxCocoa
import RxSwift

final class MyBalanceSumOfPaymentViewModel: TableViewCellModelProtocol {

    // MARK: Public properties

    let sumOfPayTextRelay = PublishRelay<String?>()
    let bag = DisposeBag()

    // MARK: Private properties

    private let title: String

    // MARK: Init

    init(title: String) {
        self.title = title
    }

    // MARK: - Public Methods

    func configure(_ cell: MyBalanceSumOfPaymentCell) {
        cell.configure(title: title)

        cell
            .sumOfPaymentTextRelay
            .bind(to: sumOfPayTextRelay)
            .disposed(by: cell.bag)
    }
}
