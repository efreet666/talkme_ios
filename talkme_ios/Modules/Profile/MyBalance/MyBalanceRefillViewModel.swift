//
//  MyBalanceViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 26.03.2021.
//

import RxSwift
import RxCocoa
import RxDataSources
import CollectionKit
import Foundation

final class MyBalanceRefillViewModel {

    // MARK: - Public Properties
    private(set) var minimumCoins: Float = 3000
    let onButtonTapped = PublishRelay<Void>()
    let dataItems = BehaviorRelay<[SectionModel<String, AnyTableViewCellModelProtocol>]>(value: [])
    let transactionItems = PublishRelay<[MyBalanceTransactionType]>()
    let selectedVariant = PublishRelay<Int>()
    let onSegmentTap = PublishRelay<Int>()
    let bag = DisposeBag()
    var historyBag = DisposeBag()
    var anotherBag = DisposeBag()
    lazy var sectionItems: [SectionModel<String, AnyTableViewCellModelProtocol>] = []
    lazy var sectionItemsTransaction: [SectionModel<String, AnyTableViewCellModelProtocol>] = [SectionModel(model: "", items: [])]
    var coinProfile: Float = 0
    var koef: Float = 0
    var coinToDiamond: Float = 0

    // MARK: - Private Properties

    private let paymentsService: PaymentsServiceProtocol
    private var timer: Timer?
    private let myBalanceViewModel = MyTotalBalanceCellViewModel()
    private let balanceRefillsViewModel = MyBalanceRefillsCellViewModel()
    private let myBalanceOverallOutputResultCellViewModel = MyBalanceOverallOutputResultCellVM()
    private let withdrawButtonCellViewModel = WithdrawButtonCellViewModel()
    private let payButtonCellViewModel = ConfirmButtonCellViewModel(title: "stream_feed_buy".localized, isEnabled: false)
    private var payItems: [AnyTableViewCellModelProtocol] = []

    private let iapManager = IAPManager.shared
    private var selectedId = 0
    private let segmentControl = MyBalanceSegmentView()

    // MARK: - Initializers

    init(paymentsService: PaymentsServiceProtocol) {
        self.paymentsService = paymentsService
        bindViewModel()
        setupSegmentTap()
    }

    // MARK: - Public Methods

    func setUpItems() {
        self.getCurrentBalance()
        self.sectionItems = [
            SectionModel(
                model: "",
                items: [
                    self.myBalanceViewModel,
                    self.balanceRefillsViewModel,
                    self.payButtonCellViewModel
                ])]
        self.dataItems.accept(self.sectionItems)
        self.setUpPurchasesCollectionItems()
    }

    func setUpItemsToTransaction() {
        self.getCurrentBalance()
        self.dataItems.accept(self.sectionItemsTransaction)
        self.setUpPurchasesCollectionItems()
    }

    func fetchData() {
        paymentsService
            .budgetHistory()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    var historyItems: [MyBalanceTransactionType] = []
                    for item in response {
                        if item.flag {
                            let replenishment = MyBalanceTransactionType.replenishment(
                                description: item.description ?? "",
                                cash: item.cash,
                                date: Date(timeIntervalSince1970: TimeInterval(Double(item.date))))
                                historyItems.append(replenishment)
                        } else {
                            let withDrawal = MyBalanceTransactionType.withdrawal(
                                description: item.description ?? "",
                                cash: item.cash,
                                date: Date(timeIntervalSince1970: TimeInterval(Double(item.date))))
                            historyItems.append(withDrawal)
                        }
                    }
                    self.transactionItems.accept(historyItems)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func fetchKoef() {
        Single.zip(paymentsService.getPaymentsKoef(), paymentsService.getCoin())
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success((let responsePaymentsKoef, let responseCoin)):
                    self.minimumCoins = Float(responsePaymentsKoef.first(where: { $0.key == "min_withdrawal_coin" })?.value ?? "3000") ?? 0
                    let diamond = responsePaymentsKoef.first(where: { $0.key == "diamond_to_rub_coef" })?.value ?? "0"
                    let coin = responsePaymentsKoef.first(where: { $0.key == "coin_to_diamond_coef" })?.value ?? "0"
                    self.coinToDiamond = Float(coin) ?? 0
                    let myBalanceWithdrawalOfFundsCellVM =  MyBalanceWithdrawalOfFundsCellVM(
                        title: "payments_withdrawal_count".localized,
                        koef: Float(coin) ?? 0
                    )
                    myBalanceWithdrawalOfFundsCellVM.maxCoin.accept(responseCoin.coin)
                    let myBalanceWithdrawalOfDiamondsCellVM =  MyBalanceWithdrawalOfDiamondsCellVM(Float(diamond) ?? 0)

                    myBalanceWithdrawalOfFundsCellVM.isMoreThanMinRelay
                        .bind(to: self.withdrawButtonCellViewModel.isMoreThanMinimum)
                        .disposed(by: self.withdrawButtonCellViewModel.bag)
                    self.sectionItemsTransaction = [
                        SectionModel(
                            model: "",
                            items: [
                                self.myBalanceViewModel,
                                myBalanceWithdrawalOfFundsCellVM,
                                myBalanceWithdrawalOfDiamondsCellVM,
                                self.myBalanceOverallOutputResultCellViewModel,
                                self.withdrawButtonCellViewModel
                            ])]
                    self.koef = Float(diamond) ?? 0
                    self.bindWithDravalCells(coinsCell: myBalanceWithdrawalOfFundsCellVM, diamondsCell: myBalanceWithdrawalOfDiamondsCellVM)
                case .error(let error):
                    print("Get payments koef error: \(error)")
                }
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func setupTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] timer in
            guard let self = self else {
                timer.invalidate()
                return
            }
            self.getCurrentBalance()
        }
    }

    func setupSegmentTap() {
        onSegmentTap.bind { [weak self] value in
            if value == 0 {
                self?.bindViewModel()
            } else if value == 1 {
                self?.payButtonCellViewModel.isEnabledRelay.accept(false)
                self?.bindHistoryItems()
            } else if value == 2 {
                self?.payButtonCellViewModel.isEnabledRelay.accept(false)
                self?.bindHistoryItems()
            }
        }
        .disposed(by: bag)
    }

    private func bindHistoryItems() {
        transactionItems
            .map { (transactions) -> [SectionModel<String, AnyTableViewCellModelProtocol>] in
                var sectionItems: [SectionModel<String, AnyTableViewCellModelProtocol>] = [SectionModel(model: "", items: [self.myBalanceViewModel])]
                let groups = Dictionary(grouping: transactions, by: { $0.date.beginningOfDay })
                    .sorted { $0.key > $1.key }
                sectionItems.append(contentsOf: groups.map {
                    let converter =  Formatters.detailedLessonFormatter.string(from: $0.key)
                    let items = $0.value.map {
                        MyBalanceTransactionCellViewModel.init($0) }
                    return SectionModel(model: converter, items: items)
                })
                return sectionItems
            }
            .bind(to: dataItems)
            .disposed(by: bag)
    }

    private func bindViewModel() {
        withdrawButtonCellViewModel
            .onButtonTapped
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.myBalanceOverallOutputResultCellViewModel.warningRelay.accept(self.coinProfile < self.minimumCoins)
                self.onButtonTapped.accept(())
            }
            .disposed(by: withdrawButtonCellViewModel.bag)

        balanceRefillsViewModel
            .selectedItem
            .bind(to: selectedVariant)
            .disposed(by: balanceRefillsViewModel.bag)

        selectedVariant
            .bind { [weak self] id in
                self?.payButtonCellViewModel.isEnabledRelay.accept(true)
                self?.selectedId = id
            }
            .disposed(by: bag)

        payButtonCellViewModel
            .tapRelay
            .bind { [weak self] _ in
                guard let self = self else { return }
                let productId = String(self.selectedId)
                self.iapManager.purchase(productWith: productId)
            }
            .disposed(by: payButtonCellViewModel.bag)

        iapManager
            .isTapped
            .bind { [weak self] _ in
                self?.payButtonCellViewModel.isHideButtonRelay.accept(())
            }
            .disposed(by: iapManager.bag)

        iapManager
            .setupTimer
            .bind { [weak self] _ in
                self?.setupTimer()
            }
            .disposed(by: iapManager.bag)
    }

    private func setUpPurchasesCollectionItems() {
        let refillItems = iapManager.purchases.map { MyBalanceRefillCollectionCellViewModel(type: $0) }
        balanceRefillsViewModel.items.accept(refillItems)
    }

    private func getCurrentBalance() {
        paymentsService
            .getCoin()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.myBalanceViewModel.setPrice(price: response.coin)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func bindWithDravalCells(coinsCell: MyBalanceWithdrawalOfFundsCellVM, diamondsCell: MyBalanceWithdrawalOfDiamondsCellVM) {
        coinsCell
            .textFieldText
            .subscribe(onNext: { [weak diamondsCell, weak self] text in
                guard let diamondsCell = diamondsCell, let self = self else { return }
                let diamondCellCoin = "\(Int((Float(text ?? "") ?? 0) * self.coinToDiamond))"
                diamondsCell.inputTextFieldText.onNext(diamondCellCoin)
                guard let text = text, let price = Float(text), price >= 0 else {
                    self.myBalanceOverallOutputResultCellViewModel.priceRelay.accept(0)
                    self.coinProfile = 0
                    return }
                self.coinProfile = price
                let outputCoin = Int((Float(diamondCellCoin) ?? 0) * self.koef)
                self.myBalanceOverallOutputResultCellViewModel.priceRelay.accept(outputCoin)

                self.withdrawButtonCellViewModel
                    .onButtonTapped
                    .bind { MyBalanceRefillViewController.summ = String(
                        describing: self.myBalanceOverallOutputResultCellViewModel.priceRelay.value
                    ) }
                    .disposed(by: self.withdrawButtonCellViewModel.bag)
            })
            .disposed(by: coinsCell.bag)
    }
}
