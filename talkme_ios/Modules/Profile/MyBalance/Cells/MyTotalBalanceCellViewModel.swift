//
//  MyTotalBalanceCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 31.03.2021.
//

import RxSwift
import RxCocoa

final class MyTotalBalanceCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let priceRelay = BehaviorRelay<Int>(value: 0)
    let bag = DisposeBag()

    // MARK: - Public methods

    func setPrice(price: Int) {
        priceRelay.accept(price)
    }

    func configure(_ cell: MyTotalBalanceCell) {
        priceRelay
            .bind { [weak cell] number in
                cell?.setPrice(price: number)
            }
            .disposed(by: cell.bag)
    }
}
