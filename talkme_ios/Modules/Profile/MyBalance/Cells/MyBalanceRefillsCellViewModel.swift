//
//  MyBalanceRefillsCellViewModel.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 04.05.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceRefillsCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties
    let onButtonTapped = PublishRelay<Void>()
    let items = BehaviorRelay<[MyBalanceRefillCollectionCellViewModel]>(value: [])
    let selectedItem = PublishRelay<Int>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init() {

    }

    // MARK: - Public Methods

    func configure(_ cell: MyBalanceRefillsCell) {
        cell.configure()

        cell.selectedItem
            .bind { [weak self] item in
                guard let selectedItem = item else { return }
                self?.selectedItem.accept(selectedItem)
            }
            .disposed(by: cell.bag)

        items
            .bind(to: cell.items)
            .disposed(by: cell.bag)
    }
}
