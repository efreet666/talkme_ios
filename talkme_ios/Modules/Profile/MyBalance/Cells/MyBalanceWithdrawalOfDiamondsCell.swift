//
//  MyBalanceWithdrawalOfDiamondsCell.swift
//  talkme_ios
//
//  Created by Vladislav on 23.07.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceWithdrawalOfDiamondsCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let briliantImage = UIImageView(image: UIImage(named: "briliant"))

    private let withdrawalRateBriliantLabel: WithdrawalTitleAndImageView = {
        let view = WithdrawalTitleAndImageView()
        view.configure(WithdrawalRateLabelType.briliant("1"))
        return view
    }()

    private let withdrawalRateRublesLabel: WithdrawalTitleAndImageView = {
        let view = WithdrawalTitleAndImageView()
        let count = 0.86
        view.configure(WithdrawalRateLabelType.rubles("= \(count)"))
        return view
    }()

    private let myBalanceBriliantTF: MyBalanceBriliantTextField = {
        let view = MyBalanceBriliantTextField()
        view.isUserInteractionEnabled = false
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(koef: Float) {
        withdrawalRateRublesLabel.configure(.rubles("= \(koef)"))
    }

    func setText(text: String?) {
        myBalanceBriliantTF.text = text
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubviews([myBalanceBriliantTF, withdrawalRateBriliantLabel, withdrawalRateRublesLabel])
        myBalanceBriliantTF.addSubview(briliantImage)

        withdrawalRateBriliantLabel.snp.makeConstraints { label in
            label.top.equalTo(myBalanceBriliantTF.snp.bottom).offset(5)
            label.trailing.equalTo(withdrawalRateRublesLabel.snp.leading).offset(UIScreen.isSE ? -2 : -3)
            label.height.equalTo(15)
            label.bottom.equalToSuperview()
        }

        withdrawalRateRublesLabel.snp.makeConstraints { label in
            label.trailing.equalToSuperview().inset(10)
            label.height.equalTo(13)
            label.bottom.equalToSuperview()
        }

        myBalanceBriliantTF.snp.makeConstraints { textView in
            textView.top.equalToSuperview().offset(UIScreen.isSE ? 21 : 22)
            textView.leading.trailing.equalToSuperview().inset(10)
            textView.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        briliantImage.snp.makeConstraints { image in
            image.centerY.equalTo(myBalanceBriliantTF.snp.centerY)
            image.leading.equalTo(myBalanceBriliantTF).inset(UIScreen.isSE ? 12 : 16)
            image.height.equalTo(UIScreen.isSE ? 16 : 20)
            image.width.equalTo(UIScreen.isSE ? 21 : 27)
        }

    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
