//
//  MyBalanceWithdrawalOfFunds.swift
//  talkme_ios
//
//  Created by Vladislav on 21.07.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceWithdrawalOfFundsCell: UITableViewCell {

    // MARK: - Public Properties

    let isMoreThanMinRelay =  BehaviorRelay<Bool>(value: false)

    // MARK: - Private Properties

    private var maxValue: Int = 0
    private lazy var valueRange = 0...maxValue
    private let coinImage = UIImageView(image: UIImage(named: "coin"))
    private(set) var bag = DisposeBag()
    private(set) lazy var textFieldText = myBalanceCoinTF.rx.text

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.codeCountry
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 15 : 17)
        return lbl
    }()

    private let withdrawalAmountLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 13)
        lbl.text = "my_balance_withdrawal_amount".localized
        return lbl
    }()

    private let withdrawalRateCoinLabel: WithdrawalTitleAndImageView = {
        let view = WithdrawalTitleAndImageView()
        view.configure(WithdrawalRateLabelType.coin(1))
        return view
    }()
    private let withdrawalRateBriliantLabel: WithdrawalTitleAndImageView = {
        let view = WithdrawalTitleAndImageView()
        let rate: Double = 0.70
        let stringRate = "= \(rate)"
        view.configure(WithdrawalRateLabelType.briliant(stringRate))
        return view
    }()

    private let myBalanceCoinTF = MyBalanceCoinTextField()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        myBalanceCoinTF.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, koef: Float) {
        titleLabel.text = title
        withdrawalRateBriliantLabel.configure(.briliant("= \(koef)"))
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    func setMaxValue(_ value: Int) {
        maxValue = value
        valueRange = 0...maxValue
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, myBalanceCoinTF, withdrawalAmountLabel, withdrawalRateCoinLabel, withdrawalRateBriliantLabel])
        myBalanceCoinTF.addSubview(coinImage)

        titleLabel.snp.makeConstraints { label in
            label.top.equalToSuperview()
            label.leading.equalToSuperview().inset(15)
        }

        withdrawalAmountLabel.snp.makeConstraints { label in
            label.top.equalTo(myBalanceCoinTF.snp.bottom).offset(5)
            label.leading.equalToSuperview().inset(15)
            label.bottom.equalToSuperview()
        }

        withdrawalRateCoinLabel.snp.makeConstraints { label in
            label.top.equalTo(myBalanceCoinTF.snp.bottom).offset(5)
            label.trailing.equalTo(withdrawalRateBriliantLabel.snp.leading).offset(UIScreen.isSE ? -2 : -3)
            label.height.equalTo(UIScreen.isSE ? 13 : 16)
            label.bottom.equalToSuperview()
        }

        withdrawalRateBriliantLabel.snp.makeConstraints { label in
            label.top.equalTo(myBalanceCoinTF.snp.bottom).offset(5)
            label.trailing.equalToSuperview().inset(10)
            label.bottom.equalToSuperview()
        }

        myBalanceCoinTF.snp.makeConstraints { textView in
            textView.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 11)
            textView.leading.trailing.equalToSuperview().inset(10)
            textView.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        coinImage.snp.makeConstraints { image in
            image.centerY.equalTo(myBalanceCoinTF.snp.centerY)
            image.leading.equalTo(myBalanceCoinTF).inset(UIScreen.isSE ? 15 : 20)
            image.size.equalTo(UIScreen.isSE ? 15 : 19)
        }

    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension MyBalanceWithdrawalOfFundsCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)

        let overflowCheck = Double(newText) ?? 0
        if Int(overflowCheck) >= Int(Int.max / 100) {
            return false
        }

        guard !newText.isEmpty else { return true }
        if Int(newText) ?? 0 > maxValue {
            myBalanceCoinTF.text = textField.text!
            isMoreThanMinRelay.accept(false)
            return true
        }
        isMoreThanMinRelay.accept(Int(newText) ?? 0 >= 3000)
        return valueRange.contains(Int(newText) ?? 0)
    }
}
