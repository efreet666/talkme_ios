//
//  MyBalanceOverallOutputResultViewModel.swift
//  talkme_ios
//
//  Created by Vladislav on 23.07.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceOverallOutputResultCellVM: TableViewCellModelProtocol {

    // MARK: - Public properties

    let warningRelay = PublishRelay<Bool>()
    let priceRelay = BehaviorRelay<Int>(value: 0)
    let bag = DisposeBag()

    // MARK: - Public methods

    func setPrice(price: Int) {
        priceRelay.accept(price)
    }

    func configure(_ cell: MyBalanceOverallOutputResultCell) {
        warningRelay
            .bind { [weak cell] isHidden in
// TODO: - ошибка о недостаточных количествах средств для минимального вывода
//                cell?.setWarningText(isHidden: !isHidden)
            }
            .disposed(by: cell.bag)

        priceRelay
            .bind { [weak cell] number in
                cell?.setPrice(price: number)
            }
            .disposed(by: cell.bag)
    }
}
