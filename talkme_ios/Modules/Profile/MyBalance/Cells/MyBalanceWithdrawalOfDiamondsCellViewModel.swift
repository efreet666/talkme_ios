//
//  MyBalanceWithdrawalOfDiamondsCellViewModel.swift
//  talkme_ios
//
//  Created by Vladislav on 23.07.2021.
//

import RxSwift

final class MyBalanceWithdrawalOfDiamondsCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let inputTextFieldText = PublishSubject<String?>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let koef: Float

    // MARK: - Initializers

    init(_ koef: Float) {
        self.koef = koef
    }

    // MARK: - Public Methods

    func configure(_ cell: MyBalanceWithdrawalOfDiamondsCell) {
        cell.configure(koef: koef)

        inputTextFieldText
            .subscribe { [weak cell] text in
                cell?.setText(text: text)
            }
            .disposed(by: cell.bag)
    }
}
