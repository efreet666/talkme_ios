//
//  MyBalanceTransactionTableCellViewModel.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 06.05.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceTransactionCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let transaction: MyBalanceTransactionType

    // MARK: - Initializers

    init(_ type: MyBalanceTransactionType) {
        transaction = type
    }

    // MARK: - Public Methods

    func configure(_ cell: MyBalanceTransactionCell) {
        cell.configure(transaction)
    }

}
