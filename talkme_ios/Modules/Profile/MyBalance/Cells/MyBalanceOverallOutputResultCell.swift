//
//  MyBalanceOverallOutputResultCell.swift
//  talkme_ios
//
//  Created by Vladislav on 23.07.2021.
//

import RxSwift
import UIKit

final class MyBalanceOverallOutputResultCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let containerView = MyBalanceResultView()

    private let warningLabel: UILabel = {
        let lb = UILabel()
        lb.textColor = TalkmeColors.warningColor
        lb.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        lb.isHidden = true
        lb.textAlignment = .center
        return lb
    }()

    private let coinImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = UIImage(named: "bigBalanceCoin")
        img.isHidden = true
        return img
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setPrice(price: Int) {
        containerView.configure(price: String(price))
    }

    func setWarningText(isHidden: Bool) {
        coinImageView.isHidden = isHidden
        warningLabel.isHidden = isHidden
        warningLabel.text = "minimum_withdrawal_amount".localized
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(containerView)
        contentView.addSubview(warningLabel)
        contentView.addSubview(coinImageView)

        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 22 : 26)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 77 : 88)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 44 : 90)
            make.height.equalTo(UIScreen.isSE ? 94 : 122)
        }

        warningLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(14)
            make.centerX.equalToSuperview()
        }

        coinImageView.snp.makeConstraints { make in
            make.leading.equalTo(warningLabel.snp.trailing).inset(-10)
            make.centerY.equalTo(warningLabel)
            make.height.width.equalTo(18.5)
        }
    }

    override func prepareForReuse() {
        bag = .init()
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
