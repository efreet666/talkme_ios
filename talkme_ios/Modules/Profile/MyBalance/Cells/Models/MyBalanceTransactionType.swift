//
//  MyBalanceTransactionType.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 06.05.2021.
//

import UIKit

enum MyBalanceTransactionType {
    case replenishment(description: String, cash: Int, date: Date)
    case withdrawal(description: String, cash: Int, date: Date)

    // MARK: - Public Properties

    var description: String {
        switch self {
        case .replenishment(let description, _, _), .withdrawal(let description, _, _):
            return description
        }
    }

    var cash: Int {
        switch self {
        case .replenishment(_, let cash, _), .withdrawal(_, let cash, _):
            return cash
        }
    }

    var icon: UIImage? {
        switch self {
        case .replenishment:
            return UIImage(named: UIScreen.isSE ? "greenRightArrow" : "bigGreenRightArrow")
        case .withdrawal:
            return UIImage(named: UIScreen.isSE ? "redLeftArrow" : "bigRedLeftArrow")
        }
    }

    var color: UIColor {
        switch self {
        case .replenishment:
            return TalkmeColors.greenLabels
        case .withdrawal:
            return TalkmeColors.warningColor
        }
    }

    var date: Date {
        switch self {
        case .replenishment(_, _, let date), .withdrawal(_, _, let date):
            return date
        }
    }

    var sign: String {
        switch self {
        case .replenishment:
            return "+"
        case .withdrawal:
            return "-"
        }
    }
}
