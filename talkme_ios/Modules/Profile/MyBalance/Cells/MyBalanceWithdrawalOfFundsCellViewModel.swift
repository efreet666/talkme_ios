//
//  MyBalanceWithdrawalOfFundsViewModel.swift
//  talkme_ios
//
//  Created by Vladislav on 21.07.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceWithdrawalOfFundsCellVM: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let isMoreThanMinRelay = PublishRelay<Bool>()
    let maxCoin = BehaviorRelay<Int>(value: 0)
    let bag = DisposeBag()
    let textFieldText = PublishSubject<String?>()

    // MARK: - Private Properties

    private let title: String
    private let koef: Float

    // MARK: - Initializers

    init(title: String, koef: Float) {
        self.title = title
        self.koef = koef
    }

    // MARK: - Public Methods

    func configure(_ cell: MyBalanceWithdrawalOfFundsCell) {
        cell.configure(title: title, koef: koef)

        cell.textFieldText
            .bind { [weak self] text in
                self?.textFieldText.onNext(text)
            }
            .disposed(by: cell.bag)

        maxCoin
            .bind { [weak cell] value in
                cell?.setMaxValue(value)
            }
            .disposed(by: cell.bag)
        cell.isMoreThanMinRelay
            .bind(to: isMoreThanMinRelay)
            .disposed(by: cell.bag)
    }
}
