//
//  MyBalanceTransactionTableCell.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 06.05.2021.
//

import RxSwift

final class MyBalanceTransactionCell: UITableViewCell {

    // MARK: - Private Properties

    private(set) var bag = DisposeBag()

    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = .white
        return view
    }()

    private let directionImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        label.textColor = TalkmeColors.blackLabels
        label.numberOfLines = 0
        return label
    }()

    private let amountLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        label.textAlignment = .right
        label.numberOfLines = 1
        label.lineBreakMode = .byClipping
        return label
    }()

    private let coinImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: UIScreen.isSE ? "coin" : "bigBalanceCoin")
        return imageView
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Methods

    func configure(_ type: MyBalanceTransactionType) {
        amountLabel.text = "\(type.sign) \(type.cash)"
        titleLabel.text = type.description
        directionImageView.image = type.icon
        amountLabel.textColor = type.color
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.addSubviews([titleLabel, amountLabel, directionImageView, coinImageView])

        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(2)
            make.bottom.equalToSuperview().inset(4)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        directionImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 16 : 21)
            make.trailing.equalTo(titleLabel.snp.leading).inset(UIScreen.isSE ? -10 : -14)
            make.width.equalTo(UIScreen.isSE ? 12 : 15)
            make.height.equalTo(UIScreen.isSE ? 11 : 14)
            make.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(directionImageView.snp.trailing).inset(UIScreen.isSE ? 10 : 14)
            make.trailing.equalTo(amountLabel.snp.leading).inset(UIScreen.isSE ? -3 : -10)
            make.centerY.equalToSuperview()
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 11: 15)
        }

        amountLabel.snp.makeConstraints { make in
            make.trailing.equalTo(coinImageView.snp.leading).inset(UIScreen.isSE ? -3 : -6)
            make.centerY.equalTo(coinImageView)
            make.leading.equalTo(titleLabel.snp.trailing).inset(UIScreen.isSE ? 3 : 10)
            make.width.equalTo(UIScreen.isSE ? 65 : 70)
        }

        coinImageView.snp.makeConstraints { make in
            make.leading.equalTo(amountLabel.snp.trailing).inset(UIScreen.isSE ? 3 : 6)
            make.trailing.equalToSuperview().inset(12)
            make.width.height.equalTo(UIScreen.isSE ? 16 : 21)
            make.centerY.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
