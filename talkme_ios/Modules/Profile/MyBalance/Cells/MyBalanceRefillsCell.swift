//
//  MyBalanceRefillsTableCell.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 04.05.2021.
//

import RxSwift
import RxCocoa

final class MyBalanceRefillsCell: UITableViewCell {

    // MARK: - Public Properties

    let items = PublishRelay<[MyBalanceRefillCollectionCellViewModel]>()
    private(set) lazy var selectedItem = collectionView.rx.modelSelected(MyBalanceRefillCollectionCellViewModel.self).map { $0.type.iosInAppId }
    var bag = DisposeBag()

    // MARK: - Private Properties

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let height = UIScreen.isSE ? 90 : 119
        let width = UIScreen.isSE ? 8 : 20
        let itemMultiplier = UIScreen.isSE ? 10 : 5
        layout.itemSize = CGSize(width: (Int(UIScreen.main.bounds.width) - (2 * itemMultiplier) - width) / 2, height: height)
        layout.minimumInteritemSpacing = UIScreen.isSE ? 8 : 10
        layout.minimumLineSpacing = UIScreen.isSE ? 8 : 10
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvc.backgroundColor = .clear

        cvc.contentInset = .init(top: 0, left: 10, bottom: 0, right: 10)
        cvc.indicatorStyle = .white
        cvc.registerCells(withModels: MyBalanceRefillCollectionCellViewModel.self)
        cvc.isScrollEnabled = false
        cvc.allowsSelection = true
        return cvc
    }()

    // MARK: - Life-Cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure() {
        bindItems()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(0)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func bindItems() {
        items
            .do(afterNext: { [weak self] _ in
                guard let self = self else { return }
                self.collectionView.snp.updateConstraints { make in
                    let itemsCount = self.collectionView.numberOfItems(inSection: 0)
                    let rowsCountMultiple = UIScreen.isSE ? 90 : 120
                    let heightValue = UIScreen.isSE ? 20 : 94
                    let rowsCount = itemsCount / 2 + itemsCount % 2
                    let height = rowsCount * rowsCountMultiple + ((rowsCount - 1) * 8) + heightValue
                    make.height.equalTo(height)
                }
            })
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }
}
