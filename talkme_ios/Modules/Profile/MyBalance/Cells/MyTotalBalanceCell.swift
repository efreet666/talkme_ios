//
//  MyTotalBalanceCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 31.03.2021.
//

import RxSwift

final class MyTotalBalanceCell: UITableViewCell {

    // MARK: - Private Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let containerView = MyBalancePriceView()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setPrice(price: Int) {
        containerView.configure(price: String(price))
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(19)
            make.bottom.equalToSuperview().inset(19)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 94 : 122)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
