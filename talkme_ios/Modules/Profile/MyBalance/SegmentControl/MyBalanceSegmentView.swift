//
//  MyBalanceSegmentTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 07.06.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class MyBalanceSegmentView: UIView {

    private enum Constants {
        static let segmentedControlHeight: CGFloat = 45
        static let underlineViewHeight: CGFloat = 2
    }

    // MARK: - Public Properties

    let bag = DisposeBag()
    let segmentValue = PublishRelay<Int>()
    lazy var segmentIndex = segmentedControl.rx.selectedSegmentIndex

    // MARK: - Private Properties

    private var updateConstraint: Constraint?
    private let items = [
        "my_balance_segment_control_payments".localized,
        "my_balance_segment_control_history".localized,
        "my_balance_withdrawal_screen".localized
    ]

    private let segmentedControlContainerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = TalkmeColors.mainAccountBackground
        return containerView
    }()

    private lazy var segmentedControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl(items: items)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.setTitleTextAttributes([
            NSAttributedString.Key.foregroundColor: TalkmeColors.grayLabels,
            NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 13)], for: .normal)
        segmentedControl.setTitleTextAttributes([
            NSAttributedString.Key.foregroundColor: TalkmeColors.blueCollectionCell,
            NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 13)], for: .selected)
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
        return segmentedControl
    }()

    private let bottomUnderlineView: UIView = {
        let underlineView = UIView()
        underlineView.backgroundColor = TalkmeColors.blueCollectionCell
        return underlineView
    }()

    // MARK: - Init

    init() {
        super.init(frame: .zero)
        segmentedControl.clearBG()
        setupLayout()
        bindSegment()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func bindSegment() {
        segmentIndex.changed.bind { [weak self] value in
            self?.segmentValue.accept(value)
        }
        .disposed(by: bag)
    }

    private func setupLayout() {
        backgroundColor = TalkmeColors.mainAccountBackground
        segmentedControlContainerView.addSubviews([segmentedControl, bottomUnderlineView])
        addSubview(segmentedControlContainerView)

        segmentedControlContainerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(10)
            make.leading.width.equalToSuperview()
            make.height.equalTo(Constants.segmentedControlHeight)
        }

        segmentedControl.snp.makeConstraints { make in
            make.top.leading.centerX.centerY.equalToSuperview()
        }

        bottomUnderlineView.snp.makeConstraints { make in
            make.bottom.equalTo(segmentedControl.snp.bottom)
            make.height.equalTo(Constants.underlineViewHeight)
            make.width.equalTo(segmentedControl.snp.width).multipliedBy(1 / CGFloat(segmentedControl.numberOfSegments))
            updateConstraint = make.left.equalTo(segmentedControl.snp.left).constraint
        }
    }

    @objc private func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        changeSegmentedControlLinePosition()
    }

    private func changeSegmentedControlLinePosition() {
        let segmentIndex = CGFloat(segmentedControl.selectedSegmentIndex)
        let segmentWidth = segmentedControl.frame.width / CGFloat(segmentedControl.numberOfSegments)
        let leadingDistance = segmentWidth * segmentIndex
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.updateConstraint?.update(inset: leadingDistance)
            self?.layoutIfNeeded()
        })
    }
}

extension UISegmentedControl {

    func clearBG() {
        setBackgroundImage(imageWithColor(color: UIColor.clear), for: .normal, barMetrics: .default)
        setBackgroundImage(imageWithColor(color: UIColor.clear), for: .normal, barMetrics: .default)
        setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }

    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
}
