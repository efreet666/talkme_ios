//
//  MyBalanceViewController.swift
//  talkme_ios
//
//  Created by Майя Калицева on 26.03.2021.
//

import RxSwift
import RxCocoa
import RxDataSources
import ecommpaySDK

struct EcommpayMSG: Codable {
    var msg: String
}

struct WithdrawEcommpayPost: Codable {
    var name: String
    var description: String
    var amount: String
}

struct EcommpayJSON: Codable {
    var customer_id: String
    var hide_saved_wallets: Bool
    var language_code: String
    var payment_amount: Int
    var payment_currency: String
    var payment_description: String
    var payment_id: String
    var project_id: String
    var region_code: String
    var signature: String
}

final class MyBalanceRefillViewController: UIViewController {

    // MARK: - Private Properties

    private let ecommpaySDK = EcommpaySDK()
    private let paymentsService = PaymentsService()
    private let segmentControl = MyBalanceSegmentView()
    private let bag = DisposeBag()

    private let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = TalkmeColors.mainAccountBackground
        tv.tableFooterView = UIView()
        tv.keyboardDismissMode = .onDrag
        tv.separatorStyle = .none
        tv.registerCells(withModels:
                            MyTotalBalanceCellViewModel.self,
                            MyBalanceRefillsCellViewModel.self,
                            ConfirmButtonCellViewModel.self,
                            MyBalanceTransactionCellViewModel.self,
                            MyBalanceWithdrawalOfFundsCellVM.self,
                            MyBalanceWithdrawalOfDiamondsCellVM.self,
                            MyBalanceOverallOutputResultCellVM.self,
                            WithdrawButtonCellViewModel.self)
        return tv
    }()

    private lazy var recognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        recognizer.cancelsTouchesInView = false
        return recognizer
    }()

    static var summ: String = ""

    private let viewModel: MyBalanceRefillViewModel
    private var isWhiteNavBar: Bool?

    // MARK: - Init

    init(viewModel: MyBalanceRefillViewModel, isWhiteNavBar: Bool? = nil) {
        self.isWhiteNavBar = isWhiteNavBar
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
        setupTableView()
        setupSegmentTap()
        viewModel.setUpItems()
        viewModel.fetchKoef()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideCustomTabBarLiveButton(true)
        setupNavigation()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
    }

    // MARK: - Private Methods

    private func setupTableView() {
        view.addGestureRecognizer(recognizer)
        view.addSubviews([tableView, segmentControl])
        tableView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
        }
        segmentControl.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.height.equalTo(50)
            make.bottom.equalTo(tableView.snp.top)
        }

        tableView
          .rx.setDelegate(self)
          .disposed(by: bag)
    }

    private func setupSegmentTap() {
        segmentControl.segmentValue.bind { [weak self] value in
            if value == 0 {
                self?.setCustomTabbarHidden(false)
                self?.viewModel.setUpItems()
            } else if value == 1 {
                self?.setCustomTabbarHidden(false)
                self?.viewModel.fetchData()
                } else if value == 2 {
                    self?.setCustomTabbarHidden(false)
                    self?.viewModel.setUpItemsToTransaction()
                    }
            }
        .disposed(by: segmentControl.bag)
    }

    private func bindVM() {
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, AnyTableViewCellModelProtocol>>(
            configureCell: { _, tableView, indexPath, element in
                let cell = tableView.dequeueReusableCell(withModel: element, for: indexPath)
                element.configureAny(cell)
                return cell
            })
        viewModel
            .onButtonTapped
            .bind { [self] _ in getDataForEcommpay() }
            .disposed(by: bag)

        viewModel
            .dataItems
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)

        segmentControl
            .segmentValue
            .bind { [weak self] value in
                self?.viewModel.onSegmentTap.accept(value)
            }
            .disposed(by: bag)

    }

    private func getDataForEcommpay() {
        paymentsService
            .cardTokenizeUrlEcommpay()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    print(response)
                    self.postRequestWithdraw(json: response)
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    private func postRequestWithdraw(json: EcommpayJSON) {
        let coin = MyBalanceRefillViewController.summ
        let request = WithdrawEcommpayPost(name: "", description: "", amount: "\(coin)")
        paymentsService
            .withdrawEcommpay(request: request)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let respose):
                    guard self.viewModel.coinProfile >= self.viewModel.minimumCoins else { return }
                    if respose.msg == "link_card".localized {
                        self.presentEcommpay(json: json)
                    } else {
                        self.payoutStatus(text: respose.msg.localized)
                    }
                    print(respose)
                case .error(let error):
                    print(error)
                }
            }
    }

    private func presentEcommpay(json: EcommpayJSON) {
        let  paymentInfo = PaymentInfo(
            projectID: Int(json.project_id) ?? 0,
            paymentID: json.payment_id,
            paymentAmount: json.payment_amount,
            paymentCurrency: json.payment_currency,
            paymentDescription: json.payment_description,
            customerID: json.customer_id,
            regionCode: json.region_code
        )
        paymentInfo.hideSavedWallets = json.hide_saved_wallets
        paymentInfo.languageCode = json.language_code
        paymentInfo.setSignature(value: json.signature)
        paymentInfo.setAction(action: .Tokenize)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.setCustomTabbarHidden(true)
        }
        self.ecommpaySDK.presentPayment(at: self, paymentInfo: paymentInfo) { (result) in
            print("ecommpaySDK finished with status \(result.status.rawValue)")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.setCustomTabbarHidden(false)
            }
            if let error = result.error { // if error occurred
                print("Error: \(error.localizedDescription)")
            }
            if let token = result.token { // if tokenize action
                print("Tokenize: \(token)")
            }
        }
    }

    private func equals(lhs: [String: Any], rhs: [String: Any] ) -> Bool {
        return NSDictionary(dictionary: lhs).isEqual(to: rhs)
    }

    private func payoutStatus(text: String) {
        let alert = UIAlertController(title: "payout_status".localized, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    private func setupNavigation() {
        if isWhiteNavBar ?? true {
            customNavigationController?.style = .plainWhite(title: "profile_meny_my_wallet".localized)
        } else {
            customNavigationController?.style = .gradientBlueWithQRAndClassNumber(UserDefaultsHelper.shared.classNumber)
        }
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
}

extension MyBalanceRefillViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section > 0,
              section <= viewModel.dataItems.value.endIndex else { return nil }
        let title = viewModel.dataItems.value[section].model

        let headerView = StreamSectionHeaderView()
        headerView.sectionLabel.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
        headerView.configure(title: title)
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        section == 0 ? 0 : 25
    }
}
