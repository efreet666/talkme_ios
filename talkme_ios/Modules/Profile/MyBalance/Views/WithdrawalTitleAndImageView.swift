//
//  WithdrawalTitleAndImageView.swift
//  talkme_ios
//
//  Created by Vladislav on 22.07.2021.
//

import UIKit

final class WithdrawalTitleAndImageView: UIView {

    // MARK: - Private Properties

    private let container: UIView = {
        let view = UIView()
        return view
    }()

    private let label: UILabel = {
        let lbl = UILabel()
        lbl.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 13)
        lbl.textColor = TalkmeColors.white
        return lbl
    }()

    private var imageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        return img
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        container.layer.cornerRadius = container.bounds.height / 2
    }

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setContainerConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ type: WithdrawalRateLabelType) {
        label.text = type.title
        container.backgroundColor = type.backgroundColor
        imageView.image = type.icon
        label.textColor = type.textColor
        label.numberOfLines = 0
        label.font = type.fontSize
    }

    // MARK: - Private Methods

    private func setContainerConstraints() {
        addSubviews(container)
        container.addSubviewsWithoutAutoresizing(imageView, label)

        container.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        label.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalTo(imageView.snp.leading).inset(UIScreen.isSE ? -2 : -3)
        }

        imageView.snp.makeConstraints { make in
            make.leading.equalTo(label.snp.trailing).inset(UIScreen.isSE ? 5 : 6)
            make.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
    }
}
