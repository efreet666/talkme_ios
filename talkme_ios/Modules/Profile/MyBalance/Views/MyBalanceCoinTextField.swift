//
//  MyBalanceCoinTextField.swift
//  talkme_ios
//
//  Created by Vladislav on 22.07.2021.
//

import UIKit

final class MyBalanceCoinTextField: UITextField {

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.isSE ? 40 : 50, height: self.frame.height))
        leftView = paddingView
        keyboardType = .numberPad
        leftViewMode = .always
        layer.cornerRadius = UIScreen.isSE ? 18 : 23.5
        backgroundColor = TalkmeColors.white
        font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        minimumFontSize = UIScreen.isSE ? 13 : 15
        textColor = TalkmeColors.codeCountry
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
