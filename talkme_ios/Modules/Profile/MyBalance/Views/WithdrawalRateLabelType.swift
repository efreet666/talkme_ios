//
//  withdrawalRateLabelType.swift
//  talkme_ios
//
//  Created by Vladislav on 22.07.2021.
//

import RxCocoa

enum WithdrawalRateLabelType {

    case coin(Int)
    case briliant(String)
    case rubles(String)

    var icon: UIImage? {
        switch self {
        case .coin:
        return UIImage(named: "minicoin")
        case .briliant:
            return UIImage(named: "briliant")
        case .rubles:
            return UIImage(named: "rubles")
        }
    }

    var title: String {
        switch self {
        case .coin(let coin):
            return String(coin)
        case .briliant(let briliant):
            return String(briliant)
        case .rubles(let rubles):
            return String(rubles)
        }
    }

    var backgroundColor: UIColor? {
        switch self {
        case .coin:
            return .clear
        case .briliant:
            return .clear
        case .rubles:
            return .clear
        }
    }

    var textColor: UIColor? {
        switch self {
        case .coin:
            return TalkmeColors.placeholderColor
        case .briliant:
            return TalkmeColors.placeholderColor
        case .rubles:
            return TalkmeColors.placeholderColor
        }
    }

    var fontSize: UIFont? {
        switch self {
        case .coin:
            return .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 13)
        case .briliant:
            return .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 13)
        case .rubles:
            return .montserratFontMedium(ofSize: UIScreen.isSE ? 12 : 13)
        }
    }
}
