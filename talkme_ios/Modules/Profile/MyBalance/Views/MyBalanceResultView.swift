//
//  MyBalanceResultView.swift
//  talkme_ios
//
//  Created by Vladislav on 23.07.2021.
//

import UIKit

final class MyBalanceResultView: UIView {

    // MARK: - Private properties

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = TalkmeColors.lightBlackBalance
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 15 : 17)
        label.textAlignment = .center
        return label
    }()

    private let moneyLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 32 : 35)
        label.textColor = TalkmeColors.blueLabels
        label.textAlignment = .right
        return label
    }()

    private let rublesImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "rubles"))
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let priceStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .equalCentering
        sv.spacing = 6
        return sv
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = TalkmeColors.graySearchBar
        layer.cornerRadius = 12
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func configure(price: String) {
        moneyLabel.text = price
        priceLabel.text = "my_balance_price_finally".localized
    }

    // MARK: - Private methods

    private func setupLayout() {
        priceStack.addArrangedSubviews([moneyLabel, rublesImageView])
        addSubviews([priceLabel, priceStack])

        priceLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(UIScreen.isSE ? 16 : 20)
        }
        priceStack.snp.makeConstraints { make in
            make.top.equalTo(priceLabel.snp.bottom).inset(UIScreen.isSE ? -10 : -14)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 21 : 30)
            make.centerX.equalToSuperview()
        }
        rublesImageView.snp.makeConstraints { make in
            make.height.equalTo(27)
            make.width.equalTo(23)
        }
    }
}
