//
//  ProfileCoordinator.swift
//  talkme_ios
//
//  Created by 1111 on 12.01.2021.
//

import RxCocoa
import RxSwift

final class ProfileCoordinator {

    enum Flow {
        case exitAppToRoot
        case exitToPreviousScreen
        case showTopUpBalance
    }

    private(set) weak var navigationController: BaseNavigationController?
    let service: AccountServiceProtocol = AccountService()
    let paymentsService: PaymentsServiceProtocol = PaymentsService()
    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()
    let profileTabImage = PublishRelay<String?>()
    let onReloadStreamsList = PublishRelay<Void>()
    let reloadSavedStreams = PublishRelay<Void>()
    let onBackProfile = PublishRelay<Void>()
    let onUpdateMainInfo = PublishRelay<Void>()

    // MARK: - Private Properties

    private let qrCoordinator = QrCodeCoordinator()
    private let streamCoordinator = StreamCoordinator()
    private let createEditCoordinator = LessonCreateEditCoordinator()
    private let lessonCoordinator = LessonCoordinator()
    private let chatsCoordinator = ChatsCoordinator()

    private let mainService = MainService()
    private let lessonsService = LessonsService()
    private let accountService = AccountService()
    private var lessonsBag = DisposeBag()

    private var isCreateSpecialist: Bool = false
    // MARK: - Initializers

    init() {
        bindStreamCoordinator()
        bindCreateEditLessonCoordinator()
        bindLessonCoordinator()
    }

    // MARK: - Public Methods

    func start() -> UINavigationController {
        let profileViewModel = ProfileMenuViewModel(service: service)
        let controller = ProfileMenuViewController(viewModel: profileViewModel)
        let navigationController = BaseNavigationController(rootViewController: controller)
        self.navigationController = navigationController
        bindProfileMenuViewModel(viewModel: profileViewModel)
        bindQrButton()
        return navigationController
    }

    func showAccountVC(navigationController: UINavigationController) {
        let accountInfoViewModel = AccountInfoViewModel(service: service)
        let accountVC = AccountInfoViewController(viewModel: accountInfoViewModel)
        navigationController.pushViewController(accountVC, animated: true)
    }

    func showMyBalanceScreen() {
        let balanceVM = MyBalanceRefillViewModel(paymentsService: paymentsService)
        let balanceVC = MyBalanceRefillViewController(viewModel: balanceVM)
        navigationController?.pushViewController(balanceVC, animated: true)
    }

    // MARK: - Private Methods

    private func bindQrButton() {
        guard let navigationController = navigationController else { return }
        navigationController
            .qrButtonTapped
            .bind { [weak self] _ in
                guard let navigationController = self?.navigationController else { return }
                self?.qrCoordinator.start(from: navigationController)
            }
            .disposed(by: navigationController.bag)
    }

    private func showAccountVC(profileResponse: ProfileResponse, contactsResponse: ContactsResponse) {
        let accountInfoViewModel = AccountInfoViewModel(service: service, profileResponse: profileResponse, contactsResponse: contactsResponse)
        let accountVC = AccountInfoViewController(viewModel: accountInfoViewModel)
        bindAccountViewModel(model: accountInfoViewModel)
        accountInfoViewModel.flow.accept(.updateMainInfo)
        navigationController?.pushViewController(accountVC, animated: true)
    }

    private func bindAccountViewModel(model: AccountInfoViewModel) {
        model.flow
            .bind { [weak self] flow in
                switch flow {
                case .updateMainInfo:
                    return
                case.watchStream(let lessonId, let streamsList, let countOfStreamsInOnePage):
                    self?.showSavedStreams(from: streamsList,
                                           createdBy: UserDefaultsHelper.shared.userId,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: countOfStreamsInOnePage)
                case .onListOfUsersSavedLessons(let userId):
                    self?.showCategoryFiteredLessonsVC(createdBy: userId, withCategory: .saved)
                case .deleteSavedStream(let lessonId):
                    self?.showDeleteStreamPopUp(for: lessonId) {
                        model.reloadSavedStreams.accept(())
                    }
                }
            }
            .disposed(by: model.bag)
    }

    private func showProfileSettingsVC(
        profileResponse: ProfileResponse,
        contactsResponse: ContactsResponse,
        menuVM: ProfileMenuViewModel,
        buttonType: SettingButtonType
    ) {
        let profileSettingsVM = ProfileSettingViewModel(
            service: service,
            profileResponse: profileResponse,
            contactsResponse: contactsResponse,
            buttonState: buttonType)
        let profileSettingsVC = ProfileSettingViewController(viewModel: profileSettingsVM)
        bindProfileSettingViewModel(viewModel: profileSettingsVM,
                                    menuVM: menuVM,
                                    profileResponse: profileResponse,
                                    contactsResponse: contactsResponse)
        navigationController?.pushViewController(profileSettingsVC, animated: true)
    }

    private func showMyStreamsScreen(profileResponse: ProfileResponse) {
        let service = LessonsService()
        let streamsViewModel = MyStreamsViewModel(service: service)
        let streamsVC = StreamsViewController(viewModel: streamsViewModel)
        bindStreamsVM(streamsViewModel)
        navigationController?.pushViewController(streamsVC, animated: true)
    }

    private func showMyStreamsScreenAfterCreatingLesson() {
        let service = LessonsService()
        let streamsViewModel = MyStreamsViewModel(service: service)
        let streamsVC = StreamsViewController(viewModel: streamsViewModel)
        bindStreamsVM(streamsViewModel)
        if let rootVC = navigationController?.viewControllers.first {
            navigationController?.setViewControllers([rootVC, streamsVC], animated: true)
        }
    }

    private func showMySubscribsScreen(profileResponse: ProfileResponse) {
        let service = LessonsService()
        let subscribsViewModel = MySubscribsViewModel(service: service, profileResponse: profileResponse)
        let streamsVC = StreamsViewController(viewModel: subscribsViewModel)
        bindStreamsVM(subscribsViewModel)
        navigationController?.pushViewController(streamsVC, animated: true)
    }

    private func showTeachersScreen() {
        let teachersVM = TeachersViewModel(service: service)
        let teachersVC = TeachersViewController(viewModel: teachersVM)
        bindTeachersScreen(viewModel: teachersVM)
        navigationController?.pushViewController(teachersVC, animated: true)
    }

    private func showDeleteStreamPopUp(for lessonId: Int, action: @escaping () -> Void) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let deleteAction = UIAlertAction(title: "popup_delete_lesson_popup_title".localized, style: .default) { [weak self] _ in
            guard let self = self
            else { return }
            self.lessonsService
                .deleteLesson(withId: lessonId)
                .subscribe { event in 
                    switch event {
                    case .success:
                        action()
                    case .error:
                        print("Get Error when try to delete lesson with id \(lessonId)")
                    }
                }.disposed(by: self.bag)
        }
        deleteAction.setValue(UIColor.red, forKey: "titleTextColor")

        let cancelAction = UIAlertAction(title: "popup_delete_profile_no".localized, style: .cancel)
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")

        alert.addAction(deleteAction)
        alert.addAction(cancelAction)

        navigationController?.present(alert, animated: true)
    }

    private func bindTeachersScreen(viewModel: TeachersViewModel) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .onProfileSelect(let classNumber):
                    self?.showUserAccountInfoVC(classNumber: classNumber)
                case.onMessegeButtonTap(id: let id):
                    self?.showChat(userId: id)
                }
            }
            .disposed(by: viewModel.bag)
    }

    func showUserAccountInfoVC(classNumber: String?) {
        guard let classNumber = classNumber else { return }
        let viewModel = UserAccountViewModel(service: service, classNumber: classNumber)
        let controller = UserAccountViewController(viewModel: viewModel)
        bindUserViewModel(viewModel: viewModel)
        self.navigationController?.pushViewController(controller, animated: true)
    }

    private func bindUserViewModel(viewModel: UserAccountViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self] flow in
                switch flow {
                case .onLessonDetail(let id):
                    self?.showLessonDetailVC(id: id)
                case .onListOfUsersSavedLessons(let userId):
                    self?.showCategoryFiteredLessonsVC(createdBy: userId, withCategory: .saved)
                case .makeComplaint(let userId):
                    self?.showActionSheet(userId: userId)
                case .watchStream(let userId, let lessonId, let streamsList, let countOsStreamsInOnePage):
                    self?.showSavedStreams(from: streamsList,
                                           createdBy: userId,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: countOsStreamsInOnePage )
                }
            })
            .disposed(by: viewModel.bag)

        viewModel
            .onCreateChatTapped
            .bind { [weak self] id in
                self?.showChat(userId: id)
            }
            .disposed(by: viewModel.bag)
    }

    private func showLessonDetailVC(id: Int) {
        lessonCoordinator.start(navigationController, lessonId: id)
        bindLessonCoordinator()
    }

    private func showCreateLessonVC(id: Int?, screenType: CreateLessonScreenType) {
        createEditCoordinator.start(navigationController, lessonID: id, type: screenType)
    }

    private func showLessonInfo(id: Int) {
        lessonsBag = DisposeBag()
        lessonCoordinator.start(navigationController, lessonId: id)
        bindLessonCoordinator()
    }

    private func showActionSheet(userId: Int) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let reportAction = UIAlertAction(title: "complaint_button".localized, style: .default) { [weak self] _ in
            self?.showReportPopUp(userId: userId)
        }
        reportAction.setValue(UIColor.red, forKey: "titleTextColor")

        let blockAction = UIAlertAction(title: "complaint_block_button_title".localized, style: .default) { [weak self] _ in
            self?.showBlockPopUp(userId: userId)
        }
        blockAction.setValue(UIColor.red, forKey: "titleTextColor")

        let cancelAction = UIAlertAction(title: "complaint_cancel".localized, style: .cancel)
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")

        alert.addAction(blockAction)
        alert.addAction(reportAction)
        alert.addAction(cancelAction)

        navigationController?.present(alert, animated: true)
    }

    private func showReportPopUp(userId: Int) {
        let view = StreamComplaintPopUpView(accountService: accountService, userAbout: userId )
        let drawer = DrawerViewController(contentView: view)

        view.flow
            .bind { [weak self] flow in
                self?.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self?.presentAlertAndCloseViewController()
                case .dissmissPopupWithError:
                    self?.presentAlertWithError()
                }
            }
            .disposed(by: bag)

        navigationController?.present(drawer, animated: true)
    }

    private func presentAlertAndCloseViewController() {
        let alert = UIAlertController(title: "complaint_alert_success".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "general_ok".localized, style: .cancel) { _ in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func presentAlertWithError() {
        let alert = UIAlertController(title: "complaint_alert_error".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "general_ok".localized, style: .cancel)
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func showBlockPopUp(userId: Int) {
        guard let accountService = accountService as? AccountService else { return }
        let view = StreamBlockUserPopUpView(accountService: accountService, userAbout: userId )
        let drawer = DrawerViewController(contentView: view)

        view.flow
            .bind { [weak self] flow in
                self?.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self?.presentSuccessBlockAndCloseStream()
                case .dissmissPopupWithError:
                    self?.presentAlertWithError()
                case .cancelPopup:
                    self?.dismissDrawer()
                }
            }
            .disposed(by: view.bag)

        navigationController?.present(drawer, animated: true)
    }

    private func presentSuccessBlockAndCloseStream() {
        let alert = UIAlertController(title: "block_alert_success".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "general_ok".localized, style: .cancel) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func showCategoryFiteredLessonsVC(createdBy userId: Int, withCategory category: LessonCategoryType) {
        let model = CategoryFilteredLessonsViewModel(category, mainService: mainService, lessonsService: lessonsService, streamsOwnerId: userId)
        let vc = CategoryFilteredLessonsViewController(model)
        bindFilteredLessonsModel(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func bindFilteredLessonsModel(_ model: CategoryFilteredLessonsViewModel) {
        model
            .flow
            .bind { [weak self, weak model] event in
                guard model != nil else { return }
                switch event {
                case .onProfileSelect(let classNumber):
                    self?.showUserAccountInfoVC(classNumber: classNumber)
                case .showLessonInfo(let id):
                    self?.showLessonDetailVC(id: id)
                case .showUsersSavedStreams(let userId, let lessonId, let categoriesIds, let streamsList, let countOfStreamsInPage):
                    self?.showSavedStreams(from: streamsList,
                                           belongingTo: categoriesIds,
                                           createdBy: userId,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: countOfStreamsInPage)
                case .showLiveStream:
                    break
                case .showAllSavedStreams:
                    break
                case .deleteButtonTapped(let lessonId):
                    self?.showDeleteStreamPopUp(for: lessonId) {
                        model?.reloadStreams.accept(())
                    }
                case .showFilters:
                    break
                }
            }
            .disposed(by: model.bag)
    }

    private func showPushNotificationsList() {
        let viewModel = PushNotificationsListViewModel()
        let controller = PushNotificationsListController(viewModel: viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func bindStreamsVM(_ viewModel: MyStreamsProtocol) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .onWatch(let lessonId, let countOfStreamsInPage):
                    self?.showLiveStreams(startFrom: lessonId, addNewStreamsToListByChunksWithSizeOf: countOfStreamsInPage)
                case .edit(let id):
                    self?.showCreateLessonVC(id: id, screenType: .edit)
                case .lessonInfo(let id):
                    self?.showLessonInfo(id: id)
                }
            }
            .disposed(by: viewModel.bag)

        onReloadStreamsList
            .bind { [weak viewModel] _ in
                viewModel?.fetchStreams()
            }
            .disposed(by: viewModel.bag)
    }

    private func showLiveStreams(startFrom lessonId: Int, addNewStreamsToListByChunksWithSizeOf countOfStreamsInOnePage: Int) {
        streamCoordinator.start(navigationController, lessonId: lessonId, numberOfStreamsInPage: countOfStreamsInOnePage, watchSave: false)
    }

    private func showSavedStreams(from streamsList: [LiveStream],
                                  belongingTo categoriesIds: [Int] = [0],
                                  createdBy userId: Int,
                                  startFrom lessonId: Int,
                                  addNewStreamsToListByChunksWithSizeOf countOfStreamsInOnePage: Int
    ) {
        streamCoordinator.start(navigationController,
                                lessonId: lessonId,
                                lessonsInfo: streamsList,
                                numberOfStreamsInPage: countOfStreamsInOnePage,
                                watchSave: true,
                                watchStreamsCreatedBy: userId,
                                withCategories: categoriesIds)
    }

    private func bindStreams(viewModel: StreamsFeedViewModel) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .onBack:
                    self?.navigationController?.popViewController(animated: true)
                default:
                    return
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func showCountriesScreen(profileViewModel: ProfileSettingViewModel) {
        let viewModel = CountryCodesViewModel()
        let controller = CountryCodesViewController(viewModel: viewModel)
        bindCountryCodeViewModel(viewModel: viewModel, profileViewModel: profileViewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showConfirmPopUp(menuVM: ProfileMenuViewModel, profileResponse: ProfileResponse, contactsResponse: ContactsResponse) {
        let vm = PopUpConfirmViewModel()
        let vc = PopUpConfirmViewController(viewModel: vm)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        bindPopUpConfirmViewModel(
            viewModel: vm,
            menuVM: menuVM,
            profileResponse: profileResponse,
            contactsResponse: contactsResponse,
            controller: vc)
        navigationController?.present(vc, animated: true)
    }

    private func showNumberPopUp() {
        let vm = PopUpUserNumberViewModel(service: service)
        let vc = PopUpUserNumberViewController(viewModel: vm)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        bindPopUpUserNumberViewModel(viewModel: vm)
        navigationController?.present(vc, animated: true)
    }

    private func showProfileMenuVC() {
        navigationController?.popToRootViewController(animated: true)
    }

    private func showCodePopup(phoneNumber: String, settingViewModel: ProfileSettingViewModel) {
        let viewModel = PopupCodeViewModel(service: service, phoneNumber: phoneNumber)
        let controller = PopupCodeViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        bindPopupCodeViewModel(viewModel: viewModel, controller: controller)
        navigationController?.present(controller, animated: true)
    }

    private func showCountriesVC(profileViewModel: ProfileSettingViewModel) {
        let vm = CountryViewModel(service: service)
        let controller = CountryViewController(viewModel: vm)
        bindCountryViewModel(viewModel: vm, profileViewModel: profileViewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showCitiesVC(isoCode: String, profileViewModel: ProfileSettingViewModel) {
        let vm = CitiesViewModel(service: service, isoCode: isoCode)
        let controller = CountryViewController(viewModel: vm)
        bindCountryViewModel(viewModel: vm, profileViewModel: profileViewModel)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func showAppRules(on parent: PopUpConfirmViewController) {
        let controller = AppRulesVC(urlString: "https://\(Configuration.base)/media/main/TALKME_SPEC.pdf")
        let navVC = UINavigationController(rootViewController: controller)
        navVC.modalPresentationStyle = .fullScreen
        parent.present(navVC, animated: true)
    }

    private func showSupportVC() {
        let viewModel = SupportViewModel()
        let viewController = SupportViewController(viewModel: viewModel)
        navigationController?.pushViewController(viewController, animated: true)
    }

    private func bindProfileMenuViewModel(viewModel: ProfileMenuViewModel) {
        viewModel
            .flow
            .bind { [weak self, weak viewModel] flow in
                guard let viewModel = viewModel else { return }
                switch flow {
                case .setUserInfo(let info, let contacts):
                    self?.showAccountVC(profileResponse: info, contactsResponse: contacts)
                case .mySubscribes(let info):
                    self?.showMySubscribsScreen(profileResponse: info)
                case .teachers:
                    self?.showTeachersScreen()
                case .myBalance:
                    self?.showMyBalanceScreen()
                case .settings(let info, let contacts):
                    self?.isCreateSpecialist = false
                    self?.showProfileSettingsVC(profileResponse: info, contactsResponse: contacts, menuVM: viewModel, buttonType: .save)
                case .notifications:
                    self?.showPushNotificationsList()
                case .exitFromScreen:
                    self?.flow.accept(.exitAppToRoot)
                case .showPopUp(let info, let contacts):
                    self?.showConfirmPopUp(menuVM: viewModel, profileResponse: info, contactsResponse: contacts)
                case .showCreateLesson:
                    self?.showCreateLessonVC(id: nil, screenType: .create)
                case .setLive(let info):
                    self?.showMyStreamsScreen(profileResponse: info)
                case .updateMainInfo:
                    return
                case .support:
                    self?.showSupportVC()
                case .exitFromProfile:
                    self?.showExitFromAccountPopUp()
                case .deleteProfile:
                    self?.showDeleteMyAccountPopUp()
                }
            }
            .disposed(by: viewModel.bag)

        viewModel
            .profileTabImage
            .bind(to: profileTabImage)
            .disposed(by: viewModel.bag)
    }

    private func bindPopUpConfirmViewModel(
        viewModel: PopUpConfirmViewModel,
        menuVM: ProfileMenuViewModel,
        profileResponse: ProfileResponse,
        contactsResponse: ContactsResponse,
        controller: PopUpConfirmViewController
    ) {
        viewModel
            .flow
            .bind { [weak self, weak controller, weak menuVM, weak viewModel] flow in
                guard let menuVM = menuVM,
                      let controller = controller,
                      let viewModel = viewModel else { return }
                switch flow {
                case .showProfileSettings:
                    controller.dismiss(animated: true)
                    self?.isCreateSpecialist = true
                    self?.showProfileSettingsVC(
                        profileResponse: profileResponse,
                        contactsResponse: contactsResponse,
                        menuVM: menuVM,
                        buttonType: .becomeTeacher)
                case .showAppRules:
                    self?.showAppRules(on: controller)
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindPopUpUserNumberViewModel(
        viewModel: PopUpUserNumberViewModel
    ) {
        viewModel
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .updateInfo:
                    self?.onBackProfile.accept(())
                    self?.onUpdateMainInfo.accept(())
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindProfileSettingViewModel(
        viewModel: ProfileSettingViewModel,
        menuVM: ProfileMenuViewModel,
        profileResponse: ProfileResponse,
        contactsResponse: ContactsResponse
    ) {
        viewModel
            .flow
            .bind { [weak self, weak viewModel, weak menuVM] flow in
                guard let self = self else { return }
                guard let menuVM = menuVM,
                      let viewModel = viewModel else { return }
                switch flow {
                case .onShowCountries:
                    self.showCountriesScreen(profileViewModel: viewModel)
                case .onProfile:
                    self.showProfileMenuVC()
                case .showNumberPopUp:
                    guard self.isCreateSpecialist else { return }
                    self.showNumberPopUp()
                case .updateMainInfo:
                    menuVM.flow.accept(.updateMainInfo)
                case .onPopup(let phoneNumber):
                    self.showCodePopup(phoneNumber: phoneNumber, settingViewModel: viewModel)
                case .onSocials(let social, let isActive):
                    if !isActive {
                        self.showSocialPopup(social: social, profileViewModel: viewModel)
                    } else {
                        viewModel.removeSocialsRequest(type: social)
                    }
                case .onUserGeoTap(let type):
                    switch type {
                    case .country:
                        self.showCountriesVC(profileViewModel: viewModel)
                    case .city:
                        let iso = UserDefaultsHelper.shared.countryCode
                        self.showCitiesVC(isoCode: iso, profileViewModel: viewModel)
                    }
                }
            }
            .disposed(by: viewModel.bag)

        onBackProfile
            .bind {
                viewModel.backProfile.accept(())
            }.disposed(by: viewModel.bag)

        onUpdateMainInfo
            .bind {
                menuVM.flow.accept(.updateMainInfo)
            }.disposed(by: menuVM.bag)
    }

    private func bindPopupCodeViewModel(viewModel: PopupCodeViewModel, controller: UIViewController) {
        viewModel
            .flow
            .bind { [weak self, weak controller] flow in
                guard let controller = controller else { return }
                switch flow {
                case .onCodeConfirm(let code, let phoneNumber):
                    self?.numberChange(phoneNumber, code: code)
                    controller.dismiss(animated: true)
                case .backOnProfile:
                    self?.onBackProfile.accept(())
                    self?.onUpdateMainInfo.accept(())
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindCountryCodeViewModel(
        viewModel: CountryCodesViewModel,
        profileViewModel: ProfileSettingViewModel
    ) {
        viewModel
            .flow
            .bind { [weak self, weak profileViewModel] flow in
                guard let profileViewModel = profileViewModel else { return }
                switch flow {
                case .setImageAndCode(let country):
                    profileViewModel.onCountryCode.accept(country)
                    self?.navigationController?.popViewController(animated: false)
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindCountryViewModel(
        viewModel: CountryCityProtocol,
        profileViewModel: ProfileSettingViewModel
    ) {
        viewModel
            .flow
            .bind { [weak self, weak profileViewModel] flow in
                guard let profileViewModel = profileViewModel else { return }
                switch flow {
                case .onItemSelected(let item):
                    profileViewModel.onSelectedCountry.accept(item)
                    self?.navigationController?.popViewController(animated: false)
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func numberChange(_ phoneNumber: String, code: String) {
        let request = SendMobileAndCodeRequest(code: code, mobile: phoneNumber)
        service
            .sendNumberChange(request: request)
            .subscribe { event in
                switch event {
                case .success(let response):
                    print(response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func bindCreateEditLessonCoordinator() {
        createEditCoordinator
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .completeCreationEdit:
                    self?.showMyStreamsScreenAfterCreatingLesson()
                }
            }
            .disposed(by: bag)
    }

    private func bindLessonCoordinator() {
        lessonCoordinator
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .completeLessonInfo:
                    return
                case .topUpBalance:
                    self?.flow.accept(.showTopUpBalance)
                case .onChat(let id):
                    self?.showChat(userId: id)
                case .makeComplaint(let id):
                    self?.showActionSheet(userId: id)
                case .exitToRoot, .exitToPreviousScreen:
                    return
                }
            }
            .disposed(by: bag)
    }

    private func showSocialPopup(social: SocialMedia, profileViewModel: ProfileSettingViewModel) {
        let viewModel = PopupSocialsViewModel(service: service, social: social)
        let controller = PopUpSocialsViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .overFullScreen
        controller.modalTransitionStyle = .crossDissolve
        navigationController?.present(controller, animated: true)
        bindSocials(viewModel: viewModel, settingsVM: profileViewModel, controller: controller)
    }

    private func bindSocials(
        viewModel: PopupSocialsViewModel,
        settingsVM: ProfileSettingViewModel,
        controller: PopUpSocialsViewController
    ) {
        viewModel
            .flow
            .bind { [weak settingsVM, weak controller] flow in
                switch flow {
                case .onAdd(let social, let url):
                    settingsVM?.onAddSocial(social, url: url)
                    controller?.dismiss(animated: true)
                }
            }
            .disposed(by: viewModel.bag)
    }

    private func bindStreamCoordinator() {
        streamCoordinator
            .flow
            .bind { [weak self] event in
                switch event {
                case .exitToRoot:
                    self?.navigationController?.popToRootViewController(animated: true)
                case .exitToPreviousScreen:
                    self?.navigationController?.popViewController(animated: true)
                case .toChat(let id):
                    self?.showChat(userId: id)
                case .makeComplaint(let id):
                    self?.showActionSheet(userId: id)
                }
            }
            .disposed(by: bag)

        streamCoordinator
            .onReloadStreamsList
            .bind(to: onReloadStreamsList)
            .disposed(by: bag)
    }

    private func showExitFromAccountPopUp() {
        AlertControllerHelper.showLogoutAlert { [weak self] in
            UserDefaultsHelper.shared.deleteTokenAndUserInfo()
            self?.flow.accept(.exitAppToRoot)
        }
    }

    private func showDeleteMyAccountPopUp() {
        AlertControllerHelper.showDeleteMyProfileAlert { [weak self] in
            self?.service.deleteMyProfile()
                .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.exitAppToRoot)
                case .error:
                    break
                }
            }
        }
    }

    private func showChat(userId: Int) {
        guard let navigationController = navigationController else { return }
        chatsCoordinator.startChat(navigationController, userId: userId)
    }

    private func dismissDrawer() {
        navigationController?.dismiss(animated: true)
    }
}
