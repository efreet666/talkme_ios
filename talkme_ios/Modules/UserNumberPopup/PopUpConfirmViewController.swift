//
//  PopUpConfirmViewController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 05.01.2021.
//

import RxCocoa
import RxSwift

final class PopUpConfirmViewController: UIViewController {

    // MARK: - Private Properties

    private lazy var popUpConfirmView: PopUpConfirmView = {
        let view = PopUpConfirmView()
        view.layer.cornerRadius = 14
        return view
    }()
    private let bag = DisposeBag()
    private let viewModel: PopUpConfirmViewModel

    init(viewModel: PopUpConfirmViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.popupBackground
        setupLayoutPopUp()
        bindUI()
    }

    // MARK: - Private Methods

    private func bindUI() {
        popUpConfirmView.confirmButtonTap
            .bind { [weak self] in
                guard let self = self else { return }
                self.viewModel.onConfirmTap()
            }
            .disposed(by: bag)

        popUpConfirmView.onRulesTapped
            .map { _ in .showAppRules }
            .bind(to: viewModel.flow)
            .disposed(by: bag)
    }

    private func setupLayoutPopUp() {
        view.addSubview(popUpConfirmView)
        popUpConfirmView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(25)
        }
    }
}
