//
//  PopUpUserNimberViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 15.01.2021.
//

import RxCocoa
import RxSwift

final class PopUpUserNumberViewModel {

    enum Flow {
        case updateInfo
    }

    // MARK: - Public Properties

    let bag = DisposeBag()
    let classNumber = PublishRelay<String>()
    let flow = PublishRelay<Flow>()

    // MARK: - Private Properties

    private let service: AccountServiceProtocol

    // MARK: - Initializers

    init(service: AccountServiceProtocol) {
        self.service = service
    }

    // MARK: - Public Methods

    func onUserNumberTap() {
        flow.accept(.updateInfo)
    }

    func getСlassNumber() {
        service
            .classNumberCreate()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.classNumber.accept(response.numberClass)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }
}
