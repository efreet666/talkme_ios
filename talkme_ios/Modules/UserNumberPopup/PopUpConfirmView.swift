//
//  PopUpConfirmView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 05.01.2021.
//

import RxCocoa
import RxSwift

final class PopUpConfirmView: UIView {

    // MARK: - Public Properties

    private(set) lazy var confirmButtonTap = confirmButton.rx.tap
    private(set) lazy var onRulesTapped = subTitle.rx.tapGesture().when(.recognized)

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "talkme_popup_agreement".localized
        lbl.textAlignment = .center
        lbl.textColor = TalkmeColors.grayLabel
        lbl.font = .montserratBold(ofSize: 15)
        return lbl
    }()

    private let subTitle: UIButton = {
        let bt = UIButton()
        let attributes: [NSAttributedString.Key: Any] =
            [NSAttributedString.Key.font: UIFont.montserratFontRegular(ofSize: 13),
             NSAttributedString.Key.foregroundColor: TalkmeColors.grayLabel]
        let attributedString = NSMutableAttributedString(
            string: "talkme_popup_press_confirm".localized, attributes: attributes)
        let anotherAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: UIFont.montserratFontRegular(ofSize: 13),
            NSAttributedString.Key.foregroundColor: TalkmeColors.blueLabels,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let anotherString = NSMutableAttributedString(
            string: "talkme_popup_rules".localized, attributes: anotherAttributes)
        attributedString.append(anotherString)
        bt.titleLabel?.numberOfLines = 0
        bt.setAttributedTitle(attributedString, for: .normal)
        bt.titleLabel?.textAlignment = .center
        return bt
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorGray
        view.alpha = 0.36
        return view
    }()

    private let confirmButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("talkme_сonfirm_сode_confirm".localized, for: .normal)
        btn.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        btn.setTitleColor(.white, for: .highlighted)
        btn.titleLabel?.font = .montserratBold(ofSize: 15)
        return btn
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = TalkmeColors.white
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviewsWithoutAutoresizing([titleLabel,
                                        subTitle,
                                        separatorView,
                                        confirmButton])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(20)
            make.leading.trailing.equalToSuperview().inset(15)
        }

        subTitle.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(3)
            make.leading.trailing.equalToSuperview().inset(25)
        }

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(subTitle.snp.bottom).offset(19)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        confirmButton.snp.makeConstraints { make in
            make.top.equalTo(separatorView.snp.bottom).offset(5)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(5)
        }
    }
}
