//
//  PopUpUserNumberView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 05.01.2021.
//

import UIKit

final class PopUpUserNumberView: UIView {

    // MARK: - Public Properties

    private(set) lazy var confirmUserNumberButtonTap = confirmUserNumberButton.rx.tap

    // MARK: - Private Properties

    private let talkMeLogo: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "talkmepopup")
        img.contentMode = .scaleAspectFit
        return img
    }()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "talkme_popup_id_number".localized
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.textColor = TalkmeColors.grayLabel
        lbl.font = .montserratBold(ofSize: 15)
        return lbl
    }()

    private let userNumberLabel: UIButton = {
        let btn = UIButton()
        btn.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        btn.setTitle("\(UserDefaultsHelper.shared.userId)", for: .normal)
        btn.titleLabel?.font = .montserratBold(ofSize: 18)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = TalkmeColors.blueLabels.cgColor
        btn.layer.cornerRadius = 17
        return btn
    }()

    private let textLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "talkme_popup_users_find".localized
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.textColor = TalkmeColors.grayLabel
        lbl.font = .montserratFontRegular(ofSize: 13)
        return lbl
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorGray
        view.alpha = 0.36
        return view
    }()

    private let confirmUserNumberButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("talkme_popup_continue".localized, for: .normal)
        btn.setTitleColor(TalkmeColors.blueLabels, for: .normal)
        btn.setTitleColor(.white, for: .highlighted)
        btn.titleLabel?.font = .montserratBold(ofSize: 15)
        return btn
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        backgroundColor = TalkmeColors.white
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    public func configure(title: String) {
        userNumberLabel.setTitle(title, for: .normal)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviewsWithoutAutoresizing([talkMeLogo,
                                        titleLabel,
                                        userNumberLabel,
                                        textLabel,
                                        separatorView,
                                        confirmUserNumberButton])

        talkMeLogo.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(35)
            make.leading.trailing.equalToSuperview().inset(50)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(talkMeLogo.snp.bottom).offset(22)
            make.leading.trailing.equalToSuperview().inset(50)
        }

        userNumberLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
            make.leading.trailing.equalToSuperview().inset(50)
        }

        textLabel.snp.makeConstraints { make in
            make.top.equalTo(userNumberLabel.snp.bottom).offset(15)
            make.leading.trailing.equalToSuperview().inset(50)
        }

        separatorView.snp.makeConstraints { make in
            make.top.equalTo(textLabel.snp.bottom).offset(22)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        confirmUserNumberButton.snp.makeConstraints { make in
            make.top.equalTo(separatorView.snp.bottom).offset(5)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(5)
        }
    }
}
