//
//  PopUpConfirmViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 15.01.2021.
//

import RxCocoa
import RxSwift

final class PopUpConfirmViewModel {

    enum Flow {
        case showProfileSettings
        case showAppRules
    }

    // MARK: - Private Properties

    let bag = DisposeBag()
    public let flow = PublishRelay<Flow>()

    // MARK: - Public Methods

    func onConfirmTap() {
        flow.accept(.showProfileSettings)
    }
}
