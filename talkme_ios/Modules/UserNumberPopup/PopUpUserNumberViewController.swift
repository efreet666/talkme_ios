//
//  PopUpUserNumberViewController.swift
//  talkme_ios
//
//  Created by Yura Fomin on 06.01.2021.
//

import RxCocoa
import RxSwift

final class PopUpUserNumberViewController: UIViewController {

    // MARK: - Private Properties

    private lazy var popUpUserNumberView: PopUpUserNumberView = {
        let view = PopUpUserNumberView()
        view.layer.cornerRadius = 17
        return view
    }()

    private let bag = DisposeBag()
    private let viewModel: PopUpUserNumberViewModel

    init(viewModel: PopUpUserNumberViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.popupBackground
        setupLayoutPopUp()
        bindUI()
        viewModel.getСlassNumber()
    }

    // MARK: - Private Methods

    private func bindUI() {
        popUpUserNumberView.confirmUserNumberButtonTap
                .bind { [weak self] in
                    guard let self = self else { return }
                    self.viewModel.onUserNumberTap()
                    self.dismiss(animated: false)
                }
                .disposed(by: bag)

        viewModel
            .classNumber
            .bind { [weak self] text in
                self?.popUpUserNumberView.configure(title: text)
            }
            .disposed(by: viewModel.bag)
    }

    private func setupLayoutPopUp() {
        view.addSubview(popUpUserNumberView)
        popUpUserNumberView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(25)
        }
    }
}
