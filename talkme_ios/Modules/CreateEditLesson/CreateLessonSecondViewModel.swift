//
//  CreateLessonSecondViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 21.01.2021.
//

import RxSwift
import RxCocoa

final class CreateLessonSecondViewModel {

    enum Flow {
        case showThirdController(streamData: StreamData)
        case alertCoinsWarning(title: String, message: String)
    }

    // MARK: - Public Properties

    let dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    let height = PublishRelay<Void>()
    let flow = PublishRelay<Flow>()
    var bag = DisposeBag()
    var isCorrectDate: Bool = false
    let error = PublishRelay<IndexPath>()
    let showError = PublishRelay<Void>()
    let showCurrentTimeError = PublishRelay<Void>()
    var isNextVC = true

    // MARK: - Private properties

    private lazy var timeString = streamData.lessonTime ??
    Formatters.setupLessonDate(streamData.date) ??
    Formatters.setupTodayTime()
    private let lessonCount = PublishRelay<[Float]>()
    private let screenType: CreateLessonScreenType
    private let streamData: StreamData
    private let service: MainServiceProtocol
    private let lessonService: LessonsServiceProtocol
    private var lessonsArray: [Lesson] = []
    private let paymentsService = PaymentsService()
    private var getCoin = 0

    // MARK: - Initializers

    init(service: MainServiceProtocol, streamData: StreamData, lessonService: LessonsServiceProtocol, screenType: CreateLessonScreenType) {
        self.service = service
        self.streamData = streamData
        self.lessonService = lessonService
        self.screenType = screenType
    }

    // MARK: - Public Methods

    func chekCurrentTime() {
        isCorrectDate = checkCorrectDate()
        guard !isCorrectDate else { return }
        showCurrentTimeError.accept(())
    }

    func fetchStreams() {
        Single.zip(lessonService.live(pageNumber: 1, lessonsInPage: 100), lessonService.myBroadcasts(page: 100))
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success((_, let lessons)):
                    guard let lesson = lessons.lessons,
                          let lessonsSoon = lessons.lessonsSoon else { return }
                    self.lessonsArray.append(contentsOf: lessonsSoon)
                    self.lessonsArray.append(contentsOf: lesson)
                    self.chekCurrentTime()
                    guard !self.lessonsArray.isEmpty else {
                        if self.isCorrectDate {
                            self.setStartTime()
                            self.flow.accept(.showThirdController(streamData: self.streamData))
                        }
                        return
                    }
                    for lesson in self.lessonsArray {
                        let streamStartTime = Double(self.streamData.lessonStartTime ?? "")
                        let streamEndTime = (
                            Double(self.streamData.lessonStartTime ?? "") ?? 0.0)
                        + (self.setupTimeToseconds(time: self.streamData.lessonDuration)
                        )
                        let lessonEndMyBroadcasts = self.setupTimeToseconds(time: lesson.lessonTime) + (lesson.date ?? 0.0)

                        if streamStartTime ?? 0.0 >= lesson.date ?? 0.0 && streamStartTime ?? 0.0 <= lessonEndMyBroadcasts ||
                            streamEndTime >= lesson.date ?? 0.0 && streamEndTime <= lessonEndMyBroadcasts {
                            switch self.screenType {
                            case .create:
                                self.isCorrectDate = false
                                self.showError.accept(())
                            case .edit:
                                if !(self.streamData.id == lesson.id) {
                                    self.isCorrectDate = false
                                    self.showError.accept(())
                                }
                            }
                        }
                    }
                    guard self.isCorrectDate else { return }
                    self.setStartTime()
                    self.flow.accept(.showThirdController(streamData: self.streamData))
                    self.lessonsArray = []
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func setupTimeToseconds(time: String? = nil) -> Double {
        let hoursString = time?.components(separatedBy: ":")
        let hour = (Double((hoursString?[0] ?? "")) ?? 0) * 60
        let seconds = (Double((hoursString?[1] ?? "")) ?? 0)
        let result = (hour + seconds) * 60
        return result
    }

    private func checkCorrectDate() -> Bool {
        var streamStartTime = Double(streamData.lessonStartTime ?? "0") ?? 0
        streamStartTime -= Date.diffrenceBetweenRealTime
        let nowTime = Date().timeIntervalSince1970
        return streamStartTime >= (nowTime - 60)
    }

    private func showErrors(
        lessonDateChange: LessonDateChangeViewModel,
        lessonTimeChange: LessonTimeChangeViewModel,
        membersCount: MembersCountViewModel
    ) -> Bool {
        var hasError = false

        showError
            .bind {
                self.isNextVC = true
                hasError = true
                lessonTimeChange.error.accept(.lessonTime)
                lessonDateChange.error.accept(.lessonDate)
                let indexPath = IndexPath(row: 1, section: 0)
                self.error.accept(indexPath)
            }
            .disposed(by: bag)

        showCurrentTimeError
            .bind {
                self.isNextVC = true
                hasError = true
                lessonTimeChange.error.accept(.currentTime)
                let indexPath = IndexPath(row: 1, section: 0)
                self.error.accept(indexPath)
            }
            .disposed(by: bag)

        return hasError
    }

    func requestGetCoin() {
        paymentsService.getCoin()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let budgetResponse):
                    self.getCoin = budgetResponse.coin
                case .error(let error):
                    print(error)
                }
            }.disposed(by: bag)
    }

    func setupItems() {
        let indicatorViewModel = LessonSelectionIndicatorViewModel(tag: 2)
        let lessonDateViewModel = LessonDateChangeViewModel(
            title: "create_lesson_stream_date".localized,
            placeholder: "",
            text: Formatters.lessonDateString(streamData.date) ?? Formatters.setupTodayDate()
        )
        let lessonTimeViewModel = LessonTimeChangeViewModel(
            title: "create_lesson_beginning_time".localized,
            placeholder: "19:00",
            text: timeString
        )

        let membersCountViewModel = MembersCountViewModel(
            title: "create_lesson_members_number".localized,
            placeholder: "",
            text: membersCount(count: streamData.subscriptions)
        )

        let lessonPriceViewModel = LessonPriceViewModel(
            title: "create_lesson_stream_price".localized,
            placeholder: "0",
            cost: String(streamData.cost ?? 0)
        )
        let lessonDurationViewModel = LessonDurationViewModel(
            title: "create_lesson_stream_time".localized,
            placeholder: "1:00",
            text: Formatters.setupHourAndMinuteWithout0(streamData.lessonDuration)
        )
        let priceForCreateLessonVM = PriceForCreateLessonViewModel()
        let showNextButtonVM = ShowNextButtonCellViewModel()

        let dataItems: [AnyTableViewCellModelProtocol] = [
            indicatorViewModel,
            lessonDateViewModel,
            lessonTimeViewModel,
            membersCountViewModel,
            lessonPriceViewModel,
            lessonDurationViewModel,
            priceForCreateLessonVM,
            showNextButtonVM]

        self.dataItems.accept(dataItems)

        onGetLessonsCount(lessonTimeViewModel: lessonTimeViewModel)
        setupData(lessonDateViewModel: lessonDateViewModel,
                  lessonTimeViewModel: lessonTimeViewModel,
                  membersCountViewModel: membersCountViewModel,
                  lessonPriceViewModel: lessonPriceViewModel,
                  lessonDurationViewModel: lessonDurationViewModel)

        bindUI(lessonDateViewModel: lessonDateViewModel,
               lessonTimeViewModel: lessonTimeViewModel,
               membersCountViewModel: membersCountViewModel,
               showNextButtonVM: showNextButtonVM)

        // TODO: Переделать цену, когда сделают на бэкенде
        lessonDurationViewModel
            .sliderRelay
            .bind { [weak self] value in
                guard let self = self else { return }
                if value <= 5460 {
                    self.streamData.price = 0
                } else {
                    self.streamData.price = ((value - 6000) / 300 + 2) * 100 / 9
                }
                priceForCreateLessonVM.priceRelay.accept(self.streamData.price)
            }
            .disposed(by: lessonDurationViewModel.bag)
    }

    // MARK: - Private Methods

    private func membersCount(count: Int?) -> String {
        guard let count = count else { return "10 000" }
        return String(Formatters.convertCount(number: count))
    }

    private func setStartTime() {
        let date = Date()
        var newDataString: String?
        var newTimeString: String?
        newDataString = streamData.lessonDate == nil
        ? Formatters.lessonDateToString(self.streamData.date)
        : streamData.lessonDate

        newTimeString = streamData.lessonTime == nil
        ? Formatters.setupLessonDate(self.streamData.date)
        : streamData.lessonTime
        let currentDateString = Formatters.dateFormatterDefault.string(from: date)
        let currentTimeString = Formatters.timeFormatter.string(from: date)
        let timeString = (newDataString ?? currentDateString) + " " + (newTimeString ?? currentTimeString) + ":00"
        let lessonTime = Formatters.createLessonTimeFormatter.date(from: timeString)
        let timeStamp = lessonTime?.timeIntervalSince1970 ?? 0
        let lessonTimeStamp = String(format: "%.0f", timeStamp + Date.diffrenceBetweenRealTime)
        self.streamData.lessonStartTime = lessonTimeStamp
    }

    private func bindUI(
        lessonDateViewModel: LessonDateChangeViewModel,
        lessonTimeViewModel: LessonTimeChangeViewModel,
        membersCountViewModel: MembersCountViewModel,
        showNextButtonVM: ShowNextButtonCellViewModel
    ) {
        lessonDateViewModel
            .lessonDate
            .bind(to: lessonTimeViewModel.lessonDate)
            .disposed(by: lessonDateViewModel.bag)

        membersCountViewModel
            .buttonIsEnabled
            .bind(to: showNextButtonVM.buttonIsEnabled)
            .disposed(by: membersCountViewModel.bag)

        showNextButtonVM
            .tapRelay
            .bind { [weak self] _ in
                guard let isNextVC = self?.isNextVC, let self = self, isNextVC else { return }
                if self.streamData.price <= self.getCoin {
                    self.isNextVC = false
                    self.setStartTime()
                    self.fetchStreams()
                } else {
                    self.flow.accept(.alertCoinsWarning(
                        title: "not_enough_coins".localized,
                        message: "top_up_your_account".localized)
                    )
                }
                guard !self.showErrors(
                    lessonDateChange: lessonDateViewModel,
                    lessonTimeChange: lessonTimeViewModel,
                    membersCount: membersCountViewModel
                ) else { return }
            }
            .disposed(by: showNextButtonVM.bag)

        lessonCount
            .bind(to: lessonTimeViewModel.lessonCount)
            .disposed(by: bag)
    }

    private func setupData(
        lessonDateViewModel: LessonDateChangeViewModel,
        lessonTimeViewModel: LessonTimeChangeViewModel,
        membersCountViewModel: MembersCountViewModel,
        lessonPriceViewModel: LessonPriceViewModel,
        lessonDurationViewModel: LessonDurationViewModel
    ) {
        lessonDateViewModel
            .lessonDate
            .bind { [weak self] date in
                self?.streamData.lessonDate = Formatters.dateFormatterDefault.string(from: date)
            }
            .disposed(by: lessonDateViewModel.bag)

        lessonTimeViewModel
            .textRelay
            .bind { [weak self] text in
                self?.streamData.lessonTime = text
            }
            .disposed(by: lessonTimeViewModel.bag)

        lessonTimeViewModel
            .lessonTime
            .bind { [weak self] date in
                self?.streamData.lessonTime = Formatters.timeFormatter.string(from: date)
            }
            .disposed(by: lessonTimeViewModel.bag)

        membersCountViewModel
            .textRelay
            .bind { [weak self] text in
                guard let text = text?.replacingOccurrences(of: " ", with: ""), let count = Int(text) else { return }
                self?.streamData.subscriptions = count
            }
            .disposed(by: membersCountViewModel.bag)

        lessonPriceViewModel
            .textRelay
            .bind { [weak self] text in
                guard let cost = Int(text ?? "") else {
                    self?.streamData.cost = 0
                    return }
                self?.streamData.cost = cost
            }
            .disposed(by: lessonPriceViewModel.bag)

        lessonPriceViewModel
            .clearCost
            .bind { [weak self] cost in
                self?.streamData.cost = Int(cost)
            }
            .disposed(by: lessonPriceViewModel.bag)

        lessonDurationViewModel
            .onLoadText
            .bind { [weak self] text in
                self?.streamData.lessonDuration = text
            }
            .disposed(by: lessonDurationViewModel.bag)

        lessonDurationViewModel
            .textRelay
            .map { text in
                let duration = Formatters.timeFormatterWithout0.date(from: text ?? "0:30")
                return Formatters.timeFormatter.string(from: duration ?? Date())
            }
            .bind { [weak self] text in
                self?.streamData.lessonDuration = text
            }
            .disposed(by: lessonDurationViewModel.bag)
    }

    private func onGetLessonsCount(lessonTimeViewModel: LessonTimeChangeViewModel) {
        lessonTimeViewModel
            .onGetLessonsCount
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.getLessonsCount(date: self.streamData.lessonDate ?? "", category: String(self.streamData.category ?? 0))
            }
            .disposed(by: lessonTimeViewModel.bag)
    }

    private func getLessonsCount(date: String, category: String) {
        service
            .dateInterval(date: date, category: String(self.streamData.category ?? 0))
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.setupLessonsCount(dateIntervalResponse: response)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func setupLessonsCount(dateIntervalResponse: DateIntervalResponse) {
        let dateIntervals = dateIntervalResponse
            .dateInterval
            .flatMap { $0 }
            .compactMap { item -> (String, Int)? in
                guard let dateString = item.key.components(separatedBy: ", ").first else { return nil }
                return (dateString, item.value)
            }
            .compactMap { item -> LessonsCountPerQuater? in
                guard let date = Formatters.lessonTimeFormatter.date(from: item.0) else { return nil }
                return LessonsCountPerQuater(date: date, count: Float(item.1))
            }

        let hourGrouppes = Dictionary(grouping: dateIntervals) {
            Calendar.current.component(.hour, from: $0.date)
        }
            .map(\.value)
            .sorted { lessons1, lessons2 -> Bool in
                guard let firstDate = lessons1.first?.date, let secondDate = lessons2.first?.date else { return false }
                return firstDate < secondDate
            }

        let lessonsCount = hourGrouppes.map { $0.map { $0.count }.reduce(0.0, +) / 4.0 }
        self.lessonCount.accept(lessonsCount)
    }
}
