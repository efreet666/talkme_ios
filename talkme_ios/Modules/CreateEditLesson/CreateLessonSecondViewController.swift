//
//  CreateLessonSecondViewController.swift
//  talkme_ios
//
//  Created by 1111 on 21.01.2021.
//

import RxSwift
import RxCocoa

final class CreateLessonSecondViewController: UIViewController {

    // MARK: - Public properties

    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.tableFooterView = UIView()
        tbv.rowHeight = UITableView.automaticDimension
        tbv.bounces = false
        tbv.delaysContentTouches = false
        tbv.keyboardDismissMode = .onDrag
        tbv.backgroundColor = TalkmeColors.mainAccountBackground
        tbv.registerCells(
            withModels:
            LessonSelectionIndicatorViewModel.self,
            LessonDateChangeViewModel.self,
            LessonTimeChangeViewModel.self,
            MembersCountViewModel.self,
            LessonPriceViewModel.self,
            LessonDurationViewModel.self,
            PriceForCreateLessonViewModel.self,
            ShowNextButtonCellViewModel.self)
        return tbv
    }()

    private let dismissButton: UIBarButtonItem = {
        let dismissButton = UIButton(type: .custom)
        let navigationButton = UIBarButtonItem(customView: dismissButton)
        dismissButton.setImage(UIImage(named: "dismissController"), for: .normal)
        return navigationButton
    }()

    private let viewModel: CreateLessonSecondViewModel

    // MARK: - Init

    init(viewModel: CreateLessonSecondViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
        setupTableView()
        viewModel.requestGetCoin()
        viewModel.setupItems()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideCustomTabBarLiveButton(true)
        viewModel.isNextVC = true
        setupNavigation()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideCustomTabBarLiveButton(false)
    }

    // MARK: - Private Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        viewModel.error
            .bind { [weak self ] value in
                self?.tableView.scrollToRow(at: value, at: .top, animated: true)
            }
            .disposed(by: viewModel.bag)
    }

    private func setupTableView() {
        tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: UIScreen.main.bounds.width / 3.1, right: 0.0)
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNavigation() {
        customNavigationController?.style = .plainWhite(title: "create_lesson_navigation_title".localized)
    }
}
