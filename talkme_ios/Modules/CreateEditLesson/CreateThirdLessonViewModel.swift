//
//  CreateThirdLessonViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 29.01.2021.
//

import RxSwift
import RxCocoa

final class CreateThirdLessonViewModel {

    enum Flow {
        case onMyLessons
    }

    // MARK: - Public Properties

    let flow = PublishRelay<Flow>()
    private(set) lazy var items = Driver<[AnyTableViewCellModelProtocol]>.just([
        indicatorVM,
        lessonPromotionVM,
        totalPriceVM,
        confirmButtonVM
    ])

    var params: CreateLessonRequest
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let indicatorVM = LessonSelectionIndicatorViewModel(tag: 3)
    private let service: MainServiceProtocol
    private let lessonPromotionVM = LessonPromotionViewModel(
        title: "create_lesson_promotion".localized,
        viewLabel: "create_lesson_without_promotion".localized
    )
    private let totalPriceVM = TotalPriceCellViewModel()
    private let confirmButtonVM = ConfirmButtonCellViewModel()
    private let id: Int?

    // MARK: - Initializers

    init(service: MainServiceProtocol, streamData: StreamData) {
        self.service = service
        self.params = .init(streamData: streamData)
        self.id = streamData.id
        bind()
        totalPriceVM.setPrice(price: streamData.price)
    }

    // MARK: - Private Methods

    private func createLesson() {
        service
            .createLesson(request: params)
            .subscribe { [weak self] event in
                switch event {
                case .success(let result):
                    self?.flow.accept(.onMyLessons)
                    guard let descriptionError = result.description?.first else { return }
                    AlertControllerHelper.showSimpleOkAlert(title: descriptionError)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func updateLesson() {
        guard let id = id else {
            self.createLesson()
            return
        }
        service
            .updateLesson(request: params, id: id)
            .subscribe { [weak self] event in
                switch event {
                case .success:
                    self?.flow.accept(.onMyLessons)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func bind() {
        confirmButtonVM
            .tapRelay
            .bind { [weak self] _ in
                self?.updateLesson()
            }
            .disposed(by: bag)
    }
}
