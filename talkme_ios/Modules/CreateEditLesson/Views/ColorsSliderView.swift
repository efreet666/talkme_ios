//
//  ColorsSliderStackView.swift
//  talkme_ios
//
//  Created by 1111 on 26.01.2021.
//

import UIKit

final class SliderTrackView: UIView {

    private let sliderTrackView: UIView = {
        let view = UIView()
        return view
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(sliderTrackView)

        sliderTrackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 6 : 7)
        }
    }
}
