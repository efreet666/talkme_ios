//
//  CreateLessonError.swift
//  talkme_ios
//
//  Created by Майя Калицева on 02.02.2021.
//

enum CreateLessonError {
    case name
    case category
    case subcategory
    case picture
    case title
    case clear
    case lessonTime
    case lessonDate
    case currentTime

    var title: String? {
        switch self {
        case .name:
            return "create_lesson_error_title".localized
        case .category:
            return "create_lesson_error_category".localized
        case .subcategory:
        return "create_lesson_error_subcategory".localized
        case .picture:
            return "create_lesson_error_picture".localized
        case .title:
            return "create_lesson_error_title".localized
        case .lessonTime:
            return "create_lesson_error_time".localized
        case .lessonDate:
            return "create_lesson_error_date".localized
        case .currentTime:
            return "create_lesson_current_time_error".localized
        case .clear:
            return ""
        }
    }
}
