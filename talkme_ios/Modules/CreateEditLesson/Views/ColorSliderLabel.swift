//
//  ColorSliderLabel.swift
//  talkme_ios
//
//  Created by 1111 on 26.01.2021.
//

import UIKit

final class SliderLabel: UIView {

    var sliderLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 11 : 13)
        label.textColor = TalkmeColors.placeholderColor
        return label
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubview(sliderLabel)

        sliderLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
