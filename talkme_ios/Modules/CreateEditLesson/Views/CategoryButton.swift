//
//  CategoryItem.swift
//  talkme_ios
//
//  Created by 1111 on 30.01.2021.
//

import UIKit

final class CategoryButton: UIButton {

    private enum Constants {
        static let cornerRadius: CGFloat = UIScreen.isSE ? 15 : 20
        static let borderWidth: CGFloat = 1
        static let borderRegularColor = TalkmeColors.grayCollectionCell
        static let borderSelectedColor = TalkmeColors.blueLabels
        static let fontStyle = UIFont.montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        static let backgroundColor = TalkmeColors.white
        static let textRegularColor = TalkmeColors.grayLabels
        static let textSelectedColor = TalkmeColors.blueLabels
    }

    enum CategoryButtonType {
        case selectedActive
        case regular

        var borderColor: CGColor? {
            switch self {
            case .regular:
                return Constants.borderRegularColor.cgColor
            case .selectedActive:
                return Constants.borderSelectedColor.cgColor
            }
        }

        var textColor: UIColor? {
            switch self {
            case .regular:
                return Constants.textRegularColor
            case .selectedActive:
                return Constants.textSelectedColor
            }
        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    convenience init(type: CategoryButtonType) {
        self.init(frame: .zero)
        layer.borderColor = type.borderColor
        setTitleColor(type.textColor, for: .normal)
        }

    // MARK: - Private Methods

    private func initialSetup() {
        clipsToBounds = true
        layer.cornerRadius = Constants.cornerRadius
        layer.borderWidth = Constants.borderWidth
        titleLabel?.font = Constants.fontStyle
        backgroundColor = Constants.backgroundColor
        layer.masksToBounds = true
        titleLabel?.textAlignment = .center
        contentEdgeInsets = UIEdgeInsets(top: 7, left: 10, bottom: 7, right: 10)
        isEnabled = false
    }
}
