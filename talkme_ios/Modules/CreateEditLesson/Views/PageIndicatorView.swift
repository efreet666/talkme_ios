//
//  PageIndicatorView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 30.01.2021.
//

import UIKit

final class PageIndicatorView: UIView {

    let firstIndicator: UIView = {
        let ind = UIView()
        ind.layer.cornerRadius = 1.5
        return ind
    }()

    let secondIndicator: UIView = {
        let ind = UIView()
        ind.layer.cornerRadius = 1.5
        return ind
    }()

    let thirdIndicator: UIView = {
        let ind = UIView()
        ind.layer.cornerRadius = 1.5
        return ind
    }()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setViewActiveWith(tag: Int) {
        switch tag {
        case 1:
            firstIndicator.backgroundColor = TalkmeColors.greenIndicator
            secondIndicator.backgroundColor = TalkmeColors.grayIndicator
            thirdIndicator.backgroundColor = TalkmeColors.grayIndicator
        case 2:
            firstIndicator.backgroundColor = TalkmeColors.grayIndicator
            secondIndicator.backgroundColor = TalkmeColors.greenIndicator
            thirdIndicator.backgroundColor = TalkmeColors.grayIndicator
        case 3:
            firstIndicator.backgroundColor = TalkmeColors.grayIndicator
            secondIndicator.backgroundColor = TalkmeColors.grayIndicator
            thirdIndicator.backgroundColor = TalkmeColors.greenIndicator
        default:
            break
        }
    }

    private func setupLayout() {
        addSubviews([firstIndicator, secondIndicator, thirdIndicator])

        firstIndicator.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(20)
            make.height.equalTo(3)
        }

        secondIndicator.snp.makeConstraints { make in
            make.leading.equalTo(firstIndicator.snp.trailing).offset(12)
            make.top.height.width.equalTo(firstIndicator)
        }

        thirdIndicator.snp.makeConstraints { make in
            make.leading.equalTo(secondIndicator.snp.trailing).offset(12)
            make.top.height.width.equalTo(secondIndicator)
            make.trailing.equalToSuperview().inset(10)
        }
    }
}
