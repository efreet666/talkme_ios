//
//  LessonCommonTextField.swift
//  talkme_ios
//
//  Created by 1111 on 28.01.2021.
//

import UIKit

final class LessonCommonTextField: UITextField {

    // MARK: - Initializers

    convenience init(leftInset: CGFloat) {
        self.init(frame: .zero)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: leftInset, height: self.frame.height))
        leftView = paddingView
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.frame.height))
        leftView = paddingView
        leftViewMode = .always
        layer.cornerRadius = UIScreen.isSE ? 18 : 23.5
        backgroundColor = TalkmeColors.white
        font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        minimumFontSize = UIScreen.isSE ? 13 : 15
        textColor = TalkmeColors.placeholderColor
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
