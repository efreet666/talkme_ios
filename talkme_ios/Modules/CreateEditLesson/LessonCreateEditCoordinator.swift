//
//  LessonCreateEditCoordinator.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 19.02.2021.
//

import RxSwift
import RxCocoa

final class LessonCreateEditCoordinator {

    enum Flow {
        case completeCreationEdit
    }

    // MARK: - Public properties

    let flow = PublishRelay<Flow>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let mainService = MainService()
    private let lessonService = LessonsService()
    private weak var navigationController: UINavigationController?

    // MARK: - Public Methods

    func start(_ navigationController: UINavigationController?, lessonID: Int?, type: CreateLessonScreenType) {
        self.navigationController = navigationController
        let model = CreateLessonViewModel(service: mainService, id: lessonID, screenType: type)
        let vc = CreateLessonViewController(viewModel: model)
        bindEditViewControllerModel(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func bindEditViewControllerModel(_ model: CreateLessonViewModel) {
        model.flow
            .bind { [weak self] event in
                switch event {
                case .showSecondController(let params, let screenType):
                    self?.showSecondEditVC(with: params, screenType: screenType)
                    return
                }
            }.disposed(by: model.bag)
    }

    private func showSecondEditVC(with streamData: StreamData, screenType: CreateLessonScreenType) {
        let model = CreateLessonSecondViewModel(service: mainService, streamData: streamData, lessonService: lessonService, screenType: screenType)
        let vc = CreateLessonSecondViewController(viewModel: model)
        bindSecondEditVM(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func bindSecondEditVM(_ model: CreateLessonSecondViewModel) {
        model.flow
            .bind { [weak self] event in
                switch event {
                case .showThirdController(let params):
                    self?.showThirdEditVC(with: params)
                case .alertCoinsWarning(let title, let message):
                    self?.alertCoinsWarning(title: title, message: message)
                }
            }.disposed(by: model.bag)
    }

    private func alertCoinsWarning(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "general_ok".localized, style: .default, handler: { _ in })
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func showThirdEditVC(with streamData: StreamData) {
        let model = CreateThirdLessonViewModel(service: mainService, streamData: streamData)
        let vc = CreateThirdLessonViewController(viewModel: model)
        bindThirdEditVM(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func bindThirdEditVM(_ model: CreateThirdLessonViewModel) {
        model.flow
            .bind { [weak self] event in
                switch event {
                case .onMyLessons:
                    self?.flow.accept(.completeCreationEdit)
                }
            }.disposed(by: model.bag)
    }
}
