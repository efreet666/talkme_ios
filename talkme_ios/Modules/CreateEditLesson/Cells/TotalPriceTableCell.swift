//
//  File.swift
//  talkme_ios
//
//  Created by 1111 on 29.01.2021.
//

import UIKit

final class TotalPriceTableCell: UITableViewCell {

    // MARK: - Private Properties

    private let containerView: PriceView = {
        let view = PriceView()
        let money = "0"
        view.configure(type: .totalPrice, price: money)
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setPrice(price: Int) {
        containerView.configure(type: .totalPrice, price: String(price))
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(17)
            make.bottom.equalToSuperview().inset(27)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 124 : 160)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
