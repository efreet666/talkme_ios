//
//  LessonCategoryTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.01.2021.
//

import RxSwift
import RxCocoa
import collection_view_layouts

final class LessonCategoryTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    let onItemSelected = PublishRelay<(Category, CGFloat)>()
    let onClearTFSubCategoryText = PublishRelay<Void>()
    let activeCategoryName = PublishRelay<Int>()

    // MARK: - Private Properties

    private var cellSizes = [CGSize]()
    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let customLayout: TagsLayout = {
        let layout = TagsLayout()
        layout.scrollDirection = .horizontal
        layout.cellsPadding = ItemsPadding(horizontal: 8, vertical: 10)
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: customLayout)
        cvc.backgroundColor = .clear
        cvc.registerCells(withModels: CategoryCollectionCellViewModel.self)
        cvc.showsHorizontalScrollIndicator = false
        return cvc
    }()

    private let errorLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.error
        lbl.font = .montserratFontMedium(ofSize: 11)
        lbl.textAlignment = .center
        return lbl
    }()

    private let firstIndicator: PageIndicatorView = {
        let ind = PageIndicatorView()
        return ind
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        customLayout.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, items: PublishRelay<[AnyCollectionViewCellModelProtocol]>, index: Int?, offset: CGFloat) {
        items
            .do(afterNext: { [weak self] _ in
                let selectedIndexPath = IndexPath(item: index ?? 1, section: 0)
                self?.collectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: [])
            })
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)

        titleLabel.text = title
        bindCollectionView()

        self.collectionView.contentOffset.x = offset
    }

    func setupWarning(type: CreateLessonError) {
        self.errorLabel.text = type.title
    }

    func setupCategoryNames(names: [String]) {
        cellSizes = names.map { item -> CGSize in
            let width = Double(self.collectionView.bounds.width)
            var size = UIFont.systemFont(ofSize: 13).sizeOfString(string: item, constrainedToWidth: width)
            size.width += UIScreen.isSE ? 40 : 70
            size.height += UIScreen.isSE ? 15 : 25
            return size
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindCollectionView() {
        collectionView.rx
            .modelSelected(CategoryCollectionCellViewModel.self)
            .bind { [weak self] item in
                guard let self = self else { return }
                self.onItemSelected.accept((item.categoryModel, self.collectionView.contentOffset.x))
                self.onClearTFSubCategoryText.accept(())
                self.activeCategoryName.accept(item.categoryModel.id)
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, isRequiredImage, collectionView, errorLabel, separatorView])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(10)
            make.leading.equalToSuperview().inset(10)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.bottom.equalToSuperview().inset(5)
            make.leading.equalToSuperview().inset(10)
            make.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 130 : 174)
        }

        errorLabel.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension LessonCategoryTableCell: LayoutDelegate {

    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.row]
    }
}
