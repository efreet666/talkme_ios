//
//  ShowNextButtonTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 18.01.2021.
//

import RxSwift

final class ShowNextButtonTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var buttonTap = saveButton.rx.tap
    private(set) lazy var buttonIsEnabled = saveButton.rx.isEnabled
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    let saveButton: UIButton = {
        let bt = UIButton()
        bt.backgroundColor = TalkmeColors.greenLabels
        bt.titleLabel?.font = .montserratExtraBold(ofSize: UIScreen.isSE ? 14 : 17)
        bt.titleLabel?.textColor = TalkmeColors.white
        bt.setTitle("create_lesson_next".localized, for: .normal)
        bt.layer.cornerRadius = UIScreen.isSE ? 21 : 27
        return bt
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    func buttonTouched(_ sender: UIButton) {
        sender.isEnabled = false
        UIButton.animate(
            withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
                sender.alpha = 0.5
            },
            completion: { _ in
                UIButton.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform.identity
                    sender.alpha = 1
                }, completion: { _ in
                    sender.isEnabled = true
                })
            })
    }

    private func setupLayout() {
        contentView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 32 : 42)
            make.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
            make.leading.trailing.equalToSuperview().inset(10)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
        separatorInset.right = .greatestFiniteMagnitude
    }
}
