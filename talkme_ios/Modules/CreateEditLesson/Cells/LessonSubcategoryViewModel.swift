//
//  LessonSubcategoryViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.01.2021.
//

import RxCocoa
import RxSwift

final class LessonSubcategoryViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let bag = DisposeBag()
    let isCustomSubcategory = PublishRelay<Bool>()
    let dataItemsRelay = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let onItemSelected = PublishRelay<String>()
    let dropDownItems = PublishRelay<[Category]?>()
    let warningRelay = PublishRelay<Void>()
    let onClearTextField = PublishRelay<Void>()
    let onEndTextField = PublishRelay<Void>()
    let textRelay = PublishRelay<String?>()
    let beginEditingSubcategory = PublishRelay<Bool>()
    var text: String?
    var handleText: String?

    // MARK: - Private Properties

    private let title: String
    private var cellItems: [Category] = []
    private var dataItems: [AnyCollectionViewCellModelProtocol] = []
    private var activeSubCategory: String?
    private var selectedIndex: Int?

    // MARK: - Initializers

    init(
        title: String,
        model: [Category],
        activeSubCategory: String? = nil,
        text: String,
        handleText: String
    ) {
        self.title = title
        self.text = text
        self.handleText = handleText
        self.activeSubCategory = activeSubCategory
        let items: [AnyCollectionViewCellModelProtocol] = model.map { SubCategoryCollectionCellViewModel(categoryModel: $0) }
        dataItemsRelay.accept(items)
        dropDownItems.accept(model)
        cellItems = model
        dataItems = items
    }

    // MARK: - Public Methods

    func clearDataItems() {
        dataItems.removeAll()
    }

    func configure(_ cell: LessonSubcategoryTableCell) {
        selectedIndex = cellItems.firstIndex(where: { self.activeSubCategory == $0.name })
        cell.setupSizes(items: cellItems.map { $0.name ?? "" })
        cell.configure(title: title, text: activeSubCategory, index: selectedIndex, anotherText: handleText)

        warningRelay
            .bind { [weak cell] _ in
                cell?.setupWarning(type: .subcategory)
            }
            .disposed(by: cell.bag)

        dataItemsRelay
            .bind(to: cell.dataItems)
            .disposed(by: cell.bag)

        dropDownItems
            .bind { [weak cell, weak self] items in
                cell?.setupDropDownItems(items: items?.map { $0.name ?? "" } ?? [])
                self?.isCustomSubcategory.accept(false)
            }
            .disposed(by: cell.bag)

        cell
            .onItemSelected
            .bind(to: onItemSelected)
            .disposed(by: cell.bag)

        cell
            .subCategoryText
            .bind { [weak self] text in
                guard let cellItems = self?.cellItems else { return }
                let filteredItems = cellItems.filter { ($0.name?.hasPrefix(text ?? "") ?? false) }
                self?.dropDownItems.accept(filteredItems)
            }
            .disposed(by: cell.bag)

        onClearTextField
            .bind { [weak cell] _ in
                cell?.clearTextFieldText()
            }
            .disposed(by: cell.bag)

        onEndTextField
            .bind { [weak cell] _ in
                cell?.endTextFieldText()
            }
            .disposed(by: cell.bag)

        onItemSelected
            .bind { [weak cell] name in
                self.cellItems.enumerated().forEach { (index, item) in
                    if item.name == name {
                        cell?.collectionItemSelected(item: index)
                    }
                }
            }
            .disposed(by: cell.bag)

        cell
            .isCustomSubcatecory
            .bind { [weak self] isCustom in
                guard isCustom else { return }
                self?.onItemSelected
                    .bind { [weak cell] _ in
                        cell?.activeSubCategoryName.accept("")
                    }
            }
            .disposed(by: cell.bag)

        cell
            .activeSubCategoryName
            .bind { [weak self] name in
                self?.activeSubCategory = name
                self?.text = name
                self?.handleText = name
            }
            .disposed(by: cell.bag)

        cell
            .textFieldText
            .do(onNext: { [weak self] in
                self?.text = $0
                self?.handleText = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .activeSubCategoryName
            .bind { [weak self] name in
                self?.activeSubCategory = name
                self?.isCustomSubcategory.accept(false)
            }
            .disposed(by: cell.bag)

        cell
            .isCustomSubcatecory
            .distinctUntilChanged()
            .bind { [weak self] isCustom in
                self?.isCustomSubcategory.accept(isCustom)
            }
            .disposed(by: cell.bag)

        cell
            .beginEditingSubcategory
            .bind(to: beginEditingSubcategory)
            .disposed(by: cell.bag)

        dataItemsRelay.accept(dataItems)
    }
}
