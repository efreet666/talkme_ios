//
//  TotalPriceCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 29.01.2021.
//

import RxSwift
import RxCocoa

final class TotalPriceCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    let priceRelay = BehaviorRelay<Int>(value: 10)
    let bag = DisposeBag()

    // MARK: - Public methods

    func setPrice(price: Int) {
        priceRelay.accept(price)
    }

    func configure(_ cell: TotalPriceTableCell) {
        priceRelay
            .bind { [weak cell] number in
                cell?.setPrice(price: number)
            }
            .disposed(by: bag)
    }
}
