//
//  LessonLengthTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxSwift
import RxCocoa

final class LessonDurationTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var lessonTimeText = lessonTimeTF.rx.text
    private(set) lazy var lessonTimeSlider = timeSlider.rx.value
    private(set) lazy var sliderRelay = BehaviorRelay<Int>(value: 0)
    let onLoadText = PublishRelay<String>()
    private(set) var bag = DisposeBag()
    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    // MARK: - Private Properties

    private let label0: SliderLabel = {
        let label = SliderLabel()
        label.sliderLabel.text = "30 мин"
        return label
    }()

    private let label1: SliderLabel = {
        let label = SliderLabel()
        label.sliderLabel.text = "1 час"
        return label
    }()

    private let label2: SliderLabel = {
        let label = SliderLabel()
        label.sliderLabel.text = "2 часа"
        return label
    }()

    private let label3: SliderLabel = {
        let label = SliderLabel()
        label.sliderLabel.text = "3 часа"
        return label
    }()

    private let label4: SliderLabel = {
        let label = SliderLabel()
        label.sliderLabel.textAlignment = .right
        label.sliderLabel.text = "5 часов"
        return label
    }()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let lessonTimeTF = LessonCommonTextField()

    private let timeButton: UIButton = {
        let btn = UIButton()
        btn.contentMode = .scaleAspectFill
        btn.setImage(UIImage(named: "lessonLength"), for: .normal)
        return btn
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: 10)
        return lbl
    }()

    private let timeSliderContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    private let timeSlider: UISlider = {
        let slider = CustomSlider()
        slider.minimumValue = 1800
        slider.maximumValue = 18000
        return slider
    }()

    private let datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .countDownTimer
        datePicker.minuteInterval = 5
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.countDownDuration = 1800
        return datePicker
    }()

    private let doneButton = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
        timeSlider.value = 3600
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, placeholder: String, text: String?) {
        bindUI()
        titleLabel.text = title
        lessonTimeTF.placeholder = placeholder
        lessonTimeTF.text = text
        guard let text = text else { return }
        timeSlider.value = Float(setupTimeToseconds(time: text))
        self.onLoadText.accept(text)
        updateDatePicker(text: text)
    }

    // MARK: - Private Methods

    private func setupTimeToseconds(time: String? = nil) -> Int {
        if let time = time {
            let hoursString = time.components(separatedBy: ":")
            let hour = (Int((hoursString[0])) ?? 0) * 60
            let seconds = (Int((hoursString[1])) ?? 0)
            let result = (hour + seconds) * 60
            return result
        } else {
            let timeString = (Formatters.timeFormatter.string(from: self.datePicker.date))
            let hoursString = timeString.components(separatedBy: ":")
            let countOfSeconds = ( (Int(hoursString[0]) ?? 0) * 60 + (Int(hoursString[1]) ?? 0) ) * 60
            return countOfSeconds
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func updateDatePicker(text: String?) {
        guard let text = text,
              let date = Formatters.timeFormatter.date(from: text) else {
            return
        }
        datePicker.setDate(date, animated: true)
    }

    private func bindUI() {
        timeButton.rx.tap
            .bind { [weak self] in
                self?.lessonTimeTF.becomeFirstResponder()
            }
            .disposed(by: bag)

        doneButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                if self.datePicker.countDownDuration > 18000 {
                    self.datePicker.countDownDuration = 18000
                           }
                let date = Formatters.timeFormatterWithout0.string(from: self.datePicker.date)
                self.lessonTimeTF.text = date
                self.timeSlider.value = Float(self.setupTimeToseconds())
                self.sliderRelay.accept(Int(self.timeSlider.value))
                self.contentView.endEditing(true)
            }
            .disposed(by: bag)

        timeSlider.rx.value
            .bind { [weak self] slider in
                guard let self = self else { return }
                self.lessonTimeTF.text = Formatters.durationFormatter.string(from: TimeInterval(slider.constraintPriorityTargetValue))
                self.sliderRelay.accept(Int(slider))
            }
            .disposed(by: bag)
    }

    private func setupUI() {
        lessonTimeTF.inputView = datePicker
        setupToolbar()
    }

    private func setupToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: true)
        lessonTimeTF.inputAccessoryView = toolBar
    }

    private func setupLayout() {
        timeSliderContainer.addSubviews([timeSlider, label0, label1, label2, label3, label4 ])
        contentView.addSubviews([titleLabel, lessonTimeTF, warningLabel, timeSliderContainer, timeButton, isRequiredImage])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(20)
            make.leading.equalToSuperview().inset(15)
        }

        lessonTimeTF.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 11)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        timeButton.snp.makeConstraints { make in
            make.centerY.equalTo(lessonTimeTF.snp.centerY)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 28 : 31)
            make.height.equalTo(UIScreen.isSE ? 17 : 22)
            make.width.equalTo(UIScreen.isSE ? 15 : 19)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(lessonTimeTF.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(20)
        }

        timeSliderContainer.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.top.equalTo(warningLabel.snp.bottom)
            make.trailing.leading.equalToSuperview().inset(10)
        }

        timeSlider.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
        }

        label0.snp.makeConstraints { make in
            make.top.equalTo(timeSlider.snp.bottom).offset(13)
            make.leading.equalToSuperview()
            make.trailing.equalTo(label2.snp.leading)
            make.bottom.equalToSuperview()
        }

        label1.snp.makeConstraints { make in
            make.top.equalTo(timeSlider.snp.bottom).offset(13)
            make.leading.equalTo(label0).offset(UIScreen.main.bounds.width * 0.15)
            make.trailing.equalTo(label2.snp.leading)
            make.bottom.equalToSuperview()
        }

        label2.snp.makeConstraints { make in
            make.top.equalTo(timeSlider.snp.bottom).offset(13)
            make.leading.equalTo(label1).offset(UIScreen.main.bounds.width * 0.17)
            make.trailing.equalTo(label3.snp.leading)
            make.bottom.equalToSuperview()
        }

        label3.snp.makeConstraints { make in
            make.top.equalTo(timeSlider.snp.bottom).offset(13)
            make.leading.equalTo(label2).offset(UIScreen.main.bounds.width * 0.22)
            make.trailing.equalTo(label4.snp.leading)
            make.bottom.equalToSuperview()
        }

        label4.snp.makeConstraints { make in
            make.top.equalTo(timeSlider.snp.bottom).offset(13)
            make.trailing.equalTo(timeSlider)
            make.bottom.equalToSuperview()
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
