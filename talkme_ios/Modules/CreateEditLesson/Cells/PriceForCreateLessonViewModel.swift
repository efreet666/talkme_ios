//
//  PriceForCreateLessonViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxCocoa
import RxSwift

final class PriceForCreateLessonViewModel: TableViewCellModelProtocol {

    // MARK: - Public properties

    lazy var priceRelay = PublishRelay<Int>()

    // MARK: - Public properties

    func configure(_ cell: PriceForCreateLessonTableCell) {
        priceRelay
            .bind { [weak cell] number in
                cell?.setPrice(price: number)
            }
            .disposed(by: cell.bag)
    }
}
