//
//  LessonDateChangeTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 21.01.2021.
//

import RxSwift
import RxCocoa

final class LessonDateChangeTableCell: UITableViewCell {

    // MARK: - Public Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))
    private(set) lazy var lessonDateText = lessonDateTF.rx.text
    let lessonDate = PublishRelay<Date>()
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let lessonDateTF = LessonCommonTextField()

    private let dateButton: UIButton = {
        let btn = UIButton()
        btn.contentMode = .scaleAspectFit
        btn.setImage(UIImage(named: "calendar"), for: .normal)
        btn.setTitleColor(TalkmeColors.white, for: .normal)
        return btn
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: 13)
        return lbl
    }()

    private let datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let minDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        datePicker.minimumDate = minDate
        return datePicker
    }()

    private let doneButton = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, placeholder: String, text: String?) {
        bindUI()
        titleLabel.text = title
        lessonDateTF.placeholder = placeholder
        guard let dateText = text?.replacingOccurrences(of: "..", with: ".") else { return }
        lessonDateTF.text = dateText
        updateDatePicker(text: text)
    }

    func setupError(_ error: CreateLessonError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubviews([isRequiredImage, titleLabel, lessonDateTF, warningLabel, dateButton])

        titleLabel.snp.makeConstraints { label in
            label.top.equalToSuperview().offset(15)
            label.leading.equalToSuperview().inset(15)
        }

        lessonDateTF.snp.makeConstraints { textView in
            textView.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 11)
            textView.leading.trailing.equalToSuperview().inset(10)
            textView.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        dateButton.snp.makeConstraints { button in
            button.centerY.equalTo(lessonDateTF.snp.centerY)
            button.trailing.equalTo(lessonDateTF).inset(UIScreen.isSE ? 18 : 20)
            button.size.equalTo(UIScreen.isSE ? 15 : 19)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(lessonDateTF.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
            make.height.equalTo(20)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }
    }

    private func updateDatePicker(text: String?) {
        guard let text = text,
              let date = Formatters.lessonDateformatter.date(from: text) else {
            return
        }
        datePicker.setDate(date, animated: true)
    }

    private func bindUI() {
        dateButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.lessonDateTF.becomeFirstResponder()
            }
            .disposed(by: bag)

        doneButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.lessonDateTF.text = Formatters.lessonDateformatter.string(from: self.datePicker.date).replacingOccurrences(of: "..", with: ".")
                self.contentView.endEditing(true)
                self.lessonDate.accept(self.datePicker.date)
            }
            .disposed(by: bag)
    }

    private func setupUI() {
        lessonDateTF.inputView = datePicker
        let correctString = Formatters.lessonDateformatter.string(from: self.datePicker.date).replacingOccurrences(of: "..", with: ".")
        self.lessonDateTF.text = correctString
        setupToolbar()
    }

    private func setupToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: true)
        lessonDateTF.inputAccessoryView = toolBar
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
