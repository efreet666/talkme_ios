//
//  LessonSubcategoryTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 17.01.2021.
//

import RxSwift
import RxCocoa
import DropDown
import collection_view_layouts

final class LessonSubcategoryTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()
    private(set) lazy var subCategoryText = textField.rx.text
    lazy var textFieldText = textField.rx.text
    let dataItems = PublishRelay<[AnyCollectionViewCellModelProtocol]>()
    let onItemSelected = PublishRelay<String>()
    let isCustomSubcatecory = PublishRelay<Bool>()
    let onTableViewIsActive = PublishRelay<Void>()
    let activeSubCategoryName = PublishRelay<String>()
    let beginEditingSubcategory = PublishRelay<Bool>()

    // MARK: - Private Properties

    private var cellSizes = [CGSize]()
    private let dropDown = DropDown()
    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let errorLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.error
        lbl.font = .montserratFontMedium(ofSize: 11)
        lbl.textAlignment = .center
        return lbl
    }()

    private let customLayout: TagsLayout = {
        let layout = TagsLayout()
        layout.scrollDirection = .horizontal
        layout.cellsPadding = ItemsPadding(horizontal: 8, vertical: 10)
        return layout
    }()

    private lazy var collectionView: UICollectionView = {
        let cvc = UICollectionView(frame: .zero, collectionViewLayout: customLayout)
        cvc.backgroundColor = .clear
        cvc.registerCells(withModels: SubCategoryCollectionCellViewModel.self)
        cvc.showsHorizontalScrollIndicator = false
        return cvc
    }()

    private let textField: UITextField = {
        let tf = UITextField()
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: tf.frame.height))
        let textFieldAttributes = [
            NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Medium", size: UIScreen.isSE ? 13 : 15)]
        tf.attributedPlaceholder = NSAttributedString(
            string: "create_lesson_name".localized,
            attributes: textFieldAttributes)
        tf.tintColor = .clear
        tf.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        tf.leftView = paddingView
        tf.minimumFontSize = UIScreen.isSE ? 13 : 15
        tf.leftViewMode = .always
        tf.layer.cornerRadius = UIScreen.isSE ? 18 : 23.5
        tf.backgroundColor = TalkmeColors.white
        tf.textColor = TalkmeColors.placeholderColor
        tf.clearsOnBeginEditing = true
        tf.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        return tf
    }()

    private let arrow: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "arrowTextField")
        img.contentMode = .scaleAspectFit
        return img
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupDropDown()
        textField.delegate = self
        customLayout.delegate = self
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    @objc func textFieldDidChange(_ textField: UITextField) {
        self.isCustomSubcatecory.accept(true)
    }

    func configure(title: String, text: String?, index: Int?, anotherText: String?) {
        titleLabel.text = title
        textField.text = text
        textField.text = anotherText
        bindCollectionView()
        bindVM(index: index)
    }

    func setupWarning(type: CreateLessonError) {
        self.errorLabel.text = type.title
    }

    func clearTextFieldText() {
        textField.text = ""
    }

    func endTextFieldText() {
        textField.resignFirstResponder()
    }

    func setupDropDownItems(items: [String]) {
        dropDown.dataSource = items
    }

    func setupSizes(items: [String] ) {
        cellSizes = items.map { item -> CGSize in
            let width = Double(self.collectionView.bounds.width)
            var size = UIFont.systemFont(ofSize: 13).sizeOfString(string: item, constrainedToWidth: width)
            size.width += UIScreen.isSE ? 40 : 70
            size.height += UIScreen.isSE ? 15 : 25
            return size
        }
    }

    func showDropDown() {
        dropDown.show()
    }

    func collectionItemSelected(item: Int) {
        let selectedIndexPath = IndexPath(item: item, section: 0)
        self.collectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: [])
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Private Methods

    private func bindVM(index: Int?) {
        dataItems
            .do(afterNext: { [weak self] _ in
                guard let index = index else { return }
                let selectedIndexPath = IndexPath(item: index, section: 0)
                self?.collectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: [])
            })
            .bind(to: collectionView.rx.items) { collectionView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                return cell
            }
            .disposed(by: bag)
    }

    private func bindCollectionView() {
        collectionView.rx
            .modelSelected(SubCategoryCollectionCellViewModel.self)
            .bind { [weak self] item in
                self?.onItemSelected.accept(item.categoryModel.name ?? "")
                self?.endTextFieldText()
                self?.textField.text = item.categoryModel.name
                self?.setupWarning(type: .clear)
                self?.activeSubCategoryName.accept(item.categoryModel.name ?? "")
            }
            .disposed(by: bag)
    }

    private func setupDropDown() {
        DropDown.appearance().textColor = TalkmeColors.grayLabels
        DropDown.appearance().textFont = .montserratSemiBold(ofSize: 13)
        DropDown.appearance().backgroundColor = TalkmeColors.white
        DropDown.appearance().cornerRadius = 12
        dropDown.direction = .bottom
        dropDown.anchorView = textField
        dropDown.bottomOffset = CGPoint(x: 0, y: UIScreen.isSE ? 36 : 47)

        dropDown.cancelAction = { [weak self] in
            self?.endEditing(true)
        }

        dropDown.selectionAction = { [weak self] (_, name) in
            guard let self = self else { return }
            self.activeSubCategoryName.accept(name)
            self.textField.text = name
            self.endTextFieldText()
            self.setupWarning(type: .clear)
            self.onItemSelected.accept(name)
            self.isCustomSubcatecory.accept(false)
        }
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, isRequiredImage, collectionView, textField, errorLabel, arrow, separatorView])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(25)
            make.leading.equalToSuperview().inset(10)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }

        collectionView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 60 : 78)
            make.bottom.equalToSuperview().inset(5)
            make.leading.equalToSuperview().inset(10)
            make.trailing.equalToSuperview()
            make.height.equalTo(UIScreen.isSE ? 90 : 125)
        }

        textField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 9 : 11)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        arrow.snp.makeConstraints { make in
            make.centerY.equalTo(textField.snp.centerY)
            make.trailing.equalTo(textField.snp.trailing).inset(20)
        }

        errorLabel.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension LessonSubcategoryTableCell: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dropDown.hide()
        return true
    }

    func textFieldDidBeginEditing(_ textField1: UITextField) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.showDropDown()
        }
        DispatchQueue.main.async {
            self.textField.endEditing(false)
        }
        self.collectionView.indexPathsForSelectedItems?
            .forEach { self.collectionView.deselectItem(at: $0, animated: false) }
        if textField1.text != nil {
            textField.text = nil
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText.containsEmoji {
                return false
            }
            let substringToReplace = text[textRange]
            let count = text.count - substringToReplace.count + string.count
            return count <= 0
        }
        return true
    }
}

extension LessonSubcategoryTableCell: LayoutDelegate {

    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.row]
    }
}
