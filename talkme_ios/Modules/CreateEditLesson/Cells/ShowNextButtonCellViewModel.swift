//
//  ShowNextButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 18.01.2021.
//

import RxCocoa
import RxSwift

final class ShowNextButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let tapRelay = PublishRelay<Void>()
    let buttonIsEnabled = PublishRelay<Bool>()
    let bag = DisposeBag()

    // MARK: - Public Methods

    func configure(_ cell: ShowNextButtonTableViewCell) {
        cell
            .buttonTap
            .bind(to: tapRelay)
            .disposed(by: cell.bag)

        tapRelay
            .bind { [weak cell] _ in
                cell?.buttonTouched(cell?.saveButton ?? UIButton())
            }
            .disposed(by: bag)

        buttonIsEnabled
            .bind(to: cell.buttonIsEnabled)
            .disposed(by: cell.bag)
    }
}
