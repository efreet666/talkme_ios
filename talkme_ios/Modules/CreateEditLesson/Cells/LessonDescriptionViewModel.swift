//
//  CreateLessonCellViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import RxCocoa

final class LessonDescriptionViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let heightChanged = PublishRelay<Void>()
    let bag = DisposeBag()
    let beginEditingDescription = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let title: String
    private let countLabel: String
    private var text: String?

    // MARK: - Initializers

    init(title: String, count: String, text: String?) {
        self.title = title
        self.countLabel = count
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonDescriptionTableCell) {
        cell.configure(title: title, count: countLabel, text: text)
        cell
            .lessonDescriptionText
            .do(onNext: { [weak self] in
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .sizeChanged
            .bind(to: heightChanged)
            .disposed(by: cell.bag)

        cell
            .beginEditingDescription
            .bind(to: beginEditingDescription)
            .disposed(by: cell.bag)
    }
}
