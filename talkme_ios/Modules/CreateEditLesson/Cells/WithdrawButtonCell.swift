//
//  WithdrawButtonCell.swift
//  talkme_ios
//
//  Created by Vladislav on 23.07.2021.
//

import RxSwift
import RxCocoa

final class WithdrawButtonCell: UITableViewCell {

    private(set) lazy var buttonTap = saveButton.rx.tap
    private(set) var bag = DisposeBag()

    // MARK: - Public Properties

    let isEnabledRelay = PublishRelay<Bool>()
    let onButtonTapped = PublishRelay<Void>()

    let saveButton: UIButton = {
        let bt = UIButton()
        bt.backgroundColor = TalkmeColors.greenLabels
        bt.titleLabel?.font = .montserratExtraBold(ofSize: UIScreen.isSE ? 14 : 17)
        bt.titleLabel?.textColor = TalkmeColors.white
        bt.layer.cornerRadius = UIScreen.isSE ? 21 : 27
        bt.isEnabled = true
        return bt
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(with title: String) {
        saveButton.setTitle(title, for: .normal)
    }

    func changeStatusButtun(_ value: Bool) {
        saveButton.backgroundColor = value ? TalkmeColors.greenLabels : TalkmeColors.grayDeactivatedButton
    }

    func buttonTouched(_ sender: UIButton) {
        UIButton.animate(
            withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
                sender.alpha = 0.5
            },
            completion: { _ in
                UIButton.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform.identity
                    sender.alpha = 1
                })
            })
    }

    // MARK: - Private Methods

    private func setupLayout() {
        contentView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
