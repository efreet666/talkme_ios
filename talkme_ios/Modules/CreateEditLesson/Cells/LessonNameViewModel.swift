//
//  LessonNameViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 16.01.2021.
//

import RxSwift
import RxCocoa

final class LessonNameViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let warningRelay = PublishRelay<Void>()
    let bag = DisposeBag()
    let beginEditingName = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let title: String
    private let count: String
    private var text: String?

    // MARK: - Initializers

    init(title: String, count: String, text: String?) {
        self.title = title
        self.count = count
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonNameTableCell) {
        cell.configure(title: title, count: count, text: text)

        warningRelay
            .bind { [weak cell] _ in
                cell?.setupWarning(type: .name)
            }
            .disposed(by: cell.bag)

        cell
            .lessonNameText
            .do(onNext: { [weak self] in
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .beginEditingName
            .bind(to: beginEditingName)
            .disposed(by: cell.bag)
    }
}
