//
//  WithdrawButtonCellViewModel.swift
//  talkme_ios
//
//  Created by Vladislav on 23.07.2021.
//

import RxCocoa
import RxSwift

final class WithdrawButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let title: String

    let isMoreThanMinimum = BehaviorRelay<Bool>(value: true)
    let onButtonTapped = PublishRelay<Void>()
    let bag = DisposeBag()
    let isEnabledRelay = BehaviorRelay<Bool>(value: true)
    let isHideButtonRelay = PublishRelay<Void>()

    // MARK: - Initializers

    init(title: String = "my_balance_widthdraw_confirm".localized, isEnabled: Bool = true) {
        self.title = title
        if !isEnabled { isEnabledRelay.accept(isEnabled) }
    }

    // MARK: - Public Methods

    func configure(_ cell: WithdrawButtonCell) {
        cell.configure(with: title)

        cell
            .buttonTap
            .bind(to: onButtonTapped)
            .disposed(by: bag)

        onButtonTapped
            .bind { [weak cell] _ in
                cell?.buttonTouched(cell?.saveButton ?? UIButton())
            }
            .disposed(by: bag)

        isEnabledRelay
            .bind(to: cell.isEnabledRelay)
            .disposed(by: bag)

        isMoreThanMinimum
            .bind { [weak cell] value in
                cell?.changeStatusButtun(value)
            }
            .disposed(by: bag)
    }
}
