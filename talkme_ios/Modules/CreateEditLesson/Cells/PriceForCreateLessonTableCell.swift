//
//  PriceForCreateLessonTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxSwift
import RxCocoa

final class PriceForCreateLessonTableCell: UITableViewCell {

    // MARK: - Public Properties

    let bag = DisposeBag()

    // MARK: - Private Properties

    let containerView: PriceView = {
        let view = PriceView()
        let money = "0"
        view.configure(type: .priceToCreateLesson, price: money)
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    func setPrice(price: Int) {
        containerView.configure(type: .priceToCreateLesson, price: String(price))
    }

    private func setupLayout() {
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(30)
            make.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 146 : 189)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
