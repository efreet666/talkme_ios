//
//  MembersCountTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxSwift
import RxCocoa

final class MembersCountTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var membersCountText = membersCountTF.rx.text
    private(set) var bag = DisposeBag()
    let buttonIsEnabled = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let membersCountTF: LessonCommonTextField = {
        let tf = LessonCommonTextField()
        tf.keyboardType = .numberPad
        tf.text = "10 000"
        return tf
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 10 : 13)
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        membersCountTF.delegate = self
        bind()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, placeholder: String, text: String?) {
        titleLabel.text = title
        membersCountTF.placeholder = placeholder
        membersCountTF.text = text
    }

    func bind() {
        membersCountText
            . bind { [weak self] numberString in
                guard let number = Float(numberString?.replacingOccurrences(of: " ", with: "") ?? "0") else { return }
                self?.warningLabel.text = number < 10
                    ? "create_lesson_error_members".localized
                    : "create_lesson_error_members_short".localized
                self?.buttonIsEnabled.accept(!(number < 10))
                self?.warningLabel.textColor = number < 10 ? TalkmeColors.warningColor : TalkmeColors.placeholderColor
                if number > 999 {
                    self?.membersCountTF.text = Formatters.convertCount(number: Int(number))
                } else {
                    self?.membersCountTF.text = String(Int(number))
                }
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        contentView.addSubviews([titleLabel, membersCountTF, warningLabel, isRequiredImage])

        titleLabel.snp.makeConstraints { label in
            label.top.equalToSuperview().inset(20)
            label.leading.equalToSuperview().inset(15)
        }

        membersCountTF.snp.makeConstraints { textView in
            textView.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 11)
            textView.leading.trailing.equalToSuperview().inset(10)
            textView.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(membersCountTF.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
            make.height.equalTo(20)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension MembersCountTableCell: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
        if let num = Int(newText.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)), num <= 30000 {
            return true
        } else if newText.isEmpty {
            return true
        } else {
            return false
        }
    }
}
