//
//  CreateLessonTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import RxCocoa
import GrowingTextView

final class LessonDescriptionTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var sizeChanged = ControlEvent(events: heightChanged)
    private(set) lazy var lessonDescriptionText = lessonDescriptionTextView.rx.text
    private(set) var bag = DisposeBag()
    let beginEditingDescription = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let heightChanged = PublishRelay<Void>()

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = 11
        return view
    }()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let lessonDescriptionTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.backgroundColor = TalkmeColors.white
        textView.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        textView.textColor = TalkmeColors.placeholderColor
        textView.attributedPlaceholder = NSAttributedString(
            string: "create_lesson_description".localized,
            attributes:
                [NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
                 NSAttributedString.Key.font: UIFont.montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
                ])
        textView.textContainerInset = UIEdgeInsets(
            top: 5,
            left: 8,
            bottom: 5,
            right: 0)
        textView.textContainer.lineFragmentPadding = 0.0
        textView.maxLength = 250
        return textView
    }()

    private let countLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.placeholderColor
        lbl.font = .montserratFontMedium(ofSize: 10)
        return lbl
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        lessonDescriptionTextView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, count: String, text: String?) {
        titleLabel.text = title
        countLabel.text = count
        lessonDescriptionTextView.text = text
        setupCounter()
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func setupLayout() {
        containerView.addSubview(lessonDescriptionTextView)
        contentView.addSubviews([titleLabel, containerView, countLabel])

        containerView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).inset(-10)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
            make.height.greaterThanOrEqualTo(UIScreen.isSE ? 134 : 174)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 16 : 21)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        lessonDescriptionTextView.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.top).offset(10)
            make.leading.trailing.equalToSuperview().inset(14)
            make.bottom.equalToSuperview().inset(17)
        }

        countLabel.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(5)
            make.trailing.equalTo(containerView)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupCounter() {
        lessonDescriptionTextView.rx.text
            .map { "\($0?.count ?? 0)/250" }
            .bind(to: countLabel.rx.text)
            .disposed(by: bag)
    }
}

extension LessonDescriptionTableCell: GrowingTextViewDelegate {

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.numberOfLines() > 10 && !text.isBackspace() {
            return false
        }
        return true
    }

    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        heightChanged.accept(())
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        self.beginEditingDescription.accept(true)
    }
}
