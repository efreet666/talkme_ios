//
//  LessonLengthViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxSwift
import RxCocoa

final class LessonDurationViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let sliderRelay = BehaviorRelay<Int>(value: 0)
    let onLoadText = PublishRelay<String>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private let placeholder: String
    private let text: String?

    // MARK: - Initializers

    init(title: String, placeholder: String, text: String?) {
        self.title = title
        self.placeholder = placeholder
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonDurationTableCell) {
        bindCell(cell)
        cell.configure(title: title, placeholder: placeholder, text: text)
    }

    func bindCell(_ cell: LessonDurationTableCell) {
        cell
            .lessonTimeText
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .lessonTimeSlider
            .map { value in
                Formatters.hoursFormatter.string(from: TimeInterval(value.constraintPriorityTargetValue))
            }
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .sliderRelay
            .bind(to: sliderRelay)
            .disposed(by: cell.bag)

        cell
            .onLoadText
            .bind(to: onLoadText)
            .disposed(by: cell.bag)
    }
}
