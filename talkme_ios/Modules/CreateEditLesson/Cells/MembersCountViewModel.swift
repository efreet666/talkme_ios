//
//  MembersCountViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxSwift
import RxCocoa

final class MembersCountViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let buttonIsEnabled = PublishRelay<Bool>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private let placeholder: String
    private var text: String?

    // MARK: - Initializers

    init(title: String, placeholder: String, text: String?) {
        self.title = title
        self.placeholder = placeholder
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: MembersCountTableCell) {
        cell.configure(title: title, placeholder: placeholder, text: text)

        cell
            .membersCountText
            .do(onNext: { [weak self] in
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .buttonIsEnabled
            .bind(to: buttonIsEnabled)
            .disposed(by: cell.bag)
    }
}
