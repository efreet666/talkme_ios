//
//  LessonPromotionTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 28.01.2021.
//

import RxSwift

final class LessonPromotionTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let commonView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = 12
        view.layer.shadowOpacity = 1
        view.layer.shadowColor = TalkmeColors.promotionShadow.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowRadius = 30
        return view
    }()

    private let viewLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 15 : 17)
        return lbl
    }()

    private let logoImage: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "logo"))
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let indicatorView = RoundedIndicatorView()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    // MARK: - Public Methods

    func configure(title: String, viewLabel: String) {
        titleLabel.text = title
        self.viewLabel.text = viewLabel
        indicatorView.setOn(true)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        commonView.addSubviews([logoImage, viewLabel, indicatorView])
        contentView.addSubviews([titleLabel, commonView])

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UIScreen.isSE ? 25 : 27)
            make.leading.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
        }

        commonView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 13 : 17)
            make.bottom.equalToSuperview().inset(10)
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
        }

        logoImage.snp.makeConstraints { make in
            make.height.equalTo(UIScreen.isSE ? 26 : 33)
            make.width.equalTo(UIScreen.isSE ? 48 : 62)
            make.top.bottom.equalToSuperview().inset(29)
            make.leading.equalToSuperview().inset(20)
        }

        viewLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 32 : 42)
            make.leading.equalTo(logoImage)
            make.trailing.equalTo(indicatorView)
        }

        indicatorView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 34 : 44)
            make.trailing.equalToSuperview().inset(UIScreen.isSE ? 18 : 26)
            make.size.equalTo(UIScreen.isSE ? 16 : 21)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
