//
//  ConfirmButtonCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 29.01.2021.
//

import RxCocoa
import RxSwift

final class ConfirmButtonCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let title: String
    let tapRelay = PublishRelay<Void>()
    let bag = DisposeBag()
    let isEnabledRelay = BehaviorRelay<Bool>(value: true)
    let isHideButtonRelay = PublishRelay<Void>()

    // MARK: - Initializers

    init(title: String = "talkme_сonfirm_сode_confirm".localized, isEnabled: Bool = true) {
        self.title = title
        if !isEnabled { isEnabledRelay.accept(isEnabled) }
    }

    // MARK: - Public Methods

    func configure(_ cell: ConfirmButtonTableCell) {
        cell.configure(with: title)

        cell
            .buttonTap
            .bind(to: tapRelay)
            .disposed(by: cell.bag)

        tapRelay
            .bind { [weak cell] _ in
                cell?.buttonTouched(cell?.saveButton ?? UIButton())
            }
            .disposed(by: bag)

        isHideButtonRelay
            .bind { [weak cell] _ in
                cell?.turnButton(on: false)
            }
            .disposed(by: bag)

        isEnabledRelay
            .bind(to: cell.isEnabledRelay)
            .disposed(by: bag)
    }
}
