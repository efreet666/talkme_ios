//
//  SubCategoryCollectionCell.swift
//  talkme_ios
//
//  Created by 1111 on 30.01.2021.
//

import RxSwift

final class SubCategoryCollectionCell: UICollectionViewCell {

    // MARK: - Private properties

    private let categoryItem = CategoryButton(type: .regular)

    override var isSelected: Bool {
        didSet {
            categoryItem.backgroundColor = isSelected ? .clear : TalkmeColors.white
            categoryItem.layer.borderColor = isSelected ? TalkmeColors.blueCollectionCell.cgColor : TalkmeColors.separatorView.cgColor
            categoryItem.setTitleColor(isSelected ? TalkmeColors.blueCollectionCell : TalkmeColors.grayLabels, for: .normal)
        }
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(categoryModel: Category) {
        categoryItem.setTitle(categoryModel.name, for: .normal)
    }

    // MARK: - Private Method

    private func setupLayout() {
        contentView.addSubview(categoryItem)
        categoryItem.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
