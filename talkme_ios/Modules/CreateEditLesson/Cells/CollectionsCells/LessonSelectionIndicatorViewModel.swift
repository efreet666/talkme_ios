//
//  LessonSelectionIndicatorViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 04.02.2021.
//

import RxSwift
import RxCocoa

final class LessonSelectionIndicatorViewModel: TableViewCellModelProtocol {

    // MARK: - Private Propertis

    var tag: Int

    // MARK: - Init

    init(tag: Int) {
        self.tag = tag
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonSelectionIndicatorCell) {
        cell.configure(tag: tag)
    }
}
