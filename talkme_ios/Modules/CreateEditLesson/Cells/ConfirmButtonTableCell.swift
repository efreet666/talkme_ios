//
//  ConfirmButtonTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 29.01.2021.
//

import RxSwift
import RxCocoa

final class ConfirmButtonTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var buttonTap = saveButton.rx.tap
    private(set) var bag = DisposeBag()
    let isEnabledRelay = PublishRelay<Bool>()

    let saveButton: UIButton = {
        let bt = UIButton()
        bt.backgroundColor = TalkmeColors.greenLabels
        bt.titleLabel?.font = .montserratExtraBold(ofSize: UIScreen.isSE ? 14 : 17)
        bt.titleLabel?.textColor = TalkmeColors.white
        bt.layer.cornerRadius = UIScreen.isSE ? 21 : 27
        bt.isEnabled = true
        return bt
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellStyle()
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(with title: String) {
        saveButton.setTitle(title, for: .normal)

        isEnabledRelay
            .do { [weak self] in
                self?.saveButton.backgroundColor = $0 ? TalkmeColors.greenLabels : TalkmeColors.grayLabel
            }
            .bind(to: saveButton.rx.isEnabled)
            .disposed(by: bag)
    }

    func turnButton(on: Bool) {
        saveButton.isEnabled = on ? false : true
    }

    func buttonTouched(_ sender: UIButton) {
        UIButton.animate(
            withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
                sender.alpha = 0.5
            },
            completion: { _ in
                UIButton.animate(withDuration: 0.2, animations: {
                    sender.transform = CGAffineTransform.identity
                    sender.alpha = 1
                    self.turnButton(on: true)
                })
            })
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    private func setupLayout() {
        contentView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 42 : 54)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
