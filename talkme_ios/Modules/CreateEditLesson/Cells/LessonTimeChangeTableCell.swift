//
//  LessonTimeChangeTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 21.01.2021.
//

import RxSwift
import RxCocoa

final class LessonTimeChangeTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var lessonTimeText = lessonTimeTF.rx.text
    private(set) lazy var lessonTimeSlider = timeSlider.rx.value
    private(set) var bag = DisposeBag()
    private(set) var onGetLessonsCount = PublishRelay<Void>()
    let lessonTime = PublishRelay<Date>()
    let onLoadText = PublishRelay<String>()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))
    private var currentTime: String {
        Formatters.timeFormatter.string(from: self.datePicker.date)
    }

    private let doneButton = UIBarButtonItem(title: "profile_setting_done".localized, style: .plain, target: self, action: nil)

    private var items: [SliderTrackView] = {
        var items: [SliderTrackView] = []
        for item in 0...7 {
            var track = SliderTrackView()
            track.backgroundColor = TalkmeColors.grayTimeSlider
            items.append(track)
        }
        items[0].layer.cornerRadius = 3
        items[0].layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        items[7].layer.cornerRadius = 3
        items[7].layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        return items
    }()

    private var labels: [SliderLabel] = {
        var labels: [SliderLabel] = []
        for label in 0...4 {
            let label = SliderLabel()
            labels.append(label)
        }
        return labels
    }()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let lessonTimeTF = LessonCommonTextField()

    private let timeButton: UIButton = {
        let btn = UIButton()
        btn.contentMode = .scaleAspectFit
        btn.setImage(UIImage(named: "lessonTime"), for: .normal)
        return btn
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: 13)
        return lbl
    }()

    private lazy var timeSlider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = .clear
        slider.maximumTrackTintColor = .clear
        return slider
    }()

    private let sliderHorizontalStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        return sv
    }()

    private let labelsHorizontalStack: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .equalSpacing
        return sv
    }()

    private let datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .time
        datePicker.minuteInterval = 1
        datePicker.locale = Locale(identifier: "en_GB")
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        let minDate = Calendar.current.date(byAdding: .hour, value: 0, to: Date())
        let maxDate = Calendar.current.date(byAdding: .hour, value: 24, to: Date())
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        return datePicker
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, placeholder: String, text: String?) {
        titleLabel.text = title
        lessonTimeTF.placeholder = placeholder
        lessonTimeTF.text = text
        if text != nil {
            setupSlider(seconds: Float(setupTimeToseconds(time: text)))
        }
        updateDatePicker(text: text)
        bindUI()
    }

    func setupError(_ error: CreateLessonError) {
        let errorText = error.title
        guard errorText != warningLabel.text else { return }

        let isHidden = errorText == nil
        UIView.transition(
            with: warningLabel,
            duration: 0.3,
            options: .transitionCrossDissolve
        ) {
            self.warningLabel.alpha = isHidden ? 0 : 1
            self.warningLabel.text = errorText
        }
    }

    func getLessonsCountInHour(countAray: [Float]) {
        let seconds = setupTimeToseconds()
        if   seconds <= 28799 {
            var currentTimeArray: [Float] = []
            for number in 0...7 {
                currentTimeArray.append(countAray[number])
            }
            self.changeLessonCountColor(curentTimeArray: currentTimeArray)
        } else if seconds >= 28800
                    && seconds <= 57599 {
            var currentTimeArray: [Float] = []
            for number in 8...15 {
                currentTimeArray.append(countAray[number])
            }
            self.changeLessonCountColor(curentTimeArray: currentTimeArray)
        } else {
            var currentTimeArray: [Float] = []
            for number in 16...23 {
                currentTimeArray.append(countAray[number])
            }
            self.changeLessonCountColor(curentTimeArray: currentTimeArray)
        }
    }

    func setLessonDate(date: Date) {
        if Calendar.current.isDateInToday(date) {
            let minDate = Calendar.current.date(byAdding: .hour, value: 0, to: Date())
            let maxDate = Calendar.current.date(byAdding: .hour, value: 24, to: Date())
            datePicker.minimumDate = minDate
            datePicker.maximumDate = maxDate
        } else {
            datePicker.minimumDate = nil
            datePicker.maximumDate = nil
        }
    }

    // MARK: - Private Methods

    func setupTimeToseconds(time: String? = nil) -> Int {
        if let time = time {
            let hoursString = time.components(separatedBy: ":")
            let hour = (Int((hoursString[0])) ?? 0) * 60
            let seconds = (Int((hoursString[1])) ?? 0)
            let result = (hour + seconds) * 60
            return result
        } else {
            let timeString = (Formatters.timeFormatter.string(from: self.datePicker.date))
            let hoursString = timeString.components(separatedBy: ":")
            let countOfSeconds = ( (Int(hoursString[0]) ?? 0) * 60 + (Int(hoursString[1]) ?? 0) ) * 60
            return countOfSeconds
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func updateDatePicker(text: String?) {
        guard let text = text,
              let date = Formatters.timeFormatter.date(from: text) else {
            return
        }
        datePicker.setDate(date, animated: true)
    }

    private func bindUI() {
        timeButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.lessonTimeTF.becomeFirstResponder()
            }
            .disposed(by: bag)

        doneButton.rx.tap
            .bind(to: onGetLessonsCount)
            .disposed(by: bag)

        doneButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.lessonTimeTF.text = self.currentTime
                let setupDate = Formatters.timeFormatter.string(from: self.datePicker.date)
                self.setupSlider(seconds: Float(self.setupTimeToseconds(time: setupDate)))
                self.setupToolbar()
                self.contentView.endEditing(true)
                self.lessonTime.accept(self.datePicker.date)
            }
            .disposed(by: bag)

        timeSlider.rx.value
            .bind { [weak self] slider in
                guard let self = self else { return }
                self.lessonTimeTF.text = Formatters.hoursFormatter.string(from: TimeInterval(slider.constraintPriorityTargetValue))
                self.onLoadText.accept(Formatters.hoursFormatter.string(from: TimeInterval(slider.constraintPriorityTargetValue)) ?? "")
            }
            .disposed(by: bag)
    }

    private func setupSlider(seconds: Float) {
        if seconds <= 28799 {
            self.timeSlider.minimumValue = 0
            self.timeSlider.maximumValue = 28799
            let timeLabels = ["00:00", "02:00", "04:00", "06:00", "08:00"]
            for label in 0..<timeLabels.count {
                labels[label].sliderLabel.text = timeLabels[label]
            }
        } else if seconds >= 28800
                    && seconds <= 57599 {
            self.timeSlider.minimumValue = 28800
            self.timeSlider.maximumValue = 57599
            let timeLabels = ["08:00", "10:00", "12:00", "14:00", "16:00"]
            for label in 0..<timeLabels.count {
                labels[label].sliderLabel.text = timeLabels[label]
            }
        } else {
            self.timeSlider.minimumValue = 57800
            self.timeSlider.maximumValue = 86400
            let timeLabels = ["16:00", "18:00", "20:00", "22:00", "24:00"]
            for label in 0..<timeLabels.count {
                labels[label].sliderLabel.text = timeLabels[label]
            }
        }
        self.timeSlider.value = Float(seconds)
    }

    private func changeLessonCountColor(curentTimeArray: [Float]) {
        for hour in 0..<curentTimeArray.count {
            if curentTimeArray[hour] <= 15 {
                items[hour].backgroundColor = TalkmeColors.grayTimeSlider
            } else if curentTimeArray[hour] >= 16 && curentTimeArray[hour] <= 25 {
                items[hour].backgroundColor = TalkmeColors.greenLabels
            } else if curentTimeArray[hour] >= 16 && curentTimeArray[hour] <= 25 {
                items[hour].backgroundColor = TalkmeColors.yellowSlider
            } else {
                items[hour].backgroundColor = TalkmeColors.warningColor
            }
        }
    }

    private func setupUI() {
        lessonTimeTF.inputView = datePicker
        setupToolbar()
    }

    private func setupToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: true)
        lessonTimeTF.inputAccessoryView = toolBar
    }

    private func setupLayout() {
        items.forEach(sliderHorizontalStack.addArrangedSubview)
        labels.forEach(labelsHorizontalStack.addArrangedSubview)

        contentView.addSubviews([titleLabel,
                                 lessonTimeTF,
                                 warningLabel,
                                 timeButton,
                                 sliderHorizontalStack,
                                 timeSlider,
                                 labelsHorizontalStack,
                                 isRequiredImage
                                ])

        titleLabel.snp.makeConstraints { label in
            label.top.equalToSuperview().inset(20)
            label.leading.equalToSuperview().inset(15)
        }

        lessonTimeTF.snp.makeConstraints { textView in
            textView.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 11)
            textView.leading.trailing.equalToSuperview().inset(10)
            textView.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        timeButton.snp.makeConstraints { button in
            button.centerY.equalTo(lessonTimeTF.snp.centerY)
            button.trailing.equalToSuperview().inset(30)
            button.size.equalTo(UIScreen.isSE ? 15 : 21)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(lessonTimeTF.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(20)
        }

        timeSlider.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.top.equalTo(warningLabel.snp.bottom).offset(5)
        }

        sliderHorizontalStack.snp.makeConstraints { make in
            make.top.equalTo(timeSlider.snp.centerY).offset(-3)
            make.leading.trailing.equalToSuperview().inset(10)
        }

        labelsHorizontalStack.snp.makeConstraints { make in
            make.top.equalTo(sliderHorizontalStack.snp.bottom).offset(14)
            make.trailing.leading.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
