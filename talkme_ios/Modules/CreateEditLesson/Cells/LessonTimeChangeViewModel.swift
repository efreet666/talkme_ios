//
//  LessonTimeChangeViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 21.01.2021.
//

import RxSwift
import RxCocoa

final class LessonTimeChangeViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let onGetLessonsCount = PublishRelay<Void>()
    let lessonCount = PublishRelay<[Float]>()
    let lessonDate = PublishRelay<Date>()
    let error = PublishRelay<CreateLessonError>()
    let bag = DisposeBag()
    let lessonTime = PublishRelay<Date>()

    // MARK: - Private Properties

    private let title: String
    private let placeholder: String
    private var text: String?

    // MARK: - Initializers

    init(title: String, placeholder: String, text: String?) {
        self.title = title
        self.placeholder = placeholder
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonTimeChangeTableCell) {
        cell.configure(title: title, placeholder: placeholder, text: text)
        bindCell(cell)
    }

    func bindCell(_ cell: LessonTimeChangeTableCell) {
        cell
            .lessonTimeText
            .do(onNext: { [weak self] in
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .lessonTimeSlider
            .map { value in
                Formatters.hoursFormatter.string(from: TimeInterval(value.constraintPriorityTargetValue))
            }
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .onGetLessonsCount
            .bind(to: onGetLessonsCount)
            .disposed(by: cell.bag)

        lessonCount
            .bind { [weak cell] count in
                cell?.getLessonsCountInHour(countAray: count)
            }
            .disposed(by: cell.bag)

        lessonDate
            .bind { [weak cell] date in
                cell?.setLessonDate(date: date)
            }
            .disposed(by: cell.bag)

        cell
            .lessonTime
            .bind(to: lessonTime)
            .disposed(by: cell.bag)

        cell
            .onLoadText
            .do(onNext: { [weak self] in
                self?.text = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
                cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }
}
