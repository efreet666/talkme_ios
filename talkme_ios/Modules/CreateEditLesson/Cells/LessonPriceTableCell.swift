//
//  LessonPriceTableCell.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxCocoa
import RxSwift

final class LessonPriceTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var priceText = priceTF.rx.text
    private(set) var bag = DisposeBag()
    let clearCost = PublishRelay<String>()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))
    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let priceTF: LessonCommonTextField = {
        let tf = LessonCommonTextField()
        tf.text = "0"
        tf.keyboardType = .numberPad
        return tf
    }()

    private let coinImageView: UIImageView = {
        let iw = UIImageView(image: UIImage(named: "coin"))
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let freeView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.cornerRadius = UIScreen.isSE ? 18 : 23.5
        view.layer.borderWidth = 1
        view.layer.borderColor = TalkmeColors.grayButtonBorder.cgColor
        return view
    }()

    private let isFreeIndicator: RoundedIndicatorView = {
        let ind = RoundedIndicatorView()
        ind.setOn(true)
        return ind
    }()

    private let isFreeLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.grayLabel
        label.text = "create_lesson_free_price".localized
        label.textAlignment = .center
        return label
    }()

    private let priceStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.spacing = 17
        return sv
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        bindUI()
        setupCellStyle()
        priceTF.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, placeholder: String, cost: String?) {
        titleLabel.text = title
        priceTF.placeholder = placeholder
        priceTF.text = cost
        isFreeIndicator.setOn(true)
    }

    // MARK: - Private Methods

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }

    private func bindUI() {
        freeView.rx.tapGesture()
            .when(.recognized)
            .bind { [weak self] _ in
                guard let self = self else { return }
                self.priceTF.text = "0"
                self.clearCost.accept("0")
                self.isFreeIndicator.setOn(true)
            }
            .disposed(by: bag)

        priceTF.rx.tapGesture()
            .bind { [weak self] _ in
                self?.isFreeIndicator.setOn(false)
            }
            .disposed(by: bag)

        priceText
            .bind { [weak self] text in
                guard let text = text else { return }
                self?.priceTF.text = (String(Int(text) ?? 0))
            }
            .disposed(by: bag)
    }

    private func setupLayout() {
        priceTF.addSubview(coinImageView)
        freeView.addSubviews([isFreeIndicator, isFreeLabel])
        priceStackView.addArrangedSubview(priceTF)
        priceStackView.addArrangedSubview(freeView)
        contentView.addSubviews([titleLabel, priceStackView, isRequiredImage])

        titleLabel.snp.makeConstraints { label in
            label.top.equalToSuperview().inset(20)
            label.leading.equalToSuperview().inset(15)
        }

        priceStackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 36 : 47)
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 8 : 11)
            make.bottom.equalToSuperview()
        }

        priceTF.snp.makeConstraints { textView in
            textView.leading.top.bottom.height.equalToSuperview()
            textView.height.equalTo(UIScreen.isSE ? 36 : 47)
        }

        coinImageView.snp.makeConstraints { make in
            make.size.equalTo(17)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(13)
        }

        freeView.snp.makeConstraints { make in
            make.width.equalTo(140)
            make.trailing.top.bottom.height.equalToSuperview()
        }

        isFreeIndicator.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 16 : 21)
            make.centerY.equalToSuperview()
            make.trailing.equalTo(isFreeLabel.snp.leading).inset(-9)
            make.leading.equalToSuperview().inset(24)
        }

        isFreeLabel.snp.makeConstraints { make in
            make.leading.equalTo(isFreeIndicator.snp.trailing).inset(-9)
            make.trailing.equalToSuperview().inset(20)
            make.centerY.equalToSuperview()
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}

extension LessonPriceTableCell: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
        if let num = Int(newText), num <= 500000 {
            return true
        } else {
            return newText.isEmpty
        }
    }
}
