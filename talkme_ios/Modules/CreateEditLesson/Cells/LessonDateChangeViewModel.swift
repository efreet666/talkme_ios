//
//  LessonDateChangeTableCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 21.01.2021.
//

import RxSwift
import RxCocoa

final class LessonDateChangeViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let lessonDate = PublishRelay<Date>()
    let error = PublishRelay<CreateLessonError>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private let placeholder: String
    private var text: String?

    // MARK: - Initializers

    init(title: String, placeholder: String, text: String?) {
        self.title = title
        self.placeholder = placeholder
        self.text = text
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonDateChangeTableCell) {
        cell.configure(title: title, placeholder: placeholder, text: text)
        bindCell(cell)
    }

    func bindCell(_ cell: LessonDateChangeTableCell) {
        cell
            .lessonDateText
            .do(onNext: { [weak self] text in
                self?.text = text
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .lessonDate
            .bind(to: lessonDate)
            .disposed(by: cell.bag)

        error
            .bind { [weak cell] in
            cell?.setupError($0)
            }
            .disposed(by: cell.bag)
    }
}
