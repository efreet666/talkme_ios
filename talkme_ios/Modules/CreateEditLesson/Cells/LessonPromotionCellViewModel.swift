//
//  LessonPromotionCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 28.01.2021.
//

import RxCocoa

final class LessonPromotionViewModel: TableViewCellModelProtocol {

    // MARK: - Private Properties

    private let title: String
    private let viewLabel: String

    // MARK: - Initializers

    init(title: String, viewLabel: String) {
        self.title = title
        self.viewLabel = viewLabel
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonPromotionTableCell) {
        cell.configure(title: title, viewLabel: viewLabel)
    }
}
