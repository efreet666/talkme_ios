//
//  LessonPictureTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 16.01.2021.
//

import RxCocoa
import RxSwift
import Kingfisher

final class LessonPictureTableCell: UITableViewCell {

    // MARK: Public Properties

    private(set) lazy var onPhotoTap = photoButton.rx.tap
    private(set) lazy var onPickImage = lessonPicture.rx.image
    private(set) var bag = DisposeBag()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))
    private let photoButton = UIButton()

    private let lessonPicture: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "addImage")
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.layer.cornerRadius = 12
        return img
    }()

    private let photoTitle: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let rulesOfPhoto: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayDescription
        lbl.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        lbl.text = "create_lesson_photo_rules".localized
        lbl.numberOfLines = 0
        return lbl
    }()

    private let errorLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.error
        lbl.font = .montserratFontMedium(ofSize: 11)
        lbl.textAlignment = .center
        return lbl
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, imageString: String?) {
        photoTitle.text = title
        if let imageUrl = URL(string: imageString ?? "") {
            lessonPicture.kf.setImage(with: ImageResource(downloadURL: imageUrl, cacheKey: imageUrl.absoluteString))
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    func setupWarning(type: CreateLessonError) {
        self.errorLabel.text = type.title
    }

    // MARK: - Private Methods

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupLayout() {
        contentView.addSubviews([photoTitle, isRequiredImage, lessonPicture, photoButton, rulesOfPhoto, errorLabel, separatorView])

        photoTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 17 : 21)
            make.leading.equalToSuperview().inset(10)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(photoTitle.snp.trailing).offset(5)
            make.centerY.equalTo(photoTitle.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }

        lessonPicture.snp.makeConstraints { make in
            make.top.equalTo(photoTitle.snp.bottom).offset(UIScreen.isSE ? 9 : 12)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 24 : 32)
            make.leading.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 150 : 190)
            make.width.equalTo(UIScreen.isSE ? 140 : 180)
        }

        photoButton.snp.makeConstraints { make in
            make.edges.equalTo(lessonPicture)
        }

        rulesOfPhoto.snp.makeConstraints { make in
            make.top.bottom.equalTo(lessonPicture)
            make.leading.equalTo(contentView.snp.centerX).offset(UIScreen.isSE ? 23 : 29)
            make.trailing.equalToSuperview().inset(10)
        }

        errorLabel.snp.makeConstraints { make in
            make.centerY.equalTo(photoTitle.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }
}
