//
//  LessonPriceViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 22.01.2021.
//

import RxSwift
import RxCocoa

final class LessonPriceViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let textRelay = PublishRelay<String?>()
    let clearCost = PublishRelay<String>()
    let bag = DisposeBag()

    // MARK: - Private Properties

    private let title: String
    private let placeholder: String
    private var cost: String?

    // MARK: - Initializers

    init(title: String, placeholder: String, cost: String?) {
        self.title = title
        self.placeholder = placeholder
        self.cost = cost
    }

    // MARK: - Public Methods

    func configure(_ cell: LessonPriceTableCell) {
        cell.configure(title: title, placeholder: placeholder, cost: cost)
        bindCell(cell)
    }

    func bindCell(_ cell: LessonPriceTableCell) {
        cell
            .priceText
            .do(onNext: { [weak self] in
                self?.cost = $0
            })
            .bind(to: textRelay)
            .disposed(by: cell.bag)

        cell
            .clearCost
            .bind(to: clearCost)
            .disposed(by: cell.bag)
    }
}
