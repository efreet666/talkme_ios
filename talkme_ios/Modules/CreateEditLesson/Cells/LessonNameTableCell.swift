//
//  LessonNameTableCell.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import RxCocoa
import GrowingTextView

final class LessonNameTableCell: UITableViewCell {

    // MARK: - Public Properties

    private(set) lazy var lessonNameText = lessonNameTextView.rx.text
    private(set) var bag = DisposeBag()
    let beginEditingName = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let isRequiredImage = UIImageView(image: UIImage(named: "isrequiredimage"))

    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.white
        view.layer.cornerRadius = UIScreen.isSE ? 18 : 23.5
        return view
    }()

    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.grayLabels
        lbl.font = .montserratBold(ofSize: UIScreen.isSE ? 17 : 20)
        return lbl
    }()

    private let lessonNameTextView: GrowingTextView = {
        let textView = GrowingTextView()
        textView.backgroundColor = TalkmeColors.white
        textView.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        textView.textColor = TalkmeColors.placeholderColor
        textView.attributedPlaceholder = NSAttributedString(
            string: "create_lesson_name_lesson".localized,
            attributes:
                [NSAttributedString.Key.foregroundColor: TalkmeColors.placeholderColor,
                 NSAttributedString.Key.font: UIFont.montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
                ])
        textView.textContainerInset = UIEdgeInsets(
            top: 10,
            left: 8,
            bottom: 10,
            right: 0)
        textView.textContainer.lineFragmentPadding = 0.0
        textView.maxLength = 30
        return textView
    }()

    private let countLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.placeholderColor
        lbl.font = .montserratFontMedium(ofSize: 10)
        return lbl
    }()

    private let errorLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = TalkmeColors.error
        lbl.font = .montserratFontMedium(ofSize: 10)
        lbl.textAlignment = .center
        return lbl
    }()

    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.separatorFilter
        return view
    }()

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        setupCellStyle()
        setupCounter()
        lessonNameTextView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(title: String, count: String, text: String?) {
        titleLabel.text = title
        countLabel.text = count
        lessonNameTextView.text = text
    }

    func setupWarning(type: CreateLessonError) {
        self.errorLabel.text = type.title
    }

    // MARK: - Private Methods

    private func setupLayout() {
        containerView.addSubview(lessonNameTextView)
        contentView.addSubviews([titleLabel, isRequiredImage, containerView, countLabel, errorLabel, separatorView])

        containerView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(UIScreen.isSE ? 9 : 12)
            make.leading.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 22 : 28)
            make.height.greaterThanOrEqualTo(UIScreen.isSE ? 36 : 47)
        }

        isRequiredImage.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.trailing).offset(5)
            make.centerY.equalTo(titleLabel.snp.centerY).offset(1)
            make.width.height.equalTo(5)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIScreen.isSE ? 16 : 20)
            make.leading.equalToSuperview().inset(10)
        }

        lessonNameTextView.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.top).inset(1)
            make.leading.trailing.equalToSuperview().inset(14)
            make.bottom.equalToSuperview().inset(1)
        }

        countLabel.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(5)
            make.trailing.equalTo(containerView)
        }

        errorLabel.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel.snp.centerY)
            make.trailing.equalToSuperview().inset(10)
        }

        separatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(UIScreen.isSE ? 10 : 12)
            make.height.equalTo(1)
            make.bottom.equalToSuperview()
        }
    }

    private func setupCellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }

    private func setupCounter() {
        lessonNameTextView.rx.text
            .map { "\($0?.count ?? 0)/30" }
            .bind(to: countLabel.rx.text)
            .disposed(by: bag)

        lessonNameTextView.rx.text
            .orEmpty
            .filter { !$0.isEmpty }
            .bind { [weak self] _ in
                self?.setupWarning(type: .clear)
            }
            .disposed(by: bag)
    }
}

extension LessonNameTableCell: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        self.beginEditingName.accept(true)
    }
}
