//
//  StreamData.swift
//  talkme_ios
//
//  Created by 1111 on 04.02.2021.
//

import UIKit

final class StreamData {
    let id: Int?
    var category: Int?
    var subCategory: String?
    var lessonName: String?
    var lessonImage: UIImage?
    var lessonDescription: String?
    var avatarUrl: String?
    var lessonDuration: String?
    var date: String?
    var subscriptions: Int?
    var cost: Int?
    var lessonStartTime: String?
    var lessonDate: String?
    var lessonTime: String?
    var price: Int

    init(
        id: Int?,
        category: Int? = nil,
        subCategory: String? = nil,
        lessonName: String? = nil,
        lessonImage: UIImage? = nil,
        lessonDescription: String? = nil,
        avatarUrl: String? = nil,
        lessonDuration: String? = nil,
        date: String? = nil,
        subscriptions: Int? = nil,
        cost: Int? = nil,
        lessonTimeStamp: String? = nil,
        lessonDate: String? = nil,
        lessonTime: String? = nil,
        price: Int = 0
    ) {
        self.category = category
        self.subCategory = subCategory
        self.lessonName = lessonName
        self.lessonImage = lessonImage
        self.lessonDescription = lessonDescription
        self.avatarUrl = avatarUrl
        self.lessonDuration = lessonDuration
        self.date = date
        self.subscriptions = subscriptions
        self.cost = cost
        self.lessonStartTime = lessonTimeStamp
        self.lessonDate = lessonDate
        self.lessonTime = lessonTime
        self.price = price
        self.id = id
    }
}
