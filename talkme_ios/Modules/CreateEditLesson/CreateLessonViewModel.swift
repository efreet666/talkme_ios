//
//  CreateLessonViewModel.swift
//  talkme_ios
//
//  Created by Майя Калицева on 15.01.2021.
//

import RxSwift
import Kingfisher
import RxCocoa

enum CreateLessonScreenType {
    case create
    case edit
}

final class CreateLessonViewModel {

    enum Flow {
        case showSecondController(streamData: StreamData, screenType: CreateLessonScreenType)
    }

    // MARK: - Public Properties

    private(set) lazy var dataItems = PublishRelay<[AnyTableViewCellModelProtocol]>()
    private(set) var bag = DisposeBag()
    let onButtonTapped = PublishRelay<Void>()
    let flow = PublishRelay<Flow>()
    let onEndDropDown = PublishRelay<Void>()
    let clearWarningPicture = PublishRelay<Void>()
    let height = PublishRelay<Void>()
    let screenType: CreateLessonScreenType
    let onPickImage = PublishRelay<UIImage>()
    let beginEditingName = PublishRelay<Void>()
    let scrollTo = PublishRelay<IndexPath>()

    // MARK: - Private Properties

    private let service: MainServiceProtocol
    private let streamData: StreamData
    private var categories: [Category] = []
    private var subcategories: [Category] = []
    private var defaultCategory = 7
    private var itemID = 0
    private var categoryOffsetX: CGFloat = 0

    // MARK: - Initializers

    init(service: MainServiceProtocol, id: Int?, screenType: CreateLessonScreenType) {
        self.service = service
        self.screenType = screenType
        streamData = .init(id: id)
    }

    // MARK: - Public Methods

    func getCategories(id: Int) {
        service
            .categories()
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.categories = response
                    self?.getSubCategories(for: id)
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func getLessonId() {
        guard let id = streamData.id else {
            self.setupViewModels()
            return
        }
        service
            .lessonID(id: id)
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    guard let categoryID = response.list?.subCategory?.parent,
                          let subCategory = response.list?.subCategory?.name else { return }
                    self.streamData.category = categoryID
                    self.streamData.subCategory = subCategory
                    self.streamData.lessonName = response.list?.name
                    self.streamData.lessonDescription = response.list?.description
                    self.streamData.avatarUrl = response.list?.tileImage
                    self.streamData.lessonDuration = response.list?.lessonTime
                    self.streamData.cost = response.list?.cost
                    self.streamData.subscriptions = response.list?.numberOfParticipants
                    self.streamData.date = response.list?.date
                    self.getCategories(id: categoryID)
                case .error(let error):
                    self.setupViewModels()
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    // MARK: - Private Methods

    private func getSubCategories(for category: Int) {
        service
            .subCategory(parent: String(category))
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    self?.subcategories = response
                    self?.setupViewModels()
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private func setupViewModels() {
        let indicatorVM = LessonSelectionIndicatorViewModel(tag: 1)
        let lessonCategoryVM = LessonCategoryViewModel(
            title: "create_lesson_choose_category".localized,
            model: categories,
            activeCategory: streamData.category ?? defaultCategory,
            offset: categoryOffsetX
        )
        let lessonSubcategoryVM = LessonSubcategoryViewModel(
            title: "create_lesson_choose_subcategory".localized,
            model: subcategories,
            activeSubCategory: streamData.subCategory, text: streamData.subCategory ?? "", handleText: streamData.subCategory ?? "")
        let lessonPictureVM = LessonPictureViewModel(
            title: "create_lesson_photo_lesson".localized,
            imageString: streamData.avatarUrl
        )
        let lessonNameVM = LessonNameViewModel(
            title: "create_lesson_name_lesson".localized,
            count: "0/30", text: streamData.lessonName
        )
        let lessonDescriptionVM = LessonDescriptionViewModel(
            title: "create_lesson_description".localized,
            count: "0/1700",
            text: streamData.lessonDescription
        )
        let showNextButtonVM = ShowNextButtonCellViewModel()

        let dataItems: [AnyTableViewCellModelProtocol] = [
            indicatorVM,
            lessonCategoryVM,
            lessonSubcategoryVM,
            lessonPictureVM,
            lessonNameVM,
            lessonDescriptionVM,
            showNextButtonVM
        ]

        self.dataItems.accept(dataItems)

        scrollToIndexPath(lessonSubcategoryVM: lessonSubcategoryVM,
                          lessonNameVM: lessonNameVM,
                          lessonDescriptionVM: lessonDescriptionVM)
        onPickImage(lessonPictureVM: lessonPictureVM)
        onEndDropDown(lessonSubcategoryVM: lessonSubcategoryVM)
        setupData(lessonCategoryVM: lessonCategoryVM,
                  lessonSubcategoryVM: lessonSubcategoryVM,
                  lessonPictureVM: lessonPictureVM,
                  lessonNameVM: lessonNameVM,
                  lessonDescriptionVM: lessonDescriptionVM)

        showNextButtonVM
            .tapRelay
            .bind { [weak self, weak lessonSubcategoryVM, weak lessonNameVM, weak lessonPictureVM] _ in
                guard let self = self,
                      let lessonPictureVM = lessonPictureVM,
                      let lessonSubcategoryVM = lessonSubcategoryVM,
                      let lessonNameVM = lessonNameVM else { return }
                if self.streamData.category == nil {
                    self.streamData.category = self.defaultCategory
                }
                if self.streamData.lessonImage == nil, self.streamData.id != nil {
                    guard self.showErrors(
                        for: self.streamData,
                           lessonSubcategoryVM: lessonSubcategoryVM,
                           lessonNameVM: lessonNameVM,
                           lessonPictureVM: lessonPictureVM) else { return }
                    self.flow.accept(.showSecondController(streamData: self.streamData, screenType: self.screenType))
                } else {
                    guard !self.showErrors(
                        for: self.streamData,
                           lessonSubcategoryVM: lessonSubcategoryVM,
                           lessonNameVM: lessonNameVM,
                           lessonPictureVM: lessonPictureVM) else { return }
                    self.flow.accept(.showSecondController(streamData: self.streamData, screenType: self.screenType))
                }
            }
            .disposed(by: showNextButtonVM.bag)

        lessonCategoryVM
            .onClearTFSubCategoryText
            .bind(to: lessonSubcategoryVM.onEndTextField)
            .disposed(by: lessonCategoryVM.bag)

        lessonCategoryVM
            .onClearTFSubCategoryText
            .bind(to: lessonSubcategoryVM.onClearTextField)
            .disposed(by: lessonCategoryVM.bag)

        lessonDescriptionVM
            .heightChanged
            .bind(to: height)
            .disposed(by: lessonDescriptionVM.bag)
    }

    private func onPickImage(lessonPictureVM: LessonPictureViewModel) {
        lessonPictureVM
            .photoButtonTap
            .bind(to: onButtonTapped)
            .disposed(by: lessonPictureVM.bag)

        onPickImage
            .do(onNext: { [weak self] in
                self?.streamData.lessonImage = $0
            })
            .bind(to: lessonPictureVM.onPickImage)
            .disposed(by: bag)

        clearWarningPicture
            .bind(to: lessonPictureVM.onClearWarning)
            .disposed(by: bag)
    }

    private func onEndDropDown(lessonSubcategoryVM: LessonSubcategoryViewModel) {
        onEndDropDown
            .bind(to: lessonSubcategoryVM.onEndTextField)
            .disposed(by: bag)
    }

    private func setupData(
        lessonCategoryVM: LessonCategoryViewModel,
        lessonSubcategoryVM: LessonSubcategoryViewModel,
        lessonPictureVM: LessonPictureViewModel,
        lessonNameVM: LessonNameViewModel,
        lessonDescriptionVM: LessonDescriptionViewModel
    ) {
        lessonCategoryVM
            .onItemSelected
            .bind { [weak self] item, offset in
                guard let self = self else { return }
                self.categoryOffsetX = offset
                if self.itemID != item.id {
                    self.itemID = item.id
                    self.getSubCategories(for: item.id)
                    self.streamData.category = item.id
                }
            }
            .disposed(by: lessonCategoryVM.bag)

        lessonSubcategoryVM
            .onItemSelected
            .bind { [weak self] name in
                self?.streamData.subCategory = name
            }
            .disposed(by: lessonSubcategoryVM.bag)

        lessonNameVM
            .textRelay
            .bind { [weak self] text in
                self?.streamData.lessonName = text
            }
            .disposed(by: lessonNameVM.bag)

        lessonDescriptionVM
            .textRelay
            .bind { [weak self] text in
                self?.streamData.lessonDescription = text
            }
            .disposed(by: lessonDescriptionVM.bag)

        lessonCategoryVM
            .onClearTFSubCategoryText
            .bind { [weak self, weak lessonSubcategoryVM] _ in
                lessonSubcategoryVM?.clearDataItems()
                self?.streamData.subCategory = nil
            }
            .disposed(by: lessonCategoryVM.bag)

        lessonSubcategoryVM
            .textRelay
            .bind { [weak self] text in
                self?.streamData.subCategory = text
            }
            .disposed(by: lessonSubcategoryVM.bag)

        lessonPictureVM
            .onPickImage
            .subscribe(onNext: { [weak self] image in
                self?.streamData.lessonImage = image
            })
            .disposed(by: lessonPictureVM.bag)

    }

    private func showErrors(
        for model: StreamData,
        lessonSubcategoryVM: LessonSubcategoryViewModel,
        lessonNameVM: LessonNameViewModel,
        lessonPictureVM: LessonPictureViewModel
    ) -> Bool {

        var hasError = false

        if streamData.subCategory?.isEmpty ?? true {
            hasError = true
            lessonSubcategoryVM.warningRelay.accept(())
            let indexPath = IndexPath(row: 3, section: 0)
            self.scrollTo.accept(indexPath)
        }
        if streamData.lessonImage == nil {
            hasError = true
            lessonPictureVM.warningRelay.accept(())
            let indexPath = IndexPath(row: 4, section: 0)
            self.scrollTo.accept(indexPath)
        }
        if streamData.avatarUrl != nil {
            lessonPictureVM.warningRelayClear.accept(())
        }
        if streamData.lessonName?.isEmpty ?? true {
            hasError = true
            lessonNameVM.warningRelay.accept(())
            let indexPath = IndexPath(row: 5, section: 0)
            self.scrollTo.accept(indexPath)
        }
        return hasError
    }

    private func scrollToIndexPath(
        lessonSubcategoryVM: LessonSubcategoryViewModel,
        lessonNameVM: LessonNameViewModel,
        lessonDescriptionVM: LessonDescriptionViewModel
    ) -> Bool {
        var needToScroll = false

        lessonNameVM
            .beginEditingName
            .bind { [weak self] value in
                if value == true {
                    needToScroll = true
                    let indexPath = IndexPath(row: 3, section: 0)
                    self?.scrollTo.accept(indexPath)
                }
            }
            .disposed(by: lessonNameVM.bag)

        lessonDescriptionVM
            .beginEditingDescription
            .bind { [weak self] value in
                if value == true {
                    needToScroll = true
                    let indexPath = IndexPath(row: 4, section: 0)
                    self?.scrollTo.accept(indexPath)
                }
            }
            .disposed(by: lessonDescriptionVM.bag)

        lessonSubcategoryVM
            .beginEditingSubcategory
            .bind { [weak self] value in
                if value == true {
                    needToScroll = true
                    let indexPath = IndexPath(row: 1, section: 0)
                    self?.scrollTo.accept(indexPath)
                }
            }
            .disposed(by: lessonSubcategoryVM.bag)

        return needToScroll
    }
}
