//
//  AppCoordinator.swift
//  talkme_ios
//
//  Created by Yura Fomin on 23.12.2020.
//

import RxCocoa
import RxSwift

final class AppCoordinator {

    // MARK: - Private properties

    private let authCoordinator = AuthCoordinator()
    private let tabBarCoordinator = TabBarCoordinator()
    weak var navController: UINavigationController?
    private let onLoggedIn = PublishRelay<Void>()
    private let bag = DisposeBag()

    // MARK: - Public Method

    func start() -> UINavigationController? {
        if UserDefaultsHelper.shared.accessToken.isEmpty {
            let navVC = authCoordinator.start(onLoggedIn: onLoggedIn)
            navController = navVC
        } else {
            let tabVC = tabBarCoordinator.start()
            let navController = SimpleNavController()
            self.navController = navController
            navController.navigationBar.isHidden = true
            navController.setViewControllers([tabVC], animated: true)
        }
        bindOnLoggedIn()
        bindAppToRoot()
        return navController
    }

    private func bindOnLoggedIn() {
        onLoggedIn
            .bind { [weak self] in
                AmplitudeProvider.shared.loginTracking()
                AppsFlyerProvider.shared.trackUserCompletedLoggin()
                self?.showTabbar()
            }
            .disposed(by: bag)
    }

    private func showTabbar() {
        let tabbarVC = tabBarCoordinator.start()
        navController?.setViewControllers([tabbarVC], animated: true)
        navController?.navigationBar.isHidden = true
    }

    private func bindAppToRoot() {
        tabBarCoordinator
            .profileCoordinator
            .flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                switch flow {
                case .exitAppToRoot, .exitToPreviousScreen:
                    self.navController?.viewControllers.removeAll()
                    guard let app = UIApplication.shared.delegate as? AppDelegate else { return }
                    app.makeLogout()
                case .showTopUpBalance:
                    self.tabBarCoordinator.pushToTopUpBalance()
                }
            }
            .disposed(by: tabBarCoordinator.profileCoordinator.bag)

        tabBarCoordinator
            .mainCoordinator
            .flow
            .bind { [weak self] flow in
                guard let self = self else { return }
                switch flow {
                case .exitAppToRoot, .exitToPreviousScreen:
                    self.navController?.viewControllers.removeAll()
                    guard let app = UIApplication.shared.delegate as? AppDelegate else { return }
                    app.makeLogout()
                case .showTopUpBalance:
                    self.tabBarCoordinator.pushToTopUpBalance()
                }
            }
            .disposed(by: tabBarCoordinator.mainCoordinator.bag)
    }
}
// build 1.0.14 (350)
