//
//  SearchCoordinator.swift
//  talkme_ios
//
//  Created by 1111 on 08.02.2021.
//

import RxCocoa
import RxSwift

final class SearchCoordinator {

    enum Flow {
        case showTopUpBalance
        case exitAppToRoot
    }

    // MARK: - Public Properties

    private(set) weak var navigationController: BaseNavigationController?
    let bag = DisposeBag()
    let accountService: AccountServiceProtocol = AccountService()
    let flow = PublishRelay<Flow>()

    // MARK: - Private Properties

    private let qrCoordinator = QrCodeCoordinator()
    private let profileCoordinator = ProfileCoordinator()
    private let lessonCoordinator = LessonCoordinator()
    private let chatsCoordinator = ChatsCoordinator()
    private let streamCoordinator = StreamCoordinator()
    private let lessonsService: LessonsServiceProtocol = LessonsService()
    private let mainService: MainServiceProtocol = MainService()
    private var lessonsBag = DisposeBag()

    // MARK: - Initializers

    init() {
        bindStreamCoordinator()
    }

    // MARK: - Public Methods

    func start() -> UINavigationController {
        let viewModel = SearchSpecialistViewModel(service: lessonsService)
        let controller = SearchSpecialistController(viewModel: viewModel)
        let navigationController = BaseNavigationController(rootViewController: controller)
        self.navigationController = navigationController
        bindSearchSpecialistViewModel(viewModel: viewModel)
        bindQrButton()
        return navigationController
    }

    // MARK: - Private Methods

    private func showUserAccountInfoVC(classNumber: String) {
        let viewModel = UserAccountViewModel(service: accountService, classNumber: classNumber)
        let controller = UserAccountViewController(viewModel: viewModel)
        bindUserViewModel(viewModel: viewModel)
        self.navigationController?.pushViewController(controller, animated: true)
    }

    private func bindUserViewModel(viewModel: UserAccountViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self] flow in
                switch flow {
                case .onLessonDetail(let id):
                    self?.showLessonDetailVC(id: id)
                case .makeComplaint(let userId):
                    self?.showActionSheet(userId: userId)
                case .watchStream(let userId, let lessonId, let streamsList, let numberOfStreamsInOnePage):
                    self?.showSavedStreams(from: streamsList,
                                           createdBy: userId,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage)
                case .onListOfUsersSavedLessons(let userId):
                    self?.showCategoryFilteredLessonsVC(createdBy: userId, withCategory: .saved)
                }
            })
            .disposed(by: viewModel.bag)

        viewModel
            .onCreateChatTapped
            .bind { [weak self] id in
                self?.showChat(userId: id)
            }
            .disposed(by: viewModel.bag)
    }

    func showAccountVC(navigationController: UINavigationController) {
        let accountInfoViewModel = AccountInfoViewModel(service: accountService)
        let accountVC = AccountInfoViewController(viewModel: accountInfoViewModel)
        bindAccountViewModel(model: accountInfoViewModel)
        accountInfoViewModel.flow.accept(.updateMainInfo)
        navigationController.pushViewController(accountVC, animated: true)
    }

    private func bindAccountViewModel(model: AccountInfoViewModel) {
        model.flow
            .bind { [weak self] flow in
                switch flow {
                case .updateMainInfo:
                    return
                case.watchStream(let lessonId, let streamsList, let numberOfStreamsInPage):
                    self?.showSavedStreams(from: streamsList,
                                           createdBy: UserDefaultsHelper.shared.userId,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInPage)
                case .onListOfUsersSavedLessons(let userId):
                    self?.showCategoryFilteredLessonsVC(createdBy: userId, withCategory: .saved)
                case .deleteSavedStream(let lessonId):
                    self?.showDeleteStreamPopUp(for: lessonId) {
                        model.reloadSavedStreams.accept(())
                    }
                }
            }
            .disposed(by: model.bag)
    }

    private func showLessonDetailVC(id: Int) {
        lessonsBag = DisposeBag()
        lessonCoordinator.start(navigationController, lessonId: id)
        bindLessonCoordinator()
    }

    private func bindLessonCoordinator() {
        lessonCoordinator
            .flow
            .bind { [weak self] flow in
                switch flow {
                case .completeLessonInfo:
                    return
                case .topUpBalance:
                    self?.flow.accept(.showTopUpBalance)
                case .onChat(let id):
                    self?.showChat(userId: id)
                case .makeComplaint(let id):
                    self?.showActionSheet(userId: id)
                case .exitToRoot, .exitToPreviousScreen:
                    return
                }
            }
            .disposed(by: lessonsBag)
    }

    private func bindSearchSpecialistViewModel(viewModel: SearchSpecialistViewModel) {
        viewModel
            .flow
            .bind(onNext: { [weak self] flow in
                switch flow {
                case .onUserAccountInfo(let classNumber):
                    guard classNumber == String(UserDefaultsHelper.shared.classNumber) else {
                        self?.showUserAccountInfoVC(classNumber: classNumber)
                        return
                    }
                    guard let navigationController = self?.navigationController else { return }
                    self?.showAccountVC(navigationController: navigationController)
                case .onChat(let userId):
                    self?.showChat(userId: userId)
                }
            })
            .disposed(by: viewModel.bag)
    }

    private func bindQrButton() {
        guard let navigationController = navigationController else { return }
        navigationController
            .qrButtonTapped
            .bind { [weak self] _ in
                guard let navigationController = self?.navigationController else { return }
                self?.qrCoordinator.start(from: navigationController)
            }
            .disposed(by: navigationController.bag)
    }

    private func showChat(userId: Int) {
        guard let navigationController = navigationController else { return }
        chatsCoordinator.startChat(navigationController, userId: userId)
    }

    private func showCategoryFilteredLessonsVC(createdBy userId: Int? = nil, withCategory category: LessonCategoryType) {
        let model = CategoryFilteredLessonsViewModel(category, mainService: mainService, lessonsService: lessonsService, streamsOwnerId: userId)
        let vc = CategoryFilteredLessonsViewController(model)
        bindFilteredLessonsModel(model)
        navigationController?.pushViewController(vc, animated: true)
    }

    private func bindFilteredLessonsModel(_ model: CategoryFilteredLessonsViewModel) {
        model
            .flow
            .bind { [weak self, weak model] event in
                guard model != nil else { return }
                switch event {
                case .showFilters:
                    break
                case .showLessonInfo(let id):
                    self?.showLessonDetailVC(id: id)
                case .showLiveStream:
                    break
                case .showAllSavedStreams(let lessonId, let categoriesIds, let streamsList, let numberOfStreamsInOnePage):
                    self?.showSavedStreams(from: streamsList,
                                           belongingTo: categoriesIds,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage)
                case .deleteButtonTapped(let lessonId):
                    self?.showDeleteStreamPopUp(for: lessonId) {
                        model?.reloadStreams.accept(())
                    }
                case .onProfileSelect:
                    break
                case .showUsersSavedStreams(let userId, let lessonId, let categoriesIds, let streamsList, let numberOfStreamsInOnePage):
                    self?.showSavedStreams(from: streamsList,
                                           belongingTo: categoriesIds,
                                           createdBy: userId,
                                           startFrom: lessonId,
                                           addNewStreamsToListByChunksWithSizeOf: numberOfStreamsInOnePage)
                }
            }
            .disposed(by: model.bag)
    }

    private func showSavedStreams(from streamsList: [LiveStream],
                                  belongingTo categoriesIds: [Int] = [0],
                                  createdBy userId: Int? = nil,
                                  startFrom lessonId: Int,
                                  addNewStreamsToListByChunksWithSizeOf countOfStreamsInOnePage: Int
    ) {
        streamCoordinator.start(navigationController,
                                lessonId: lessonId,
                                lessonsInfo: streamsList,
                                numberOfStreamsInPage: countOfStreamsInOnePage,
                                watchSave: true,
                                watchStreamsCreatedBy: userId,
                                withCategories: categoriesIds)
    }

    private func bindStreamCoordinator() {
        streamCoordinator
            .flow
            .bind { [weak self] event in
                switch event {
                case .exitToRoot:
                    self?.navigationController?.popToRootViewController(animated: true)
                case .exitToPreviousScreen:
                    self?.navigationController?.popViewController(animated: true)
                case .toChat:
                    break
                case .makeComplaint(let id):
                    self?.showActionSheet(userId: id)
                }
            }
            .disposed(by: bag)
    }

    private func showReportPopUp(userId: Int) {
        guard let accountService = accountService as? AccountService else { return }
        let view = StreamComplaintPopUpView(accountService: accountService, userAbout: userId )
        let drawer = DrawerViewController(contentView: view)

        view.flow
            .bind { [weak self] flow in
                self?.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self?.presentAlertAndCloseStream()
                case .dissmissPopupWithError:
                    self?.presentAlertWithError()
                }
            }
            .disposed(by: bag)

        navigationController?.present(drawer, animated: true)
    }

    private func showActionSheet(userId: Int) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let reportAction = UIAlertAction(title: "Пожаловаться", style: .default) { _ in
            self.showReportPopUp(userId: userId)
        }
        reportAction.setValue(UIColor.red, forKey: "titleTextColor")

        let blockAction = UIAlertAction(title: "Заблокировать", style: .default) { _ in
            self.showBlockPopUp(userId: userId)
        }
        blockAction.setValue(UIColor.red, forKey: "titleTextColor")

        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel)
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")

        alert.addAction(blockAction)
        alert.addAction(reportAction)
        alert.addAction(cancelAction)

        navigationController?.present(alert, animated: true)
    }

    private func showBlockPopUp(userId: Int) {
        guard let accountService = accountService as? AccountService else { return }
        let view = StreamBlockUserPopUpView(accountService: accountService, userAbout: userId )
        let drawer = DrawerViewController(contentView: view)

        view.flow
            .bind { [weak self] flow in
                self?.dismissDrawer()
                switch flow {
                case .dissmissPopupWithSuccess:
                    self?.presentSuccessBlockAndCloseStream()
                case .dissmissPopupWithError:
                    self?.presentAlertWithError()
                case .cancelPopup:
                    self?.dismissDrawer()
                }
            }
            .disposed(by: view.bag)

        navigationController?.present(drawer, animated: true)
    }

    private func presentSuccessBlockAndCloseStream() {
        let alert = UIAlertController(title: "block_alert_success".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { _ in
            self.flow.accept(.exitAppToRoot)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func presentAlertWithError() {
        let alert = UIAlertController(title: "complaint_alert_error".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func presentAlertAndCloseStream() {
        let alert = UIAlertController(title: "complaint_alert_success".localized, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { _ in
            self.flow.accept(.exitAppToRoot)
        }
        alert.addAction(action)
        navigationController?.present(alert, animated: true)
    }

    private func dismissDrawer() {
        navigationController?.dismiss(animated: true)
    }

    private func showDeleteStreamPopUp(for lessonId: Int, action: @escaping () -> Void) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let deleteAction = UIAlertAction(title: "popup_delete_lesson_popup_title".localized, style: .default) { [weak self] _ in
            guard let self = self
            else { return }
            self.lessonsService
                .deleteLesson(withId: lessonId)
                .subscribe { event in
                    switch event {
                    case .success:
                        action()
                    case .error:
                        print("Get Error when try to delete lesson with id \(lessonId)")
                    }
                }.disposed(by: self.bag)
        }
        deleteAction.setValue(UIColor.red, forKey: "titleTextColor")

        let cancelAction = UIAlertAction(title: "popup_delete_profile_no".localized, style: .cancel)
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")

        alert.addAction(deleteAction)
        alert.addAction(cancelAction)

        navigationController?.present(alert, animated: true)
    }
}
