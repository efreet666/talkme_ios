//
//  SearchSpecialistController.swift
//  talkme_ios
//
//  Created by 1111 on 07.02.2021.
//

import RxCocoa
import RxSwift

final class SearchSpecialistController: UIViewController {

    // MARK: - Private Properties

    private let tableView: UITableView = {
        let tbv = UITableView()
        tbv.backgroundColor = .clear
        tbv.separatorStyle = .none
        tbv.keyboardDismissMode = .onDrag
        tbv.rowHeight = UITableView.automaticDimension
        tbv.registerCells(withModels: SpecialistInfoCellViewModel.self)
        tbv.isUserInteractionEnabled = true
        return tbv
    }()

    private let navigationTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: 12)
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let searchBar: StreamSearchBar = {
        let bar = StreamSearchBar()
        bar.searchTextField.placeholder = "search_specialist_searchBar_placeholder".localized
        bar.backgroundColor = TalkmeColors.white
        bar.searchTextField.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        bar.searchTextField.minimumFontSize = UIScreen.isSE ? 13 : 15
        bar.layer.cornerRadius = UIScreen.isSE ? 17 : 21
        return bar
    }()

    private let warningLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.textColor = TalkmeColors.warningColor
        lbl.font = .montserratFontMedium(ofSize: 13)
        lbl.isHidden = true
        return lbl
    }()

    private lazy var recognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        recognizer.cancelsTouchesInView = false
        return recognizer
    }()

    private let viewModel: SearchSpecialistViewModel
    private let bag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: SearchSpecialistViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = TalkmeColors.blackLabels
        searchBar.searchTextField.becomeFirstResponder()
        searchBar.searchTextField.delegate = self
        setupViews()
        bindVM()
        tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: UIScreen.main.bounds.width / 3.1, right: 0.0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        setupNavigationBar()
    }

    // MARK: - Private Methods

    private func bindVM() {
        viewModel
            .dataItems
            .bind(to: tableView.rx.items) { tableView, row, data in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withModel: data, for: indexPath)
                data.configureAny(cell)
                tableView.deselectRow(at: indexPath, animated: true)
                return cell
            }
            .disposed(by: bag)

        tableView.rx
            .modelSelected(AnyTableViewCellModelProtocol.self)
            .compactMap { user in
                if let users = user as? SpecialistInfoCellViewModel {
                    return .onUserAccountInfo(classNumber: String(users.user.id) ?? "")
                }
                return nil
            }
            .bind(to: viewModel.flow)
            .disposed(by: viewModel.bag)

        searchBar.searchTextField.rx.text
            .orEmpty
            .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
            .bind { [weak viewModel] text in
                viewModel?.onEnter(text: text)
            }
            .disposed(by: bag)

        viewModel
            .onShowWarning
            .bind { [weak self] needToShowError in
                self?.showError(needToShowError, classesArrayIsEmpty: needToShowError)
            }
            .disposed(by: bag)
    }

    private func showError(_ needToShowError: Bool, classesArrayIsEmpty: Bool) {
        self.warningLabel.isHidden = !needToShowError
        self.tableView.isHidden = needToShowError
        self.warningLabel.text = classesArrayIsEmpty
            ? "search_specialist_searchBar_warning_if_empty".localized
            : "search_specialist_searchBar_warning_if_no_empty".localized
    }

    private func setupViews() {
        view.addSubviews([tableView, searchBar, warningLabel])
        view.addGestureRecognizer(recognizer)

        tableView.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(additionalSafeAreaInsets)
        }

        searchBar.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(11)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 34 : 42)
        }

        warningLabel.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom).offset(6)
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(16)
        }
    }
    private func setupNavigationBar() {
        customNavigationController?.style = UserDefaultsHelper.shared.isSpecialist
            ? .gradientBlueWithQRAndClassNumber(UserDefaultsHelper.shared.classNumber)
            : .gradientBlueWithQR
    }

    @objc func hideKeyboard() {
        searchBar.endEditing(true)
    }
}

extension SearchSpecialistController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        var newText = (text as NSString).replacingCharacters(in: range, with: string) as String
        newText = newText.trimmingCharacters(in: .whitespacesAndNewlines)
        let ruCharacters = "йцукенгшщзхъфывапролджэёячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ"
        let engCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"
        let singCharacters = "0123456789!@#$%^&*()_+~:{}|\"?><\\`,\"./;'[]=- "
        let set = CharacterSet(charactersIn: "\(ruCharacters)\(engCharacters)\(singCharacters)")
        let hasCharacterInRange = newText.rangeOfCharacter(from: set.inverted) != nil
        showError(hasCharacterInRange, classesArrayIsEmpty: false)
        return !hasCharacterInRange
    }
}
