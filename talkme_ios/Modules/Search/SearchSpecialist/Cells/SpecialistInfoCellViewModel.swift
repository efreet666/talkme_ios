//
//  SpecialistInfoCellViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 07.02.2021.
//

import RxSwift

final class SpecialistInfoCellViewModel: TableViewCellModelProtocol {

    // MARK: - Public Properties

    let user: ProfileResponse
    let onMessageTapped = PublishSubject<Void>()
    let bag = DisposeBag()

    // MARK: - Initializers

    init(user: ProfileResponse) {
        self.user = user
    }

    // MARK: - Public Methods

    func configure(_ cell: SpecialistInfoTableCell) {
        cell.setUserInfo(
            firstName: user.firstName,
            lastName: user.lastName,
            idDescription: user.username,
            classNumber: user.numberClass,
            rating: user.rating,
            avatar: user.avatarUrl,
            userId: user.id)

        cell
            .onMessageTapped
            .map { _ in }
            .bind(to: onMessageTapped)
            .disposed(by: cell.bag)
    }
}
