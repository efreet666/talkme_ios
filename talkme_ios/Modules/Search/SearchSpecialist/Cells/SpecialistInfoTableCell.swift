//
//  SpecialistInfoCell.swift
//  talkme_ios
//
//  Created by 1111 on 07.02.2021.
//

import RxSwift
import RxCocoa
import Kingfisher

final class SpecialistInfoTableCell: UITableViewCell {

    // MARK: - Public properties

    private(set) lazy var onMessageTapped = messageButton.rx.tapGesture().when(.recognized)
    private(set) var bag = DisposeBag()

    // MARK: - Private properties

    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = TalkmeColors.grayLabels
        return view
    }()

    private let firstNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 14 : 17)
        label.textColor = TalkmeColors.white
        return label
    }()

    private let lastNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = .montserratBold(ofSize: UIScreen.isSE ? 14 : 17)
        label.textColor = TalkmeColors.white
        return label
    }()

    private let idLabel: UILabel = {
        let label = UILabel()
        label.text = "ID"
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        label.textColor = TalkmeColors.white
        return label
    }()

    private let idDescriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        label.textColor = TalkmeColors.white
        return label
    }()

    private let classLabel: ClassNumberLabel = {
        let label = ClassNumberLabel()
        label.configure(type: .search)
        return label
    }()

    private let messageButton: AccountUnsubscribeButton = {
        let button = AccountUnsubscribeButton(buttonType: .message)
        button.fontSize = UIScreen.isSE ? 12 : 15
        return button
    }()

    private let avatarRatingView = AvatarRatingView()

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        cellStyle()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Method

    func setUserInfo(
        firstName: String?,
        lastName: String?,
        idDescription: String?,
        classNumber: String?,
        rating: Double?,
        avatar: String?,
        userId: Int
    ) {
        messageButton.isHidden = userId == UserDefaultsHelper.shared.userId
        firstNameLabel.text = firstName
        lastNameLabel.text = lastName
        idDescriptionLabel.text = idDescription
        classLabel.text = "№ \(classNumber ?? "")"
        avatarRatingView.configure(rating: rating)
        guard let avatar = avatar else {
            avatarRatingView.setImage(image: UIImage(named: "noAvatar"))
            return
        }
        let fullAvatarString = avatar
        guard let url = URL(string: fullAvatarString) else {
            avatarRatingView.setImage(image: UIImage(named: "noAvatar"))
            return
        }

        KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil) {
            [weak self] result in
            switch result {
            case .success(let value):
                self?.avatarRatingView.setImage(image: value.image)
            case .failure:
                self?.avatarRatingView.setImage(image: UIImage(named: "noAvatar"))
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        bag = .init()
    }

    // MARK: - Private Method

    private func setupLayout() {
        containerView.addSubviews([
            firstNameLabel,
            lastNameLabel,
            idLabel,
            idDescriptionLabel,
            classLabel,
            avatarRatingView,
            messageButton
        ])
        contentView.addSubview(containerView)

        classLabel.isHidden = true

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(10)
            make.height.equalTo(UIScreen.isSE ? 149 : 158)
            make.top.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
        }

        avatarRatingView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(12)
        }

        firstNameLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarRatingView.snp.trailing).offset(15)
            make.trailing.lessThanOrEqualTo(classLabel.snp.leading).offset(-10)
            make.top.equalToSuperview().offset(20)
        }

        lastNameLabel.snp.makeConstraints { make in
            make.leading.equalTo(firstNameLabel.snp.leading)
            make.trailing.lessThanOrEqualTo(classLabel.snp.leading).offset(-10)
            make.top.equalTo(firstNameLabel.snp.bottom)
        }

        idLabel.snp.makeConstraints { make in
            make.leading.equalTo(lastNameLabel.snp.leading)
            make.bottom.equalTo(messageButton.snp.top).offset(-20)
        }

        idDescriptionLabel.snp.makeConstraints { make in
            make.height.equalTo(idLabel.snp.height)
            make.trailing.lessThanOrEqualTo(classLabel.snp.leading).offset(-10)
            make.centerY.equalTo(idLabel.snp.centerY)
            make.leading.equalTo(idLabel.snp.trailing).offset(6)
        }

        classLabel.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.isSE ? 81 : 90)
            make.height.equalTo(29)
            make.top.equalTo(avatarRatingView.snp.top)
            make.trailing.equalToSuperview().offset(-10)
        }

        messageButton.snp.makeConstraints { make in
            make.leading.equalTo(firstNameLabel)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 18 : 21)
            make.height.equalTo(UIScreen.isSE ? 33 : 37)
            make.width.equalTo(UIScreen.isSE ? 136 : 181)
        }
    }

    private func cellStyle() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
