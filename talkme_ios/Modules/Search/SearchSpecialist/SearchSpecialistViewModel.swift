//
//  SearchSpecialistViewModel.swift
//  talkme_ios
//
//  Created by 1111 on 07.02.2021.
//

import RxCocoa
import RxSwift

final class SearchSpecialistViewModel {

    enum Flow {
        case onUserAccountInfo(classNumber: String)
        case onChat(userId: Int)
    }

    // MARK: - Public Properties

    let dataItems = PublishRelay<[SpecialistInfoCellViewModel]>()
    let bag = DisposeBag()
    let flow = PublishRelay<Flow>()
    let onShowWarning = PublishRelay<Bool>()

    // MARK: - Private Properties

    private let service: LessonsServiceProtocol

    // MARK: - Initializers

    init(service: LessonsServiceProtocol) {
        self.service = service
    }

    // MARK: - Private Methods

    func searchSpecialists(searchText: String) {
        service
            .searchSpecialists(searchText: searchText)
            .subscribe { [weak self] event in
                switch event {
                case .success(let response):
                    guard let specialists = response.user else { return }
                    if specialists.isEmpty {
                        self?.onShowWarning.accept(true)
                        self?.dataItems.accept([])
                    } else {
                        self?.onShowWarning.accept(false)
                        let items: [SpecialistInfoCellViewModel] = specialists.map { SpecialistInfoCellViewModel(user: $0) }
                        items.forEach { item in
                            self?.bindCellVM(item)
                        }
                        self?.dataItems.accept(items)
                    }
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    func onEnter(text: String) {
        guard !text.isEmpty else {
            self.dataItems.accept([])
            return
        }
        searchSpecialists(searchText: text)
    }

    private func bindCellVM(_ model: SpecialistInfoCellViewModel) {
        model.onMessageTapped
            .map { .onChat(userId: model.user.id) }
            .bind(to: flow)
            .disposed(by: model.bag)
    }
}
