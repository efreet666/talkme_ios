//
//  BaseNavigationController.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 20.02.2021.
//

import RxSwift
import RxCocoa
import UIKit

final class BaseNavigationController: UINavigationController {

    private enum Constants {
        static let navBarHeight: CGFloat = UIScreen.isSE ? 53 : 67
    }

    enum Style {
        case plainWhite(title: String?)
        case plainWhiteWithQR(title: String?)
        case gradientBlue
        case gradientBlueWithQR
        case gradientBlueWithQRAndClassNumber(String)
        case popup
    }

    // MARK: - Public properties

    lazy var qrButtonTapped = qrButton.rx.tapGesture().when(.recognized)
    let bag = DisposeBag()

    var style: Style  = .gradientBlue {
        didSet {
            changeAppearance()
        }
    }

    override var isNavigationBarHidden: Bool {
        get {
            return customNavigationBar.isHidden
        }
        set {
            customNavigationBar.isHidden = newValue
            statusBarView.isHidden = newValue
            updateConstraintsForHidden(newValue)
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        switch style {
        case .gradientBlue, .gradientBlueWithQR, .gradientBlueWithQRAndClassNumber:
            return .lightContent
        default:
            if #available(iOS 13.0, *) {
                return .darkContent
            } else {
                return .default
            }
        }
    }

    // MARK: - Private Properties

    private let classNumber: ClassNumberLabel = {
        let label = ClassNumberLabel()
        label.configure(type: .navBar)
        return label
    }()

    private let qrButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "qrCodeBig")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentMode = .scaleAspectFit
        button.tintColor = .white
        button.contentEdgeInsets = UIScreen.isSE
            ? .init(top: 13, left: 13, bottom: 13, right: 13)
            : .init(top: 9, left: 9, bottom: 9, right: 9)
        return button
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
        label.textAlignment = .center
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let customNavigationBar = NavBarView(withStyle: .gradientBlue)

    private let backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "backButton")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentMode = .scaleAspectFit
        button.tintColor = .white
        button.contentEdgeInsets = UIScreen.isSE
            ? .init(top: 14, left: 17, bottom: 14, right: 17)
            : .init(top: 12, left: 16, bottom: 12, right: 16)
        return button
    }()

    private let dismissButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "dismissButton"), for: .normal)
        button.contentMode = .scaleAspectFit
        button.tintColor = .white
        button.isHidden = true
        button.contentEdgeInsets = UIScreen.isSE
            ? .init(top: 15, left: 15, bottom: 15, right: 15)
            : .init(top: 12, left: 12, bottom: 12, right: 12)
        return button
    }()

    private let logoView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo")?.withRenderingMode(.alwaysTemplate))
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let statusBarView: NavBarView = {
        let view = NavBarView(withStyle: .gradientBlue)
        return view
    }()

    // MARK: - Initializers

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        initialSetUp()
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initialSetUp()
    }

    init() {
        super.init(nibName: nil, bundle: nil)
        initialSetUp()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpConstraints()
        additionalSafeAreaInsets.top = Constants.navBarHeight
        interactivePopGestureRecognizer?.delegate = self
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        backButton.isHidden = false
        setNeedsStatusBarAppearanceUpdate()
    }

    @discardableResult
    override func popViewController(animated: Bool) -> UIViewController? {
        let vc = super.popViewController(animated: animated)
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        return vc
    }

    @discardableResult
    override func popToViewController(_ viewController: UIViewController, animated: Bool) -> [UIViewController]? {
        let vcs = super.popToViewController(viewController, animated: animated)
        backButton.isHidden = viewControllers.count <= 1
        return vcs
    }

    @discardableResult
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        let vcs = super.popToRootViewController(animated: animated)
        backButton.isHidden = true
        return vcs
    }

    override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        super.setViewControllers(viewControllers, animated: animated)
        backButton.isHidden = viewControllers.count <= 1
    }

    // MARK: - Private Methods

    private func initialSetUp() {
        super.isNavigationBarHidden = true
        backButton.isHidden = true
        setNeedsStatusBarAppearanceUpdate()
        bindBackButton()
        bindDismissButton()
    }

    private func setUpConstraints() {
        view.addSubviews(customNavigationBar, statusBarView)

        customNavigationBar.addSubviews(backButton, dismissButton, logoView, titleLabel, qrButton, classNumber)

        statusBarView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(UIApplication.topInset == 0 ? UIApplication.leftInset: UIApplication.topInset )
        }

        customNavigationBar.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(statusBarView.snp.bottom)
            make.height.equalTo(Constants.navBarHeight)
        }

        backButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.size.equalTo(44)
            make.centerX.equalTo(customNavigationBar.snp.leading).inset(UIScreen.isSE ? 16 : 19)
        }

        dismissButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(-1)
            make.centerY.equalToSuperview()
            make.size.equalTo(44)
        }

        logoView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(UIScreen.isSE ? CGSize(width: 56, height: 30) : CGSize(width: 82, height: 44))
        }

        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.greaterThanOrEqualTo(backButton.snp.centerX).offset(UIScreen.isSE ? 10 : 11).priority(750)
            make.trailing.greaterThanOrEqualTo(qrButton.snp.centerX).offset(UIScreen.isSE ? -14 : -18).priority(1000)
        }

        qrButton.snp.makeConstraints { make in
            make.size.equalTo(44)
            make.centerY.equalToSuperview()
            make.centerX.equalTo(customNavigationBar.snp.trailing).inset(UIScreen.isSE ? 20 : 25)
            make.centerX.equalTo(classNumber.snp.trailing).offset(UIScreen.isSE ? 20 : 29)
        }

        classNumber.snp.makeConstraints { make in
            make.centerY.equalTo(qrButton)
            make.height.equalTo(UIScreen.isSE ? 18 : 26)
            make.leading.greaterThanOrEqualTo(logoView.snp.trailing).offset(UIScreen.isSE ? 22 : 18)
        }
    }

    private func changeAppearance() {
        switch style {
        case .gradientBlue:
            hideLogoShowTitleChangeStyle(false)
            qrButton.isHidden = true
            classNumber.isHidden = true
            backButton.isHidden = false
        case.gradientBlueWithQR:
            hideLogoShowTitleChangeStyle(false)
            qrButton.isHidden = false
            qrButton.tintColor = .white
            classNumber.isHidden = true
            backButton.isHidden = viewControllers.count <= 1
        case .gradientBlueWithQRAndClassNumber(let number):
            hideLogoShowTitleChangeStyle(false)
            qrButton.isHidden = false
            qrButton.tintColor = .white
            classNumber.isHidden = true
            classNumber.text = "№ \(number)"
            backButton.isHidden = viewControllers.count <= 1
        case .plainWhite(let title):
            hideLogoShowTitleChangeStyle(true)
            qrButton.isHidden = true
            classNumber.isHidden = true
            titleLabel.text = title
            backButton.isHidden = false
        case .plainWhiteWithQR(let title):
            hideLogoShowTitleChangeStyle(true)
            qrButton.isHidden = false
            qrButton.tintColor = TalkmeColors.blueCollectionCell
            classNumber.isHidden = true
            titleLabel.text = title
            backButton.isHidden = viewControllers.count <= 1
        case .popup:
            hideLogoShowTitleChangeStyle(false)
            backButton.isHidden = true
            dismissButton.isHidden = false
            qrButton.isHidden = true
            classNumber.isHidden = true
            backButton.isHidden = true
        }
        setNeedsStatusBarAppearanceUpdate()
    }

    func hideLogoShowTitleChangeStyle(_ logoIsHidden: Bool) {
        logoView.isHidden = logoIsHidden
        titleLabel.isHidden = !logoIsHidden
        switch style {
        case .gradientBlue, .gradientBlueWithQR, .gradientBlueWithQRAndClassNumber, .popup:
            customNavigationBar.style = .gradientBlue
            statusBarView.style = .gradientBlue
            backButton.tintColor = .white
        case .plainWhite, .plainWhiteWithQR:
            customNavigationBar.style = .plainWhite
            statusBarView.style = .plainWhite
            backButton.tintColor = TalkmeColors.blueCollectionCell
        }
    }

    private func updateConstraintsForHidden(_ isHidden: Bool) {
        if isHidden {
            additionalSafeAreaInsets.top -= Constants.navBarHeight
        } else {
            additionalSafeAreaInsets.top += Constants.navBarHeight
        }
    }

    private func bindBackButton() {
        backButton.rx
            .tap
            .bind { [weak self] _ in
                self?.popViewController(animated: true)
            }.disposed(by: bag)

        interactivePopGestureRecognizer?.rx
            .event
            .bind { [weak self] gesture in
                guard let self = self else { return }
                switch gesture.state {
                case .changed:
                    self.backButton.isHidden = false
                case .ended:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.backButton.isHidden = self.viewControllers.count <= 1
                    }
                default:
                    break
                }
            }.disposed(by: bag)
    }

    private func bindDismissButton() {
        dismissButton.rx
            .tap
            .bind { [weak self] _ in
                self?.dismiss(animated: true)
            }.disposed(by: bag)
    }
}

extension BaseNavigationController: UIGestureRecognizerDelegate {

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}
