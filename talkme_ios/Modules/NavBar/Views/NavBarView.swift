//
//  NavBarView.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 20.02.2021.
//

import UIKit

final class NavBarView: UIView {

    enum Style {
        case gradientBlue
        case plainWhite
    }

    // MARK: - Public properties

    var style: Style {
        didSet {
            configure()
        }
    }

    // MARK: - Private Properties

    private lazy var gradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [
            TalkmeColors.deepBlue.cgColor,
            TalkmeColors.blueLabels.cgColor
        ]
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer.transform = CATransform3DMakeAffineTransform(
            CGAffineTransform(
                a: 1,
                b: 0,
                c: 0,
                d: 18.37,
                tx: 0,
                ty: -8.68
            )
        )
        return layer
    }()

    // MARK: - Initializers

    init(withStyle style: Style) {
        self.style = style
        super.init(frame: .zero)
        configure()
        backgroundColor = .white
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    override func layoutSubviews() {
        super.layoutSubviews()
        guard style == .gradientBlue else { return }
        gradient.frame = bounds
    }

    // MARK: - Private Methods

    private func configure() {
        switch style {
        case .gradientBlue:
            guard let sublayers = layer.sublayers else {
                layer.addSublayer(gradient)
                return
            }
            if !sublayers.contains(gradient) {
                layer.insertSublayer(gradient, at: 0)
            }
        case .plainWhite:
            gradient.removeFromSuperlayer()
        }
    }
}
