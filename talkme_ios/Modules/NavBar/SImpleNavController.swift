//
//  SimpleNavController.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 24.02.2021.
//

import UIKit

final class SimpleNavController: UINavigationController {

    override var childForStatusBarStyle: UIViewController? {
        return visibleViewController
    }
    
    func setupUI() {
        navigationBar.backItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationBar.backItem?.title = ""// "talkme_navigation_back".localized
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.tintColor = .white
    }
}
