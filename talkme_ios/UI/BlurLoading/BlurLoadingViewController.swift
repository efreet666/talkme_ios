//
//  BlurLoadingViewController.swift
//  talkme_ios
//
//  Created by andy on 01.08.2022.
//

import UIKit

final class BlurLoadingViewController: UIViewController {

    // MARK: - Private Properties

    private var loadingActivityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()

        indicator.style = .whiteLarge
        indicator.color = .white
        indicator.autoresizingMask = [
            .flexibleLeftMargin, .flexibleRightMargin,
            .flexibleTopMargin, .flexibleBottomMargin
        ]

        indicator.startAnimating()

        return indicator
    }()

    private var blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.8
        blurEffectView.autoresizingMask = [
            .flexibleWidth, .flexibleHeight
        ]

        return blurEffectView
    }()

    private var isPresented = false

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)

        blurEffectView.frame = view.bounds
        view.insertSubview(blurEffectView, at: 0)

        loadingActivityIndicator.center = CGPoint(
            x: view.bounds.midX,
            y: view.bounds.midY
        )
        view.addSubview(loadingActivityIndicator)
    }

    // MARK: - Public Methods

    func show() {
        guard !isPresented else { return }

        isPresented = true

        UIApplication.shared.topViewController?.present(self, animated: false)
    }

    func hide() {
        guard isPresented else { return }

        dismiss(animated: false, completion: nil)

        isPresented = false
    }
}
