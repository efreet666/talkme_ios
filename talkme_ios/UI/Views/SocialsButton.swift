//
//  SocialsButton.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 24.02.2021.
//

import UIKit

enum SocialButtonType {
    case vkontakte
    case facebook
    case instagram
    case tiktok
    case telegram
    case odnoclassniki
    case yandex
    case google
    case appleID
    case mailru

    var buttonImage: UIImage {
        switch self {
        case .vkontakte:
            return UIImage(named: "vk") ?? UIImage()
        case .facebook:
            return UIImage(named: "facebook") ?? UIImage()
        case .instagram:
            return UIImage(named: "instagram") ?? UIImage()
        case .tiktok:
            return UIImage(named: "tiktok") ?? UIImage()
        case .telegram:
            return UIImage(named: "telegram") ?? UIImage()
        case .odnoclassniki:
            return UIImage(named: "odnoclassniki") ?? UIImage()
        case .yandex:
            return UIImage(named: "yandexButton") ?? UIImage()
        case .google:
            return UIImage(named: "googleButton") ?? UIImage()
        case .appleID:
            return UIImage(named: "appleButton") ?? UIImage()
        case .mailru:
            return UIImage(named: "mailRuButton") ?? UIImage()
        }
    }
}

final class SocialsButton: UIButton {

    // MARK: - Private properties

    private let type: SocialButtonType

    // MARK: - Init

    init(type: SocialButtonType) {
        self.type = type
        super.init(frame: .zero)
        setupButton()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private methods

    private func setupButton() {
        self.setImage(type.buttonImage, for: .normal)
        self.contentHorizontalAlignment = .fill
        self.contentVerticalAlignment = .fill
        self.contentMode = .scaleAspectFit
    }
}
