//
//  PopupButtonView.swift
//  talkme_ios
//
//  Created by 1111 on 09.03.2021.
//

import RxSwift

enum PopupButtonType {
    case share
    case scan

    var image: UIImage? {
        switch self {
        case .share:
            return UIImage(named: "share")
        case .scan:
            return UIImage(named: "camera")
        }
    }

    var text: String {
        switch self {
        case .share:
            return "talkme_account_info_share".localized
        case .scan:
            return "talkme_account_info_scan".localized
        }
    }

    var color: UIColor? {
        switch self {
        case .share:
            return TalkmeColors.greenLabels
        case .scan:
            return TalkmeColors.shadow
        }
    }
}

final class PopupActionButton: UIView {

    // MARK: - Public Properties

    private(set) lazy var buttonTapped = self.rx.tapGesture().when(.recognized)

    // MARK: - Private Properties

    private let label: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 14 : 15)
        label.textColor = TalkmeColors.white
        return label
    }()

    private let imageView: UIImageView = {
        let iw = UIImageView()
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private let stackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .center
        stack.axis = .horizontal
        stack.spacing = UIScreen.isSE ? 16 : 19
        return stack
    }()

    private var buttonType: PopupButtonType = .share

    // MARK: - Init

    init(frame: CGRect, type: PopupButtonType) {
        super.init(frame: frame)
        setupLayout()
        backgroundColor = type.color
        label.text = type.text
        imageView.image = type.image
        buttonType = type
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setupLayout() {
        tintColor = .white
        layer.cornerRadius = UIScreen.isSE ? 20 : 28
        stackView.addArrangedSubviews([imageView, label])
        addSubview(stackView)

        imageView.snp.makeConstraints { make in
            make.height.equalTo(
                buttonType == .scan
                    ? UIScreen.isSE ? 15 : 21
                    : UIScreen.isSE ? 18 : 26
            )
            make.width.equalTo(
                buttonType == .scan
                    ? UIScreen.isSE ? 16 : 23
                    : UIScreen.isSE ? 15 : 21
            )
        }

        stackView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
