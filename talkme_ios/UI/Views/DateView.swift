//
//  DayMonthAndTimeView.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 16/6/2022.
//

import UIKit

class DateView: UIView {

    // MARK: - Public Methods

    func setDate(to dateInSeconds: Double) {
        dayAndMonthLabel.text = Formatters.convertDate(seconds: dateInSeconds)
    }

    func setDuration(to duration: String) {
        minutesAndHoursLabel.text = duration
    }

    // MARK: - Private Properties

    private let dayAndMonthLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        label.numberOfLines = 1
        label.textAlignment = .right
        label.textColor = TalkmeColors.white

        return label
    }()

    private let minutesAndHoursLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        label.numberOfLines = 1
        label.textAlignment = .right
        label.textColor = TalkmeColors.white

        return label
    }()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func setUpLayout() {
        addSubviews([dayAndMonthLabel,
                     minutesAndHoursLabel])

        dayAndMonthLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.trailing.equalToSuperview()
            make.leading.equalToSuperview()
            make.bottom.equalTo(minutesAndHoursLabel.snp.top)
        }

        minutesAndHoursLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.leading.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
}
