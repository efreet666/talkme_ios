//
//  Avatar+RatingView.swift
//  talkme_ios
//
//  Created by 1111 on 11.02.2021.]
//

import UIKit

final class AvatarRatingView: UIView {

    // MARK: - Private Properties

    private let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = UIScreen.isSE ? 35 : 40
        imageView.clipsToBounds = true
        return imageView
    }()

    private let ratingView: ImageAndTitleView = {
        let view = ImageAndTitleView()
        view.configure(ImageAndTitleViewType.rating(0))
        return view
    }()

    private let backgroundRating: UIView = {
        let view = UIView()
        view.backgroundColor = TalkmeColors.ratingViewYellow
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        return view
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setImage(image: UIImage?) {
        avatarImageView.image = image
    }

    func hideRating(_ hide: Bool) {
        ratingView.isHidden = hide
        backgroundRating.backgroundColor = hide ? .clear: TalkmeColors.ratingViewYellow
    }

    func configure(rating: Double?) {
        ratingView.configure(ImageAndTitleViewType.rating(rating ?? 0))
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([avatarImageView, backgroundRating, ratingView])

        avatarImageView.snp.makeConstraints { make in
            make.size.equalTo(UIScreen.isSE ? 70 : 80)
            make.leading.trailing.top.equalToSuperview()
        }

        backgroundRating.snp.makeConstraints { make in
            make.height.equalTo(21)
            make.width.equalTo(53)
            make.centerX.equalTo(avatarImageView)
            make.top.equalTo(avatarImageView.snp.bottom).inset(10)
            make.bottom.equalToSuperview()
        }
        ratingView.snp.makeConstraints { make in
            make.center.equalTo(backgroundRating)
        }
    }
}
