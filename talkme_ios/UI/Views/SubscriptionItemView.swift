//
//  SubscriptionTableau.swift
//  talkme_ios
//
//  Created by VladimirCH on 30.03.2022.
//

import RxSwift
import UIKit

final class SubscriptionItemView: UIView {

    // MARK: - Private Properties

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.blackLabels
        return label
    }()

    private let countLabel: UILabel = {
        let label = UILabel()
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 20)
        label.textColor = TalkmeColors.shadow
        return label
    }()

    // MARK: - Init

    init(titleText: String, countText: String) {
        super.init(frame: .zero)
        setupLayout()
        backgroundColor = .clear
        layer.cornerRadius = UIScreen.isSE ? 6 : 8
        layer.borderWidth = 1
        layer.borderColor = TalkmeColors.separatorFilter.cgColor
        titleLabel.text = titleText
        countLabel.text = countText
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(text: String) {
        countLabel.text = text
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews(titleLabel, countLabel)

        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(UIScreen.isSE ? 4 : 6)
        }

        countLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom)
            make.bottom.equalToSuperview().inset(UIScreen.isSE ? 8 : 10)
        }
    }
}
