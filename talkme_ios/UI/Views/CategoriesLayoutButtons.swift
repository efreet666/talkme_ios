//
//  TableOrCollectionButtonsStack.swift
//  talkme_ios
//
//  Created by 1111 on 20.02.2021.
//

import RxSwift

final class CategoriesLayoutButtons: UIView {

    // MARK: - Public Properties

    private(set) lazy var collectionButtonTapped = collectionButton.rx.tap
    private(set) lazy var tableButtonTapped = tableButton.rx.tap

    // MARK: - Private Properties

    private let bag = DisposeBag()

    private let collectionButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = TalkmeColors.white
        button.layer.cornerRadius = UIScreen.isSE ? 4 : 6
        button.setImage(UIImage(named: "activeCollectionButton"), for: .selected)
        button.setImage(UIImage(named: "collectionButton"), for: .normal)
        button.contentMode = .scaleAspectFill
        button.contentEdgeInsets = UIScreen.isSE
            ? .init(top: 8, left: 8, bottom: 8, right: 8)
            : .init(top: 11, left: 11, bottom: 11, right: 11)
        return button
    }()

    private let tableButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = TalkmeColors.white
        button.layer.cornerRadius = UIScreen.isSE ? 4 : 6
        button.setImage(UIImage(named: "activeTableButton"), for: .selected)
        button.setImage(UIImage(named: "tableButton"), for: .normal)
        button.contentMode = .scaleAspectFill
        button.contentEdgeInsets = UIScreen.isSE
            ? .init(top: 8, left: 7, bottom: 8, right: 6)
            : .init(top: 11, left: 9, bottom: 11, right: 8)
        return button
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        bindUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func setCollectionLayoutActive() {
        collectionButton.isSelected = true
        tableButton.isSelected = false
    }

    func setTableLayoutActive() {
        collectionButton.isSelected = false
        tableButton.isSelected = true
    }

    // MARK: - Private Methods

    private func bindUI() {
        collectionButtonTapped.bind { [weak self] _ in
            self?.setCollectionLayoutActive()
        }.disposed(by: bag)

        tableButtonTapped.bind { [weak self] _ in
            self?.setTableLayoutActive()
        }.disposed(by: bag)
    }

    private func setupLayout() {
        addSubviews([collectionButton, tableButton])

        collectionButton.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalTo(tableButton.snp.leading).offset(UIScreen.isSE ? -8 : -11)
            make.width.equalTo(collectionButton.snp.height)
            make.top.bottom.equalToSuperview()
            make.trailing.equalTo(snp.centerX).offset(UIScreen.isSE ? -4 : -5.5)
        }

        tableButton.snp.makeConstraints { make in
            make.size.centerY.equalTo(collectionButton)
            make.trailing.equalToSuperview()
        }
    }
}
