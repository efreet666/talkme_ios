//
//  GenderViewWithIndicator.swift
//  talkme_ios
//
//  Created by 1111 on 02.03.2021.
//

import RxSwift

enum GenderViewType {
    case male
    case female

    var genderImage: UIImage {
        switch self {
        case .male:
            return UIImage(named: "FilterMale") ?? UIImage()
        case .female:
            return UIImage(named: "FilterFemale") ?? UIImage()
        }
    }

    var indicatorColor: UIColor {
        switch self {
        case .male:
            return TalkmeColors.maleColor
        case .female:
            return TalkmeColors.femaleColor
        }
    }

    var text: String {
        switch self {
        case .male:
            return "filter_lesson_male".localized
        case .female:
            return "filter_lesson_female".localized
        }
    }

    var isMale: Bool {
        switch self {
        case .male:
            return true
        case .female:
            return false
        }
    }
}

final class GenderViewWithIndicator: UIView {

    // MARK: - Public Properties

    private(set) lazy var buttonTapped = self.rx.tapGesture().when(.recognized)

    // MARK: - Private Properties

    private let indicatorView = RoundedIndicatorView()

    private let label: UILabel = {
        let label = UILabel()
        label.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    private let imageView: UIImageView = {
        let iw = UIImageView()
        iw.contentMode = .scaleAspectFit
        return iw
    }()

    private var isMale = false
    private var indicatorColor: UIColor = .clear

    // MARK: - Init

    init(frame: CGRect, type: GenderViewType) {
        super.init(frame: .zero)
        setupLayout()
        backgroundColor = TalkmeColors.white
        layer.cornerRadius = UIScreen.isSE ? 18 : 23
        imageView.image = type.genderImage
        label.text = type.text
        indicatorView.setColor(type.indicatorColor)
        indicatorView.setOn(false)
        isMale = type.isMale
        indicatorColor = type.indicatorColor
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func becomeActive(_ isActive: Bool) {
        indicatorView.setColor(indicatorColor)
        indicatorView.setOn(isActive)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([indicatorView, label, imageView])

        indicatorView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(14)
            make.size.equalTo(UIScreen.isSE ? 16 : 20.86)
            make.centerY.equalToSuperview()
        }

        label.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(indicatorView.snp.leading)
        }

        isMale
            ? imageView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(UIScreen.isSE ? 13 : 17)
                make.size.equalTo(UIScreen.isSE ? 15 : 20)
                make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 10 : 13)
                make.trailing.equalTo(label.snp.leading).offset(UIScreen.isSE ? -8 : -10)
            }
            : imageView.snp.makeConstraints { make in
                make.leading.equalToSuperview().inset(UIScreen.isSE ? 15 : 19)
                make.size.equalTo(UIScreen.isSE ? 17 : 22)
                make.top.bottom.equalToSuperview().inset(UIScreen.isSE ? 9 : 11)
                make.trailing.equalTo(label.snp.leading).offset(UIScreen.isSE ? -9 : -11)
            }
    }
}
