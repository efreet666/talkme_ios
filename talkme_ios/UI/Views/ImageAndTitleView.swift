//

//  ImageAndTitleView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 08.01.2021.
//

import UIKit

final class ImageAndTitleView: UIView {

    // MARK: - Private Properties

    private let container: UIView = {
        let view = UIView()
        return view
    }()

    private let label: UILabel = {
        let lbl = UILabel()
        lbl.font = .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 13)
        lbl.textColor = TalkmeColors.white
        return lbl
    }()

    private var imageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        return img
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        container.layer.cornerRadius = container.bounds.height / 2
    }

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setContainerConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(_ type: ImageAndTitleViewType) {
        label.text = type.title
        container.backgroundColor = type.backgroundColor
        imageView.image = type.icon
        label.textColor = type.textColor
        label.numberOfLines = 0
        label.font = type.fontSize
    }

    // MARK: - Private Methods

    private func setContainerConstraints() {
        addSubviews(container)
        container.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            container.addSubviewsWithoutAutoresizing(imageView, label)

            imageView.snp.makeConstraints { make in
                make.height.width.equalTo(UIScreen.isSE ? 11 : 15)
                make.top.bottom.equalToSuperview()
                make.leading.equalToSuperview()
                make.trailing.equalTo(label.snp.leading).inset(UIScreen.isSE ? -5 : -6)
            }

            label.snp.makeConstraints { make in
                make.leading.equalTo(imageView.snp.trailing).inset(UIScreen.isSE ? 5 : 6)
                make.trailing.equalToSuperview()
                make.top.bottom.equalToSuperview()
            }
        }
    }
}
