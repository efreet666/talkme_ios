//
//  ImageAndTitleViewType.swift
//  talkme_ios
//
//  Created by Майя Калицева on 08.01.2021.
//

import RxCocoa
import Darwin

enum ImageAndTitleViewType {

    case rating(Double)
    case time(String)
    case bigTime(String)
    case durationTime(String)
    case date(String)
    case publicProgileTime(String)
    case liveTime(String)
    case streamRating(Double)

    var icon: UIImage? {
        switch self {
        case .rating, .streamRating:
            return UIImage(named: "ratingStar")
        case .time:
            return UIImage(named: "clockNearest")
        case .date:
            return UIImage(named: "whiteCalendar")
        case .publicProgileTime, .liveTime, .bigTime, .durationTime:
            return UIImage(named: "clock")
        }
    }

    var title: String {
        switch self {
        case .rating(let rating), .streamRating(let rating):
            return String(rating)
        case .date(let date):
            return date
        case .time(let time), .publicProgileTime(let time), .liveTime(let time), .bigTime(let time), .durationTime(let time):
            return time
        }
    }

    var backgroundColor: UIColor? {
        switch self {
        case .rating, .streamRating:
            return TalkmeColors.orangeViewBackground
        case .date, .time, .publicProgileTime:
            return .clear
        case .liveTime, .bigTime, .durationTime:
            return .clear
        }
    }

    var textColor: UIColor? {
        switch self {
        case .rating, .date, .liveTime, .bigTime, .streamRating:
            return TalkmeColors.white
        case .time, .publicProgileTime, .durationTime:
            return TalkmeColors.redLessonLabel
        }
    }

    var fontSize: UIFont? {
        switch self {
        case .streamRating:
            return .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        case .rating:
            return UIFont.montserratSemiBold(ofSize: 11)
        case .date, .liveTime, .time, .publicProgileTime:
            return UIFont.montserratSemiBold(ofSize: 12)
        case .bigTime, .durationTime:
            return .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 13)
        }
    }
}
