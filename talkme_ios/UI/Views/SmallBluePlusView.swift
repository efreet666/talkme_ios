//
//  SmallBluePlusView.swift
//  talkme_ios
//
//  Created by Майя Калицева on 12.01.2021.
//

import UIKit

final class SmallBluePlusView: UIView {

    // MARK: - Private properties

    private let image: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "plusSocialMedia")
        return img
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private methods

    private func setupLayout() {
        addSubview(image)
        image.snp.makeConstraints { make in
        make.width.height.equalToSuperview().inset(15)
        }
    }
}
