//
//  ButtonWithGreenIndicator.swift
//  talkme_ios
//
//  Created by 1111 on 25.02.2021.
//

import RxSwift

final class ButtonWithRoundedIndicator: UIView {

    // MARK: - Public Properties

    private(set) lazy var buttonTapped = self.rx.tapGesture().when(.recognized)

    // MARK: - Private Properties

    private let indicatorView = RoundedIndicatorView()

    private let label: UILabel = {
        let label = UILabel()
        label.font = .montserratFontMedium(ofSize: UIScreen.isSE ? 13 : 15)
        label.textColor = TalkmeColors.grayLabels
        return label
    }()

    // MARK: - Init

    init(frame: CGRect, text: String) {
        super.init(frame: frame)
        setupLayout()
        backgroundColor = .clear
        layer.cornerRadius = UIScreen.isSE ? 15 : 20
        layer.borderWidth = 1
        layer.borderColor = TalkmeColors.grayButtonBorder.cgColor
        label.text = text
        indicatorView.setOn(false)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func becomeActive(_ isActive: Bool) {
        indicatorView.setColor(TalkmeColors.greenIndicator)
        indicatorView.setOn(isActive)
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([indicatorView, label])

        indicatorView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(14)
            make.size.equalTo(UIScreen.isSE ? 16 : 20.86)
            make.centerY.equalToSuperview()
        }

        label.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(14)
            make.centerY.equalToSuperview()
            make.trailing.equalTo(indicatorView.snp.leading)
        }

    }
}
