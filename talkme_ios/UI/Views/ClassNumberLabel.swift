//
//  ClassNumberLabel.swift
//  talkme_ios
//
//  Created by 1111 on 11.02.2021.
//

import UIKit

enum ClassNumberLabelType {
    case mainInfo
    case search
    case navBar
    case streamSpecialist
    case stream

    var borderColor: CGColor {
        return UIColor.white.cgColor
    }

    var borderWidth: CGFloat {
        switch self {
        case .mainInfo, .streamSpecialist, .stream:
            return 0
        case .navBar, .search:
            return 1
        }
    }

    var backgroundColor: UIColor {
        switch self {
        case .streamSpecialist:
            return TalkmeColors.blueCollectionCell
        case .mainInfo:
            return TalkmeColors.classView
        case .navBar, .search:
            return .clear
        case .stream:
            return UIColor.black.withAlphaComponent(0.25)
        }
    }

    var font: UIFont {
        switch self {
        case .mainInfo, .streamSpecialist, .stream:
            return .montserratSemiBold(ofSize: UIScreen.isSE ? 13 : 15)
        case .navBar:
            return .montserratSemiBold(ofSize: UIScreen.isSE ? 12 : 15)
        case .search:
            return .montserratSemiBold(ofSize: UIScreen.isSE ? 11 : 13)
        }
    }

    var textColor: UIColor {
        switch self {
        case .mainInfo:
            return TalkmeColors.blueLabels
        case .navBar, .search, .streamSpecialist, .stream:
            return TalkmeColors.white
        }
    }
}

final class ClassNumberLabel: UILabel {

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        textAlignment = .center
        layer.masksToBounds = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func configure(type: ClassNumberLabelType) {
        layer.borderWidth = type.borderWidth
        layer.borderColor = type.borderColor
        backgroundColor = type.backgroundColor
        font = type.font
        textColor = type.textColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
    }

    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var rect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
        rect.size.width += UIScreen.isSE ? 20 : (UIScreen.main.bounds.width / 15.9231)
        return rect
    }
}
