//
//  LoaderView.swift
//  talkme_ios
//
//  Created by Yura Fomin on 26.01.2021.
//

import UIKit

final class LoaderView: UIView {

    // MARK: - Private Properties

    private let circleView = UIView()
    private let circleMediumView = UIView()
    private let circleLargeView = UIView()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func startAnimating() {
        startAnimations()
    }

    func stopAnimating() {
        circleView.layer.removeAllAnimations()
        circleMediumView.layer.removeAllAnimations()
        circleLargeView.layer.removeAllAnimations()
    }

    // MARK: - Private Methods

    private func setupLayout() {
        addSubviews([circleView, circleMediumView, circleLargeView])
        [circleView, circleMediumView, circleLargeView].forEach { view in
            view.backgroundColor = TalkmeColors.white; view.alpha = 0 }

        circleView.layer.cornerRadius = 6.5
        circleView.snp.makeConstraints { make in
            make.size.equalTo(13)
        }

        circleMediumView.layer.cornerRadius = 9.5
        circleMediumView.snp.makeConstraints { make in
            make.size.equalTo(19)
        }

        circleLargeView.layer.cornerRadius = 13.5
        circleLargeView.snp.makeConstraints { make in
            make.size.equalTo(27)
        }

        circleMediumView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview()
        }

        circleView.snp.makeConstraints { make in
            make.trailing.equalTo(circleMediumView.snp.leading).offset(-5)
            make.bottom.equalToSuperview()
        }

        circleLargeView.snp.makeConstraints { make in
            make.leading.equalTo(circleMediumView.snp.trailing).offset(5)
            make.bottom.equalToSuperview()
        }
    }

    private func startAnimations() {
        UIView.animateKeyframes(withDuration: 3.0, delay: 0, options: [.repeat]) {

            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1.0/6.0, animations: {
                self.circleView.alpha = 1
            })
            UIView.addKeyframe(withRelativeStartTime: 1.0/6.0, relativeDuration: 1.0/6.0, animations: {
                self.circleMediumView.alpha = 1
            })
            UIView.addKeyframe(withRelativeStartTime: 2.0/6.0, relativeDuration: 1.0/6.0, animations: {
                self.circleLargeView.alpha = 1
            })
            UIView.addKeyframe(withRelativeStartTime: 3.0/6.0, relativeDuration: 1.0/6.0, animations: {
                self.circleLargeView.alpha = 0
            })
            UIView.addKeyframe(withRelativeStartTime: 4.0/6.0, relativeDuration: 1.0/6.0, animations: {
                self.circleMediumView.alpha = 0
            })
            UIView.addKeyframe(withRelativeStartTime: 5.0/6.0, relativeDuration: 1.0/6.0, animations: {
                self.circleView.alpha = 0
            })
        }
    }
}
