//
//  LoaderFullView.swift
//  talkme_ios
//
//  Created by Elina Efremova on 20.04.2021.
//

import UIKit

final class LoaderFullView: UIView {

    // MARK: - Private Properties

    private let loaderView = LoaderView()

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    func startAnimating() {
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.alpha = 1
        } completion: { _ in
            self.loaderView.startAnimating()
        }
    }

    func stopAnimating() {
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.alpha = 0
        } completion: { _ in
            self.loaderView.stopAnimating()
        }
    }

    // MARK: - Private Methods

    private func setupLayout() {
        backgroundColor = TalkmeColors.lightBlack.withAlphaComponent(0.2)
        addSubview(loaderView)

        loaderView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
