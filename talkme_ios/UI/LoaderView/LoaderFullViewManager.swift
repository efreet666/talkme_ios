//
//  LoaderFullViewManager.swift
//  talkme_ios
//
//  Created by Elina Efremova on 20.04.2021.
//

import UIKit

enum LoaderFullViewManager {

    static func startAnimating() {
        let window = UIApplication.shared.delegate?.window
        window??.addSubview(loaderFullView)
        loaderFullView.startAnimating()
    }

    static func stopAnimating() {
        loaderFullView.removeFromSuperview()
        loaderFullView.stopAnimating()
    }

    // MARK: Private properties

    private static let loaderFullView = LoaderFullView(frame: UIScreen.main.bounds)
}
