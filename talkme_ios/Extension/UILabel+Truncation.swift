//
//  UILabel+Truncation.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 15.03.2021.
//

import UIKit

extension UILabel {
    func isTruncatedOrNot(_ width: CGFloat) -> Bool {
        guard let height = attributedText?.string.height(font: font, width: width) else { return false }
        let numberOfLines = Int(CGFloat(height) / self.font.lineHeight)
        if numberOfLines > self.numberOfLines {
            return true
        }
        return false
    }
}
