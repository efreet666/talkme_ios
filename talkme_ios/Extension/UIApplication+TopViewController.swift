//
//  UIApplication+TopViewController.swift
//  talkme_ios
//
//  Created by andy on 01.08.2022.
//

import UIKit

extension UIApplication {
    var topViewController: UIViewController? {
        guard let app = delegate as? AppDelegate,
              let topRootVC = app.window?.rootViewController else { return nil }

        var topController = topRootVC
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        return topController
    }
}
