//
//  UIViewController+NavigationController.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 21.02.2021.
//

import UIKit

extension UIViewController {

    var customNavigationController: BaseNavigationController? {
        return navigationController as? BaseNavigationController
    }
}
