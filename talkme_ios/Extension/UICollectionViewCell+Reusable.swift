//
//  UICollectionViewCell+Reusable.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

import UIKit

extension UICollectionViewCell {

    static var reuseIdentifier: String { String(describing: Self.self) }
}
