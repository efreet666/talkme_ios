//
//  UITableViewCell+Reusable.swift
//  talkme_ios
//
//  Created by Elina Efremova
//

import UIKit

extension UITableViewCell {

    static var reuseIdentifier: String { String(describing: Self.self) }
}
