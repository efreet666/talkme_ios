//
//  UIInterfaceOrientation+OrientationMask.swift
//  talkme_ios
//
//  Created by andy on 21.07.2022.
//

import UIKit

extension UIInterfaceOrientation {
    var orientationMask: UIInterfaceOrientationMask {
        switch self {
        case .unknown:
            return UIInterfaceOrientationMask.all
        case .portrait:
            return UIInterfaceOrientationMask.portrait
        case .portraitUpsideDown:
            return UIInterfaceOrientationMask.portraitUpsideDown
        case .landscapeLeft:
            return UIInterfaceOrientationMask.landscapeLeft
        case .landscapeRight:
            return UIInterfaceOrientationMask.landscapeRight
        @unknown default:
            return UIInterfaceOrientationMask.all
        }
    }
}
