//
//  Double+roundedToPlaces.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 21/7/2022.
//

import Foundation

extension Double {

    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
