//
//  UIDevice+Orientation.swift
//  talkme_ios
//
//  Created by andy on 21.07.2022.
//

import UIKit

extension UIDevice {
    func changeOrientation(_ orientation: UIInterfaceOrientation) {
        guard UIApplication.shared.statusBarOrientation.isPortrait != orientation.isPortrait else {
            return
        }

        // В начале поворачиваем к исходной ориентации, иначе при рассинхроне ориентации statusBar и устройства смена ориентации будет игнорироваться.
        _changeOrientation(orientation == .portrait ? .landscapeRight : .portrait)
        _changeOrientation(orientation)
    }

    func toggleOrientation() {
        let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation.isPortrait
            ? .landscapeRight
            : .portrait

        changeOrientation(orientation)
    }

    private func _changeOrientation(_ orientation: UIInterfaceOrientation) {
        AppDelegate.deviceOrientation = orientation.orientationMask
        setValue(orientation.rawValue, forKey: "orientation")
    }
}
