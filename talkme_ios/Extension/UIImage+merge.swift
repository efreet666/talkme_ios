//
//  UIImage+merge.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 27/7/2022.
//

import UIKit

extension UIImage {
    func merge(with topImage: UIImage, andInsets insets: UIEdgeInsets) -> UIImage {
    let bottomImage = self

    UIGraphicsBeginImageContext(size)

    let fullAreaSize = CGRect(x: 0, y: 0, width: bottomImage.size.width, height: bottomImage.size.height)
    bottomImage.draw(in: fullAreaSize)

    let isettedAreaSize = CGRect(x: insets.left,
                                 y: insets.top,
                                 width: bottomImage.size.width - insets.right - insets.left,
                                 height: bottomImage.size.height - insets.bottom - insets.top)
        topImage.draw(in: isettedAreaSize, blendMode: .normal, alpha: 1.0)

    let mergedImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return mergedImage
  }
}
