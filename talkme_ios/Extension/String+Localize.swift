//
//  String+Localize.swift
//  talkme_ios
//
//  Created by Майя Калицева on 11.01.2021.
//

import Foundation

extension String {

    var localized: String {
        NSLocalizedString(self, comment: "")
    }

    func localizeWithArgument(_ value: String) -> String {
        return String(format: self.localized, value)
    }
}
