//
//  UIViewController+Tabbar.swift
//  talkme_ios
//
//  Created by Elina Efremova on 05.02.2021.
//

import UIKit

extension UIViewController {

    var customTabbarVC: TabBarController? {
        tabBarController as? TabBarController
    }

    func setCustomTabbarHidden(_ isHidden: Bool) {
        customTabbarVC?.setTabbarHidden(isHidden)
    }

    func hideCustomTabBarLiveButton(_ isHidden: Bool) {
        customTabbarVC?.hideLiveButton(isHidden)
    }

}
