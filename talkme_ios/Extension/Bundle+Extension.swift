//
//  Bundle+Extension.swift
//  talkme_ios
//
//  Created by andy on 18.07.2022.
//

import Foundation

extension Bundle {
    var versionReleaseNumber: String {
        infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0"
    }
}
