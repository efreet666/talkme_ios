//
//  UIImage+resized.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 23/7/2022.
//

import UIKit

extension UIImage {
    func resized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
