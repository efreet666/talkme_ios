//
//  UIStackView+AddSubviews.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 24.02.2021.
//

import UIKit

public extension UIStackView {

    func addArrangedSubviews(_ subviews: [UIView]) {
        subviews.forEach {
            addArrangedSubview($0)
        }
    }
}
