//
//  Date + hardcodedRuMonth.swift
//  talkme_ios
//
//  Created by Yura Fomin on 08.02.2021.
//

import Foundation

extension Date {

    var hardcodedRuMonth: String {
        switch Calendar.current.component(.month, from: self) {
        case 1:
            return "янв"
        case 2:
            return "февр"
        case 3:
            return "мар"
        case 4:
            return "апр"
        case 5:
            return "мая"
        case 6:
            return "июн"
        case 7:
            return "июл"
        case 8:
            return "авг"
        case 9:
            return "сент"
        case 10:
            return "окт"
        case 11:
            return "нояб"
        case 12:
            return "дек"
        default: return ""
        }
    }
}
