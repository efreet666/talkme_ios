//
//  UIScreen+Size.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.03.2021.
//

import UIKit

extension UIScreen {

    static var size: CGSize {
        Self.main.bounds.size
    }

    static var height: CGFloat {
        size.height
    }

    static var width: CGFloat {
        size.width
    }
}
