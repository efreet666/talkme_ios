//
//  String + BanEmoji.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 07.10.2021.
//

import Foundation

extension String {
    var containsEmoji: Bool { contains { $0.isEmoji } }
}
