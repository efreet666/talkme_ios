//
//  String+backspace.swift
//  talkme_ios
//
//  Created by Dmitry Bobrov on 01.02.2022.
//

import Foundation

extension String {
    func isBackspace() -> Bool {
        return strcmp(self, "\\b") == -92
    }
}
