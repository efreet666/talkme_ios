//
//  UITextField.swift
//  talkme_ios
//
//  Created by Майя Калицева on 14.04.2021.
//

import UIKit
import RxSwift
import RxCocoa

extension UITextField {

    func setEmailTextField() {
        autocorrectionType = .no
        autocapitalizationType = .none
        keyboardType = .emailAddress
        inputAssistantItem.leadingBarButtonGroups = []
        inputAssistantItem.trailingBarButtonGroups = []
    }

    func resignWhenFinished(_ disposeBag: DisposeBag) {
        setNextResponder(nil, disposeBag: disposeBag)
    }

    func setNextResponder(_ nextResponder: UIResponder?, disposeBag: DisposeBag) {
        returnKeyType = (nextResponder != nil) ? .next : .done
        rx.controlEvent(.editingDidEndOnExit)
            .subscribe(onNext: { [weak self, weak nextResponder] in
                if let nextResponder = nextResponder {
                    nextResponder.becomeFirstResponder()
                } else {
                    self?.resignFirstResponder()
                }
            })
            .disposed(by: disposeBag)
    }
}
