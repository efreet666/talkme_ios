//
//  String+DigitsOnly.swift
//  talkme_ios
//
//  Created by Yura Fomin on 29.03.2021.
//

import Foundation

extension String {

    var digitsOnly: String {
        var string = self
        string.removeAll { !("0"..."9" ~= $0) }
        return string
    }
}
