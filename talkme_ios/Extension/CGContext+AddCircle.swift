//
//  CGContext+AddCircle.swift
//  talkme_ios
//
//  Created by 1111 on 28.01.2021.
//

import CoreGraphics

internal extension CGContext {

    func addCircle(center: CGPoint, radius: CGFloat) {
        addArc(center: center,
               radius: radius,
               startAngle: 0,
               endAngle: CGFloat(2 * Double.pi),
               clockwise: true)
    }
}
