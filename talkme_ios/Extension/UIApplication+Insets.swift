//
//  UIApplication+Insets.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.03.2021.
//

import UIKit

extension UIApplication {
    static var topInset: CGFloat {
        return Self.shared.delegate?.window??.safeAreaInsets.top ?? 0
    }

    static var bottomInset: CGFloat {
        return Self.shared.delegate?.window??.safeAreaInsets.bottom ?? 0
    }

    static var leftInset: CGFloat {
        return Self.shared.delegate?.window??.safeAreaInsets.left ?? 0
    }

    static var rightInset: CGFloat {
        return Self.shared.delegate?.window??.safeAreaInsets.right ?? 0
    }
}
