//
//  UIView+Visible.swift
//  talkme_ios
//
//  Created by andy on 27.07.2022.
//

import UIKit

extension UIView {
    enum VisibilityType {
        case show
        case hide
    }

    class func visible(_ type: VisibilityType, views: [UIView], withDuration duration: TimeInterval, animations: (() -> Void)? = nil) {
        switch type {
        case .show:
            show(views, withDuration: duration, animations: animations)
        case .hide:
            hide(views, withDuration: duration, animations: animations)
        }
    }

    class func hide(_ views: [UIView], withDuration duration: TimeInterval, animations: (() -> Void)? = nil) {
        if duration > 0 {
            animate(withDuration: duration, animations: {
                views.forEach { $0.alpha = 0 }
                animations?()
            }, completion: { _ in
                views.forEach { $0.isHidden = true }
            })
        } else {
            views.forEach { view in
                view.alpha = 0
                view.isHidden = true
            }
            animations?()
        }
    }

    class func show(_ views: [UIView], withDuration duration: TimeInterval, animations: (() -> Void)? = nil) {
        views.forEach { $0.isHidden = false }

        if duration > 0 {
            animate(withDuration: duration) {
                views.forEach { $0.alpha = 1 }
                animations?()
            }
        } else {
            views.forEach { $0.alpha = 1 }
            animations?()
        }
    }
}
