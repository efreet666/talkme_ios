//
//  Data+fromJson.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 13/8/2022.
//

import Foundation

extension Data {
    static func fromJson(fileName: String) -> Data {
        let bundle = Bundle(for: TestBundleClass.self)
        guard let url = bundle.url(forResource: fileName, withExtension: "json"),
              let data = try? Data(contentsOf: url)
        else {
            print("Can not find file \(fileName).json")
            return Data()
        }
        return data
    }
}

private class TestBundleClass {  }
