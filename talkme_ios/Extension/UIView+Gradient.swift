//
//  UIView+Gradient.swift
//  talkme_ios
//
//  Created by admin on 30.09.2022.
//

import UIKit

extension UIView {

    enum GradientDirection {
        case leftToRight
        case rightToLeft
        case topToBottom
        case bottomToTop
        case newTopToBottom
        case linear
    }

    @discardableResult
    func gradientBackground(from color1: UIColor, to color2: UIColor, direction: GradientDirection) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        switch direction {
        case .leftToRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.colors = [color1.cgColor, color2.cgColor]
        case .rightToLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.colors = [color1.cgColor, color2.cgColor]
        case .bottomToTop:
            gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
            gradient.colors = [color1.cgColor, color2.cgColor]
            gradient.locations = [0, 1]
        case .topToBottom:
            gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
            gradient.colors = [color1.cgColor, color2.cgColor]
            gradient.locations = [0.4, 1]
        case .linear:
            gradient.startPoint = CGPoint(x: 0, y: 1)
            gradient.endPoint = CGPoint(x: 1, y: 0.5)
            gradient.colors = [color1.cgColor, color2.cgColor]
            gradient.locations = [0, 1]
        case .newTopToBottom:
            gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradient.colors = [color1.cgColor, color2.cgColor]
            gradient.locations = [0, 1]
        }
        self.layer.insertSublayer(gradient, at: 0)

        return gradient
    }
}
