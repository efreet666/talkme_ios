//
//  Date+beginningOfDay.swift
//  talkme_ios
//
//  Created by Gridyushko Dmitriy on 06.05.2021.
//

import Foundation

extension Date {
    var beginningOfDay: Date {
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([.year, .month, .day])
        let components = calendar.dateComponents(unitFlags, from: self)
        return calendar.date(from: components)!
    }

    static var diffrenceBetweenRealTime: Double = 0
}
