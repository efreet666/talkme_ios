//
//  AppDelegate.swift
//  talkme_ios
//
//  Created by 1111 on 14.12.2020.
//

import GoogleSignIn
import UIKit
import FBSDKCoreKit
import SwiftyVK
import YandexLoginSDK
import MRMailSDK
import AppsFlyerLib
import Amplitude

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, AppsFlyerLibDelegate {

    var window: UIWindow?
    var vkDelegateReference: SwiftyVKDelegate?
    static var deviceOrientation: UIInterfaceOrientationMask = .portrait
    private var appCoordinator = AppCoordinator()
    private lazy var versionCheckService: AppVersionServiceProtocol = AppVersionService()
    static let addSocialSignIn = false
    static let isMvp2 = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = appCoordinator.start()
        window?.makeKeyAndVisible()
        vkDelegateReference = VKSwiftySignInService()
        do {
            try
                YXLSdk.shared.activate(withAppId: "08196842f4f64e20b41ea66614d99baf")
        } catch {
            print("YXLSdk ERROR: \(error)")
        }
        let mailSDK = MRMailSDK.sharedInstance()
        mailSDK.initialize(withClientID: "89f4563a3c4d4987932b547175be7668", redirectURI: "talk-me-auth-callback://")
        mailSDK.returnScheme = "talk-me-auth-callback"
        mailSDK.forceLogout()

        Amplitude.instance().trackingSessionEvents = true
        Amplitude.instance().initializeApiKey("05e4493db11269b74fa1f466745e4dd7")
        Amplitude.instance().logEvent("app_start")

        NotificationCenter.default.addObserver(self,
                                               selector: NSSelectorFromString("sendLaunch"),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)

        AppsFlyerLib.shared().appsFlyerDevKey = "Wqd8r6XseskuLhyNCWnRP9"
        AppsFlyerLib.shared().appleAppID = "1560817329"

        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 1.5) as Date)
        versionCheckService.checkUpdate()
        return true
    }

    @objc func sendLaunch() {
        AppsFlyerLib.shared().start()
    }

    func onConversionDataSuccess(_ installData: [AnyHashable: Any]) {
        AmplitudeProvider.shared.firstStartTraking(installData)
    }

    func onConversionDataFail(_ error: Error!) {
        // Logic for when conversion data resolution fails
        if let err = error {
            print(err)
        }
    }

    func makeLogout() {
        appCoordinator = AppCoordinator()
        VK.release()
        UserDefaultsHelper.shared.deleteTokenAndUserInfo()
        KeychainManager.remove(service: "MobileService", account: "Mobile")
        KeychainManager.remove(service: "PasswordService", account: "Password")
        window?.rootViewController = appCoordinator.start()
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        GIDSignIn.sharedInstance().handle(url)
        VK.handle(url: url, sourceApplication: options[.sourceApplication] as? String)
        YXLSdk.shared.handleOpen(url, sourceApplication: options[.sourceApplication] as? String)
        MRMailSDK.sharedInstance().handle(url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation] as? String)
        return true
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return Self.deviceOrientation
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        versionCheckService.checkUpdate()
    }
}
