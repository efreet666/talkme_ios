//
//  GcoreResponse.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.02.2021.
//

struct GcoreResponse: Decodable, Hashable {
    let owner: Bool
    let userId: Int
    let firstName: String
    let lastName: String
    let avatarUrl: String?
    let pushUrl: String?
    let token: String?
    let gcoreHlsUrl: String?
    let gcoreDashUrl: String?
    let specialistInfo: Owner?
}

struct StreamTime: Decodable, Hashable {
    let duration: String
    let lessonDate: Double
}
