//
//  TokBoxTarget.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.02.2021.
//

import Moya

enum GcoreTarget {
    case createToken(lessonId: Int)
    case finishLesson(lessonId: Int)
}

extension GcoreTarget: TargetType {

    var baseURL: URL {
        return Configuration.baseUrl
    }

    var path: String {
        switch self {
        case .createToken:
            return "/tb/g_core/"
        case .finishLesson:
            return "/tb/g_core/finish_lesson/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .createToken, .finishLesson:
            return .get
        }
    }

    var sampleData: Data {
        switch self {
        case .createToken(let lessonId):
            return Data.fromJson(fileName: "GCoreResponseStubFor\(lessonId)")
        default:
            return Data()
        }
    }

    var task: Task {
        switch self {
        case .createToken(let id):
            let params = ["lesson_id": id]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .finishLesson(let id):
            let params = ["lesson_id": id]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return [
            "Content-Type": "application/json"
        ]
    }
}
