//
//  TokBoxService.swift
//  talkme_ios
//
//  Created by Elina Efremova on 03.02.2021.
//

import Moya
import RxSwift

protocol GcoreServiceProtocol {
    func getLessonStream(lessonId: Int) -> Single<GcoreResponse>
    func finishLesson(lessonId: Int) -> Single<Int?>
}

final class GcoreService: GcoreServiceProtocol {

    // MARK: - Private Properties

    private let isLoggingEnabled = false
    private let serviceIsStubbed = Consts.liveStreamsStubbed

    private lazy var plugins: [NetworkLoggerPlugin] = {
        return isLoggingEnabled
            ? [NetworkLoggerPlugin(configuration: .init( logOptions: .verbose))]
            : []
    }()

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    private let stubbedEndpointClosure = { (target: GcoreTarget) -> Endpoint in
        return Endpoint(url: URL(target: target).absoluteString,
                        sampleResponseClosure: {.networkResponse(200, target.sampleData)},
                        method: target.method,
                        task: target.task,
                        httpHeaderFields: target.headers)
    }

    private lazy var provider = MoyaProvider<GcoreTarget>(plugins: plugins)
    private lazy var mockedProvider = MoyaProvider<GcoreTarget>(stubClosure: MoyaProvider.immediatelyStub)

    // MARK: - Public Methods

    func getLessonStream(lessonId: Int) -> Single<GcoreResponse> {
        if serviceIsStubbed,
           let lessonDetails = try? decoder.decode(GcoreResponse.self,
                                                   from: Data.fromJson(fileName: "GCoreResponseStubFor\(lessonId)")) {
            return mockedProvider.rx
                .request(.createToken(lessonId: lessonId))
                .map(GcoreResponse.self, using: decoder)
                .catchErrorJustReturn(GcoreResponse(owner: false,
                                                    userId: -1,
                                                    firstName: "",
                                                    lastName: "",
                                                    avatarUrl: nil,
                                                    pushUrl: nil,
                                                    token: nil,
                                                    gcoreHlsUrl: nil,
                                                    gcoreDashUrl: nil,
                                                    specialistInfo: nil))
        } else {
            return provider.rx
                .request(.createToken(lessonId: lessonId))
                .map(GcoreResponse.self, using: decoder)
        }
    }

    func finishLesson(lessonId: Int) -> Single<Int?> {
        provider
            .rx
            .request(.finishLesson(lessonId: lessonId))
            .map { $0.response?.statusCode }
    }
}
