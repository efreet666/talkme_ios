//
//  AppVersionTarget.swift
//  talkme_ios
//
//  Created by andy on 18.07.2022.
//

import Moya

enum AppVersionTarget {
    case versionCheck(currentVersion: String)
}

extension AppVersionTarget: TargetType {

    var baseURL: URL { Configuration.baseUrl }

    var path: String {
        switch self {
        case .versionCheck: return "/application/version_check/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .versionCheck: return .get
        }
    }

    var sampleData: Data { Data() }

    var task: Task {
        switch self {
        case .versionCheck(let currentVersion):
            return .requestParameters(
                parameters: ["operating_system": "IOS", "version": currentVersion],
                encoding: URLEncoding.queryString
            )
        }
    }

    var headers: [String: String]? {
        switch self {
        case .versionCheck: return ["Content-Type": "application/json"]
        }
    }

    private static let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }()
}
