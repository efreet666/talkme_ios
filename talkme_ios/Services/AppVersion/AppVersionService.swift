//
//  AppVersionService.swift
//  talkme_ios
//
//  Created by andy on 18.07.2022.
//

import Moya
import RxSwift

protocol AppVersionServiceProtocol {
    func checkUpdate()
}

final class AppVersionService: AppVersionServiceProtocol {

    // MARK: - Private Properties

    private let appUrlString = "itms-apps://itunes.apple.com/app/id1560817329"
    private let changeInterval: TimeInterval = 60 * 60 * 72
    private let isLoggingEnabled = false
    private let bag = DisposeBag()
    private lazy var provider = MoyaProvider<AppVersionTarget>(plugins: plugins)

    private lazy var plugins: [NetworkLoggerPlugin] = {
        isLoggingEnabled
            ? [NetworkLoggerPlugin(configuration: .init( logOptions: [.verbose, .formatRequestAscURL]))]
            : []
    }()

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    private lazy var loadingViewController: BlurLoadingViewController = {
        let vc = BlurLoadingViewController()
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }()

    // MARK: - Public Methods

    func checkUpdate() {
        let lastVersionCheckDate = UserDefaultsHelper.shared.lastVersionCheckDate
        guard
            lastVersionCheckDate == nil
            || Date() > lastVersionCheckDate!.addingTimeInterval(changeInterval)
        else { return }

        loadingViewController.show()

        checkVersion(currentVersion: Bundle.main.versionReleaseNumber)
            .subscribe { [weak self] result in
                guard let self = self else { return }

                switch result {
                case .success(let data):
                    guard data.update else {
                        self.updateLastVersionCheckDate()
                        self.loadingViewController.hide()
                        return
                    }
                    self.showUpdateAlert(required: data.required)
                case .error(let error):
                    print("[AppVersion] error: \(error)")
                    self.loadingViewController.hide()
                }
            }.disposed(by: bag)
    }

    // MARK: - Private Methods

    private func showUpdateAlert(required: Bool) {
        let alert = UIAlertController(
            title: "app_version_alert_title".localized,
            message: nil,
            preferredStyle: .alert
        )

        if !required {
            alert.addAction(UIAlertAction(title: "app_version_alert_cancel_title".localized, style: .default) { [weak self] _ in
                guard let self = self else { return }

                self.updateLastVersionCheckDate()
                self.loadingViewController.hide()
            })
        }

        let updateAction = UIAlertAction(title: "app_version_alert_update_title".localized, style: .default) { _ in
            guard
                let url = URL(string: self.appUrlString),
                UIApplication.shared.canOpenURL(url)
            else {
                self.showUpdateAlert(required: required)
                return
            }

            UIApplication.shared.open(url)
        }

        alert.addAction(updateAction)
        alert.preferredAction = updateAction

        AlertControllerHelper.present(alert)
    }

    private func checkVersion(currentVersion: String) -> Single<AppVersionCheckUpdateResponse> {
        provider.rx.request(.versionCheck(currentVersion: currentVersion))
            .map(AppVersionCheckUpdateResponse.self, using: decoder)
    }

    private func updateLastVersionCheckDate() {
        UserDefaultsHelper.shared.lastVersionCheckDate = Date()
    }
}
