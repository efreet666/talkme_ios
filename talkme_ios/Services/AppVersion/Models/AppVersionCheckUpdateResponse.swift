//
//  AppVersionCheckUpdateResponse.swift
//  talkme_ios
//
//  Created by andy on 18.07.2022.
//

struct AppVersionCheckUpdateResponse: Decodable {
    let update: Bool
    let required: Bool
}
