//
//  StreamsFeedViewController.swift
//  talkme_ios
//
//  Created by Elina Efremova on 06.01.2021.
//

import Foundation
import Photos
import RxSwift

final class PermissionsHelper {

    enum PhotosStatus {
        case authorized
        case denied
        case becomeAuthorized
        case becomeDenied
    }

    static var isPhotosAuthorized: Bool {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        #if swift(>=5.3) // iOS 12 support
        case .limited:
            return true
        #endif
        case .authorized:
            return true
        case .denied, .restricted, .notDetermined:
            return false
        @unknown default:
            assertionFailure("unhandled case in PHAuthorizationStatus")
            return false
        }
    }

    static var isPhotosRequested: Bool {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        #if swift(>=5.3) // iOS 12 support
        case .limited:
            return true
        #endif
        case .authorized, .denied, .restricted:
            return true
        case .notDetermined:
            return false
        @unknown default:
            assertionFailure("unhandled case in PHAuthorizationStatus")
            return false
        }
    }

    static func requestPhotosAuthorization(_ handler: ((Bool) -> Void)?) {
        PHPhotoLibrary.requestAuthorization { _ in
            DispatchQueue.main.async {
                handler?(isPhotosAuthorized)
            }
        }
    }

    static func checkPhotos(_ completion: @escaping (PhotosStatus) -> Void) {
        if PermissionsHelper.isPhotosRequested {
            if PermissionsHelper.isPhotosAuthorized {
                completion(.authorized)
            } else {
                completion(.denied)
            }
        } else {
            PermissionsHelper.requestPhotosAuthorization { isGranted in
                isGranted ? completion(.becomeAuthorized) : completion(.becomeDenied)
            }
        }
    }

    static func checkCameraPermissions() -> Single<Bool> {
        Single<Bool>.create { single -> Disposable in
            CameraService.checkCameraAccess { success, error in
                guard error != nil else {
                    single(.success(true))
                    return
                }
                single(.success(false))
            }
            return Disposables.create()
        }
    }

    static func checkMicrophonePermissions() -> Single<Bool> {
        Single<Bool>.create { single -> Disposable in
            CameraService.checkMicrophoneAccess { success, error in
                guard error != nil else {
                    single(.success(true))
                    return
                }
                single(.success(false))
            }
            return Disposables.create()
        }
    }
}
