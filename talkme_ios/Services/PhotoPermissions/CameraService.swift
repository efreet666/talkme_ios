//
//  StreamsFeedViewController.swift
//  talkme_ios
//
//  Created by Elina Efremova on 06.01.2021.
//

import UIKit
import AVFoundation

// MARK: - PermissionError

enum PermissionError {
    case cameraDenied
    case photosDenied
    case microphoneDenied
    case cameraMicrophoneDenied

    var localized: String {
        switch self {
        case .cameraDenied:
            return "error_camera_no_access".localized
        case .photosDenied:
            return "general_no_access".localized
        case .microphoneDenied:
            return "general_no_microphone_access".localized
        case .cameraMicrophoneDenied:
            return "general_no_camera_microphone_access".localized
        }
    }
}

// MARK: - CameraService

final class CameraService {
    static func checkCameraAccess(_ completion: @escaping (Bool, PermissionError?) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            completion(false, .cameraDenied)
        case .restricted:
            completion(false, nil)
        case .authorized:
            completion(true, nil)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                completion(success, nil)
            }
        @unknown default:
            //            Switch covers known cases, but 'AVAuthorizationStatus' may have additional unknown values, possibly added in future versions
            assertionFailure("unhandled case in AVAuthorizationStatus")
        }
    }

    static func checkMicrophoneAccess(_ completion: @escaping (Bool, PermissionError?) -> Void) {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .granted:
            completion(true, nil)
        case .denied:
            completion(false, .microphoneDenied)
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ success in
                if success {
                    completion(true, nil)
                } else {
                    completion(false, nil)
                }
            })
        @unknown default:
//            Switch covers known cases, but 'AVAudioSession.RecordPermission' may have additional unknown values, possibly added in future versions
            assertionFailure("unhandled case in AVAudioSession.RecordPermission")
        }
    }

}
