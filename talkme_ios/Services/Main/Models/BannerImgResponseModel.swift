//
//  BannerImgResponseModel.swift
//  talkme_ios
//
//  Created by VladimirCH on 24.03.2022.
//

struct BannerList: Decodable {
    var id: Int
    var bannerImg: String
    var bannerImg440: String
    var priority: Int
    var advertiserLink: String?
}
