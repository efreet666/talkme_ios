//
//  CurrentTimeResponse.swift
//  talkme_ios
//
//  Created by nikita on 26.05.2022.
//

struct CurrentTimeResponse: Decodable {
    let currentTime: Double?
}
