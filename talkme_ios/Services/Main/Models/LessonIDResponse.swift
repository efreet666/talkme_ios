//
//  LessonIDResponse.swift
//  talkme_ios
//
//  Created by Yura Fomin on 10.02.2021.
//

struct LessonIDResponse: Decodable {
    let list: List?
    let category: CategoryLesson?
}

struct List: Decodable {
    let id: Int?
    let date: String?
    let subCategory: SubCategory?
    let tileImage: String?
    let name: String?
    let description: String?
    let vip: Bool?
    let lessonTime: String?
    let cost: Int?
    let numberOfParticipants: Int?
    let unautherizationSubscribers: Int?
}

struct SubCategory: Decodable {
    let name: String?
    let slug: String?
    let id: Int?
    let parent: Int?
}

struct CategoryLesson: Decodable {
    let name: String?
}
