//
//  CreateLesson.swift
//  talkme_ios
//
//  Created by 1111 on 31.01.2021.
//
import Moya
import UIKit

struct CreateLessonRequest: Encodable {
    let name: String?
    let category: String?
    let parentCategory: Int?
    let tileImage: Data?
    let description: String?
    let date: String?
    let cost: Int?
    let lessonTime: String?
    let numberOfParticipants: Int?
    let vip: Bool?

    init(streamData: StreamData?) {
        self.name = streamData?.lessonName
        self.category = streamData?.subCategory
        self.parentCategory = streamData?.category
        self.description = streamData?.lessonDescription
        self.cost = streamData?.cost
        self.numberOfParticipants = streamData?.subscriptions
        self.lessonTime = (streamData?.lessonDuration ?? "") + ":0"
        self.date = streamData?.lessonStartTime
        self.tileImage = streamData?.lessonImage?.jpegData(compressionQuality: 0.3)
        self.vip = false
    }

    enum CodingKeys: String, CodingKey {
        case name
        case category
        case parentCategory = "parent_category"
        case tileImage = "tile_image"
        case description
        case date
        case cost
        case lessonTime = "lesson_time"
        case numberOfParticipants = "number_of_participants"
        case vip
    }

    func multipartData() -> [MultipartFormData] {
        var multipartData = [MultipartFormData]()

        if let name = data(from: name) {
            multipartData.append(MultipartFormData(provider: .data(name), name: CodingKeys.name.rawValue))
        }

        if let category = data(from: category) {
            multipartData.append(MultipartFormData(provider: .data(category), name: CodingKeys.category.rawValue))
        }

        if let category = data(from: category) {
            multipartData.append(MultipartFormData(provider: .data(category), name: CodingKeys.category.rawValue))
        }

        if let parentCategory = data(from: String(parentCategory ?? 0)) {
            multipartData.append(MultipartFormData(provider: .data(parentCategory), name: CodingKeys.parentCategory.rawValue))
        }

        if let image = tileImage {
            multipartData.append(MultipartFormData(provider: .data(image), name: "tile_image", fileName: "photo.jpg", mimeType: "image/jpeg"))
        }

        if let description = data(from: description) {
            multipartData.append(MultipartFormData(provider: .data(description), name: CodingKeys.description.rawValue))
        }

        if let date = data(from: date) {
            multipartData.append(MultipartFormData(provider: .data(date), name: CodingKeys.date.rawValue))
        }

        if let cost = data(from: String(cost ?? 0)) {
            multipartData.append(MultipartFormData(provider: .data(cost), name: CodingKeys.cost.rawValue))
        }

        if let lessonTime = data(from: lessonTime) {
            multipartData.append(MultipartFormData(provider: .data(lessonTime), name: CodingKeys.lessonTime.rawValue))
        }

        if let numberOfParticipants = data(from: String(numberOfParticipants ?? 0)) {
            multipartData.append(MultipartFormData(provider: .data(numberOfParticipants), name: CodingKeys.numberOfParticipants.rawValue))
        }

        if let vip = data(from: String(vip ?? false)) {
            multipartData.append(MultipartFormData(provider: .data(vip), name: CodingKeys.vip.rawValue))
        }

        return multipartData
    }

    private func data(from value: String?) -> Data? {
        guard !(value?.isEmpty ?? true) else { return nil }
        return value?.data(using: .utf8)
    }
}

struct CreateLessonResponse: Decodable {
    let description: [String]?
}

struct UpdateLessonResponses: Decodable {
    let description: String?
}
