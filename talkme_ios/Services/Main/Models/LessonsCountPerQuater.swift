//
//  LessonsCountPerQuatter.swift
//  talkme_ios
//
//  Created by 1111 on 29.01.2021.
//

import Foundation

struct LessonsCountPerQuater: Hashable {
    let date: Date
    let count: Float
}
