//
//  DateIntervalResponse.swift
//  talkme_ios
//
//  Created by 1111 on 27.01.2021.
//

struct DateIntervalResponse: Decodable {
    let countLessons: Int
    let dateInterval: [[String: Int]]

    enum CodingKeys: String, CodingKey {
        case countLessons = "count_lessons"
        case dateInterval = "date_interval"
    }
}
