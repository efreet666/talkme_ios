//
//  MainTarget.swift
//  talkme_ios
//
//  Created by 1111 on 27.01.2021.
//

import Moya

enum MainTarget {
    case dateInterval(date: String, category: String)
    case categories
    case subCategory(String)
    case createLesson(CreateLessonRequest)
    case lessonID(Int)
    case nearestLessons(pageSize: Int)
    case popularLessons(pageSize: Int)
    case updateLesson(CreateLessonRequest, Int)
    case allCategories
    case chatsCreate(searchText: String?)
    case createNewChat(chats: NewChatRequest)
    case bannerImg
    case serverCurrentTime
}

extension MainTarget: TargetType {

    static let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }()

    var baseURL: URL { Configuration.baseUrl }

    var path: String {
        switch self {
        case .dateInterval:
            return "/main/date_interval"
        case .categories:
            return "/main/categories"
        case .subCategory(let parent):
            return "/main/sub_category/\(parent)"
        case .createLesson:
            return "/main/create_lessons/"
        case .lessonID(let id):
            return "/main/lesson/\(id)"
        case .nearestLessons:
            return "/main/nearest_lessons/"
        case .popularLessons:
            return "/main/popular_lessons/"
        case .updateLesson(_, let id):
            return "/main/update_lessons/\(id)"
        case .allCategories:
            return "/lessons/all_categories"
        case .chatsCreate, .createNewChat:
            return "/chats/create/"
        case .bannerImg:
            return "/main/banner_img/"
        case .serverCurrentTime:
            return "/main/get_current_time/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .dateInterval, .categories, .subCategory, .lessonID, .nearestLessons, .popularLessons, .allCategories, .chatsCreate, .bannerImg, .serverCurrentTime:
            return .get
        case .createLesson, .createNewChat:
            return .post
        case .updateLesson:
            return .patch
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {

        switch self {
        case .dateInterval(let date, let category):
            let dateIntervalParams: [String: String] = ["date": date, "category_id": category]
            return .requestParameters(parameters: dateIntervalParams, encoding: URLEncoding.queryString)
        case .categories, .subCategory, .lessonID, .allCategories, .serverCurrentTime:
            return .requestPlain
        case .nearestLessons(let pageSize):
            let params = ["page_size": pageSize]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .popularLessons(let pageSize):
            let params = ["page_size": pageSize]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .createLesson(let request):
            let multipartData = request.multipartData()
            return .uploadMultipart(multipartData)
        case .updateLesson(let request, _):
            let multipartData = request.multipartData()
            return .uploadMultipart(multipartData)
        case .chatsCreate(let searchText):
            guard let searchText = searchText else { return .requestPlain }
            let searchParams: [String: String] = ["search": searchText]
            return .requestParameters(parameters: searchParams, encoding: URLEncoding.queryString)
        case .createNewChat(let request):
            return .requestCustomJSONEncodable(request, encoder: Self.encoder)
        case  .bannerImg:
            return .requestParameters(parameters: ["img_format": "mobile"], encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        let isLoggedfromSocials = UserDefaultsHelper.shared.isLoggedfromSocials
        switch self {
        case .dateInterval,
                .categories,
                .subCategory,
                .lessonID,
                .nearestLessons,
                .popularLessons,
                .allCategories,
                .chatsCreate,
                .createNewChat,
                .bannerImg,
                .serverCurrentTime:
            return [
                "Content-Type": "application/json",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]
        case .createLesson, .updateLesson:
            return [
                "Content-Type": "multipart/form-data",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]

        }
    }
}
