//
//  MainService.swift
//  talkme_ios
//
//  Created by 1111 on 27.01.2021.
//

import Moya
import RxSwift

protocol MainServiceProtocol {
    func dateInterval(date: String, category: String) -> Single<DateIntervalResponse>
    func categories() -> Single<[Category]>
    func subCategory(parent: String) -> Single<[Category]>
    func createLesson(request: CreateLessonRequest) -> Single<CreateLessonResponse>
    func lessonID(id: Int)  -> Single<LessonIDResponse>
    func nearestLessons(pageSize: Int) -> Single<[LiveStream]>
    func updateLesson(request: CreateLessonRequest, id: Int) -> Single<UpdateLessonResponses>
    func popularLessons(pageSize: Int) -> Single<[LiveStream]>
    func allCategories() -> Single<[CategoryResponse]>
    func getBannerImg() -> Single<[BannerList]>
    func chatsCreate(searchText: String?) -> Single<[ChatContactCellModel]>
    func createNewChat(chats: NewChatRequest) -> Single<[String: Int]>
    func serverTime() -> Single<CurrentTimeResponse>
}

final class MainService: MainServiceProtocol {

    // MARK: - Private Properties

    private let isLoggingEnabled = false

    private lazy var plugins: [NetworkLoggerPlugin] = {
        return isLoggingEnabled
            ? [NetworkLoggerPlugin(configuration: .init( logOptions: .verbose))]
            : []
    }()

    private lazy var provider = MoyaProvider<MainTarget>(plugins: plugins)

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    // MARK: - Public Methods

    func dateInterval(date: String, category: String) -> Single<DateIntervalResponse> {
        provider.rx.request(.dateInterval(date: date, category: category))
            .map(DateIntervalResponse.self)
    }

    func categories() -> Single<[Category]> {
        provider.rx.request(.categories)
            .map([Category].self)
    }

    func subCategory(parent: String) -> Single<[Category]> {
        provider.rx.request(.subCategory(parent))
            .map([Category].self)
    }

    func createLesson(request: CreateLessonRequest) -> Single<CreateLessonResponse> {
        provider.rx.request(.createLesson(request))
            .map(CreateLessonResponse.self)
            .catchErrorJustReturn(.init(description: nil))
    }

    func lessonID(id: Int) -> Single<LessonIDResponse> {
        provider.rx.request(.lessonID(id))
            .map(LessonIDResponse.self, using: decoder)
    }

    func nearestLessons(pageSize: Int) -> Single<[LiveStream]> {
        provider.rx.request(.nearestLessons(pageSize: pageSize))
            .map([LiveStream].self, using: decoder)
    }

    func popularLessons(pageSize: Int) -> Single<[LiveStream]> {
        provider.rx.request(.popularLessons(pageSize: pageSize))
            .map([LiveStream].self, using: decoder)
    }

    func updateLesson(request: CreateLessonRequest, id: Int) -> Single<UpdateLessonResponses> {
        provider.rx.request(.updateLesson(request, id))
            .map(UpdateLessonResponses.self)
    }

    func allCategories() -> Single<[CategoryResponse]> {
        provider.rx.request(.allCategories)
            .map([CategoryResponse].self, using: decoder)
    }

    func getBannerImg() -> Single<[BannerList]> {
        provider.rx.request(.bannerImg)
            .map([BannerList].self, using: decoder)
    }

    func chatsCreate(searchText: String?) -> Single<[ChatContactCellModel]> {
        provider.rx.request(.chatsCreate(searchText: searchText))
            .map([ChatContactCellModel].self, using: decoder)
    }

    func createNewChat(chats: NewChatRequest) -> Single<[String: Int]> {
        provider.rx.request(.createNewChat(chats: chats))
            .map([String: Int].self, using: decoder)
    }

    func serverTime() -> Single<CurrentTimeResponse> {
        provider.rx.request(.serverCurrentTime)
            .map(CurrentTimeResponse.self, using: decoder)
    }
}
