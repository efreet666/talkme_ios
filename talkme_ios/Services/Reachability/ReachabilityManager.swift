//
//  ReachabilityManager.swift
//  talkme_ios
//
//  Created by Den on 03.01.2021.
//

import Reachability
import RxSwift

final class ReachabilityManager {

    // MARK: - Public Properties

    let hasNetworkConnection = BehaviorSubject<Bool>(value: true)

    // MARK: - Private Properties

    private var reachability: Reachability?
    private let reachabilityDebouncer = PublishSubject<Bool>()
    private var disposeBag = DisposeBag()

    // MARK: - Public Methods

    init() {
        start()
    }

    deinit {
        stop()
    }

    private func start() {
        DispatchQueue.main.async {
            print("start")
            guard self.reachability == nil else { return }
            self.reachability = try? Reachability()
            self.disposeBag = DisposeBag()
            self.reachability?.whenReachable = { [weak self] _ in
                print("whenReachable")
                self?.reachabilityDebouncer.onNext(true)
            }
            self.reachability?.whenUnreachable = { [weak self] _ in
                print("whenUnreachable")
                self?.reachabilityDebouncer.onNext(false)
            }

            self.reachabilityDebouncer
                .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
                .distinctUntilChanged()
                .bind(to: self.hasNetworkConnection)
                .disposed(by: self.disposeBag)

            do {
                try self.reachability?.startNotifier()
            } catch {
                print("Unable to start notifier")
            }
        }
    }

    private func stop() {
        print("stop")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
        disposeBag = DisposeBag()
        reachability = nil
    }
}
