//
//  LessonsService.swift
//  talkme_ios
//
//  Created by Yura Fomin on 27.01.2021.
//

import Moya
import RxSwift

protocol LessonsServiceProtocol {
    func putLike(id: Int) -> Single<LikeModel>
    func deleteLike(id: Int) -> Single<LikeModel>
    func myBroadcasts(page: Int) -> Single<MyBroadcastsResponse>
    func live(pageNumber: Int, lessonsInPage: Int) -> Single<LiveResponse>
    func mySubscriptions(page: Int) -> Single<MySubscriptionsResponse>
    func searchSpecialists(searchText: String) -> Single<SearchMyLessonResponse>
    func cancelLesson(id: Int) -> Single<CancelLessonResponse>
    func lessonUnsubscribe(id: Int) -> Single<CancelLessonResponse>
    func lessonDetail(id: Int, saved: Bool) -> Single<LessonDetailResponse>
    func mainLesson(id: Int) -> Single<MainLessonResponse>
    func budget() -> Single<BudgetResponse>
    func lessonSubscribe(id: Int, lessonIsSaved: Bool) -> Single<Response>
    func exactCategory(request: ExactCategoryRequest) -> Single<ExactCategoryResponse>
    func tips(tips: Int, lessonId: Int, lessonIsSaved: Bool) -> Single<Int?>
    func leaveRating(request: LeaveRatingRequest) -> Single<LeaveRatingResponse>
    func saveStream(lessonId: Int) -> Single<SaveStreamResponse>
    func savedStreams(page: Int, count: Int) -> Single<LiveResponse>
    func savedStreams(withOwnersId id: Int?, page: Int, count: Int) -> Single<LiveResponse>
    func savedStreamsOnlyOwners(page: Int, count: Int) -> Single<LiveResponse>
    func deleteLesson(withId lessonId: Int) -> Single<Response>
}

final class LessonsService: LessonsServiceProtocol {

    // MARK: - Private Properties

    private let isLoggingEnabled = false
    private let serviceIsStubbed = Consts.liveStreamsStubbed

    private lazy var plugins: [NetworkLoggerPlugin] = {
        return isLoggingEnabled
            ? [NetworkLoggerPlugin(configuration: .init( logOptions: [.verbose, .formatRequestAscURL]))]
            : []
    }()

    private lazy var provider = MoyaProvider<LessonsTarget>(plugins: plugins)
    private lazy var mockedProvider = MoyaProvider<LessonsTarget>(stubClosure: MoyaProvider.immediatelyStub)

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    // MARK: - Public Methods
    
    func putLike(id: Int) -> Single<LikeModel> {
        provider.rx.request(.putLike(id))
            .map(LikeModel.self, using: decoder)
    }

    func deleteLike(id: Int) -> Single<LikeModel> {
        provider.rx.request(.deleteLike(id))
            .map(LikeModel.self, using: decoder)
    }

    func myBroadcasts(page: Int) -> Single<MyBroadcastsResponse> {
        provider.rx.request(.myBroadCasts(page))
            .map(MyBroadcastsResponse.self, using: decoder)
    }

    func live(pageNumber: Int, lessonsInPage: Int) -> Single<LiveResponse> {
        if serviceIsStubbed {
            let mockedOobservable = mockedProvider.rx.request(.live(pageNumber: pageNumber, pageSize: lessonsInPage))
                .map(LiveResponse.self, using: decoder)
                .catchErrorJustReturn(.init(data: nil, msg: nil)).asObservable()
            let normalObservable = provider.rx.request(.live(pageNumber: pageNumber, pageSize: lessonsInPage))
                .map(LiveResponse.self, using: decoder)
                .catchErrorJustReturn(.init(data: nil, msg: nil)).asObservable()
            return Observable
                .zip(mockedOobservable, normalObservable)
                . map { LiveResponse(data: ($0.data?.shuffled() ?? []) + ($1.data ?? []), msg: nil) }
                .asSingle()
        } else {
            return provider.rx.request(.live(pageNumber: pageNumber, pageSize: lessonsInPage))
                .map(LiveResponse.self, using: decoder)
                .catchErrorJustReturn(.init(data: nil, msg: nil))
        }
    }

    func mySubscriptions(page: Int) -> Single<MySubscriptionsResponse> {
        provider.rx.request(.mySubscriptions(page))
            .map(MySubscriptionsResponse.self, using: decoder)
    }

    func searchSpecialists(searchText: String) -> Single<SearchMyLessonResponse> {
        provider.rx.request(.searchSpecialists(searchText))
            .map(SearchMyLessonResponse.self)
    }

    func cancelLesson(id: Int) -> Single<CancelLessonResponse> {
        provider.rx.request(.cancelLesson(id))
            .map(CancelLessonResponse.self)
            .catchErrorJustReturn(.init(id: nil, msg: nil))
    }

    func lessonUnsubscribe(id: Int) -> Single<CancelLessonResponse> {
        provider.rx.request(.lessonUnsubscribe(id))
            .map(CancelLessonResponse.self)
            .catchErrorJustReturn(.init(id: nil, msg: nil))
    }

    func lessonDetail(id: Int, saved: Bool) -> Single<LessonDetailResponse> {
        if serviceIsStubbed,
           let lessonDetails = try? decoder.decode([LessonDetailResponse].self, from: Data.fromJson(fileName: "LessonDetailStubs")),
           lessonDetails.map({ $0.id }).contains(id) {

            return mockedProvider.rx.request(.lessonDetail(id, saved))
                .map([LessonDetailResponse].self, using: decoder)
                .map { lessonsDetails -> LessonDetailResponse in
                    let correctDetails = lessonsDetails.filter { $0.id == id }
                    return correctDetails.first ?? LessonDetailResponse(id: 0,
                                                                        name: "",
                                                                        date: 0,
                                                                        tileImage: "",
                                                                        lessonTime: "",
                                                                        numberOfParticipants: 0,
                                                                        owner: Owner(firstName: nil,
                                                                                     lastName: nil,
                                                                                     id: nil,
                                                                                     userName: nil,
                                                                                     avatarUrl: nil,
                                                                                     avatarUrlOriginal: nil,
                                                                                     numberClass: nil),
                                                                        categoryName: "",
                                                                        description: "",
                                                                        vip: false,
                                                                        isOwner: false,
                                                                        isSubscribe: false,
                                                                        isStarted: false,
                                                                        streamId: "",
                                                                        similarLesson: [],
                                                                        cost: 0,
                                                                        gcoreHlsUrl: "",
                                                                        subCategory: SubCategorysInfo(parent: 0), liked: false,
                                                                        usersLiked: []
                    )
                }
        } else {
            return provider.rx.request(.lessonDetail(id, saved)).map(LessonDetailResponse.self, using: decoder)
        }
    }

    func mainLesson(id: Int) -> Single<MainLessonResponse> {
        provider.rx.request(.mainLesson(id)).map(MainLessonResponse.self, using: decoder)
    }

    func budget() -> Single<BudgetResponse> {
        provider.rx.request(.budget).map(BudgetResponse.self)
    }

    func lessonSubscribe(id: Int, lessonIsSaved: Bool) -> Single<Response> {
        provider.rx.request(.lessonSubscribe(id, lessonIsSaved))
    }

    func exactCategory(request: ExactCategoryRequest) -> Single<ExactCategoryResponse> {
        provider.rx
            .request(.exactCategories(request: request))
            .map(ExactCategoryResponse.self, using: decoder)
    }

    func tips(tips: Int, lessonId: Int, lessonIsSaved: Bool) -> Single<Int?> {
        provider.rx.request(.tips(tips: tips, lessonId: lessonId, lessonIsSaved: lessonIsSaved))
            .map { $0.response?.statusCode }
    }

    func leaveRating(request: LeaveRatingRequest) -> Single<LeaveRatingResponse> {
        provider.rx.request(.leaveRating(request))
            .map(LeaveRatingResponse.self)
    }

    func saveStream(lessonId: Int) -> Single<SaveStreamResponse> {
        provider.rx.request(.saveStream(lessonId))
            .map(SaveStreamResponse.self, using: decoder)
            .catchErrorJustReturn(.init(saveStream: nil, remainingVideoSlots: nil))
    }

    func savedStreams(page: Int, count: Int) -> Single<LiveResponse> {
        provider.rx.request(.savedStreams(pageNumber: page, pageSize: count))
            .map(LiveResponse.self, using: decoder)
            .catchErrorJustReturn(.init(data: nil, msg: nil))
    }

    func savedStreams(withOwnersId id: Int? = nil, page: Int, count: Int) -> Single<LiveResponse> {
        if let streamsQwnerId = id {
            return provider.rx.request(.savedStreamsByOwner(id: streamsQwnerId, pageNumber: page, pageSize: count))
                .map(LiveResponse.self, using: decoder)
                .catchErrorJustReturn(.init(data: nil, msg: nil))
        } else {
            return provider.rx.request(.savedStreams(pageNumber: page, pageSize: count))
                .map(LiveResponse.self, using: decoder)
                .catchErrorJustReturn(.init(data: nil, msg: nil))
        }
    }

    func savedStreamsOnlyOwners(page: Int, count: Int) -> Single<LiveResponse> {
        provider.rx.request(.savedSteramsOnlyForOwner(pageNumber: page, pageSize: count))
            .map(LiveResponse.self, using: decoder)
            .catchErrorJustReturn(.init(data: nil, msg: nil))
    }

    func deleteLesson(withId lessonId: Int) -> Single<Response> {
        provider.rx.request(.deleteSavedStream(lessonId: lessonId))
    }
}
