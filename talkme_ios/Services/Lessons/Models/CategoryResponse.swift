//
//  CategoryResponse.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 03.03.2021.
//

struct CategoryResponse: Decodable {
    let id: Int
    let name: String
}
