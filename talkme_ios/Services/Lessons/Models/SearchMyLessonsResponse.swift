//
//  SearchMyLessonsResponse.swift
//  talkme_ios
//
//  Created by 1111 on 07.02.2021.
//

struct SearchMyLessonResponse: Decodable {
    let user: [ProfileResponse]?
}
