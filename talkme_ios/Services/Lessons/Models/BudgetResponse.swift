//
//  BudgetResponse.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 16.02.2021.
//

struct BudgetResponse: Decodable {
    let budget: Int
}

struct TipsResponse: Decodable {
    let tips: Int?
    let lesson: Int?
}

struct LeaveRatingRequest: Encodable {
    let lessonId: Int?
    let rating: Int?

    enum CodingKeys: String, CodingKey {
        case lessonId = "lesson_id"
        case rating = "rating"
    }
}

struct LeaveRatingResponse: Decodable {
    let detail: String?
}
