//
//  ExactCategoryResponse.swift
//  talkme_ios
//
//  Created by 1111 on 22.02.2021.
//

struct ExactCategoryResponse: Decodable {
    let numOfObjects: Int
    let data: [LiveStream]
    let subCategories: [Category]
    let categoryName: String
}
