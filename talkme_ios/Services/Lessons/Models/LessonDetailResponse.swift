//
//  LessonDetailResponse.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 15.02.2021.
//


struct LessonDetailResponse: Codable, Hashable {
    let id: Int
    let name: String
    let date: Double
    let tileImage: String?
    let lessonTime: String
    let numberOfParticipants: Int
    let owner: Owner
    let categoryName: String
    let description: String
    let vip: Bool
    let isOwner: Bool
    var isSubscribe: Bool
    let isStarted: Bool
    let streamId: String
    let similarLesson: [LiveStream]
    let cost: Int
    let gcoreHlsUrl: String?
    let subCategory: SubCategorysInfo
    var liked: Bool?
    var usersLiked: [Int]
}

struct SubCategorysInfo: Codable, Hashable {
    let parent: Int
}
