//
//  MyBroadcastsResponse.swift
//  talkme_ios
//
//  Created by Yura Fomin on 27.01.2021.
//

struct MyBroadcastsResponse: Decodable {
    let lessons: [Lesson]?
    let lessonsSoon: [Lesson]?
}

struct Lesson: Decodable {
    let id: Int
    let name: String
    let description: String?
    let vip: Bool?
    let tileImage: String?
    let category: String?
    let date: Double?
    let lessonTime: String?
    let cost: Int?
    let slug: String?
    let subscribers: Int?
    let isFinish: Bool?
}

struct CancelLessonResponse: Decodable {
    let id: Int?
    let msg: String?
}
