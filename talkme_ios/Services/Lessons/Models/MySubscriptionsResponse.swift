//
//  MySubscriptionsResponse.swift
//  talkme_ios
//
//  Created by Yura Fomin on 31.01.2021.
//

struct MySubscriptionsResponse: Decodable {
    let live: [MySubscriptions]?
    let data: [MySubscriptions]?
}

struct MySubscriptions: Decodable {
    let name: String
    let description: String?
    let vip: Bool?
    let tileImage: String?
    let category: Int
    let date: Double?
    let lessonTime: String?
    let cost: Int?
    let owner: String?
    let slug: String?
    let id: Int
    let isFinish: Bool?
}
