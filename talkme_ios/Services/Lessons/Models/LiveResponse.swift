//
//  LiveResponse.swift
//  talkme_ios
//
//  Created by Yura Fomin on 28.01.2021.
//

struct LiveResponse: Decodable {
    let data: [LiveStream]?
    let msg: String?
}

struct LiveStreamShort {
    let id: Int
    let ownerNumberClass: String?
    let cost: Int?

    init(model: LiveStream) {
        id = model.id
        ownerNumberClass = model.owner.numberClass
        cost = model.cost
    }
}

struct LiveStream: Codable, Hashable {
    let id: Int
    let date: Double
    let category: Category?
    let tileImage: String?
    let owner: Owner
    let categoryName: String
    let categoryIco: String
    let placesLeft: Int
    let name: String
    let description: String
    let slug: String
    let vip: Bool?
    let lessonTime: String
    let cost: Int?
    let status: String?
    let numberOfParticipants: Int
    let isFinish: Bool
    let isStarted: Bool
    let streamId: String?
    let unautherizationNumber: Int?
    let unautherizationSubscribers: Int?
    let savedStream: Bool?
    let gcoreHlsUrl: String?

    init(from savedStream: SavedStream) {
        self.id = savedStream.id
        self.date = savedStream.date
        self.category = savedStream.category
        self.tileImage = savedStream.tileImage
        self.owner = savedStream.owner
        self.categoryName = savedStream.categoryName
        self.categoryIco = savedStream.categoryIco
        self.placesLeft = savedStream.placesLeft
        self.name = savedStream.name
        self.description = savedStream.description
        self.slug = savedStream.slug
        self.vip = savedStream.vip
        self.lessonTime = savedStream.lessonTime
        self.cost = savedStream.cost
        self.status = savedStream.status
        self.numberOfParticipants = savedStream.numberOfParticipants
        self.isFinish = savedStream.isFinish
        self.isStarted = savedStream.isStarted
        self.streamId = savedStream.streamId
        self.unautherizationNumber = savedStream.unautherizationNumber
        self.unautherizationSubscribers = savedStream.unautherizationSubscribers
        self.savedStream = savedStream.savedStream
        self.gcoreHlsUrl = savedStream.gcoreHlsUrl
    }
}

struct Category: Codable, Hashable {
    let name: String?
    let slug: String?
    let id: Int
    let parent: Int?
}

struct Owner: Codable, Hashable {
    let firstName: String?
    let lastName: String?
    let id: Int?
    let userName: String?
    let avatarUrl: String?
    let avatarUrlOriginal: String?
    let numberClass: String?
}
