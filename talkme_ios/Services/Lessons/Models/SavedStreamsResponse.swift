//
//  SavedStreamsResponse.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 10/7/2022.
//

import Foundation

struct SavedStreamsResponse: Decodable {
    let data: [SavedStream]?
    let msg: String?
}

struct SavedStream: Decodable, Hashable {
    let id: Int
    let date: Double
    let category: Category?
    let tileImage: String?
    let owner: Owner
    let categoryName: String
    let categoryIco: String
    let placesLeft: Int
    let name: String
    let description: String
    let slug: String
    let vip: Bool?
    let lessonTime: String
    let cost: Int?
    let status: String?
    let numberOfParticipants: Int
    let isFinish: Bool
    let isStarted: Bool
    let streamId: String?
    let unautherizationNumber: Int?
    let unautherizationSubscribers: Int?
    let savedStream: Bool?
    let gcoreHlsUrl: String?
}

struct SavedStreamShort {
    let id: Int
    let ownerNumberClass: String?
    let cost: Int?

    init(model: SavedStream) {
        id = model.id
        ownerNumberClass = model.owner.numberClass
        cost = model.cost
    }
}
