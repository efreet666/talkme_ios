//
//  SaveStreamResponseModel.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 6/6/2022.
//

import Foundation

struct SaveStreamResponse: Codable {
    let saveStream: Int?
    let remainingVideoSlots: String?

    enum CodingKeys: String, CodingKey {
        case saveStream = "save_stream"
        case remainingVideoSlots = "remaining_video_slots"
    }
}
