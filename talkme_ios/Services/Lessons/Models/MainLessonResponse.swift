//
//  MainLessonResponse.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 15.02.2021.
//

struct MainLessonResponse: Decodable {
    let category: Category
}
