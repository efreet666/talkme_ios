//
//  ExactCategoryRequest.swift
//  talkme_ios
//
//  Created by 1111 on 22.02.2021.
//

struct ExactCategoryRequest: Encodable {
    var categoryId: String
    var costFrom: Int?
    var costTo: Int?
    var dateStart: Int?
    var dateEnd: Int?
    var free: Bool?
    var gender: String?
    var subCategory: [Int]
    var timeEnd: String?
    var timeStart: String?
    var page: Int?
    var pageSize: Int?
}
