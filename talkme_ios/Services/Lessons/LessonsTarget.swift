//
//  LessonsTarget.swift
//  talkme_ios
//
//  Created by Yura Fomin on 27.01.2021.
//

import Moya
import Foundation

enum LessonsTarget {
    case putLike(Int)
    case deleteLike(Int)
    case myBroadCasts(Int)
    case live(pageNumber: Int, pageSize: Int)
    case mySubscriptions(Int)
    case searchSpecialists(String)
    case cancelLesson(Int)
    case lessonUnsubscribe(Int)
    case lessonDetail(Int, Bool)
    case mainLesson(Int)
    case lessonSubscribe(Int, Bool)
    case budget
    case exactCategories(request: ExactCategoryRequest)
    case tips(tips: Int, lessonId: Int, lessonIsSaved: Bool)
    case leaveRating(LeaveRatingRequest)
    case saveStream(Int)
    case savedStreams(pageNumber: Int, pageSize: Int)
    case savedStreamsByOwner(id: Int, pageNumber: Int, pageSize: Int)
    case savedSteramsOnlyForOwner(pageNumber: Int, pageSize: Int)
    case deleteSavedStream(lessonId: Int)
}

extension LessonsTarget: TargetType {

    private var encoder: JSONEncoder {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }

    var baseURL: URL {
        return Configuration.baseUrl
    }

    var path: String {
        switch self {
        case .myBroadCasts:
            return "/lessons/my_broadcasts/"
        case .live:
            return "/main/live/"
        case .mySubscriptions:
            return "/users/my_lesson/"
        case .searchSpecialists:
            return "/lessons/search_my_lesson/"
        case .cancelLesson:
            return "/lessons/cancel_lesson/"
        case .lessonUnsubscribe:
            return "/lessons/lesson_unsubscribe/"
        case .lessonDetail:
            return "/lessons/lesson_detail"
        case .mainLesson(let id):
            return "/main/lesson/\(id)"
        case .budget:
            return "/payments/budget"
        case .lessonSubscribe:
            return "/lessons/lesson_subscribe"
        case .exactCategories:
            return "/lessons/exact_category/"
        case .tips:
            return "/payments/tips/"
        case .leaveRating:
            return "/users/leave_rating/"
        case .saveStream(let id):
            return "main/save_stream/\(id)/"
        case .savedStreams, .savedStreamsByOwner, .savedSteramsOnlyForOwner:
            return "main/saved_streams/"
        case .deleteSavedStream(let lessonId):
            return "/main/saved_streams/\(lessonId)/"
        case .putLike(let id):
            return "/main/lesson_like/\(id)/"
        case .deleteLike(let id):
            return "/main/lesson_like/\(id)/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .myBroadCasts,
             .live,
             .mySubscriptions,
             .cancelLesson,
             .lessonUnsubscribe,
             .searchSpecialists,
             .lessonDetail,
             .mainLesson,
             .budget,
             .lessonSubscribe,
             .tips,
             .saveStream,
             .savedStreams,
             .savedStreamsByOwner,
             .savedSteramsOnlyForOwner:
            return .get
        case .exactCategories, .leaveRating, .putLike:
            return .post
        case .deleteSavedStream:
            return .delete
        case .deleteLike:
            return .delete
        }
    }

    var sampleData: Data {
        switch self {
        case .live(let pageNumber, _): // (let pageNumber, let pageSize):
            return pageNumber == 1 ? Data.fromJson(fileName: "LiveResponseStub") : Data()
        case .lessonDetail: // (let lessonId, let lessonIsSaved):
            return Data.fromJson(fileName: "LessonDetailStubs")
        default:
            return Data()
        }
    }

    var task: Task {
        switch self {
        case .mainLesson, .budget, .deleteSavedStream, .putLike, .deleteLike:
            return .requestPlain
        case .mySubscriptions(let page):
            let params = ["page_size": page]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .myBroadCasts(let page):
            let params = ["page_size": page]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .searchSpecialists(let searchText):
            let searchMyLessonParams: [String: String] = ["search_class": searchText]
            return .requestParameters(parameters: searchMyLessonParams, encoding: URLEncoding.queryString)
        case .cancelLesson(let id), .lessonUnsubscribe(let id):
            let params = ["id": id]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .lessonSubscribe(let id, let lessonIsSaved):
            var params: [String: Any] = ["id": id]
            params["saved"] = lessonIsSaved ? "true" : "false"
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .lessonDetail(let id, let lessonIsSaved):
            let parameter = lessonIsSaved ? "saved_id" : "id"
            let params = [parameter: id]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .exactCategories(let request):
            return .requestCustomJSONEncodable(request, encoder: encoder)
        case .tips(let tips, let lessonId, let lessonIsSaved):
            let params = ["tips": tips, "lesson": lessonId, "saved": lessonIsSaved ? "true" : "false"] as [String: Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .leaveRating(let request):
            return .requestJSONEncodable(request)
        case .saveStream:
            let params = ["save_stream": true] as [String: Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .live(let pageNumber, let pageSize):
            let params = ["page": pageNumber, "page_size": pageSize] as [String: Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .savedStreams(let pageNumber, let pageSize):
            let params = ["page": pageNumber, "page_size": pageSize] as [String: Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .savedStreamsByOwner(let ownerId, let pageNumber, let pageSize):
            let params = ["owner": ownerId, "page": pageNumber, "page_size": pageSize] as [String: Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .savedSteramsOnlyForOwner(let pageNumber, let pageSize):
            let params = ["only_owner": true, "page": pageNumber, "page_size": pageSize] as [String: Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        let isLoggedfromSocials = UserDefaultsHelper.shared.isLoggedfromSocials
        switch self {
        case .live,
             .mySubscriptions,
             .cancelLesson,
             .lessonUnsubscribe,
             .searchSpecialists,
             .tips,
             .saveStream,
             .savedStreams,
             .savedStreamsByOwner,
             .savedSteramsOnlyForOwner,
             .deleteSavedStream:
            return [
                "Content-Type": "application/json",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]
        case .lessonDetail,
             .mainLesson,
             .budget,
             .lessonSubscribe,
             .exactCategories,
             .leaveRating,
             .myBroadCasts,
             .putLike,
             .deleteLike:
            return [
                "Content-Type": "application/json",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]
        }
    }
}
