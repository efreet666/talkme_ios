//
//  PaymentsTarget.swift
//  talkme_ios
//
//  Created by 1111 on 22.03.2021.
//

import Moya

enum PaymentsTarget {
    case getCoin
    case getGifts
    case getPlan
    case getCameraPlan
    case makeGift(MakeGiftRequest)
    case sendReceipt(SendReceiptRequest)
    case budgetHistory
    case getPaymentsKoef
    case cardTokenizeUrlEcommpay
    case withdrawEcommpay(WithdrawEcommpayPost)
}

extension PaymentsTarget: TargetType {

    static let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }()

    var baseURL: URL { Configuration.baseUrl }

    var path: String {
        switch self {
        case .getCoin:
            return "/payments/get_coin/"
        case .getGifts:
            return "/payments/get_gift/"
        case .getPlan:
            return "/payments/get_plan/"
        case .getCameraPlan:
            return "/payments/get_camera_plan/"
        case .makeGift:
            return "/payments/make_gift/"
        case .sendReceipt:
            return "/payments/in_app_purchase/apple/"
        case .budgetHistory:
            return "/payments/budget_history/"
        case .getPaymentsKoef:
            return "/settings/"
        case .cardTokenizeUrlEcommpay:
            return "/payments/ecommpay/card_tokenize_url/"
        case .withdrawEcommpay:
            return "/payments/ecommpay/withdraw/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .getCoin, .getGifts, .getPlan, .getCameraPlan, .getPaymentsKoef, .budgetHistory, .cardTokenizeUrlEcommpay:
            return .get
        case .makeGift, .sendReceipt, .withdrawEcommpay:
            return .post
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {

        switch self {
        case .getCoin, .getGifts, .getPlan, .getCameraPlan, .getPaymentsKoef, .budgetHistory:
            return .requestPlain
        case .withdrawEcommpay(let request):
            return .requestJSONEncodable(request)
        case .cardTokenizeUrlEcommpay:
            return .requestParameters(parameters: ["platform": "ios"], encoding: URLEncoding.default)
        case .makeGift(let request):
            return .requestJSONEncodable(request)
        case .sendReceipt(let request):
            return .requestJSONEncodable(request)
        }
    }

    var headers: [String: String]? {
        let isLoggedfromSocials = UserDefaultsHelper.shared.isLoggedfromSocials
        switch self {
        case .getCoin, .getGifts, .getPlan, .getCameraPlan, .makeGift, .sendReceipt, .getPaymentsKoef, .budgetHistory, .cardTokenizeUrlEcommpay, .withdrawEcommpay:
            return [
                "Content-Type": "application/json",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]
        }
    }
}
