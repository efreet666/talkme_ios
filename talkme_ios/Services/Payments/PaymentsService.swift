//
//  PaymentsService.swift
//  talkme_ios
//
//  Created by 1111 on 22.03.2021.
//

import Moya
import RxSwift

protocol PaymentsServiceProtocol {
    func getCoin() -> Single<GetCoinResponse>
    func getGifts() -> Single<[GetPlansResponse]>
    func getPlan() -> Single<[GetPlansResponse]>
    func getCameraPlan() -> Single<[GetCameraPlanResponse]>
    func makeGift(request: MakeGiftRequest) -> Single<GetPlansResponse>
    func sendReceipt(request: SendReceiptRequest) -> Single<Int?>
    func budgetHistory() -> Single<[BudgetHistoryResponse]>
    func getPaymentsKoef() -> Single<[PaymentsKoef]>
    func cardTokenizeUrlEcommpay() -> Single<EcommpayJSON>
    func withdrawEcommpay(request: WithdrawEcommpayPost) -> Single<EcommpayMSG>
}

final class PaymentsService: PaymentsServiceProtocol {

    // MARK: - Private Properties

    private let isLoggingEnabled = false

    private lazy var plugins: [NetworkLoggerPlugin] = {
        return isLoggingEnabled
            ? [NetworkLoggerPlugin(configuration: .init( logOptions: .verbose))]
            : []
    }()

    private lazy var provider = MoyaProvider<PaymentsTarget>(plugins: plugins)

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    // MARK: - Public Methods

    func getCoin() -> Single<GetCoinResponse> {
        provider.rx.request(.getCoin)
            .map(GetCoinResponse.self)
    }

    func getGifts() -> Single<[GetPlansResponse]> {
        provider.rx.request(.getGifts)
            .map([GetPlansResponse].self)
    }

    func getPlan() -> Single<[GetPlansResponse]> {
        provider.rx.request(.getPlan)
            .map([GetPlansResponse].self, using: decoder)
    }

    func getCameraPlan() -> Single<[GetCameraPlanResponse]> {
        provider.rx.request(.getCameraPlan)
            .map([GetCameraPlanResponse].self)
    }

    func makeGift(request: MakeGiftRequest) -> Single<GetPlansResponse> {
        provider.rx.request(.makeGift(request))
            .map(GetPlansResponse.self)
    }

    func sendReceipt(request: SendReceiptRequest) -> Single<Int?> {
        provider
            .rx
            .request(.sendReceipt(request))
            .map { $0.response?.statusCode }
    }

    func budgetHistory() -> Single<[BudgetHistoryResponse]> {
        provider.rx.request(.budgetHistory)
            .map([BudgetHistoryResponse].self, using: decoder)
    }

    func getPaymentsKoef() -> Single<[PaymentsKoef]> {
        provider.rx.request(.getPaymentsKoef)
            .map([PaymentsKoef].self)
    }

    func cardTokenizeUrlEcommpay() -> Single<EcommpayJSON> {
        provider.rx.request(.cardTokenizeUrlEcommpay)
            .map(EcommpayJSON.self)
    }

    func withdrawEcommpay(request: WithdrawEcommpayPost) -> Single<EcommpayMSG> {
            provider.rx.request(.withdrawEcommpay(request))
                .map(EcommpayMSG.self)
        }
}
