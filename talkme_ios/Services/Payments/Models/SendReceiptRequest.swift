//
//  SendReceiptRequest.swift
//  talkme_ios
//
//  Created by 1111 on 19.05.2021.
//

struct SendReceiptRequest: Encodable {
    let receipt: String
}
