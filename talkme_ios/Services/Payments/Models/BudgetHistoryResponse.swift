//
//  BudgetHistoryResponse.swift
//  talkme_ios
//
//  Created by Майя Калицева on 09.06.2021.
//

struct BudgetHistoryResponse: Decodable, Equatable {
    let date: Int
    let description: String?
    let cash: Int
    let flag: Bool
}
