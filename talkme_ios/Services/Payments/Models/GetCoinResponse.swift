//
//  GetCoinResponse.swift
//  talkme_ios
//
//  Created by 1111 on 22.03.2021.
//

struct GetCoinResponse: Decodable {
    let coin: Int
}
