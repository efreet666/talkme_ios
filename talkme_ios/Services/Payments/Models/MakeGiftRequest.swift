//
//  MakeGiftRequest.swift
//  talkme_ios
//
//  Created by 1111 on 24.03.2021.
//

struct MakeGiftRequest: Encodable {
    let gift: Int
    let lesson: Int
    let saved: Bool
}
