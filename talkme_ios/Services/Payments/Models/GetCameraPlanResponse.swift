//
//  GetCameraPlanResponse.swift
//  talkme_ios
//
//  Created by 1111 on 23.03.2021.
//

struct GetCameraPlanResponse: Decodable {
    let id: CameraPlanImagesModel
    let image: String
    let description: String
    let title: String?
    let cost: Int
    let minutes: Int
}
