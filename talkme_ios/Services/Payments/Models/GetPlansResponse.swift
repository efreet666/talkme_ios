//
//  GetPlansResponse.swift
//  talkme_ios
//
//  Created by 1111 on 23.03.2021.
//

struct GetPlansResponse: Decodable {
    let id: Int
    let image: String
    let description: String?
    let title: String?
    let cost: Int
    let iosInAppId: Int?
    let iosCoinAmount: Int?
}
