//
//  PaymentsKoef.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 05.08.2021.
//

import Foundation

struct PaymentsKoef: Codable {
    let key: String
    let value: String
}
