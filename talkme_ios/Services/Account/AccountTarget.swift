//
//  AccountTarget.swift
//  talkme_ios
//
//  Created by 1111 on 08.01.2021.
//

import Moya

enum AccountTarget {
    case profile
    case contacts
    case achievements
    case profileEdit(ProfileEditRequest)
    case passwordChange(PasswordChangeRequest)
    case numberChange(SendMobileAndCodeRequest)
    case getMobileCode(SendMobileRequest)
    case sendMobileAndCode(SendMobileAndCodeRequest)
    case geoCities(String)
    case geoCountries
    case logout
    case publicProfile(String)
    case streamPublicProfile(String)
    case addContact(Int)
    case removeContact(Int)
    case numberClassCreate
    case addSocial(type: SocialMedia, url: String?)
    case removeSocial(type: SocialMedia)
    case myNetwork(pageNumber: Int, pageSize: Int)
    case complaintCreate(toUserId: Int, compliantType: Int, compliantString: String)
    case complaint
    case deleteMyProfile
}

extension AccountTarget: TargetType {

    static let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }()

    var baseURL: URL { Configuration.baseUrl }

    var path: String {
        switch self {
        case .profile:
            return "/users/profile/"
        case .contacts:
            return "/users/contacts/"
        case .achievements:
            return "/users/achievements/"
        case .profileEdit:
            return "/users/profile_edit/"
        case .passwordChange:
            return "/users/password/change/"
        case .numberChange:
            return "/users/phone_edit/"
        case .getMobileCode:
            return "users/mobile_verify/"
        case .sendMobileAndCode:
            return "users/mobile_verify/"
        case .geoCities:
            return "/geo/cities/"
        case .geoCountries:
            return "/geo/countries/"
        case .logout:
            return "/users/logout/"
        case .publicProfile:
            return "users/public_profile/"
        case .streamPublicProfile:
            return "users/public_profile/"
        case .addContact, .removeContact:
            return "/users/a_r_contact/"
        case .numberClassCreate:
            return "/users/class_number_create/"
        case .addSocial:
            return "/users/add_social/"
        case .removeSocial:
            return "/users/remove_social/"
        case .myNetwork:
            return "/users/my_network/"
        case .complaint:
            return "/users/complaint/types/"
        case .complaintCreate:
            return "users/complaint/create/"
        case .deleteMyProfile:
            return "users/delete_profile/"
        }
    }

    var method: Moya.Method {
        switch self {
        case .profile, .contacts, .achievements, .logout,
             .geoCities, .geoCountries, .publicProfile, .streamPublicProfile, .addContact,
             .removeContact, .numberClassCreate, .removeSocial, .myNetwork, .complaint:
            return .get
        case .profileEdit:
            return .patch
        case .passwordChange, .numberChange, .getMobileCode, .sendMobileAndCode, .addSocial, .complaintCreate, .deleteMyProfile:
            return .post
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .profile, .geoCountries, .numberClassCreate, .complaint, .deleteMyProfile:
            return .requestPlain
        case .myNetwork(let pageNumber, let pageSize):
            let params: [String: Any] = ["page": pageNumber, "page_size": pageSize]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .contacts:
            return .requestPlain
        case .achievements:
            return .requestPlain
        case .profileEdit(let request):
            if request.avatarUrl == nil {
                return .requestParameters(
                    parameters: ["last_name": request.lastName,
                                 "first_name": request.firstName,
                                 "bio": request.bio,
                                 "birthday": request.birthday,
                                 "email": request.email,
                                 "gender": request.gender,
                                 "username": request.username,
                                 "country": request.country,
                                 "geo_id": request.geoId
                                ],
                    encoding: JSONEncoding.default
                )
            } else {
                let multipartData = request.multipartData()
                return .uploadMultipart(multipartData)
            }
        case .passwordChange(let request):
            return .requestJSONEncodable(request)
        case .numberChange(let request):
            return .requestJSONEncodable(request)
        case .getMobileCode(let request):
            let data = try? AuthTarget.encoder.encode(request)
            return .requestCompositeData(bodyData: data ?? Data(), urlParameters: ["call": true])
        case .sendMobileAndCode(let request):
            let data = try? AuthTarget.encoder.encode(request)
            return .requestCompositeData(bodyData: data ?? Data(), urlParameters: ["call": true])
        case .logout:
            return .requestPlain
        case .geoCities(let code):
            let params = ["iso": code]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case .publicProfile(let id):
            let publicProfileParams: [String: String] = ["id": id]
            return .requestParameters(parameters: publicProfileParams, encoding: URLEncoding.queryString)
        case .streamPublicProfile(let id):
            let streamPublicProfileParams: [String: String] = ["number_class": id]
            return .requestParameters(parameters: streamPublicProfileParams, encoding: URLEncoding.queryString)
        case .addContact(let id):
            let addContactParams: [String: Any] = ["user_id": id, "add": true]
            return .requestParameters(parameters: addContactParams, encoding: URLEncoding.queryString)
        case .removeContact(let id):
            let removeContactParams: [String: Any] = ["user_id": id]
            return .requestParameters(parameters: removeContactParams, encoding: URLEncoding.queryString)
        case .addSocial(let type, let url):
            let parameters = [type.rawValue: url]
            return .requestParameters(
                parameters: parameters as [String: Any],
                encoding: JSONEncoding.default
            )
        case .removeSocial(let type):
            let removeSocialParams: [String: Any] = ["social": type.rawValue]
            return .requestParameters(parameters: removeSocialParams, encoding: URLEncoding.queryString)
        case .complaintCreate(let userId, let compliantType, let compliantString):
            let params = ["type": compliantType, "text" : compliantString, "user_about" : userId] as [String : Any]
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        }
    }

    var headers: [String: String]? {
        let isLoggedfromSocials = UserDefaultsHelper.shared.isLoggedfromSocials
        switch self {
        case .profile, .contacts, .passwordChange, .numberChange,
             .getMobileCode, .sendMobileAndCode, .logout, .geoCities,
             .geoCountries, .publicProfile, .streamPublicProfile, .numberClassCreate, .myNetwork,
             .complaintCreate, .complaint, .deleteMyProfile:
            return [
                "Content-Type": "application/json",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]
        case .achievements, .addContact, .removeContact, .addSocial, .removeSocial:
            return [
                "Content-Type": "application/json",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]
        case .profileEdit(let request):
            return [
                "Content-Type": request.avatarUrl == nil ? "application/json": "multipart/form-data",
                "Authorization": isLoggedfromSocials ? UserDefaultsHelper.shared.accessToken : "Token \(UserDefaultsHelper.shared.accessToken)"
            ]
        }
    }

    var validationType: ValidationType { .successAndRedirectCodes }
}
