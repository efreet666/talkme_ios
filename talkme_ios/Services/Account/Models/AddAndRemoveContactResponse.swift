//
//  AddAndRemoveContactResponse.swift
//  talkme_ios
//
//  Created by 1111 on 12.02.2021.
//

struct AddAndRemoveContactResponse: Decodable {
    let resp: String?
}

struct ClassNumberCreateResponse: Decodable {
    let numberClass: String
}
