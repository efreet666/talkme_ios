//
//  MyNetworkResponse.swift
//  talkme_ios
//
//  Created by Майя Калицева on 24.03.2021.
//

struct MyNetworkResponse: Decodable, Equatable {
    let data: [TeacherResponse]
}

struct TeacherResponse: Decodable, Equatable {
    let firstName: String
    let lastName: String
    let avatar: String?
    let rating: Double
    let id: Int
    let classNumber: String?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case avatar = "avatar_url"
        case rating
        case id
        case classNumber = "number_class"
    }
}
