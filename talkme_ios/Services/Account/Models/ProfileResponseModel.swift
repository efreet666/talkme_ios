//
//  ProfileResponseModel.swift
//  talkme_ios
//
//  Created by 1111 on 08.01.2021.
//

struct ProfileResponse: Decodable, Equatable {
    var firstName: String?
    var lastName: String?
    var bio: String?
    var gender: String?
    var city: String?
    var avatarUrl: String?
    var avatarUrl100: String?
    var isSpecialist: Bool? = false
    var isStaff: Bool? = false
    var rating: Double? = 0.0
    var username: String?
    var birthday: String?
    var numberClass: String?
    var country: String?
    var id: Int

    var fullName: String {
        let firstName = self.firstName ?? ""
        let lastName = self.lastName ?? ""
        if !firstName.isEmpty && !lastName.isEmpty {
            return "\(firstName) \(lastName)"
        } else if !firstName.isEmpty {
            return firstName
        } else if !lastName.isEmpty {
            return lastName
        } else {
            assertionFailure("State when we don't recieve username. We must to know that it is possible by this assert")
            return ""
        }
    }

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case bio
        case gender
        case city
        case avatarUrl = "avatar_url"
        case avatarUrl100 = "avatar_url_100"
        case isSpecialist = "is_specialist"
        case isStaff = "is_staff"
        case rating
        case username
        case birthday
        case numberClass = "class_number"
        case country
        case id
        }

    init(model: PublicProfileResponse ) {
        firstName = model.specialistInfo.firstName
        lastName = model.specialistInfo.lastName
        bio = model.specialistInfo.bio
        gender = model.specialistInfo.gender
        city = model.specialistInfo.city?.city
        avatarUrl = model.specialistInfo.avatarUrl
        avatarUrl100 = nil
        isSpecialist = true
        isStaff = nil
        rating = model.specialistInfo.rating
        username = model.specialistInfo.username
        birthday = nil
        numberClass = model.specialistInfo.numberClass
        country = model.specialistInfo.city?.country
        id = model.specialistInfo.id ?? 0
    }
}
