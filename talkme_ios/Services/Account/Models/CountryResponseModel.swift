//
//  CountryResponseModel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.01.2021.
//

struct Cities: Decodable {
    var cities: [CityResponse]?

    init(from decoder: Decoder) throws {
        let unkeyedContainer = try? decoder.singleValueContainer()
        cities = try? unkeyedContainer?.decode([CityResponse].self)
    }
}

struct CityResponse: Decodable {
    let city: String
    let geoId: Int

    enum CodingKeys: String, CodingKey {
        case city
        case geoId = "geoname_id"
    }
}

struct Countries: Decodable {
    var countries: [CountryResponse]?

    init(from decoder: Decoder) throws {
        let unkeyedContainer = try? decoder.singleValueContainer()
        countries = try? unkeyedContainer?.decode([CountryResponse].self)
    }
}

struct CountryResponse: Decodable {
    var country: String
    var countryCode: String

    enum CodingKeys: String, CodingKey {
        case country
        case countryCode = "country_iso_code"
    }
}
