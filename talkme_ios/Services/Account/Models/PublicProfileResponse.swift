//
//  PublicProfileResponse.swift
//  talkme_ios
//
//  Created by 1111 on 10.02.2021.
//

struct PublicProfileResponse: Decodable {
    let allLessons: [LiveStream]?
    let specialistInfo: PublicSpecialistInfo
    let isContact: Bool
}

struct PublicSpecialistInfo: Decodable {
    let id: Int?
    let gender: String?
    let age: Int?
    let geo: Int?
    let bio: String?
    let instagram: String?
    let vk: String?
    let facebook: String?
    let telegram: String?
    let avatarUrl: String?
    let firstName: String?
    let lastName: String?
    let city: City?
    let genderName: String?
    let lessonPlanedSP: Int?
    let username: String?
    let numberClass: String?
    let lessonPassed: Int?
    let rating: Double?
    let phone: String?
    let vip: Bool
    let subscriptionsCount: Int?
    let subscribersCount: Int?
}

struct City: Decodable {
    let city, region, country: String?
    let geonameID: Int?
}
