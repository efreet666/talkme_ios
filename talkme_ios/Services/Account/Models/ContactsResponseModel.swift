//
//  ContactsResponseModel.swift
//  talkme_ios
//
//  Created by 1111 on 08.01.2021.
//

struct ContactsResponse: Decodable, Equatable {
    let email: String?
    let mobile: String?
    var instagram: String?
    var vk: String?
    var facebook: String?
    var telegram: String?

    init(contacts: PublicProfileResponse) {
        email = nil
        mobile = contacts.specialistInfo.phone
        instagram = contacts.specialistInfo.instagram
        vk = contacts.specialistInfo.vk
        facebook = contacts.specialistInfo.facebook
        telegram = contacts.specialistInfo.telegram
    }
}
