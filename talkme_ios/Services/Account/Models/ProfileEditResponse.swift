//
//  ProfileEditResponse.swift
//  talkme_ios
//
//  Created by Yura Fomin on 13.01.2021.
//

import Moya

struct ProfileEditRequest: Encodable {
    let lastName: String?
    let firstName: String?
    let bio: String?
    let birthday: String?
    let email: String?
    let gender: String?
    let username: String?
    let avatarUrl: Data?
    let country: String?
    let city: String?
    let geoId: String?
    let socials: ContactsResponse

    enum CodingKeys: String, CodingKey {
        case lastName = "last_name"
        case firstName = "first_name"
        case bio
        case birthday
        case email
        case gender
        case username
        case avatarUrl = "avatar_url"
        case country
        case geoId = "geo_id"
        }

    func multipartData() -> [MultipartFormData] {
        var multipartData = [MultipartFormData]()

        if let lastName = data(from: lastName) {
            multipartData.append(MultipartFormData(provider: .data(lastName), name: CodingKeys.lastName.rawValue))
        }
        if let firstName = data(from: firstName) {
            multipartData.append(MultipartFormData(provider: .data(firstName), name: CodingKeys.firstName.rawValue))
        }
        if let bio = data(from: bio) {
            multipartData.append(MultipartFormData(provider: .data(bio), name: CodingKeys.bio.rawValue))
        }
        if let birthday = data(from: birthday) {
            multipartData.append(MultipartFormData(provider: .data(birthday), name: CodingKeys.birthday.rawValue))
        }
        if let email = data(from: email) {
            multipartData.append(MultipartFormData(provider: .data(email), name: CodingKeys.email.rawValue))
        }
        if let gender = data(from: gender) {
            multipartData.append(MultipartFormData(provider: .data(gender), name: CodingKeys.gender.rawValue))
        }
        if let username = data(from: username) {
            multipartData.append(MultipartFormData(provider: .data(username), name: CodingKeys.username.rawValue))
        }
        if let avatar = avatarUrl {
            multipartData.append(MultipartFormData(provider: .data(avatar), name: "avatar_url", fileName: "photo.jpg", mimeType: "image/jpeg"))
        }
        if let country = data(from: country) {
            multipartData.append(MultipartFormData(provider: .data(country), name: CodingKeys.country.rawValue))
        }
        if let geoId = data(from: geoId) {
            multipartData.append(MultipartFormData(provider: .data(geoId), name: CodingKeys.geoId.rawValue))
        }

        return multipartData
    }

    private func data(from value: String?) -> Data? {
        guard !(value?.isEmpty ?? true) else { return "".data(using: .utf8) }
        return value?.data(using: .utf8)
    }
}

struct ProfileEditResponse: Decodable {
    let response: String?

    let emailErrors: [String]?
    let email: String?

    enum CodingKeys: String, CodingKey {
        case response = "resp"
        case email
    }

    init(from decoder: Decoder) throws {
        let container = try? decoder.container(keyedBy: CodingKeys.self)

        response = try? container?.decodeIfPresent(String.self, forKey: CodingKeys.response)
        emailErrors = try? container?.decodeIfPresent([String].self, forKey: CodingKeys.email)
        email = try? container?.decodeIfPresent(String.self, forKey: CodingKeys.email)
    }
}
