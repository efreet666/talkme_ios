//
//  PasswordChangeResponse.swift
//  talkme_ios
//
//  Created by Yura Fomin on 13.01.2021.
//

struct PasswordChangeRequest: Encodable {
    let newPassword: String
    let repeatNewPassword: String

    enum CodingKeys: String, CodingKey {
        case newPassword = "new_password1"
        case repeatNewPassword = "new_password2"
    }
}

struct PasswordChangeResponse: Decodable {
    let detail: String?
}
