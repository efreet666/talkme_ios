//
//  ComplaintResponseModel.swift
//  talkme_ios
//
//  Created by nikita on 26.04.2022.
//

struct Complaints: Decodable {
    let complaints: [ComplaintResponse]?

    init(from decoder: Decoder) throws {
        let unkeyedContainer = try? decoder.singleValueContainer()
        complaints = try? unkeyedContainer?.decode([ComplaintResponse].self)
    }
}

struct ComplaintResponse: Decodable, Hashable {
    let key: Int
    let value: String
    
    enum CodingKeys: String, CodingKey {
        case key
        case value
    }
}
