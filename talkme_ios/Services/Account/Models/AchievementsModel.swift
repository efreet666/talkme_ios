//
//  AchievementsModel.swift
//  talkme_ios
//
//  Created by 1111 on 09.01.2021.
//

struct AchievementsResponse: Decodable {
    let lessonPassed: Int?
    enum CodingKeys: String, CodingKey {
        case lessonPassed = "lesson_passed"
    }
}
