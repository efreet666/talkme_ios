//
//  AccountService.swift
//  talkme_ios
//
//  Created by 1111 on 08.01.2021.
//

import Moya
import RxSwift

protocol AccountServiceProtocol {
    func getProfileInfo() -> Single<ProfileResponse>
    func getContacts() -> Single<ContactsResponse>
    func getAchievements() -> Single<AchievementsResponse>
    func sendProfileEdit(request: ProfileEditRequest) -> Single<ProfileEditResponse>
    func sendPasswordChange(request: PasswordChangeRequest) -> Single<PasswordChangeResponse>
    func sendNumberChange(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse>
    func getMobileCode(request: SendMobileRequest) -> Single<MobileVerifyResponse>
    func sendMobileAndCode(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse>
    func fetchCities(isoCode: String) -> Single<Cities>
    func fetchCountries() -> Single<Countries>
    func logout() -> Single<()>
    func publicProfile(classNumber: String) -> Single<PublicProfileResponse>
    func streamPublicProfile(classNumber: String) -> Single<PublicProfileResponse>
    func addContact(id: Int) -> Single<AddAndRemoveContactResponse>
    func removeContact(id: Int) -> Single<AddAndRemoveContactResponse>
    func classNumberCreate() -> Single<ClassNumberCreateResponse>
    func addSocial(request: SocialMedia, url: String?) -> Single<()>
    func removeSocial(request: SocialMedia) -> Single<()>
    func myNetwork(pageNumber: Int, pageSize: Int) -> Single<MyNetworkResponse>
    func complaintCreate(toUserId: Int, compliantType: Int, compliantString: String) -> Single<Response>
    func getComplaints() -> Single<Complaints>
    func deleteMyProfile() -> Single<()>
}

final class AccountService: AccountServiceProtocol {

    // MARK: - Private Properties

    private let isLoggingEnabled = false

    private lazy var plugins: [NetworkLoggerPlugin] = {
        return isLoggingEnabled
        ? [NetworkLoggerPlugin(configuration: .init( logOptions: [.verbose, .formatRequestAscURL]))]
            : []
    }()

    private lazy var provider = MoyaProvider<AccountTarget>(plugins: plugins)

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    // MARK: - Public Methods

    func getProfileInfo() -> Single<ProfileResponse> {
        provider.rx.request(.profile)
            .map(ProfileResponse.self).do(onSuccess: { response in
                UserDefaultsHelper.shared.avatarImage = response.avatarUrl ?? ""
                UserDefaultsHelper.shared.isSpecialist = response.isSpecialist ?? false
                UserDefaultsHelper.shared.classNumber = response.numberClass ?? ""
                UserDefaultsHelper.shared.fullName = response.fullName
                AmplitudeProvider.shared.updateIdentify()
            })
    }

    func getContacts() -> Single<ContactsResponse> {
        provider.rx.request(.contacts)
            .map(ContactsResponse.self)
    }

    func getAchievements() -> Single<AchievementsResponse> {
        provider.rx.request(.achievements)
            .map(AchievementsResponse.self)
    }

    func sendProfileEdit(request: ProfileEditRequest) -> Single<ProfileEditResponse> {
        provider.rx.request(.profileEdit(request))
            .map(ProfileEditResponse.self)
    }

    func sendPasswordChange(request: PasswordChangeRequest) -> Single<PasswordChangeResponse> {
        provider.rx.request(.passwordChange(request))
            .map(PasswordChangeResponse.self)
    }

    func sendNumberChange(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse> {
        provider.rx.request(.numberChange(request))
            .map(MobileVerifyResponse.self)
    }

    func getMobileCode(request: SendMobileRequest) -> Single<MobileVerifyResponse> {
        provider.rx.request(.getMobileCode(request))
            .map(MobileVerifyResponse.self, using: decoder)
    }

    func sendMobileAndCode(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse> {
        provider.rx.request(.sendMobileAndCode(request))
            .map(MobileVerifyResponse.self, using: decoder)
    }

    func fetchCities(isoCode: String) -> Single<Cities> {
        provider.rx.request(.geoCities(isoCode))
            .map(Cities.self)
            .asObservable()
            .share()
            .asSingle()
    }

    func fetchCountries() -> Single<Countries> {
        provider.rx.request(.geoCountries)
            .map(Countries.self)
            .asObservable()
            .share()
            .asSingle()
    }

    func logout() -> Single<()> {
        provider.rx.request(.logout)
            .filterSuccessfulStatusCodes()
            .map { _ in }
    }

    func publicProfile(classNumber: String) -> Single<PublicProfileResponse> {
        provider.rx.request(.publicProfile(classNumber))
            .map(PublicProfileResponse.self, using: decoder)
    }

    func streamPublicProfile(classNumber: String) -> Single<PublicProfileResponse> {
        provider.rx.request(.streamPublicProfile(classNumber))
            .map(PublicProfileResponse.self, using: decoder)
    }

    func addContact(id: Int) -> Single<AddAndRemoveContactResponse> {
        provider.rx.request(.addContact(id))
            .map(AddAndRemoveContactResponse.self)
            .catchErrorJustReturn(.init(resp: nil))
    }

    func removeContact(id: Int) -> Single<AddAndRemoveContactResponse> {
        provider.rx.request(.removeContact(id))
            .map(AddAndRemoveContactResponse.self)
            .catchErrorJustReturn(.init(resp: nil))
    }

    func classNumberCreate() -> Single<ClassNumberCreateResponse> {
        provider.rx.request(.numberClassCreate)
            .map(ClassNumberCreateResponse.self, using: decoder)
    }

    func addSocial(request: SocialMedia, url: String?) -> Single<()> {
        provider.rx
            .request(.addSocial(type: request, url: url))
            .filterSuccessfulStatusCodes()
            .map { _ in }
    }

    func removeSocial(request: SocialMedia) -> Single<()> {
        provider.rx
            .request(.removeSocial(type: request))
            .filterSuccessfulStatusCodes()
            .map { _ in }
    }

    func myNetwork(pageNumber: Int, pageSize: Int) -> Single<MyNetworkResponse> {
        provider.rx.request(.myNetwork(pageNumber: pageNumber, pageSize: pageSize))
            .map(MyNetworkResponse.self)
    }

    func getComplaints() -> Single<Complaints> {
        provider.rx.request(.complaint)
            .map(Complaints.self, using: decoder)
    }

    func complaintCreate(toUserId: Int, compliantType: Int, compliantString: String) -> Single<Response> {
        provider.rx.request(.complaintCreate(toUserId: toUserId, compliantType: compliantType, compliantString: compliantString))
    }

    func deleteMyProfile() -> Single<()> {
        provider.rx.request(.deleteMyProfile)
            .map { _ in () }
    }
}
