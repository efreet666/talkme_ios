//
//  AppsFlyerProvider.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 30/6/2022.
//

import Foundation
import AppsFlyerLib
import RxSwift
import StoreKit

class AppsFlyerProvider {

    private enum TrackingEvents: String {
        case userCompletedRegistration
        case userDonateToLiveStream
        case userDonateToSavedStream
        case userBuyedCoinsFrom5To10
        case userBuyedCoinsFrom10To25
        case userBuyedCoinsFrom25To50
        case userBuyedCoinsFrom50To200
        case userBuyedCoinsUpTo200

        var name: String {
            switch self {
            case .userCompletedRegistration:
                return "registerFinished"
            case .userDonateToLiveStream:
                return "donateLive"
            case .userDonateToSavedStream:
                return "donateSavedStream"
            case .userBuyedCoinsFrom5To10:
                return "incoming5_10"
            case .userBuyedCoinsFrom10To25:
                return "incoming10_25"
            case .userBuyedCoinsFrom25To50:
                return "incoming25_50"
            case .userBuyedCoinsFrom50To200:
                return "incoming50_200"
            case .userBuyedCoinsUpTo200:
                return "incoming200"
            }
        }
    }

    private enum EventProperties: String {
        case time
        case userId = "user_id"
        case appVersion = "app_version"
        case isStreamer = "isStreamer"
        case productId = "product_id"
        case productName = "product_name"
        case productCost = "product_cost"
        case productCoinAmount = "product_coin_amount"
        case currency
    }

    private let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
        return dateFormatter
    }()

    private let bag = DisposeBag()
    private var appVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
    }

    var currentProduct: SKProduct?
    var currentPurchase: GetPlansResponse?
    static let shared = AppsFlyerProvider()

    // MARK: - Public Methods

    func trackUserCompletedLoggin() {
        let date = Date()
        AppsFlyerLib.shared().logEvent(TrackingEvents.userCompletedRegistration.name,
                                       withValues: [EventProperties.time: dateFormatter.string(from: date),
                                                    EventProperties.userId: UserDefaultsHelper.shared.userId,
                                                    EventProperties.appVersion: appVersion].merging(baseEventProperties) { first, second in first })
    }

    func trackUserBoughtCoins() {
        guard let purchase = currentPurchase,
              let product = currentProduct else { return }
        let date = Date()

        let eventProperties: [AnyHashable: Any] = [EventProperties.time: dateFormatter.string(from: date),
                                                   EventProperties.userId: UserDefaultsHelper.shared.userId,
                                                   EventProperties.productId: product.productIdentifier,
                                                   EventProperties.productName: purchase.title ?? "",
                                                   EventProperties.productCoinAmount: purchase.iosCoinAmount ?? "",
                                                   EventProperties.productCost: String(product.price.intValue),
                                                   EventProperties.currency: product.priceLocale.currencySymbol ?? "",
                                                   EventProperties.appVersion: appVersion]

        switch product.price.intValue {
        case 5...10:
            AppsFlyerLib.shared().logEvent( TrackingEvents.userBuyedCoinsFrom5To10.name, withValues: eventProperties)
        case 11...25:
            AppsFlyerLib.shared().logEvent( TrackingEvents.userBuyedCoinsFrom10To25.name, withValues: eventProperties)
        case 26...50:
            AppsFlyerLib.shared().logEvent( TrackingEvents.userBuyedCoinsFrom25To50.name, withValues: eventProperties)
        case 50...200:
            AppsFlyerLib.shared().logEvent( TrackingEvents.userBuyedCoinsFrom50To200.name, withValues: eventProperties)
        case 200...:
            AppsFlyerLib.shared().logEvent( TrackingEvents.userBuyedCoinsUpTo200.name, withValues: eventProperties)
        default:
            break
        }

        self.currentPurchase = nil
        self.currentProduct = nil
    }

    func trackUserDonatingToStreamer(andGetResponse response: GetPlansResponse) {
        let date = Date()
        AppsFlyerLib.shared().logEvent( TrackingEvents.userDonateToLiveStream.name,
                                        withValues:
                                        [EventProperties.time: dateFormatter.string(from: date),
                                         EventProperties.userId: UserDefaultsHelper.shared.userId,
                                         EventProperties.productId: response.id,
                                         EventProperties.productName: response.title ?? "",
                                         EventProperties.productCoinAmount: response.iosCoinAmount ?? "",
                                         EventProperties.productCost: String(response.cost),
                                         EventProperties.appVersion: appVersion])
    }

    private var baseEventProperties: [AnyHashable: Any] {
        var result = [AnyHashable: Any]()
        result[EventProperties.isStreamer] = UserDefaultsHelper.shared.isSpecialist.description as NSObject

        let installData = UserDefaultsHelper.shared.appsFlyerData
        for (key, value) in installData {
            guard let key = key as? String,
                  let value = value as? NSObject
            else { continue }
            result[key] = value
        }
        return result
    }
}
