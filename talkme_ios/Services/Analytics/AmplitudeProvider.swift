//
//  AmplitudeProvider.swift
//  talkme_ios
//
//  Created by nikita on 16.05.2022.
//

import CollectionKit
import StoreKit
import RxCocoa
import RxSwift
import Amplitude

class AmplitudeProvider {

    private enum TrakingEvents: String {
        case signUp
        case login
        case startTeacherStream
        case startStudentStream
        case finishTeacherStream
        case finishStudentStream
        case streamEventMessage
        case buyCoins
        case firstStart

        func name() -> String {
            switch self {
            case .firstStart:
                return "AppsFlyerConversionData"
            case .signUp:
                return "Complete registration"
            case .login:
                return "Complete login"
            case .startTeacherStream:
                return "StartTeacherStream"
            case .startStudentStream:
                return "StartStudentStream"
            case .finishStudentStream:
                return "FinishStudentStream"
            case .finishTeacherStream:
                return "FinishTeacherStream"
            case .buyCoins:
                return "Complete purchase"
            case .streamEventMessage:
                return "StreamEventMessage"
            }
        }
    }

    private enum EventProperties: String {
        case appVersion = "app_version"
        case time
        case userId = "user_id"
        case teacherId = "teacher_id"
        case lessonId = "lesson_id"
        case lessonDate = "lesson_date"
        case lessonName = "lesson_name"
        case lessonCategory = "lesson_category"
        case lessonVIP = "lesson_vip"
        case lessonEndTime = "lesson_end_time"
        case lessonLength = "lesson_length"
        case lessonMaxParticipant = "lesson_max_participant"
        case lessonGiftCount = "lesson_gift_count"
        case lessonGiftCostSum = "lesson_gift_cost_sum"
        case lessonNumberOfMessages = "lesson_number_of_messages"
        case productId = "product_id"
        case productName = "product_name"
        case productCost = "product_cost"
        case productCoinAmount = "product_coin_amount"
        case currency
        case body
    }

    private let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
        return dateFormatter
    }()

    private var appVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
    }

    private let bag = DisposeBag()
    private var identify = AMPIdentify()
    private let paymentsService = PaymentsService()
    private var gifts = [GetPlansResponse]()
    private var currentLesson: LessonDetailResponse?

    var currentPurchase: GetPlansResponse?
    var currentProduct: SKProduct?
    var currentChatModel = ArrayDataSource<LessonMessage>()

    static let shared = AmplitudeProvider()

    // MARK: - Public Methods

    func firstStartTraking(_ installData: [AnyHashable: Any]) {
        guard UserDefaultsHelper.shared.isFirstStart != false else { return }
        UserDefaultsHelper.shared.isFirstStart = false

        UserDefaultsHelper.shared.appsFlyerData = installData
        updateIdentify()

        Amplitude.instance().logEvent( TrakingEvents.firstStart.name(), withEventProperties: installData)
    }

    func loginTracking() {
        let date = Date()
        updateIdentify()

        Amplitude.instance().logEvent( TrakingEvents.login.name(), withEventProperties:
                                        [EventProperties.appVersion: appVersion,
                                         EventProperties.time: dateFormatter.string(from: date),
                                         EventProperties.userId: UserDefaultsHelper.shared.userId])
    }

    func signUpTracking() {
        let date = Date()
        updateIdentify()

        Amplitude.instance().logEvent( TrakingEvents.signUp.name(), withEventProperties:
                                        [EventProperties.time: dateFormatter.string(from: date),
                                         EventProperties.userId: UserDefaultsHelper.shared.userId,
                                         EventProperties.appVersion: appVersion])
    }

    func startStreamTracking(lesson: LessonDetailResponse?) {
        guard let lesson = lesson else { return }
        currentLesson = lesson
        let event = lesson.isOwner ? TrakingEvents.startTeacherStream : TrakingEvents.startStudentStream

        Amplitude.instance().logEvent( event.name(), withEventProperties:
                                        [EventProperties.teacherId: lesson.owner.id ?? "",
                                         EventProperties.lessonName: lesson.name,
                                         EventProperties.lessonDate: dateFormatter.string(from: Date(timeIntervalSince1970: lesson.date)),
                                         EventProperties.lessonCategory: lesson.categoryName,
                                         EventProperties.lessonVIP: lesson.cost != 0 && !lesson.isOwner,
                                         EventProperties.appVersion: appVersion])
    }

    func finishStreamTraking() {
        guard let currentLesson = currentLesson else { return }

        var cost = 0
        var giftCount = 0
        if currentChatModel.data.count == 0 {
            finishStreamEvent(cost: cost, giftsCount: giftCount, currentLesson: currentLesson)
            return
        }

        let bufferedCharModel = currentChatModel
        for message in bufferedCharModel.data {
            guard let gift = gifts.filter({ $0.image == message.gift }).first else { continue }
            cost += gift.cost
            giftCount += 1
        }
        finishStreamEvent(cost: cost, giftsCount: giftCount, currentLesson: currentLesson)

    }

    func streamMessageTraking(userId: Int, message: String) {
        guard let currentLesson = currentLesson else { return }

        let date = Date()
        Amplitude.instance().logEvent( TrakingEvents.login.name(), withEventProperties:
                                        [EventProperties.time: dateFormatter.string(from: date),
                                         EventProperties.userId: UserDefaultsHelper.shared.userId,
                                         EventProperties.body: message,
                                         EventProperties.lessonId: currentLesson.id])
    }

    func buyCoinsTraking() {
        guard let purchase = currentPurchase,
              let product = currentProduct else { return }

        let date = Date()
        Amplitude.instance().logEvent( TrakingEvents.login.name(), withEventProperties:
                                        [EventProperties.time: dateFormatter.string(from: date),
                                         EventProperties.userId: UserDefaultsHelper.shared.userId,
                                         EventProperties.productId: product.productIdentifier,
                                         EventProperties.productName: purchase.title ?? "",
                                         EventProperties.productCoinAmount: purchase.iosCoinAmount ?? "",
                                         EventProperties.productCost: String(product.price.intValue),
                                         EventProperties.currency: product.priceLocale.currencySymbol ?? "",
                                         EventProperties.appVersion: appVersion])

        Amplitude.instance().logRevenueV2(AMPRevenue()
            .setProductIdentifier(product.productIdentifier)
            .setPrice(product.price.intValue as NSNumber)
            .setQuantity(1))

        self.currentPurchase = nil
        self.currentProduct = nil
    }

    func updateIdentify() {
        identify = AMPIdentify()
        Amplitude.instance().setUserId(String(format: "%d", UserDefaultsHelper.shared.userId))
        identify.set("isStreamer", value: UserDefaultsHelper.shared.isSpecialist.description as NSObject)
        Amplitude.instance().identify(identify)

        let installData = UserDefaultsHelper.shared.appsFlyerData
        for (key, value) in installData {
            guard let key = key as? String,
                  let value = value as? NSObject
            else { continue }
            identify.set( key, value: value)
        }
    }

    // MARK: - Private Methods

    private func finishStreamEvent(cost: Int, giftsCount: Int, currentLesson: LessonDetailResponse) {

        let event = currentLesson.isOwner ? TrakingEvents.finishTeacherStream : TrakingEvents.finishStudentStream
        Amplitude.instance().logEvent( event.name(), withEventProperties:
                                        [EventProperties.teacherId: currentLesson.owner.id as Any,
                                         EventProperties.lessonName: currentLesson.name,
                                         EventProperties.lessonDate: dateFormatter.string(from: Date(timeIntervalSince1970: currentLesson.date)),
                                         EventProperties.lessonCategory: currentLesson.categoryName,
                                         EventProperties.lessonVIP: currentLesson.cost != 0 && !currentLesson.isOwner,
                                         EventProperties.lessonEndTime: time,
                                         EventProperties.lessonLength: currentLesson.lessonTime,
                                         EventProperties.lessonMaxParticipant: currentLesson.numberOfParticipants,
                                         EventProperties.lessonGiftCount: giftsCount,
                                         EventProperties.lessonGiftCostSum: cost,
                                         EventProperties.lessonNumberOfMessages: currentChatModel.data.count,
                                         EventProperties.appVersion: appVersion])
        self.currentLesson = nil
    }

    private func getGifts() {
        paymentsService.getGifts()
            .subscribe { event in
                switch event {
                case .success(let response):
                    self.gifts = response
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: bag)
    }

    private init() {
        getGifts()
    }

}
