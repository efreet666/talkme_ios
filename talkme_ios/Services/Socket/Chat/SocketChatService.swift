//
//  SocketChatService.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation
import RxSwift

protocol SocketChatService {
    var messages: PublishSubject<MessagesModel> { get }
    var moreMessages: PublishSubject<MoreMessagesModel> { get }
    var newMessage: PublishSubject<NewMessageModel> { get }
    var isConnected: PublishSubject<SocketConnection> { get }

    func fetchMessages(chatId: Int, userId: Int)
    func moreMessages(chatId: Int, userId: Int, length: Int)
    func newMessage(chatId: Int, userId: Int, message: String, image: [String?])
    func readNewMessages(chatId: Int, userId: Int)
    func removeChat(userId: Int, chatId: Int)
}

struct SocketChatServiceFactory {
    static func make(roomId: String) -> SocketChatService {
        let manager = SocketClientManager(with: .chat(roomId: roomId))
        return SocketChatServiceImpl(manager: manager)
    }
}

class SocketChatServiceImpl: SocketChatService {

    // MARK: - Public Properties

    private(set) var messages = PublishSubject<MessagesModel>()
    private(set) var moreMessages = PublishSubject<MoreMessagesModel>()
    private(set) var newMessage = PublishSubject<NewMessageModel>()
    private(set) var isConnected = PublishSubject<SocketConnection>()

    // MARK: - Private Properties

    private let manager: SocketClientManager
    private let disposeBag = DisposeBag()

    // MARK: - Lifecycle

    init(manager: SocketClientManager) {
        self.manager = manager
        bind()
    }

    // MARK: - Public Methods

    func fetchMessages(chatId: Int, userId: Int) {
        let request = FetchMessagesRequest(chatId: chatId, userId: userId)
        manager.send(message: request)
    }

    func moreMessages(chatId: Int, userId: Int, length: Int) {
        let request = MoreMessagesRequest(chatId: chatId, userId: userId, length: length)
        manager.send(message: request)
    }

    func newMessage(chatId: Int, userId: Int, message: String, image: [String?]) {
        let request = NewMessageRequest(chatId: chatId, userId: userId, message: message, image: image)
        manager.send(message: request)
    }

    func readNewMessages(chatId: Int, userId: Int) {
        let request = MessageReaderRequest(chatId: chatId, userId: userId)
        manager.send(message: request)
    }

    func removeChat(userId: Int, chatId: Int) {
        let request = RemoveChatRequest(userId: userId, chatId: chatId)
        manager.send(message: request)
    }

    // MARK: - Private Methods

    private func bind() {

        manager
            .isConnected
            .bind(to: isConnected)
            .disposed(by: disposeBag)

        let decoded = manager.message
            .compactMap { SocketResponseMapper.decodeEvent(message: $0) }
            .share()

        decoded.compactMap { $0 as? MessagesModel }
            .bind(to: messages)
            .disposed(by: disposeBag)

        decoded.compactMap { $0 as? MoreMessagesModel }
            .bind(to: moreMessages)
            .disposed(by: disposeBag)

        decoded.compactMap { $0 as? NewMessageModel }
            .bind(to: newMessage)
            .disposed(by: disposeBag)
    }
}
