//
//  RemoveChatRequest.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 28.07.2021.
//

import Foundation

struct RemoveChatRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .removeChat
    let userId: Int
    let chatId: Int

    enum CodingKeys: String, CodingKey {
        case command
        case userId = "user_id"
        case chatId = "chat_id"
    }
}
