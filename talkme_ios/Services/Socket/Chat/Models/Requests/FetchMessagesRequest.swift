//
//  FetchMessagesRequest.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

struct FetchMessagesRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .fetchMessages
    let chatId: Int?
    let userId: Int

    enum CodingKeys: String, CodingKey {
        case command
        case chatId = "chat_id"
        case userId = "user_id"
    }
}
