//
//  NewMessageRequest.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

struct NewMessageRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .newMessage
    let chatId: Int
    let userId: Int
    let message: String
    let image: [String?]

    enum CodingKeys: String, CodingKey {
        case command
        case chatId = "chat_id"
        case userId = "user_id"
        case message
        case image
    }
}
