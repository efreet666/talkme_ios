//
//  MessageReaderRequest.swift
//  talkme_ios
//
//  Created by 1111 on 28.04.2021.
//

import Foundation

struct MessageReaderRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .messageReader
    let chatId: Int?
    let userId: Int

    enum CodingKeys: String, CodingKey {
        case command
        case chatId = "chat_id"
        case userId = "user_id"
    }
}
