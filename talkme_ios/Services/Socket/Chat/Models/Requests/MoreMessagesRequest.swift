//
//  MoreMessagesRequest.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

struct MoreMessagesRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .moreMessages
    let chatId: Int
    let userId: Int
    let length: Int

    enum CodingKeys: String, CodingKey {
        case command
        case chatId = "chat_id"
        case userId = "user_id"
        case length
    }
}
