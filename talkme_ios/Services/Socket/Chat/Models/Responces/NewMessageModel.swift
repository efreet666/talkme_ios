//
//  NewMessageModel.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

struct NewMessageModel: Decodable {
    let lastMessage: Double
    let message: Message

    enum CodingKeys: String, CodingKey {
        case lastMessage = "last_message"
        case message
    }
}
