//
//  MessagesModel.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

struct MessagesModel: Decodable {
    let userId: Int
    let contact: [MessagesContact]
    let messages: [Message]

    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case contact
        case messages
    }
}

struct MessagesContact: Decodable {
    let avatar: String?
    let id: Int
    let fullName: String
    let lastSeen: Double
    let numberClass: String?

    enum CodingKeys: String, CodingKey {
        case avatar
        case id
        case fullName = "full_name"
        case lastSeen = "last_seen"
        case numberClass = "number_class"
    }
}

struct Message: Decodable {
    let author: String
    let content: String
    let image: [String]?
    let timestamp: Double
    let userId: Int
    let avatar: String?
    let new: Bool
    let chatId: Int

    enum CodingKeys: String, CodingKey {
        case author, content, image, timestamp, avatar, new
        case userId = "user_id"
        case chatId = "chat_id"
    }
}

struct ChatMessage {
    let message: Message
    let isNew: Bool
}
