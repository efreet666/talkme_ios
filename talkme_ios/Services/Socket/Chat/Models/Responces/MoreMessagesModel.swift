//
//  MoreMessagesModel.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

struct MoreMessagesModel: Decodable {
    let userId: Int
    let messages: [Message]

    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case messages
    }
}
