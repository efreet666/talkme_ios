//
//  FetchChatRequest.swift
//  talkme_ios
//
//  Created by 1111 on 20.03.2021.
//

import Foundation

struct FetchChatRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .fetchChat
    let userId: Int

    enum CodingKeys: String, CodingKey {
        case command
        case userId = "user_id"
    }
}
