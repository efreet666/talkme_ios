//
//  ChatsSearchingRequest.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 03.08.2021.
//

import Foundation

struct ChatsSearchingRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .searching
    let userId: Int
    let search: String

    enum CodingKeys: String, CodingKey {
        case command
        case userId = "user_id"
        case search
    }
}
