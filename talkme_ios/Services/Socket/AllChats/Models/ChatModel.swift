//
//  ChatModel.swift
//  talkme_ios
//
//  Created by 1111 on 20.03.2021.
//

import Foundation

struct ChatModel: Decodable {
    let userId: Int
    let chats: [Chat]

    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case chats
    }
}

struct Chat: Decodable {
    let lastMessage: String
    let chatId: Int
    let lastTimeStamp: Double
    let participants: [Participants]
    let new: Int
    let group: Bool

    enum CodingKeys: String, CodingKey {
        case lastMessage = "last_message"
        case chatId = "chat_id"
        case lastTimeStamp = "last_timestamp"
        case participants
        case new
        case group
    }
}

struct Participants: Decodable {
    let userId: Int
    let name: String?
    let avatarUrl: String?
    let lastSeen: Double?
    let online: Bool?
    let timeLeft: Double?

    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case online
        case name
        case avatarUrl = "avatar_url"
        case lastSeen = "last_seen"
        case timeLeft = "time_left"
    }
}
