//
//  SearchChatsModel.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 03.08.2021.
//

import Foundation

struct SearchChatsModel: Decodable {
    var command = "search"
    let userId: Int
    let chats: [Chat]
    let messages: [Message]

    enum CodingKeys: String, CodingKey {
        case command
        case userId = "user_id"
        case chats
        case messages
    }
}
