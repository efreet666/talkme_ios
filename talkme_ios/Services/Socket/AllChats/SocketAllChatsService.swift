//
//  SocketAllChatsService.swift
//  talkme_ios
//
//  Created by 1111 on 28.04.2021.
//

import Foundation
import RxSwift

protocol SocketAllChatsService {
    var chats: PublishSubject<ChatModel> { get }
    var searchChats: PublishSubject<SearchChatsModel> { get }
    var isConnected: PublishSubject<SocketConnection> { get }

    func fetchChats(userId: Int)
    func searching(userId: Int, searchText: String)
    func disconnect()
}

struct SocketAllChatsServiceFactory {
    static func make() -> SocketAllChatsService {
        let manager = SocketClientManager(with: .chats)
        return SocketAllChatsServiceImpl(manager: manager)
    }
}

class SocketAllChatsServiceImpl: SocketAllChatsService {

    // MARK: - Public Properties

    private(set) var chats = PublishSubject<ChatModel>()
    private(set) var searchChats = PublishSubject<SearchChatsModel>()
    private(set) var isConnected = PublishSubject<SocketConnection>()

    // MARK: - Private Properties

    private let manager: SocketClientManager
    private let disposeBag = DisposeBag()

    // MARK: - Lifecycle

    init(manager: SocketClientManager) {
        self.manager = manager
        bind()
    }

    // MARK: - Public Methods

    func fetchChats(userId: Int) {
        let request = FetchChatRequest(userId: userId)
        manager.send(message: request)
    }

    func searching(userId: Int, searchText: String) {
        let request = ChatsSearchingRequest(userId: userId, search: searchText)
        manager.send(message: request)
    }

    func disconnect() {
        manager.disconnect()
    }

    // MARK: - Private Methods

    private func bind() {

        manager
            .isConnected
            .bind(to: isConnected)
            .disposed(by: disposeBag)

        let decoded = manager.message
            .compactMap { SocketResponseMapper.decodeEvent(message: $0) }
            .share()

        decoded.compactMap { $0 as? ChatModel }
            .bind(to: chats)
            .disposed(by: disposeBag)

        decoded.compactMap { $0 as? SearchChatsModel }
            .bind(to: searchChats)
            .disposed(by: disposeBag)
    }
}
