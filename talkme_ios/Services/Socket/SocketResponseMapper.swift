//
//  SocketResponseMapper.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

struct SocketResponseMapper {
    static func decodeEvent(message: String) -> Decodable? {
        guard let response: SocketResponse = map(message: message) else { return nil }
        switch response.command {
        case .messages:
            guard let model: MessagesModel = map(message: message) else {
                guard let lessonChatModel: LessonMessagesResponce = map(message: message) else { return nil }
                return lessonChatModel }
            return model
        case .moreMessages:
            guard let model: MoreMessagesModel = map(message: message) else {
                guard let model: LessonMessagesResponce = map(message: message) else { return nil }
                return model }
            return model
        case .newMessage:
            guard let model: NewMessageModel = map(message: message) else {
                guard let lessonChatModel: NewLessonMessageResponce = map(message: message) else { return nil }
                return lessonChatModel }
            return model
        case .chats:
            guard let model: ChatModel = map(message: message) else { return nil }
            return model
        case .participantsCount:
            guard let model: ParticipantsCountModel = map(message: message) else { return nil }
            return model
        case .buyMinutes:
            guard let model: BuyMinutesModel = map(message: message) else { return nil}
            return model
        case .cameraList:
            guard let model: CamerasListModel = map(message: message) else { return nil}
            return model
        case .rewriteUuid:
            guard let model: BuyMinutesModel = map(message: message) else { return nil}
            return model
        case .someOneDisconnect:
            guard let model: SomeoneDisconnectModel = map(message: message) else { return nil}
            return model
        case .online:
            guard let model: UserStatusOnlineModel = map(message: message) else { return nil}
            return model
        case .offline:
            guard let model: UserStatusOfflineModel = map(message: message) else { return nil}
            return model
        case .search:
            guard let model: SearchChatsModel = map(message: message) else { return nil }
            return model
        case .customChannel:
            guard let model: DataChannelResponce = map(message: message) else { return nil }
            return model
        default:
            return nil
        }
    }

    static private func map<T: Decodable>(message: String) -> T? {
        guard let data = message.data(using: .utf8) else {
            assertionFailure("Can't transform socket response string to data")
            return nil
        }
        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            print("[Socket] Can't decode \(message). Error: \(error.localizedDescription)")
            return nil
        }
    }
}
