//
//  SocketCamerasService.swift
//  talkme_ios
//
//  Created by 1111 on 26.04.2021.
//

import Foundation
import RxSwift

class SocketCamerasService {

    // MARK: - Private Properties

    private let manager: SocketClientManager
    private let disposeBag = DisposeBag()

    // MARK: - Lifecycle

    init(manager: SocketClientManager) {
        self.manager = manager
    }

    // MARK: - Public Methods

    func buyMinutes(user: String, minutePlan: Int) {
        let request = BuyMinutesRequest(minutePlan: minutePlan, user: user)
        manager.send(message: request)
    }

    func fetchCamerasList() {
        let request = FetchCameraListRequest()
        manager.send(message: request)
    }

    func rewriteBoughtCamers(user: String) {
        let request = RewriteBoughtCamerasRequest(user: user)
        manager.send(message: request)
    }
}
