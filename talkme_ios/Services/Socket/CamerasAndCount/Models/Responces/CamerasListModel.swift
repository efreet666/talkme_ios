//
//  BuyMinutesResponse.swift
//  talkme_ios
//
//  Created by 1111 on 24.03.2021.
//

import Foundation

struct CamerasListModel: Decodable {
    let command: String
    let list: [CamerasListItem]
}

struct CamerasListItem: Decodable {
    let user: String
    let timeEnd: Double
    let online: Bool

    enum CodingKeys: String, CodingKey {
        case user
        case timeEnd = "timestamp_end"
        case online
    }
}
