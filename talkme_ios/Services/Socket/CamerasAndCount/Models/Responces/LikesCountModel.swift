//
//  LikesCountModel.swift
//  talkme_ios
//
//  Created by admin on 11.10.2022.
//

import Foundation

struct LikesCountModel: Decodable {
    let count: Int?
    let likes: Int?
}
