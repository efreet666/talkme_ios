//
//  SomeoneDisconnectModel.swift
//  talkme_ios
//
//  Created by 1111 on 19.04.2021.
//

import Foundation

struct SomeoneDisconnectModel: Decodable {
    let command: String
    let user: String
}
