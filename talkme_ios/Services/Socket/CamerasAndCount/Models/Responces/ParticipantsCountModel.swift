//
//  ParticipantsCountModel.swift
//  talkme_ios
//
//  Created by 1111 on 20.03.2021.
//

import Foundation

struct ParticipantsCountModel: Decodable {
    let command: String
    let count: Int
    let user: Int
}
