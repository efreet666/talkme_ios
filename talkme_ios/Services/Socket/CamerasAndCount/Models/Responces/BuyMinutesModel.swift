//
//  BuyMinutesModel.swift
//  talkme_ios
//
//  Created by 1111 on 07.04.2021.
//

import Foundation

struct BuyMinutesModel: Decodable {
    let command: String
    let timeEnd: Double

    enum CodingKeys: String, CodingKey {
        case command
        case timeEnd = "timestamp_end"
    }
}
