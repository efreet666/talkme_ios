//
//  RewriteBoughtCamerasRequest.swift
//  talkme_ios
//
//  Created by 1111 on 16.04.2021.
//

import Foundation

struct RewriteBoughtCamerasRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .rewriteUuid
    let user: String

    enum CodingKeys: String, CodingKey {
        case command
        case user
    }
}
