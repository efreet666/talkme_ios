//
//  BuyMinutesRequest.swift
//  talkme_ios
//
//  Created by 1111 on 24.03.2021.
//

import Foundation

struct BuyMinutesRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .buyMinutes
    let minutePlan: Int
    let user: String

    enum CodingKeys: String, CodingKey {
        case command
        case minutePlan = "minute_plan"
        case user
    }
}
