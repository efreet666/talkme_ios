//
//  FetchCameraListRequest.swift
//  talkme_ios
//
//  Created by 1111 on 02.04.2021.
//

import Foundation

struct FetchCameraListRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .cameraList
}
