//
//  SocketRequest.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

enum SocketRequestCommand: String, Encodable {
    case fetchMessages = "fetch_messages"
    case moreMessages = "more_messages"
    case newMessage = "new_message"
    case fetchChat = "fetch_chats"
    case cameraList = "cameras_list"
    case buyMinutes = "buy_minutes"
    case rewriteUuid = "rewrite_uuid"
    case messageReader = "message_reader"
    case removeChat = "remove_chat"
    case searching = "searching"
    case customChannel = "custom_channel"
}

protocol SocketRequest: Encodable {
    var command: SocketRequestCommand { get }
}

extension SocketRequest {
    var json: String? {
        do {
            let data = try JSONEncoder().encode(self)
            return String(data: data, encoding: .utf8)
        } catch {
            assertionFailure("Can't encode socket request model \(type(of: self))")
            return nil
        }
    }
}
