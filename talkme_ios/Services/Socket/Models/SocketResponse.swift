//
//  SocketResponse.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

enum SocketResponseEventType: String, Decodable {
    case messages
    case moreMessages = "more_messages"
    case newMessage = "new_message"
    case chats
    case participantsCount = "users_count"
    case cameraList = "cameras_list"
    case buyMinutes = "buy_minutes"
    case rewriteUuid = "rewrite_uuid"
    case someOneDisconnect = "someone_disconnect"
    case online = "online"
    case offline = "offline"
    case customChannel = "custom_channel"
    case other
    case search

    init(from decoder: Decoder) throws {
        let value = try decoder.singleValueContainer().decode(String.self)
        self = SocketResponseEventType(rawValue: value) ?? .other
    }
}

struct SocketResponse: Decodable {
    let command: SocketResponseEventType
}
