//
//  SocketClientManager.swift
//  talkme_ios
//
//  Created by Den on 03.01.2021.
//

import RxSwift
import Starscream

enum SocketConnection {
    case connected
    case disconnected
    case cancelled
    case error
}

final class SocketClientManager {

    // MARK: - Public Properties

    let message = PublishSubject<String>()
    let disposeBag = DisposeBag()
    let isConnected = PublishSubject<SocketConnection>()

    // MARK: - Private Properties

    private let connectionUrl: URL
    private let reachabilityObserver = ReachabilityManager()
    private var clientConnected = false

    private lazy var client: WebSocket = {
        let request = URLRequest(url: connectionUrl)
//        let compression = WSCompression()
//        let pinner = FoundationSecurity(allowSelfSigned: true)
        // NOTE: can be uncommented and used as parameters for WebSocket creation if needed
        return WebSocket(request: request)
    }()
    private let bag = DisposeBag()

    // MARK: - Lifecycle

    init(with configuration: SocketConfiguration) {
        connectionUrl = configuration.url

        // NOTE: maybe must start later, because we will initialize services on cell configuration, but chat will be visible only on viewing stream
        // other connections is redundant
        startWithNetwork()
    }

    deinit {
        closeConnection()
    }

    // MARK: - Public Methods

    func send(message: SocketRequest) {
        guard let json = message.json else { return }
        client.write(string: json)
    }

    func disconnect() {
        closeConnection()
    }

    // MARK: - Private Methods

    private func startWithNetwork() {
        reachabilityObserver.hasNetworkConnection
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self] connected in
                connected
                ? self?.openConnection()
                : self?.closeConnection()
            }).disposed(by: bag)
    }

    private func openConnection() {
        guard !clientConnected else { return }
        client.connect()
        clientConnected = true
        client.onEvent = { [weak self] event in
            switch event {
            case .connected:
                print("[Socket] connected")
                self?.isConnected.onNext(.connected)
            case .disconnected(let message, let code):
                self?.isConnected.onNext(.disconnected)
                print("[Socket] disconnected: \(message), code \(code)")
            case .cancelled:
                self?.isConnected.onNext(.cancelled)
                print("[Socket] connection cancelled")
            case .text(let message):
                self?.message.onNext(message)
            case .binary(let data):
                guard let message = String(data: data, encoding: .utf8) else { return }
                self?.message.onNext(message)
            case .error(let error):
                guard let error = error else { return }
                print(error)
                self?.isConnected.onNext(.error)
            default:
                break
            }
        }
        print("[Socket] try to connect \(connectionUrl)")
    }

    private func closeConnection() {
        print("[Socket] try to disconnect \(connectionUrl)")
        client.disconnect()
        clientConnected = false
    }
}
