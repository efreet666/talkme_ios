//
//  UserStatusModel.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 02.08.2021.
//

import Foundation

struct UserStatusOnlineModel: Decodable {
    var command = "online"
    let user: Int
}

struct UserStatusOfflineModel: Decodable {
    var command = "offline"
    let user: Int
}

struct UserStatus {
    let user: Int
    let isOnline: Bool
}
