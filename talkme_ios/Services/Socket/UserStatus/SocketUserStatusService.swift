//
//  SocketUserStatusService.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 02.08.2021.
//

import Foundation
import RxSwift

protocol SocketUserStatusService {
    var userOnline: PublishSubject<UserStatusOnlineModel> { get }
    var userOffline: PublishSubject<UserStatusOfflineModel> { get }
}

struct SocketUserStatusServiceFactory {
    static func make() -> SocketUserStatusService {
        let manager = SocketClientManager(with: .online)
        return SocketUserStatusServiceImp(manager: manager)
    }
}

class SocketUserStatusServiceImp: SocketUserStatusService {

    // MARK: - Public Properties

    private(set) var userOnline = PublishSubject<UserStatusOnlineModel>()
    private(set) var userOffline = PublishSubject<UserStatusOfflineModel>()

    // MARK: - Private Properties

    private let manager: SocketClientManager
    private let disposeBag = DisposeBag()

    // MARK: - Lifecycle

    init(manager: SocketClientManager) {
        self.manager = manager
        bind()
    }

    // MARK: - Private Methods

    private func bind() {
        let decoded = manager.message
            .compactMap { SocketResponseMapper.decodeEvent(message: $0) }
            .share()

        decoded.compactMap { $0 as? UserStatusOnlineModel }
            .bind(to: userOnline)
            .disposed(by: disposeBag)

        decoded.compactMap { $0 as? UserStatusOfflineModel }
            .bind(to: userOffline)
            .disposed(by: disposeBag)
    }
}
