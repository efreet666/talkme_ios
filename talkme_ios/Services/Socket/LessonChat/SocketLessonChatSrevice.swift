//
//  SocketLessonChatSrevice.swift
//  talkme_ios
//
//  Created by 1111 on 14.07.2021.
//

import Foundation
import RxSwift

protocol SocketLessonChatSrevice {
    var messages: PublishSubject<LessonMessagesResponce> { get }
    var newMessage: PublishSubject<NewLessonMessageResponce> { get }
    var viewersCount: PublishSubject<ParticipantsCountModel> { get }
    var dataChannel: PublishSubject<DataChannelResponce> { get }
    var isConnected: PublishSubject<SocketConnection> { get }
    var likesCount: PublishSubject<Int> { get }
    var roomId: String { get }

    func fetchMessages()
    func moreMessages(length: Int)
    func newMessage(unauthUser: String, message: String, gift: Int?)
    func newCustomMessage(message: [String: Bool])
    func disconnect()
}

struct SocketLessonChatServiceFactory {
    static func make(roomId: String) -> SocketLessonChatSrevice {
        let manager = SocketClientManager(with: .classRoom(roomId: roomId))
        return SocketLessonChatServiceImpl(manager: manager, roomId: roomId)
    }
}

class SocketLessonChatServiceImpl: SocketLessonChatSrevice {

    // MARK: - Public Properties

    private(set) var messages = PublishSubject<LessonMessagesResponce>()
    private(set) var moreMessages = PublishSubject<LessonMessagesResponce>()
    private(set) var newMessage = PublishSubject<NewLessonMessageResponce>()
    private(set) var viewersCount = PublishSubject<ParticipantsCountModel>()
    private(set) var dataChannel = PublishSubject<DataChannelResponce>()
    private(set) var isConnected = PublishSubject<SocketConnection>()
    private(set) var likesCount = PublishSubject<Int>()
    var roomId: String

    // MARK: - Private Properties

    private let manager: SocketClientManager
    private let disposeBag = DisposeBag()

    // MARK: - Lifecycle

    init(manager: SocketClientManager, roomId: String) {
        self.manager = manager
        self.roomId = roomId
        bind()
    }

    deinit {
        manager.disconnect()
    }

    // MARK: - Public Methods

    func fetchMessages() {
        let request = FetchLessonMessagesRequest()
        manager.send(message: request)
    }

    func moreMessages(length: Int) {
        let request = MoreLessonMessagesRequest(length: length)
        manager.send(message: request)
    }

    func newMessage(unauthUser: String, message: String, gift: Int?) {
        let request = NewLessonChatMessageRequest(
            command: .newMessage,
            message: message,
            userId: UserDefaultsHelper.shared.userId,
            unauthUser: unauthUser,
            gift: gift)
        manager.send(message: request)
    }

    func newCustomMessage(message: [String: Bool]) {
        let request = DataChannelRequest(data: [], anotherData: message)
        manager.send(message: request)
    }

    func disconnect() {
        manager.disconnect()
    }

    // MARK: - Private Methods

    private func bind() {

        manager
            .isConnected
            .bind(to: isConnected)
            .disposed(by: disposeBag)

        let decoded = manager.message
            .compactMap { SocketResponseMapper.decodeEvent(message: $0) }
            .share()

        decoded.compactMap { $0 as? LessonMessagesResponce }
            .bind(to: messages)
            .disposed(by: disposeBag)

        decoded.compactMap { $0 as? NewLessonMessageResponce }
            .bind(to: newMessage)
            .disposed(by: disposeBag)

        decoded.compactMap { $0 as? ParticipantsCountModel }
            .bind(to: viewersCount)
            .disposed(by: disposeBag)

        decoded.compactMap { $0 as? DataChannelResponce }
            .bind(to: dataChannel)
            .disposed(by: disposeBag)
        
        decoded.compactMap { $0 as? LikesCountModel }
            .map { $0.count ?? $0.likes ?? 0 }
            .bind(to: likesCount)
            .disposed(by: disposeBag)
    }
}
