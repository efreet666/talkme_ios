//
//  LessonMessage.swift
//  talkme_ios
//
//  Created by 1111 on 14.07.2021.
//

import Foundation

struct LessonMessage: Decodable {
    let author: String
    let content: String
    let gift: String?
    let timestamp: TimeInterval
    let authorId: Int
    let avatar: String?

    enum CodingKeys: String, CodingKey {
        case author, content, gift, timestamp, avatar
        case authorId = "author_id"
    }
}

struct LessonChatMessage {
    let message: LessonMessage
    let isNew: Bool
}
