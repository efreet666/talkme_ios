//
//  DataChannelResponce.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 10.08.2021.
//

import Foundation

struct DataChannelResponce: Codable {
    let data: DataChannelModel
}

struct DataChannelModel: Codable {
    let anotherData: [String: Bool]

    enum CodingKeys: String, CodingKey {
        case anotherData = "another_data"
    }
}
