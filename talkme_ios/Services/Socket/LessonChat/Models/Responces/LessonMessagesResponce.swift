//
//  LessonMessagesResponce.swift
//  talkme_ios
//
//  Created by 1111 on 14.07.2021.
//

import Foundation

struct LessonMessagesResponce: Decodable {
    let messages: [LessonMessage]
}
