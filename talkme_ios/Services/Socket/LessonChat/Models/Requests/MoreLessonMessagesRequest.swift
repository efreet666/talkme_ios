//
//  MoreLessonMessagesRequest.swift
//  talkme_ios
//
//  Created by 1111 on 14.07.2021.
//

import Foundation

struct MoreLessonMessagesRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .moreMessages
    let length: Int
}
