//
//  FetchLessonMessagesRequest.swift
//  talkme_ios
//
//  Created by 1111 on 14.07.2021.
//

import Foundation

struct FetchLessonMessagesRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .fetchMessages
}
