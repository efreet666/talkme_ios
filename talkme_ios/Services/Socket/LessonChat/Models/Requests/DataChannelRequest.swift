//
//  DataChannelRequest.swift
//  talkme_ios
//
//  Created by Maxim Drachev on 10.08.2021.
//

import Foundation

struct DataChannelRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .customChannel
    let data: [Int]
    let anotherData: [String: Bool]

    enum CodingKeys: String, CodingKey {
        case command
        case data
        case anotherData = "another_data"
    }
}
