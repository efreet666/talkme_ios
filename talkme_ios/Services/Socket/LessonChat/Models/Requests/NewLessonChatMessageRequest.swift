//
//  NewLessonChatMessageRequest.swift
//  talkme_ios
//
//  Created by 1111 on 14.07.2021.
//

import Foundation

struct NewLessonChatMessageRequest: SocketRequest {
    private(set) var command: SocketRequestCommand = .newMessage
    let message: String
    let userId: Int
    let unauthUser: String
    let gift: Int?

    enum CodingKeys: String, CodingKey {
        case command
        case message
        case gift
        case userId = "user_id"
        case unauthUser = "unauth_user"
    }
}
