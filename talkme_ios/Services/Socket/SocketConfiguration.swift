//
//  SocketConfiguration.swift
//  talkme_ios
//
//  Created by Den on 06.01.2021.
//

import Foundation

enum SocketConfiguration {
    case chats
    case cameras(roomId: String)
    case chat(roomId: String)
    case classRoom(roomId: String)
    case online

    // MARK: - Public Properties

    var url: URL {
        let url: URL?
        switch self {
        case .chats:
            url = URL(string: "\(urlPrefix)chats/")
        case .cameras(let roomId):
            url = URL(string: "\(urlPrefix)cameras/\(roomId)/")
        case .chat(let roomId):
            url = URL(string: "\(urlPrefix)chat/\(roomId)/")
        case .classRoom(let roomId):
            url = URL(string: "\(urlPrefix)class/\(roomId)/")
        case .online:
            url = URL(string: "\(urlPrefix)online/")
        }

        guard let finalUrl = url else {
            assertionFailure("Cannot create socket connection url")
            return URL(fileURLWithPath: "")
        }
        return finalUrl
    }

    // MARK: - Private Properties

    private var urlPrefix: String { "\(Configuration.wsDomain)" }
}
