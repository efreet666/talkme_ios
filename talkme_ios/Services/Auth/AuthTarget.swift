//
//  NetworkTarget.swift
//  talkme_ios
//
//  Created by 1111 on 14.12.2020.
//

import Moya

enum AuthTarget {
    case login(LoginRequest)
    case signUp(SignUpRequest)
    case getMobileCode(SendMobileRequest)
    case sendMobileAndCode(SendMobileAndCodeRequest)
    case vkSignUp(SocialSignUpRequest)
    case facebookSignUp(SocialSignUpRequest)
    case mailRuSignUp(SocialSignUpRequest)
    case yandexSignUp(SocialSignUpRequest)
    case googleSignUp(SocialSignUpRequest)
    case appleIDSignUp(SocialSignUpRequest)
    case passowrdChangeConfirm(MobilePasswordConfirmRequest)
    case resetPassword(SendMobileAndCodeRequest)
    case resetPasswordEmail(ResetPasswordRequest)
}

extension AuthTarget: TargetType {

    static let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }()

    var baseURL: URL { Configuration.baseUrl }

    var path: String {
        switch self {
        case .login:
            return "users/login/"
        case .signUp:
            return "users/signup/"
        case .getMobileCode:
            return "users/mobile_verify/"
        case .sendMobileAndCode:
            return "users/mobile_verify/"
        case .vkSignUp:
            return "users/auth/vk/"
        case .facebookSignUp:
            return "users/auth/facebook"
        case .mailRuSignUp:
            return "users/auth/mailru/"
        case .yandexSignUp:
            return "users/auth/yandex/"
        case .googleSignUp:
            return "users/auth/google/"
        case .appleIDSignUp:
            return "users/auth/apple"
        case .resetPassword:
            return "/users/mobile/password/reset/"
        case .passowrdChangeConfirm:
            return "/users/mobile/password/confirm/"
        case .resetPasswordEmail:
            return "/users/password/reset/"
        }
    }

    var method: Moya.Method { .post }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .login(let request):
            return .requestCustomJSONEncodable(request, encoder: AuthTarget.encoder)
        case .signUp(let request):
            return .requestCustomJSONEncodable(request, encoder: AuthTarget.encoder)
        case .getMobileCode(let request):
            let data = try? AuthTarget.encoder.encode(request)
            return .requestCompositeData(bodyData: data ?? Data(), urlParameters: ["call": true])
        case .sendMobileAndCode(let request):
            let data = try? AuthTarget.encoder.encode(request)
            return .requestCompositeData(bodyData: data ?? Data(), urlParameters: ["call": true])
        case .vkSignUp(let request), .facebookSignUp(let request), .yandexSignUp(let request),
             .mailRuSignUp(let request), .googleSignUp(let request), .appleIDSignUp(let request):
            return .requestCustomJSONEncodable(request, encoder: AuthTarget.encoder)
        case .resetPassword(let request):
            let data = try? AuthTarget.encoder.encode(request)
            return .requestCompositeData(bodyData: data ?? Data(), urlParameters: ["call": true])
        case .passowrdChangeConfirm(let request):
            return .requestCustomJSONEncodable(request, encoder: AuthTarget.encoder)
        case .resetPasswordEmail(let email):
            return .requestJSONEncodable(email)
        }
    }

    var headers: [String: String]? {
        return [
            "Content-Type": "application/json"
        ]
    }
}
