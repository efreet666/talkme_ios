//
//  LoginModels.swift
//  talkme_ios
//
//  Created by Elina Efremova on 28.12.2020.
//

struct LoginRequest: Encodable {
    let emailOrMobile: String
    let password: String
    let rememberMe: Bool
}

struct LoginResponse: Decodable {
    let id: Int?
    let email: String?
    let mobile: String?
    let firstName: String?
    let lastName: String?
    let avatarUrl: String?
    let authToken: Token?

    let errorMessages: [String]?

    enum CodingKeys: String, CodingKey {
        case id
        case email
        case mobile
        case firstName
        case lastName
        case authToken
        case avatarUrl
    }

    init(from decoder: Decoder) throws {
        if let keyedContainer = try? decoder.container(keyedBy: CodingKeys.self) {
            errorMessages = nil
            authToken = try keyedContainer.decodeIfPresent(Token.self, forKey: .authToken)
            id = try keyedContainer.decodeIfPresent(Int.self, forKey: .id)
            email = try keyedContainer.decodeIfPresent(String.self, forKey: .email)
            mobile = try keyedContainer.decodeIfPresent(String.self, forKey: .mobile)
            firstName = try keyedContainer.decodeIfPresent(String.self, forKey: .firstName)
            lastName = try keyedContainer.decodeIfPresent(String.self, forKey: .lastName)
            avatarUrl = try keyedContainer.decodeIfPresent(String.self, forKey: .avatarUrl)
        } else {
            let unkeyedContainer = try? decoder.singleValueContainer()
            errorMessages = try unkeyedContainer?.decode([String].self)
            authToken = nil
            id = nil
            email = nil
            mobile = nil
            firstName = nil
            lastName = nil
            avatarUrl = nil
        }
    }
}

struct Token: Decodable {
    let refresh: String
    let access: String
}
