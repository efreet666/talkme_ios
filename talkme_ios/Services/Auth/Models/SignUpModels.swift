//
//  SignUpModels.swift
//  talkme_ios
//
//  Created by 1111 on 14.12.2020.
//

struct SignUpRequest: Encodable {
    let firstName: String?
    let lastName: String?
    let mobile: String?
    let email: String?
    let password: String?

    init(
        firstName: String? = nil,
        lastName: String? = nil,
        mobile: String? = nil,
        email: String? = nil,
        password: String? = nil
    ) {
        self.firstName = firstName
        self.lastName = lastName
        self.mobile = mobile
        self.email = email
        self.password = password
    }
}

struct SignUpResponse: Codable {
    let authToken: MyToken?
    let password: [String]?
    let msg: String?
}

struct SignUpErrorResponse: Decodable {
    let email: [String]
}

struct MyToken: Codable {
    let refresh: String?
    let access: String?
}

struct SocialSignUpRequest: Encodable {
    let accessToken: String
    let code: String?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case code
    }
}

struct SocialSignUpResponse: Decodable {
    let token: String
    let user: User
}

struct User: Decodable {
    let email: String?
    let firstName: String
    let lastName: String
    let avatarUrl: String?
    let username: String
    let numberClass: String?
    let isSpecialist: Bool?
    let id: Int
    let rating: Float
    let hasEcommpayToken: Bool?

    enum CodingKeys: String, CodingKey {
        case email
        case firstName = "first_name"
        case lastName = "last_name"
        case avatarUrl = "avatar_url"
        case username
        case numberClass = "number_class"
        case isSpecialist = "is_specialist"
        case id
        case rating
        case hasEcommpayToken = "has_ecommpany_token"
    }
}

struct MailRuResponesToken: Decodable {
    let accessToken: String
    let expiresIn: Int
    let refreshToken: String

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case refreshToken = "refresh_token"

    }
}
