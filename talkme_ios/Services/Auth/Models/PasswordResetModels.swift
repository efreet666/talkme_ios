//
//  PasswordResetModels.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 06.04.2021.
//

struct ResetPasswordRequest: Encodable {
    let email: String
}
