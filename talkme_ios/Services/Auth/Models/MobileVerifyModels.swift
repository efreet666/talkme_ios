//
//  MobileVerifyModels.swift
//  talkme_ios
//
//  Created by Elina Efremova on 28.12.2020.
//

struct SendMobileRequest: Encodable {
    let mobile: String
}

struct SendMobileAndCodeRequest: Encodable {
    let code: String?
    let mobile: String
}

struct MobileVerifyResponse: Decodable {
    let mobile: String?
    let user: String?
    let msg: String?
}

struct MobilePasswordConfirmRequest: Encodable {
    let user: String
    let newPassword: String
    let newRepeatPassword: String

    enum CodingKeys: String, CodingKey {
        case user
        case newPassword = "new_password1"
        case newRepeatPassword = "new_password2"
    }
}
