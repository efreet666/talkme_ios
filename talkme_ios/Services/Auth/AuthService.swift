//
//  NetworkService.swift
//  talkme_ios
//
//  Created by 1111 on 14.12.2020.
//

import Moya
import RxSwift

protocol AuthServiceProtocol {
    func login(request: LoginRequest) -> Single<LoginResponse>
    func loginVK(request: SocialSignUpRequest) -> Single<SocialSignUpResponse>
    func loginGoogle(request: SocialSignUpRequest) -> Single<SocialSignUpResponse>
    func loginMailRu(request: SocialSignUpRequest) -> Single<SocialSignUpResponse>
    func loginYandex(request: SocialSignUpRequest) -> Single<SocialSignUpResponse>
    func signUp(request: SignUpRequest) -> Single<SignUpResponse>
    func getMobileCode(request: SendMobileRequest) -> Single<MobileVerifyResponse>
    func sendMobileAndCode(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse>
    func socialSignUp(type: AuthTarget, request: SocialSignUpRequest) -> Single<SocialSignUpResponse>
    func passwordChangeConfirm(request: MobilePasswordConfirmRequest) -> Single<MobileVerifyResponse>
    func resetPassword(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse>
    func resetPasswordEmail(request: ResetPasswordRequest) -> Maybe<(text: String, isEmail: Bool)>
    func checkEmail(request: SignUpRequest) -> Maybe<(text: String, isEmail: Bool)>
}

final class AuthService: AuthServiceProtocol {

    // MARK: - Private Properties

    private let isLoggingEnabled = true

    private lazy var plugins: [NetworkLoggerPlugin] = {
        return isLoggingEnabled
        ? [NetworkLoggerPlugin(configuration: .init( logOptions: [.verbose, .formatRequestAscURL]))]
            : []
    }()

    private lazy var provider = MoyaProvider<AuthTarget>(plugins: plugins)

    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    // MARK: - Public Methods
    func userDefaultHelper(refreshToken: String?, accessToken: String?, isLoggedfromSocials: Bool) {
        UserDefaultsHelper.shared.isLoggedfromSocials = isLoggedfromSocials
        UserDefaultsHelper.shared.refreshToken = refreshToken ?? ""
        UserDefaultsHelper.shared.accessToken = accessToken ?? ""
        UserDefaultsHelper.shared.tokBoxUniqueId = UUID().uuidString
        UserDefaultsHelper.shared.isBlockedVideoByPub = false
        UserDefaultsHelper.shared.isBlockedAudioByPub  = false
        UserDefaultsHelper.shared.blockCameraByMyself = true
        UserDefaultsHelper.shared.blockMicrophoneByMyself = true
    }
    func login(request: LoginRequest) -> Single<LoginResponse> {
        provider.rx.request(.login(request))
            .map(LoginResponse.self, using: decoder)
            .do(onSuccess: { response in
                self.userDefaultHelper(refreshToken: response.authToken?.refresh, accessToken: response.authToken?.access, isLoggedfromSocials: false)
            })
    }

    func loginVK(request: SocialSignUpRequest) -> Single<SocialSignUpResponse> {
        provider.rx.request(.vkSignUp(request))
            .map(SocialSignUpResponse.self)
            .do(onSuccess: { response in
                self.userDefaultHelper(refreshToken: nil, accessToken: response.token, isLoggedfromSocials: true)
                UserDefaultsHelper.shared.userId = response.user.id
            }, onError: { error in
                print("VK REGISTRATION FAILED\(error.localizedDescription)")
            })
    }

    func loginGoogle(request: SocialSignUpRequest) -> Single<SocialSignUpResponse> {
        provider.rx.request(.googleSignUp(request))
            .map(SocialSignUpResponse.self)
            .do(onSuccess: { response in
                self.userDefaultHelper(refreshToken: nil, accessToken: response.token, isLoggedfromSocials: true)
                UserDefaultsHelper.shared.userId = response.user.id
            }, onError: { error in
                print("Google REGISTRATION FAILED\(error.localizedDescription)")
            })
    }

    func loginYandex(request: SocialSignUpRequest) -> Single<SocialSignUpResponse> {
        provider.rx.request(.yandexSignUp(request))
            .map(SocialSignUpResponse.self)
            .do(onSuccess: { response in
                self.userDefaultHelper(refreshToken: nil, accessToken: response.token, isLoggedfromSocials: true)
                UserDefaultsHelper.shared.userId = response.user.id
            }, onError: { error in
                print("Yandex REGISTRATION FAILED\(error.localizedDescription)")
            })
    }

    func loginMailRu(request: SocialSignUpRequest) -> Single<SocialSignUpResponse> {
        provider.rx.request(.mailRuSignUp(request))
            .map(SocialSignUpResponse.self)
            .do(onSuccess: { response in
                self.userDefaultHelper(refreshToken: nil, accessToken: response.token, isLoggedfromSocials: true)
                UserDefaultsHelper.shared.userId = response.user.id
            }, onError: { error in
                print("MailRu REGISTRATION FAILED\(error.localizedDescription)")
            })
    }

    func signUp(request: SignUpRequest) -> Single<SignUpResponse> {
        provider
            .rx
            .request(.signUp(request))
            .map(SignUpResponse.self, using: decoder)
            .do { response in
                guard let refresh = response.authToken?.refresh,
                      let access = response.authToken?.access else {
                    return
                }
                self.userDefaultHelper(refreshToken: refresh, accessToken: access, isLoggedfromSocials: false)
            }
    }

    func getMobileCode(request: SendMobileRequest) -> Single<MobileVerifyResponse> {
        provider.rx.request(.getMobileCode(request))
            .map(MobileVerifyResponse.self, using: decoder)
    }

    func sendMobileAndCode(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse> {
        provider.rx.request(.sendMobileAndCode(request))
            .map(MobileVerifyResponse.self, using: decoder)
    }

    func resetPassword(request: SendMobileAndCodeRequest) -> Single<MobileVerifyResponse> {
        provider.rx.request(.resetPassword(request))
            .map(MobileVerifyResponse.self)
    }

    func passwordChangeConfirm(request: MobilePasswordConfirmRequest) -> Single<MobileVerifyResponse> {
        provider.rx.request(.passowrdChangeConfirm(request))
            .map(MobileVerifyResponse.self)
    }

    func socialSignUp(type: AuthTarget, request: SocialSignUpRequest) -> Single<SocialSignUpResponse> {
        switch type {
        case .vkSignUp:
            return provider.rx.request(.vkSignUp(request)).map(SocialSignUpResponse.self, using: decoder)
        case .facebookSignUp:
            return provider.rx.request(.facebookSignUp(request)).map(SocialSignUpResponse.self, using: decoder)
        case .mailRuSignUp:
            return provider.rx.request(.mailRuSignUp(request)).map(SocialSignUpResponse.self, using: decoder)
        case .yandexSignUp:
            return provider.rx.request(.yandexSignUp(request)).map(SocialSignUpResponse.self, using: decoder)
        case .googleSignUp:
            return provider.rx.request(.googleSignUp(request)).map(SocialSignUpResponse.self, using: decoder)
        case .appleIDSignUp:
            return provider.rx.request(.appleIDSignUp(request)).map(SocialSignUpResponse.self, using: decoder)
        default:
            return provider.rx.request(.appleIDSignUp(request)).map(SocialSignUpResponse.self, using: decoder)
        }
    }

    func resetPasswordEmail(request: ResetPasswordRequest) -> Maybe<(text: String, isEmail: Bool)> {
        provider
            .rx
            .request(.resetPasswordEmail(request))
            .compactMap { response -> (text: String, isEmail: Bool)? in
                switch response.statusCode {
                case 200:
                    return (text: request.email, isEmail: true)
                case 400:
                    guard
                        let responseBody = try? response.map(SignUpErrorResponse.self),
                        let message = responseBody.email.first else { return nil }
                    return (text: message, isEmail: false)
                default:
                    return nil
                }
            }
    }

    func checkEmail(request: SignUpRequest) -> Maybe<(text: String, isEmail: Bool)> {
        provider
            .rx
            .request(.signUp(request))
            .compactMap { response -> (text: String, isEmail: Bool)? in
                switch response.statusCode {
                case 500:
                    guard let email = request.email else { return nil }
                    return (text: email, isEmail: true)
                case 400:
                    guard
                        let responseBody = try? response.map(SignUpErrorResponse.self),
                        let message = responseBody.email.first else { return nil }
                    return (text: message, isEmail: false)
                default:
                    return nil
                }
            }
    }
}
