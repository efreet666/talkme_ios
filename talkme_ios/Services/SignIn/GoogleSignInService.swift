//
//  GoogleSignInService.swift
//  testProject
//
//  Created by Алексей Смицкий on 20.02.2021.
//

import GoogleSignIn

// todo: Will need to be refactored when app will be registered in SDK
final class GoogleSignInService: NSObject, GIDSignInDelegate {

    // MARK: - Private properties

    private let clientID = "1085956071228-645mq82090mti7hub6pl1gn1fim0rim2.apps.googleusercontent.com"
    var completion: ((String, String) -> Void)?

    // MARK: - Init

    override init() {
        super.init()
        setupLogin()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public methods

    func signIn() {
        GIDSignIn.sharedInstance()?.signIn()
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            let idToken = user.authentication.idToken ?? ""
            let token = user.authentication.accessToken ?? ""
            completion?(token, idToken)
        } else {
            print("Google sign ERROR:\(error.localizedDescription)")
        }
    }

    // MARK: - Private methods

    private func setupLogin() {
        GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.keyWindow?.rootViewController
        GIDSignIn.sharedInstance()?.clientID = clientID
        GIDSignIn.sharedInstance()?.delegate = self
    }
}
