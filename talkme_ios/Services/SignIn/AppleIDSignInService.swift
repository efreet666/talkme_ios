//
//  AppleIDSignInService.swift
//  talkme_ios
//
//  Created by Алексей Смицкий on 20.02.2021.
//

import AuthenticationServices

// todo: Will need to be refactored when app will be ready for use AppleID signIn
final class AppleIDSignInService: NSObject {

    // MARK: - Public methods

    @available(iOS 13.0, *)
    func signIn() {
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()
        request.requestedScopes = [.fullName, .email]

        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.presentationContextProvider = self
        controller.performRequests()
    }
}

@available(iOS 13.0, *)
extension AppleIDSignInService: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("failed!")
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {

        switch authorization.credential {
        case let credintials as ASAuthorizationAppleIDCredential:
            let firstname = credintials.fullName?.givenName ?? ""
            let lastname = credintials.fullName?.familyName ?? ""
            let email = credintials.email ?? ""
            print(firstname, lastname, email)
        default:
            break
        }
    }
}

@available(iOS 13.0, *)
extension AppleIDSignInService: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        guard let view = UIApplication.shared.keyWindow?.rootViewController?.view.window else { return ASPresentationAnchor() }
        return view
    }
}
