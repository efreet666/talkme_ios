//
//  FacebookSignInService.swift
//  testProject
//
//  Created by Алексей Смицкий on 20.02.2021.
//

import FBSDKLoginKit

// todo: Will need to be refactored when app will be registered in SDK
final class FacebookSignInService {

    // MARK: - Public methods

    static func signIn() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile], viewController: UIApplication.shared.keyWindow?.rootViewController) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions,
                          let declinedPermissions,
                          let accessToken):
                print(accessToken?.tokenString ?? "")
                print(grantedPermissions)
                print(declinedPermissions)
            }
        }
    }
}
