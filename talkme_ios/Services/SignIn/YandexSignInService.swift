//
//  YandexSignInService.swift
//  talkme_ios
//
//  Created by Vladislav on 31.08.2021.
//

import YandexLoginSDK

final class YandexSignInService: NSObject, YXLObserver {

    // MARK: - Private properties

    private var completion: ((String, String) -> Void)?

    // MARK: - Public methods

    func signIn(completion: ((String, String) -> Void)?) {
        self.completion = completion
        YXLSdk.shared.logout()
        YXLSdk.shared.authorize()
        YXLSdk.shared.add(observer: self)
    }

    func loginDidFinish(with result: YXLLoginResult) {
        let token = result.token
        let code = result.jwt
        completion?(token, code)
    }

    func loginDidFinishWithError(_ error: Error) {
        print(error)
    }

    func logout() {
        YXLSdk.shared.logout()
    }
}
