//
//  VkSwiftySignInService.swift
//  talkme_ios
//
//  Created by Vladislav on 19.08.2021.
//
import SwiftyVK

final class VKSwiftySignInService: NSObject, SwiftyVKDelegate {
    private let appId = "7873801"
    private let scopes: Scopes = [.offline, .email]

    override init() {
        super.init()
    }

    func vkNeedsScopes(for sessionId: String) -> Scopes {
        return scopes
    }

    func vkNeedToPresent(viewController: VKViewController) {
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            rootController.present(viewController, animated: true)
        }
    }

    func authorize(completion: ((String, String) -> Void)?) {
        VK.setUp(appId: appId, delegate: self)
        VK.sessions.default.logOut()
        VK.sessions.default.logIn(
            onSuccess: { info in
                print("SwiftyVK: success authorize with", info)
                let token = String(info["access_token"] ?? "")
                let userId = String(info["user_id"] ?? "")
                completion!(token, userId)
            },
            onError: { error in
                print("SwiftyVK: authorize failed with", error)
            }
        )
    }

    func vkTokenCreated(for sessionId: String, info: [String: String]) {
        print("SwiftyVK: token created in session \(sessionId) with info \(info)")
    }

    func vkTokenUpdated(for sessionId: String, info: [String: String]) {
        print("SwiftyVK: token updated in session \(sessionId) with info \(info)")
    }

    func vkTokenRemoved(for sessionId: String) {
        print("SwiftyVK: token removed in session \(sessionId)")
    }

}
