//
//  MailRuSignInService.swift
//  talkme_ios
//
//  Created by Vladislav on 31.08.2021.
//

import MRMailSDK
import WebKit

final class MailRuSignInService: NSObject, MRMailSDKDelegate {

    // MARK: - Public Properties

    var completion: ((String, String) -> Void)?
    var code = ""

    // MARK: - Public Methods

    override init() {
        super.init()
    }

    func signIn(completion: ((String, String) -> Void)?) {
        self.completion = completion
        MRMailSDK.sharedInstance().delegate = self
        MRMailSDK.sharedInstance().forceLogout()
        MRMailSDK.sharedInstance().authorize()
    }

    func mrMailSDK(_ sdk: MRMailSDK, authorizationDidFinishWith result: MRSDKAuthorizationResult) {
        self.code = result.authorizationCode ?? ""
        if result.authorizationCode != nil {
            request(authorizationCode: result.authorizationCode!)
        }
    }

    func request(authorizationCode: String) {
        let parameters: [String: String] = [
            "client_id": "89f4563a3c4d4987932b547175be7668",
            "grant_type": "authorization_code",
            "code": "\(authorizationCode)",
            "redirect_uri": "talk-me-auth-callback://",
            "client_secret": "cf376744ae3e4f68929dc4c07b7bdd9d"
        ]
        let url = String(format: "https://oauth.mail.ru/token/")
        guard let serviceUrl = URL(string: url) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        do {
            request.httpBody = parameters.percentEncoded()
        }
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let token = try decoder.decode(MailRuResponesToken.self, from: data)
                    self.completion?(token.accessToken, self.code)
                } catch {
                    print("MailRu: Decode Error ")
                }
            }
            if let error = error {
                print("MailRu: Error request - \(error)")
            }
        }.resume()
    }

    func mrMailSDK(_ sdk: MRMailSDK, authorizationDidFinishWithCode code: String) {
        print(code)
    }

    func mrMailSDK(_ sdk: MRMailSDK, authorizationDidFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}
