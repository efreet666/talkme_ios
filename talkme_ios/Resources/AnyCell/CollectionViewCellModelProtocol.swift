//
//  CollectionViewCellModelProtocol.swift
//  talkme_ios
//
//  Created by 1111 on 18.01.2021.
//

import UIKit

protocol AnyCollectionViewCellModelProtocol: AnyCellViewModelProtocol {

    func configureAny(_ cell: UICollectionViewCell)
}

protocol CollectionViewCellModelProtocol: AnyCollectionViewCellModelProtocol {

    associatedtype CellType: UICollectionViewCell

    func configure(_ cell: CellType)
}

extension CollectionViewCellModelProtocol {

    static var cellClass: UIView.Type {
        return CellType.self
    }

    func configureAny(_ cell: UICollectionViewCell) {
        guard let cell = cell as? CellType else {
            assertionFailure()
            return
        }
        configure(cell)
    }
}
