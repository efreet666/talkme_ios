//
//  AnyViewModelProtocol.swift
//  talkme_ios
//
//  Created by Elina Efremova on 13.01.2021.
//

import UIKit

protocol AnyViewModelProtocol {
    static var viewClass: UIView.Type { get }
}

extension AnyViewModelProtocol {

    private static var cellClassName: String {
        return String(describing: viewClass)
    }
}
