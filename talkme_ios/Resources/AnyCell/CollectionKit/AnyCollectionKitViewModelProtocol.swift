//
//  AnyCollectionKitViewModelProtocol.swift
//  talkme_ios
//
//  Created by Elina Efremova on 13.01.2021.
//

import UIKit

protocol AnyCollectionKitViewModelProtocol: AnyViewModelProtocol {

    func configureAny(_ view: UIView)
}

protocol CollectionKitViewModelProtocol: AnyCollectionKitViewModelProtocol {

    associatedtype ViewType: UIView

    func configure(_ view: ViewType)
}

extension CollectionKitViewModelProtocol {

    static var viewClass: UIView.Type {
        return ViewType.self
    }

    func configureAny(_ view: UIView) {
        guard let view = view as? ViewType else {
            assertionFailure()
            return
        }
        configure(view)
    }
}
