//
//  AnyCellViewModelProtocol.swift
//  talkme_ios
//
//  Created by Yura Fomin on 21.12.2020.
//

import UIKit

protocol AnyCellViewModelProtocol: class {

    static var cellClass: UIView.Type { get }
    static var reuseIdentifier: String { get }
}

extension AnyCellViewModelProtocol {

    private static var cellClassName: String {
        return String(describing: cellClass)
    }

    static var reuseIdentifier: String {
        return cellClassName
    }
}
