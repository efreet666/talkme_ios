//
//  Gradients.swift
//  talkme_ios
//
//  Created by Майя Калицева on 16.02.2021.
//

import UIKit

final class Gradients: CAGradientLayer {

    enum GradientType {
        case stream
        case popular
        case nearest
        case live

        var startPoint: CGPoint {
            switch self {
            case .popular, .nearest, .live:
                return CGPoint(x: 0, y: 1)
            case .stream:
                return CGPoint(x: 0.25, y: 0.5)
            }
        }

        var endPoint: CGPoint {
            switch self {
            case .popular, .nearest, .live:
                return CGPoint(x: 0.15, y: 0)
            case .stream:
                return CGPoint(x: 0.75, y: 0.5)
            }
        }

        var locations: [NSNumber]? {
            switch self {
            case .nearest:
                return [0.25, 0.81]
            case .popular, .live:
                return [0.25, 0.71]
            case .stream:
                return [0, 1]
            }
        }

        var startColor: UIColor {
            switch self {
            case .nearest:
                return UIColor(red: 0.244, green: 0.292, blue: 0.458, alpha: 1)
            case .stream:
                return TalkmeColors.deepBlue
            case .live, .popular:
                return UIColor(red: 0.136, green: 0.143, blue: 0.175, alpha: 0.8)
            }
        }

        var endColor: UIColor {
            switch self {
            case .nearest:
                return UIColor(red: 0.165, green: 0.174, blue: 0.217, alpha: 0.04)
            case .stream:
                return TalkmeColors.blueLabels
            case .popular, .live:
                return UIColor(red: 0.165, green: 0.174, blue: 0.217, alpha: 0.04)
            }
        }

        var cornerRadius: CGFloat {
            switch self {
            case .popular, .nearest, .live, .stream:
                return 12
            }
        }
    }

    // MARK: - Lifecycle

    override init() {
        super.init()
    }

    convenience init(_ type: GradientType) {
        self.init()
        startPoint = type.startPoint
        endPoint = type.endPoint
        locations = type.locations
        let startColor = type.startColor.cgColor
        let endColor = type.endColor.cgColor
        colors = [startColor, endColor]
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CAGradientLayer {
    enum Points {
        case nearestStart
        case nearestEnd
        case liveStart
        case liveEnd
        case popularStart
        case popularEnd
        case streamStart
        case streamEnd
        case allTeacherStart
        case allTeacherEnd

        var point: CGPoint {
            switch self {
            case .nearestStart:
                return CGPoint(x: 0, y: 1)
            case .liveStart, .popularStart:
                return CGPoint(x: 1, y: 1)
            case .nearestEnd:
                return CGPoint(x: 0.15, y: 0)
            case .liveEnd, .popularEnd:
                return CGPoint(x: 1, y: 0)
            case .streamStart, .allTeacherStart:
                return CGPoint(x: 0.25, y: 0.5)
            case .streamEnd, .allTeacherEnd:
                return CGPoint(x: 0.75, y: 0.5)
            }
        }
    }

    convenience init(start: Points, end: Points, colors: [CGColor], locations: [NSNumber]) {
        self.init()
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors
        self.locations = locations
        self.type = type
    }
}
