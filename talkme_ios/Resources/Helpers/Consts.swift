//
//  Consts.swift
//  talkme_ios
//
//  Created by Владимир Олейников on 19/7/2022.
//

import Foundation

enum Consts {
    static let liveStreamsStubbed = false
    static let isSwitchUrlEnable = true
}
