//
//  Color.swift
//  talkme_ios
//
//  Created by Yura Fomin on 16.12.2020.
//

import UIKit

enum TalkmeColors {
    // Registration
    /// #ECF8FF
    static let lightGreyView = UIColor(hex: 0xECF8FF)
    /// #D8E7F2
    static let separatorView = UIColor(hex: 0xD8E7F2)
    /// #484C60
    static let codeCountry = UIColor(hex: 0x484C60)
    /// #FFFFFF
    static let white = UIColor(hex: 0xFFFFFF)
    /// #A9DBFA
    static let noActivatedButton = UIColor(hex: 0xA9DBFA)
    /// #FE6868
    static let warningColor = UIColor(hex: 0xFE6868)
    /// #0098FF
    static let shadow = UIColor(hex: 0x0098FF)
    /// #FF9090
    static let error = UIColor(hex: 0xFF9090)
    /// #3B49B7
    static let symbolView = UIColor(hex: 0x3B49B7)
    /// #030303
    static let black = UIColor(hex: 0x030303)
    /// #b7dfff
    static let lightBlue = UIColor(hex: 0xb7dfff)
    /// #3B49B7
    static let deepBlue = UIColor(hex: 0x3B49B7)

    // Account
    /// #EFF5FE
    static let lightBlueView = UIColor(red: 0.937, green: 0.96, blue: 0.996, alpha: 1)
    /// #484C60
    static let blackLabels = UIColor(hex: 0x484C60)
    /// #0198FF
    static let blueLabels = UIColor(hex: 0x0198FF)
    /// #5ACD49
    static let greenLabels = UIColor(hex: 0x5ACD49)
    /// #EAEEFF
    static let classView = UIColor(hex: 0xEAEEFF)
    /// #FFBA21
    static let ratingViewYellow = UIColor(hex: 0xFFBA21)
    /// #F6F7FB
    static let mainAccountBackground = UIColor(hex: 0xF6F7FB)
    /// #6C809F
    static let grayLabels = UIColor(hex: 0x6C809F)
    // ProfileMenu
    /// #FF003D
    static let redLabels = UIColor(hex: 0xFF003D)
    /// #1D71DB
    static let blueClassView = UIColor(hex: 0x1D71DB)
    /// #484С60
    static let lightBlackBalance = UIColor(hex: 0x484C60)
    /// #484С60, alpha: 0.8
    static let lightBlackBalanceWithAlpha = UIColor(hex: 0x484C60, alpha: 0.8)
    /// #242630 85%
    static let popupBackground = UIColor(hex: 0x242630, alpha: 0.85)
    /// #8C35FA
    static let purpleColor = UIColor(hex: 0x8C35FA)
    /// #F4F6FF
    static let buttonGrayColor = UIColor(hex: 0xF4F6FF)
    /// #9A9FB8
    static let etherDataColor = UIColor(hex: 0x9A9FB8)
    /// #F2F9FF
    static let liveStreamMembersBackground = UIColor(hex: 0xF2F9FF)
    /// #242630
    static let lightBlack = UIColor(hex: 0x242630)
    /// #566E94
    static let grayLabel = UIColor(hex: 0x566E94)
    /// #3C3C43
    static let separatorGray = UIColor(hex: 0x3C3C43)
    /// #FFBA21
    static let orangeViewBackground = UIColor(hex: 0xFFBA21)
    /// #949AB9
    static let placeholderColor = UIColor(hex: 0x949AB9)
    /// #D3D8EA
    static let grayButtonBorder = UIColor(hex: 0xD3D8EA)
    /// #879FC3
    static let grayDescription = UIColor(hex: 0x879FC3)
    // lessonsCollectionItem
    /// #DFFCEE
    static let greenPriceLabel = UIColor(hex: 0xDFFCEE)
    /// #FE6868
    static let redLessonLabel = UIColor(hex: 0xFE6868)
    /// #E9ECFC
    static let graySearchBar = UIColor(hex: 0xE9ECFC)
    /// #ECEFFC
    static let grayDeactivatedButton = UIColor(hex: 0xECEFFC)
    /// #F2F2F2 8%
    static let lightGrayPopup = UIColor(hex: 0xF2F2F2, alpha: 0.8)
    // CreateLesson
    /// #FBD300
    static let yellowSlider = UIColor(hex: 0xFBD300)
    /// #E5E7F1
    static let graySlider = UIColor(hex: 0xE5E7F1)
    /// #E0E4F3
    static let grayTimeSlider = UIColor(hex: 0xE0E4F3)
    // Lesson
    /// #000000 70%
    static let lessonTextDescription = UIColor(hex: 0x000000, alpha: 0.7)
    /// #2F3243 30%
    static let shadowSupport = UIColor(red: 0.663, green: 0.741, blue: 0.854, alpha: 0.3)
    /// #23242D 82%
    static let firstGradientColor = UIColor(red: 0.136, green: 0.143, blue: 0.175, alpha: 0.82)
    /// #3E4A75 100%
    static let nearestGradientMain = UIColor(red: 0.244, green: 0.292, blue: 0.458, alpha: 1)
    /// #2A2C37 4%
    static let secondGradientColor = UIColor(red: 0.165, green: 0.174, blue: 0.217, alpha: 0.04)
    /// #23242D 80%
    static let gradientColor1 = UIColor(red: 0.136, green: 0.143, blue: 0.175, alpha: 0.8)
    /// #E8EDFF
    static let grayOwnerViewBackgroud = UIColor(hex: 0xE8EDFF)
    /// #8A53FF
    static let purpleLessonPrice = UIColor(hex: 0x8A53FF)
    /// #C6D1E2
    static let lightGrayIcon = UIColor(hex: 0xC6D1E2)
    /// #A35BFF
    static let purpleAttributeIcon = UIColor(hex: 0xA35BFF)
    /// #49CDCD
    static let turquoiseAttributeIcon = UIColor(hex: 0x49CDCD)
    /// #9F3FFF
    static let purpleCategoryIcon = UIColor(hex: 0x9F3FFF)
    /// #00C2FF
    static let cyanCategoryIcon = UIColor(hex: 0x00C2FF)
    /// #2FEECC
    static let turquoiseCategoryIcon = UIColor(hex: 0x2FEECC)
    /// #FFC137
    static let yellowCategoryIcon = UIColor(hex: 0xFFC137)
    /// #F65252
    static let redCategoryIcon = UIColor(hex: 0xF65252)
    /// #EE2F96
    static let pinkCategoryIcon = UIColor(hex: 0xEE2F96)
    /// #2F9EEE
    static let blueCategoryIcon = UIColor(hex: 0x2F9EEE)
    /// #2EE49A
    static let greenCategoryIcon = UIColor(hex: 0x2EE49A)
    /// #FFA51F
    static let darkYellowCategoryIcon = UIColor(hex: 0xFFA51F)
    /// #2F64EE
    static let darkBlueCategoryIcon = UIColor(hex: 0x2F64EE)
    /// #46AFEA
    static let darkCyanCategoryIcon = UIColor(hex: 0x46AFEA)
    /// #22D9B8
    static let darkTurquoiseCategoryIcon = UIColor(hex: 0x22D9B8)
    /// #AE52F6
    static let lightPurpleCategoryIcon = UIColor(hex: 0xAE52F6)
    /// #34CE5F
    static let darkGreenCategoryIcon = UIColor(hex: 0x34CE5F)
    /// #6D809F
    static let grayAttributeTitle = UIColor(hex: 0x6D809F)
    /// #5DCC4C
    static let greenIndicator = UIColor(hex: 0x5DCC4C)
    /// #DADDED
    static let grayIndicator = UIColor(hex: 0xDADDED)
    /// #0098FF
    static let blueCollectionCell = UIColor(hex: 0x0098FF)
    /// #D7DFED
    static let grayCollectionCell = UIColor(hex: 0xD7DFED)
    /// #3E4A75, 100 %
    static let gradientMain = UIColor(hex: 0x3E4A75)
    /// #2A2C37, 4 %
    static let gradient4 = UIColor(hex: 0x2A2C37, alpha: 0.04)
    /// #CDDBE4, 50 %
    static let gradientMainLight = UIColor(hex: 0xCDDBE4, alpha: 0.5)
    /// 3E4A75, 100 %
    static let secondMainGradient = UIColor(hex: 0x3E4A75)
    /// 2A324F, 100%
    static let popularGradient = UIColor(hex: 0x2A324F)
    /// 23242D, 80%
    static let liveGradient = UIColor(hex: 0x23242D, alpha: 0.8)
    /// #2B2D38 80%
    static let lessonTimeAlphaGray80 = UIColor(hex: 0x2B2D38, alpha: 0.8)
    /// #2B2D38 50%
    static let lessonTimeAlphaGray50 = UIColor(hex: 0x2B2D38, alpha: 0.5)
    /// #CBCEDC
    static let promotionShadow = UIColor(hex: 0xCBCEDC)
    // Search
    /// #FF003D
    static let searchWarning = UIColor(hex: 0xFF003D)
    /// #FA59B0
    static let femaleColor = UIColor(hex: 0xFA59B0)
    /// #0098FF
    static let maleColor = UIColor(hex: 0x0098FF)
    /// #E2E7F9
    static let separatorFilter = UIColor(hex: 0xE2E7F9)

    // Stream feed
    /// #FE4747
    static let microRed = UIColor(hex: 0xFE4747)
    /// #484C60 95%
    static let streamPopUpGray = UIColor(hex: 0x484C60, alpha: 0.95)
    /// #C4C6CF
    static let streamGrayLabel = UIColor(hex: 0xC4C6CF)
    /// #484C60, 95 %
    static let streamHeaderBackground = UIColor(hex: 0x484C60, alpha: 0.95)
    /// #55596D
    static let balanceViewGray = UIColor(hex: 0x55596D)
    /// #FE6868
    static let pinkCollectionItem = UIColor(hex: 0xFE6868)
    /// #6C809F
    static let unselectedTimeItem = UIColor(hex: 0x6C809F)
    /// #54596D
    static let selectedTimeItem = UIColor(hex: 0x54596D)
    /// #686E85
    static let selectedTimeItemBorder = UIColor(hex: 0x686E85)
    static let StreamGrayLabel = UIColor(hex: 0xC4C6CF)
    /// #54596D
    static let streamSelectedBackground = UIColor(hex: 0x54596D)
    /// #7F8EA7
    static let grayPageIndicator = UIColor(hex: 0x7F8EA7)
    /// #C4C6CF
    static let grayMembersLabel = UIColor(hex: 0xC4C6CF)
    /// #6C809F
    static let grayReloadButton = UIColor(hex: 0x6C809F)
    /// #F35058
    static let likeButton = UIColor(hex: 0xF35058)
}

// MARK: - Stream feed hints

extension TalkmeColors {
    /// #1F2026 80%
    static let streamHintBG = UIColor(hex: 0x1F2026).withAlphaComponent(0.8)
    static let streamCommentPlaceholder = UIColor(hex: 0x6A737A).withAlphaComponent(0.8)
    /// #3E4A74
    static let streamPlugScreen = UIColor(hex: 0x3E4A74)
    /// #181920
    static let streamBG = UIColor(hex: 0x181920)
}

// MARK: - Stream chat

extension TalkmeColors {
    static let messageAuthor = UIColor(hex: 0x0098FF)
    static let messageText = UIColor.white
    static let messageBG = UIColor.black.withAlphaComponent(0.25)
    static let avatarBG = UIColor(hex: 0x2A2D30)
    static let sendMessageBG = UIColor(hex: 0x484C60)
    static let sendMessagePlaceholder = UIColor(hex: 0x868B9F)
}

// MARK: - Chats

extension TalkmeColors {
    /// #E0EAFF
    static let selectedChatLightBlue = UIColor(hex: 0xE0EAFF)
    /// #BCC2DC
    static let dashBorderGray = UIColor(hex: 0xBCC2DC)
    /// #59CC4A
    static let greenSendMessageButton = UIColor(hex: 0x59CC4A)
    /// #96A8C3
    static let chatsSendMessagePlaceholder = UIColor(hex: 0x96A8C3)
    /// #F5F7FB
    static let popUoPayToJoin = UIColor(hex: 0xF5F7FB)
    /// #EEF1F6
    static let searchbarNewChatUser = UIColor(hex: 0xEEF1F6)
    
    static let gradientLightGreen = UIColor(red: 13, green: 186, blue: 93)

    static let gradientGreen = UIColor(red: 13, green: 186, blue: 155)
    
    static let vkIconColor = UIColor(red: 0, green: 87, blue: 254)

    static let telegramIconColor = UIColor(red: 20, green: 170, blue: 255)

    static let odnoklasnikiIconColor = UIColor(red: 255, green: 138, blue: 0)
    
    static let darkColorView = UIColor(red: 3, green: 14, blue: 22, alpha: 0.4)
    
    static let gradientLightBlue = UIColor(red: 59, green: 73, blue: 183)

    static let gradientBlue = UIColor(red: 1, green: 152, blue: 255)

    static let gradientLightPurple = UIColor(red: 23, green: 56, blue: 227, alpha: 0.9)

    static let gradientPurple = UIColor(red: 113, green: 23, blue: 227, alpha: 0.9)
    
    static let notGradient = UIColor(red: 3, green: 14, blue: 22)
    
    static let dummyBacgroundColor = UIColor(red: 6, green: 22, blue: 32, alpha: 0.8)
    
    static let swipeLineColor = UIColor(red: 84, green: 94, blue: 100)
    
    static let blueBorderColor = UIColor(red: 24, green: 119, blue: 225)
    
    static let pinkBorder = UIColor(red: 255, green: 84, blue: 84)
    
    static let otherDarkViewColor = UIColor(red: 94, green: 103, blue: 108)
    
    static let darkViewColor = UIColor(red: 19, green: 27, blue: 29)
}
