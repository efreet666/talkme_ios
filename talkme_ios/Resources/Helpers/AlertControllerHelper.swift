//
//  AlertControllerHelper.swift
//  talkme_ios
//
//  Created by Андрей Гедзюра on 09.03.2021.
//

import UIKit

enum AlertControllerHelper {

    private static var topViewController: UIViewController? {
        guard let app = UIApplication.shared.delegate as? AppDelegate,
              let window = app.window,
              let topRootVC = window.rootViewController else { return nil }
        var topController = topRootVC
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        guard !(topController is UIAlertController) else { return nil }
        return topController
    }

    static func showFinishStreamAlert(action: @escaping () -> Void) {
        let alert = UIAlertController(
            title: "",
            message: "",
            preferredStyle: .alert
        )
        let finishAction = UIAlertAction(title: "popup_delete_lesson_yes".localized, style: .default) {_ in
            action()
        }
        let cancelAction = UIAlertAction(title: "popup_delete_lesson_no".localized, style: .cancel)
        let titleFont = [NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 15)]
        let messageFont = [NSAttributedString.Key.font: UIFont.montserratFontRegular(ofSize: 12)]
        let titleAlert = NSMutableAttributedString(string: "talkme_popup_finish_stream_title".localized, attributes: titleFont)
        let messageAlert =  NSMutableAttributedString(string: "talkme_popup_finish_stream_message".localized, attributes: messageFont)

        alert.setValue(titleAlert, forKey: "attributedTitle")
        alert.setValue(messageAlert, forKey: "attributedMessage")
        alert.addAction(finishAction)
        alert.addAction(cancelAction)
        alert.preferredAction = finishAction

        DispatchQueue.main.async {
            topViewController?.present(alert, animated: true)
        }
    }

    static func showRedirectToSettingsAlert(title: String) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
        let alert = UIAlertController(
            title: title,
            message: nil,
            preferredStyle: .alert)
        let oKAction = UIAlertAction(title: "general_ok".localized, style: .default, handler: { _ in
            UIApplication.shared.open(settingsUrl)
        })
        let closeAction = UIAlertAction(title: "general_cancel".localized, style: .cancel)

        alert.addAction(oKAction)
        alert.addAction(closeAction)
        topViewController?.present(alert, animated: true)
    }

    static func showLogoutAlert(action: @escaping () -> Void) {
        let alert = UIAlertController(
            title: "",
            message: "",
            preferredStyle: .alert
        )
        let finishAction = UIAlertAction(title: "popup_delete_lesson_yes".localized, style: .default) {_ in
            action()
        }
        let cancelAction = UIAlertAction(title: "popup_delete_lesson_no".localized, style: .cancel)
        let titleFont = [NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 15)]
        let titleAlert = NSMutableAttributedString(string: "logout_alert_profile".localized, attributes: titleFont)

        alert.setValue(titleAlert, forKey: "attributedTitle")
        alert.addAction(finishAction)
        alert.addAction(cancelAction)
        alert.preferredAction = finishAction

        DispatchQueue.main.async {
            topViewController?.present(alert, animated: true)
        }
    }

    static func showDeleteMyProfileAlert(action: @escaping () -> Void) {
        let alert = UIAlertController(
            title: "",
            message: "",
            preferredStyle: .alert
        )
        let finishAction = UIAlertAction(title: "popup_delete_profile_yes".localized, style: .destructive) {_ in
            action()
        }
        let cancelAction = UIAlertAction(title: "popup_delete_profile_no".localized, style: .cancel)
        let titleFont = [NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 15)]
        let titleAlert = NSMutableAttributedString(string: "delete_my_account_alert_profile".localized, attributes: titleFont)

        alert.setValue(titleAlert, forKey: "attributedTitle")
        alert.addAction(finishAction)
        alert.addAction(cancelAction)
        alert.preferredAction = finishAction

        DispatchQueue.main.async {
            topViewController?.present(alert, animated: true)
        }
    }

    static func showSimpleOkAlert(title: String) {
        let alert = UIAlertController(
            title: "",
            message: title,
            preferredStyle: .alert
        )

        let oKAction = UIAlertAction(title: "general_ok".localized, style: .default, handler: { _ in })

        alert.addAction(oKAction)
        topViewController?.present(alert, animated: true)
    }

    static func present(_ alert: UIAlertController) {
        topViewController?.present(alert, animated: false)
    }

    // error codes for showNetworkErrorAlert (see below)

    // ChangePasswordViewModel - 1
    // ResetPasswordViewModel - 2
    // ResetPasswordConfirmCodeViewModel - 3
    // AuthEnterDataViewModel - 4
    // RegistrationControllerViewModel - 5
    // RegistrationConfirmCodeViewModel - 6
    // SignInViewModel - 7
    // StreamsFeedViewModel - 8
    // SavedStreamsFeedViewModel - 9
    // version 373

    static func showNetworkErrorAlert(errorMessage: String?, action: @escaping () -> Void) {
        let alert = UIAlertController(
            title: "",
            message: errorMessage == nil ? "" : errorMessage!,
            preferredStyle: .alert
        )
        let finishAction = UIAlertAction(title: "general_ok".localized, style: .default) {_ in
            action()
        }
        let titleFont = [NSAttributedString.Key.font: UIFont.montserratSemiBold(ofSize: 15)]
        let titleAlert = NSMutableAttributedString(string: "network_error_title".localized, attributes: titleFont)

        alert.setValue(titleAlert, forKey: "attributedTitle")
        alert.addAction(finishAction)
        alert.preferredAction = finishAction

        DispatchQueue.main.async {
            topViewController?.present(alert, animated: true)
        }
    }
}
