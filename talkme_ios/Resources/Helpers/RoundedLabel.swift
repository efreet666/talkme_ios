//
//  RoundedLabel.swift
//  talkme_ios
//
//  Created by Yura Fomin on 03.02.2021.
//

import UIKit

class RoundedLabel: UILabel {

    private var topInset: CGFloat
    private var bottomInset: CGFloat
    private var leftInset: CGFloat
    private var rightInset: CGFloat

    required init(withInsets top: CGFloat, _ bottom: CGFloat, _ left: CGFloat, _ right: CGFloat) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
        self.clipsToBounds = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
    }

    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let rect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
        let insets = UIEdgeInsets(top: -topInset, left: -leftInset, bottom: -bottomInset, right: -rightInset)
        return rect.inset(by: insets)
    }
}
