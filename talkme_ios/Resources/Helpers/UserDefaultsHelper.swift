//
//  UserDefaultsHelper.swift
//  talkme_ios
//
//  Created by Yura Fomin on 29.12.2020.
//

import RxCocoa

final class UserDefaultsHelper {

    private enum UDKey: String {
        case refreshToken
        case accessToken
        case fullName
        case avatarImage
        case isSpecialist
        case classNumber
        case countryCode
        case geoId
        case wasHintShown
        case tokBoxUniqueId
        case userId
        case isBlockedVideoByPub
        case isBlockedAudioByPub
        case blockCameraByMyself
        case blockMicrophoneByMyself
        case cameraTimeEnd
        case isLoggedFromSocials
        case isProd
        case userTypeOfNumberClass
        case isFirstStart
        case appsFlyerData
        case lastVersionCheckDate
    }

    // MARK: - Public properties

    static let shared = UserDefaultsHelper()

    var userTypeOfNumberClass: String {
        get { getValue(.userTypeOfNumberClass ) as? String ?? "" }
        set { setValue(newValue ?? "", key: .userTypeOfNumberClass) }
    }

    var isProd: Bool {
        get { getValue(.isProd) as? Bool ?? true }
        set { setValue(newValue, key: .isProd) }
    }

    var refreshToken: String {
        get { getValue(.refreshToken) as? String ?? "" }
        set { setValue(newValue, key: .refreshToken) }
    }

    var accessToken: String {
        get { getValue(.accessToken) as? String ?? "" }
        set { setValue(newValue, key: .accessToken) }
    }

    var fullName: String {
        get { getValue(.fullName) as? String ?? "" }
        set { setValue(newValue, key: .fullName) }
    }

    var avatarImage: String {
        get { getValue(.avatarImage) as? String ?? "" }
        set { setValue(newValue, key: .avatarImage) }
    }

    var isSpecialist: Bool {
        get { getValue(.isSpecialist) as? Bool ?? false }
        set { setValue(newValue, key: .isSpecialist) }
    }

    var classNumber: String {
        get { getValue(.classNumber) as? String ?? "" }
        set { setValue(newValue, key: .classNumber) }
    }

    var countryCode: String {
        get { getValue(.countryCode) as? String ?? "" }
        set { setValue(newValue, key: .countryCode) }
    }

    var geoId: String {
        get { getValue(.geoId) as? String ?? "" }
        set { setValue(newValue, key: .geoId) }
    }

    var wasHintShown: Bool {
        get { getValue(.wasHintShown) as? Bool ?? false }
        set { setValue(newValue, key: .wasHintShown) }
    }

    var tokBoxUniqueId: String {
        get { getValue(.tokBoxUniqueId) as? String ?? "" }
        set { setValue(newValue, key: .tokBoxUniqueId) }
    }

    var userId: Int {
        get { getValue(.userId) as? Int ?? 0 }
        set { setValue(newValue, key: .userId) }
    }

    var isBlockedVideoByPub: Bool {
        get { getValue(.isBlockedVideoByPub) as? Bool ?? false }
        set { setValue(newValue, key: .isBlockedVideoByPub) }
    }

    var isBlockedAudioByPub: Bool {
        get { getValue(.isBlockedAudioByPub) as? Bool ?? false }
        set { setValue(newValue, key: .isBlockedAudioByPub) }
    }

    var blockCameraByMyself: Bool {
        get { getValue(.blockCameraByMyself) as? Bool ?? false }
        set { setValue(newValue, key: .blockCameraByMyself) }
    }

    var blockMicrophoneByMyself: Bool {
        get { getValue(.blockMicrophoneByMyself) as? Bool ?? false }
        set { setValue(newValue, key: .blockMicrophoneByMyself) }
    }

    var cameraTimeEnd: Double? {
        get { getValue(.cameraTimeEnd) as? Double }
        set { setValue(newValue, key: .cameraTimeEnd) }
    }

    var isLoggedfromSocials: Bool {
        get { getValue(.isLoggedFromSocials) as? Bool ?? false }
        set { setValue(newValue, key: .isLoggedFromSocials) }
    }

    var isFirstStart: Bool {
        get { getValue(.isFirstStart) as? Bool ?? true }
        set { setValue(newValue, key: .isFirstStart) }
    }

    var appsFlyerData: [AnyHashable: Any] {
        get { getValue(.appsFlyerData) as? [AnyHashable: Any] ?? [:] }
        set { setValue(newValue, key: .appsFlyerData) }
    }

    var lastVersionCheckDate: Date? {
        get { getValue(.lastVersionCheckDate) as? Date }
        set { setValue(newValue, key: .lastVersionCheckDate) }
    }

    // MARK: - Public Methods

    func deleteTokenAndUserInfo() {
        self.refreshToken = ""
        self.accessToken = ""
        self.tokBoxUniqueId = ""
        self.userId = 0
        self.isSpecialist = false
    }

    // MARK: - Initializers

    private init() { }

    // MARK: - Private Properties

    private let suiteName = "talkmeUserDefaults"
    private lazy var userDefaults = UserDefaults(suiteName: suiteName)

    // MARK: - Private Methods

    private func setValue(_ value: Any, key: UDKey) {
        userDefaults?.set(value, forKey: key.rawValue)
        userDefaults?.synchronize()
    }

    private func getValue(_ key: UDKey) -> Any? {
        return userDefaults?.value(forKey: key.rawValue)
    }
}
