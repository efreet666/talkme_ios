//
//  TimerManager.swift
//  talkme_ios
//
//  Created by Yura Fomin on 10.02.2021.
//

import Foundation

enum TimerManager {

    static func diffFromNowTime(timeInterval: TimeInterval) -> TimeInterval {
        let nowTime = Date().timeIntervalSince1970
        let currentTime = nowTime - timeInterval
        return currentTime
    }

    static func diffFromFutureTime(timeInterval: TimeInterval) -> TimeInterval {
        let nowTime = Date().timeIntervalSince1970
        let currentTime = timeInterval - nowTime
        return currentTime
    }
}
