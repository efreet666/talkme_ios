//
//  Formatters.swift
//  talkme_ios
//
//  Created by 1111 on 09.01.2021.
//

import Foundation

final class Formatters {

    // MARK: - Public Properties

    static let dateFormatterDefault: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()

    static let dateComponentsFormatter: DateComponentsFormatter = {
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "ru_RU")
        let dateComponentsFormatter = DateComponentsFormatter()
        dateComponentsFormatter.allowedUnits = .year
        dateComponentsFormatter.unitsStyle = .full
        dateComponentsFormatter.calendar = calendar
        return dateComponentsFormatter
    }()

    static let formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter
    }()

    static let timeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }()

    static let timeFormatterWithout0: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm"
        return dateFormatter
    }()

    static let hoursFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.zeroFormattingBehavior = .pad
        return formatter
    }()

    static let durationFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.zeroFormattingBehavior = .dropLeading
        return formatter
    }()

    static let lessonTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Formatters.kUTCTimeZone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()

    static let createLessonTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()

    static let lessonDateformatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        var calendar = Calendar.current
        dateFormatter.locale = Locale(identifier: "ru_RU" )
        dateFormatter.dateFormat = "dd.MMM.yyyy"
        return dateFormatter
    }()

    static let detailedLessonFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "d MMM yyyy"
        return dateFormatter
    }()

    static let spaceAfterThousand: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        return formatter
    }()

    static let dateLessonFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()

    static let filterDateformatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU" )
        dateFormatter.dateFormat = "dd.MMM"
        return dateFormatter
    }()

    static let streamTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Formatters.kUTCTimeZone
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter
    }()

    // MARK: - Private Properties

    static let kUTCTimeZone = TimeZone(identifier: "UTC")

    private static let dateToTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU" )
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }()

    private static let dateToMonthFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd"
        return dateFormatter
    }()

    private static let dateMonthFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "d"
        return dateFormatter
    }()

    static let hourMinuteSecondsFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter
    }()

    private static let hourMinuteSecondsLocalFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Formatters.kUTCTimeZone
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter
    }()

    static let minutesSecondsLocalFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Formatters.kUTCTimeZone
        dateFormatter.dateFormat = "mm:ss"
        return dateFormatter
    }()

    private static let secondsMilisecondsLocalFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Formatters.kUTCTimeZone
        dateFormatter.dateFormat = "ss.SS"
        return dateFormatter
    }()

    private static let hourAndMinutesFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        let localHourComponent = String(format: "time_hour_h_component".localized, "")
        let localMinuteComponent = String(format: "time_minute_m_component".localized, "")
        dateFormatter.dateFormat = "H\(localHourComponent) mm\(localMinuteComponent)"
        return dateFormatter
    }()
    
    private static let hourAndMinutesWithoutLettersFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        let localHourComponent = String(format: "time_hour_h_component".localized, "")
        let localMinuteComponent = String(format: "time_minute_m_component".localized, "")
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }()

    private static let hourAndMinutesFormatterLocalized: DateFormatter = {
        let dateFormatter = DateFormatter()
        let localHourComponent = String(format: "time_hour_h_component".localized, "")
        let localMinuteComponent = String(format: "time_minute_min_component".localized, "")
        dateFormatter.dateFormat = "H'\(localHourComponent)' mm '\(localMinuteComponent)'"
        return dateFormatter
    }()

    private static let hourAndMinutesFormatterShortAndLocalized: DateFormatter = {
        let dateFormatter = DateFormatter()
        let localHourComponent = String(format: "time_hour_h_component".localized, "")
        let localMinuteComponent = String(format: "time_minute_m_component".localized, "")
        dateFormatter.dateFormat = "H'\(localHourComponent)' mm'\(localMinuteComponent)'"
        return dateFormatter
    }()

    // MARK: - Public Methods

    static func checkingForMillionAndThousand(count: Int) -> String {
        if count > 1000000 {
            let countMillion = Int(count / 1000000)
            let thousand = Int((count - countMillion * 1000000) / 1000)
            return "\(countMillion).\(thousand)M"
        } else if count > 1000 {
            let countThousand = Int(count / 1000)
            return "\(countThousand)K"
        } else {
            return "\(count)"
        }
    }

    static func minutesMoreTheSixtyAndSeconds(seconds: Double) -> String {
        let mins = seconds / 60
        let secs = seconds.truncatingRemainder(dividingBy: 60)
        let timeformatter = NumberFormatter()
        timeformatter.minimumIntegerDigits = 2
        timeformatter.minimumFractionDigits = 0
        timeformatter.roundingMode = .down
        guard var minsStr = timeformatter.string(from: NSNumber(value: mins)),
              var secsStr = timeformatter.string(from: NSNumber(value: secs))
        else { return "00:00"}

        minsStr = minsStr == "-00" ? "00" : minsStr
        secsStr = secsStr == "-00" ? "00" : secsStr

        return "\(minsStr):\(secsStr)"
    }

    static func convertTime(seconds: Double) -> String {
        let date = Date(timeIntervalSince1970: seconds)
        let str = Formatters.dateToTimeFormatter.string(from: date)
        return str
    }

    static func convertCount(number: Int) -> String {
        let number = Formatters.spaceAfterThousand.string(from: NSNumber(value: number)) ?? ""
        return number
    }

    static func convertDate(seconds: Double) -> String {
        let date = Date(timeIntervalSince1970: seconds)
        let stringMonth = date.hardcodedRuMonth
        let str = Formatters.dateToMonthFormatter.string(from: date)
        let allString = str + " " + stringMonth
        return allString
    }

    static func convertDate3LettersMonth(_ seconds: Double) -> String {
        let date = Date(timeIntervalSince1970: seconds)
        var stringMonth = date.hardcodedRuMonth
        stringMonth = String(stringMonth[stringMonth.startIndex...stringMonth.index(stringMonth.startIndex, offsetBy: 2)])
        let str = Formatters.dateMonthFormatter.string(from: date)
        let allString = str + " " + stringMonth
        return allString
    }

    static func convertDateDayMonthYear(from seconds: Double) -> String {
        let date = Date(timeIntervalSince1970: seconds)
        var str = detailedLessonFormatter.string(from: date).lowercased()
        str.removeAll(where: { $0 == "."})
        return str
    }

    static func setupHourAndMinute(date: String) -> String {
        let myDate = Formatters.hourMinuteSecondsFormatter.date(from: date)
        guard let newDate = myDate else {  return "" }
        let str = Formatters.hourAndMinutesFormatter.string(from: newDate)
        return str
    }

    static func setUpHourAndMinuteWithoutZeroHoursLocalized(date: String) -> String {
        guard
            let newDate = Formatters.hourMinuteSecondsFormatter.date(from: date),
            let calendar = Formatters.hourMinuteSecondsFormatter.calendar
        else { return "" }
        let hour = calendar.component(.hour, from: newDate)
        let minute = calendar.component(.minute, from: newDate)
        if hour == 0 {
            return String(format: "time_minute_min_component".localized, "\(minute) ")
        } else {
            return Formatters.hourAndMinutesFormatterLocalized.string(from: newDate)
        }
    }

    static func setUpHourAndMinWithoutZeroHoursLocalized(date: String) -> String {
        guard
            let newDate = Formatters.hourMinuteSecondsFormatter.date(from: date),
            let calendar = Formatters.hourMinuteSecondsFormatter.calendar
        else { return "" }
        let hour = calendar.component(.hour, from: newDate)
        let minute = calendar.component(.minute, from: newDate)
        if hour == 0 {
            return String(format: "time_minute_m_component".localized, "\(minute)")
        } else {
            return Formatters.hourAndMinutesFormatter.string(from: newDate)
        }
    }

    static func hourMinuteSecondsString(interval: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: interval)
        return Self.hourMinuteSecondsLocalFormatter.string(from: date)
    }

    static func getIntervalWithDuration(duration: String, dateStart: Double) -> Double {
        let durationInSeconds = self.getInterval(from: duration)
        let dateEnd = dateStart + durationInSeconds
        return dateEnd
    }

    static func getInterval(from stringInterval: String) -> Double {
        let timeStrings = stringInterval.components(separatedBy: ":")
        let hour = (Int((timeStrings[0])) ?? 0) * 3600
        let minutes = (Int((timeStrings[1])) ?? 0) * 60
        let seconds = (Int((timeStrings[2])) ?? 0)
        let durationInSeconds = Double(hour + minutes + seconds)
        return durationInSeconds
    }

    static func minutesSeconds(time: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: time)
        return minutesSecondsLocalFormatter.string(from: date)
    }

    static func lessonDateString(_ date: String?) -> String? {
        guard let stringDate = date,
              let date = Self.createLessonTimeFormatter.date(from: stringDate) else {
            return nil
        }
        return Self.lessonDateformatter.string(from: date)
    }

    static func setupLessonDate(_ date: String?) -> String? {
        guard let stringDate = date,
              let date = Self.createLessonTimeFormatter.date(from: stringDate) else {
            return nil
        }
        return Self.dateToTimeFormatter.string(from: date)
    }

    static func setupHourAndMinute(_ date: String?) -> String? {
        guard let stringDate = date,
              let date = Self.hourMinuteSecondsFormatter.date(from: stringDate) else {
            return nil
        }
        return Self.dateToTimeFormatter.string(from: date)
    }

    static func convertStringToDate(_ date: String?) -> String? {
        guard let stringDate = date,
              let date = Self.lessonDateformatter.date(from: stringDate) else {
            return nil
        }
        return Self.dateFormatterDefault.string(from: date)
    }

    static func setupTodayDate() -> String {
        let date = Date()
        return Self.lessonDateformatter.string(from: date)
    }

    static func setupTodayTime() -> String {
        let date = Date()
        return Self.dateToTimeFormatter.string(from: date)
    }

    static func lessonDateToString(_ date: String?) -> String? {
        guard let stringDate = date,
              let date = Self.createLessonTimeFormatter.date(from: stringDate) else {
            return nil
        }
        return Self.dateLessonFormatter.string(from: date)
    }

    static func setupHourAndMinuteWithout0(_ date: String?) -> String? {
        guard let stringDate = date,
              let date = Self.hourMinuteSecondsFormatter.date(from: stringDate) else {
            return nil
        }
        return Self.timeFormatterWithout0.string(from: date)
    }

    static func setupMinutesAndSecondsWithout0(_ stringDate: String) -> String? {
        guard let date = Self.hourMinuteSecondsFormatter.date(from: stringDate) else {
            return nil
        }
        return Self.minutesSecondsLocalFormatter.string(from: date)
    }

    static func convertMinutesIntToString(minutes: Int) -> String {
        let min = minutes % 60
        let hours = (minutes - min) / 60
        let timeString = "\(hours):\(min)"
        guard let date = Formatters.dateToTimeFormatter.date(from: timeString) else { return "23:59" }
        let dateString = Formatters.dateToTimeFormatter.string(from: date)
        return dateString
    }

    static func transformDateToFilterStringWithFrom(fromDate: Date) -> String {
        let stringFromDate = Formatters.filterDateformatter.string(from: fromDate)
        var dateString = "\("filter_lesson_date_from".localized) \(stringFromDate)"
        if dateString.last == "." {
            dateString = String(dateString.dropLast())
        }
        guard let firstString = dateString.split(separator: ".").first, let secondString = dateString.split(separator: ".").last else { return "" }
        let fixDateString = "\(firstString)  \(secondString)"
        return fixDateString
    }

    static func transformDateToFilterStringWithTo(toDate: Date) -> String {
        let stringToDate = Formatters.filterDateformatter.string(from: toDate)
        var dateString = "\("filter_lesson_date_to".localized) \(stringToDate)"
        if dateString.last == "." {
            dateString = String(dateString.dropLast())
        }
        guard let firstString = dateString.split(separator: ".").first, let secondString = dateString.split(separator: ".").last else { return "" }
        let fixDateString = "\(firstString)  \(secondString)"
        return fixDateString
    }

    static func getHoursAndMinutes(from dateInSeconds: Double) -> String {
        let timeInterval = TimeInterval(dateInSeconds)
        let date = Date(timeIntervalSince1970: timeInterval)
        return Formatters.hourAndMinutesFormatterShortAndLocalized.string(from: date)
    }

    static func hoursMinutesWithoutLetters(date: String) -> String {
        let myDate = Formatters.hourMinuteSecondsFormatter.date(from: date)
        guard let newDate = myDate else {  return "" }
        let str = Formatters.hourAndMinutesWithoutLettersFormatter.string(from: newDate)
        return str
    }

    static func currentSecondsAndMiliseconds(from dateInSeconds: Double) -> String {
        let timeInterval = TimeInterval(dateInSeconds)
        let date = Date(timeIntervalSince1970: timeInterval)
        return Formatters.secondsMilisecondsLocalFormatter.string(from: date)
    }
}
