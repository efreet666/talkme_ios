//
//  IAPManager.swift
//  talkme_ios
//
//  Created by 1111 on 17.05.2021.
//

import Foundation
import StoreKit
import RxSwift
import RxCocoa

final class IAPManager: NSObject {

    static let shared = IAPManager()

    private override init() {}

    let isTapped = PublishRelay<Void>()
    let setupTimer = PublishRelay<Void>()
    let bag = DisposeBag()
    var purchases = [GetPlansResponse]()

    private var products = [SKProduct]()
    private let paymentQueue = SKPaymentQueue.default()
    private let service = PaymentsService()

    public func setupPurchases(callBack: @escaping(Bool) -> Void) {
        if SKPaymentQueue.canMakePayments() {
            paymentQueue.add(self)
            callBack(true)
            return
        }
        callBack(false)
    }

    public func getProducts(identifiers: [String]) {
        let productRequest = SKProductsRequest(productIdentifiers: Set(identifiers))
        productRequest.delegate = self
        productRequest.start()
    }

    public func purchase(productWith identifire: String) {
        guard let product = products.filter({ $0.productIdentifier == identifire }).first else { return }
        let payment = SKPayment(product: product)
        paymentQueue.add(payment)
        isTapped.accept(())

        AmplitudeProvider.shared.currentPurchase = purchases.filter({ $0.iosInAppId == Int(identifire) ?? 0}).first
        AmplitudeProvider.shared.currentProduct = product
        AppsFlyerProvider.shared.currentPurchase = purchases.filter({ $0.iosInAppId == Int(identifire) ?? 0}).first
        AppsFlyerProvider.shared.currentProduct = product
    }

    public func price(for id: String) -> String? {
        var price: String?
        products.forEach { product in
            guard let symbol = product.priceLocale.currencySymbol,
                  product.productIdentifier == id
            else { return }
            if symbol.contains("$") {
                price = "\(product.price.doubleValue.roundToDecimal(2)) \(symbol)"
            } else {
                price = "\(product.price.intValue) \(symbol)"
            }
        }
        return price
    }
}

extension IAPManager: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .deferred:
                isTapped.accept(())
            case .purchasing:
                break
            case .failed:
                isTapped.accept(())
                failed(transaction)
            case .purchased:
                isTapped.accept(())
                completed(transaction)
            case .restored:
                print("[IAP] restored")
            @unknown default:
                print("[IAP] unknown error")
            }
        }
    }

    private func failed(_ transaction: SKPaymentTransaction) {
        if let transactionError = transaction.error as NSError? {
            if transactionError.code != SKError.paymentCancelled.rawValue {
                print("transaction error \(transaction.error!.localizedDescription)")
            }
        }
        paymentQueue.finishTransaction(transaction)
    }

    private func completed(_ transaction: SKPaymentTransaction) {
        paymentQueue.finishTransaction(transaction)
        setupTimer.accept(())
        guard let receiptURL = Bundle.main.appStoreReceiptURL,
              let receiptString = try? Data(contentsOf: receiptURL).base64EncodedString()
        else { return }

        AmplitudeProvider.shared.buyCoinsTraking()
        AppsFlyerProvider.shared.trackUserBoughtCoins()

        service
            .sendReceipt(request: SendReceiptRequest(receipt: receiptString))
            .subscribe()
            .disposed(by: bag)
    }
}

extension IAPManager: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
    }
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
