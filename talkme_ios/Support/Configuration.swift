//
//  Configurations.swift
//  talkme_ios
//
//  Created by 1111 on 16.12.2020.
//
import Foundation

enum Configuration {
    static var isProd: Bool {
        return UserDefaultsHelper.shared.isProd
    }

    static var base: String {
        return isProd ? "talkme.life" : "dev.talkme.life"
    }

    static let baseDomain = "\(base)/api/v1"
    static let wsDomain = "wss://\(base)/ws/"

    static let baseUrl: URL = {

        let urlString = "https://\(baseDomain)"
        guard let url = URL(string: urlString) else {
            fatalError("Unable to init URL for \(urlString)")
        }
        return url
    }()

    static let startUrlString = "https://\(base)"
}
