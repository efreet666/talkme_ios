//
//  CustomSliderWithSteps.swift
//  talkme_ios
//
//  Created by 1111 on 23.01.2021.
//

import UIKit

final class CustomSlider: UISlider {

    // MARK: Interface

    private var trackHeight: CGFloat = UIScreen.isSE ? 6 : 7
    private var selectedTrackColor: UIColor = TalkmeColors.warningColor
    private var unselectedTrackColor: UIColor = TalkmeColors.graySlider
    private var selectedAnchorColor: UIColor = TalkmeColors.warningColor
    private var unselectedAnchorColor: UIColor = TalkmeColors.white
    private var isThumbHidden: Bool = false
    private var anchors: [CGFloat] = [0, 0.15, 0.32, 0.55, 1]
    private var anchorRadius: CGFloat = 6

    // MARK: Overridde methods

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let selectedTrackImage = drawImage(trackColor: selectedTrackColor, anchorColor: selectedAnchorColor)
        let unselectedTrackImage = drawImage(trackColor: unselectedTrackColor, anchorColor: unselectedAnchorColor)
        setMinimumTrackImage(selectedTrackImage, for: .normal)
        setMaximumTrackImage(unselectedTrackImage, for: .normal)
        isThumbHidden ? setThumbImage(UIImage(), for: .normal): setThumbImage(nil, for: .normal)
    }

    // MARK: Private methods

    private func drawImage(trackColor: UIColor, anchorColor: UIColor) -> UIImage? {
        let startX = frame.minX + abs(frame.minX)
        let endX = frame.maxX - abs(frame.minX)
        let size = CGSize(width: endX - startX, height: frame.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        defer {
            UIGraphicsEndImageContext()
        }

        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }

        let offset = trackHeight / 2
        drawLine(inContext: context, between: (startX + offset, endX - offset), color: trackColor)
        drawCircles(inContext: context, between: (startX, endX), color: anchorColor)
        return UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: .zero)
    }

    private func drawLine(inContext context: CGContext,
                          between: (startX: CGFloat, endX: CGFloat),
                          color: UIColor) {
        let startLinePoint = CGPoint(x: between.startX, y: frame.height / 2)
        let endLinePoint = CGPoint(x: between.endX, y: frame.height / 2)

        context.addLines(between: [startLinePoint, endLinePoint])
        context.setLineWidth(trackHeight)
        context.setLineCap(.round)
        context.setStrokeColor(color.cgColor)
        context.strokePath()
    }

    private func drawCircles(inContext context: CGContext,
                             between: (startX: CGFloat, endX: CGFloat),
                             color: UIColor) {
        anchors
            .map { circleCenter(anchor: $0, between: between) }
            .forEach { circleCenter in
                context.addCircle(center: circleCenter, radius: anchorRadius)
                context.setFillColor(color.cgColor)
                context.fillPath()
            }
    }

    private func circleCenter(anchor: CGFloat, between: (startX: CGFloat, endX: CGFloat)) -> CGPoint {
        let pointX = anchor * frame.width
        let xCoordinate = max(between.startX + anchorRadius, min(pointX, between.endX - anchorRadius))
        let yCoordinate = frame.height / 2
        return CGPoint(x: xCoordinate, y: yCoordinate)
    }
}
