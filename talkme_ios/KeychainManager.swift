//
//  KeychainManager.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 25.03.2022.
//

import Foundation
import Security

class KeychainManager {

    static func remove(service: String, account: String) {
        let status = SecItemDelete(modifierQuery(service: service, account: account))
        checkError(status)
    }

    static func save(service: String, account: String, data: String) {
        guard let dataFromString = data.data(using: .utf8, allowLossyConversion: false) else { return }
        let keychainQuery: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: service,
            kSecAttrAccount: account,
            kSecValueData: dataFromString]
        let status = SecItemAdd(keychainQuery as CFDictionary, nil)
        checkError(status)
    }

    static func load(service: String, account: String) -> String? {
        var dataTypeRef: CFTypeRef?
        let status = SecItemCopyMatching(modifierQuery(service: service, account: account), &dataTypeRef)
        if status == errSecSuccess,
           let retrievedData = dataTypeRef as? Data {
            return String(data: retrievedData, encoding: .utf8)
        } else {
            checkError(status)
            return nil
        }
    }

    fileprivate static func modifierQuery(service: String, account: String) -> CFDictionary {
        let keychainQuery: [CFString: Any] = [
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: service,
            kSecAttrAccount: account,
            kSecReturnData: kCFBooleanTrue ?? false]
        return keychainQuery as CFDictionary
    }

    fileprivate static func checkError(_ status: OSStatus) {
        if status != errSecSuccess {
            if #available(iOS 12.0, *),
               let err = SecCopyErrorMessageString(status, nil) {
                print("Operation failed: \(err)")
            } else {
                print("Operation failed: \(status). Check the error message through https://osstatus.com.")
            }
        }
    }
}
