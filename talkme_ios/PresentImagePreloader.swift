//
//  PresentImagePreloader.swift
//  talkme_ios
//
//  Created by Кирилл Блохин on 22.12.2021.
//

import RxSwift
import Kingfisher

class PresentImagePreloader {

    private let paymentsService = PaymentsService()
    private let bag = DisposeBag()

    static let shared = PresentImagePreloader()

    func getPaymentsTypesData() {
        self.paymentsService.getGifts()
            .subscribe { [weak self] event in
                guard let self = self else { return }
                switch event {
                case .success(let response):
                    for img in response {
                        guard let url = URL(string: img.image) else { return }
                        KingfisherManager.shared.retrieveImage(with: url) { _ in return }
                    }
                case .error(let error):
                    print(error)
                }
            }
            .disposed(by: self.bag)
    }
}
